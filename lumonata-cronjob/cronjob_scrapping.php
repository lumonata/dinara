<?php
define('HOSTNAME', 'localhost');
define('DBUSER', 'root');
define('DBPASSWORD', '');
define('DBNAME', 'samaya');

define('ERR_DEBUG', true);

require_once('../lumonata-classes/db.php');

scrapping_save_database();


/* ============================================================================================= */
// FUNCTION UNTUK CROBJOB WEB SCRAPPING DAN SAVE KE DATABASE
/* ============================================================================================= */
function scrapping_save_database(){
    global $db;
    $max_i = 2;

    $qdes = $db->prepare_query("SELECT larticle_id, larticle_title, lsef FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title ASC", "destinations", "publish");
    $rdes = $db->do_query($qdes);
    $ndes = $db->num_rows($rdes);

    if($ndes > 0)
    {
        while($ddes = $db->fetch_array($rdes))
        {
            $sef = $ddes['lsef'];
            $dest_id = $ddes['larticle_id'];
            $html = "";
            
            if($sef == "seminyak")
            {
                $html = file_get_contents('https://www.tripadvisor.com/Hotel_Review-g469404-d603376-Reviews-The_Samaya_Bali_Seminyak-Seminyak_Kuta_District_Bali.html');
            }
            elseif($sef == "ubud")
            {
                $html = file_get_contents('https://www.tripadvisor.com/Hotel_Review-g297701-d1810099-Reviews-The_Samaya_Bali_Ubud-Ubud_Bali.html');
                echo $sef;
            }

            $doc = new DOMDocument();
            libxml_use_internal_errors(TRUE);

            if(!empty($html)){
                $rating = "";
                $overview = "";

                $doc->loadHTML($html);
                libxml_clear_errors();
                
                $xpath = new DOMXPath($doc);

                // GET REVIEW COUNT
                $rows_review_count = $xpath->query('//span[@class="reviewCount"]');
                $review_count = "";
                if($rows_review_count->length > 0){
                    foreach($rows_review_count as $row_review_count){
                        $review_count = $row_review_count->nodeValue;
                    }
                }

                // GET OVERALL RATING
                $rows_overall_rating = $xpath->query('//span[@class="overallRating"]');
                $overall_rating = "";
                if($rows_overall_rating->length > 0){
                    foreach($rows_overall_rating as $row_overall_rating){
                        $overall_rating = $row_overall_rating->nodeValue;
                    }
                }

                // GET TRAVELLER CHOICE BADGE
                $rows_tc_badge = $xpath->query('//span[@class="award_text"]');

                $tc_badge = "";
                if($rows_tc_badge->length > 0){
                    foreach($rows_tc_badge as $row_tc_badge){
                        $tc_badge = $row_tc_badge->nodeValue;
                    }
                }
                $result['review_count']   = $review_count;
                $result['overall_rating'] = $overall_rating;
                $result['tc_badge']       = $tc_badge;

                $value = json_encode($result);

                set_scraaping_setting('trip_advisor_samaya', $value, $dest_id);
            }
        }
    }
}

/* ============================================================================================= */
// FUNCTION UNTUK PROSES SIMPAN DAN UPDATE META DATA SCRAPPING SETTING
/* ============================================================================================= */
function set_scraaping_setting($key, $val, $destination)
{
    global $db;
    
    if(is_num_scrapping_setting($key, $destination))
    {
        $s = 'UPDATE lumonata_meta_data SET lmeta_value=%s
        WHERE lmeta_name=%s AND lapp_name=%s AND lapp_id=%d';
        $q = $db->prepare_query( $s, $val, $key, 'scrapping_value', $destination );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'INSERT INTO lumonata_meta_data(
                    lmeta_name,
                    lmeta_value,
                    lapp_name,
                    lapp_id)
                VALUES(%s,%s,%s,%d)';
        $q = $db->prepare_query( $s, $key, $val, 'scrapping_value', $destination );
        $r = $db->do_query( $q );
    }
    return true;
}


/* ============================================================================================= */
// FUNCTION UNTUK MENGECEK APAKAH META DATA SCRAPPING SETTING SUDAH ADA ATAU BELUM PADA DATABASE
/* ============================================================================================= */
function is_num_scrapping_setting($key, $destination)
{
    global $db;

    $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s AND lapp_id=%d';
    $q = $db->prepare_query( $s, $key, 'scrapping_value', $destination );
    $n = count_rows( $q );

    if( $n > 0 )
    {
        return true;
    }

    return false;
}


function count_rows( $sql )
{
    global $db;
    $r = $db->do_query( $sql );
    return $db->num_rows( $r );
}
?>