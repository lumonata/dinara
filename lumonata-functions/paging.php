<?php
	function page_count($num_rows,$view){
		$kel=$num_rows/$view;
		if ($kel==floor($num_rows/$view)){
			return $kel;
		}else{
			return floor($num_rows/$view)+1;
		} 
	}
	
	function paging($url,$num_rows,$top_page=1,$view=10,$page_view=5){
		if ($num_rows == 0)
		return;
		$top_go=1;
		$page_go=1;
		
		$prev=$top_page-1;
		$next=$top_page+1;
		$page_range=floor($page_view/ 2);
		$html="<ul class=\"pagging\"><li><a href=\"$url"."1\">Newest</a></li>";
		
		if ($top_page!=1)
		{
			$html.="<li><a href=\"$url".$prev."\">Prev</a></li>";
		}else{
			$html.="<li><a href=\"#\" class=\"selected\">Prev</a></li>";
		}
		
		//melipat halaman berdasarkan range yang di inginkan
		if($top_page<$page_view && page_count($num_rows,$view) >= $page_view){
			$top_go=1;
			$page_go=$page_view;
		}elseif($top_page< $page_view && page_count($num_rows,$view) < $page_view){
			if($top_page - $page_range <=1){
				$top_go=1;
			}else{
				$top_go=$top_page - $page_range;
			}
			$page_go=page_count($num_rows,$view);
		
		}elseif($top_page >= $page_view && $top_page <= page_count($num_rows,$view)){
			if($top_page - $page_range<=1){
				$top_go=1;
			}else{
				$top_go=$top_page - $page_range;
			}
			if($top_page +$page_range > page_count($num_rows,$view)){
				$page_go=page_count($num_rows,$view);
			}else{
				$page_go=$top_page + $page_range;
			}
		}
		//stop lipatan
		for($i=$top_go;$i<=$page_go;$i++){
			if ($i==$top_page){
				$html.= "<li><a href=\"#\" class=\"selected\">$i</a></li>";
			}else{
				$html.= "<li><a href=\"$url".$i."\" >$i</a></li>";
			}
		}
		if ($top_page!=page_count($num_rows,$view)){
			$html.="<li><a href=\"$url".$next."\">Next</a></li>";
		}else{
			$html.= "<li><a href=\"#\" class=\"selected\">Next</a></li>";
		}
		
		$html.="<li><a href=\"$url".page_count($num_rows,$view)."\">Oldest</a></li></ul>";
		
		return $html;
	}

	function paging_link($url,$num_rows,$top_page=1,$view=10,$page_view=5,$ps='Prev',$ns='Next',$include_first_last_link=true,$fs='Newest',$ls='Oldest')
	{
		if($num_rows==0) return;
		
		$top_go  = 1;
		$page_go = 1;
		
		$prev  = $top_page-1;
		$next  = $top_page+1;
		$range = floor($page_view/ 2);
		
		$html  = '<ul class="pagging">';
		if($include_first_last_link)
			$html .= '<li><a href="'.$url.'1">'.$fs.'</a></li>';	
		
		if($top_page!=1)
		{
			$html .= '<li><a href="'.$url.$prev.'">'.$ps.'</a></li>';
		}
		else
		{
			$html .= '<li><a href="#" class="selected">'.$ps.'</a></li>';
		}
		
		//-- melipat halaman berdasarkan range yang di inginkan
		if($top_page<$page_view && page_count($num_rows,$view) >= $page_view)
		{
			$top_go  = 1;
			$page_go = $page_view;
		}
		elseif($top_page< $page_view && page_count($num_rows,$view) < $page_view)
		{
			if(($top_page-$range) <= 1)
			{
				$top_go = 1;
			}
			else
			{
				$top_go = $top_page - $range;
			}
			
			$page_go = page_count($num_rows,$view);
		}
		elseif(($top_page >= $page_view) && ($top_page <= page_count($num_rows,$view)))
		{
			if(($top_page-$range) <= 1)
			{
				$top_go = 1;
			}
			else
			{
				$top_go = $top_page - $range;
			}
			
			if(($top_page+$range) > page_count($num_rows,$view))
			{
				$page_go = page_count($num_rows,$view);
			}
			else
			{
				$page_go = $top_page + $range;
			}
		}
		
		//-- stop lipatan
		for($i=$top_go; $i<=$page_go; $i++)
		{
			if($i==$top_page)
			{
				$html .= '<li><a href="#" class="selected">'.$i.'</a></li>';
			}
			else
			{
				$html .= '<li><a href="'.$url.$i.'">'.$i.'</a></li>';
			}
		}
		
		if($top_page!=page_count($num_rows,$view))
		{
			$html .= '<li><a href="'.$url.$next.'">'.$ns.'</a></li>';
		}
		else
		{
			$html .= '<li><a href="#" class="selected">'.$ns.'</a></li>';
		}
		
		if($include_first_last_link)
			$html .= '<li><a href="'.$url.page_count($num_rows,$view).'">'.$ls.'</a></li>';
			
		$html .= '</ul>';
		
		return $html;
	}
	
	function get_paging($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=")
	{		
		//defaults
		if(!$adjacents) $adjacents = 1;
		if(!$limit) $limit = 15;
		if(!$page) $page = 1;
		if(!$targetpage) $targetpage = "/";
		
		//other vars
		$prev = $page - 1;									//previous page is page - 1
		$next = $page + 1;									//next page is page + 1
		$lastpage = ceil($totalitems / $limit);				//lastpage is = total items / items per page, rounded up.
		$lpm1 = $lastpage - 1;								//last page minus 1
		
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$pagination = "";
		$margin = '';
		$padding = '';
		if($lastpage > 1)
		{	
			$pagination .= "<div class=\"pagination\"";
			if($margin || $padding)
			{
				$pagination .= " style=\"";
				if($margin)
					$pagination .= "margin: $margin;";
				if($padding)
					$pagination .= "padding: $padding;";
				$pagination .= "\"";
			}
			$pagination .= ">";
	
			//previous button
			if ($page > 1) 
				$pagination .= "<a class=\"prev\" href=\"$targetpage$pagestring$prev\">&laquo;</a>";
			else
				$pagination .= "<span class=\"prev disabled\">&laquo;</span>";	
			
			//pages	
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\">$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";					
				}
			}
			elseif($lastpage >= 7 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 3))		
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination .= "<span class=\"current\">$counter</span>";
						else
							$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";					
					}
					$pagination .= "<span class=\"elipses\">...</span>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>";		
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
					$pagination .= "<span class=\"elipses\">...</span>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination .= "<span class=\"current\">$counter</span>";
						else
							$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";					
					}
					$pagination .= "...";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>";		
				}
				//close to end; only hide early pages
				else
				{
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
					$pagination .= "<span class=\"elipses\">...</span>";
					for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination .= "<span class=\"current\">$counter</span>";
						else
							$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";					
					}
				}
			}
			
			//next button
			if ($page < $counter - 1) 
				$pagination .= "<a class=\"next\" href=\"" . $targetpage . $pagestring . $next . "\">&raquo;</a>";
			else
				$pagination .= "<span class=\"next disabled\">&raquo;</span>";
			$pagination .= "</div>\n";
		}
		
		return $pagination;
	}

	function getPaginationString($page = 1, $totalitems, $limit = 15, $adjacents = 1, $targetpage = "/", $pagestring = "?page=", $add_url="")
	{
		//defaults
		if(!$adjacents) $adjacents = 1;
		if(!$limit) $limit = 15;
		if(!$page) $page = 1;
		if(!$targetpage) $targetpage = "/";

		//other vars
		$prev = $page - 1;									//previous page is page - 1
		$next = $page + 1;									//next page is page + 1
		$lastpage = ceil($totalitems / $limit);				//lastpage is = total items / items per page, rounded up.
		$lpm1 = $lastpage - 1;								//last page minus 1

		/*
			Now we apply our rules and draw the pagination object.
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$pagination = "";
		$margin = '';
		$padding = '';
		if($lastpage > 1)
		{
			$pagination .= "<div class=\"pagination\"";
			if($margin || $padding)
			{
				$pagination .= " style=\"";
				if($margin)
					$pagination .= "margin: $margin;";
				if($padding)
					$pagination .= "padding: $padding;";
				$pagination .= "\"";
			}
			$pagination .= ">";

			//previous button
			if ($page > 1)
				$pagination .= "<a href=\"$targetpage$pagestring$prev\">&laquo;</a>";
			else
				$pagination .= "<span class=\"disabled\">&laquo;</span>";

			//pages
			if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
			{
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination .= "<span class=\"current\">$counter</span>";
					else
						$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
				}
			}
			elseif($lastpage >= 7 + ($adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($adjacents * 3))
				{
					for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination .= "<span class=\"current\">$counter</span>";
						else
							$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
					}
					$pagination .= "<span class=\"elipses\">...</span>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>";
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
				{
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
					$pagination .= "<span class=\"elipses\">...</span>";
					for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination .= "<span class=\"current\">$counter</span>";
						else
							$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
					}
					$pagination .= "...";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lpm1 . "\">$lpm1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . $lastpage . "\">$lastpage</a>";
				}
				//close to end; only hide early pages
				else
				{
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "1\">1</a>";
					$pagination .= "<a href=\"" . $targetpage . $pagestring . "2\">2</a>";
					$pagination .= "<span class=\"elipses\">...</span>";
					for ($counter = $lastpage - (1 + ($adjacents * 3)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination .= "<span class=\"current\">$counter</span>";
						else
							$pagination .= "<a href=\"" . $targetpage . $pagestring . $counter . "\">$counter</a>";
					}
				}
			}

			//next button
			if ($page < $counter - 1)
				$pagination .= "<a class='pagination__next' href=\"" . $targetpage . $pagestring . $next . $add_url . "\">&raquo;</a>";
			else
				$pagination .= "<span class=\"disabled\">&raquo;</span>";
			$pagination .= "</div>\n";
		}

		return $pagination;
	}
?>