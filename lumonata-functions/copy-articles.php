<?php

/*
| -------------------------------------------------------------------------------------
| Page Function
| -------------------------------------------------------------------------------------
*/
function get_admin_page()
{
    if( isset( $_POST['loadMore'] ) && ( $_POST['loadMore'] == 'pages' ) )
    {
        echo get_pages_list_ajax( 'pages' );

        exit;
    }
    
    $post_id = 0;

    if( isset( $_POST['select'] ) )
    {
        $status = ( is_save_draft() ? 'draft' : ( is_unpublish() ? 'unpublish' : 'publish' ) );
        
        foreach( $_POST['select'] as $key => $val )
        {
            update_articles_status( $val, $status );
        }
    }

    if( is_add_new() )
    {
        return add_new_page( $post_id );
    }
    elseif( is_edit() )
    {
        if( is_contributor() || is_author() )
        {
            if( is_num_articles( 'id=' . $_GET['id'] ) > 0 )
            {
                return edit_page( $_GET['id'] );
            }
            else
            {
                return not_found_page(); "<div class=\"alert_red_form\">You don't have an authorization to access this page</div>";
            }
        }
        else
        {
            return edit_page( $_GET['id'] );
        }
    }
    elseif( is_delete_all() )
    {
        return batch_delete_page();
    }
    elseif( is_confirm_delete() )
    {
        foreach( $_POST['id'] as $key => $val )
        {
            delete_article( $val, 'pages' );
        }
    }

    if( is_num_articles() == 0 )
    {
        header( 'location:' . get_state_url( 'pages' ) . '&prc=add_new' );
    }
    elseif( is_num_articles() > 0 )
    {
        return get_pages_list();
    }
}

/*
| -------------------------------------------------------------------------------------
| Not Found Page
| -------------------------------------------------------------------------------------
*/
function not_found_page()
{
    set_template( TEMPLATE_PATH . '/template/not-found.html', 'not-found' );

    add_block( 'not-found-block', 'nf-block', 'not-found' );  

    add_actions( 'section_title', 'Page Not Found' );

    parse_template( 'not-found-block', 'nf-block', false );

    return return_template( 'not-found' );   
}

/*
| -------------------------------------------------------------------------------------
| Page Filter Options
| -------------------------------------------------------------------------------------
*/
function get_filter_option( $show_data = '' )
{
    $filter = array( 'all' => 'All', 'publish' => 'Publish', 'unpublish' => 'Unpublish', 'draft' => 'Draft' );
    $option = '';

    foreach( $filter as $key => $val )
    {
        if( !empty( $show_data ) )
        {
            if( $show_data == $key )
            {
                $option .= '<input type="radio" name="data_to_show" value="' . $key . '" checked="checked" /><label>' . $val . '</label>';
            }
            else
            {
                $option .= '<input type="radio" name="data_to_show" value="' . $key . '" /><label>' . $val . '</label>';
            }
        }
        elseif( $key == 'all' )
        {
            $option .= '<input type="radio" name="data_to_show" value="' . $key . '" checked="checked" /><label>' . $val . '</label>';
        }
        else
        {
            $option .= '<input type="radio" name="data_to_show" value="' . $key . '" /><label>' . $val . '</label>';
        }
    }

    return $option;
}

/*
| -------------------------------------------------------------------------------------
| Page Table List
| -------------------------------------------------------------------------------------
*/
function get_pages_list( $type = 'pages' )
{
    global $db;

    $showed  = isset( $_POST['data_to_show'] ) ? $_POST['data_to_show'] : ( isset( $_GET['data_to_show'] ) ? $_GET['data_to_show'] : 'all' );
    $orderby = is_orderby() ? 'ORDER BY ' . $_GET['orderby'] . ' ' . $_GET['order'] : 'ORDER BY lorder';
    $odrplus = is_orderby() ? '&orderby=' . $_GET['orderby'] . '&order=' . $_GET['order'] : '';
    $page    = isset( $_GET['page'] ) ? $_GET['page'] : 1;
    $filter  = get_filter_option( $showed );
    $viewed  = list_viewed();
    $limit   = $viewed * ( $page - 1 );
    $start   = $limit + 1;    
    $list    = '';

    if( is_search() )
    {
        if( $_POST['s'] != 'Search pages' && !empty( $_POST['s'] ) )
        {
            if( $showed != 'all' )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
                $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
                
                $url  = get_state_url( 'pages&data_to_show=' . $showed . $odrplus . '&page=' );
                $purl = get_state_url( 'pages&data_to_show=' . $showed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
                $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
                
                $url  = get_state_url( 'pages' . $odrplus . '&page=' );
                $purl = get_state_url( 'pages' );
            }
        }
        else
        {
            if( $showed != 'all' )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s';
                $q = $db->prepare_query( $s, $showed, $type );
                
                $url  = get_state_url( 'pages&data_to_show=' . $showed . $odrplus . '&page=' );
                $purl = get_state_url( 'pages&data_to_show=' . $showed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s';
                $q = $db->prepare_query( $s, $type );
                
                $url  = get_state_url( 'pages' . $odrplus . '&page=' );
                $purl = get_state_url( 'pages' );
            }
        }
        
        $n = count_rows( $q );
    }
    else
    {
        if( $showed != 'all' )
        {
            $q = $db->prepare_query( 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s', $showed, $type );
            
            $url  = get_state_url( 'pages&data_to_show=' . $showed . $odrplus . '&page=' );
            $purl = get_state_url( 'pages&data_to_show=' . $showed );
        }
        else
        {
            $q = $db->prepare_query( 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s', $type );
            
            $url  = get_state_url( 'pages' . $odrplus . '&page=' );
            $purl = get_state_url( 'pages' );
        }
        
        $n = count_rows( $q );
    }
    
    if( is_search() )
    {
        if( $_POST['s'] != 'Search pages' && !empty( $_POST['s'] ) )
        {
            if( $showed != 'all' )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
            }
        }
        else
        {
            if( ( isset( $_POST['data_to_show'] ) && $_POST['data_to_show'] != "all" ) || ( isset( $_GET['data_to_show'] ) && $_GET['data_to_show'] != "all" ) )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $showed, $type, $limit, $viewed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $type, $limit, $viewed );
            }
        }
    }
    else
    {
        if( $showed != 'all' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
            $q = $db->prepare_query( $s, $showed, $type, $limit, $viewed );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
            $q = $db->prepare_query( $s, $type, $limit, $viewed );
        }
    }

    $r  = $db->do_query( $q );
    
    if( $_COOKIE['user_type'] == 'contributor' )
    {
        $button = '
        <li>' . button( 'button=add_new', get_state_url( 'pages' ) . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
    }
    else
    {
        $button = '
        <li>' . button( 'button=add_new', get_state_url( 'pages' ) . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
        <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
        <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
    }

    set_template( TEMPLATE_PATH . '/template/page/list.html', 'page-template' );

    add_block( 'list-block', 'l-block', 'page-template' );

    add_variable( 'filter', $filter );
    add_variable( 'button', $button );
    add_variable( 'start_order', $start );
    add_variable( 'oderby_date', $purl . '&orderby=lpost_date&order=' . set_orderby() );
    add_variable( 'oderby_title', $purl . '&orderby=larticle_title&order=' . set_orderby() );
    add_variable( 'state_url', get_state_url( 'pages' ) );
    add_variable( 'sort_date', set_sortable( 'lpost_date' ) );
    add_variable( 'sort_title', set_sortable( 'larticle_title' ) );
    add_variable( 'sort_date_cls', set_orderby() . ' ' . set_sortable( 'lpost_date' ) );
    add_variable( 'sort_title_cls', set_orderby() . ' ' . set_sortable( 'larticle_title' ) );
    add_variable( 'search_box', search_box( 'articles.php', 'list_item', 'state=pages&prc=search&', 'right', 'alert_green_form', 'Search pages' ) );
    add_variable( 'page_list', pages_list( $r, $start ) );
    add_variable( 'load_more', $n > $viewed ? get_load_more_link( $page ) : '' );
    add_variable( 'load_more_js', $n > $viewed ? get_load_more_js( 'pages' ) : '' );

    add_actions( 'section_title', 'Pages' );
    add_actions( 'header_elements', 'get_javascript', 'articles_list' );
    add_actions( 'footer_elements', popup_delete() );

    parse_template( 'list-block', 'l-block', false );

    return return_template( 'page-template' );
}

/*
| -------------------------------------------------------------------------------------
| Page Table List Ajax
| -------------------------------------------------------------------------------------
*/
function get_pages_list_ajax( $type = 'pages' )
{
    global $db;

    $showed  = isset( $_POST['data_to_show'] ) ? $_POST['data_to_show'] : ( isset( $_GET['data_to_show'] ) ? $_GET['data_to_show'] : 'all' );
    $orderby = is_orderby() ? 'ORDER BY ' . $_GET['orderby'] . ' ' . $_GET['order'] : 'ORDER BY lorder';
    $odrplus = is_orderby() ? '&orderby=' . $_GET['orderby'] . '&order=' . $_GET['order'] : '';
    $page    = isset( $_POST['page'] ) ? $_POST['page'] : 1;
    $filter  = get_filter_option( $showed );
    $viewed  = list_viewed();
    $limit   = $viewed * ( $page - 1 );
    $start   = $limit + 1;    
    $list    = '';

    if( is_search() )
    {
        if( $_POST['s'] != 'Search pages' && !empty( $_POST['s'] ) )
        {
            if( $showed != 'all' )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
                $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
                
                $url  = get_state_url( 'pages&data_to_show=' . $showed . $odrplus . '&page=' );
                $purl = get_state_url( 'pages&data_to_show=' . $showed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
                $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
                
                $url  = get_state_url( 'pages' . $odrplus . '&page=' );
                $purl = get_state_url( 'pages' );
            }
        }
        else
        {
            if( $showed != 'all' )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s';
                $q = $db->prepare_query( $s, $showed, $type );
                
                $url  = get_state_url( 'pages&data_to_show=' . $showed . $odrplus . '&page=' );
                $purl = get_state_url( 'pages&data_to_show=' . $showed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s';
                $q = $db->prepare_query( $s, $type );
                
                $url  = get_state_url( 'pages' . $odrplus . '&page=' );
                $purl = get_state_url( 'pages' );
            }
        }
        
        $n = count_rows( $q );
    }
    else
    {
        if( $showed != 'all' )
        {
            $q = $db->prepare_query( 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s', $showed, $type );
            
            $url  = get_state_url( 'pages&data_to_show=' . $showed . $odrplus . '&page=' );
            $purl = get_state_url( 'pages&data_to_show=' . $showed );
        }
        else
        {
            $q = $db->prepare_query( 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s', $type );
            
            $url  = get_state_url( 'pages' . $odrplus . '&page=' );
            $purl = get_state_url( 'pages' );
        }
        
        $n = count_rows( $q );
    }
    
    if( is_search() )
    {
        if( $_POST['s'] != 'Search pages' && !empty( $_POST['s'] ) )
        {
            if( $showed != 'all' )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
            }
        }
        else
        {
            if( ( isset( $_POST['data_to_show'] ) && $_POST['data_to_show'] != "all" ) || ( isset( $_GET['data_to_show'] ) && $_GET['data_to_show'] != "all" ) )
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $showed, $type, $limit, $viewed );
            }
            else
            {
                $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
                $q = $db->prepare_query( $s, $type, $limit, $viewed );
            }
        }
    }
    else
    {
        if( $showed != 'all' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
            $q = $db->prepare_query( $s, $showed, $type, $limit, $viewed );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s ' . $orderby . ' LIMIT %d, %d';
            $q = $db->prepare_query( $s, $type, $limit, $viewed );
        }
    }

    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );    
    
    if( $n > 0 )
    {
        $list  = pages_list( $r, $start );
        $list .= get_load_more_link( $page );
    }

    return $list;
}

/*
| -------------------------------------------------------------------------------------
| Page Table List Item
| -------------------------------------------------------------------------------------
*/
function pages_list( $result, $i = 1 )
{
    global $db;
    
    if( $db->num_rows( $result ) == 0 )
    {
        if( isset( $_POST['s'] ) && !empty( $_POST['s'] ) )
        {
            return '
            <div class="alert_yellow_form">
                No result found for <em>' . $_POST['s'] . '</em>. 
                Check your spellling or try another terms
            </div>';
        }
        else
        {
            return '
            <div class="alert_yellow_form">
                No page found
            </div>';
        }
    }

    set_template( TEMPLATE_PATH . '/template/page/loop.html', 'page-loop' );
    add_block( 'loop-block', 'lp-block', 'page-loop' );
    
    while( $d = $db->fetch_array( $result ) )
    {  
        $users  = fetch_user( $d['lpost_by'] );

        add_variable( 'i', $i );
        add_variable( 'id', $d['larticle_id'] );
        add_variable( 'title', $d['larticle_title'] );
        add_variable( 'subtitle', $d['larticle_subtitle'] );
        add_variable( 'overview', $d['larticle_overview'] );
        add_variable( 'page_url', get_state_url( 'pages' ) );
        add_variable( 'profile_name', $users['ldisplay_name'] );
        add_variable( 'profile_link', user_url( $users['luser_id'] ) );
        add_variable( 'avatar_img', get_avatar( $users['luser_id'], 3 ) );
        add_variable( 'comment', number_format( $d['lcomment_count'] ) );
        add_variable( 'date', date( get_date_format(), strtotime( $d['lpost_date'] ) ) );
        add_variable( 'date_updated', date( get_date_format(), strtotime( $d['ldlu'] ) ) );
        add_variable( 'status', $d['larticle_status'] != 'publish' ? ' - <strong style="color:red;">' . ucfirst( $d['larticle_status'] ) . '</strong>' : '' );

        parse_template( 'loop-block', 'lp-block', true );
        
        $i++;
    }

    return return_template( 'page-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Page Form
| -------------------------------------------------------------------------------------
*/
function add_new_page( $post_id = 0 )
{
    if( is_save_draft() || is_publish() )
    {
        //print_r($_POST); return;
        $id         = $_POST['post_id'][0];
        $title      = $_POST['title'][0];
        $subtitle   = '';
        $overview   = '';
        $content    = $_POST['post'][0];
        $shareop    = $_POST['share_option'][0];
        $status     = ( is_save_draft() ? 'draft' : 'publish' );
        $post_date  = date('Y-m-d H:i:s', strtotime( $_POST['post_date'][0] ) );

        $start_date = '';
        $end_date   = '';
        $start_time = '';
        $end_time   = '';

        if ( empty($_POST['start_date'][0]) ) {
            $start_date = '';
        }
        else{
            $start_date = date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) );
        }

        if ( empty($_POST['end_date'][0]) ) {
            $end_date = '';
        }
        else{
            $end_date = date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) );
        }

        if ( empty($_POST['publish_date'][0]) ) {
            $publish_date = '';
        }
        else{
            $publish_date = date('Y-m-d H:i:s', strtotime( $_POST['publish_date'][0] ) );
        }

        /*if ( empty($_POST['start_time'][0]) ) {
            $start_time = '';
        }
        else{
            $start_time = date('Y-m-d H:i:s', strtotime( $_POST['start_time'][0] ) );
        }

        if ( empty($_POST['end_time'][0]) ) {
            $end_time = '';
        }
        else{
            $end_time = date('Y-m-d H:i:s', strtotime( $_POST['end_time'][0] ) );
        }*/

        $start_time = $_POST['start_time'][0];
        $end_time   = $_POST['end_time'][0];

        // $start_date = date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) );
        // $end_date   = date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) );
        // $start_time = date('Y-m-d H:i:s', strtotime( $_POST['start_time'][0] ) );
        // $end_time   = date('Y-m-d H:i:s', strtotime( $_POST['end_time'][0] ) );
        $link       = isset( $_POST['link'][0] ) ? $_POST['link'][0] : '';
        $location   = isset( $_POST['location'][0] ) ? $_POST['location'][0] : '';
        $map        = isset( $_POST['map'][0] ) ? $_POST['map'][0] : '';
        $comments   = ( isset( $_POST['allow_comments'][0] ) ? 'allowed' : 'not_allowed' );
        $sefbox     = ( isset( $_POST['sef_box'][0] ) ? $_POST['sef_box'][0] : generateSefUrl( $title ) );

        save_article( $title, $subtitle, $overview, $content, $status, 'pages', $post_date, $publish_date, $start_date, $end_date, $start_time, $end_time, $link, $location, $map, $comments, $sefbox, $shareop );
        
        $post_id = mysql_insert_id();

        attachment_sync( $id, $post_id );
        additional_field_sync( $id, $post_id );

        if( isset( $_POST['additional_fields'] ) )
        {
            foreach( $_POST['additional_fields'] as $key => $val )
            {
                if( $key == 'custom_field' )
                {
                    $field = array();

                    foreach( $val as $subkey => $subval )
                    {
                        foreach( $subval as $name => $obj )
                        {
                            foreach( $obj as $label => $data )
                            {
                                foreach( $data as $types => $d )
                                {
                                    $field[$name] = array( 'type' => $types, 'label' => $label, 'value' => $d );
                                }
                            }
                        }
                    }
                    
                    edit_additional_field( $post_id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), 'pages' );
                }
                else
                {
                    foreach( $val as $subkey => $subval )
                    {
                        $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                        edit_additional_field( $post_id, $key, $subval, 'pages' );
                    }
                }
            }
        }

        save_additional_product($post_id);
    }
    
    $args    = array( $index = 0, $post_id );
    $thepost = new StdClass;
    $thepost->post_id    = $post_id;
    $thepost->post_index = $index;

    if( !is_contributor() )
    {
        $button = '
        <li>' . button( 'button=publish' ) . '</li>    
        <li>' . button( 'button=save_draft' ) . '</li>                    
        <li>' . button( 'button=cancel', get_state_url( 'pages' ) ) . '</li>
        <li>' . button( 'button=add_new', get_state_url( 'pages' ) . '&prc=add_new' ) . '</li>';
    }
    else
    {
        $button = '
        <li>' . button( 'button=add_new', get_state_url( 'pages' ) . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=save_draft&label=Save' ) . '</li>
        <li>' . button( 'button=cancel', get_state_url( 'pages' ) ) . '</li>';
    }

    set_template( TEMPLATE_PATH . '/template/page/form.html', 'page-form' );
    add_block( 'form-block', 'f-block', 'page-form' );
    
    add_variable( 'textarea', textarea( 'post[0]', 0 ) );
    add_variable( 'title', '' );
    add_variable( 'subtitle', '' );
    add_variable( 'overview', '' );

    add_variable( 'i', 0 );
    add_variable( 'button', $button );
    add_variable( 'post_date', date('d F Y') );
    add_variable( 'sef_scheme', get_sef_scheme() );
    add_variable( 'additional_data', attemp_actions( 'page_additional_data' ) );
    add_variable( 'share_to', additional_data( 'Share To', 'article_share_option', true, $args ) );
    add_variable( 'comments_settings', additional_data( 'Comments', 'discussion_settings', false, $args ) );
    
    add_actions( 'section_title', 'Pages - Add New' );
    
    parse_template( 'form-block', 'f-block', false );

    return return_template( 'page-form' );
}

/*
| -------------------------------------------------------------------------------------
| Edit Page Form
| -------------------------------------------------------------------------------------
*/
function edit_page( $post_id = 0 )
{
    global $thepost;

    // print_r($_POST); return;

    if( is_save_draft() || is_publish() )
    {
        // print_r($_POST); return;
        $id             = $_POST['post_id'][0];
        $title          = $_POST['title'][0];
        $subtitle       = '';
        $overview       = '';
        $content        = $_POST['post'][0];
        $shareop        = $_POST['share_option'][0];
        $status         = is_save_draft() ? 'draft' : 'publish';
        $post_date      = date('Y-m-d H:i:s', strtotime( $_POST['post_date'][0] ) );
        /*$start_date = date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) );
        $end_date   = date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) );*/
        $publish_date   = isset( $_POST['publish_date'][0] ) ? date('Y-m-d H:i:s', strtotime( $_POST['publish_date'][0] ) ) : '';
        $start_date     = isset( $_POST['start_date'][0] ) ? date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) ) : '';
        $end_date       = isset( $_POST['end_date'][0] ) ? date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) ) : '';
        $comments       = isset( $_POST['allow_comments'][0] ) ? 'allowed' : 'not_allowed';
        // $start_time = isset( $_POST['start_time'][0] ) ? date('H:i:s', strtotime( $_POST['start_time'][0] ) ) : '';
        // $end_time   = isset( $_POST['end_time'][0] ) ? date('H:i:s', strtotime( $_POST['end_time'][0] ) ) : '';
        $link           = isset( $_POST['link'][0] ) ? $_POST['link'][0] : '';
        $location       = isset( $_POST['location'][0] ) ? $_POST['location'][0] : '';
        $map            = isset( $_POST['map'][0] ) ? $_POST['map'][0] : '';

        /*if ( empty($_POST['start_time'][0]) ) {
            $start_time = '';
        }
        else{
            $start_time = date('Y-m-d H:i:s', strtotime( $_POST['start_time'][0] ) );
        }

        if ( empty($_POST['end_time'][0]) ) {
            $end_time = '';
        }
        else{
            $end_time = date('Y-m-d H:i:s', strtotime( $_POST['end_time'][0] ) );
        }*/
        $start_time = $_POST['start_time'][0];
        $end_time   = $_POST['end_time'][0];
        
        update_article( $id, $title, $subtitle, $overview, $content, $status, 'pages', $post_date, $publish_date, $start_date, $end_date, $start_time, $end_time, $link, $location, $map, $comments, $shareop );
        
        if( isset( $_POST['additional_fields'] ) )
        {
            foreach( $_POST['additional_fields'] as $key => $val )
            {
                if( $key == 'custom_field' )
                {
                    $field = array();

                    foreach( $val as $subkey => $subval )
                    {
                        foreach( $subval as $name => $obj )
                        {
                            foreach( $obj as $label => $data )
                            {
                                foreach( $data as $types => $d )
                                {
                                    $field[$name] = array( 'type' => $types, 'label' => $label, 'value' => $d );
                                }
                            }
                        }
                    }
                    
                    edit_additional_field( $id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), 'pages' );
                }
                else
                {
                    foreach( $val as $subkey => $subval )
                    {
                        $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                        edit_additional_field( $id, $key, $subval, 'pages' );
                    }
                }
            }
        }

        save_additional_product($post_id);
    }    

    if( !is_contributor() )
    {
        $button = '
        <li>' . button( 'button=publish' ) . '</li>
        <li>' . button( 'button=save_draft' ) . '</li>           
        <li>' . button( 'button=cancel', get_state_url( 'pages' ) ) . '</li>
        <li>' . button( 'button=add_new', get_state_url( 'pages' ) . '&prc=add_new' ) . '</li>';
    }
    else
    {
        $button = '
        <li>' . button( 'button=add_new', get_state_url( 'pages' ) . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=save_draft&label=Save' ) . '</li>
        <li>' . button( 'button=cancel', get_state_url( 'pages' ) ) . '</li>';
    }
    
    $index = 0;    
    $args  = array( $index, $post_id );
    $thepost->post_id    = $post_id;
    $thepost->post_index = $index;

    $d = fetch_artciles( 'id=' . $post_id );

    set_template( TEMPLATE_PATH . '/template/page/form.html', 'page-form' );
    add_block( 'form-block', 'f-block', 'page-form' );
    
    if( is_save_draft() || is_publish() )
    {
        add_variable( 'textarea', textarea( 'post[' . $index . ']', $index, rem_slashes( $_POST['post'][$index] ), $post_id ) );
        add_variable( 'title', rem_slashes( $_POST['title'][$index] ) );
    }
    else
    {
        add_variable( 'textarea', textarea( 'post[' . $index . ']', $index, rem_slashes( $d['larticle_content'] ), $post_id ) );
        add_variable( 'title', rem_slashes( $d['larticle_title'] ) );
    }

    add_variable( 'i', $index );
    add_variable( 'button', $button );
    add_variable( 'post_date', date('d F Y', strtotime( $d['lpost_date'] ) ) );
    add_variable( 'sef_scheme', get_sef_scheme( 'pages', $index, $post_id, $d, true ) );
    add_variable( 'additional_data', attemp_actions( 'page_additional_data' ) );
    add_variable( 'share_to', additional_data( 'Share To', 'article_share_option', true, $args ) );
    add_variable( 'comments_settings', additional_data( 'Comments', 'discussion_settings', false, $args ) );
    
    add_actions( 'section_title', 'Pages - Edit' ); 
    
    parse_template( 'form-block', 'f-block', false );

    return return_template( 'page-form' );
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Page
| -------------------------------------------------------------------------------------
*/
function batch_delete_page()
{
    set_template( TEMPLATE_PATH . '/template/page/batch-delete.html', 'page-delete' );
    add_block( 'loop-block', 'dl-block', 'page-delete' );
    add_block( 'delete-block', 'd-block', 'page-delete' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = fetch_artciles( 'id=' . $val );

        add_variable('id', $d['larticle_id'] );
        add_variable('title',  $d['larticle_title'] );

        parse_template('loop-block', 'dl-block', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this page?' : 'these pages?' ) );
    add_variable('action', get_state_url( 'pages' ) );

    parse_template( 'delete-block', 'd-block', false );
    
    add_actions( 'section_title', 'Delete Page' );

    return return_template( 'page-delete' );
}

/*
| -------------------------------------------------------------------------------------
| Post Function
| -------------------------------------------------------------------------------------
*/
function get_admin_article( $type = 'blogs', $thetitle = 'Blog|Blogs', $tabs = array(), $category_tabs = true, $tags_tabs = true, $action_prm = array() )
{
    // print_r($_POST); //return;
    if( isSet( $_POST['loadMore'] ) && ( $_POST['loadMore'] != 'pages' ) )
    {
        get_article_list_ajax( $type, $thetitle, '' );
        exit;
    }

    if( !empty( $action_prm ) )
    {
        foreach( $action_prm as $a )
        {
            add_actions( $a['var'], $a['function'], $a['param'] );
        }
    }
    
    $post_id              = 0;
    $articletabtitle      = explode( "|", $thetitle );
    $default_tab['blogs'] = $articletabtitle[1];
    
    if( $category_tabs )
    {
        $default_tab['categories'] = 'Categories';
    }
    
    if( $tags_tabs )
    {
        $default_tab['tags'] = 'Tags';
    }
    
    $tabs = array_merge( $default_tab, $tabs );
    
    if( is_contributor() || is_author() )
    {
        foreach( $tabs as $key => $val )
        {
            if( is_grant_app( $key ) )
            {
                $thetabs[$key] = $val;
            }
        }
        
        $tabs = $thetabs;
    }
    else
    {
        $tabs = $tabs;
    }

    $tabb     = '';
    $tab_keys = array_keys( $tabs );
    $the_tab  = empty( $_GET['tab'] ) ? $tab_keys[0] : $_GET['tab'];    
    $post_tab = set_tabs( $tabs, $the_tab );
    
    add_variable( 'tab', $post_tab );

    if( $the_tab == 'blogs' )
    {
        if( is_save_draft() || is_publish() )
        {
            if( is_save_draft() )
            {
                $status = 'draft';

                run_actions( 'article_draft' );
            }
            elseif( is_publish() )
            {
                //print_r($_POST); return;
                $status = 'publish';

                run_actions( 'article_publish' );
                
                if( isset( $_POST['select'] ) )
                {
                    foreach( $_POST['select'] as $key => $val )
                    {
                        update_articles_status( $val, 'publish' );
                    }
                }
            }
            
            if( is_add_new() )
            {
                if( is_save_draft() )
                {
                    attemp_actions( 'article_addnew_draft' );
                }
                elseif( is_publish() )
                {
                    attemp_actions( 'article_addnew_publish' );
                }
                
                //print_r($_POST); return;
                $title          = $_POST['title'][0];
                $subtitle       = $_POST['subtitle'][0]; 
                $overview       = '';
                $post_date      = date('Y-m-d H:i:s', strtotime( $_POST['post_date'][0] ) );
                $publish_date   = date('Y-m-d H:i:s', strtotime( $_POST['publish_date'][0] ) );
                $start_date     = date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) );
                $end_date       = date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) );
                // $start_time  = date('H:i:s', strtotime( $_POST['start_time'][0] ) );
                // $end_time    = date('H:i:s', strtotime( $_POST['end_time'][0] ) );
                $link           = $_POST['link'][0];
                $location       = $_POST['location'][0];
                $map            = isset( $_POST['map'][0] ) ? $_POST['map'][0] : '';
                $sef_box        = isset( $_POST['sef_box'][0] ) ? $_POST['sef_box'][0] : '';
                $comments       = isset( $_POST['allow_comments'][0] ) ? 'allowed' : 'not_allowed';

                /*if ( empty($_POST['start_time'][0]) ) {
                    $start_time = '';
                }
                else{
                    $start_time = date('Y-m-d H:i:s', strtotime( $_POST['start_time'][0] ) );
                }

                if ( empty($_POST['end_time'][0]) ) {
                    $end_time = '';
                }
                else{
                    $end_time = date('Y-m-d H:i:s', strtotime( $_POST['end_time'][0] ) );
                }*/

                // echo $start_time;

                $start_time = $_POST['start_time'][0];
                $end_time   = $_POST['end_time'][0];

                
                if( !is_saved() )
                {
                    save_article( $title, $subtitle, $overview, $_POST['post'][0], $status, $type, $post_date, $publish_date, $start_date, $end_date, $start_time, $end_time, $link, $location, $map, $comments, $sef_box, $_POST['share_option'][0] );

                    $post_id = mysql_insert_id();

                    attachment_sync( $_POST['post_id'][0], $post_id );

                    additional_field_sync( $_POST['post_id'][0], $post_id );

                    if( isset( $_POST['additional_fields'] ) )
                    {
                        foreach( $_POST['additional_fields'] as $key => $val )
                        {
                            if( $key == 'custom_field' )
                            {
                                $field = array();

                                foreach( $val as $subkey => $subval )
                                {
                                    foreach( $subval as $name => $obj )
                                    {
                                        foreach( $obj as $label => $data )
                                        {
                                            foreach( $data as $types => $d )
                                            {
                                                $field[$name] = array( 'type' => $types, 'label' => $label, 'value' => $d );
                                            }
                                        }
                                    }
                                }

                                edit_additional_field( $post_id, $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), $type );
                            }
                            else
                            {
                                foreach( $val as $subkey => $subval )
                                {
                                    $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                                    edit_additional_field( $post_id, $key, $subval, $type );
                                }
                            }
                        }
                    }

                    if( isset( $_POST['category'][0] ) )
                    {
                        foreach( $_POST['category'][0] as $val )
                        {
                            insert_rules_relationship( $post_id, $val );
                        }
                    }
                    else
                    {
                        insert_rules_relationship( $post_id, 1 );
                    }

                    if( isset( $_POST['tags'][0] ) )
                    {
                        foreach( $_POST['tags'][0] as $val )
                        {
                            $rule_id = insert_rules( 0, $val, '', 'tags', $type, false );

                            insert_rules_relationship( $post_id, $rule_id );
                        }
                    }
                }
                else
                {
                    update_article( $_POST['post_id'][0], $title, $subtitle, $overview, $_POST['post'][0], $status, $type, date('Y-m-d H:i:s', strtotime( $_POST['post_date'][0] ) ), date('Y-m-d H:i:s', strtotime( $_POST['publish_date'][0] ) ), date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) ), date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) ), $start_time, $end_time, $link, $location, $map, $comments, $_POST['share_option'][0] );

                    if( isset( $_POST['additional_fields'] ) )
                    {
                        foreach( $_POST['additional_fields'] as $key => $val )
                        {
                            if( $key == 'custom_field' )
                            {
                                $field = array();

                                foreach( $val as $subkey => $subval )
                                {
                                    foreach( $subval as $name => $obj )
                                    {
                                        foreach( $obj as $label => $data )
                                        {
                                            foreach( $data as $types => $d )
                                            {
                                                $field[$name] = array( 'type' => $types, 'label' => $label, 'value' => $d );
                                            }
                                        }
                                    }
                                }

                                edit_additional_field( $_POST['post_id'][0], $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), $type );
                            }
                            else
                            {
                                foreach( $val as $subkey => $subval )
                                {
                                    $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                                    edit_additional_field( $_POST['post_id'][0], $key, $subval, $type );
                                }
                            }
                        }
                    }

                    if( isset( $_POST['category'][0] ) )
                    {
                        delete_rules_relationship( 'app_id=' . $_POST['post_id'][0], 'categories' );

                        foreach( $_POST['category'][0] as $val )
                        {
                            insert_rules_relationship( $_POST['post_id'][0], $val );
                        }
                    }
                    else
                    {
                        delete_rules_relationship( 'app_id=' . $_POST['post_id'][0], 'categories' );

                        insert_rules_relationship( $post_id, 1 );
                    }

                    delete_rules_relationship( 'app_id=' . $_POST['post_id'][0], 'tags' );
                    
                    if( isset( $_POST['tags'][0] ) )
                    {
                        foreach( $_POST['tags'][0] as $val )
                        {
                            $rule_id = insert_rules( 0, $val, '', 'tags', $type );

                            insert_rules_relationship( $_POST['post_id'][0], $rule_id );
                        }
                    }
                }
            }
            elseif( is_edit() )
            {
                //print_r($_POST);return;
                if( is_save_draft() )
                {
                    run_actions( 'article_edit_draft' );
                }
                elseif( is_publish() )
                {
                    run_actions( 'article_edit_publish' );
                }
                
                $comments   = isset( $_POST['allow_comments'][0] ) ? 'allowed' : 'not_allowed';
                $title      = $_POST['title'][0];
                $subtitle   = $_POST['subtitle'][0]; //echo $subtitle; return;
                $overview   = '';
                // $start_time = date('H:i:s', strtotime( $_POST['start_time'][0] ) );
                // $end_time   = date('H:i:s', strtotime( $_POST['end_time'][0] ) );
                $link       = $_POST['link'][0];
                $location   = $_POST['location'][0];
                $map        = $_POST['map'][0];

                /*if ( empty($_POST['start_time'][0]) ) {
                    $start_time = '';
                }
                else{
                    $start_time = date('Y-m-d H:i:s', strtotime( $_POST['start_time'][0] ) );
                }

                if ( empty($_POST['end_time'][0]) ) {
                    $end_time = '';
                }
                else{
                    $end_time = date('Y-m-d H:i:s', strtotime( $_POST['end_time'][0] ) );
                }*/

                $start_time = $_POST['start_time'][0];
                $end_time   = $_POST['end_time'][0];

                update_article( $_POST['post_id'][0], $title, $subtitle, $overview, $_POST['post'][0], $status, $type, date('Y-m-d H:i:s', strtotime( $_POST['post_date'][0] ) ), date('Y-m-d H:i:s', strtotime( $_POST['publish_date'][0] ) ), date('Y-m-d H:i:s', strtotime( $_POST['start_date'][0] ) ), date('Y-m-d H:i:s', strtotime( $_POST['end_date'][0] ) ), $start_time, $end_time, $link, $location, $map, $comments, $_POST['share_option'][0] );

                if( isset( $_POST['additional_fields'] ) )
                {
                    foreach( $_POST['additional_fields'] as $key => $val )
                    {
                        if( $key == 'custom_field' )
                        {
                            $field = array();

                            foreach( $val as $subkey => $subval )
                            {
                                foreach( $subval as $name => $obj )
                                {
                                    foreach( $obj as $label => $data )
                                    {
                                        foreach( $data as $types => $d )
                                        {
                                            $field[$name] = array( 'type' => $types, 'label' => $label, 'value' => $d );
                                        }
                                    }
                                }
                            }

                            edit_additional_field( $_POST['post_id'][0], $key, json_encode( $field, JSON_HEX_QUOT | JSON_HEX_TAG ), $type );
                        }
                        else
                        {
                            foreach( $val as $subkey => $subval )
                            {
                                $subval = is_array( $subval ) ? json_encode( $subval ) : $subval;

                                edit_additional_field( $_POST['post_id'][0], $key, $subval, $type );
                            }
                        }
                    }
                }

                if( isset( $_POST['category'][0] ) )
                {
                    delete_rules_relationship( "app_id=" . $_POST['post_id'][0], 'categories' );
                    
                    foreach( $_POST['category'][0] as $key => $val )
                    {
                        insert_rules_relationship( $_POST['post_id'][0], $val );
                    }
                }
                else
                {
                    delete_rules_relationship( "app_id=" . $_POST['post_id'][0], 'categories' );

                    insert_rules_relationship( $_POST['post_id'][0], 1 );
                }

                delete_rules_relationship( "app_id=" . $_POST['post_id'][0], 'tags' );
                
                if( isset( $_POST['tags'][0] ) )
                {
                    foreach( $_POST['tags'][0] as $val )
                    {
                        $rule_id = insert_rules( 0, $val, '', 'tags', $type );

                        insert_rules_relationship( $_POST['post_id'][0], $rule_id );
                    }
                }
            }
        }
        elseif( is_unpublish() )
        {
            if( isset( $_POST['select'] ) )
            {
                foreach( $_POST['select'] as $key => $val )
                {
                    update_articles_status( $val, 'unpublish' );
                }
            }
        }

        if( is_num_articles( 'type=' . $type ) == 0 && !isset( $_GET['prc'] ) )
        {
            if( $_GET['state'] != 'articles' )
            {
                if( isset( $_GET['sub'] ) && $_GET['sub'] != '' )
                {
                    header( "location:" . get_state_url( $_GET['state'] ) . "&sub=" . $_GET['sub'] . "&prc=add_new" );
                }
                else
                {
                    header( "location:" . get_state_url( $_GET['state'] ) . "&prc=add_new" );
                }
            }
            else
            {
                header( "location:" . get_state_url( $type ) . "&prc=add_new" );
            }
        }

        if( is_add_new() )
        {
            return add_new_article( $post_id, $type, $thetitle );
        }
        elseif( is_edit() )
        {
            if( is_contributor() || is_author() )
            {
                if( is_num_articles( "id=" . $_GET['id'] . "&type=" . $type ) > 0 )
                {
                    return edit_article( $_GET['id'], $type, $thetitle );
                }
                else
                {
                    return "<div class=\"alert_red_form\">You don't have an authorization to access this page</div>";
                }
            }
            else
            {
                return edit_article( $_GET['id'], $type, $thetitle );
            }
        }
        elseif( is_edit_all() && isset( $_POST['select'] ) )
        {
            return edit_article( 0, $type, $thetitle );
        }
        elseif( is_delete_all() )
        {
            return batch_delete_article( $type, $thetitle );
        }
        elseif( is_confirm_delete() )
        {
            foreach( $_POST['id'] as $key => $val )
            {
                run_actions( $type . '_additional_delete' );

                delete_article( $val, $type );
            }
        }
        
        if( is_num_articles( 'type=' . $type ) > 0 )
        {
            return get_article_list( $type, $thetitle, $post_tab );
        }
        else
        {
            header( 'location:' . get_state_url( $_GET['state'] ) . '&prc=add_new' );
        }
    }
    elseif( ( $the_tab == 'categories' || $the_tab == 'tags' ) && ( is_administrator() || is_editor() ) )
    {
        return get_admin_rule( $the_tab, $type, $thetitle, $tabs );
    }
    else
    {
        global $actions;
        
        $extendName = $the_tab . '_article_extends';
        
        if( is_administrator() && isset( $actions->action[$extendName] ) && !empty( $actions->action[$extendName]['func_name'][0] ) )
        {
            return call_user_func( $actions->action[$extendName]['func_name'][0], $tabs );
        }
        else
        {
            return '<div class="alert_red_form">You don\'t have an authorization to access this page</div>';
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Post Table List
| -------------------------------------------------------------------------------------
*/
function get_article_list( $type, $title, $articles_tabs )
{
    global $db;

    $showed  = isset( $_POST['data_to_show'] ) ? $_POST['data_to_show'] : ( isset( $_GET['data_to_show'] ) ? $_GET['data_to_show'] : 'all' );
    $orderby = is_orderby() ? 'ORDER BY ' . $_GET['orderby'] . ' ' . $_GET['order'] : 'ORDER BY lorder';
    $odrplus = is_orderby() ? '&orderby=' . $_GET['orderby'] . '&order=' . $_GET['order'] : '';
    $app_url = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;
    $url     = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
    $page    = isset( $_GET['page'] ) ? $_GET['page'] : 1;
    $filter  = get_filter_option( $showed );
    $viewed  = list_viewed();
    $limit   = $viewed * ( $page - 1 );
    $start   = $limit + 1;    
    $list    = '';
    
    if( is_search() )
    {
        if( $showed != 'all' && $showed != '' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
            $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
            $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
        }
        
        $n = count_rows( $q );
    }
    else
    {
        if( $showed != 'all' && $showed != '' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s';
            $q = $db->prepare_query( $s, $showed, $type );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s';
            $q = $db->prepare_query( $s, $type );
        }
        
        $n = count_rows( $q );
    }
    
    if( is_search() )
    {
        if( $showed != 'all' && $showed != '' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) LIMIT %d, %d';
            $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) LIMIT %d, %d';
            $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
        }
        
        $aurl = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
    }
    else
    {        
        $order = is_orderby() ? 'ORDER BY ' . $_GET['orderby'] . ' ' . $_GET['order'] : 'ORDER BY lorder';

        if( $showed != 'all' && $showed != '' )
        {
            $aurl  = is_admin_application() ? get_application_url( $type . '&data_to_show=' . $showed ) : get_state_url( $app_url . '&data_to_show=' . $showed );
            $where = $db->prepare_query( ' WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s', $showed, $type );
        }
        else
        {
            $aurl  = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
            $where = $db->prepare_query( 'WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s', $type );
        }
        
        if( is_category_view() )
        {
            $d = fetch_rule( 'sef=' . $_GET['category_name'] );
            $s = 'SELECT a.* FROM lumonata_articles a, lumonata_rule_relationship b WHERE a.larticle_type=%s  AND a.larticle_status=%s AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0 ' . $order . ' limit %d, %d';
            $q = $db->prepare_query( $s, $type, 'publish', $d['lrule_id'], $limit, $viewed );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles '. $where . ' ' . $order . ' LIMIT %d, %d';
            $q = $db->prepare_query( $s, $limit, $viewed );
        }
    }
    
    $r = $db->do_query( $q );
    
    if( $_COOKIE['user_type'] == 'contributor' )
    {
        $button = '
        <li>' . button( 'button=add_new', ( is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url ) ) . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>';
    }
    else
    {
        $button = '
        <li>' . button( 'button=add_new', ( is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url ) ) . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=delete&type=submit&enable=false' ) . '</li>
        <li>' . button( 'button=publish&type=submit&enable=false' ) . '</li>
        <li>' . button( 'button=unpublish&type=submit&enable=false' ) . '</li>';
    }
    
    $title = explode( '|', $title );
    $title = $n > 1 ? ( count( $title ) == 2 ? $title[1] : $title[0] ) : $title[0];

    set_template( TEMPLATE_PATH . '/template/post/list.html', 'post-template' );

    add_block( 'list-block', 'l-block', 'post-template' );
  
    add_variable( 'type', $type );
    add_variable( 'state_url', $url );
    add_variable( 'button', $button );
    add_variable( 'filter', $filter );
    add_variable( 'post_title', $title );
    add_variable( 'start_order', $start );
    add_variable( 'tabs', $articles_tabs );
    add_variable( 'category_css', $type=='blogs'||$type=='news' ? '' : 'hidden' );
    add_variable( 'style_blog', $type=='blogs'||$type=='news' ? 'width:40%' : '' );
    add_variable( 'comment_css', $type=='blogs' ? '' : 'hidden' );
    add_variable( 'tabs_css', empty( $articles_tabs ) ? 'hidden' : '' );
    add_variable( 'tabs_container_css', empty( $articles_tabs ) ? 'style="border-top:1px solid #bbbbbb;"' : '' );
    add_variable( 'oderby_date', $aurl . '&orderby=lpost_date&order=' . set_orderby() );
    add_variable( 'oderby_title', $aurl . '&orderby=larticle_title&order=' . set_orderby() );
    add_variable( 'sort_date', set_sortable( 'lpost_date' ) );
    add_variable( 'sort_title', set_sortable( 'larticle_title' ) );
    add_variable( 'sort_date_cls', set_orderby() . ' ' . set_sortable( 'lpost_date' ) );
    add_variable( 'sort_title_cls', set_orderby() . ' ' . set_sortable( 'larticle_title' ) );
    add_variable( 'post_list', article_list( $r, $type, $start ) );  
    add_variable( 'load_more', $n > $viewed ? get_load_more_link( $page ) : '' );
    add_variable( 'load_more_js', $n > $viewed ? get_load_more_js( $type ) : '' );
    add_variable( 'search_box', search_box( 'articles.php', 'list_item', 'state=' . $app_url . '&prc=search&', 'right', 'alert_green_form', isset( $_POST['s'] ) ? $_POST['s'] : '', 'Search' ) );

    add_actions( 'section_title', $title );
    add_actions( 'header_elements', 'get_javascript', 'articles_list' );
    add_actions( 'footer_elements', popup_delete() );

    parse_template( 'list-block', 'l-block', false );

    return return_template( 'post-template' );
}

/*
| -------------------------------------------------------------------------------------
| Post Table List Ajax
| -------------------------------------------------------------------------------------
*/
function get_article_list_ajax( $type, $title, $articles_tabs )
{
    global $db;

    $showed  = isset( $_POST['data_to_show'] ) ? $_POST['data_to_show'] : ( isset( $_GET['data_to_show'] ) ? $_GET['data_to_show'] : 'all' );
    $orderby = is_orderby() ? 'ORDER BY ' . $_GET['orderby'] . ' ' . $_GET['order'] : 'ORDER BY lorder';
    $odrplus = is_orderby() ? '&orderby=' . $_GET['orderby'] . '&order=' . $_GET['order'] : '';
    $app_url = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;
    $page    = isset( $_POST['page'] ) ? $_POST['page'] : 1;
    $filter  = get_filter_option( $showed );
    $viewed  = list_viewed();
    $limit   = $viewed * ( $page - 1 );
    $start   = $limit + 1;    
    $list    = '';
    
    if( is_search() )
    {
        if( $showed != 'all' && $showed != '' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
            $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s)';
            $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%' );
        }
        
        $n = count_rows( $q );
    }
    else
    {
        if( $showed != 'all' && $showed != '' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s';
            $q = $db->prepare_query( $s, $showed, $type );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s';
            $q = $db->prepare_query( $s, $type );
        }
        
        $n = count_rows( $q );
    }
    
    if( is_search() )
    {
        if( $showed != 'all' && $showed != '' )
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) LIMIT %d, %d';
            $q = $db->prepare_query( $s, $showed, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s AND (larticle_title LIKE %s OR larticle_content LIKE %s) LIMIT %d, %d';
            $q = $db->prepare_query( $s, $type, '%' . $_POST['s'] . '%', '%' . $_POST['s'] . '%', $limit, $viewed );
        }
        
        $aurl = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
    }
    else
    {        
        $order = is_orderby() ? 'ORDER BY ' . $_GET['orderby'] . ' ' . $_GET['order'] : 'ORDER BY lorder';

        if( $showed != 'all' && $showed != '' )
        {
            $aurl  = is_admin_application() ? get_application_url( $type . '&data_to_show=' . $showed ) : get_state_url( $app_url . '&data_to_show=' . $showed );
            $where = $db->prepare_query( ' WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_status=%s AND larticle_type=%s', $showed, $type );
        }
        else
        {
            $aurl  = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
            $where = $db->prepare_query( 'WHERE ' . ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ? ' lpost_by=' . $_COOKIE['user_id'] . ' AND ' : '' ) . ' larticle_type=%s', $type );
        }
        
        if( is_category_view() )
        {
            $d = fetch_rule( 'sef=' . $_GET['category_name'] );
            $s = 'SELECT a.* FROM lumonata_articles a, lumonata_rule_relationship b WHERE a.larticle_type=%s  AND a.larticle_status=%s AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0 ' . $order . ' limit %d, %d';
            $q = $db->prepare_query( $s, $type, 'publish', $d['lrule_id'], $limit, $viewed );
        }
        else
        {
            $s = 'SELECT * FROM lumonata_articles '. $where . ' ' . $order . ' LIMIT %d, %d';
            $q = $db->prepare_query( $s, $limit, $viewed );
        }
    }
    
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );    
    
    if( $n > 0 )
    {
        $list  = article_list( $r, $type, $start );
        $list .= get_load_more_link( $page );
    }

    echo $list;
}

/*
| -------------------------------------------------------------------------------------
| Post Table List Item
| -------------------------------------------------------------------------------------
*/
function article_list( $r, $type, $i = 1 )
{
    global $db;
    
    $list    = '';
    $state   = isset( $_GET['state'] ) ? $_GET['state'] : ( isset( $_POST['state'] ) ? $_POST['state'] : '' );
    $sub     = isset( $_GET['sub'] ) ? $_GET['sub'] : ( isset( $_POST['sub'] ) ? $_POST['sub'] : '' );
    $app_url = empty( $sub ) ? $type : $state . '&sub=' . $type;
    $url     = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
    
    if( $db->num_rows( $r ) == 0 )
    {
        if( isset( $_POST['s'] ) && !empty( $_POST['s'] ) )
        {
            return '
            <div class="alert_yellow_form">
                No result found for <em>' . $_POST['s'] . '</em>. 
                Check your spellling or try another terms
            </div>';
        }
        else
        {
            return '
            <div class="alert_yellow_form">
                No ' . $type . ' found
            </div>';
        }
    }

    set_template( TEMPLATE_PATH . '/template/post/loop.html', 'post-loop' );
    add_block( 'loop-block', 'lp-block', 'post-loop' );
    
    while( $d = $db->fetch_array( $r ) )
    {  
        $frules = fetch_rule_relationship( 'app_id=' . $d['larticle_id'] );
        $users  = fetch_user( $d['lpost_by'] );
        $cat    = array();
        
        foreach( $frules as $id )
        {
            $rd    = fetch_rule( 'rule_id=' . $id );
            $cat[] = '<a class="field-text" data-field-id="' . $id . '" data-field-name="lname" href="' . $url . '&category_name=' . $rd['lsef'] . '">' . $rd['lname'] . '</a>';
        }

        add_variable( 'i', $i );
        add_variable( 'post_url', $url );
        add_variable( 'id', $d['larticle_id'] );
        add_variable( 'title', $d['larticle_title'] );
        add_variable( 'profile_name', $users['ldisplay_name'] );
        add_variable( 'style_blog', $type=='blogs'||$type=='news' ? 'width:40%' : '' );
        add_variable( 'category', implode( ',', $cat ) );
        add_variable( 'profile_link', user_url( $users['luser_id'] ) );
        add_variable( 'avatar_img', get_avatar( $users['luser_id'], 3 ) );
        add_variable( 'comment', number_format( $d['lcomment_count'] ) );
        add_variable( 'date', date( get_date_format(), strtotime( $d['lpost_date'] ) ) );
        add_variable( 'date_updated', date( get_date_format(), strtotime( $d['ldlu'] ) ) );
        add_variable( 'status', $d['larticle_status'] != 'publish' ? ' - <strong style="color:red;">' . ucfirst( $d['larticle_status'] ) . '</strong>' : '' );

        parse_template( 'loop-block', 'lp-block', true );
        
        $i++;
    }

    return return_template( 'post-loop' );
}

/*
| -------------------------------------------------------------------------------------
| Add New Post Form
| -------------------------------------------------------------------------------------
*/
function add_new_article( $post_id = 0, $type, $title )
{
    global $thepost;
    global $actions;

    $index   = 0;
    $args    = array( $index, $post_id );
    $app_url = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;
    $url     = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );
    $title   = explode( '|', $title );
    $title   = $title[0];
    $option_times       = '';
    $option_times_end   = '';

    $thepost->post_id    = $post_id;
    $thepost->post_index = $index;
    
    if( is_contributor() )
    {
        $button = '
        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=save_draft&label=Save' ) . '</li>
        <li>' . button( 'button=cancel', $url ) . '</li>';
    }
    else
    {
        $button = '
        <li>' . button( 'button=publish' ) . '</li>
        <li>' . button( 'button=save_draft' ) . '</li>            
        <li>' . button( 'button=cancel', $url ) . '</li>
        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>';
    }

    $start = strtotime('00:00');
    $end   = strtotime('23:30');
    for ($i=$start; $i<=$end; $i = $i + 30*60){
        $option_times .= '<option>'.date('H:i',$i).'</option>';
        $option_times_end .= '<option>'.date('H:i',$i).'</option>';
    }

    set_template( TEMPLATE_PATH . '/template/post/form.html', 'post-form' );
    add_block( 'form-block', 'f-block', 'post-form' );

    add_variable( 'i', 0 );
    add_variable( 'title', '' );
    add_variable( 'subtitle', '' );
    add_variable( 'overview', '' );
    add_variable( 'group', $type );
    add_variable( 'button', $button );
    add_variable( 'post_id', $post_id );
    add_variable( 'post_date', date('d F Y' ) );
    add_variable( 'start_date', date('d F Y' ) );
    add_variable( 'end_date', date('d F Y' ) );
    add_variable( 'publish_date', 'Choose Date' );
    add_variable( 'start_time', '' );
    add_variable( 'end_time', '' );
    add_variable( 'option_times', $option_times );
    add_variable( 'option_times_end', $option_times_end );
    add_variable( 'link', '' );
    add_variable( 'location', '' );
    add_variable( 'map', '' );
    add_variable( 'sef_scheme', get_sef_scheme() );
    add_variable( 'app_title', 'Add New ' . $title );
    add_variable( 'textarea', textarea( 'post[0]', 0 ) );
    add_variable( 'add_new_tag', add_new_tag( $post_id, 0 ) );
    add_variable( 'most_used_tags', get_most_used_tags( $type ) );
    add_variable( 'all_tags', get_post_tags( $post_id, 0, $type ) );
    add_variable( 'all_categories', all_categories( 0, 'categories', $type ) );
    add_variable( 'most_used_categories', get_most_used_categories( $type ) );
    add_variable( 'additional_data', attemp_actions( 'articles_additional_data' ) );
    add_variable( 'application_additional_data', attemp_actions( $type . '_additional_filed' ) );
    add_variable( 'share_to', additional_data( 'Share To', 'article_share_option', true, $args ) );
    add_variable( 'comments_settings', additional_data( 'Comments', 'discussion_settings', false, $args ) );
    add_variable( 'add_new_category', is_editor() || is_administrator() ? article_new_category( 0, 'categories', $type ) : '' );
    add_variable( 'post_date_cls', isset( $actions->action[ $type . '_post_date' ] ) ? run_actions( $type . '_post_date' ) : 'hidden' );
    add_variable( 'start_date_cls', isset( $actions->action[ $type . '_start_date' ] ) ? run_actions( $type . '_start_date' ) : 'hidden' );
    add_variable( 'end_date_cls', isset( $actions->action[ $type . '_end_date' ] ) ? run_actions( $type . '_end_date' ) : 'hidden' );
    add_variable( 'post_time_cls', isset( $actions->action[ $type . '_time' ] ) ? run_actions( $type . '_time' ) : 'hidden' );
    add_variable( 'post_link_cls', isset( $actions->action[ $type . '_link' ] ) ? run_actions( $type . '_link' ) : 'hidden' );
    add_variable( 'post_loc_cls', isset( $actions->action[ $type . '_loc' ] ) ? run_actions( $type . '_loc' ) : 'hidden' );
    add_variable( 'post_page_cls', isset( $actions->action[ $type . '_page' ] ) ? run_actions( $type . '_page' ) : 'hidden' );
    add_variable( 'related_product', additional_data_func('Show on Pages', 'related_product', true, $args));
    add_variable( 'post_news_cls', isset( $actions->action[ $type . '_news' ] ) ? run_actions( $type . '_news' ) : 'hidden' );
    // add_variable( 'related_news', additional_data_func('Related News', 'related_news', true, $args));
    add_variable( 'post_map_cls', isset( $actions->action[ $type . '_map' ] ) ? run_actions( $type . '_map' ) : 'hidden' );
    add_variable( 'post_publish_cls', isset( $actions->action[ $type . '_publish_date' ] ) ? run_actions( $type . '_publish_date' ) : 'hidden' );

    add_actions( 'section_title', $title . ' - Add New' );

    parse_template( 'form-block', 'f-block', true );

    return return_template( 'post-form' );  
}

/*
| -------------------------------------------------------------------------------------
| Edit Post Form
| -------------------------------------------------------------------------------------
*/
function edit_article( $post_id = 0, $type, $title )
{
    global $thepost;
    global $actions;

    $index   = 0;
    $args    = array( $index, $post_id, $type );
    $app_url = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;
    $url     = is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url );    
    $title   = explode("|", $title);
    $title   = $title[0];
    $option_times       = '';
    $option_times_end   = '';
    $selected_time      = '';
    $selected_time_end  = '';
    $starttime          = '';
    $endtime            = '';

    $thepost->post_id    = $post_id;
    $thepost->post_index = $index;

    $d = fetch_artciles( 'id=' . $post_id . '&type=' . $type );
    
    if( is_contributor() )
    {
        $button = '
        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>
        <li>' . button( 'button=save_draft&label=Save' ) . '</li>
        <li>' . button( 'button=cancel', $url ) . '</li>';
    }
    else
    {
        $button = '
        <li>' . button( 'button=publish' ) . '</li>
        <li>' . button( 'button=save_draft' ) . '</li>           
        <li>' . button( 'button=cancel', $url ) . '</li>
        <li>' . button( 'button=add_new', $url . '&prc=add_new' ) . '</li>';
    }

    set_template( TEMPLATE_PATH . '/template/post/form.html', 'post-form' );
    add_block( 'form-block', 'f-block', 'post-form' );

    if( is_save_draft() || is_publish() )
    {
        add_variable( 'textarea', textarea( 'post[' . $index . ']', $index, rem_slashes( $_POST['post'][$index] ), $post_id ) );
        add_variable( 'title', rem_slashes( $_POST['title'][$index] ) );
        add_variable( 'subtitle', rem_slashes( $_POST['subtitle'][$index] ) );
        add_variable( 'overview', rem_slashes('') );
        add_variable( 'link', $_POST['link'][$index] );
        add_variable( 'location', $_POST['location'][$index] );
        add_variable( 'map', $_POST['map'][$index] );

        $selected_categories = isset( $_POST['category'][$index] ) ? $_POST['category'][$index] : array( 1 );        
    }
    else
    {
        add_variable( 'textarea', textarea( 'post[' . $index . ']', $index, rem_slashes( $d['larticle_content'] ), $post_id ) );
        add_variable( 'title', rem_slashes( $d['larticle_title'] ) );
        add_variable( 'subtitle', rem_slashes( $d['larticle_subtitle'] ) );
        add_variable( 'overview', rem_slashes( $d['larticle_overview'] ) );
        add_variable( 'link', $d['llink'] );
        add_variable( 'location', $d['lloc'] );
        add_variable( 'map', $d['lmap'] );

        $selected_categories = find_selected_rules( $post_id, 'categories', $type );
    }

    /*if (empty( $d['lstart_time'] )) {
        $starttime = '';
    }
    else{
        $starttime = date('H:i', strtotime($d['lstart_time']));
    }

    if (empty( $d['lend_time'] )) {
        $endtime = '';
    }
    else{
        $endtime = date('H:i', strtotime($d['lend_time']));
    }*/
    
    /*if ( $d['lstart_time'] == '00:00:00' && $d['lend_time'] == '00:00:00' ) {
        $starttime  = 'Choose Time';
        $endtime    = 'Choose Time';
    }
    else if ( $d['lend_time'] == '00:00:00' ) {
        $endtime    = 'Choose Time';
        $starttime  = date('H:i', strtotime($d['lstart_time']));
    }
    else {
        $endtime    = date('H:i', strtotime($d['lend_time']));
        $starttime  = date('H:i', strtotime($d['lstart_time']));
    }*/

    $start = strtotime('00:00');
    $end   = strtotime('23:30');
    $selected_time = strtotime( $starttime );
    for ($i=$start; $i<=$end; $i = $i + 30*60){
        if ( $i == $selected_time) {
            $option_times .= '<option value="'.date('H:i',$i).'" selected>'.date('H:i',$i).'</option>';
        }
        else{
            $option_times .= '<option value="'.date('H:i',$i).'">'.date('H:i',$i).'</option>';
        }
    }

    $selected_time_end = strtotime( $endtime );
    for ($j=$start; $j<=$end; $j = $j + 30*60){
        if ( $j == $selected_time_end) {
            $option_times_end .= '<option value="'.date('H:i',$j).'" selected>'.date('H:i',$j).'</option>';
        }
        else{
            $option_times_end .= '<option value="'.date('H:i',$j).'">'.date('H:i',$j).'</option>';
        }
    }

    $starttime  = $d['lstart_time'];
    $endtime    = $d['lend_time'];
    
    add_variable( 'i', $index );
    add_variable( 'group', $type );
    add_variable( 'button', $button );
    add_variable( 'app_title', 'Edit ' . $title );
    add_variable( 'add_new_tag', add_new_tag( $post_id, $index ) );
    add_variable( 'all_tags', get_post_tags( $post_id, $index, $type ) );
    add_variable( 'most_used_tags', get_most_used_tags( $type, $index ) );
    add_variable( 'post_date', date('d F Y', strtotime( $d['lpost_date'] ) ) );
    add_variable( 'start_date', date('d F Y', strtotime( $d['lstart_date'] ) ) );
    // add_variable( 'start_time', date('H:i', strtotime( $d['lstart_time'] ) ) );
    // add_variable( 'end_time', date('H:i', strtotime( $d['lend_time'] ) ) );
    add_variable( 'start_time', $starttime );
    add_variable( 'end_time', $endtime );
    add_variable( 'publish_date', date('d F Y', strtotime( $d['lpublish_date'] ) ) );
    add_variable( 'option_times', $option_times );
    add_variable( 'option_times_end', $option_times_end );
    add_variable( 'end_date', date('d F Y', strtotime( $d['lend_date'] ) ) );
    add_variable( 'additional_data', attemp_actions( 'articles_additional_data' ) );
    add_variable( 'sef_scheme', get_sef_scheme( $type, $index, $post_id, $d, true ) );
    add_variable( 'application_additional_data', attemp_actions( $type . '_additional_filed' ) );
    add_variable( 'share_to', additional_data( "Share To", "article_share_option", true, $args ) );
    add_variable( 'all_categories', all_categories( $index, 'categories', $type, $selected_categories ) );
    add_variable( 'most_used_categories', get_most_used_categories( $type, $index, $selected_categories ) );
    add_variable( 'comments_settings', additional_data( 'Comments', 'discussion_settings', false, $args ) );
    add_variable( 'add_new_category', is_editor() || is_administrator() ? article_new_category( $index, 'categories', $type ) : '' );
    add_variable( 'post_date_cls', isset( $actions->action[ $type . '_post_date' ] ) ? run_actions( $type . '_post_date' ) : 'hidden' );
    add_variable( 'start_date_cls', isset( $actions->action[ $type . '_start_date' ] ) ? run_actions( $type . '_start_date' ) : 'hidden' );
    add_variable( 'end_date_cls', isset( $actions->action[ $type . '_end_date' ] ) ? run_actions( $type . '_end_date' ) : 'hidden' );
    add_variable( 'post_time_cls', isset( $actions->action[ $type . '_time' ] ) ? run_actions( $type . '_time' ) : 'hidden' );
    add_variable( 'post_link_cls', isset( $actions->action[ $type . '_link' ] ) ? run_actions( $type . '_link' ) : 'hidden' );
    add_variable( 'post_loc_cls', isset( $actions->action[ $type . '_loc' ] ) ? run_actions( $type . '_loc' ) : 'hidden' );
    add_variable( 'post_page_cls', isset( $actions->action[ $type . '_page' ] ) ? run_actions( $type . '_page' ) : 'hidden' );
    add_variable( 'related_product', additional_data_func('Show on Pages', 'related_product', true, $args));
    add_variable( 'post_news_cls', isset( $actions->action[ $type . '_news' ] ) ? run_actions( $type . '_news' ) : 'hidden' );
    // add_variable( 'related_news', additional_data_func('Related News', 'related_news', true, $args));
    add_variable( 'post_map_cls', isset( $actions->action[ $type . '_map' ] ) ? run_actions( $type . '_map' ) : 'hidden' );
    add_variable( 'post_publish_cls', isset( $actions->action[ $type . '_publish_date' ] ) ? run_actions( $type . '_publish_date' ) : 'hidden' );

    add_actions( 'section_title', $title . ' - Edit' );

    parse_template( 'form-block', 'f-block', true );

    return return_template( 'post-form' );  
}

/*
| -------------------------------------------------------------------------------------
| Delete Batch Post
| -------------------------------------------------------------------------------------
*/
function batch_delete_article( $type, $title )
{
    $title   = explode( '|', $title );
    $app_url = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' . $type : $type;

    set_template( TEMPLATE_PATH . '/template/post/batch-delete.html', 'post-delete' );
    add_block( 'loop-block', 'dl-block', 'post-delete' );
    add_block( 'delete-block', 'd-block', 'post-delete' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = fetch_artciles( 'id=' . $val . '&type=' . $type );

        add_variable('id', $d['larticle_id'] );
        add_variable('title',  $d['larticle_title'] );

        parse_template('loop-block', 'dl-block', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this data?' : 'these data?' ) );
    add_variable('action', is_admin_application() ? get_application_url( $type ) : get_state_url( $app_url ) );

    parse_template( 'delete-block', 'd-block', false );
    
    add_actions( 'section_title', 'Delete ' . $title[0] );

    return return_template( 'post-delete' );
}

/*
| -------------------------------------------------------------------------------------
| Sef URL Box
| -------------------------------------------------------------------------------------
*/
function get_sef_scheme( $type = 'pages', $i = 0, $post_id = 0, $d = null, $is_edit = false )
{
    if( $is_edit && is_permalink() )
    {
        $sef_url = $type == 'pages' ? 'http://' . site_url() . '/' : 'http://' . site_url() . '/' .$type . '/';
        $sef     = isset( $_POST['sef_box'][$i] ) ? $_POST['sef_box'][$i] : $d['lsef'];
        $more    = strlen( $sef ) > 50 ? '...' : '';

        return '
        <div id="sef_scheme_0">
            <strong>Permalink : </strong>
            <span>' . $sef_url . '</span>
            <span id="the_sef_' . $i . '">
                <span id="the_sef_content_' . $i . '" class="sef_content">' . substr( $sef, 0, 50 ) . $more . '</span>/
                <input type="button" value="Edit" id="edit_sef_' . $i . '" class="button_bold">
            </span>
            <span id="sef_box_' . $i . '" class="sef_box_act">
                <span>
                    <input type="text" name="sef_box[' . $i . ']" value="' . $sef . '" />/
                    <input type="button" value="Done" id="done_edit_sef_' . $i . '" class="button_bold">
                </span>
            </span>
                      
            <script type="text/javascript">
                jQuery("#the_sef_' . $i . '").click(function(){
                    jQuery("#the_sef_' . $i . '").hide();
                    jQuery("#sef_box_' . $i . '").show();
                });

                jQuery("#edit_sef_' . $i . '").click(function(){
                    jQuery("#the_sef_' . $i . '").hide();
                    jQuery("#sef_box_' . $i . '").show();
                });

                jQuery("#done_edit_sef_' . $i . '").click(function(){                                                              
                    var new_sef = jQuery("input[name=sef_box[' . $i . ']]").val();
                    var more = (new_sef.length > 50 ? "..." : "");
                    var prm  = new Object;
                        prm.title      = jQuery("input[name=title[0]]").val();
                        prm.update_sef = "true";
                        prm.post_id    = ' . $post_id . ';
                        prm.type       = "' . $type . '";
                        prm.new_sef    = new_sef;

                    jQuery("#the_sef_' . $i . '").show();
                    jQuery("#sef_box_' . $i . '").hide();
                    jQuery.post("articles.php", prm, function(theResponse){
                        if(theResponse=="BAD")
                        {
                            jQuery("input[name=sef_box[' . $i . ']]").val("' . $sef . '");
                            jQuery("#the_sef_content_' . $i . '").html("' . substr( $sef, 0, 50 ) . '");
                        }
                        else if(theResponse=="OK")
                        {
                            jQuery("#the_sef_content_' . $i . '").html(new_sef.substr(0,50) + more);
                        }
                    });
                });
            </script>
        </div>';
    }
    else
    {
        return '
        <div id="sef_scheme_0"></div>
        <script type="text/javascript">
            jQuery(function(){
                jQuery("input[name=title[0]]").blur(function(){
                    var url = "articles.php";
                    var prm = new Object;
                        prm.get_sef = "true";
                        prm.type    = "' . $type . '";
                        prm.index   = 0;
                        prm.title   = jQuery(this).val();
                    jQuery.post(url, prm,function(theResponse){
                        jQuery("#sef_scheme_0").html(theResponse);
                    });                     
                });
            });
        </script>'; 
    }
}

/*
| -------------------------------------------------------------------------------------
| Additional Data Box
| -------------------------------------------------------------------------------------
*/
function additional_data( $name, $tag, $display = true, $args = array() )
{
    global $thepost;

    $details = function_exists( $tag ) ? call_user_func_array( $tag, $args ) : $tag;
    $display = $display == false ? 'style="display:none;"' : '';
    
    return '
    <div class="additional_data">
        <h2 id="' . $tag . '_' . $thepost->post_index . '">' . $name . '</h2>
        <div class="additional_content" id="' . $tag . '_details_' . $thepost->post_index . '" '. $display .'>
           ' . $details . '
        </div>
        <script type="text/javascript">
            $(function(){
                $("#' . $tag . '_' . $thepost->post_index . '").click(function(){
                    $("#' . $tag . '_details_' . $thepost->post_index . '").slideToggle(100);
                    return false;
                });
            });
        </script>
    </div>';    
}

/*
| -------------------------------------------------------------------------------------
| Discussion Setting Box
| -------------------------------------------------------------------------------------
*/
function discussion_settings( $i, $post_id, $type = 'pages' )
{    
    $result = '';

    if( is_save_draft() || is_publish() )
    {
        if( isset( $_POST['allow_comments'][$i] ) )
        {
            $result = '<input type="checkbox" name="allow_comments[' . $i . ']" checked="checked" value="allow" />Allow Comments';
        }
        else
        {
            $result = '<input type="checkbox" name="allow_comments[' . $i . ']"  />Allow Comments';
        }
    }
    else
    {
        if( is_edit() || is_edit_all() )
        {
            $d = fetch_artciles( 'id=' . $post_id . '&type=' . $type );

            if( $d['lcomment_status'] == 'allowed' )
            {
                $result = '<input type="checkbox" name="allow_comments[' . $i . ']" checked="checked" value="allow" />Allow Comments';
            }
            else
            {
                $result = '<input type="checkbox" name="allow_comments[' . $i . ']" value="allow" />Allow Comments';
            }
        }
        else
        {
            if( get_meta_data( 'is_allow_comment' ) == 1 )
            {
                $result = '<input type="checkbox" name="allow_comments[' . $i . ']" checked="checked" value="allow" />Allow Comments';
            }
            else
            {
                $result = '<input type="checkbox" name="allow_comments[' . $i . ']"  value="allow" />Allow Comments';
            }
        }
    }

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Share Option Box
| -------------------------------------------------------------------------------------
*/
function article_share_option( $i, $post_id, $type = 'pages' )
{
    $result = '';
    $f_list = get_friend_list( $_COOKIE['user_id'] );
    
    if( is_save_draft() || is_publish() )
    {        
        if( $_POST['share_option'][$i] == 0 )
        {
            $result .= '<input type="radio" name="share_option[' . $i . ']" checked="checked" value="0" />Everyone';
        }
        else
        {
            $result .= '<input type="radio" name="share_option[' . $i . ']" value="0" />Everyone';
        }
        
        if( count( $f_list ) > 0 && count( $f_list['friends_list_id'] ) > 0 )
        {
            foreach( $f_list['friends_list_id'] as $key => $val )
            {
                if( isset( $_POST['share_option'][$i] ) && $_POST['share_option'][$i] == $f_list['friends_list_id'][$key] )
                {
                    $result .= '<input type="radio" name="share_option[' . $i . ']" checked="checked" value="' . $f_list['friends_list_id'][$key] . '" />' . $f_list['list_name'][$key];
                }
                else
                {
                    $result .= '<input type="radio" name="share_option[' . $i . ']" value="' . $f_list['friends_list_id'][$key] . '" />' . $f_list['list_name'][$key];
                }
            }
        }
    }
    else
    {
        if( is_edit() || is_edit_all() )
        {
            $d = fetch_artciles( 'id=' . $post_id . '&type=' . $type );
            
            if( $d['lshare_to'] == 0 )
            {
                $result .= '<input type="radio" name="share_option[' . $i . ']" checked="checked" value="0" />Everyone';
            }
            else
            {
                $result .= '<input type="radio" name="share_option[' . $i . ']" value="0" />Everyone';
            }
            
            if( count( $f_list ) > 0 && count( $f_list['friends_list_id'] ) > 0 )
            {
                foreach( $f_list['friends_list_id'] as $key => $val )
                {
                    if( $d['lshare_to'] == $f_list['friends_list_id'][$key] )
                    {
                        $result .= '<input type="radio" name="share_option[' . $i . ']" checked="checked" value="' . $f_list['friends_list_id'][$key] . '" />' . $f_list['list_name'][$key];
                    }
                    else
                    {
                        $result .= '<input type="radio" name="share_option[' . $i . ']" value="' . $f_list['friends_list_id'][$key] . '" />' . $f_list['list_name'][$key];
                    }
                }
            }
        }
        else
        {            
            $result .= '<input type="radio" name="share_option[' . $i . ']" checked="checked" value="0" />Everyone';

            if( count( $f_list ) > 0 && count( $f_list['friends_list_id'] ) > 0 )
            {
                foreach( $f_list['friends_list_id'] as $key => $val )
                {
                    $result .= '<input type="radio" name="share_option[' . $i . ']" value="' . $f_list['friends_list_id'][$key] . '" />' . $f_list['list_name'][$key];
                }
            }
        }
    }

    return $result;
}

/*
| -------------------------------------------------------------------------------------
| Save Post Data
| -------------------------------------------------------------------------------------
*/
function save_article( $title, $subtitle, $overview, $content, $status, $type, $post_date = '', $publish_date='', $start_date = '', $end_date = '', $start_time = '', $end_time = '', $link = '', $location = '', $map = '', $comments, $sef = '', $share_to = 0 )
{
    global $db, $allowedposttags, $allowedtitletags; 
    //echo $subtitle; return;
    
    $title          = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );
    $subtitle       = $subtitle;
    $overview       = $overview;
    $content        = kses( rem_slashes( $content ), $allowedposttags );
    $post_date      = empty( $post_date ) ? date( 'Y-m-d H:i:s' ) : $post_date;
    $publish_date   = empty( $publish_date ) ? date( 'Y-m-d H:i:s' ) : $publish_date;
    $start_date     = empty( $start_date ) ? date( 'Y-m-d H:i:s' ) : $start_date;
    $end_date       = empty( $end_date ) ? date( 'Y-m-d H:i:s' ) : $end_date;
    $link           = $link;
    $start_time     = empty( $start_time ) ? '' : $start_time;
    $end_time       = empty( $end_time ) ? '' : $end_time;
    $location       = $location;
    $map            = $map;

    if( empty( $sef ) )
    {
        $num_by_title_and_type = is_num_articles( 'title=' . $title . '&type=' . $type );
        
        if( $num_by_title_and_type > 0 )
        {
            for( $i = 2; $i <= $num_by_title_and_type + 1; $i++ )
            {
                $sef = generateSefUrl( $title ) . '-' . $i;
                
                if( is_num_articles( 'sef=' . $sef . '&type=' . $type ) < 1 )
                {
                    $sef = $sef;
                    break;
                }
            }
        }
        else
        {
            $sef = generateSefUrl( $title );
        }
    }
    
    $s = 'INSERT INTO lumonata_articles(
            larticle_title,
            larticle_subtitle,
            larticle_overview,
            larticle_content,
            larticle_status,
            larticle_type,
            lcomment_status,
            lsef,
            lpost_by,
            lpost_date,
            lpublish_date,
            lstart_date,
            lend_date,
            lstart_time,
            lend_time,
            llink,
            lloc,
            lmap,
            lupdated_by,
            ldlu,
            lshare_to)
         VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d,%s,%d)';
    $q = $db->prepare_query( $s, $title, $subtitle, $overview, $content, $status, $type, $comments, $sef, $_COOKIE['user_id'], $post_date, $publish_date, $start_date, $end_date, $start_time, $end_time, $link, $location, $map, $_COOKIE['user_id'], $post_date, $share_to );
    
    if( reset_order_id( 'lumonata_articles' ) )
    {
        return $db->do_query( $q );
    }
    
    return false;
}

/*
| -------------------------------------------------------------------------------------
| Syncronize Attachment Data
| -------------------------------------------------------------------------------------
*/
function attachment_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_attachment SET larticle_id = %d WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $new_id, $old_id );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Syncronize Additional Data
| -------------------------------------------------------------------------------------
*/
function additional_field_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_additional_fields SET lapp_id = %d WHERE lapp_id = %d';
    $q = $db->prepare_query( $s, $new_id, $old_id );

    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Update Post Data
| -------------------------------------------------------------------------------------
*/
function update_article( $post_id, $title, $subtitle, $overview, $content, $status, $type, $post_date = '', $publish_date='', $start_date = '', $end_date = '', $start_time = '', $end_time = '', $link = '', $location = '', $map = '', $comments, $share_to = 0 )
{
    //echo $subtitle; return;
    global $db, $allowedposttags, $allowedtitletags;
    
    $title          = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );
    $subtitle       = $subtitle;
    $overview       = $overview;
    $content        = kses( rem_slashes( $content ), $allowedposttags );
    $post_date      = empty( $post_date ) ? date( 'Y-m-d H:i:s' ) : $post_date;
    $publish_date   = empty( $publish_date ) ? date( 'Y-m-d H:i:s' ) : $publish_date;
    $start_date     = empty( $start_date ) ? date( 'Y-m-d H:i:s' ) : $start_date;
    $end_date       = empty( $end_date ) ? date( 'Y-m-d H:i:s' ) : $end_date;
    $start_time     = empty( $start_time ) ? '' : $start_time;
    $end_time       = empty( $end_time ) ? '' : $end_time;
    
    $s = 'UPDATE lumonata_articles SET 
            larticle_title=%s,
            larticle_subtitle=%s,
            larticle_overview=%s,
            larticle_content=%s,
            larticle_status=%s,
            larticle_type=%s,
            lcomment_status=%s,
            lupdated_by=%s,
            lpost_date=%s,
            lpublish_date=%s,
            lstart_date=%s,
            lend_date=%s,
            lstart_time=%s,
            lend_time=%s,
            llink=%s,
            lloc=%s,
            lmap=%s,
            ldlu=%s,
            lshare_to=%d
         WHERE larticle_id=%d'; 
    $q = $db->prepare_query( $s, $title, $subtitle, $overview, $content, $status, $type, $comments, $_COOKIE['user_id'], $post_date, $publish_date, $start_date, $end_date, $start_time, $end_time, $link, $location, $map, date( 'Y-m-d H:i:s' ), $share_to, $post_id );
    //echo $q; return;
    return $db->do_query( $q );
}

/*
| -------------------------------------------------------------------------------------
| Update Sef URL
| -------------------------------------------------------------------------------------
*/
function update_sef( $post_id, $title, $sef, $type )
{
    global $db, $allowedposttags, $allowedtitletags;

    $update = false;
    $title   = empty( $title ) ? 'Untitled' : kses( rem_slashes( $title ), $allowedtitletags );
    
    if( empty( $sef ) )
    {
        $update = false;
    }
    else
    {
        $num = is_num_articles( 'sef=' . $sef . '&type=' . $type );

        if( $num > 0 )
        {
            $update = false;
        }
        else
        {
            $update = true;
            $sef    = generateSefUrl( $sef );
        }        
    }
    
    if( $update )
    {
        $s = 'UPDATE lumonata_articles SET lsef=%s WHERE larticle_id=%d';
        $q = $db->prepare_query( $s, kses( rem_slashes( $sef ), $allowedtitletags ), $post_id );
        
        return $db->do_query( $q );
    }
    
    return false;
}

/*
| -------------------------------------------------------------------------------------
| Update Post Data
| -------------------------------------------------------------------------------------
*/
function delete_article( $artilce_id, $app_name )
{
    global $db;

    delete_attachment_by_article( $artilce_id );
    
    if( delete_additional_field( $artilce_id, $app_name ) )
    {
        $d = fetch_rule_relationship( 'app_id=' . $artilce_id );

        if( delete_rules_relationship( 'app_id=' . $artilce_id ) )
        {
            if( is_array( $d ) )
            {
                foreach( $d as $key => $val )
                {
                    $rule_count = count_rules_relationship( 'rule_id=' . $val );

                    update_rule_count( $val, $rule_count );
                }
                
                $s = 'DELETE FROM lumonata_articles WHERE larticle_id=%d';
                $q = $db->prepare_query( $s, $artilce_id );
                
                return $db->do_query( $q );
            }
        }
    }
}

/*
| -------------------------------------------------------------------------------------
| Count Post Data
| -------------------------------------------------------------------------------------
*/
function is_num_articles( $args = '' )
{
    global $db;

    $var_name['title'] = '';
    $var_name['id']    = '';
    $var_name['sef']   = '';
    $var_name['type']  = 'pages';
    
    $args = str_replace( '&amp;', 'U+0026', $args );
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'title' || $variable == 'id' || $variable == 'type' || $variable == 'sef' )
            {
                $var_name[$variable] = $value;
            }

            $var_name[$variable] = str_replace( 'U+0026', '&amp;', $var_name[$variable] );
        }
    }
    
    if( isset( $_COOKIE['user_type'] ) && ( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' ) )
    {
        $w = " lpost_by=" . $_COOKIE['user_id'] . " AND ";
    }
    else
    {
        $w = "";
    }
    
    if( !empty( $var_name['title'] ) && !empty( $var_name['id'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_id=%d", $var_name['type'], $var_name['id'] );
    }
    elseif( !empty( $var_name['id'] ) && empty( $var_name['title'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_id=%d", $var_name['type'], $var_name['id'] );
    }
    elseif( !empty( $var_name['title'] ) && empty( $var_name['id'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_title=%s", $var_name['type'], $var_name['title'] );
        
    }
    elseif( !empty( $var_name['sef'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND lsef=%s", $var_name['type'], $var_name['sef'] );
    }
    else
    {
        $sql = $db->prepare_query( "SELECT * from lumonata_articles WHERE $w larticle_type=%s", $var_name['type'] );
    }
    
    $r = $db->do_query( $sql );
    
    
    return $db->num_rows( $r );    
}

function is_have_articles( $type = 'articles' )
{
    if( is_num_articles( "type=$type" ) > 0 )
        return true;
    else
        return false;
}

function update_articles_order( $order, $start, $app_name )
{
    global $db;
    foreach( $order as $key => $val )
    {
        $sql = $db->prepare_query( "UPDATE lumonata_articles
                                     SET lorder=%d,
                                     lupdated_by=%s,
                                     ldlu=%s
                                     WHERE larticle_id=%d AND larticle_type=%s", $key + $start, $_COOKIE['user_id'], date( "Y-m-d H:i:s" ), $val, $app_name );
        $db->do_query( $sql );
        
        
    }
}

function update_articles_status( $id, $status )
{
    global $db;
    
    $sql = $db->prepare_query( "UPDATE lumonata_articles
                                 SET larticle_status=%s,
                                 lupdated_by=%s,
                                 ldlu=%s
                                 WHERE larticle_id=%d", $status, $_COOKIE['user_id'], date( "Y-m-d H:i:s" ), $id );
    $db->do_query( $sql );
    
    
    
    
}

function fetch_artciles_by_id( $article_id )
{
    global $db;
    $sql = $db->prepare_query( "SELECT * FROM lumonata_articles 
                                WHERE larticle_id=%d", $article_id );
    
    $r = $db->do_query( $sql );
    return $db->fetch_array( $r );
    
}

function fetch_artciles( $args = '', $fetch = true )
{
    global $db;
    $var_name['sef']   = '';
    $var_name['title'] = '';
    $var_name['id']    = '';
    $var_name['type']  = 'pages';
    $var_name['order'] = 'lorder';
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );
        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );
            if( $variable == 'sef' || $variable == 'title' || $variable == 'id' || $variable == 'type' || $variable == 'order' )
                $var_name[$variable] = $value;
            
        }
    }
    $w = "";
    if( isset( $_COOKIE['user_type'] ) && isset( $_COOKIE['user_type'] ) )
        if( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' )
        {
            $w = " lpost_by=" . $_COOKIE['user_id'] . " AND ";
        }
        else
        {
            $w = "";
        }
    
    if( !empty( $var_name['title'] ) && !empty( $var_name['id'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_id=%d ORDER BY $var_name[order]", $var_name['type'], $var_name['id'] );
    }
    elseif( !empty( $var_name['id'] ) && empty( $var_name['title'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_id=%d ORDER BY $var_name[order]", $var_name['type'], $var_name['id'] );
    }
    elseif( !empty( $var_name['title'] ) && empty( $var_name['id'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND larticle_title=%d ORDER BY $var_name[order]", $var_name['type'], $var_name['title'] );
    }
    elseif( !empty( $var_name['sef'] ) )
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE $w larticle_type=%s AND lsef=%s ORDER BY $var_name[order]", $var_name['type'], $var_name['sef'] );
    }
    else
    {
        $sql = $db->prepare_query( "SELECT * from lumonata_articles WHERE $w larticle_type=%s ORDER BY $var_name[order]", $var_name['type'] );
    }
    $r = $db->do_query( $sql );
    
    if( $fetch == true )
        return $db->fetch_array( $r );
    
    return $r;
}

function fetch_artciles_by_rule( $args = '', $fetch = true )
{
    global $db;

    $var_name['sef']    = '';
    $var_name['id']     = '';
    $var_name['search'] = '';
    $var_name['limit']  = '';
    $var_name['viewed'] = '';
    $var_name['type']   = 'blogs';
    $var_name['order']  = 'a.lorder';
    
    if( !empty( $args ) )
    {
        $args = explode( '&', $args );

        foreach( $args as $val )
        {
            list( $variable, $value ) = explode( '=', $val );

            if( $variable == 'sef' || $variable == 'title' || $variable == 'id' || $variable == 'type' || $variable == 'order' || $variable == 'limit' || $variable == 'viewed' || $variable == 'search' )
            {
                $var_name[$variable] = $value;
            }            
        }
    }

    $w = '';

    if( isset( $_COOKIE['user_type'] ) && isset( $_COOKIE['user_type'] ) )
    {
        if( $_COOKIE['user_type'] == 'contributor' || $_COOKIE['user_type'] == 'author' )
        {
            $w = ' lpost_by=' . $_COOKIE['user_id'] . ' AND ';
        }
        else
        {
            $w = '';
        }
    }

    $l = $var_name['limit'] == '' ? '' : $db->prepare_query( ' LIMIT %d, %d', $var_name['limit'], $var_name['viewed'] );

    if( !empty( $var_name['id'] ) && !empty( $var_name['sef'] ) )
    {
        if( !empty( $var_name['search'] ) )
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND b.lrule_id = %d 
                  AND b.lsef = %s
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0 
                  AND ( a.larticle_title LIKE %s OR a.larticle_content LIKE %s )
                  GROUP BY a.larticle_id 
                  ORDER BY %s';
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['id'], $var_name['sef'], '%' . $var_name['search'] . '%',  '%' . $var_name['search'] . '%', $var_name['order'] );
        }
        else
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND b.lsef = %d 
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0
                  GROUP BY a.larticle_id 
                  ORDER BY %s' . $l;
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['sef'], $var_name['order'] );
        }     
    }
    elseif( !empty( $var_name['id'] ) && empty( $var_name['sef'] ) )
    {
        if( !empty( $var_name['search'] ) )
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND b.lrule_id = %d 
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0 
                  AND ( a.larticle_title LIKE %s OR a.larticle_content LIKE %s ) 
                  GROUP BY a.larticle_id 
                  ORDER BY %s';
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['id'], '%' . $var_name['search'] . '%',  '%' . $var_name['search'] . '%', $var_name['order'] );
        }
        else
        {     
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND b.lrule_id = %d 
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0
                  GROUP BY a.larticle_id 
                  ORDER BY %s' . $l;
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['id'], $var_name['order'] );
        }
    }
    elseif( !empty( $var_name['sef'] ) && empty( $var_name['id'] ) )
    {
        if( !empty( $var_name['search'] ) )
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND b.lsef = %d 
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0 
                  AND ( a.larticle_title LIKE %s OR a.larticle_content LIKE %s ) 
                  GROUP BY a.larticle_id 
                  ORDER BY %s';
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['sef'], '%' . $var_name['search'] . '%',  '%' . $var_name['search'] . '%', $var_name['order'] );
        }
        else
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND b.lsef = %d 
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0
                  GROUP BY a.larticle_id 
                  ORDER BY %s' . $l;
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['sef'], $var_name['order'] );
        }
    }
    else
    {
        if( !empty( $var_name['search'] ) )
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0 
                  AND ( a.larticle_title LIKE %s OR a.larticle_content LIKE %s ) 
                  GROUP BY a.larticle_id 
                  ORDER BY %s';
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', '%' . $var_name['search'] . '%',  '%' . $var_name['search'] . '%', $var_name['order'] );
        }
        else
        {
            $s = 'SELECT a.*, c.lrule_id, c.lname, c.lparent, c.ldescription, c.lrule, c.lgroup, c.lorder, c.lsef AS rsef
                  FROM lumonata_articles a, lumonata_rule_relationship b, lumonata_rules c
                  WHERE a.larticle_type = %s 
                  AND a.larticle_status = %s
                  AND a.larticle_id = b.lapp_id
                  AND c.lrule_id = b.lrule_id
                  AND a.lshare_to = 0
                  GROUP BY a.larticle_id 
                  ORDER BY %s' . $l;
            $q = $db->prepare_query( $s, $var_name['type'], 'publish', $var_name['order'] );
        }
    }

    $r = $db->do_query( $q );
    
    if( $fetch == true )
    {
        return $db->fetch_array( $r );
    }
    else
    {
        return $r;
    }
}

function the_looping_articles( $type = 'articles', $args = '' )
{
    global $db;
    //set template
    set_template( TEMPLATE_PATH . "/article-lists.html", 'article' );
    //set block
    
    add_block( 'loopArticle', 'lArticle', 'article' );
    if( !empty( $args ) )
    {
        $id = post_to_id();
        if( $args == 'category' )
            $sql = $db->prepare_query( "SELECT a.*
                                        FROM lumonata_articles a,lumonata_rule_relationship b
                                        WHERE a.larticle_type=%s AND a.larticle_status=%s
                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
                                        ORDER BY a.lorder", $type, 'publish', $id );
        else
            $sql = $db->prepare_query( "SELECT a.*
                                        FROM lumonata_articles a,lumonata_rule_relationship b
                                        WHERE a.larticle_status=%s
                                        AND b.lrule_id=%d AND a.larticle_id=b.lapp_id AND lshare_to=0
                                        ORDER BY a.lorder", 'publish', $id );
        
        
    }
    else
    {
        $sql = $db->prepare_query( "SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s AND lshare_to=0 order by lorder", $type, 'publish' );
    }
    
    $r = $db->do_query( $sql );
    $i = 1;
    while( $data = $db->fetch_array( $r ) )
    {
        if( $i <= post_viewed() )
        {
            add_variable( 'the_thumbs', the_attachment( $data['larticle_id'] ) );
            add_variable( 'artilce_link', permalink( $data['larticle_id'] ) );
            add_variable( 'the_title', $data['larticle_title'] );
            add_variable( 'the_brief', filter_content( $data['larticle_content'], true ) );
            add_variable( 'post_date', date( get_date_format(), strtotime( $data['lpost_date'] ) ) );
            add_variable( 'the_user', the_user( $data['lpost_by'] ) );
            add_variable( 'the_categories', the_categories( $data['larticle_id'], $data['larticle_type'] ) );
            add_variable( 'the_tags', the_tags( $data['larticle_id'], $data['larticle_type'] ) );
            if( is_break_comment() )
            {
                add_variable( 'comments', comments( $data['larticle_id'], $data['lcomment_status'], comment_per_page() ) );
            }
            parse_template( 'loopArticle', 'lArticle', true );
            $i++;
        }
        else
        {
            break;
        }
        
    }
    
    return return_template( 'article' );
}

function the_looping_categories( $type = 'articles' )
{
    $rules  = fetch_rule( 'rule_id=' . post_to_id() . '&group=' . get_appname() );
    $return = "<div class=\"category_name\"><h1>" . $rules['lname'] . "</h1></div>";
    $return .= the_looping_articles( $type, 'category' );
    return $return;
}

function the_looping_tags()
{
    return the_looping_articles( 'article', 'tag' );
}

function article_detail()
{
    global $db;
    
    //set template
    set_template( TEMPLATE_PATH . "/article-detail.html", 'thearticle' );
    //set block
    
    add_block( 'article', 'bArticle', 'thearticle' );
    $sql      = $db->prepare_query( "SELECT * from lumonata_articles
                                WHERE larticle_id=%d", post_to_id() );
    $r        = $db->do_query( $sql );
    $data     = $db->fetch_array( $r );
    $the_post = $data['larticle_content'];
    $the_post = filter_content( $the_post );
    $the_post = str_replace( "<p>[album_set]</p>", "<div class=\"album_set\">" . the_attachment( $data['larticle_id'], 0 ) . "<br clear=\"both\" /></div>", $the_post, $count_replace );
    $the_post = str_replace( "[album_set]", "<div class=\"album_set\">" . the_attachment( $data['larticle_id'], 0 ) . "<br clear=\"both\" /></div>", $the_post, $count_replace2 );
    
    if( $count_replace > 0 || $count_replace2 > 0 )
    {
        add_actions( 'header', 'get_css_inc', 'fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.css' );
        add_actions( 'header', 'get_javascript_inc', 'fancybox-1.3.4/fancybox/jquery.fancybox-1.3.4.pack.js' );
        add_actions( 'header', 'get_javascript_inc', 'fancybox-1.3.4/fancybox/fancybox.js' );
    }
    
    add_variable( 'the_thumbs', the_attachment( $data['larticle_id'], 0 ) );
    add_variable( 'the_title', $data['larticle_title'] );
    add_variable( 'the_post', $the_post );
    add_variable( 'post_date', date( get_date_format(), strtotime( $data['lpost_date'] ) ) );
    add_variable( 'the_user', the_user( $data['lpost_by'] ) );
    add_variable( 'the_categories', the_categories( $data['larticle_id'], $data['larticle_type'] ) );
    add_variable( 'the_tags', the_tags( $data['larticle_id'], $data['larticle_type'] ) );
    add_variable( 'comments', comments( $data['larticle_id'], $data['lcomment_status'] ) );
    add_variable( 'additional_article_plugins', attemp_actions( 'additional_article_plugins' ) );
    parse_template( 'article', 'bArticle', false );
    
    return return_template( 'thearticle' );
}

function the_user( $id )
{
    $the_user = fetch_user( $id );
    return $the_user['ldisplay_name'];
}

function the_categories( $post_id, $type )
{
    $the_selected_categories = find_selected_rules( $post_id, 'categories', $type );
    $the_categories          = recursive_taxonomy( 0, 'categories', $type, 'category', $the_selected_categories );
    return trim( $the_categories, ', ' );
}

function the_tags( $post_id, $type )
{
    $the_selected_tags = find_selected_rules( $post_id, 'tags', $type );
    $the_tags          = recursive_taxonomy( 0, 'tags', $type, 'tag', $the_selected_tags );
    return trim( $the_tags, ', ' );
}

function filter_content( $the_content, $splitit = false )
{
    if( $splitit )
    {
        $match       = preg_split( "/<!--\s+pagebreak\s+-->/", $the_content );
        $the_content = $match[0];
    }
    
    return $the_content;
}

function get_article_title( $id )
{
    global $db;

    $query  = $db->prepare_query( "SELECT larticle_title 
                                    FROM lumonata_articles
                                    WHERE larticle_id=%d", $id );
    $result = $db->do_query( $query );
    $dt     = $db->fetch_array( $result );
    return $dt['larticle_title'];
}

function get_load_more_link( $page = 1 )
{
    $page = $page + 1;

    return '
    <div id="loadMore' . $page . '" class="morebox">
        <a href="#" class="loadMore" id="' . $page . '" rel="1">Load more...</a>
    </div>';
}

function get_load_more_js( $type = 'pages' )
{
    $type   = empty( $type ) ? $_GET['state'] : $type;
    $cname  = isset( $_GET['category_name'] ) ? $_GET['category_name'] : '';
    $loader = 'http://' . TEMPLATE_URL . '/images/loading.gif';
    $ajxurl = 'http://' . site_url() . '/lumonata-admin/?state=' . $type . '&category_name=' . $cname;

    return '
    <script type="text/javascript">
        $(function(){
            $(".loadMore").live("click",function(){
                var page = $(this).attr("id");
                var type = "'. $type . '";

                if( page )
                {
                    $("#loadMore"+page).html("<img src=\"' . $loader . '\" border=\"0\">");
                    $.ajax({
                        type: "POST",
                        url: "' . $ajxurl . '",
                        data: "page="+ page + "&loadMore=" + type,
                        cache: false,
                        success: function(html){                                                                                        
                            $("#list_item").append(html);
                            $("#loadMore"+page).remove();
                        }
                    });
                }
                else
                {
                    $(".morebox").html("The End");
                }
                return false;
            });
        });
    </script>';
}

function popup_delete()
{
    return '   
    <div style="display: none;" id="delete_confirmation_wrapper">
        <div class="fade"></div>
        <div class="popup_block">
            <div class="popup">
                <div class="alert_yellow">Are sure want to delete Welcome to Arunna?</div>
                <div style="text-align:right;margin:10px 5px 0 0;">
                    <button id="delete_yes" class="button" value="yes" name="confirm_delete" type="submit" data-type="' . $_GET['state'] . '">Yes</button>
                    <button id="delete_no" class="button" value="no" name="confirm_delete" type="button">No</button>
                    <button id="delete_cancel" class="button" value="cancel" name="confirm_delete" type="button">Cancel</button>                        
                </div>
            </div>
        </div>
    </div> ';
}

function additional_data_func( $name, $tag, $display=true, $args=array() )
{
    global $thepost;
    
    $details = function_exists($tag) ? call_user_func_array($tag, $args) : $tag;
    $display = $display==false ? 'style="display:none;"' : '';
        
    $data = '
    <div class="additional_data">
        <h2 id="'.$tag.'_'.$thepost->post_index.'">'.$name.'</h2>
        <div class="additional_content '.$tag.'" id="'.$tag.'_details_'.$thepost->post_index.'" '.$display.' >
            <div class="additional-block">
                '.$details.'
            </div>
        </div>
        <script type="text/javascript">
            jQuery(function(){
                jQuery("#'.$tag.'_'.$thepost->post_index.'").click(function(){
                    jQuery("#'.$tag.'_details_'.$thepost->post_index.'").slideToggle(100);
                    return false;
                });
            });
        </script>
    </div>';

    return $data;
}

/*
| -------------------------------------------------------------------------------------
| Related Product
| -------------------------------------------------------------------------------------
*/
function save_additional_product( $post_id )
{
    if( isset($_POST['additional_fields']) )
    {
        foreach( $_POST['additional_fields'] as $key=>$val )
        {
           foreach( $val as $subkey=>$subval )
           {
                $subval = is_array($subval) ? json_encode($subval) : $subval;
                // edit_additional_field($post_id, $key, $subval, 'quick-facts');
           }
        }

        if( !isset($_POST['additional_fields']['related_product']) )
        {
            // edit_additional_field($post_id, 'related_product', '', 'quick-facts');
        }
    }
}

function related_product( $index=0, $post_id, $type='pages' )
{
    global $db;
    //echo $post_id;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_status=%s AND larticle_type=%s AND (  lsef<>%s AND lsef<>%s AND lsef<>%s ) ORDER BY lorder';
    $q = $db->prepare_query($s, 'publish', 'pages', 'quick-facts', 'contact', 'events');
    $r = $db->do_query($q);

    if( $db->num_rows($r) > 0 )
    {
        $value = get_additional_field($post_id, 'related_product', 'quickfacts'); //echo $value;
        $value = json_decode($value, true);
        $list  = '
        <select data-placeholder="Choose pages..." style="width:100%;" multiple class="chosen-select related_productform" name="additional_fields[related_product]['.$index.'][]" autocomplete="off">';
            while( $d=$db->fetch_array($r) )
            {
                $check = is_array($value) && in_array($d['larticle_id'], $value) ? 'selected="selected"' : '';
                $list .= '<option value="'.$d['larticle_id'].'" '.$check.'>'.$d['larticle_title'].'</option>';
            }
            $list .= '
        </select>';

        return $list;
    }
}

/*
| -------------------------------------------------------------------------------------
| Related News
| -------------------------------------------------------------------------------------
*/

/*function related_news( $index=0, $post_id, $type='news' )
{
    global $db;
    //echo $post_id;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_status=%s AND larticle_type=%s  ORDER BY lorder';
    $q = $db->prepare_query($s, 'publish', 'news');
    $r = $db->do_query($q);

    if( $db->num_rows($r) > 0 )
    {
        $value = get_additional_field($post_id, 'related_news', 'news'); //echo $value;
        $value = json_decode($value, true);
        $list  = '
        <select data-placeholder="Choose pages..." style="width:100%;" class="chosen-select related_newsform" name="additional_fields[related_news]['.$index.'][]" autocomplete="off">';
            while( $d=$db->fetch_array($r) )
            {
                $check = is_array($value) && in_array($d['larticle_id'], $value) ? 'selected="selected"' : '';
                $list .= '<option value="'.$d['larticle_id'].'" '.$check.'>'.$d['larticle_title'].'</option>';
            }
            $list .= '
        </select>';

        return $list;
    }
}*/

?>