<?php

function insert_attachment( $article_id, $title, $mime_type, $attach_loc, $attach_loc_thumb = '', $attach_loc_med = '', $attach_large = '', $alt_text = '', $caption = '', $attach_app_name = 'attachment' )
{
    global $db;

    $upload_date      = date( 'Y-m-d H:i:s', time() );
    $date_last_update = date( 'Y-m-d H:i:s', time() );
    $title            = rem_slashes( $title );
    $alt_text         = rem_slashes( $alt_text );
    $caption          = rem_slashes( $caption );

    $sql              = 'INSERT INTO lumonata_attachment(
                            larticle_id,
                            lattach_loc,
                            lattach_loc_thumb,
                            lattach_loc_medium,
                            lattach_loc_large,
                            lattach_app_name,
                            ltitle,
                            lalt_text,
                            lcaption,
                            upload_date,
                            date_last_update,
                            mime_type,
                            lorder)
                        VALUES(%d,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%d)';
    $query            = $db->prepare_query( $sql, 
                            $article_id, 
                            $attach_loc, 
                            $attach_loc_thumb, 
                            $attach_loc_med, 
                            $attach_large, 
                            $attach_app_name,
                            $title, 
                            $alt_text, 
                            $caption, 
                            $upload_date, 
                            $date_last_update, 
                            $mime_type, 
                            1 );
    
    if( reset_order_id( 'lumonata_attachment' ) )
    {
        if( $db->do_query( $query ) )
        {
            return true;
        }
    }

    return false;
}

function edit_attachment( $attach_id, $title, $order = null, $alt_text = '', $caption = '', $youtube_url = '', $photo_360 = '', $img_title_lang = '' )
{
    global $db;

    $title            = rem_slashes( $title );
    $alt_text         = rem_slashes( $alt_text );
    $caption          = rem_slashes( $caption );
    $date_last_update = date( 'Y-m-d H:i:s', time() );

    $img_title_lang_result = "";
    if(!empty($img_title_lang))
    {
        $img_title_lang_result = json_encode($img_title_lang);
    }

    if( $order == null || $order == '' )
    {
        $sql = 'UPDATE lumonata_attachment SET
                    ltitle=%s,
                    lalt_text=%s,
                    lcaption=%s,
                    date_last_update=%s,
                    lyoutube_url=%s,
                    lphoto_360=%s,
                    lattach_language=%s
                WHERE lattach_id=%d';
        $query = $db->prepare_query( $sql, $title, $alt_text, $caption, $date_last_update, $youtube_url, $photo_360, $img_title_lang_result, $attach_id );
    }
    else
    {
        $sql = 'UPDATE lumonata_attachment SET
                    ltitle=%s,
                    lalt_text=%s,
                    lcaption=%s,
                    date_last_update=%s,
                    lyoutube_url=%s,
                    lphoto_360=%s
                    lorder=%d
                WHERE lattach_id=%d';
        $query = $db->prepare_query( $sql, $title, $alt_text, $caption, $date_last_update, $youtube_url, $photo_360, $order, $attach_id );
    }

    if( $db->do_query( $query ) )
    {
        return true;
    }

    return false;
}
function delete_attachment( $attach_id )
{
    global $db;

    //-- original file
    $loc = attachment_value( 'lattach_loc', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    //-- thumbanil
    $loc = attachment_value( 'lattach_loc_thumb', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    //-- Medium
    $loc = attachment_value( 'lattach_loc_medium', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    //-- Large
    $loc = attachment_value( 'lattach_loc_large', $attach_id );

    if( !empty( $loc ) )
    {
        $file = ROOT_PATH . $loc;

        if( file_exists( $file ) )
        {
            unlink( $file );
        }
    }

    $query = $db->prepare_query( 'DELETE FROM lumonata_attachment WHERE lattach_id=%d', $attach_id );

    if( $db->do_query( $query ) )
    {
        return true;
    }

    return false;

}

function attachment_value( $val, $attach_id )
{
    global $db;

    $q = $db->prepare_query( "SELECT $val FROM lumonata_attachment WHERE lattach_id=%d", $attach_id );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    return $d[$val];
}

function upload_image_attachment( $source, $file_type, $file_name, $post_id )
{
    global $db;

    $folder_name = upload_folder_name();

    if( !defined( 'FILES_LOCATION' ) )
    {
        define( 'FILES_LOCATION', '/lumonata-content/files' );
    }

    if( !is_dir( FILES_PATH . '/' . upload_folder_name() ) )
    {
        create_dir( FILES_PATH . '/' . upload_folder_name() );
    }

    $default_title   = file_name_filter( $file_name );
    $file_name       = character_filter( $file_name );
    $file_name       = file_name_filter( $file_name ) . '-' . time() . file_name_filter( $file_name, true );

    //-- Thumbnail Image
    $file_name_t     = '';
    $file_location_t = '';
    $file_name_t     = file_name_filter( $file_name ) . '-thumbnail' . file_name_filter( $file_name, true );
    $file_location_t = FILES_LOCATION . '/' . $folder_name . '/' . $file_name_t;
    $destination     = FILES_PATH . '/' . $folder_name . '/' . $file_name_t;

    add_actions( 'thumbnail_file_location', $file_location_t );

    //-- Create thumbnail image here
    if( upload_resize( $source, $destination, $file_type, thumbnail_image_width(), thumbnail_image_height() ) )
    {
        //-- Medium Image
        $file_name_m     = '';
        $file_location_m = '';
        $file_name_m     = file_name_filter( $file_name ) . '-medium' . file_name_filter( $file_name, true );
        $file_location_m = FILES_LOCATION . '/' . $folder_name . '/' . $file_name_m;
        $destination = FILES_PATH . '/' . $folder_name . '/' . $file_name_m;

        add_actions( 'medium_file_location', $file_location_m );

        //-- Create medium size image here
        if( upload_resize( $source, $destination, $file_type, medium_image_width(), medium_image_height() ) )
        {
            //-- Large Image
            $file_name_l     = '';
            $file_location_l = '';
            $file_name_l     = file_name_filter( $file_name ) . '-large' . file_name_filter( $file_name, true );
            $file_location_l = FILES_LOCATION . '/' . $folder_name . '/' . $file_name_l;
            $destination_l   = FILES_PATH . '/' . $folder_name . '/' . $file_name_l;

            add_actions( 'large_file_location', $file_location_l );

            //-- Create Large size image here
            if( upload_resize( $source, $destination_l, $file_type, large_image_width(), large_image_height() ) )
            {
                //-- Original Image
                $destination   = FILES_PATH . '/' . $folder_name . '/' . $file_name;
                $file_location = FILES_LOCATION . '/' . $folder_name . '/' . $file_name;

                add_actions( 'original_file_location', FILES_LOCATION . '/' . $folder_name . '/' . $file_name );

                //-- Upload the original image
                if( upload( $source, $destination ) )
                {
                    return insert_attachment( $post_id, $default_title, $file_type, $file_location, $file_location_t, $file_location_m, $file_location_l );
                }
            }
        }
    }

    return false;
}

function the_attachment( $article_id, $limit = 4 )
{
    global $db;

    if( empty( $article_id ) )
    {
        return;
    }

    $img   = '';
    $limit = $limit == 0 ? '' : 'limit ' . $limit;

    $s = "SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lorder ASC $limit ";
    $q = $db->prepare_query( $s, $article_id );
    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
        $img .= "
        <div class=\"the_thumbs_item\" >
            <a rel=\"example_group\" href=\"". HTTP . site_url() . $d['lattach_loc_large'] . "\" title=\"" . $d['ltitle'] . "\" >
                <img id=\"thumbs_" . $d['lattach_id'] . "\" src=\"". HTTP . site_url() . $d['lattach_loc_thumb'] . "\" alt=\"" . $d['lalt_text'] . "\" title=\"" . $d['ltitle'] . "\" border=\"0\" />
            </a>
       </div>";
    }

    return $img;
}

function jQuery_centering_images( $id )
{
    return "
    <script type=\"text/javascript\">
        $(window).load(function(){
            var parent_height = $('.the_thumbs_item').height();
            var image_height  = $('#thumbs_" . $id . "').height();
            var top_margin    = ( parent_height - image_height ) / 2;

            $('#thumbs_" . $id . "').css( 'margin-top' , top_margin);
        });
    </script>";
}

function get_attachment( $article_id = 0, $type = '' )
{
    global $db;

    $url        = get_attachment_tab_url( $_GET['tab'] ) . '&page=';
    $sort_order = " ORDER BY lorder ASC";
    $attch      = '';
    $js         = '';

    if( isset( $_GET['sort_order'] ) )
    {
        if( $_GET['sort_order'] == 'desc' )
        {
            $sort_order = " ORDER BY lorder DESC";
        }
    }

    if( $article_id != 0 )
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d $sort_order", $article_id );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND mime_type LIKE %s $sort_order", $article_id, '%' . $type . '%' );
        }
    }
    else
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment $sort_order" );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE mime_type LIKE %s $sort_order", '%' . $type . '%' );
        }
    }

    //-- Setup paging system
    $num_rows = count_rows( $sql );
    $viewed   = list_viewed();

    if( isset( $_GET['page'] ) )
    {
        $page = $_GET['page'];
    }
    else
    {
        $page = 1;
    }

    $limit = ( $page - 1 ) * $viewed;

    if( $article_id != 0 )
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND ltitle!='thumb_pdf' $sort_order LIMIT %d, %d", $article_id, $limit, $viewed );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND ltitle!='thumb_pdf' AND mime_type LIKE %s $sort_order LIMIT %d, %d", $article_id, '%' . $type . '%', $limit, $viewed );
        }
    }
    else
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE ltitle!='thumb_pdf' $sort_order LIMIT %d, %d", $limit, $viewed );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE ltitle!='thumb_pdf' AND mime_type LIKE %s $sort_order LIMIT %d, %d", '%' . $type . '%', $limit, $viewed );
        }
    }

    $result = $db->do_query( $sql );

    $i = $viewed * ( $page - 1 ) + 1; //-- start order number
    $y = $i + count_rows( $sql ) - 1; //-- end order number

    $attch  = "
    <form method=\"post\" action=\"\">
        <input type=\"hidden\" name=\"start_order\" value=\"$i\" />
        <input type=\"hidden\" name=\"the_tab\" value=\"" . $_GET['tab'] . "\" />
        <input type=\"hidden\" name=\"site_url\" value=\"" . SITE_URL . "\" />
        <div class=\"media_navigation\">
            <div class=\"search_wraper\">" . search_box( 'upload-media.php', 'media_gallery', 'textarea_id=' . $_GET['textarea_id'] . '&tab=' . $_GET['tab'] . '&article_id=' . $article_id . '&' ) . "</div>
            <!-- <div class=\"paging_media\">" . paging( $url, $num_rows, $page, $viewed, 5 ) . "</div> -->
            <br clear=\"all\" >
            <div class=\"sort_order\">
                Sort Order: 
                <a href=\"" . get_attachment_tab_url( $_GET['tab'] ) . "&sort_order=asc\">Ascending</a> |
                <a href=\"" . get_attachment_tab_url( $_GET['tab'] ) . "&sort_order=desc\">Descending </a>
            </div>
        </div>

        <div id=\"response\">{response}</div>

        <div class=\"media_title clearfix\">
            <input type=\"checkbox\" class=\"title_checkbox\" name=\"select_all\" style=\"margin:10px 0px 10px 1px;\">
            <div class=\"media_description\">Media</div>
            <div class=\"media_action\" style=\"width: 45%;\">Sort / Order</div>
            <br clear=\"left\" />
        </div>";

        $attch .= "
        <div id=\"media_gallery\"  style=\"border: medium none;\">";

            if( isset( $_POST['s'] ) && $_POST['s'] != "Search" )
            {
                $attch .= search_attachment_results( $_POST['s'], $_GET['tab'], $article_id, $_GET['textarea_id'] );
            }
            else
            {
                $attch .= gallery_items( $result, $_GET['tab'], $_GET['textarea_id'], $i );
            }

            $attch .= "
        </div>";

        if( $num_rows > $viewed )
        {
            $attch .= get_load_more_link_media( $page );
        }

        $attch .= "
        <div class=\"attach-list-action-box\">" . button( "button=save_changes&label=Save All Changes&name=save_all_changes" ) . " &nbsp;" . button( 'button=delete&type=submit&enable=false' ) . "</div>
        <!-- <div class=\"paging_media\">" . paging( $url, $num_rows, $page, $viewed, 5 ) . "</div> -->";

        $attch .= "
    </form>";

    $attch .= get_load_more_js_media();

    $attch .= "
    <script type=\"text/javascript\" language=\"javascript\">
        $(function(){ });
    </script>";
    return $attch;
}

function get_attachment_ajax( $article_id = 0, $type = '' )
{
    global $db;

    $url        = get_attachment_tab_url( $_GET['tab'] ) . '&page=';
    $sort_order = " ORDER BY lorder ASC";
    $attch      = '';
    $js         = '';

    if( isset( $_GET['sort_order'] ) )
    {
        if( $_GET['sort_order'] == 'desc' )
        {
            $sort_order = " ORDER BY lorder DESC";
        }
    }

    if( $article_id != 0 )
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d $sort_order", $article_id );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND mime_type LIKE %s $sort_order", $article_id, '%' . $type . '%' );
        }
    }
    else
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment $sort_order" );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE mime_type LIKE %s $sort_order", '%' . $type . '%' );
        }
    }

    //-- Setup paging system
    $num_rows = count_rows( $sql );
    $viewed   = list_viewed();

    if( isset( $_GET['page'] ) )
    {
        $page = $_GET['page'];
    }
    else
    {
        $page = 1;
    }

    $page  = $_POST['page'] + 1;
    $limit = $viewed * ( $page - 1 );

    if( $article_id != 0 )
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND ltitle!='thumb_pdf' $sort_order LIMIT %d, %d", $article_id, $limit, $viewed );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND ltitle!='thumb_pdf' AND mime_type LIKE %s $sort_order LIMIT %d, %d", $article_id, '%' . $type . '%', $limit, $viewed );
        }
    }
    else
    {
        if( empty( $type)  )
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE ltitle!='thumb_pdf' $sort_order LIMIT %d, %d", $limit, $viewed );
        }
        else
        {
            $sql = $db->prepare_query( "SELECT * FROM lumonata_attachment WHERE ltitle!='thumb_pdf' AND mime_type LIKE %s $sort_order LIMIT %d, %d", '%' . $type . '%', $limit, $viewed );
        }
    }

    $result = $db->do_query( $sql );
    $number = $db->num_rows( $result );

    $i = $viewed * ( $page - 1 ) + 1; //-- start order number
    $y = $i + count_rows( $sql ) - 1; //-- end order number

    if( $number > 0 )
    {
        $attch  = gallery_items( $result, $_GET['tab'], $_GET['textarea_id'], $i );
        $attch .= get_load_more_link_media( $page );
    }

    echo $attch;
}

function search_attachment_results( $s = '', $tab = 'gallery', $textarea_id = 0 )
{
    global $db;

    if( empty( $s ) )
    {
        if( $tab == 'gallery' )
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lorder', $article_id );
        }
        else
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment ORDER BY lorder' );
        }
    }
    else
    {
        if( $tab == 'gallery' )
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id=%d AND (ltitle LIKE %s OR lalt_text LIKE %s OR lcaption LIKE %s) ORDER BY lorder', $article_id, '%' . $s . '%', '%' . $s . '%', '%' . $s . '%' );
        }
        else
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE ltitle LIKE %s OR lalt_text LIKE %s OR lcaption LIKE %s ORDER BY lorder', '%' . $s . '%', '%' . $s . '%', '%' . $s . '%' );
        }
    }

    $result = $db->do_query( $sql );

    if( $db->num_rows( $result ) == 0 )
    {
        return "<div class=\"alert_yellow\">No results found for <em>$s</em>. Check your spelling or try another term.</div><br />";
    }

    return gallery_items( $result, $tab, $textarea_id );
}

function gallery_items( $result, $tab, $textarea_id, $i = 1 )
{
    global $db;

    $attch = '';

    while( $d = $db->fetch_array( $result ) )
    {
        $attch .= "
        <div class=\"media_gallery_item clearfix\" id=\"attachment_" . $d['lattach_id'] . "\">
            <div class=\"attach-loader\"></div>
            <div class=\"clearfix\">
                <input type=\"checkbox\" value=\"" . $d['lattach_id'] . "\" class=\"title_checkbox select\" name=\"select[]\"  style=\"margin-top: 0px;\">
                <div class=\"media_description\">
                    <div class=\"media_image_small\">" . attachment_icon( $d['mime_type'], '60:60', $d['lattach_loc_thumb'] ) . "</div>
                    <div class=\"media_item_title\">" . stripslashes( $d['ltitle'] ) . "</div>
                </div>
                <div class=\"media_action\">
                    <div class=\"media_item_action\">
                        <input type=\"text\" value=\"$i\" id=\"order_" . $d['lattach_id'] . "\" class=\"small_textbox\" name=\"order[" . $i . "]\">&nbsp;
                        <a href=\"#\" id=\"show_attach_" . $d['lattach_id'] . "\">Show</a> | <a href=\"#\" rel=\"delete_" . $d['lattach_id'] . "\">Delete</a>
                    </div>
                </div>
                <br clear=\"all\">
            </div>
            <div class=\"details_item clearfix\" style=\"display:none\" id=\"detail_attach_" . $d['lattach_id'] . "\" >";
                $attch .= attachment_details( $d, $i, $textarea_id, $tab );
                $attch .= "
            </div>
        </div>
        <script type=\"text/javascript\" language=\"javascript\">
            $(function(){
                $('#show_attach_" . $d['lattach_id'] . "').click(function(){
                    $('#detail_attach_" . $d['lattach_id'] . "').slideToggle(100);
                    $(this).text($(this).text()=='Show'?'Hide':'Show');

                    return false;
                });

                $('#detail_attach_" . $d['lattach_id'] . "').css('display','none');
            });
        </script>";

        $attch .= delete_confirmation_box( $d['lattach_id'], "Are you sure want to delete <code>" . $d['ltitle'] . "</code> from the gallery?", "upload-media.php", "attachment_" . $d['lattach_id'] );
        $i++;
    }

    return $attch;
}

function count_attachment( $article_id = 0, $type = '' )
{
    global $db;

    if( $article_id != 0 )
    {
        if( empty( $type ) )
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d', $article_id );
        }
        else
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND mime_type LIKE %s', $article_id, '%' . $type . '%' );
        }
    }
    else
    {
        if( empty( $type ) )
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment' );
        }
        else
        {
            $sql = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE mime_type LIKE %s', '%' . $type . '%' );
        }
    }

    $r = $db->do_query( $sql );

    return $db->num_rows( $r );
}

function upload_media_attachment( $source, $file_type, $file_name, $post_id )
{
    $folder_name = upload_folder_name();

    if( !defined( 'FILES_LOCATION' ) )
    {
        define( 'FILES_LOCATION', '/lumonata-content/files' );
    }

    $default_title = file_name_filter( $file_name );
    $file_name     = character_filter( $file_name );
    $destination   = FILES_PATH . '/' . $folder_name . '/' . $file_name;
    $file_location = FILES_LOCATION . '/' . $folder_name . '/' . $file_name;

    if( upload( $source, $destination ) )
    {
        add_actions( 'original_file_location', $file_location );

        return insert_attachment( $post_id, $default_title, $file_type, $file_location );
    }
}

function upload_folder_name()
{
    return date( 'Y', time() ) . date( 'm', time() );
}

function attachment_icon( $file_type, $file_size = '', $file_location = '' )
{
    $width  = '';
    $height = '';
    $img    = '';

    if( !empty( $file_size ) )
    {
        list( $width, $height ) = explode( ':', $file_size );
        $width  = "width=\"" . $width . "px\"";
        $height = "width=\"" . $height . "px\"";
    }

    switch( $file_type )
    {
        case "image/jpg":
        case "image/jpeg":
        case "image/pjpeg":
        case "image/gif":
        case "image/png":
            $img = "<img src=\"". HTTP . SITE_URL . $file_location . "\" $width $height />";
            break;
        case "application/x-shockwave-flash":
            $img = "<img src=\"". HTTP . SITE_URL . "/lumonata-admin/includes/media/default.png\" $width $height />";
            break;
        case "application/octet-stream":
            $img = "<img src=\"". HTTP . SITE_URL . "/lumonata-admin/includes/media/video.png\" $width $height />";
            break;
        case "audio/m4a":
        case "audio/x-ms-wma":
        case "audio/mpeg":
            $img = "<img src=\"". HTTP . SITE_URL . "/lumonata-admin/includes/media/audio.png\" $width $height />";
            break;
        case "application/octet":
        case "application/pdf":
            $img = "<img src=\"". HTTP . SITE_URL . "/lumonata-admin/includes/media/document.png\" $width $height />";
            break;
        case "application/msword":
            $img = "<img src=\"". HTTP . SITE_URL . "/lumonata-admin/includes/media/document.png\" $width $height />";
            break;
        case "text/plain":
            $img = "<img src=\"". HTTP . SITE_URL . "/lumonata-admin/includes/media/text.png\" $width $height />";
            break;
    }
    return $img;
}

function update_attachment_order( $order, $start )
{
    global $db;

    foreach( $order as $key => $val )
    {
        $s = 'UPDATE lumonata_attachment SET lorder=%d WHERE lattach_id=%d';
        $q = $db->prepare_query( $s, $key + $start, $val );
        $db->do_query( $q );
    }
}

function attachment_details( $d, $index, $textarea_id, $tab )
{
    $mime_type = array(
        'image/jpg',
        'image/jpeg',
        'image/pjpeg',
        'image/gif',
        'image/png' 
    );

    $detail = '';

    if( in_array( $d['mime_type'], $mime_type ) )
    {
        $detail = "
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left_image\">";
                $detail .= attachment_icon( $d['mime_type'], '100:100', $d['lattach_loc_medium'] );
                $detail .= "
            </div>
            <div class=\"media_field_right_image\">
                <strong>File Type : </strong>" . $d['mime_type'] . "<br /><br />
                <strong>Upload Date : </strong>" . date( get_date_format(), strtotime( $d['upload_date'] ) ) . "<br /><br />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Title: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"title[$index]\" value=\"" . stripslashes( $d['ltitle'] ) . "\" />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Alternate Text: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"alt_text[$index]\" value=\"" . stripslashes( $d['lalt_text'] ) . "\" />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Caption: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"caption[$index]\" value=\"" . stripslashes( $d['lcaption'] ) . "\" />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Alignment: </strong>
            </div>
            <div class=\"media_field_right\">
                <ul id=\"alignment\">
                    <li id=\"alignment_none\" ><input type=\"radio\" name=\"alignment[$index]\" value=\"none\" checked=\"checked\" />None</li>
                    <li id=\"alignment_left\"><input type=\"radio\" name=\"alignment[$index]\" value=\"left\"   />Left</li>
                    <li id=\"alignment_middle\"> <input type=\"radio\" name=\"alignment[$index]\" value=\"center\"  />Center</li>
                    <li id=\"alignment_right\"><input type=\"radio\" name=\"alignment[$index]\" value=\"right\"  />Right</li>
                </ul>
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Image Size: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"radio\" name=\"image_size[$index]\" value=\"thumbnail\" />Thumbnail (" . thumbnail_image_width() . " x " . thumbnail_image_height() . ")
                <input type=\"radio\" name=\"image_size[$index]\" value=\"medium\" checked=\"checked\" />Medium (" . medium_image_width() . " x " . medium_image_height() . ")
                <input type=\"radio\" name=\"image_size[$index]\" value=\"large\"  />Large (" . large_image_width() . " x " . large_image_height() . ")
                <input type=\"radio\" name=\"image_size[$index]\" value=\"original\"  />Original
                <input type=\"hidden\" name=\"thumbnail[$index]\" value=\"". HTTP . SITE_URL . $d['lattach_loc_thumb'] . "\" />
                <input type=\"hidden\" name=\"medium[$index]\" value=\"". HTTP . SITE_URL . $d['lattach_loc_medium'] . "\" />
                <input type=\"hidden\" name=\"large[$index]\" value=\"". HTTP . SITE_URL . $d['lattach_loc_large'] . "\" />
                <input type=\"hidden\" name=\"original[$index]\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Link Image to: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"link_to[$index]\" id=\"link_to_" . $d['lattach_id'] . "\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" /><br />
                <button type=\"button\" value=\"\" class=\"button\" id=\"link_none_" . $d['lattach_id'] . "\">None</button>
                <button type=\"button\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" class=\"button\" id=\"the_link_" . $d['lattach_id'] . "\">Link to Image</button><br />
                <input type=\"hidden\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" id=\"link_to_file_" . $d['lattach_id'] . "\" />Enter a link URL or click above for presets.
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <ul class=\"button_navigation\" style=\"margin:20px 0 20px 0px;text-align:right;\">
                <li>" . button( "button=delete&type=button&id=delete_" . $d['lattach_id'] . "&index=" . $index . "" ) . "&nbsp</li>
                <li>" . button( "button=insert&id=insert_" . $d['lattach_id'] . "&index=" . $index . "&name=insert[$index]" ) . "&nbsp</li>
                <li>" . button( "button=save_changes&id=save_changes_" . $d['lattach_id'] . "&index=" . $index . "&name=save_changes[$index]" ) . "</li>
            </ul>
        </div>
        <input type=\"hidden\" name=\"type[$index]\" value=\"" . $d['mime_type'] . "\" />
        <input type=\"hidden\" name=\"textarea_id[$index]\" value=\"" . $textarea_id . "\" />
        <input type=\"hidden\" name=\"attachment_id[$index]\" value=\"" . $d['lattach_id'] . "\" />";
    }
    else
    {
        $detail = "
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left_image\">";
                $detail .= attachment_icon( $d['mime_type'] );
                $detail .= "
            </div>
            <div class=\"media_field_right_image\">
                <strong>File Type : </strong>" . $d['mime_type'] . "<br /><br />
                <strong>Upload Date : </strong>" . date( get_date_format(), strtotime( $d['upload_date'] ) ) . "<br /><br />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Title: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"title[$index]\" value=\"" . $d['ltitle'] . "\" />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Caption: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"caption[$index]\" value=\"" . $d['lcaption'] . "\" />
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Description: </strong>
            </div>
            <div class=\"media_field_right\">
                <textarea  name=\"alt_text[$index]\" rows=\"5\" />" . $d['lalt_text'] . "</textarea>
            </div>
        </div>
        <div class=\"details_attachment_set clearfix\">
            <div class=\"media_field_left\">
                <strong>Link File to: </strong>
            </div>
            <div class=\"media_field_right\">
                <input type=\"text\" class=\"textbox\" name=\"link_to[$index]\" id=\"link_to_" . $d['lattach_id'] . "\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" /><br />
                <button type=\"button\" value=\"\" class=\"button\" id=\"link_none_" . $d['lattach_id'] . "\">None</button>
                <button type=\"button\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" class=\"button\" id=\"the_link_" . $d['lattach_id'] . "\">Link to Image</button><br />
                <input type=\"hidden\" value=\"". HTTP . SITE_URL . $d['lattach_loc'] . "\" id=\"link_to_file_" . $d['lattach_id'] . "\" />
            </div>
        </div>";

        if( $d['mime_type'] == 'application/pdf' )
        {
            $detail .= "
            <div class=\"details_attachment_set clearfix\">
                <div class=\"media_field_left\">
                    <strong>Thumb Pdf: </strong>
                </div>
                <div class=\"media_field_right\">
                    <form id=\"mini_form[$index]\" method=\"post\" enctype=\"multipart/form-data\" action=\"". HTTP . SITE_URL . "/the-upload-thumb-pdf-file-ajax/\" >
                        <input type=\"hidden\" name=\"id_article\" val=" . $d['lattach_id'] . " />
                        <input type=\"file\" name=\"thumb_pdf\" id=\"upload-thumb-pdf_$index\" rel=" . $d['lattach_id'] . " />
                    </form
                </div>
            </div>
            </div>";
        }

        $detail .= "
        <div class=\"details_attachment_set clearfix\">
            <ul class=\"button_navigation\" style=\"margin:20px 0 20px 0px;text-align:right;\">
                <li>" . button( "button=delete&type=button&id=delete_" . $d['lattach_id'] . "&index=" . $index . "" ) . "&nbsp</li>
                <li>" . button( "button=insert&type=button&id=insert_" . $d['lattach_id'] . "&index=" . $index . "&name=insert[$index]" ) . "&nbsp </li>
                <li>" . button( "button=save_changes&id=save_changes_" . $d['lattach_id'] . "&index=" . $index . "&name=save_changes[$index]" ) . "</li>
            </ul>
        </div>
        <input type=\"hidden\" name=\"type[$index]\" value=\"" . $d['mime_type'] . "\" />
        <input type=\"hidden\" name=\"textarea_id[$index]\" value=\"" . $textarea_id . "\" />
        <input type=\"hidden\" name=\"attachment_id[$index]\" value=\"" . $d['lattach_id'] . "\" />";
    }

    $detail .= "
    <script type=\"text/javascript\" language=\"javascript\">

        /* LINK TO FILE */
        $(function(){
            $('#the_link_" . $d['lattach_id'] . "').click(function(){
                $('#link_to_" . $d['lattach_id'] . "').val($('#link_to_file_" . $d['lattach_id'] . "').val());
            });
         });

         /* LINK TO NONE */
         $(function(){
            $('#link_none_" . $d['lattach_id'] . "').click(function(){
                 $('#link_to_" . $d['lattach_id'] . "').val('');
            });
         });

         /* INSERT INTO ARTICLE */
         $(function(){
            $('input[name=insert[" . $index . "]]').click(function(){
                var type        = $('input[name=type[" . $index . "]]').val();
                var textarea_id = $('input[name=textarea_id[" . $index . "]]').val();
                var link_to     = $('input[name=link_to[" . $index . "]]').val();
                var title_text  = $('input[name=title[" . $index . "]]').val();
                var caption     = $('input[name=caption[" . $index . "]]').val();
                var order       = $('input[name=order[" . $index . "]]').val();

                if( title_text.length!=0 )
                {
                    title='title=\"'+title_text+'\"';
                }
                else
                {
                    title='';
                }

                if( type=='image/jpg'|| type=='image/jpeg' || type=='image/pjpeg' || type=='image/gif' || type=='image/png' )
                {
                    var image_size = $('input[name=image_size[" . $index . "]]:checked').val();

                    if( image_size=='thumbnail' )
                    {
                        var src = $('input[name=thumbnail[" . $index . "]]').val();
                    }
                    else if( image_size=='medium' )
                    {
                        var src = $('input[name=medium[" . $index . "]]').val();
                    }
                    else if( image_size=='large' )
                    {
                        var src = $('input[name=large[" . $index . "]]').val();
                    }
                    else if( image_size=='original' )
                    {
                        var src = $('input[name=original[" . $index . "]]').val();
                    }

                    var alignment = $('input[name=alignment[" . $index . "]]:checked').val();
                    var alt_text  = $('input[name=alt_text[" . $index . "]]').val();

                    if( alt_text.length!=0 )
                    {
                        alt = 'alt=\"'+alt_text+'\"';
                    }
                    else
                    {
                        alt = '';
                    }

                    if( alignment!='center' )
                    {
                        the_float      = 'float:'+alignment;
                        the_center_div = '';
                        end_center_div = '';
                    }
                    else
                    {
                        the_center_div = \"<p style='text-align:center;'>\";
                        the_float      = '';
                        end_center_div = \"</p>\";
                    }

                    if( link_to.length!=0 )
                    {
                        the_link = '<a href=\"'+src+'\" '+title+'>';
                        end_link = \"</a>\";

                    }
                    else
                    {
                        the_link = '';
                        end_link = '';
                    }

                    the_content  = the_center_div;
                    the_content += the_link;
                    the_content += '<img src=\"'+src+'\" '+alt+' '+title+' style=\"'+the_float+'\" />';
                    the_content += end_link;
                    the_content += end_center_div;
                }
                else if( type=='video/mp4' )
                {
                    the_content = '<iframe class=\"iframe-block\" src=\"'+link_to+'\" frameborder=\"0\" allowfullscreen></iframe>';
                }
                else
                {
                    the_content = '';

                    src = link_to;   

                    var alt_text = $('textarea[name=alt_text[" . $index . "]]').val();

                    if( link_to.length!=0 )
                    {
                        the_link = '<a href=\"'+src+'\" '+title+'>';
                        end_link = \"</a>\";
                    }
                    else
                    {
                        the_link = '';
                        end_link = '';
                    }

                    the_content += the_link;
                    the_content += title_text;
                    the_content += end_link;
                }

                $.post('upload-media.php', 'insert=true&attachment_id=" . $d['lattach_id'] . "&title='+title_text+'&alt_text='+alt_text+'&caption='+caption+'&order='+order);
                
                top.tinymce.activeEditor.execCommand('mceInsertContent', false, the_content);
                top.tinymce.activeEditor.windowManager.close();

                return false;
            });
        });

        /* SAVE CHANGES */
        $(function(){
            $('#save_changes_" . $d['lattach_id'] . "').click(function(){
                var type       = $('input[name=type[" . $index . "]]').val();
                var title_text = $('input[name=title[" . $index . "]]').val();
                var caption    = $('input[name=caption[" . $index . "]]').val();
                var order       = $('input[name=order[" . $index . "]]').val();                

                if( type=='image/jpg'|| type=='image/jpeg' || type=='image/pjpeg' || type=='image/gif' || type=='image/png' )
                {
                    var alt_text= $('input[name=alt_text[" . $index . "]]').val();
                }
                else
                {
                    var alt_text= $('textarea[name=alt_text[" . $index . "]]').val();
                }

                $.post('upload-media.php', 'save_changes=save_item&attachment_id=" . $d['lattach_id'] . "&title='+title_text+'&alt_text='+alt_text+'&caption='+caption+'&order='+order,function(data){
                    $('#response').html(data);
                });

                $('#show_attach_" . $d['lattach_id'] . "').text('Show');";

                if( $tab == "from-computer" )
                {
                    $detail .= "$('#upload_image_detail').slideUp();";
                }
                else
                {
                    $detail .= "$('#detail_attach_" . $d['lattach_id'] . "').slideUp();";
                }

                $detail .= "
                $('#response').slideDown(500);
                $('#response').delay(3000);
                $('#response').slideUp(500);

                return false;
            });
        });

         /* INSERT INTO ARTICLE */
         $(function(){
            jQuery(\"#upload-thumb-pdf_$index\").change(function(){
                 jQuery('#mini_form[$index]').submit();
            });

            jQuery('#mini_form[$index]').submit(function(){
                var formData = $(this).serialize();

                //-- You should sterilise the file names
                $.each(data.files, function(key, value){
                    formData = formData + 'filenames[]=' + value;
                });
            });
         });
    </script>";

    return $detail;
}

function get_load_more_js_media()
{
    $type = $_GET['tab'];
    $url  = get_attachment_tab_url( $_GET['tab'] );    
    $js   = "
    <script type=\"text/javascript\">
        $(function(){
            $('.loadMore').live(\"click\",function(){
                var PAGE = $(this).attr(\"id\");
                var TYPE = \"$type\";

                if( PAGE )
                {
                    $(\"#loadMore\"+PAGE).html('<img src=\"". HTTP . TEMPLATE_URL . "/images/loading.gif\" border=\"0\">');        

                    $.ajax({
                        type: \"POST\",
                        url: \"" . $url . "\",
                        data: \"page=\"+ PAGE + \"&loadMore=\" + TYPE,
                        cache: false,
                        success: function(html){
                            $(\"#media_gallery\").append(html);
                            $(\"#loadMore\"+PAGE).remove(); // removing old more button
                        }
                    });
                }
                else
                {
                    $(\".morebox\").html('The End');// no results
                }

                return false;
            });
        });
    </script>";

    return $js;
}

function get_load_more_link_media( $pageID = '' )
{
    $link = '
    <div id="loadMore' . $pageID . '" class="morebox" style="width: 98.8% !important;">
        <a href="#" class="loadMore" id="' . $pageID . '" rel="1">Load more...</a>
    </div>';

    return $link;
}

?>