<?php

/*
Title: Aruna
Preview: preview.png
Author: Wahya Biantara
Author Url: http://wahya.biantara.com
Description: Default themes since March 2010
*/

if( !is_user_logged() )
{
    if( is_login_form() )
    {
        $http = empty( $_SERVER["HTTPS"] ) ? 'http://' : 'https://';

        if( $http.site_url().'/lumonata-admin/' != cur_pageURL() && !isset( $_GET['redirect'] ) )
        {
            header( 'location:'.$http.site_url().'/lumonata-admin/' );
        }

        get_login_form();
    }
    elseif( is_register_form() )
    {
        signup_user();
    }
    elseif( is_thanks_page() )
    {
        resendPassword();
    }
    elseif( is_verify_account() )
    {
        verify_account();
    }
    elseif( is_forget_password() )
    {
        get_forget_password_form();
    }
    else
    {
        header( 'location:'.get_admin_url().'/?state=login&redirect='.base64_encode( cur_pageURL() ) );
    }
}
else
{
    $http = empty( $_SERVER["HTTPS"] ) ? 'http://' : 'https://';
    
    if( empty( $_GET['state'] ) )
    {
        header( 'location:'.get_admin_url().'/?state=dashboard' );
    }

    set_template( TEMPLATE_PATH."/index.html" );
    add_block( 'adminArea', 'aBlock' );

    add_variable( 'site_url', $http.site_url() );
    add_variable( 'web_title', web_title() );
    add_variable( 'avatar', get_avatar( $_COOKIE['user_id'], 2 ) );
    add_variable( 'logout_link', get_state_url( 'logout' ) );

    if( is_administrator() )
    {
        add_variable( 'profile_link', get_state_url( 'users' ).'&tab=my-profile' );
    }
    else
    {
        add_variable( 'profile_link', get_state_url( 'my-profile' ).'&tab=my-profile' );
    }

    if( is_administrator() )
    {
        add_variable( 'profile_updates', get_state_url( 'users' ).'&tab=my-updates' );
    }
    else
    {
        add_variable( 'profile_updates', get_state_url( 'my-profile' ).'&tab=my-updates' );
    }

    $d = fetch_user( $_COOKIE['user_id'] );

    add_variable( 'displayname', $d['ldisplay_name'] );

    /*
	| -------------------------------------------------------------------------------------
	| Setting up the header elements. CSS, Javascript, JQuery And the others
	| -------------------------------------------------------------------------------------
	*/
    $settings_menu   = get_admin_menu( 'settings_menu' );
    $connection_menu = get_admin_menu( 'connection_menu' );

    //-- Get The Content Area
    add_variable( 'content_area', $thecontent );

    //-- Setting Up Navigation Menu
    add_variable( 'navmenu', get_admin_menu() );

    //-- Setting Up Settings Menu
    add_variable( 'settings_menu', $settings_menu );
    add_variable( 'settings_menu_class', empty( $settings_menu ) ? 'hidden' : '' );

    //-- Setting Up Connection Menu
    add_variable( 'connection_menu', $connection_menu );
    add_variable( 'connection_menu_class', empty( $connection_menu ) ? 'hidden' : '' );

    //-- Add Top Search
    add_variable( 'top_search_box', top_search_box() );

    //-- Get CSS
    add_actions( 'header_elements', 'get_css' );

    //-- Get the CSS For Dropzone
    add_actions( 'header_elements', 'get_css_inc', 'dropzone/dropzone.min.css' );

    //-- Get the CSS For Chosen
    add_actions( 'header_elements', 'get_css_inc', 'chosen/chosen.min.css' );

    //-- Get the CSS For Timepicker
    add_actions( 'header_elements', 'get_css_inc', 'timepicker/jquery.timepicker.css' );

    //-- Get jQuery UI
    add_actions( 'header_elements', 'get_javascript', 'jquery_ui' );

    //-- Get jQuery For Navigation
    add_actions( 'header_elements', 'get_javascript', 'navigation' );

    //-- Get jQuery Custom jQuery UI
    add_actions( 'header_elements', 'get_javascript_inc', 'dropzone/jquery-ui-1.8.2.custom.min.js' );

    //-- Get jQuery Nested Sortable
    add_actions( 'header_elements', 'get_javascript_inc', 'dropzone/jquery.mjs.nestedSortable.js' );

    //-- Get jQuery Dropzone
    add_actions( 'header_elements', 'get_javascript_inc', 'dropzone/dropzone.min.js' );

    //-- Get jQuery Chosen
    add_actions( 'header_elements', 'get_javascript_inc', 'chosen/chosen.jquery.min.js' );

    //-- Get jQuery Chosen
    add_actions( 'header_elements', 'get_javascript_inc', 'chosen/chosen.order.jquery.min.js' );

    //-- Get jQuery For Tinymce
    add_actions( 'header_elements', 'get_javascript_inc', 'tiny_mce/tinymce.min.js' );

    //-- Get jQuery For Tinymce Config
    add_actions( 'header_elements', 'get_javascript', 'tiny_mce' );

    //-- Get jQuery For Form Validation
    add_actions( 'header_elements', 'get_javascript', 'form_validation' );

    //-- Get jQuery For Taxonomy Function
    add_actions( 'header_elements', 'get_javascript', 'taxonomy' );

    //-- Get jQuery For Timepicker
    add_actions( 'header_elements', 'get_javascript_inc', 'timepicker/jquery.timepicker.js' );

    //-- Get jQuery For Main
    add_actions( 'header_elements', 'get_javascript', 'main.js' );
    
    //-- Get jQuery
    add_variable( 'jquery', get_javascript( 'jquery' ) );
    

    //-- Attempt the action that already add in the whole script
    add_variable( 'header_elements', attemp_actions( 'header_elements' ) );
    add_variable( 'section_title', attemp_actions( 'section_title' ) );
    add_variable( 'admin_tail', attemp_actions( 'admin_tail' ) );
    add_variable( 'admin_tail_language', attemp_actions( 'admin_tail_language' ) );
    add_variable( 'footer_elements', attemp_actions( 'footer_elements' ) );

    parse_template( 'adminArea', 'aBlock' );
    print_template();
}

?>