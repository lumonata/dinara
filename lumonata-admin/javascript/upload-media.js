jQuery(function(){

    jQuery('input[name=cancel]').click(function(){
        top.tinymce.activeEditor.windowManager.close();
    });

    jQuery('input[name=url]').keyup(function(){
        jQuery('input[name=link_to]').val(jQuery('input[name=url]').val());
        jQuery('#link_to_file').val(jQuery('input[name=url]').val());
    });

    jQuery('input[name=url]').blur(function(){
        jQuery('input[name=link_to]').val(jQuery('input[name=url]').val());
        jQuery('#link_to_file').val(jQuery('input[name=url]').val());
    });

    jQuery('#my_link').click(function(){
        jQuery('#link_to').val(jQuery('#link_to_file').val());
    });

    jQuery('#link_none').click(function(){
        jQuery('#link_to').val('');
    });

    jQuery('input[name=insert]').click(function(){
        var type = jQuery('input[name=type]').val();
        var textarea_id =jQuery('input[name=textarea_id]').val();
        var link_to = jQuery('input[name=link_to]').val();
        var title = jQuery('input[name=title]').val();
        var src = jQuery('input[name=url]').val();

        if(type=='image')
        {
            var alt_text  = jQuery('input[name=alt_text]').val();
            var caption   = jQuery('input[name=caption]').val();
            var alignment = jQuery('input[name=alignment]:checked').val();

            alt   = (alt_text.length!=0 ? 'alt="'+alt_text+'"' : '') ;
            title = (title.length!=0 ? 'title="'+title+'"' : '');

            if(alignment!='center')
            {
                the_float      = 'float:'+alignment;
                the_center_div = '';
                end_center_div = '';
            }
            else
            {
                the_center_div = '<p style="text-align:center;">';
                the_float      = '';
                end_center_div = '</p>';
            }

            if(link_to.length!=0)
            {
                the_link = '<a href="'+link_to+'">';
                end_link = '</a>';
            }
            else
            {
                the_link = '';
                end_link = '';
            }

            the_content  = the_center_div;
            the_content += the_link;
            the_content += '<img src="'+src+'" '+alt+' '+title+' style="'+the_float+'" />';
            the_content += end_link;
            the_content += end_center_div;
        }
        else if(type=='video')
        {
            the_content = '<iframe class="iframe-block" src="'+link_to+'" frameborder="0" allowfullscreen></iframe>';
        }
        else
        {
            the_content = '';
            src = link_to;

            if(link_to.length!=0)
            {
                the_link = '<a href="'+src+'">';
                end_link = '</a>';
            }
            else
            {
                the_link = '';
                end_link = '';
            }

            the_content += the_link;
            the_content += title;
            the_content += end_link;
        }

        top.tinymce.activeEditor.execCommand('mceInsertContent', false, the_content);
        top.tinymce.activeEditor.windowManager.close();
    });

    //-- Reset to unchecked
    jQuery('input[name=select_all]').removeAttr('checked');
    jQuery('input[name=select[]]').each(function(){
        jQuery('input[name=select[]]').removeAttr('checked');
    });

    jQuery('input[name=select_all]').click(function(){
        var checked_status = this.checked;

        jQuery('.select').each(function(){
            this.checked = checked_status;

            if(checked_status)
            {
                jQuery('input[name=delete]').removeClass('btn_delete_disable');
                jQuery('input[name=delete]').addClass('btn_delete_enable');
                jQuery('input[name=delete]').removeAttr('disabled');
            }
            else
            {
                jQuery('input[name=delete]').removeClass('btn_delete_enable');
                jQuery('input[name=delete]').addClass('btn_delete_disable');
                jQuery('input[name=delete]').attr('disabled', 'disabled');
            }
        });
    });

    jQuery('.select').live('click', function(){
        //-- Count how many checkbox are checked, 
        //-- if more then 0 than enable the edit and delete button
        if(jQuery('.select:checked').length > 0)
        {
            jQuery('input[name=delete]').removeClass('btn_delete_disable');
            jQuery('input[name=delete]').addClass('btn_delete_enable');
            jQuery('input[name=delete]').removeAttr('disabled');
        }
        else
        {
            jQuery('input[name=delete]').removeClass('btn_delete_enable');
            jQuery('input[name=delete]').addClass('btn_delete_disable');
            jQuery('input[name=delete]').attr('disabled', 'disabled');
        }
    });
    
});

