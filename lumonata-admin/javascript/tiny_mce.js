function initTinyMce( selector )
{
    var site_dir = jQuery('#site_url').val();

    tinymce.init({
        selector: selector,
        height: 500,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | table styleselect | bold underline italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link unlink | print preview | forecolor backcolor emoticons | codesample code',
        // toolbar2: 'up_images up_docs up_pdf up_music up_swf up_videos',
        toolbar2: 'up_images up_videos',
        table_class_list: [
            {title: 'None', value: ''},
            {title: 'Price List', value: 'price-list-table'},
            {title: 'Price List Note', value: 'price-list-note-table'}
        ],
        link_class_list: [
            {title: 'None', value: ''},
            {title: 'Download PDF Style', value: 'download-pdf-file'}
        ],
        image_advtab: true,
        content_css: [
            '../lumonata-admin/textarea.css',
        ],
        // enable title field in the Image dialog
        image_title: true, 
        // enable automatic uploads of images represented by blob or data URIs
        automatic_uploads: true,
        // URL of our upload handler (for more details check: https://www.tinymce.com/docs/configure/file-image-upload/#images_upload_url)
        /*images_upload_url: site_dir+'/postAcceptor.php',
        images_upload_base_path: site_dir,
        // here we add custom filepicker only to Image dialog
        file_picker_types: 'image', 
        // and here's our custom image picker
        file_picker_callback: function(cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            
            // Note: In modern browsers input[type="file"] is functional without 
            // even adding it to the DOM, but that might not be the case in some older
            // or quirky browsers like IE, so you might want to add it to the DOM
            // just in case, and visually hide it. And do not forget do remove it
            // once you do not need it anymore.

            input.onchange = function() {
              var file = this.files[0];
              
              var reader = new FileReader();
              reader.readAsDataURL(file);
              reader.onload = function () {
                // Note: Now we need to register the blob in TinyMCEs image blob
                // registry. In the next release this part hopefully won't be
                // necessary, as we are looking to handle it internally.
                var id = 'tinymce' + (new Date()).getTime();
                var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
                var base64 = reader.result.split(',')[1];
                var blobInfo = blobCache.create(id, file, base64);
                blobCache.add(blobInfo);

                // call the callback and populate the Title field with the file name
                cb(blobInfo.blobUri(), { title: file.name });
              };
            };
            
            input.click();
        }*/
        setup: function (ed) {
            var post_id = jQuery('.post-id-field').val();

            ed.addButton('up_images', {
                tooltip: 'Attach Custom Images',
                image : '../lumonata-admin/themes/default/images/ico-upload-images.svg',
                onclick: function (ele) {
                    ed.windowManager.open({
                        title: 'Attach Custom Images',
                        url: '../lumonata-admin/upload-media.php?tab=from-computer&type=image&textarea_id='+ed.id+'&post_id='+post_id,
                        width: 800,
                        height: 720
                    });
                }
            });

            /*ed.addButton('up_docs', {
                tooltip: 'Attach Custom Documents',
                image : '../lumonata-admin/themes/default/images/ico-upload-doc.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Documents',
                        url: '../lumonata-admin/upload-media.php?tab=from-computer&type=doc&textarea_id='+ed.id+'&post_id='+post_id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_pdf', {
                tooltip: 'Attach Custom PDF',
                image : '../lumonata-admin/themes/default/images/ico-upload-pdf.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom PDF',
                        url: '../lumonata-admin/upload-media.php?tab=from-computer&type=pdf&textarea_id='+ed.id+'&post_id='+post_id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_music', {
                tooltip: 'Attach Custom Music File',
                image : '../lumonata-admin/themes/default/images/ico-upload-music.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Music File',
                        url: '../lumonata-admin/upload-media.php?tab=from-computer&type=music&textarea_id='+ed.id+'&post_id='+post_id,
                        width: 800,
                        height: 720
                    });
                }
            });

            ed.addButton('up_swf', {
                tooltip: 'Attach Custom Flash File',
                image : '../lumonata-admin/themes/default/images/ico-upload-flash.svg',
                onclick: function () {
                    ed.windowManager.open({
                        title: 'Attach Custom Flash File',
                        url: '../lumonata-admin/upload-media.php?tab=from-computer&type=flash&textarea_id='+ed.id+'&post_id='+post_id,
                        width: 800,
                        height: 720
                    });
                }
            });*/


            // ed.onKeyUp.add(function(ed, e) {
            //     //showPreview ( $('.mceContentBody').val() );
            //     showPreview (tinyMCE.activeEditor.getContent({format : 'raw'}) );
      
            // });
        }
    });
}

jQuery(function() {
    if( typeof set_mlanguage_forclone === 'undefined' )
    {
        initTinyMce( '.tinymce' );
    }
});

jQuery(function() {
    jQuery('.visual_view_button').attr('disabled', 'disabled');
    jQuery('.visual_view_button').removeClass('visual_view_button').addClass('visual_view_button_selected');
});

jQuery(function() {
    jQuery('.visual_view_button_selected').click(function() {
        var id = this.id.replace('visual_view_', '');

        jQuery('#html_view_' + id).removeClass('html_view_button_selected').addClass('html_view_button');
        jQuery('#html_view_' + id).removeAttr('disabled');
        jQuery('#visual_view_' + id).attr('disabled', 'disabled');
        jQuery('#visual_view_' + id).removeClass('visual_view_button').addClass('visual_view_button_selected');

        return false;
    });
});

jQuery(function() {
    jQuery('.html_view_button').click(function() {
        var id = this.id.replace('html_view_', '');

        jQuery('#visual_view_' + id).removeClass('visual_view_button_selected').addClass('visual_view_button');
        jQuery('#visual_view_' + id).removeAttr('disabled');
        jQuery('#html_view_' + id).attr('disabled', 'disabled');
        jQuery('#html_view_' + id).removeClass('html_view_button').addClass('html_view_button_selected');

        return false;
    });
});