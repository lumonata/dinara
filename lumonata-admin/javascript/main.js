jQuery(document).ready(function (e) {
    button_language();
    button_language_menus();
});

function button_language(){
    jQuery('.button-language ul li').click(function(){
        jQuery('.button-language ul li').removeClass("active");
        jQuery(this).addClass("active");

        const val = jQuery(this).find('a.btn').attr("data-filter");

        jQuery('.content-lang').removeClass('active');
        jQuery('.'+val).addClass('active');

        return false;
    });
}

function button_language_menus(){
    jQuery('#list-box .button-language-menu ul li').click(function(){
        jQuery('#list-box .button-language-menu ul li').removeClass("active");
        jQuery(this).addClass("active");

        const val = jQuery(this).find('a.btn').attr("data-filter");
        const res = val.toLowerCase();

        jQuery('#list-box .content-lang-menu').removeClass('active');
        jQuery('#list-box .content-menu-'+res).addClass('active');

        return false;
    });

    jQuery('#category-box .button-language-menu ul li').click(function(){
        jQuery('#category-box .button-language-menu ul li').removeClass("active");
        jQuery(this).addClass("active");

        const val = jQuery(this).find('a.btn').attr("data-filter");
        const res = val.toLowerCase();

        jQuery('#category-box .content-lang-menu').removeClass('active');
        jQuery('#category-box .content-menu-'+res).addClass('active');

        return false;
    });
}