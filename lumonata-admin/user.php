<?php

    require_once('../lumonata_config.php');
    require_once('../lumonata_settings.php');
    require_once('../lumonata-functions/settings.php');
    require_once('../lumonata-classes/actions.php');
    require_once('../lumonata-functions/upload.php');
    require_once('../lumonata-functions/attachment.php');
    require_once('../lumonata-classes/directory.php');
    require_once('../lumonata-functions/friends.php');
    require_once('../lumonata-functions/user.php');
    require_once('../lumonata-functions/paging.php');
    require_once('../lumonata-classes/user_privileges.php');
    require_once('admin_functions.php');
    
    if(!defined('SITE_URL'))
    {
		define('SITE_URL',get_meta_data('site_url'));
    }
    
    if(!is_user_logged())
    {
	    header('location:'.get_admin_url().'/?state=login');
    }
    else
    {
        if(is_delete('users'))
        {
            if(!delete_user($_POST['id']))
            {
                echo '<div class="alert_red_form">Deleting process failed.</div>';
            }
        }
        elseif(is_search())
        {
            $s = 'SELECT * FROM lumonata_users WHERE lusername LIKE %s OR ldisplay_name LIKE %s OR lemail=%s';
    	    $q = $db->prepare_query($s,'%'.$_POST['s'].'%','%'.$_POST['s'].'%',$_POST['s']);
    	    $r = $db->do_query($q);

            if($db->num_rows($r) > 0)
            {
                echo users_list($r);
            }
            else
            {
                echo '
                <div class="alert_yellow_form">
                    No result found for <em>'.$_POST['s'].'</em>. 
                    Check your spellling or try another terms
                </div>';
            }
    	}
        elseif(is_ajax_request())
        {
            extract($_POST);

            if( $ajax_key=='manage_user_privileges' )
            {
                if( manage_user_privileges($_POST) )
                {
                    $arr = array();

                    $s = 'SELECT * FROM lumonata_meta_data WHERE lapp_name=%s';
                    $q = $db->prepare_query($s, 'user_privileges');
                    $r = $db->do_query($q);

                    while( $d=$db->fetch_array($r) )
                    {
                        $arr[$d['lmeta_name']] = json_decode($d['lmeta_value'], true);
                    }

                    $res['result'] = 'success';
                    $res['data']   = $arr;

                    echo json_encode($res);
                }
                else
                {
                    echo '{"result":"failed"}';
                }
            }
            elseif( $ajax_key=='get_user_privileges' )
            {
                $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
                $q = $db->prepare_query($s, $_POST['type_key'], 'user_privileges');
                $r = $db->do_query($q);

                if( $db->num_rows($r) > 0 )
                {
                    $d = $db->fetch_array($r);
                    $v = json_decode($d['lmeta_value'], true);

                    $res['result'] = 'success';
                    $res['data']   = array(
                        'key'=>$d['lmeta_name'], 
                        'name'=>( isset($v['name']) ? $v['name'] : '' ), 
                        'privileges'=>( isset($v['privileges']) ? $v['privileges'] : '' )
                    );

                    echo json_encode($res);
                }
                else
                {
                    echo '{"result":"failed"}';
                }
            }
            elseif( $ajax_key=='get_user_type' )
            {
                $s = 'SELECT * FROM lumonata_meta_data WHERE lapp_name=%s';
                $q = $db->prepare_query($s, 'user_privileges');
                $r = $db->do_query($q);

                while( $d=$db->fetch_array($r) )
                {
                    $arr[$d['lmeta_name']] = json_decode($d['lmeta_value'], true);
                }    
            }
        }
    }
    
?>   