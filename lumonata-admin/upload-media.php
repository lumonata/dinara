<?php

require_once( '../lumonata_config.php' );
require_once( '../lumonata_settings.php' );
require_once( '../lumonata-functions/settings.php' );
require_once( '../lumonata-classes/actions.php' );
require_once( '../lumonata-functions/upload.php' );
require_once( '../lumonata-functions/attachment.php' );
require_once( '../lumonata-classes/directory.php' );
require_once( '../lumonata-functions/user.php' );
require_once( '../lumonata-functions/paging.php' );

if( !defined( 'SITE_URL' ) )
{
    define( 'SITE_URL', get_meta_data( 'site_url' ) );
}

/*
Check is the user is logged or not
If user is not logged yet, then redirect user to the login form
*/
if( !is_user_logged() )
{
    header( 'location:' . get_admin_url() . '/?state=login' );
}
else
{
    if( !defined( 'TEMPLATE_PATH' ) );
    {
        define( 'TEMPLATE_PATH', ROOT_PATH . '/lumonata-admin/' );
    }
    
    require_once( '../lumonata-functions/template.php' );
    
    $theme = get_meta_data( 'admin_theme', 'themes' );
    
    if( !defined( 'TEMPLATE_URL' ) )
    {
        define( 'TEMPLATE_URL', SITE_URL . '/lumonata-admin/themes/' . $theme );
    }
    
    if( !defined( 'FILES_PATH' ) )
    {
        define( 'FILES_PATH', ROOT_PATH . '/lumonata-content/files' );
    }

    $ssl_option = get_meta_data('ssl_option');
    
    define('HTTP',($ssl_option==1?'https://':'http://')); 
    
    if( isset( $_GET['post_id'] ) )
    {
        $file_name     = '';
        $count_gallery = count_attachment( $_GET['post_id'], $_GET['type'] );
        $count_lib     = count_attachment( 0, $_GET['type'] );
        
        $tab = set_attachment_tab( array(
            'from-computer' => 'From Computer',
            'from-url' => 'From URL',
            'gallery' => 'Gallery (' . $count_gallery . ')',
            'library' => 'Library (' . $count_lib . ')' 
        ) );
        
        set_template( TEMPLATE_PATH . 'upload-media.html', 'uploadMedia' );
        
        add_block( 'uploadMediaBlock', 'upMediaBlock', 'uploadMedia' );
        add_block( 'imageMediaURLBlock', 'iMediaBlock', 'uploadMedia' );
        add_block( 'otherMediaURLBlock', 'oMediaBlock', 'uploadMedia' );
        add_block( 'galleryMediaBlock', 'gMediaBlock', 'uploadMedia' );
        
        add_actions( 'header_elements', 'get_javascript', 'jquery' );
        add_actions( 'header_elements', 'get_javascript', 'upload-media' );
        add_actions( 'header_elements', 'get_javascript', 'jquery.filedrop' );
        add_actions( 'header_elements', 'get_javascript', 'upload-media-dragdrop' );
        
        add_variable( 'tabs', $tab );
        add_variable( 'textarea_id', $_GET['textarea_id'] );
        add_variable( 'css', HTTP . TEMPLATE_URL . '/css/style.css' );
        add_variable( 'dragdropimage', dragdropimage( $_GET['type'], $_GET['post_id'], $_GET['textarea_id'] ) );
    }
    
    //-- Sort order the gallery
    if( isset( $_POST['update_media_order'] ) )
    {
        update_attachment_order( $_POST['attachment'], $_POST['start'] );
    }
    elseif( isset( $_POST['confirm_delete'] ) )
    {
        if( $_POST['confirm_delete'] == 'yes' )
        {
            delete_attachment( $_POST['delete_id'] );
        }
    }
    elseif( isset( $_POST['save_changes'] ) && $_POST['save_changes'] == 'save_item' )
    {
        if( edit_attachment( $_POST['attachment_id'], $_POST['title'], $_POST['order'], $_POST['alt_text'], $_POST['caption'] ) )
        {
            echo '<div class="alert_green">' . UPDATE_SUCCESS . '</div>';
        }
    }
    elseif( isset( $_POST['insert'] ) && !is_array( $_POST['insert'] ) )
    {
        edit_attachment( $_POST['attachment_id'], $_POST['title'], $_POST['order'], $_POST['alt_text'], $_POST['caption'] );
    }
    elseif( isset( $_POST['s'] ) && isset( $_POST['tab'] ) )
    {
        echo search_attachment_results( $_POST['s'], $_POST['tab'], $_POST['article_id'], $_POST['textarea_id'] );
    }
    else
    {
        if( array_key_exists( 'pic', $_FILES ) && $_FILES['pic']['error'] == 0 )
        {
            //-- File uploaded from user computer and then 
            //-- save it into database IF upload drag drop
            $pic = $_FILES['pic'];
            
            $file_name   = $pic['name'];
            $file_size   = $pic['size'];
            $file_type   = $pic['type'];
            $file_source = $pic['tmp_name'];
            
            if( is_allow_file_type( 'image/' . get_extension( $file_name ), "image" ) )
            {
                if( is_allow_file_size( $file_size ) )
                {
                    $post_id     = $_POST['post_id'];
                    $textarea_id = $_POST['textarea_id'];
                    
                    if( upload_image_attachment( $file_source, 'image/' . get_extension( $file_name ), $file_name, $post_id ) )
                    {
                        $article_id = $post_id;
                        $attach_id  = mysql_insert_id();
                        
                        $i = $db->num_rows( $db->do_query( $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id=%d', $article_id ) ) );
                        $q = $db->prepare_query( 'SELECT * FROM lumonata_attachment WHERE larticle_id=%d ORDER BY lattach_id DESC LIMIT 1', $article_id );
                        $r = $db->do_query( $q );
                        
                        $attch = gallery_items( $r, 'from-computer', $textarea_id, $i );
                        exit_status( $attch );
                    }
                    else
                    {
                        exit_status( '<div class="alert_red_form" style="width:93%;">Something went wrong with your upload!</div>', 'error' );
                    }
                }
                else
                {
                    exit_status( '<div class="alert_red_form" style="width:93%;">The uploaded file exceeds the <code>upload_max_filesize</code> directive in <code>php.ini</code>.</div>', 'error' );
                }
            }
            else
            {
                exit_status( '<div class="alert_red_form" style="width:93%;">File type not allowed</div>', 'error' );
            }
        }
        elseif( isset( $_GET['tab'] ) && $_GET['tab'] == 'from-computer' )
        {
            //-- File uploaded from user computer and then 
            //-- save it into database
            if( $_GET['type'] == 'pdf' )
            {
                $thumb_pdf_html = '
					<br />
					<h3>Choose thumb for pdf file.</h3>
					<p><input type="file" name="thumb_pdf" /></p>
					<br/>';
                
                add_variable( 'thumb_pdf', $thumb_pdf_html );
            }
            
            //-- If upload button clicked
            if( isset( $_POST['upload'] ) )
            {
                //-- Create destination folder if folder is not exist yet
                if( !is_dir( FILES_PATH . '/' . upload_folder_name() ) )
                {
                    if( !create_dir( FILES_PATH . '/' . upload_folder_name() ) )
                    {
                        $alert_mess = '
                        	<div class="alert_red_form" style="width:93%;">
                        		Unable to create new folder <code>' . FILES_PATH . '/' . $folder_name . '</code>
                        	</div>';
                        
                        add_variable( 'alert', $alert_mess );
                    }
                }
                
                if( $_GET['type'] == 'pdf' )
                {
                    $s = 'SELECT * FROM lumonata_attachment WHERE (ltitle=%s OR mime_type=%s)  AND larticle_id=%d';
                    $q = $db->prepare_query( $s, 'thumb_pdf', 'application/pdf', $_GET['post_id'] );
                    $r = $db->do_query( $q );
                    $n = $db->num_rows( $r );
                    
                    if( $n > 0 )
                    {
                        while( $d = $db->fetch_array( $r ) )
                        {
                            $s2 = 'DELETE FROM lumonata_attachment WHERE lattach_id=%d';
                            $q2 = $db->prepare_query( $s2, $d['lattach_id'] );
                            $r2 = $db->do_query( $q2 );
                        }
                    }
                    
                    $ext        = explode( '.', $_FILES['thumb_pdf']['name'] );
                    $name_pdf   = explode( '.', $_FILES['media']['name'] );
                    $name_image = $_FILES['thumb_pdf']['name'];
                    $location   = FILES_PATH . '/' . upload_folder_name() . '/';
                    
                    move_uploaded_file( $_FILES['thumb_pdf']['tmp_name'], $location . $name_image );
                }
                
                $file_name   = $_FILES['media']['name'];
                $file_size   = $_FILES['media']['size'];
                $file_type   = $_FILES['media']['type'];
                $file_source = $_FILES['media']['tmp_name'];
                
                if( is_allow_file_type( $file_type, $_GET['type'] ) )
                {
                    if( is_allow_file_size( $file_size ) )
                    {
                        //-- If file type is Image
                        if( $_GET['type'] == 'image' )
                        {
                            if( upload_image_attachment( $file_source, $file_type, $file_name, $_GET['post_id'] ) )
                            {
                                $file        = attemp_actions( 'original_file_location' );
                                $large_file  = attemp_actions( 'large_file_location' );
                                $medium_file = attemp_actions( 'medium_file_location' );
                                $thumb_file  = attemp_actions( 'medium_file_location' );
                                
                                $default_title = file_name_filter( $file_name );
                                
                                $d = array(
                                     'lattach_id' => mysql_insert_id(),
                                    'larticle_id' => $_GET['post_id'],
                                    'lattach_loc' => $file,
                                    'lattach_loc_large' => $large_file,
                                    'lattach_loc_medium' => $medium_file,
                                    'lattach_loc_thumb' => $thumb_file,
                                    'ltitle' => $default_title,
                                    'lalt_text' => '',
                                    'lcaption' => '',
                                    'mime_type' => $file_type,
                                    'upload_date' => date( get_date_format(), time() ) 
                                );
                                
                                $bottom_action = '
									<script type="text/javascript">
										jQuery("#upload_image_detail").show("slow");
									</script>';
                                
                                add_variable( 'attachment_details', attachment_details( $d, 0, $_GET['textarea_id'], $_GET['tab'] ) );
                                add_actions( 'bottom_elements', $bottom_action );
                            }
                        }
                        else
                        {
                            if( upload_media_attachment( $file_source, $file_type, $file_name, $_GET['post_id'] ) )
                            {
                                //-- For thumb pdf
                                if( $_GET['type'] == 'pdf' && !empty( $_FILES['thumb_pdf'] ) )
                                {
                                    $thumb_file_type     = $_FILES['thumb_pdf']['type'];
                                    $thumb_file_location = FILES_LOCATION . '/' . upload_folder_name() . '/' . $name_image;
                                    
                                    insert_attachment( $_GET['post_id'], 'thumb_pdf', $thumb_file_type, $thumb_file_location );
                                }
                                
                                $file = attemp_actions( 'original_file_location' );
                                
                                $default_title = file_name_filter( $file_name );
                                
                                $d = array(
                                     'lattach_id' => mysql_insert_id(),
                                    'larticle_id' => $_GET['post_id'],
                                    'lattach_loc' => $file,
                                    'ltitle' => $default_title,
                                    'lalt_text' => '',
                                    'lcaption' => '',
                                    'mime_type' => $file_type,
                                    'upload_date' => date( get_date_format(), time() ) 
                                );
                                
                                $bottom_action = '
									<script type="text/javascript">
										jQuery("#upload_image_detail").show("slow");
									</script>';
                                
                                add_variable( 'attachment_details', attachment_details( $d, 0, $_GET['textarea_id'], $_GET['tab'] ) );
                                add_actions( 'bottom_elements', $bottom_action );
                            }
                        }
                        
                        $delete_mess = 'Are you sure want to delete <code>' . $default_title . '</code> from the gallery?';
                        add_variable( 'delete_box', delete_confirmation_box( mysql_insert_id(), $delete_mess, 'upload-media.php', 'upload_image_detail' ) );
                    }
                    else
                    {
                        add_variable( 'alert', '<div class="alert_red_form" style="width:93%;">The uploaded file exceeds the <code>upload_max_filesize</code> directive in <code>php.ini</code>.</div>' );
                    }
                }
                else
                {
                    add_variable( 'alert', '<div class="alert_red_form" style="width:93%;">File type not allowed</div>' );
                }
            }
            
            add_variable( 'upload_button', upload_button() );
            add_variable( 'cancel_button', cancel_button() );
            
            parse_template( 'uploadMediaBlock', 'upMediaBlock' );
        }
        elseif( isset( $_GET['tab'] ) && $_GET['tab'] == 'from-url' )
        {
            //-- File URL get from another website and then 
            //-- save it into database
            add_variable( 'insert_button', button( 'button=insert&type=button' ) );
            add_variable( 'textarea_id', $_GET['textarea_id'] );
            add_variable( 'type', $_GET['type'] );
            
            if( $_GET['type'] == 'image' )
            {
                add_variable( 'type_label', 'Image' );
                parse_template( 'imageMediaURLBlock', 'iMediaBlock' );
            }
            elseif( $_GET['type'] == 'flash' )
            {
                add_variable( 'type_label', 'SWF' );
                parse_template( 'otherMediaURLBlock', 'oMediaBlock' );
            }
            elseif( $_GET['type'] == 'video' )
            {
                add_variable( 'type_label', 'Video' );
                parse_template( 'otherMediaURLBlock', 'oMediaBlock' );
            }
            elseif( $_GET['type'] == 'music' )
            {
                add_variable( 'type_label', 'Music' );
                parse_template( 'otherMediaURLBlock', 'oMediaBlock' );
            }
            elseif( $_GET['type'] == 'pdf' )
            {
                add_variable( 'type_label', 'PDF' );
                parse_template( 'otherMediaURLBlock', 'oMediaBlock' );
            }
            elseif( $_GET['type'] == 'doc' )
            {
                add_variable( 'type_label', 'Document' );
                parse_template( 'otherMediaURLBlock', 'oMediaBlock' );
            }
        }
    }
    
    if( ( isset( $_GET['tab'] ) && $_GET['tab'] == 'gallery' ) )
    {
        if( isSet( $_POST['loadMore'] ) && ( $_POST['loadMore'] == 'gallery' ) )
        {
            get_attachment_ajax( $_GET['post_id'], $_GET['type'] );
            exit;
        }
        
        if( isset( $_POST['save_all_changes'] ) && $_POST['save_all_changes'] == 'Save All Changes' )
        {
            for( $i = $_POST['start_order']; $i < count( $_POST['attachment_id'] ) + $_POST['start_order']; $i++ )
            {
                edit_attachment( $_POST['attachment_id'][$i], $_POST['title'][$i], $_POST['order'][$i], $_POST['alt_text'][$i], $_POST['caption'][$i] );
            }
            
            $bottom_action = '
                <script type="text/javascript">
                	jQuery("#response").slideDown(500);
                	jQuery("#response").delay(3000);
                	jQuery("#response").slideUp(500);
                </script>';
            
            add_variable( 'response', '<div class="alert_green">' . UPDATE_SUCCESS . '</div>' );
            add_actions( 'bottom_elements', $bottom_action );
        }
        
        if( is_delete_all_media() )
        {
            add_actions( 'section_title', 'Delete Comments' );
            
            $warning = '
                <form action="" method="post">';
            
            $warning .= '
	                <div class="alert_red_form">
	                	<strong>
	                		Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this comment' : 'these comments' ) . ':
	                	</strong>
		                <ol>';
            
            foreach( $_POST['select'] as $key => $val )
            {
                $warning .= '
		                        <li>' . attachment_value( 'ltitle', $val ) . '<input type="hidden" name="id[]" value="' . $val . '"></li>';
            }
            
            $warning .= '

		                </ol>
	                </div>
	                <div class="action_box">
	                	<input type="submit" name="confirm_delete" value="Yes" class="button" />
	                	<input type="button" name="confirm_delete" value="No" class="button" onclick="location=\'' . get_attachment_tab_url( $_GET['tab'] ) . '\'" />
	                </div>
	            </form>';
            
            add_variable( 'attachment', $warning );
        }
        else
        {
            if( is_confirm_delete_media() )
            {
                foreach( $_POST['id'] as $key => $val )
                {
                    delete_attachment( $val );
                }
            }
            
            add_variable( 'attachment', get_attachment( $_GET['post_id'], $_GET['type'] ) );
        }
        
        add_variable( 'asc_order', get_attachment_tab_url( $_GET['tab'] ) . '&sort_order=asc' );
        add_variable( 'desc_order', get_attachment_tab_url( $_GET['tab'] ) . '&sort_order=desc' );
        
        add_actions( 'header_elements', 'get_javascript', 'jquery_ui' );
        parse_template( 'galleryMediaBlock', 'gMediaBlock' );
    }
    
    if( isset( $_GET['tab'] ) && $_GET['tab'] == 'library' )
    {
        if( isSet( $_POST['loadMore'] ) && ( $_POST['loadMore'] == 'library' ) )
        {
            get_attachment_ajax( 0, $_GET['type'] );
            exit;
        }
        
        if( isset( $_POST['save_all_changes'] ) && $_POST['save_all_changes'] == 'Save All Changes' )
        {
            for( $i = $_POST['start_order']; $i < count( $_POST['attachment_id'] ) + $_POST['start_order']; $i++ )
            {
                edit_attachment( $_POST['attachment_id'][$i], $_POST['title'][$i], $_POST['order'][$i], $_POST['alt_text'][$i], $_POST['caption'][$i] );
            }
            
            $bottom_action = '
                <script type="text/javascript">
                	jQuery("#response").slideDown(500);
                	jQuery("#response").delay(3000);
                	jQuery("#response").slideUp(500);
                </script>';
            
            add_variable( 'response', '<div class="alert_green">' . UPDATE_SUCCESS . '</div>' );
            add_actions( 'bottom_elements', $bottom_action );
        }
        
        if( is_delete_all_media() )
        {
            
            add_actions( 'section_title', 'Delete Comments' );
            
            $warning = '
                <form action="" method="post">';
            
            $warning .= '
	                <div class="alert_red_form">
	                	<strong>
	                		Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this comment' : 'these comments' ) . ':
	                	</strong>
		                <ol>';
            
            foreach( $_POST['select'] as $key => $val )
            {
                $warning .= '
		                        <li>' . attachment_value( 'ltitle', $val ) . '<input type="hidden" name="id[]" value="' . $val . '"></li>';
            }
            
            $warning .= '

		                </ol>
	                </div>
	                <div class="action_box">
	                	<input type="submit" name="confirm_delete" value="Yes" class="button" />
	                	<input type="button" name="confirm_delete" value="No" class="button" onclick="location=\'' . get_attachment_tab_url( $_GET['tab'] ) . '\'" />
	                </div>
	            </form>';
            
            add_variable( 'attachment', $warning );
        }
        else
        {
            if( is_confirm_delete_media() )
            {
                foreach( $_POST['id'] as $key => $val )
                {
                    delete_attachment( $val );
                }
            }
            
            add_variable( 'attachment', get_attachment( 0, $_GET['type'] ) );
        }
        
        add_variable( 'asc_order', get_attachment_tab_url( $_GET['tab'] ) . '&sort_order=asc' );
        add_variable( 'desc_order', get_attachment_tab_url( $_GET['tab'] ) . '&sort_order=desc' );
        
        add_actions( 'header_elements', 'get_javascript', 'jquery_ui' );
        parse_template( 'galleryMediaBlock', 'gMediaBlock' );
    }
    
    //-- Attempt the action that already add in the whole script
    add_variable( 'header_elements', attemp_actions( 'header_elements' ) );
    add_variable( 'bottom_elements', attemp_actions( 'bottom_elements' ) );
    
    echo return_template( 'uploadMedia' );
}

function set_attachment_tab( $tabs )
{
    $tab = '';
    
    foreach( $tabs as $key => $val )
    {
        if( $_GET['tab'] == $key )
        {
            $tab .= '
                <li class="active">
                	<a href="'. HTTP . SITE_URL . '/lumonata-admin/upload-media.php?tab=' . $key . '&post_id=' . $_GET['post_id'] . '&type=' . $_GET['type'] . '&textarea_id=' . $_GET['textarea_id'] . '">
                		' . $val . '
                	</a>
                </li>';
        }
        else
        {
            $tab .= '
                <li>
                	<a href="'. HTTP . SITE_URL . '/lumonata-admin/upload-media.php?tab=' . $key . '&post_id=' . $_GET['post_id'] . '&type=' . $_GET['type'] . '&textarea_id=' . $_GET['textarea_id'] . '">
                		' . $val . '
                	</a>
                </li>';
        }
    }
    
    return $tab;
}

function get_attachment_tab_url( $tab )
{
    $page_URL = 'http' . ( !empty( $_SERVER['HTTPS'] ) ? 's' : '' );
    $page_URL .= '://';
    
    if( $_SERVER["SERVER_PORT"] != '80' )
    {
        $page_URL .= SITE_URL . ':' . $_SERVER['SERVER_PORT'] . '/lumonata-admin/upload-media.php?tab=' . $tab . '&post_id=' . $_GET['post_id'] . '&type=' . $_GET['type'] . '&textarea_id=' . $_GET['textarea_id'];
    }
    else
    {
        $page_URL .= SITE_URL . '/lumonata-admin/upload-media.php?tab=' . $tab . '&post_id=' . $_GET['post_id'] . '&type=' . $_GET['type'] . '&textarea_id=' . $_GET['textarea_id'];
    }
    
    return $page_URL;
}

function dragdropimage( $type, $post_id, $textarea_id )
{
    $dragdropimage = '
    	<div id="dropbox">
    		<span class="message">
	    		<input type="hidden" value="' . $post_id . '" name="post_id"/>
	    		<input type="hidden" value="' . $textarea_id . '" name="textarea_id"/>
	    		<i>Drop images here to upload.</i>
	    	</span>
		</div>';
    
    if( $type == 'image' )
    {
        return $dragdropimage;
    }
    else
    {
        return '';
    }
}

function exit_status( $str, $error = '' )
{
    echo json_encode( array(
         'status' => $str,
        'error' => $error 
    ) );
    exit;
}

function get_extension( $file_name )
{
    $ext = explode( '.', $file_name );
    $ext = array_pop( $ext );
    
    return strtolower( $ext );
}

function is_delete_all_media()
{
    if( ( isset( $_POST['delete'] ) && $_POST['delete'] == 'Delete' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function is_confirm_delete_media()
{
    if( ( isset( $_POST['confirm_delete'] ) && $_POST['confirm_delete'] == 'Yes' ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

?>