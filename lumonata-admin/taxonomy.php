<?php

    require_once('../lumonata_config.php');
    require_once('../lumonata_settings.php');
    require_once('../lumonata-functions/settings.php');
    require_once('../lumonata-functions/kses.php');
    require_once('../lumonata-classes/actions.php');
    require_once('../lumonata-functions/user.php');
    require_once('../lumonata-classes/user_privileges.php');
    require_once('../lumonata-functions/taxonomy.php');
    require_once('admin_functions.php');    

    if(!defined('SITE_URL'))
    {
		define('SITE_URL',get_meta_data('site_url'));
    }

    if(!is_user_logged())
    {
	    header('location:'.get_admin_url().'/?state=login');
    }
    elseif(isset($_POST['insert_rules']))
    {
        // $selected = json_decode(rem_slashes($_POST['selected']));
        // print_r(array_merge($selected,array(16)));
        // echo $_POST['selected'];
        // exit();

		if(insert_rules($_POST['parent'],$_POST['name'],$_POST['description'],$_POST['rule'],$_POST['group'],false,'',$_POST['add_language_rule']))
		{
            $rule_id = mysql_insert_id();
            // echo $rule_id;

            if(isset($_POST['additional_rule_fields']))
            {
                foreach($_POST['additional_rule_fields'] as $key=>$val)
                {
                   foreach($val as $subkey=>$subval)
                   {
                        edit_additional_field($rule_id, $key, $subval, $_POST['group']);
                   }
                }
            }

			if(!empty($_POST['selected']))
			{
				$selected = json_decode(rem_slashes($_POST['selected']));
				$merge_selected = array_merge($selected,array($rule_id));
			}
			else
			{
				$merge_selected = array(mysql_insert_id());
			}

			echo all_categories($_POST['index'],$_POST['rule'],$_POST['group'],$merge_selected);
		}
    }
    elseif(isset($_POST['update_parent']))
    {
		$select = '
		<select class="category_combobox" name="parent['.$_POST['index'].']" autocomplete="off">
            <option value="0">Parent</option>
            '.recursive_taxonomy($_POST['index'],$_POST['rule'],$_POST['group'],'select').'
        </select>';

		echo $select;
    }
    elseif(isset($_POST['update_order']))
    {
		update_taxonomy_order($_POST['theitem'],$_POST['start'],$_POST['state']);
    }
    else
    {
        if(is_delete($_POST['state']))
        {
            if(!delete_rule($_POST['id'], $_POST['group']))
            {
            	echo '<div class="alert_red_form">Deleting process failed.</div>';
            }
        }
        elseif(is_search())
        {
        	$s = 'SELECT * FROM lumonata_rules WHERE lrule=%s AND (lname LIKE %s OR ldescription LIKE %s) AND lgroup=%s';
	    	$q = $db->prepare_query($s,$_POST['rule'],'%'.$_POST['s'].'%','%'.$_POST['s'].'%',$_POST['group']);
	    	$r = $db->do_query($q);

            if($db->num_rows($r) > 0)
            {
				echo rule_list($r,$_POST['group'],$_POST['rule']);
            }
            else
            {
                echo '
                <div class="alert_yellow_form">
                	No result found for <em>'.$_POST['s'].'</em>. 
                	Check your spellling or try another terms
                </div>';
            }
		}
    }  

?>   