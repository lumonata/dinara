<?php

    require_once('../lumonata_config.php');
    require_once('../lumonata_settings.php');
    require_once('../lumonata-functions/settings.php');
    require_once('../lumonata-classes/actions.php');
    require_once('../lumonata-functions/upload.php');
    require_once('../lumonata-functions/attachment.php');
    require_once('../lumonata-functions/kses.php');
    require_once('../lumonata-classes/directory.php');
    require_once('../lumonata-functions/user.php');
    require_once('../lumonata-functions/paging.php');
    require_once('../lumonata-classes/user_privileges.php');
    require_once('../lumonata-functions/articles.php');
    require_once('../lumonata-functions/taxonomy.php');
    require_once('admin_functions.php');

    define('TEMPLATE_PATH',ROOT_PATH.'/lumonata-admin/themes/'. get_meta_data('admin_theme','themes'));
    define('PLUGINS_PATH',ROOT_PATH.'/lumonata-plugins');
    define('APPS_PATH',ROOT_PATH.'/lumonata-apps');
    define('FUNCTIONS_PATH',ROOT_PATH.'/lumonata-functions');
    define('CLASSES_PATH',ROOT_PATH.'/lumonata-classes');
    define('ADMIN_PATH',ROOT_PATH.'/lumonata-admin');
    define('CONTENT_PATH',ROOT_PATH.'/lumonata-content');

    require_once("../lumonata-functions/rewrite.php");
    require_once('../lumonata-classes/admin_menu.php');	
    require_once("../lumonata-functions/plugins.php");	 
    require_once("../lumonata-functions/template.php");  

    if(!defined('SITE_URL'))
    {
		define('SITE_URL', get_meta_data('site_url'));
    }

    $ssl_option = get_meta_data('ssl_option');
    
    if(!defined('HTTP'))
    {
        define('HTTP', ($ssl_option==1?'https://':'http://'));
    }

    if(!is_user_logged())
    {
	    header("location:".get_admin_url()."/?state=login");
    }
    elseif(isset($_POST['update_order']))
    {
		update_articles_order($_POST['theitem'],$_POST['start'],$_POST['state']);
    }
    elseif(isset($_POST['delete_post']))
    {
    	$article = fetch_artciles("id=".$_POST['id']."&type=status");

    	if($_COOKIE['user_id']==$article['lpost_by'])
        {
    	   return delete_article($_POST['id'], 'status');
        }
    }
    elseif(isset($_POST['update_sef']))
    {
        if(update_sef($_POST['post_id'],$_POST['title'], $_POST['new_sef'],$_POST['type']))
        {
            echo 'BAD';
        }
        else
        {
        	echo 'OK';
        }
    }
    elseif(isset($_POST['get_sef']))
    {
        if(empty($_POST['title']))
        {
           $_POST['title'] = 'Untitled';
        }
        else
        {
           $_POST['title'] = kses(rem_slashes($_POST['title']),$allowedtitletags);
        }

        $num_by_title_and_type = is_num_articles('title='.$_POST['title'].'&type='.$_POST['type']);

        if($num_by_title_and_type > 0)
        {
         	for($i = 2; $i <= $num_by_title_and_type + 1; $i++)
            {
	        	$sef = generateSefUrl($_POST['title']).'-'.$i;

	            if(is_num_articles('sef='.$sef.'&type='.$_POST['type']) < 1)
                {
	            	$sef = $sef;
	                break;
	            }
	         }
        }
        else
        {
            $sef=generateSefUrl($_POST['title']);
        }

        if(strlen($sef)>50)
        {
            $more = '...';
        }
        else
        {
            $more = '';
        }

        if($_POST['type']=='pages')
        {
            $link_structure = HTTP.site_url().'/';
            $ext = '/';
        }
        else
        {
            $link_structure = HTTP.site_url().'/'.$_POST['type'].'/category/';
            $ext = '.html';
        } 

        echo '
        <strong>Permalink:</strong>
        '.$link_structure.'
        <span id="the_sef_'.$_POST['index'].'">
            <span id="the_sef_content_'.$_POST['index'].'"  style="background:#FFCC66; cursor:pointer;">
                '.substr($sef,0,50).$more.'
            </span>
            '.$ext.'
            <input type="button" value="Edit" id="edit_sef_'.$_POST['index'].'" class="button_bold">
        </span>

        <span id="sef_box_'.$_POST['index'].'" style="display:none;">
            <span>
                <input type="text" name="sef_box['.$_POST['index'].']" value="'.$sef.'" style="border:1px solid #CCC; width:300px; font-size:11px;" />
                '.$ext.' <input type="button" value="Done" id="done_edit_sef_'.$_POST['index'].'" class="button_bold">
            </span>
        </span>

        <script type="text/javascript">

    		jQuery("#the_sef_'.$_POST['index'].'").click(function(){
    			jQuery("#the_sef_'.$_POST['index'].'").hide();
    			jQuery("#sef_box_'.$_POST['index'].'").show();
        	});

        	jQuery("#edit_sef_'.$_POST['index'].'").click(function(){
    			jQuery("#the_sef_'.$_POST['index'].'").hide();
    			jQuery("#sef_box_'.$_POST['index'].'").show();
        	});

        	jQuery("#done_edit_sef_'.$_POST['index'].'").click(function(){
        		var new_sef = jQuery("input[name=sef_box['.$_POST['index'].']]").val();
                var more    = (new_sef.length > 50 ? "..." : "");

        		jQuery("#the_sef_content_'.$_POST['index'].'").html(new_sef.substr(0,50)+more);
        		jQuery("#the_sef_'.$_POST['index'].'").show();
        		jQuery("#sef_box_'.$_POST['index'].'").hide();
        	});
        </script>';
    }
    else
    {
        if( is_delete( $_POST['state'] ) )
        {
            run_actions($_POST['state'].'_additional_delete');
            
            if(!delete_article($_POST['id'],$_POST['state']))
            {
                echo '<div class="alert_red_form">Deleting process failed.</div>';
            }
        }
        elseif(is_search())
        {
            $apps = isset( $_POST['sub'] ) ? $_POST['sub'] : $_POST['state'];

    	    if( $_COOKIE['user_type']=='contributor' || $_COOKIE['user_type']=='author' )
            {
                $w = ' lpost_by='.$_COOKIE['user_id'].' AND ';
    	    }
            else
            {
                $w = '';
    	    }

            $s = 'SELECT * FROM lumonata_articles WHERE '.$w.' larticle_type=%s AND ( larticle_title LIKE %s OR larticle_content LIKE %s )';
            $q = $db->prepare_query( $s, $apps, '%'.$_POST['s'].'%', '%'.$_POST['s'].'%' );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) > 0 )
            {
        		if( $apps == 'pages')
                {
        		    echo pages_list( $r );
                }
        		else
                {
        		    echo article_list( $r, $apps );
                }
            }
            else
            {
                echo '
                <div class="alert_yellow_form">
                    No result found for <em>'.$_POST['s'].'</em>. 
                    Check your spellling or try another terms
                </div>';
            }
        }
    }

?>   