<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN TITLE HERO PADA DETAIL PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_data_detail_page($sef, $destination, $type, $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    // STRING TRANSLATIONS
    $translations = array('Accommodation', 'Dining', 'Spa', 'Events', 'Weddings', 'What To Do Act In', 'What To Do Around', 'Special Offers', 'News');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $result = array();

    $data   = get_post_detail( $sef, $type );

    if(!empty($data))
    {
        extract( $data );

        $post_title_new = $post_title;

        if($type != "spa")
        {
            if(strpos($post_title, ' of '))
            {
                $post_title_new = explode_title_by_at_of(" of ", $post_title_new);
            }
            elseif(strpos($post_title_new, ' at '))
            {
                $post_title_new = explode_title_by_at_of(" at ", $post_title_new);
            }
        }

        // GET SLIDER HERO PAGE
        if($type == "accommodation" || $type == "dining" || $type == "what-to-do-act-in" || $type == "what-to-do-around" || $type == "events")
        {
            if(!empty($post_attachment))
            {
                add_actions( 'slider_hero_page', 'get_slider_hero_page', $post_attachment, $post_title );
            }
        }


        $type_new = $type;
        if(strpos($type, '-'))
        {
            $explode_type = explode("-", $type_new);
            $type_new = "";
            foreach($explode_type as $d)
            {
                $type_new .= $d." ";
            }
        }

        $type_for_tr_string = str_replace("-", "_", $type);
        $subtitle = $tr_string[$type_for_tr_string];
        // $subtitle = (empty($post_subtitle) ? strtoupper($type_new) : $post_subtitle);

        // META DATA
        $meta_title         = get_additional_field( $post_id, 'meta_title', $type );
        $meta_keywords      = get_additional_field( $post_id, 'meta_keywords', $type );
        $meta_description   = get_additional_field( $post_id, 'meta_description', $type );

        if($check_lang)
        {
            $post_title_new_lang   = get_additional_field($post_id, 'title_'.$lang, $type);
            // $subtitle_lang      = get_additional_field($post_id, 'subtitle_'.$lang, $type);
            $content_lang          = get_additional_field($post_id, 'content_'.$lang, $type);
            $brief_lang            = get_additional_field($post_id, 'brief_'.$lang, $type);
            $meta_title_lang       = get_additional_field( $post_id, 'meta_title_'.$lang, $type );
            $meta_keywords_lang    = get_additional_field( $post_id, 'meta_keywords_'.$lang, $type );
            $meta_description_lang = get_additional_field( $post_id, 'meta_description_'.$lang, $type );
            
            $post_title_new   = (empty($post_title_new_lang) ? $post_title_new:     $post_title_new_lang);
            $post_title       = (empty($post_title_new_lang) ? $post_title:         $post_title_new_lang);
            // $subtitle      = (empty($subtitle_lang) ? $subtitle:                 $subtitle_lang);
            $post_brief       = (empty($brief_lang) ? $post_brief:                  $brief_lang);
            $post_content     = (empty($content_lang) ? $post_content:              $content_lang);
            $meta_title       = (empty($meta_title_lang) ? $meta_title:             $meta_title_lang);
            $meta_keywords    = (empty($meta_keywords_lang) ? $meta_keywords:       $meta_keywords_lang);
            $meta_description = (empty($meta_description_lang) ? $meta_description: $meta_description_lang);
        }


        $result['title_hero_template'] = '
            <div class="container container-title-hero-detail-page clearfix">
                <h2 class="text _text sub-title-detail-page">'.$subtitle.'</h2>
                <h1 class="text _text title-detail-page-1">'.$post_title_new.'</h1>
                <!--<p class="text _text title-detail-page-2">COURTYARD VILLA</p>-->
            </div>
        ';
        
        $result['bg_hero']      = "";
        $result['post_title']   = $post_title;
        $result['post_sef']     = $post_sef;
        $result['post_content'] = $post_content;
        $result['post_brief']   = $post_brief;
        $result['post_id']      = $post_id;
        $result['post_type']    = $post_type;
        $result['metatitle']    = $meta_title;
        $result['metakey']      = $meta_keywords;
        $result['metadesc']     = $meta_description;
        $result['og_image']     = "";

        if(!empty($post_featured_img))
        {
            $result['bg_hero']      = HTTP.site_url().$post_featured_img['original'];
        }
    }

    return $result;
}


function get_slider_hero_page($attachment='', $title_page)
{
    set_template( TEMPLATE_PATH . '/layout/header_slide.html', 'header_slide_template' );
    add_block( 'loop-popup-slide-block', 'lpsblock', 'header_slide_template' );
    add_block( 'loop-header-slide-block', 'lhsblock', 'header_slide_template' );
    add_block( 'header-slide-block', 'hsblock', 'header_slide_template' );

    set_switch_language();

    add_variable( 'title_page_popup', $title_page);

    if(!empty($attachment))
    {
        foreach($attachment as $d)
        {
            $img_large = $d['img_large'];

            add_variable( 'img_large', $img_large);

            parse_template( 'loop-popup-slide-block', 'lpsblock', true );
            parse_template( 'loop-header-slide-block', 'lhsblock', true );
        }
    }

    parse_template( 'header-slide-block', 'hsblock', false );
    
    return return_template( 'header_slide_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Accomodation Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function accommodation_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/accommodation_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('Accommodation', 'Single', 'King Size', 'Double', 'Double Twin', 'Villa Size', 'Bed Type', 'Maximum Capacity', 'Pool Size', 'Villa Benefits', 'Villa Amenities', 'Villa Location');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // ADDITIONAL FIELD
        $size             = get_additional_field( $post_id, 'size', 'accommodation' );
        $occupancy        = get_additional_field( $post_id, 'occupancy', 'accommodation' );
        $pool_size        = get_additional_field( $post_id, 'pool_size', 'accommodation' );
        $bed              = get_additional_field( $post_id, 'bed', 'accommodation' );
        $features         = get_additional_field( $post_id, 'features', 'accommodation' );
        $benefits         = get_additional_field( $post_id, 'benefits', 'accommodation' );
        $map_location_img = get_map_location_image( $post_id, 'accommodation', false );


        if($check_lang)
        {
            $str_replace_bed = strtolower(str_replace("/ ", "", $bed));
            $str_replace_bed = strtolower(str_replace(" ", "_", $str_replace_bed));
            $bed_lang        = $tr_string[$str_replace_bed];
            $size_lang       = get_additional_field($post_id, 'size_'.$lang, 'accommodation');
            $occupancy_lang  = get_additional_field($post_id, 'occupancy_'.$lang, 'accommodation');
            $pool_size_lang  = get_additional_field($post_id, 'pool_size_'.$lang, 'accommodation');
            $features_lang   = get_additional_field($post_id, 'features_'.$lang, 'accommodation');
            $benefits_lang   = get_additional_field($post_id, 'benefits_'.$lang, 'accommodation');
            
            $size      = (empty($size_lang) ? $size:           $size_lang);
            $pool_size = (empty($pool_size_lang) ? $pool_size: $pool_size_lang);
            $occupancy = (empty($occupancy_lang) ? $occupancy: $occupancy_lang);
            $benefits  = (empty($benefits_lang) ? $benefits:   $benefits_lang);
            $features  = (empty($features_lang) ? $features:   $features_lang);
            $bed       = (empty($bed_lang) ? $bed:             $bed_lang);
        }


        // VILLA DETAILS
        $villa_details = "";
        if(!empty($size))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['villa_size'].'</p>
                    <p class="text _text text-value">'.$size.'</p>
                </div>
            ';
        }
        if(!empty($bed))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['bed_type'].'</p>
                    <p class="text _text text-value">'.$bed.'</p>
                </div>
            ';
        }
        if(!empty($pool_size))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['pool_size'].'</p>
                    <p class="text _text text-value">'.$pool_size.'</p>
                </div>
            ';
        }
        if(!empty($occupancy))
        {
            $villa_details .= '
                <div class="container container-detail-acco clearfix">
                    <p class="text _text text-label">'.$tr_string['maximum_capacity'].'</p>
                    <p class="text _text text-value">'.$occupancy.'</p>
                </div>
            ';
        }
        add_variable( 'villa_details', $villa_details);

        // VILLA BENEFITS
        if(!empty($benefits))
        {
            $benefits_html = '
                <div class="benefits-villa" data-aos="fade-up">
                    <h2>'.$tr_string['villa_benefits'].'</h2>
                    <p>'.$benefits.'</p>
                </div>
            ';
            add_variable( 'benefits_html', $benefits_html);
        }
        
        $section_map_location = "";
        if(!empty($map_location_img))
        {
            $map_location_img_medium = HTTP.site_url().$map_location_img['original'];
            $section_map_location = '
                <div class="container-map-location" data-aos="fade-up">
                    <h2>'.$tr_string['villa_location'].'</h2>
                    <img src="'.$map_location_img_medium.'" />
                </div>
            ';
        }

        // VILLA AMENITIS
        $amenities_content = "";
        $amenities = get_rule_list_post($post_id, 'accommodation','amenities', 1, $lang, $check_lang);
        if(!empty($amenities))
        {
            $amenities_content = '
                <div class="container-features" data-aos="fade-up">
                    <h2>'.$tr_string['villa_amenities'].'</h2>
                    <div class="features-acco">
                        '.$amenities.'
                    </div>
                </div>
            ';
        }
        add_variable( 'amenities_content', $amenities_content);

        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'size', $size);
        add_variable( 'occupancy', $occupancy);
        add_variable( 'pool_size', $pool_size);
        add_variable( 'bed', $bed);
        add_variable( 'features', $features);
        add_variable( 'section_map_location', $section_map_location);
        add_variable( 'destination_upper', strtoupper($destination));

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('accommodation', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product       = $d['post_id'];
                $title_product    = $d['post_title'];
                // $subtitle_product = $d['post_subtitle'];
                // $subtitle_product = (empty($subtitle_product) ? $tr_string['accommodations']: $subtitle_product);
                $subtitle_product = $tr_string['accommodation'];
                $sef_product      = $d['post_sef'];
                $brief_product    = $d['post_brief'];
                $content_product  = $d['post_content'];
                $image_product    = get_featured_img( $id_product, 'accommodation', false );
                $image_product    = HTTP . site_url().$image_product['medium'];
                $post_link        = HTTP . site_url().'/'.$destination.'/accommodation/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang    = get_additional_field($id_product, 'title_'.$lang, 'accommodation');
                    $brief_product_lang    = get_additional_field($id_product, 'brief_'.$lang, 'accommodation');
                    $content_product_lang  = get_additional_field($id_product, 'content_'.$lang, 'accommodation');
                    $subtitle_product_lang = get_additional_field($id_product, 'subtitle_'.$lang, 'accommodation');
                    
                    $title_product    = (empty($title_product_lang) ? $title_product:       $title_product_lang);
                    $brief_product    = (empty($brief_product_lang) ? $brief_product:       $brief_product_lang);
                    $content_product  = (empty($content_product_lang) ? $content_product:   $content_product_lang);
                    $subtitle_product = (empty($subtitle_product_lang) ? $subtitle_product: $subtitle_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/accommodation/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('subtitle_product', $subtitle_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }
        
        $link_explore  = HTTP.site_url().'/'.$destination.'/accommodation';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/accommodation';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/accommodation/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/accommodation/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);
        $properties_name = get_additional_field($corporatedata[0]['id'],'fb_properties_name','destinations');
        $hotels_name     = get_additional_field($corporatedata[0]['id'],'fb_hotels_name','destinations');
        add_variable( 'properties_name', $properties_name );
        add_variable( 'hotels_name', $hotels_name );

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Dining Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function dining_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/dining_detail.html', 'page_template' );
    add_block( 'loop-cat-menu-block', 'lcmblock', 'page_template' );
    add_block( 'menu-dining-block', 'mdblock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    add_actions( 'booking_popup_dinings', 'booking_popup_dinings_content' );

    add_variable( 'site_url', site_url() );
    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    add_actions( 'include-js', 'get_custom_javascript', 'https://booking.resdiary.com/bundles/jquery.js' );

    // STRING TRANSLATIONS
    $translations = array('Download Our Menu', 'Breakfast', 'Lunch', 'Dining', 'Restaurant Open', 'Dresscode', 'Book Offer Now', 'Book Now', 'Reserve Table');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $label_link_hero      = $tr_string['reserve_table'];

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $breakfast_open     = get_additional_field( $post_id, 'breakfast_open', 'dining' );
        $lunch_open         = get_additional_field( $post_id, 'lunch_open', 'dining' );
        $dining_open        = get_additional_field( $post_id, 'dining_open', 'dining' );
        $dresscode          = get_additional_field( $post_id, 'dresscode', 'dining' );
        $our_menu_desc      = get_additional_field( $post_id, 'our_menu_desc', 'dining' );
        $menu_pdf           = get_additional_field( $post_id, 'menu_pdf', 'dining' );
        $menu_pdf_text_link = get_additional_field( $post_id, 'menu_pdf_text_link', 'dining' );
        $code_rest_diary    = get_additional_field( $post_id, 'code_rest_diary', 'dining' );

        // print_r($code_rest_diary);
        $hide_reservation = "hide";
        if(!empty($code_rest_diary))
        {
            $hide_reservation = "";
            add_actions( 'code_rest_diary', $code_rest_diary );

            // BUTTON BOOK NOW
            $button_book_hero = '
                <a href="#" class="container container-button-book-now-link book-now-hero clearfix" target="_blank">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </a>
            ';

            add_actions('button_book_hero', $button_book_hero);
        }
        add_variable( 'hide_reservation', $hide_reservation);

        // LANGUAGE CHECK
        if($check_lang)
        {
            $breakfast_open_lang = get_additional_field($post_id, 'breakfast_open_'.$lang, 'dining');
            $lunch_open_lang     = get_additional_field($post_id, 'lunch_open_'.$lang, 'dining');
            $dining_open_lang    = get_additional_field($post_id, 'dining_open_'.$lang, 'dining');
            $dresscode_lang      = get_additional_field( $post_id, 'dresscode_'.$lang, 'dining' );
            $our_menu_desc_lang  = get_additional_field( $post_id, 'our_menu_desc_'.$lang, 'dining' );
            
            $breakfast_open = (empty($breakfast_open_lang) ? $breakfast_open: $breakfast_open_lang);
            $lunch_open     = (empty($lunch_open_lang) ? $lunch_open:         $lunch_open_lang);
            $dining_open    = (empty($dining_open_lang) ? $dining_open:       $dining_open_lang);
            $dresscode      = (empty($dresscode_lang) ? $dresscode:           $dresscode_lang);
            $our_menu_desc  = (empty($our_menu_desc_lang) ? $our_menu_desc:   $our_menu_desc_lang);
        }

        
        $breakfast_open     = (empty($breakfast_open) ? "": "<p>".$tr_string['breakfast']." $breakfast_open</p>");
        $lunch_open         = (empty($lunch_open) ? "":     "<p>".$tr_string['lunch']." $lunch_open</p>");
        $dining_open        = (empty($dining_open) ? "":    "<p>".$tr_string['dining']." $dining_open</p>");
        $restaurant_open    = $breakfast_open.$lunch_open.$dining_open;

        $menu_pdf_download = "";
        if(!empty($menu_pdf) && file_exists( PLUGINS_PATH . '/menus/pdf/' . $menu_pdf ))
        {
            $menu_pdf_download = '
                <div class="container-download-menu" data-aos="fade-up">
                    <a href="'.URL_PLUGINS . 'menus/pdf/' . $menu_pdf.'" target="_blank">
                        <p>'.$tr_string['download_our_menu'].'</p>
                    </a>
                </div>
            ';
        }
        elseif(!empty($menu_pdf_text_link))
        {
            $menu_pdf_download = '
                <div class="container-download-menu" data-aos="fade-up">
                    <a href="'.$menu_pdf_text_link.'" target="_blank">
                        <p>'.$tr_string['download_our_menu'].'</p>
                    </a>
                </div>
            ';
        }

        // RESTAURANT OPEN & DRESS CODE
        if(!empty($restaurant_open) || !empty($dresscode))
        {
            $dresscode_restaurant = '
            <div class="container-open-dresscode clearfix">';

            if(!empty($restaurant_open))
            {
                $dresscode_restaurant .= '
                <div class="dining-open">
                    <h4>'.$tr_string['restaurant_open'].'</h4>
                    '.$restaurant_open.'
                </div>
                ';
            }
            if(!empty($dresscode))
            {
                $dresscode_restaurant .= '
                <div class="dining-dresscode">
                    <h4>'.$tr_string['dresscode'].'</h4>
                    <p>'.$dresscode.'</p>
                </div>
                ';
            }
            $dresscode_restaurant .= '
            </div>
            ';
            add_variable( 'dresscode_restaurant', $dresscode_restaurant);
        }

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'restaurant_open', $restaurant_open);
        add_variable( 'dresscode', $dresscode);
        add_variable( 'our_menu_desc', $our_menu_desc);
        add_variable( 'menu_pdf_download', $menu_pdf_download);


        // GET CATEGORY
        $qcmenu = $db->prepare_query("
            SELECT a.*
            FROM lumonata_menu_category AS a
            WHERE a.larticle_id = %d AND a.lcmenu_parent = %d
            ORDER BY lorder
        ", $post_id, 0);
        $rcmenu = $db->do_query($qcmenu);
        $ncmenu = $db->num_rows($rcmenu);

        if($ncmenu > 0)
        {  
            $icm = 1;
            $menu_type_all = "";
            $category_li_html = "";

            while($dcmenu = $db->fetch_array($rcmenu))
            {
                $category_id       = $dcmenu['lcmenu_id'];
                $category_name     = $dcmenu['lcmenu_name'];
                $category_sanitize = generateSefUrl($dcmenu['lcmenu_name']);

                add_variable('category_sanitize', $category_sanitize);

                // LANGUAGE CHECK
                if($check_lang)
                {
                    $category_name_lang = get_additional_menu_field($category_id, 'lcmenu_name_'.$lang, 'menu_category');
                    $category_name      = (empty($category_name_lang) ? $category_name: $category_name_lang);
                }
                
                if($icm == 1)
                {
                    $class_active = "active";
                    $category_li_html .= '<li class="active" data-filter="'.$category_sanitize.'">'.$category_name.'</li>';
                }
                else
                {
                    $class_active = "";
                    $category_li_html .= '<li data-filter="'.$category_sanitize.'">'.$category_name.'</li>';
                }

                add_variable('class_active', $class_active);
                

                // GET CHILD CATEGORY
                $qccmenu = $db->prepare_query("
                    SELECT *
                    FROM lumonata_menu_category AS a
                    WHERE lcmenu_parent = %d
                    ORDER BY lorder
                ", $category_id);
                $rccmenu = $db->do_query($qccmenu);
                $nccmenu = $db->num_rows($rccmenu);
                
                $list_item_menu_html = "";

                if($nccmenu > 0)
                {
                    while($dccmenu = $db->fetch_array($rccmenu))
                    {
                        $child_category_id       = $dccmenu['lcmenu_id'];
                        $child_category_name     = $dccmenu['lcmenu_name'];

                        // LANGUAGE CHECK
                        if($check_lang)
                        {
                            $child_category_name_lang = get_additional_menu_field($child_category_id, 'lcmenu_name_'.$lang, 'menu_category');
                            $child_category_name      = (empty($child_category_name_lang) ? $child_category_name: $child_category_name_lang);
                        }

                        $list_item_menu_html .= '<h3 data-aos="fade-up">'.($child_category_name).'</h3><div data-aos="fade-up" class="container-list-menu clearfix">';

                        // START GET MENU LIST
                        $qmenu = $db->prepare_query("
                            SELECT *
                            FROM lumonata_menu
                            WHERE lcmenu_id = %d
                            ORDER BY lorder
                        ", $child_category_id);
                        $rmenu = $db->do_query($qmenu);
                        $nmenu = $db->num_rows($rmenu);

                        if($nmenu > 0)
                        {
                            while($dmenu = $db->fetch_array($rmenu))
                            {
                                $menu_id   = $dmenu['lmenu_id'];
                                $menu_name = $dmenu['lmenu_name'];
                                $menu_desc = $dmenu['lmenu_desc'];

                                // LANGUAGE CHECK
                                if($check_lang)
                                {
                                    $menu_name_lang = get_additional_menu_field($menu_id, 'lmenu_name_'.$lang, 'menu_list');
                                    $menu_desc_lang = get_additional_menu_field($menu_id, 'lmenu_desc_'.$lang, 'menu_list');
                                    
                                    $menu_name      = (empty($menu_name_lang) ? $menu_name: $menu_name_lang);
                                    $menu_desc      = (empty($menu_desc_lang) ? $menu_desc: $menu_desc_lang);
                                }

                                $list_item_menu_html .= '
                                    <div class="list-item-menu" data-aos="fade-up">
                                        <h4>'.$menu_name.'</h4>
                                        <p>'.$menu_desc.'</p>
                                    </div>
                                ';
                            }
                        }
                        $list_item_menu_html .= '</div>';
                    }

                    
                }
                
                add_variable('list_item_menu_html', $list_item_menu_html);

                parse_template('loop-cat-menu-block', 'lcmblock', true);

                add_variable('category_li_html', $category_li_html);

                $icm++;
            }

            
            parse_template('menu-dining-block', 'mdblock', false);
        }
        else
        {
            add_variable( 'hide', 'hide');
        }



        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('dining', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'dining', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/dining/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'dining');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'dining');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'dining');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/dining/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/dining';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/dining';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/dining/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/dining/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NILAI MENU TYPE DAN MENGHAPUS VALUE YANG SAMA
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_menu_type_dining($menu_type_all)
{
    $last_menu_type   = array();
    $menu_type_before = "";

    foreach($menu_type_all as $d)
    {
        $menu_type = $d;
        if($menu_type != $menu_type_before)
        {
            $last_menu_type[] = $menu_type;
        }
        $menu_type_before = $menu_type;
    }

    return $last_menu_type;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Dining Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function spa_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/spa_detail.html', 'page_template' );
    add_block( 'loop-cat-treatment-block', 'lctblock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('Price', 'Book Now');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $option_spa = "";

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        $val_array = array(
            'key' => $post_id,
            'title' => $post_title
        );
        $values_json = base64_encode( json_encode( $val_array ) );
        add_actions( 'value_json_booking', $values_json );

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);

        $option_spa .= '<option value="'.$post_id.'">'.ucwords(strtolower($post_title)).'</option>';

        // GET SPA CATEGORY LIST
        $qcat = $db->prepare_query("
            SELECT * 
            FROM lumonata_treatment_category
            WHERE larticle_id = %d
        ", $post_id);
        $rcat = $db->do_query($qcat);
        $ncat = $db->num_rows($rcat);

        if($ncat > 0)
        {
            while($d = $db->fetch_array($rcat))
            {
                $cat_id   = $d['lctreatment_id'];
                $cat_name = $d['lctreatment_name'];
                $cat_desc = $d['lctreatment_desc'];

                // LANGUAGE CHECK
                if($check_lang)
                {
                    $cat_name_lang = get_additional_treatment_field($cat_id, 'lctreatment_name_'.$lang, 'treatment_category');
                    $cat_desc_lang = get_additional_treatment_field($cat_id, 'lctreatment_desc_'.$lang, 'treatment_category');
                    
                    $cat_name      = (empty($cat_name_lang) ? $cat_name: $cat_name_lang);
                    $cat_desc      = (empty($cat_desc_lang) ? $cat_desc: $cat_desc_lang);
                }

                // echo $cat_desc;

                add_variable( 'cat_name', $cat_name);

                if(!empty($cat_desc))
                {
                    $cat_desc = '
                        <div class="desc-cat">
                            '.$cat_desc.'
                        </div>
                    ';
                }
                add_variable( 'cat_desc', $cat_desc);

                // GET LIST TREATMENT
                $qtreatment = $db->prepare_query("
                    SELECT *
                    FROM lumonata_treatment
                    WHERE lctreatment_id = %d
                ", $cat_id);
                $rtreatment = $db->do_query($qtreatment);
                $ntreatment = $db->num_rows($rtreatment);

                $content_treatment_list = "";

                if($ntreatment > 0)
                {
                    while($dtreatment = $db->fetch_array($rtreatment))
                    {
                        $treatment_id    = $dtreatment['ltreatment_id'];
                        $treatment_name  = $dtreatment['ltreatment_name'];
                        $treatment_price = $dtreatment['ltreatment_price'];

                        $val_array_treatment = array(
                            'key'      => $post_id,
                            'cat_id'   => $cat_id,
                            'treat_id' => $treatment_id
                        );
                        $values_json_treatment = base64_encode( json_encode( $val_array_treatment ) );

                        // LANGUAGE CHECK
                        if($check_lang)
                        {
                            $treatment_name_lang  = get_additional_treatment_field($treatment_id, 'ltreatment_name_'.$lang, 'treatment_list');
                            
                            $treatment_name  = (empty($treatment_name_lang) ? $treatment_name:        $treatment_name_lang);
                        }

                        $content_treatment_list .= '
                            <div class="list-treatment clearfix">
                                <div class="left">
                                    <p>'.$treatment_name.'</p>
                                    <p class="price-spa">'.$tr_string['price'].': '.$treatment_price.' k</p>
                                </div>
                                <div class="right clearfix">
                                    <p class="price-spa">'.$tr_string['price'].': '.$treatment_price.' k</p>
                                    <button class="book-now-treatment" data-key="'.$values_json_treatment.'">'.$tr_string['book_now'].'</button>
                                </div>
                            </div>
                        ';
                    }

                    add_variable( 'content_treatment_list', $content_treatment_list);
                }

                parse_template('loop-cat-treatment-block', 'lctblock', true);
            }
        }

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('spa', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'spa', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/spa/'.$sef_product.'.html';

                $title_option_book = ucwords(strtolower($title_product));

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'spa');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'spa');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'spa');
                    
                    $title_product     = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $title_option_book = (empty($title_product_lang) ? $title_option_book: $title_product_lang);
                    $brief_product     = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product   = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/spa/'.$sef_product.'.html';
                }

                $option_spa .= '<option value="'.$id_product.'">'.$title_option_book.'</option>';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/spa';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/spa';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/spa/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/spa/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);

        $destination_id = $corporatedata[0]['id'];
        add_actions( 'booking_popup_spa', 'booking_popup_spa_content', $destination, $destination_id );
        add_actions( 'option_spa', $option_spa );
        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}



/*
| -------------------------------------------------------------------------------------------------------------------------
| Special Offers Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function offers_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    add_actions( 'booking_popup_offers', 'booking_popup_offers_content' );

    set_template( TEMPLATE_PATH . '/template/offers_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('Additional Value Adds', 'Terms Conditions', 'Book Offer Now', 'Book Now', 'Price', 'Valid Until');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        $booking_link    = get_additional_field($post_id, 'booking_link', 'special-offers');
        $label_link      = get_additional_field($post_id, 'label_link', 'special-offers');
        $label_link_book = ((empty($label_link)) ? $tr_string['book_offer_now']: $label_link);
        $label_link_hero = ((empty($label_link)) ? $tr_string['book_now']:       $label_link);
        $form_type       = get_additional_field($post_id, 'form_type', 'special-offers');
        $form_type       = (empty($form_type) ? 2 : $form_type );

        if(empty($booking_link))
        {
            $button_book = '<button class="book-now" data-aos="fade-up" data-title="'.$post_title.'" data-form_type="'.$form_type.'">'.$label_link_book.'</button>';
            $button_book_hero = '
                <button class="container container-button-book-now book-now-hero clearfix" data-title="'.$post_title.'" data-form_type="'.$form_type.'">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </button>
            ';
        }
        else
        {
            $button_book = '<a href="'.$booking_link.'" class="book-now-link" data-aos="fade-up">'.$label_link_book.'</a>';
            $button_book_hero = '
                <a href="'.$booking_link.'" class="container container-button-book-now-link book-now-hero clearfix">
                    <p class="text _text text-book-now">'.$label_link_hero.'</p>
                </a>
            ';
        }
        add_variable( 'button_book', $button_book);
        add_actions( 'button_book_hero', $button_book_hero);


        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'special-offers' );
        $valid_date = get_additional_field( $post_id, 'valid_date', 'special-offers' );

        if(!empty($price))
        {
            $html_price = '
                <div class="container-price" data-aos="fade-up">
                    <h4>'.$tr_string['price'].'</h4>
                    <p>'.$price.'</p>
                </div>
            ';
            add_variable( 'html_price', $html_price);
        }

        if(!empty($valid_date))
        {
            $valid_date = date("d M Y", strtotime($valid_date));
            $html_date = '
                <div class="container-valid-date" data-aos="fade-up">
                    <h4>'.$tr_string['valid_until'].'</h4>
                    <p>'.$valid_date.'</p>
                </div>
            ';
            add_variable( 'html_date', $html_date);
        }

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);


        // GET ADDITIONAL VALUE CONTENT (RULE)
        $additional_adds_content = "";
        $additional_adds = get_rule_list_post($post_id, 'special-offers','additional_value', 1, $lang, $check_lang);
        if(!empty($additional_adds))
        {
            $additional_adds_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['additional_value_adds'].':</h3>
                    '.$additional_adds.'
                </div>
            ';
        }
        add_variable( 'additional_adds_content', $additional_adds_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'special-offers','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('special-offers', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, 'special-offers', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/special-offers/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'special-offers');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'special-offers');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'special-offers');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/special-offers';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/special-offers';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/special-offers/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);

        parse_template( 'page-block', 'pblock', false );
    }

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Events Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function events_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/events_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('Inclusion', 'Additional Service', 'Terms Conditions', 'Book Now');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $destination_id = $corporatedata[0]['id'];
    $bg_wedding_events_reservation      = get_additional_field( $destination_id, 'bg_image_events', 'destinations' );
    if(!empty($bg_wedding_events_reservation))
    {
        $bg_wedding_events_reservation = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_wedding_events_reservation;
    }
    else
    {
        $bg_wedding_events_reservation = HTTP.TEMPLATE_URL.'/assets/images/wedding.jpg';
    }
    add_variable( 'bg_wedding_events_reservation', $bg_wedding_events_reservation);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'events' );

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'price', $price);


        // GET INCLUSION (RULE)
        $inclusion_content = "";
        $inclusion = get_rule_list_post($post_id, 'events','inclusion', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $inclusion_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['inclusion'].':</h3>
                    '.$inclusion.'
                </div>
            ';
        }
        add_variable( 'inclusion_content', $inclusion_content);

        // GET ADDITIONAL SERVICE (RULE)
        $additional_content = "";
        $additional = get_rule_list_post($post_id, 'events','additional_service', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $additional_content = '
                <div class="additional-list clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['additional_service'].':</h3>
                    '.$additional.'
                </div>
            ';
        }
        add_variable( 'additional_content', $additional_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'events','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix" data-aos="fade-up">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('events', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'events', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/events/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'events');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'events');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'events');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/events/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/events';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/events';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/events/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/events/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);

        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix">
                <p class="text _text text-book-now">'.$tr_string['book_now'].'</p>
            </button>
        ';
        add_actions( 'button_book_hero', $button_book_hero);

        parse_template( 'page-block', 'pblock', false );
    }
    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Weddings Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function weddings_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/weddings_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('Inclusion', 'Additional Service', 'Terms Conditions');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    
    $destination_id = $corporatedata[0]['id'];
    add_actions( 'booking_popup_wedding', 'booking_popup_wedding_content', $destination, $destination_id );
    
    $bg_wedding_events_reservation      = get_additional_field( $destination_id, 'bg_image_events', 'destinations' );
    if(!empty($bg_wedding_events_reservation))
    {
        $bg_wedding_events_reservation = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_wedding_events_reservation;
    }
    else
    {
        $bg_wedding_events_reservation = HTTP.TEMPLATE_URL.'/assets/images/wedding.jpg';
    }
    add_variable( 'bg_wedding_events_reservation', $bg_wedding_events_reservation);

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode(" at ", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'weddings' );

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'price', $price);


        // GET INCLUSION (RULE)
        $inclusion_content = "";
        $inclusion = get_rule_list_post($post_id, 'weddings','inclusion', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $inclusion_content = '
                <div class="additional-list clearfix">
                    <h3>'.$tr_string['inclusion'].':</h3>
                    '.$inclusion.'
                </div>
            ';
        }
        add_variable( 'inclusion_content', $inclusion_content);

        // GET ADDITIONAL SERVICE (RULE)
        $additional_content = "";
        $additional = get_rule_list_post($post_id, 'weddings','additional_service', 1, $lang, $check_lang);
        if(!empty($inclusion))
        {
            $additional_content = '
                <div class="additional-list clearfix">
                    <h3>'.$tr_string['additional_service'].':</h3>
                    '.$additional.'
                </div>
            ';
        }
        add_variable( 'additional_content', $additional_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'weddings','terms_conditions', 1, $lang, $check_lang);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix">
                    <h3>'.$tr_string['terms_conditions'].':</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('weddings', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'weddings', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/weddings/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'weddings');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'weddings');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'weddings');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/weddings/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/weddings';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/weddings';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/weddings/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/weddings/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);

        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );

        parse_template( 'page-block', 'pblock', false );
    }
    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| WHAT TO DO Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function whattodo_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    // STRING TRANSLATIONS
    $translations = array('What To Do Act In', 'What To Do Around');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    set_template( TEMPLATE_PATH . '/template/whattodo_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];
        $post_type    = $post_data['post_type'];

        $type_for_tr_string = str_replace("-", "_", $post_type);
        $subtitle = $tr_string[$type_for_tr_string];

        // explode title
        $explode_title = explode(" at ", $post_title);

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'post_subtitle', $subtitle);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list($post_type, '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, $post_type, false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/'.$post_type.'/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, $post_type);
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, $post_type);
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, $post_type);
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$post_type.'/'.$sef_product.'.html';
                }


                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/'.$post_type;
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/'.$post_type;
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$post_type.'/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/'.$post_type.'/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| News Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function news_detail_content($destination, $corporatedata, $post_data='', $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/news_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $disqus_count_code = get_meta_data( 'disqus_count_code', 'static_setting' );  
    $disqus_script    = get_meta_data( 'disqus_script', 'static_setting' );

    add_variable( 'disqus_script', $disqus_script);
    add_actions( 'include-js', 'get_custom_javascript', $disqus_count_code, '', 1 );
    
    // STRING TRANSLATIONS
    $translations = array('Download This Press', 'Download');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    
    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_content = str_replace("<table", '<div class="table-responsive"><table class="responsive"', $post_content);
        $post_content = str_replace("</table>", '</table></div>', $post_content);
        $post_brief   = $post_data['post_brief'];
        $post_type    = $post_data['post_type'];
        
        $file_download = get_additional_field( $post_id, 'file_download', 'news' );

        add_actions( 'button_download_hero', 'get_button_download_hero', $file_download, $tr_string['download_this_press'] );


        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'button_download_file', get_button_download_hero($file_download, $tr_string['download']));

        // GET OTHER NEWS
        $where_add = " AND larticle_id != $post_id AND a.lstatus_file_download=1";
        $product = get_post_list('news', '', $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $id_product      = $d['post_id'];
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $id_product, 'news', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/news/'.$sef_product.'.html';

                if($check_lang)
                {
                    $title_product_lang   = get_additional_field($id_product, 'title_'.$lang, 'news');
                    $brief_product_lang   = get_additional_field($id_product, 'brief_'.$lang, 'news');
                    $content_product_lang = get_additional_field($id_product, 'content_'.$lang, 'news');
                    
                    $title_product   = (empty($title_product_lang) ? $title_product:     $title_product_lang);
                    $brief_product   = (empty($brief_product_lang) ? $brief_product:     $brief_product_lang);
                    $content_product = (empty($content_product_lang) ? $content_product: $content_product_lang);

                    $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/news/'.$sef_product.'.html';
                }

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        $link_explore  = HTTP.site_url().'/'.$destination.'/news';
        if($check_lang)
        {
            $link_explore  = HTTP.site_url().'/'.$lang.'/'.$destination.'/news';
        }
        add_variable( 'link_explore', $link_explore);

        $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/news/'.$post_data['post_sef'] : HTTP . site_url().'/'.$destination.'/news/'.$post_data['post_sef']);
        config_meta_data($page_url, $post_data);


        parse_template( 'page-block', 'pblock', false );
    }

    return return_template( 'page_template' );
}

function get_button_download_hero($file_download, $text_download)
{
    $button = "";

    if( !empty( $file_download ) && file_exists( PLUGINS_PATH . '/additional/file_download/' . $file_download ) )
    {
        $button = '
            <div class="detail-download-item">
                <a target="_blank" href="'.URL_PLUGINS . 'additional/file_download/' . $file_download.'">'.$text_download.'</a>
            </div>
        ';
    }

    return $button;
}
?>