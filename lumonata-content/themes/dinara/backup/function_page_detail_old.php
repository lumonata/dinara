<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN TITLE HERO PADA DETAIL PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_data_detail_page($sef, $destination, $type)
{
    global $db;

    $result = array();

    $data   = get_post_detail( $sef, $type );

    if(!empty($data))
    {
        extract( $data );

        $post_title_new = $post_title;

        if($type != "spa")
        {
            if(strpos($post_title, 'of'))
            {
                $post_title_new = explode_title_by_at_of("of", $post_title_new);
            }
            elseif(strpos($post_title_new, 'at'))
            {
                $post_title_new = explode_title_by_at_of("at", $post_title_new);
            }
        }

        // GET SLIDER HERO PAGE
        if($type == "accommodation" || $type == "dining" || $type == "what-to-do-act-in" || $type == "what-to-do-around" || $type == "events")
        {
            if(!empty($post_attachment))
            {
                add_actions( 'slider_hero_page', 'get_slider_hero_page', $post_attachment, $post_title );
            }
        }


        if(strpos($type, '-'))
        {
            $explode_type = explode("-", $type);
            $type = "";
            foreach($explode_type as $d)
            {
                $type .= $d." ";
            }
        }

        $subtitle = (empty($post_subtitle) ? strtoupper($type) : $post_subtitle);

        $result['title_hero_template'] = '
            <div class="container container-title-hero-detail-page clearfix">
                <h2 class="text _text sub-title-detail-page">'.$subtitle.'</h2>
                <h1 class="text _text title-detail-page-1">'.$post_title_new.'</h1>
                <!--<p class="text _text title-detail-page-2">COURTYARD VILLA</p>-->
            </div>
        ';
        
        $result['bg_hero'] = "";
        $result['post_title']   = $post_title;
        $result['post_content'] = $post_content;
        $result['post_brief']   = $post_brief;
        $result['post_id']      = $post_id;
        $result['post_type']    = $post_type;
        

        if(!empty($post_featured_img))
        {
            $result['bg_hero']      = HTTP.site_url().$post_featured_img['original'];
        }

        
    }


    return $result;
}


function get_slider_hero_page($attachment='', $title_page)
{
    set_template( TEMPLATE_PATH . '/layout/header_slide.html', 'header_slide_template' );
    add_block( 'loop-popup-slide-block', 'lpsblock', 'header_slide_template' );
    add_block( 'loop-header-slide-block', 'lhsblock', 'header_slide_template' );
    add_block( 'header-slide-block', 'hsblock', 'header_slide_template' );

    set_switch_language();

    add_variable( 'title_page_popup', $title_page);

    if(!empty($attachment))
    {
        foreach($attachment as $d)
        {
            $img_large = $d['img_large'];

            add_variable( 'img_large', $img_large);

            parse_template( 'loop-popup-slide-block', 'lpsblock', true );
            parse_template( 'loop-header-slide-block', 'lhsblock', true );
        }
    }

    parse_template( 'header-slide-block', 'hsblock', false );
    
    return return_template( 'header_slide_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Accomodation Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function accommodation_detail_content($destination, $corporatedata, $post_data='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/accommodation_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // ADDITIONAL FIELD
        $size         = get_additional_field( $post_id, 'size', 'accommodation' );
        $occupancy    = get_additional_field( $post_id, 'occupancy', 'accommodation' );
        $bed          = get_additional_field( $post_id, 'bed', 'accommodation' );
        $features     = get_additional_field( $post_id, 'features', 'accommodation' );

        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'size', $size);
        add_variable( 'occupancy', $occupancy);
        add_variable( 'bed', $bed);
        add_variable( 'features', $features);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('accommodation', 6, $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'accommodation', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/accommodation/'.$sef_product.'.html';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Dining Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function dining_detail_content($destination, $corporatedata, $post_data='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/dining_detail.html', 'page_template' );
    add_block( 'loop-cat-menu-block', 'lcmblock', 'page_template' );
    add_block( 'menu-dining-block', 'mdblock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode("at", $post_title);
        
        // ADDITIONAL FIELD
        $breakfast_open     = get_additional_field( $post_id, 'breakfast_open', 'dining' );
        $breakfast_open     = (empty($breakfast_open) ? "": "<p>Breakfast $breakfast_open</p>");
        $lunch_open         = get_additional_field( $post_id, 'lunch_open', 'dining' );
        $lunch_open         = (empty($lunch_open) ? "":     "<p>Lunch $lunch_open</p>");
        $dining_open        = get_additional_field( $post_id, 'dining_open', 'dining' );
        $dining_open        = (empty($dining_open) ? "":    "<p>Dining $dining_open</p>");
        $restaurant_open    = $breakfast_open.$lunch_open.$dining_open;
        $dresscode          = get_additional_field( $post_id, 'dresscode', 'dining' );
        $our_menu_desc      = get_additional_field( $post_id, 'our_menu_desc', 'dining' );
        $menu_pdf           = get_additional_field( $post_id, 'menu_pdf', 'dining' );
        $menu_pdf_text_link = get_additional_field( $post_id, 'menu_pdf_text_link', 'dining' );

        $menu_pdf_download = "";
        if(!empty($menu_pdf) && file_exists( PLUGINS_PATH . '/menus/pdf/' . $menu_pdf ))
        {
            $menu_pdf_download = '
                <div class="container-download-menu">
                    <a href="'.URL_PLUGINS . 'menus/pdf/' . $menu_pdf.'" target="_blank">
                        <p>Download Our Menu (PDF)</p>
                    </a>
                </div>
            ';
        }
        elseif(!empty($menu_pdf_text_link))
        {
            $menu_pdf_download = '
                <div class="container-download-menu">
                    <a href="'.$menu_pdf_text_link.'" target="_blank">
                        <p>Download Our Menu (PDF)</p>
                    </a>
                </div>
            ';
        }

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'restaurant_open', $restaurant_open);
        add_variable( 'dresscode', $dresscode);
        add_variable( 'our_menu_desc', $our_menu_desc);
        add_variable( 'menu_pdf_download', $menu_pdf_download);


        // GET CATEGORY
        $qcmenu = $db->prepare_query("
            SELECT DISTINCT a.*
            FROM lumonata_menu_category AS a
            JOIN lumonata_menu AS b
                ON (a.lcmenu_id=b.lcmenu_id)
            WHERE a.larticle_id = %d
            ORDER BY lorder
        ", $post_id);
        $rcmenu = $db->do_query($qcmenu);
        $ncmenu = $db->num_rows($rcmenu);

        if($ncmenu > 0)
        {  
            $icm = 1;
            $menu_type_all = "";
            $category_li_html = "";

            while($dcmenu = $db->fetch_array($rcmenu))
            {
                $category_id       = $dcmenu['lcmenu_id'];
                $category_name     = $dcmenu['lcmenu_name'];
                $category_sanitize = generateSefUrl($dcmenu['lcmenu_name']);

                add_variable('category_sanitize', $category_sanitize);
                
                if($icm == 1)
                {
                    $class_active = "active";
                    $category_li_html .= '<li class="active" data-filter="'.$category_sanitize.'">'.$category_name.'</li>';
                }
                else
                {
                    $class_active = "";
                    $category_li_html .= '<li data-filter="'.$category_sanitize.'">'.$category_name.'</li>';
                }

                add_variable('class_active', $class_active);

                // START GET MENU LIST
                $qmenu = $db->prepare_query("
                    SELECT *
                    FROM lumonata_menu
                    WHERE lcmenu_id = %d
                    ORDER BY lorder
                ", $category_id);
                $rmenu = $db->do_query($qmenu);
                $nmenu = $db->num_rows($rmenu);

                if($nmenu > 0)
                {
                    $menu_type = [];
                    $test      = array(1,3,5,7,9);
                    $test2     = array(1,3,5,7,9);

                    $list_item_menu_html = "";
                    
                    while($dmenu = $db->fetch_array($rmenu))
                    {
                        if(!empty($dmenu['lmenu_type']))
                        {
                            $menu_name      = $dmenu['lmenu_name'];
                            $menu_desc      = $dmenu['lmenu_desc'];
                            $menu_type_json = json_decode($dmenu['lmenu_type']);
                            $menu_type      = array_merge($menu_type, $menu_type_json);

                            $class_menu_list = "";
                            foreach($menu_type_json as $mtj)
                            {
                                $class_menu_list .= $mtj." ";
                            }

                            $list_item_menu_html .= '
                                <div class="list-item-menu '.$class_menu_list.'">
                                    <h4>'.$menu_name.'</h4>
                                    <p>'.$menu_desc.'</p>
                                </div>
                            ';
                        }
                    }

                    add_variable('list_item_menu_html', $list_item_menu_html);
                }
                // END GET MENU LIST


                // START GET MENU TYPE
                if(!empty($menu_type))
                {
                    $menu_type_dining = get_menu_type_dining($menu_type);
                    $itp = 1;

                    $menu_type_html = "<ul class='clearfix'>";
                    foreach($menu_type_dining as $mt)
                    {
                        $type_name = str_replace("-", " ", $mt);
                        
                        if($itp == 1)
                        {
                            $menu_type_html .= '<li data-filter="'.$mt.'" class="active">'.$type_name.'</li>';
                        }
                        else
                        {
                            $menu_type_html .= '<li data-filter="'.$mt.'">'.$type_name.'</li>';
                        }
                        $itp++;
                    }
                    $menu_type_html .= "<ul>";

                    add_variable('menu_type_html', $menu_type_html);
                    
                }
                // END GET MENU TYPE

                parse_template('loop-cat-menu-block', 'lcmblock', true);

                add_variable('category_li_html', $category_li_html);

                $icm++;
            }
            
             
            

            parse_template('menu-dining-block', 'mdblock', false);
        }
        else
        {
            add_variable( 'hide', 'hide');
        }



        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('dining', 6, $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'dining', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/dining/'.$sef_product.'.html';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NILAI MENU TYPE DAN MENGHAPUS VALUE YANG SAMA
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_menu_type_dining($menu_type_all)
{
    $last_menu_type   = array();
    $menu_type_before = "";

    foreach($menu_type_all as $d)
    {
        $menu_type = $d;
        if($menu_type != $menu_type_before)
        {
            $last_menu_type[] = $menu_type;
        }
        $menu_type_before = $menu_type;
    }

    return $last_menu_type;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Dining Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function spa_detail_content($destination, $corporatedata, $post_data='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/spa_detail.html', 'page_template' );
    add_block( 'loop-cat-treatment-block', 'lctblock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode("at", $post_title);
        
        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);


        // GET SPA CATEGORY LIST
        $qcat = $db->prepare_query("
            SELECT * 
            FROM lumonata_treatment_category
            WHERE larticle_id = %d
        ", $post_id);
        $rcat = $db->do_query($qcat);
        $ncat = $db->num_rows($rcat);

        if($ncat > 0)
        {
            while($d = $db->fetch_array($rcat))
            {
                $cat_id   = $d['lctreatment_id'];
                $cat_name = $d['lctreatment_name'];
                $cat_desc = $d['lctreatment_desc'];

                add_variable( 'cat_name', $cat_name);
                add_variable( 'cat_desc', $cat_desc);

                // GET LIST TREATMENT
                $qtreatment = $db->prepare_query("
                    SELECT *
                    FROM lumonata_treatment
                    WHERE lctreatment_id = %d
                ", $cat_id);
                $rtreatment = $db->do_query($qtreatment);
                $ntreatment = $db->num_rows($rtreatment);

                $content_treatment_list = "";

                if($ntreatment > 0)
                {
                    while($dtreatment = $db->fetch_array($rtreatment))
                    {
                        $treatment_id    = $dtreatment['ltreatment_id'];
                        $treatment_name  = $dtreatment['ltreatment_name'];
                        $treatment_price = $dtreatment['ltreatment_price'];

                        $content_treatment_list .= '
                            <div class="list-treatment clearfix">
                                <div class="left">
                                    <p>'.$treatment_name.'</p>
                                </div>
                                <div class="right clearfix">
                                    <p>Price: '.$treatment_price.'</p>
                                    <button class="book-now-treatment">BOOK NOW</button>
                                </div>
                            </div>
                        ';
                    }

                    add_variable( 'content_treatment_list', $content_treatment_list);
                }

                parse_template('loop-cat-treatment-block', 'lctblock', true);
            }
        }

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('spa', 6, $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'spa', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/spa/'.$sef_product.'.html';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}



/*
| -------------------------------------------------------------------------------------------------------------------------
| Special Offers Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function offers_detail_content($destination, $corporatedata, $post_data='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/offers_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode("at", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'special-offers' );
        $valid_date = date("d M Y", strtotime(get_additional_field( $post_id, 'valid_date', 'special-offers' )));

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'price', $price);
        add_variable( 'valid_date', $valid_date);


        // GET ADDITIONAL VALUE CONTENT (RULE)
        $additional_adds_content = "";
        $additional_adds = get_rule_list_post($post_id, 'special-offers','additional_value', 1);
        if(!empty($additional_adds))
        {
            $additional_adds_content = '
                <div class="additional-list clearfix">
                    <h3>ADDITIONAL VALUE ADDS:</h3>
                    '.$additional_adds.'
                </div>
            ';
        }
        add_variable( 'additional_adds_content', $additional_adds_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'special-offers','terms_conditions', 1);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix">
                    <h3>TERMS AND CONDITIONS (PLEASE READ ALL THE INFORMATION):</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('special-offers', 6, $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'special-offers', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/special-offers/'.$sef_product.'.html';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        parse_template( 'page-block', 'pblock', false );
    }

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| Events Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function events_detail_content($destination, $corporatedata, $post_data='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/events_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];

        // explode title
        $explode_title = explode("at", $post_title);
        
        // ADDITIONAL FIELD
        $price      = get_additional_field( $post_id, 'price', 'events' );

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);
        add_variable( 'price', $price);


        // GET INCLUSION (RULE)
        $inclusion_content = "";
        $inclusion = get_rule_list_post($post_id, 'events','inclusion', 1);
        if(!empty($inclusion))
        {
            $inclusion_content = '
                <div class="additional-list clearfix">
                    <h3>INCLUSION:</h3>
                    '.$inclusion.'
                </div>
            ';
        }
        add_variable( 'inclusion_content', $inclusion_content);

        // GET ADDITIONAL SERVICE (RULE)
        $additional_content = "";
        $additional = get_rule_list_post($post_id, 'events','additional_service', 1);
        if(!empty($inclusion))
        {
            $additional_content = '
                <div class="additional-list clearfix">
                    <h3>ADDITIONAL SERVICE:</h3>
                    '.$additional.'
                </div>
            ';
        }
        add_variable( 'additional_content', $additional_content);

        // GET TERMS AND CONDITIONS (RULE)
        $terms_conditions_content = "";
        $terms_conditions = get_rule_list_post($post_id, 'events','terms_conditions', 1);
        if(!empty($terms_conditions))
        {
            $terms_conditions_content = '
                <div class="terms-conditions clearfix">
                    <h3>TERMS AND CONDITIONS (PLEASE READ ALL THE INFORMATION):</h3>
                    '.$terms_conditions.'
                </div>
            ';
        }
        add_variable( 'terms_conditions_content', $terms_conditions_content);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list('events', 6, $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], 'events', false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/events/'.$sef_product.'.html';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        parse_template( 'page-block', 'pblock', false );
    }
    
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| WHAT TO DO Detail Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function whattodo_detail_content($destination, $corporatedata, $post_data='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/whattodo_detail.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    if(!empty($post_data))
    {
        $post_id      = $post_data['post_id'];
        $post_title   = $post_data['post_title'];
        $post_content = $post_data['post_content'];
        $post_brief   = $post_data['post_brief'];
        $post_type    = $post_data['post_type'];

        // explode title
        $explode_title = explode("at", $post_title);

        add_variable( 'title_1_word', $explode_title[0]);
        add_variable( 'post_title', $post_title);
        add_variable( 'post_content', $post_content);
        add_variable( 'post_brief', $post_brief);

        // GET OTHER PRODUCT
        $where_add = " AND larticle_id != $post_id";
        $product = get_post_list($post_type, 6, $where_add, $destination);

        if(count($product) > 0)
        {
            foreach($product as $d)
            {
                $title_product   = $d['post_title'];
                $sef_product     = $d['post_sef'];
                $brief_product   = $d['post_brief'];
                $content_product = $d['post_content'];
                $image_product   = get_featured_img( $d['post_id'], $post_type, false );
                $image_product   = HTTP . site_url().$image_product['medium'];
                $post_link       = HTTP . site_url().'/'.$destination.'/'.$post_type.'/'.$sef_product.'.html';

                add_variable('title_product', $title_product);
                add_variable('content_product', $content_product);
                add_variable('brief_product', $brief_product);
                add_variable('image_product', $image_product);
                add_variable('sef_product', $sef_product);
                add_variable('post_link', $post_link);

                parse_template('loop-list-block', 'llblock', true);
            }
        }

        parse_template( 'page-block', 'pblock', false );
    }

    
    return return_template( 'page_template' );
}

?>