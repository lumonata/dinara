<?php
/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN FORM BOOKING DI HERO PADA PAGE
| -----------------------------------------------------------------------------
*/
function form_booking_hero_page($dest_id='')
{
    global $db;
    set_template( TEMPLATE_PATH . '/layout/form_booking_hero_page.html', 'fbh-template' );
    add_block( 'section-form-booking', 'sfb-block', 'fbh-template' );

    add_variable( 'value_json_booking', attemp_actions( 'value_json_booking' ) );
    add_variable( 'button_book_hero', attemp_actions( 'button_book_hero' ) );

    // FAST BOOKING CONFIGURATION
    if(!empty($dest_id))
    {

	    add_variable( 'properties_name', attemp_actions( 'properties_name' ) );
        add_variable( 'hotels_name', attemp_actions( 'hotels_name' ) );
    }

    parse_template( 'section-form-booking', 'sfb-block', false );
    return return_template( 'fbh-template' );
}

add_actions( 'booking_popup_accommodation', 'booking_popup_accommodation_content' );
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP ACCOMMODATION
| -------------------------------------------------------------------------------------------------------------------------
*/
function booking_popup_accommodation_content()
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/booking_accommodation_popup.html', 'popup_template' );
    add_block( 'popup-block', 'ppblock', 'popup_template' );

    set_switch_language();

    add_variable( 'properties_name', attemp_actions( 'properties_name' ) );
    add_variable( 'hotels_name', attemp_actions( 'hotels_name' ) );
    add_variable( 'dest_list', attemp_actions( 'dest_list' ) );
    add_variable( 'dest_hidden', attemp_actions( 'dest_hidden' ) );
    add_variable( 'app_name', attemp_actions( 'app_name' ) );

    parse_template( 'popup-block', 'ppblock', false );

    return return_template( 'popup_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP SPA
| -------------------------------------------------------------------------------------------------------------------------
*/
function booking_popup_wedding_content($destination='', $destination_id='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/booking_wedding_popup.html', 'popup_wedding_template' );
    add_block( 'popup-wedding-block', 'ppwblock', 'popup_wedding_template' );

    set_switch_language();

    $lang                          = attemp_actions( 'lang' );
    $check_lang                    = attemp_actions( 'check_lang' );

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    add_variable( 'option_spa', attemp_actions( 'option_spa' ) );
    add_variable( 'lang', $lang );
    add_variable( 'check_lang', $check_lang );

    $captcha_site_key   = get_meta_data( 'r_public_key', 'static_setting' );
    add_variable( 'captcha_site_key', $captcha_site_key );

    // TEXT ADDITIONAL BOOKING SPA POPUP
    $post_type_setting             = get_meta_data('post_type_setting', 'weddings', $destination_id);
    $post_type_setting             = json_decode($post_type_setting, true);
    $spa_reservation_email_address = get_additional_field($destination_id, 'spa_reservation_email_address', 'destinations');
    $title_reception_venue_1       = get_additional_field($destination_id, 'title_reception_venue_1', 'destinations');
    $dinner_reception_venue_1       = get_additional_field($destination_id, 'dinner_reception_venue_1', 'destinations');
    $title_reception_venue_2       = get_additional_field($destination_id, 'title_reception_venue_2', 'destinations');
    $dinner_reception_venue_2       = get_additional_field($destination_id, 'dinner_reception_venue_2', 'destinations');

    if($check_lang)
    {
        $title_reception_venue_1_lang = get_additional_field($destination_id, 'title_reception_venue_1_'.$lang, 'destinations');
        $title_reception_venue_1      = (empty($title_reception_venue_1_lang) ? $title_reception_venue_1: $title_reception_venue_1_lang);
        $title_reception_venue_2_lang = get_additional_field($destination_id, 'title_reception_venue_2_'.$lang, 'destinations');
        $title_reception_venue_2      = (empty($title_reception_venue_2_lang) ? $title_reception_venue_2: $title_reception_venue_2_lang);
    }

    add_variable( 'title_reception_venue_1', $title_reception_venue_1 );
    add_variable( 'title_reception_venue_2', $title_reception_venue_2 );
    add_variable( 'dinner_reception_venue_1', HTTP.site_url().'/lumonata-plugins/destinations/reception-venue/'.$dinner_reception_venue_1 );
    add_variable( 'dinner_reception_venue_2', HTTP.site_url().'/lumonata-plugins/destinations/reception-venue/'.$dinner_reception_venue_2 );


    // GET LIST OCCASION TYPE
    $occasion_type      = get_rule_list_front("weddings", "occasion_type", $lang, $check_lang, false);
    $occasion_type_html = "";
    $io = 1;
    foreach($occasion_type as $od)
    {
        $checked  = "";
        if($io == 1){
            $checked = "checked";
        }
        $occasion_type_html .= '
            <div class="list-occasion-type clearfix">
                <input type="checkbox" name="occasion_type" value="'.$od.'"> <span>'.$od.'</span>
            </div>
        ';
        $io++;
    }
    add_variable( 'occasion_type_html', $occasion_type_html );

    // GET LIST SPECIAL REQUEST
    $special_request      = get_rule_list_front("weddings", "special_request", $lang, $check_lang, false);
    $special_request_html = "";
    $is = 1;
    foreach($special_request as $sr)
    {
        $checked  = "";
        if($is == 1){
            $checked = "checked";
        }
        $special_request_html .= '
            <div class="list-special-request clearfix">
                <input type="checkbox" name="special_request" value="'.$sr.'"> <span>'.$sr.'</span>
            </div>
        ';
        $is++;
    }
    add_variable( 'special_request_html', $special_request_html );

    $country_option = get_data_country();
    add_variable( 'country_option', $country_option );

    parse_template( 'popup-wedding-block', 'ppwblock', false );

    return return_template( 'popup_wedding_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP SPA
| -------------------------------------------------------------------------------------------------------------------------
*/
function booking_popup_spa_content($destination='', $destination_id='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/booking_spa_popup.html', 'popup_spa_template' );
    add_block( 'popup-spa-block', 'ppsblock', 'popup_spa_template' );

    set_switch_language();

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    add_variable( 'option_spa', attemp_actions( 'option_spa' ) );

    $captcha_site_key   = get_meta_data( 'r_public_key', 'static_setting' );
    add_variable( 'captcha_site_key', $captcha_site_key );

    // TEXT ADDITIONAL BOOKING SPA POPUP
    $post_type_setting             = get_meta_data('post_type_setting', 'spa', $destination_id);
    $post_type_setting             = json_decode($post_type_setting, true);
    $text_additional_book          = $post_type_setting['text_additional_book'];
    $lang                          = attemp_actions( 'lang' );
    $check_lang                    = attemp_actions( 'check_lang' );
    $spa_reservation_email_address = get_additional_field($destination_id, 'spa_reservation_email_address', 'destinations');

    add_variable( 'lang', $lang );
    add_variable( 'check_lang', $check_lang );


    if($check_lang){
        $text_additional_book_lang  = (isset($post_type_setting['text_additional_book_'.$lang]) ? $post_type_setting['text_additional_book_'.$lang]: '' );
        $text_additional_book       = (empty($text_additional_book_lang) ? $text_additional_book: $text_additional_book_lang);
    }
    $text_additional_book = str_replace('[email_reservations_spa]', $spa_reservation_email_address, $text_additional_book);
    add_variable( 'text_additional_book', $text_additional_book );


    // OPTION TIME HOURS & MINUTE
    $option_time_hour = "";
    $max_time_hour = 20;
    $max_time_hour = (($destination == "ubud") ? 22 : $max_time_hour);
    $format_time = "AM";
    for($i=9; $i<=$max_time_hour; $i++)
    {
        $new_i = $i;
        if($new_i > 12){
            $new_i = $new_i - 12;
            $format_time = "PM";
        }

        // $text_hour = (($i > 1) ? "Hours" : "Hour");
        $new_i = (($new_i < 10) ? "0$new_i" : "$new_i");


        $option_time_hour .= '<option value="'.$i.'">'.$new_i.' '.$format_time.'</option>';
    }
    add_variable( 'option_time_hour', $option_time_hour );

    $option_time_minute = "";
    $minute = 0;
    do{
        $new_minute = (($minute == 0) ? "0$minute" : "$minute");
        $option_time_minute .= '<option value="'.$minute.'">'.$new_minute.'</option>';
        $minute+=15;
    }while( $minute <= 45);
    add_variable( 'option_time_minute', $option_time_minute );

    $country_option = get_data_country();
    add_variable( 'country_option', $country_option );

    parse_template( 'popup-spa-block', 'ppsblock', false );

    return return_template( 'popup_spa_template' );
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING SPA FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function booking_spa_form_action($post_data)
{
    global $db;

    if(isset($post_data['pkey']) && !empty($post_data['value']))
    {
        if($post_data['pkey']=='choose_spa')
        {
            $key       = $post_data['value'];
            $decode    = json_decode(base64_decode($key), true);
            $id        = $decode['key'];
            $cat_id    = ((!empty(isset($decode['cat_id']))) ? $decode['cat_id']:     '');
            $treat_id  = ((!empty(isset($decode['treat_id']))) ? $decode['treat_id']: '');
            $category  = get_treatment_category_list($id);

            if(!empty($cat_id))
            {
                $treatment = get_treatment_list($cat_id, $treat_id);
                $result['treatment_price']  = $treatment['price'];
                $result['treatment_tax']    = $treatment['tax'];
                $result['treatment_total']  = ($treatment['price']*$post_data['no_of_person'])+($treatment['tax']*$post_data['no_of_person']);
            }
            else
            {
                if(isset($category['category_id']))
                {
                    $treatment = get_treatment_list($category['category_id'], $treat_id);
                    $result['treatment_price']  = 0;
                    $result['treatment_tax']    = 0;
                    $result['treatment_total']  = 0;
                }
                else{
                    $treatment['html'] = "";
                }
            }

            // print_r($treatment);

            $result['status']           = "success";
            $result['key']              = $id;
            $result['option']           = $category['html'];
            $result['option_treatment'] = $treatment['html'];
            $result['cat_id']           = $cat_id;
            $result['treat_id']         = $treat_id;
        }
        elseif($post_data['pkey']=='change_spa')
        {
            $id         = $post_data['value'];
            $lang       = $post_data['lang'];
            $check_lang = $post_data['check_lang'];

            $category  = get_treatment_category_list($id, $lang, $check_lang);
            if(isset($category['category_id']))
            {
                $treatment = get_treatment_list($category['category_id'], '', $lang, $check_lang);
            }
            else{
                $treatment['html'] = "";
            }

            $result['status']           = "success";
            $result['option']           = $category['html'];
            $result['option_treatment'] = $treatment['html'];
            $result['treatment_price']  = 0;
            $result['treatment_tax']    = 0;
            $result['treatment_total']  = 0;
            // $result['treatment_price']  = $treatment['price'];
            // $result['treatment_tax']    = $treatment['tax'];
            // $result['treatment_total']  = ($treatment['price']*$post_data['no_of_person'])+($treatment['tax']*$post_data['no_of_person']);
        }
        elseif($post_data['pkey']=='change_treatment_category')
        {
            $id         = $post_data['value'];
            $lang       = $post_data['lang'];
            $check_lang = $post_data['check_lang'];

            $treatment = get_treatment_list($id, '', $lang, $check_lang);

            $result['status']           = "success";
            $result['option_treatment'] = $treatment['html'];
            $result['treatment_price']  = 0;
            $result['treatment_tax']    = 0;
            $result['treatment_total']  = 0;
            // $result['treatment_price']  = $treatment['price'];
            // $result['treatment_tax']    = $treatment['tax'];
            // $result['treatment_total']  = ($treatment['price']*$post_data['no_of_person'])+($treatment['tax']*$post_data['no_of_person']);
        }
        elseif($post_data['pkey']=='change_treatment_list')
        {
            $id        = $post_data['value'];
            $treatment = get_treatment_price_tax($id);

            $result['status']          = "success";
            $result['treatment_price'] = $treatment['price'];
            $result['treatment_tax']   = $treatment['tax'];
            $result['treatment_total'] = ($treatment['price']*$post_data['no_of_person'])+($treatment['tax']*$post_data['no_of_person']);
        }
    }
    else
    {
        $result['status']   = 'error';
        $result['message']  = '<h3><b>Error!</b> Something went wrong, please try again later.</h3>';
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE SEND EMAIL ADMIN
| -----------------------------------------------------------------------------
*/
function spa_reservation_email_template_admin($post_data)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/mail/mail_spa_respond_admin.html', 'mail_template' );
    add_block( 'main-block', 'mblock', 'mail_template' );

    add_variable_spa_mail($post_data, 'admin');

    parse_template( 'main-block', 'mblock', false );

    return return_template( 'mail_template' );
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE SEND EMAIL CUSTOMER
| -----------------------------------------------------------------------------
*/
function spa_reservation_email_template_customer($post_data)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/mail/mail_spa_respond_customer.html', 'mail_template_cus' );
    add_block( 'main-cus-block', 'mcblock', 'mail_template_cus' );

    add_variable_spa_mail($post_data, 'customer');

    parse_template( 'main-cus-block', 'mcblock', false );

    return return_template( 'mail_template_cus' );
}

function add_variable_spa_mail($post_data,$type='admin')
{
    $hidden = 'style="display:none;"';
    $treatment_price    = $post_data['treatment_price'];
    $treatment_tax      = $post_data['treatment_tax'];
    $no_of_person       = $post_data['no_of_person'];

    if(!empty($treatment_price))
    {
        $total_cost     = $treatment_price*$no_of_person;
        $tax_service    = $treatment_tax*$no_of_person;
        $total_charge   = $total_cost+$tax_service;
        $hidden         = "";

        add_variable('total_cost', 'Rp '. number_format($total_cost, 0));
        add_variable('tax_service', 'Rp '. number_format($tax_service,0));
        add_variable('total_charge', 'Rp '. number_format($total_charge,0));
    }

    $message_top = 'Thank you for making reservation in Spa The Samaya Bali site';
    $title_profile = 'Your Details';
    if($type=="admin")
    {
        $title_profile = 'Customer Details';
        $message_top = 'You have a new spa inquiry from the website.';
    }

    add_variable('title_profile', $title_profile);
    add_variable('message_top', $message_top);
    add_variable('hidden', $hidden);
    add_variable('name', $post_data['name']);
    add_variable('nationality', $post_data['nationality']);
    add_variable('email', $post_data['email']);
    add_variable('phone', $post_data['phone']);
    add_variable('contact_you', $post_data['contact_you']);
    add_variable('spa_name', $post_data['spa_name']);
    add_variable('treatment_category_name', $post_data['treatment_category_name']);
    add_variable('treatment_list_name', $post_data['treatment_list_name']);
    add_variable('no_of_person', $post_data['no_of_person']);
    add_variable('promotion_code', $post_data['promotion_code']);
    add_variable('date', $post_data['date']);
    add_variable('time', $post_data['time']);
    add_variable('where_staying', $post_data['where_staying']);
    add_variable('special_request', $post_data['special_request']);
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING SPA FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function booking_spa_inquiry($post_data)
{
    global $db;
    $result['status']   = 'error asdasd';
    $result['message']  = '<h3><b>Error!</b> Something went wrong, please try again later.</h3>';

    if (isset($post_data['recaptcha']) && !empty($post_data['recaptcha'])) {
        $params             = array();
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $params['secret']   = get_meta_data('r_secret_key', 'static_setting');
        $params['response'] = (!empty($post_data['recaptcha']) ? urlencode($post_data['recaptcha']) : '');

        $prm_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $prm_string;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $data = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($data);

        if ($response->success)
        {

            if(!$post_data['understand'])
            {
                $result['status']   = 'warning';
                $result['message']  = '<h3><b>Warning!</b> Please check Terms & Conditions.</h3>';
            }
            else
            {

                $destination        = $post_data['destination'];
                $dest_id            = get_id_article_by_sef($destination, 'destinations');
                $spa                = $post_data['spa'];
                $treatment_category = $post_data['treatment_category'];
                $treatment_list     = $post_data['treatment_list'];
                $no_of_person       = $post_data['no_of_person'];
                $promotion_code     = empty($post_data['promotion_code']) ? '-' : $post_data['promotion_code'];
                $hours              = $post_data['hours']<=9 ? "0".$post_data['hours'] : $post_data['hours'];
                $minutes            = $post_data['minutes']<=9 ? "0".$post_data['minutes'] : $post_data['minutes'];
                $time               = $hours.":".$minutes;
                $where_staying      = $post_data['where_staying'];
                $special_request    = $post_data['special_request'];
                $name               = ucwords($post_data['name']);
                $nationality        = $post_data['nationality'];
                $email              = $post_data['email'];
                $phone              = $post_data['phone'];
                $contact_you        = ucwords($post_data['contact_you']);
                $date               = date("d M Y", strtotime($post_data['date']));

                // GET NAME SPA
                $spa_name                = ucwords(strtolower(get_name_spa_by_id($spa)));
                $treatment_category_name = ucwords(strtolower(get_name_treatment_category_by_id($treatment_category)));
                $treatment_list_name     = get_name_treatment_by_id($treatment_list);
                $treatment               = get_treatment_price_tax($treatment_list);
                $treatment_price         = $treatment['price'];
                $treatment_tax           = $treatment['tax'];

                $data_post['spa_name']                  = $spa_name;
                $data_post['treatment_category_name']   = $treatment_category_name;
                $data_post['treatment_list_name']       = $treatment_list_name;
                $data_post['treatment_price']           = $treatment_price;
                $data_post['treatment_tax']             = $treatment_tax;
                $data_post['no_of_person']              = $no_of_person;
                $data_post['promotion_code']            = $promotion_code;
                $data_post['time']                      = $time;
                $data_post['where_staying']             = $where_staying;
                $data_post['special_request']           = $special_request;
                $data_post['name']                      = $name;
                $data_post['nationality']               = $nationality;
                $data_post['email']                     = $email;
                $data_post['phone']                     = $phone;
                $data_post['contact_you']               = $contact_you;
                $data_post['date']                      = $date;

                $subject        = "Spa Inquiry - Samaya - $name";
                $cont_email     = get_additional_field($dest_id, 'spa_reservation_email_address', 'destinations');
                $web_name       = get_meta_data( 'web_title', 'static_setting' );
                $msg_body       = spa_reservation_email_template_admin($data_post);

                $send_email = send_email($email, $name, $cont_email, $web_name, $subject, $msg_body);

                if($send_email)
                {
                    $msg_body_cus = spa_reservation_email_template_customer($data_post);
                    $send_email = send_email($cont_email, $name, $email, $web_name, $subject, $msg_body_cus);

                    $result['status']   = 'success';
                    $result['message']  = '<h3><b>Success!</b> Your inquiry has been sent.</h3>';
                }
                else
                {
                    $result['status']   = 'error';
                    $result['message']  = '<h3><b>Danger!</b> Sorrysss your message cannot be send, please try again later.</h3>';
                }
            }
        }
        else
        {
            $result['status']   = 'error';
            $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
        }
    }
    else
    {
        $result['status']   = 'warning';
        $result['message']  = '<h3><b>Warning!</b> Please check the reCaptcha.</h3>';
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING EVENTS FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function booking_events_inquiry($post_data)
{
    global $db;
    $result['status']   = 'error asdasd';
    $result['message']  = '<h3><b>Error!</b> Something went wrong, please try again later.</h3>';


    if (isset($post_data['recaptcha']) && !empty($post_data['recaptcha']))
    {
        $params             = array();
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $params['secret']   = get_meta_data('r_secret_key', 'static_setting');
        $params['response'] = (!empty($post_data['recaptcha']) ? urlencode($post_data['recaptcha']) : '');

        $prm_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $prm_string;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $data = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($data);

        if ($response->success)
        {

                $destination = $post_data['destination'];
                $dest_id     = get_id_article_by_sef($destination, 'destinations');

                $events           = $post_data['events'];
                $number_of_guest  = $post_data['number_of_guest'];
                $start_date       = $post_data['start_date'];
                $end_date         = $post_data['end_date'];
                $detail_booking   = $post_data['detail_booking'];
                $title_salutation = $post_data['title'];
                $first_name       = ucwords($post_data['first_name']);
                $last_name        = ucwords($post_data['last_name']);
                $nationality      = $post_data['nationality'];
                $email            = $post_data['email'];
                $phone            = $post_data['phone'];

                $subject       = "Events Inquiry - Samaya - $last_name";
                // $cont_email = get_meta_data('email_address', 'static_setting');
                $cont_email    = get_events_inqury_email_address($dest_id);
                $web_name      = get_meta_data( 'web_title', 'static_setting' );

                $msg_body        = '
                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="content" style="padding-left: 40px;padding-right: 40px;padding-top: 40px;padding-bottom: 20px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;font-size: 13px;color: #666;">
                                        You have a new events inquiry from the website. Please check the detail below for your reference.
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Events
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$events.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Number Of Guest
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$number_of_guest.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Events Start Date
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$start_date.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Events End Date
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$end_date.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Detail Booking
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$detail_booking.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Title / Salutation
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$title_salutation.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        First Name
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$first_name.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Last Name
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$last_name.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Country Of Residence
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$nationality.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Phone
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$phone.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                <tr>
                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                        Email
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                        :
                                    </td>

                                    <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                        '.$email.'
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                ';

                // $result['msg']   = $msg_body;

                $send_email = send_email($email, $name, $cont_email, $web_name, $subject, $msg_body);

                if($send_email)
                {
                    $result['status']   = 'success';
                    $result['message']  = '<h3><b>Success!</b> Your inquiry has been sent.</h3>';
                }
                else
                {
                    $result['status']   = 'error';
                    $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
                }
        }
        else
        {
            $result['status']   = 'error';
            $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
        }
    }
    else
    {
        $result['status']   = 'warning';
        $result['message']  = '<h3><b>Warning!</b> Please check the reCaptcha.</h3>';
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING OFFERS FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function booking_offers_inquiry($post_data)
{
    global $db;

    $result['status']   = 'error';
    $result['message']  = '<h3><b>Error!</b> Something went wrong, please try again later.</h3>';

    $destination = $post_data['destination'];
    $subject     = $post_data['subject'];
    $fullname    = $post_data['fullname'];
    $email       = $post_data['email'];
    $message     = $post_data['message'];
    $nationality = $post_data['nationality'];
    $phone       = $post_data['phone'];

    if(!empty($subject) && !empty($fullname) && !empty($email) && !empty($nationality) && !empty($phone))
    {
        $dest_id     = get_id_article_by_sef($destination, 'destinations');
        $cont_email  = get_additional_field($dest_id, 'reservation_email_address', 'destinations');

        if(!empty($cont_email)){
            $subject_email  = "Special Offers Inquiry - Samaya";
            $web_name = get_meta_data( 'web_title', 'static_setting' );

            $msg_body = '
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="content" style="padding-left: 40px;padding-right: 40px;padding-top: 40px;padding-bottom: 20px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;font-size: 13px;color: #666;">
                                    You have a new special offers inquiry from the website. Please check the detail below for your reference.
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                                Subject
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                                :
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                                '.$subject.'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                                Full Name
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                                :
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                                '.$fullname.'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                                Nationality
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                                :
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                                '.$nationality.'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                                Phone Number
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                                :
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                                '.$phone.'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                                Email
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                                :
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                                '.$email.'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                                        <tr>
                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                                Message
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                                :
                                            </td>

                                            <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                                '.$message.'
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            ';

            $send_email = send_email($email, $fullname, $cont_email, $web_name, $subject_email, $msg_body);

            if($send_email)
            {
                $result['status']   = 'success';
                $result['message']  = '<h3><b>Success!</b> Your inquiry has been sent.</h3>';
            }
            else
            {
                $result['status']   = 'error';
                $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
            }
        }

    }


    return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP EVENTS
| -------------------------------------------------------------------------------------------------------------------------
*/
function booking_popup_events_content()
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/booking_events_popup.html', 'popup_events_template' );
    add_block( 'popup-events-block', 'ppeblock', 'popup_events_template' );

    set_switch_language();
    $country_option = get_data_country();

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    add_variable( 'option_events', attemp_actions( 'option_events' ) );
    add_variable( 'country_option', $country_option );

    $captcha_site_key   = get_meta_data( 'r_public_key', 'static_setting' );
    add_variable( 'captcha_site_key', $captcha_site_key );

    $lang       = attemp_actions( 'lang' );
    $check_lang = attemp_actions( 'check_lang' );

    add_variable( 'lang', $lang );
    add_variable( 'check_lang', $check_lang );


    parse_template( 'popup-events-block', 'ppeblock', false );

    return return_template( 'popup_events_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP OFFERS
| -------------------------------------------------------------------------------------------------------------------------
*/
function booking_popup_offers_content()
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/booking_offers_popup.html', 'popup_offers_template' );
    add_block( 'popup-offers-block', 'ppoblock', 'popup_offers_template' );

    set_switch_language();
    $country_option = get_data_country();

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    add_variable( 'country_option', $country_option );

    parse_template( 'popup-offers-block', 'ppoblock', false );

    return return_template( 'popup_offers_template' );
}



/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP DININGS
| -------------------------------------------------------------------------------------------------------------------------
*/
function booking_popup_dinings_content()
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/booking_dinings_popup.html', 'popup_dining_template' );
    add_block( 'popup-dining-block', 'pdblock', 'popup_dining_template' );


    set_switch_language();

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    // GET DATA COUNTRY
    $q = $db->prepare_query("
        SELECT lcountry, lcountry_id
        FROM lumonata_country
        WHERE llang_id=6
        ORDER BY lcountry ASC
    ");
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $country_option = "";
    if($n > 0)
    {
        while($d = $db->fetch_array($r))
        {
            $country         = $d['lcountry'];
            $country_option .= '<option value="'.$country.'">'.$country.'</option>';
        }
    }
    add_variable( 'country_option', $country_option );

    parse_template( 'popup-dining-block', 'pdblock', false );

    return return_template( 'popup_dining_template' );
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING WEDDING FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function booking_wedding_inquiry($post_data)
{
    global $db;

    $result['status']   = 'error';
    $result['message']  = '<h3><b>Error!</b> Something went wrong, please try again later.</h3>';

    if (isset($post_data['recaptcha']) && !empty($post_data['recaptcha'])) {
        $params             = array();
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $params['secret']   = get_meta_data('r_secret_key', 'static_setting');
        $params['response'] = (!empty($post_data['recaptcha']) ? urlencode($post_data['recaptcha']) : '');

        $prm_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $prm_string;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $data = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($data);

        if ($response->success)
        {
            $destination = $post_data['destination'];
            $dest_id     = get_id_article_by_sef($destination, 'destinations');

            $name               = ucwords($post_data['fullname']);
            $email              = $post_data['email'];
            $nationality        = ucwords($post_data['nationality']);
            $spouse_fullname    = ucwords($post_data['spouse_fullname']);
            $spouse_nationality = ucwords($post_data['spouse_nationality']);
            $phone              = ucwords($post_data['phone']);
            $wedding_date       = ucwords($post_data['wedding_date']);
            $number_guest       = $post_data['number_guest'];
            $occasion_type      = $post_data['occasion_type'];
            $theme_color        = ucwords($post_data['theme_color']);
            $food_type          = ($post_data['food_type']);
            $food_dinner        = ucwords($post_data['food_dinner']);
            $reception_venue    = ucwords($post_data['reception_venue']);
            $special_request    = ($post_data['special_request']);
            $other_text         = ucwords($post_data['other_text']);

            $subject       = "Weddings Inquiry - Samaya - $name";
            $cont_email    = get_additional_field($dest_id, 'reservation_email_address', 'destinations');
            $web_name      = get_meta_data( 'web_title', 'static_setting' );

            $msg_body        = '
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="content" style="padding-left: 40px;padding-right: 40px;padding-top: 40px;padding-bottom: 20px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;font-size: 13px;color: #666;">
                                    You have a new weddings inquiry from the website. Please check the detail below for your reference.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Full Name
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$name.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Email
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$email.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Nationality
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$nationality.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Spouse Fullname
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$spouse_fullname.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Spouse Nationality
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$spouse_nationality.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Phone
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$phone .'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Wedding Date
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$wedding_date.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Number of Guest
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$number_guest.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Occasion Type
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    <ul>
            ';
                                    foreach($occasion_type as $ot)
                                    {
                                        $msg_body .= "<li>$ot</li>";
                                    }
            $msg_body .= '
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                   Theme Color
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$theme_color .'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                   Food Type
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    <ul>
            ';
                                foreach($food_type as $ft)
                                {
                                    $msg_body .= "<li>$ft</li>";
                                }
            $msg_body .= '
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                   Food Dinner
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$food_dinner.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                   Reception Venue
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$reception_venue.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                   Special Request
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    <ul>
                ';
                            foreach($special_request as $sr)
                            {
                                if(strtolower($sr) == "other")
                                {
                                    $msg_body .= "<li>Other: ".$other_text."</li>";
                                }
                                else
                                {
                                    $msg_body .= "<li>$sr</li>";
                                }
                            }

                $msg_body .= '
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            ';

            $send_email = send_email($email, $name, $cont_email, $web_name, $subject, $msg_body);

            if($send_email)
            {
                $result['status']   = 'success';
                $result['message']  = '<h3><b>Success!</b> Your inquiry has been sent.</h3>';
            }
            else
            {
                $result['status']   = 'error';
                $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
            }
        }
        else
        {
            $result['status']   = 'error';
            $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
        }
    }
    else
    {
        $result['status']   = 'warning';
        $result['message']  = '<h3><b>Warning!</b> Please check the reCaptcha.</h3>';
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING DINING FORM INQUIRY (UBUD VILLAGE)
| -----------------------------------------------------------------------------
*/
function bo_dining_inquiry($post_data)
{
    global $db;

    $result['status']   = 'error';
    $result['message']  = '<h2>Error!</h2>
                           <p>
                           Something went wrong, please try again later.
                           </p>';

    $destination = $post_data['destination'];
    $dest_id     = get_id_article_by_sef($destination, 'destinations');

    $name               = ucwords($post_data['name']);
    $email              = $post_data['email'];
    $people             = $post_data['people'];
    $date               = $post_data['date'];
    $phone              = $post_data['phone'];
    $time               = $post_data['time'];
    $msg                = $post_data['msg'];

    $subject       = "Dining Inquiry - Ubud Village - $name";
    $cont_email    = get_additional_field($dest_id, 'reservation_email_address', 'destinations');
    $web_name      = get_meta_data( 'web_title', 'static_setting' );

    $msg_body      = 'mashoooooooooooooookkkkkkk';

    $send_email = send_email($email, $name, $cont_email, $web_name, $subject, $msg_body);

    if($send_email)
    {
        $result['status']   = 'success';
        $result['message']  = '<h2>Success!</h2>
                               <p>
                               Your inquiry has been sent.
                               </p>';
    }
    else
    {
        $result['status']   = 'error';
        $result['message']  = '<h2>Danger!</h2>
                               <p>
                               Sorry your message cannot be send, please try again later.
                               </p>';
    }


    return $result;
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX BOOKING WEDDING FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function bo_spa_inquiry($post_data)
{
    global $db;

    $result['status']   = 'error';
    $result['message']  = '<h2>Error!</h2>
                           <p>
                           Something went wrong, please try again later.
                           </p>';

    if (isset($post_data['recaptcha']) && !empty($post_data['recaptcha'])) {
        $params             = array();
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $params['secret']   = get_meta_data('r_secret_key', 'static_setting');
        $params['response'] = (!empty($post_data['recaptcha']) ? urlencode($post_data['recaptcha']) : '');

        $prm_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $prm_string;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $data = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($data);

        if ($response->success)
        {
            $destination = $post_data['destination'];
            $dest_id     = get_id_article_by_sef($destination, 'destinations');

            $spaloc         = $post_data['spaloc'];
            $spacat         = $post_data['spacat'];
            $spalist        = $post_data['spalist'];
            $person         = $post_data['person'];
            $date           = $post_data['date'];
            $hour           = $post_data['hour'];
            $minute         = $post_data['minute'];
            $stay           = $post_data['stay'];
            $srequest       = $post_data['srequest'];
            $name           = $post_data['name'];
            $national       = $post_data['national'];
            $email          = $post_data['email'];
            $phonenum       = $post_data['phonenum'];
            $prefercontact  = $post_data['prefercontact'];
            $destination    = $post_data['destination'];

            $subject       = "Spa Inquiry - Ubud Village - $name";
            $cont_email    = get_additional_field($dest_id, 'spa_reservation_email_address', 'destinations');
            $web_name      = get_meta_data( 'web_title', 'static_setting' );

            $msg_body        = 'masok dong broo';

            $send_email = send_email($email, $name, $cont_email, $web_name, $subject, $msg_body);

            if($send_email)
            {
              $result['status']   = 'success';
              $result['message']  = '<h2>Success!</h2>
                                     <p>
                                     Your inquiry has been sent.
                                     </p>';
            }
            else
            {
              $result['status']   = 'error';
              $result['message']  = '<h2>Danger!</h2>
                                     <p>
                                     Sorry your message cannot be send, please try again later.
                                     </p>';
            }
        }
        else
        {
          $result['status']   = 'error';
          $result['message']  = '<h2>Danger!</h2>
                                 <p>
                                 Sorry your message cannot be send, please try again later.
                                 </p>';
        }
    }
    else
    {
        $result['status']   = 'warning';
        $result['message']  = '<h2>Warning!</h2>
                               <p>
                               Please check the reCaptcha.
                               </p>';
    }

    return $result;
}

?>
