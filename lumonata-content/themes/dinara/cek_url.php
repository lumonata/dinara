<?php

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CEK URL LANDING PAGE JIKA TERDAPAT CODE LANGUAGE
| -----------------------------------------------------------------------------
*/
function is_language_home()
{
    $cek_url      = cek_url();
    $cek_language = check_language_code($cek_url[0]);
    if($cek_language && !isset($cek_url[1]))
    {
      return true;
    }

    return false;
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CEK URL HOME DESTINATION
| -----------------------------------------------------------------------------
*/
function is_destination()
{
    $cek_url         = cek_url();
    $cek_destination = check_destination($cek_url['0']);
    if($cek_destination)
    {
        return true;
    }
    return false;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK ROOM DETAIL
| -------------------------------------------------------------------------------
*/
function is_accommodation_detail()
{
    $cek_url = cek_url();
    if( $cek_url[0] == 'accommodation' && isset( $cek_url[1] ) )
    {
        $exp = explode( '.', $cek_url[1] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[1] == 'accommodation' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[1] );
        $sef = $exp[0];

        return true;
    }

    return false;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK DINING DETAIL
| -------------------------------------------------------------------------------
*/
function is_dining_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'dining' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'dining' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK ACTIVITIES DETAIL
| -------------------------------------------------------------------------------
*/
function is_activities_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'activities' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'activities' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK ACTIVITIES DETAIL
| -------------------------------------------------------------------------------
*/
function is_facilities_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'facilities' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'facilities' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK SPA DETAIL
| -------------------------------------------------------------------------------
*/
function is_spa_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'spa' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'spa' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK OFFERS DETAIL
| -------------------------------------------------------------------------------
*/
function is_offers_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'special-offers' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'special-offers' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK EVENT DETAIL
| -------------------------------------------------------------------------------
*/
function is_events_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'events' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'events' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK EVENT DETAIL
| -------------------------------------------------------------------------------
*/
function is_weddings_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'weddings' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'weddings' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK EVENT DETAIL
| -------------------------------------------------------------------------------
*/
function is_what_to_do_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'what-to-do-act-in' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[1] == 'what-to-do-around' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'what-to-do-act-in' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'what-to-do-around' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }

    return false;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK EVENT DETAIL
| -------------------------------------------------------------------------------
*/
function is_news_detail()
{
    $cek_url = cek_url();

    if( $cek_url[1] == 'news' && isset( $cek_url[2] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
    elseif( $cek_url[2] == 'news' && isset( $cek_url[3] ) )
    {
        $exp = explode( '.', $cek_url[2] );
        $sef = $exp[0];

        return true;
    }
}


/*
| -----------------------------------------------------------------------------
| Check Permalink - Ajax Request
| -----------------------------------------------------------------------------
*/
function is_ajax_page()
{
    global $actions;

    if( ( isset( $actions->action['is_use_ajax'] ) && $actions->action['is_use_ajax']['func_name'][0] ) || isset( $_POST['pkey'] ) )
    {
        return true;
    }
    elseif(trim(get_appname())=='ajax-function')
    {
        return true;
    }
    else
    {
        return false;
    }
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK CHECK APAKAH PAGE YANG DITUJU MERUPAKAN LINK CRONJOB SCRAPPING
| -------------------------------------------------------------------------------
*/
function is_cronjob_scrapping()
{
    $cek_url = cek_url();

    if( $cek_url[0] == 'cronjob-scrapping' )
    {
        return true;
    }
}


?>
