<?php
require_once "cek_url.php";
require_once "function_home_destination.php";
require_once "function_page.php";
require_once "function_page_detail.php";
require_once "function_booking.php";


/*
| -----------------------------------------------------------------------------
| Add Meta Data Value
| -----------------------------------------------------------------------------
*/
function add_meta_data_value($mtitle='', $mkey='', $mdesc='', $og_url='', $og_title='', $og_image='', $og_site_name='', $og_description='')
{
	global $actions;

	if(isset($actions->action['meta_title']))
		unset($actions->action['meta_title']);

	if(isset($actions->action['meta_keywords']))
		unset($actions->action['meta_keywords']);

	if(isset($actions->action['meta_description']))
        unset($actions->action['meta_description']);

    if(isset($actions->action['og_url']))
        unset($actions->action['og_url']);

    if(isset($actions->action['og_title']))
        unset($actions->action['og_title']);

    if(isset($actions->action['og_image']))
        unset($actions->action['og_image']);

    if(isset($actions->action['og_site_name']))
        unset($actions->action['og_site_name']);

    if(isset($actions->action['og_description']))
        unset($actions->action['og_description']);

    $og_url         = get_meta_og_url($og_url);
    $og_title       = get_meta_og_title($mtitle);
    $og_image       = get_meta_og_image($og_image);
    $og_site_name   = get_meta_og_site_name(web_title());
    $og_description = get_meta_og_description($mdesc);

	add_actions('meta_title', $mtitle);
	add_actions('meta_keywords', $mkey);
    add_actions('meta_description', get_meta_description_tag($mdesc));
    add_actions('og_url', $og_url);
    add_actions('og_title', $og_title);
    add_actions('og_image', $og_image);
    add_actions('og_site_name', $og_site_name);
    add_actions('og_description', $og_description);
}

/*
| -----------------------------------------------------------------------------
| Function Config Meta Data
| -----------------------------------------------------------------------------
*/
function config_meta_data($the_url='', $data_page='')
{
    $cek_url = cek_url();
    $mtitle  = "";
    $mkey    = "";
    $mdesc   = "";
    $ogimage = "";

    if(!empty($data_page))
    {
        $mtitle  = $data_page['metatitle'];
        $mkey    = get_meta_keywords_tag($data_page['metakey']);
        $mdesc   = $data_page['metadesc'];
        $ogimage = (isset($data_page['og_image']) ? $data_page['og_image'] : '');
    }

    if(is_home() || is_language_home())
    {
        $mtitle         = (empty($mtitle) ? trim(web_title()) : $mtitle);
    }
    else
    {
        if(get_uri_count() == 1)
        {
            $mtitle = (empty($mtitle) ? ucwords(strtolower($data_page['title'])).' - '.ucwords($cek_url[0]).' | '.trim(web_title()) : $mtitle);
        }
        else if(get_uri_count() == 2)
        {
            $mtitle = (empty($mtitle) ? ucwords(strtolower($data_page['title'])).' - '.ucwords($cek_url[0]).' | '.trim(web_title()) : $mtitle);
        }
        elseif(get_uri_count() == 3)
        {
            if(check_destination($cek_url[1]))
            {
                $mtitle = (empty($mtitle) ? ucwords(strtolower($data_page['title'])).' - '.ucwords($cek_url[1]).' | '.trim(web_title()) : $mtitle);
            }
            else
            {
                $mtitle = (empty($mtitle) ? ucwords(strtolower($data_page['post_title'])).' - '.ucwords($cek_url[1]).' | '.trim(web_title()) : $mtitle);
            }
        }
        elseif(get_uri_count() == 4)
        {
            if(check_destination($cek_url[1]))
            {
                $mtitle = (empty($mtitle) ? ucwords(strtolower($data_page['post_title'])).' - '.ucwords($cek_url[2]).' | '.trim(web_title()) : $mtitle);
            }
        }
    }


    add_meta_data_value($mtitle, $mkey, $mdesc, $the_url, '', $ogimage);
}

/*
| -----------------------------------------------------------------------------
| Function Untuk Header Layout
| -----------------------------------------------------------------------------
*/
function header_layout($title_hero='', $bg_hero='', $appname='', $header_menu='', $corporate_data='', $lang='', $check_lang=false)
{
    set_template( TEMPLATE_PATH . '/layout/header.html', 'header_layout' );
    add_block( 'header-block', 'hblock', 'header_layout' );

    $home_url = HTTP.site_url().'/'.$appname;
    if($check_lang)
    {
        $home_url = HTTP.site_url().'/'.$lang.'/'.$appname;
    }

    set_switch_language();

    $dest_id = $corporate_data[0]['id'];
    $form_booking_hero = form_booking_hero_page($dest_id);

    add_variable( 'list_link_product_hero', attemp_actions( 'list_link_product_hero' ) );
    add_variable( 'slider_hero_page', attemp_actions( 'slider_hero_page' ) );
    add_variable( 'button_download_hero', attemp_actions( 'button_download_hero' ) );
    add_variable( 'title_hero', $title_hero );
    add_variable( 'bg_hero', $bg_hero );
    add_variable( 'form_booking_hero', $form_booking_hero );
    add_variable( 'appname', $appname );
    add_variable( 'appname_upper', strtoupper($appname ));
    add_variable( 'header_menu', $header_menu );

    add_variable( 'home_url', $home_url );
    add_variable( 'HTTP', HTTP );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $social_media_html = $corporate_data[0]['social_media']['social_media_header'];
    add_variable( 'social_media_html', $social_media_html );

    // GET LIST CORPORATE
    $corporate        = get_list_corporate($lang, $check_lang);
    $destination_list = "";
    $dest_list        = "";
    $dest_hidden      = "";

    foreach( $corporate as $d )
    {
        $link = HTTP.site_url().'/'.$d['sef'];

        if($check_lang)
        {
            $link = HTTP.site_url().'/'.$lang.'/'.$d['sef'];
        }

        $destination_list .= '<li><a href="'.$link.'">'.$d['title'].'</a></li>';
        $dest_list        .= '<li data-value="'.$d['title'].'" data-hotelsname="'.$d['hotels_name'].'" data-properties="'.$d['properties_name'].'">'.$d['title'].'</li>';
        $dest_hidden      .= '<div class="booking-data-'.strtolower($d['title']).'" data-hotelsname="'.$d['hotels_name'].'" data-properties="'.$d['properties_name'].'"></div>';
    }
    add_variable( 'destination_list', $destination_list );
    add_actions( 'dest_list', $dest_list );
    add_actions( 'dest_hidden', $dest_hidden );

    parse_template( 'header-block', 'hblock', false );

    return return_template( 'header_layout' );
}

add_actions( 'landing_page_welcome_popup', 'landing_page_welcome_popup_content' );
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT TEMPLATE BOOKING POPUP ACCOMMODATION
| -------------------------------------------------------------------------------------------------------------------------
*/
function landing_page_welcome_popup_content()
{
    global $db;

    set_template( TEMPLATE_PATH . '/layout/landing_page_welcome_popup.html', 'landing_popup_template' );
    add_block( 'landing-popup-block', 'lpblock', 'landing_popup_template' );

    set_switch_language();

    add_variable( 'properties_name', attemp_actions( 'properties_name' ) );
    add_variable( 'hotels_name', attemp_actions( 'hotels_name' ) );
    add_variable( 'dest_list', attemp_actions( 'dest_list' ) );

    parse_template( 'landing-popup-block', 'lpblock', false );

    return return_template( 'landing_popup_template' );
}


function get_list_corporate($lang='', $check_lang=false)
{
    global $db;

    $res = array();

    $q = $db->prepare_query("
        SELECT larticle_id, larticle_title, lsef
        FROM lumonata_articles
        WHERE larticle_type=%s AND larticle_status=%s
        ORDER BY lorder ASC
    ",
        'destinations', 'publish'
    );
    $r = $db->do_query($q);

    if( $db->num_rows( $r ) > 0 )
    {

        while( $d = $db->fetch_array( $r ) )
        {
            $id    = $d['larticle_id'];
            $title = $d['larticle_title'];

            $properties_name = get_additional_field($id,'fb_properties_name','destinations');
            $hotels_name     = get_additional_field($id,'fb_hotels_name','destinations');

            if($check_lang)
            {
                $title_lang    = get_additional_field($id, 'title_'.$lang, 'destinations');
                $title = (empty($title_lang) ? $title: $title_lang);
            }

            $value = array(
                'sef'             => $d['lsef'],
                'id'              => $id,
                'title'           => $title,
                'properties_name' => $properties_name,
                'hotels_name'     => $hotels_name
            );

            $res[] = $value;

        }
    }

    return $res;
}


/*
| -----------------------------------------------------------------------------
| Set Ajax Function
| -----------------------------------------------------------------------------
*/
add_actions( 'ajax-function_page', 'ajax_page_content' );

function ajax_page_content()
{
    add_actions( 'is_use_ajax', true );

    if (isset($_POST['pkey']) and $_POST['pkey']=='view-locations')
    {
        $result = get_location_datas($_POST);
    }
    elseif (isset($_POST['pkey']) and $_POST['pkey']=='view-destinations')
    {
        $result = array('status', 'ok');
    }
    elseif (isset($_POST['pkey']) and $_POST['pkey']=='contact_message')
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $result = contact_form_inquiry($_POST);
        }
    }
    elseif (isset($_POST['pkey']) and ($_POST['pkey']=='choose_spa' or $_POST['pkey']=='change_spa' or $_POST['pkey']=='change_treatment_category' or $_POST['pkey']=='change_treatment_list' ))
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $result = booking_spa_form_action($_POST);
        }
    }
    elseif($_POST['pkey']=='spa_booking_inquiry')
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $result = booking_spa_inquiry($_POST);
        }
    }
    elseif($_POST['pkey']=='offers_booking_inquiry')
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // $result['ok'] = "asdasdas";
            $result = booking_offers_inquiry($_POST);
        }
    }
    elseif($_POST['pkey']=='weddings_booking_inquiry')
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // $result['ok'] = "asdasdas";
            $result = booking_wedding_inquiry($_POST);
        }
    }
    elseif($_POST['pkey']=='mailchimp_subscribe')
    {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $result = mailchimp_subscribe($_POST);
        }
    }

    echo json_encode($result);
    exit();
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK SEND EMAIL DENGAN PHPMAILER
| -----------------------------------------------------------------------------
*/
function send_email($from, $origin, $to, $name, $subject, $message){
    // require_once(ROOT_PATH.'/PHPMailer/PHPMailerAutoload.php');

    $smtp_server    = get_meta_data( 'smtp', 'static_setting' );
    $smtp_port      = get_meta_data( 'smtp_email_port', 'static_setting' );
    $smtp_email     = get_meta_data( 'smtp_email_address', 'static_setting' );
    $smtp_pwd       = get_meta_data( 'smtp_email_pwd', 'static_setting' );

    $mail           = new PHPMailer( true );

    // return $smtp_server;

    try
    {
        // $mail->SMTPDebug  = 3;
        $mail->IsSMTP();
        $mail->SMTPAuth   = true;
        $mail->Host       = $smtp_server;
        $mail->SMTPSecure = 'tls';
        $mail->Port       = $smtp_port;
        $mail->Username   = $smtp_email;
        $mail->Password   = $smtp_pwd;
        $mail->Subject    = $subject;
        $mail->Timeout    = 100;

        $mail->AddReplyTo( $from, $origin );
        $mail->SetFrom( $smtp_email, "Inquiry Samaya" );
        $mail->AltBody    = "This is the body in plain text for non-HTML mail clients";
        $mail->MsgHTML( $message );
        $mail->AddAddress( $to, $name );
        $mail->Send();

        return true;
    }
    catch( phpmailerException $e )
    {
        // echo $e->errorMessage().$web_name;
        // return $e->errorMessage().$name;
        return false;
    }
    catch( Exception $e )
    {
        // echo $e->getMessage();
        // return $e->getMessage();
        return false;
    }
}


/*
| -----------------------------------------------------------------------------
| Set Home Content - Corporate List
| -----------------------------------------------------------------------------
*/
function set_corporate_list($lang='', $check_lang=false, $string_translations='')
{
    // STRING TRANSLATIONS
    $translations = array('Explore More', 'Direction');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $app_name = trim( get_appname() );

    $lang_url = "";
    if(!empty($app_name))
    {
        $lang_url = $app_name.'/';
    }

    $corporate   = get_articles_data('destinations');

    if( is_home() || is_language_home() )
    {
        add_block( 'section-loop-block', 'sl-block' );
    }

    $dest = array();
    $destination_list = "";
    $dest_list = "";
    foreach( $corporate as $d )
    {
        $id                  = $d['id'];
        $title               = $d['title'];
        $content             = $d['desc'];
        $brief               = $d['brief'];
        $content             = get_additional_field($id,'content_'.$lang,'destinations');
        $address             = get_additional_field($id,'address','destinations');
        $phone               = get_additional_field($id,'phone_number','destinations');
        $email               = get_additional_field($id,'email_address','destinations');
        $link_direction      = get_additional_field($id,'link_direction','destinations');
        $coordinate_location = get_additional_field($id,'coordinate_location','destinations');
        $coordinate_location = explode(',', $coordinate_location);

        // FAST BOOKING ACCOMMODATION SETTING
        $properties_name = get_additional_field($id,'fb_properties_name','destinations');
        $hotels_name     = get_additional_field($id,'fb_hotels_name','destinations');
        $corporate_image = get_additional_field($id,'corporate_image','destinations');

        if($check_lang)
        {
            $title_lang   = get_additional_field($id,'title_'.$lang,'destinations');
            $content_lang = get_additional_field($id,'content_'.$lang,'destinations');
            $brief_lang   = get_additional_field($id,'brief_'.$lang,'destinations');

            $title   = (empty($title_lang) ? $title:     $title_lang);
            $content = (empty($content_lang) ? $content: $content_lang);
            $brief   = (empty($brief_lang) ? $brief:     $brief_lang);
        }

        add_variable( 'sef', $d['sef'] );
        add_variable( 'text', $title );
        add_variable( 'content', $content );
        add_variable( 'brief', $brief );
        add_variable( 'background', $d['background'] );
        add_variable( 'link', HTTP.site_url() . '/' . $lang_url . $d['sef'] );

        $destination_list .= '<li data-hotelsname="'.$hotels_name.'" data-properties="'.$properties_name.'">'.$title.'</li>';


        // SET DESTINATIONS DATA FOR GOOGLE MAPS
        $dest[] = array(
            'title'           => 'The Samaya '.ucwords($d['sef']),
            'address'         => $address,
            'sef'             => $d['sef'],
            'email'           => $email,
            'phone'           => $phone,
            'text_explore'    => $tr_string['explore_more'],
            'text_direction'  => $tr_string['direction'],
            'latitude'        => $coordinate_location[0],
            'longitude'       => $coordinate_location[1],
            'link_direction'  => $link_direction,
            'corporate_image' => HTTP.site_url() . '/lumonata-plugins/destinations/background/'.$corporate_image,
            'link_explore'    => HTTP.site_url() . '/' . $lang_url . $d['sef'],
        );

        if( is_home() || is_language_home() )
        {
            $properties_name = get_additional_field($id,'fb_properties_name','destinations');
            $hotels_name     = get_additional_field($id,'fb_hotels_name','destinations');
            $dest_list .= '<li data-value="'.$d['title'].'" data-hotelsname="'.$hotels_name.'" data-properties="'.$properties_name.'">'.$d['title'].'</li>';

            add_variable( 'background', $d['background'] );

            parse_template( 'section-loop-block', 'sl-block', true );
        }
    }

    add_actions( 'dest_list', $dest_list );
    add_variable( 'destination_list', $destination_list );
    add_variable('dest_data', json_encode($dest));
}

/*
| -----------------------------------------------------------------------------
| Get Articles Data
| -----------------------------------------------------------------------------
*/
function get_articles_data($type='pages', $appname='', $lang='', $check_lang=false)
{
    global $db;

    $code_lang = "";

    $res = array();

    $whereplus = "";
	if(!empty($appname))
	{
		$whereplus = " AND lsef='$appname'";
	}

    $s = 'SELECT
            lsef,
            larticle_id,
            larticle_title,
            larticle_subtitle,
            larticle_brief,
            larticle_content
          FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s'.$whereplus.'
          ORDER BY lorder ASC';
    $q = $db->prepare_query( $s, $type, 'publish' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $res = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $id         = $d['larticle_id'];
            $cimage     = get_background_image( $id, 'large' );
            $bg_homepage = "";

            if($type == 'destinations')
            {
                $bg_homepage = get_background_image_homepage( $id, 'large' );
            }

            if($type=="language")
            {
                $code_lang = get_additional_field($id,'language_code','language');
            }

            $title            = $d['larticle_title'];
            $subtitle         = $d['larticle_subtitle'];
            $content          = $d['larticle_content'];
            $brief            = $d['larticle_brief'];
            $meta_title       = get_additional_field($id, 'meta_title', $type);
            $meta_keywords    = get_additional_field($id, 'meta_keywords', $type);
            $meta_description = get_additional_field($id, 'meta_description', $type);

            if($check_lang)
            {
                $title_lang            = get_additional_field($id, 'title_'.$lang, $type);
                $subtitle_lang         = get_additional_field($id, 'subtitle_'.$lang, $type);
                $content_lang          = get_additional_field($id, 'content_'.$lang, $type);
                $brief_lang            = get_additional_field($id, 'brief_'.$lang, $type);
                $meta_title_lang       = get_additional_field($id, 'meta_title_'.$lang, $type);
                $meta_keywords_lang    = get_additional_field($id, 'meta_keywords_'.$lang, $type);
                $meta_description_lang = get_additional_field($id, 'meta_description_'.$lang, $type);

                $title            = (empty($title_lang) ? $title:                 $title_lang);
                $subtitle         = (empty($subtitle_lang) ? $subtitle:           $subtitle_lang);
                $content          = (empty($content_lang) ? $content:             $content_lang);
                $brief            = (empty($brief_lang) ? $brief:                 $brief_lang);
                $meta_title       = (empty($meta_title_lang) ? $meta_title:       $meta_title_lang);
                $meta_keywords    = (empty($meta_keywords_lang) ? $meta_keywords: $meta_keywords_lang);
                $meta_description = (empty($meta_description_lang) ? $meta_description:       $meta_description_lang);
            }

            $value = array(
                'sef'              => $d['lsef'],
                'id'               => $id,
                'title'            => $title,
                'subtitle'         => $subtitle,
                'desc'             => $content,
                'brief'            => $brief,
                'background'       => $cimage,
                'bg_homepage'      => $bg_homepage,
                'code_lang'        => $code_lang,
                'meta_title'       => $meta_title,
                'meta_keywords'    => $meta_keywords,
                'meta_description' => $meta_description
            );

            if($type == 'destinations')
            {
                $value += array(
                    'social_media' => get_social_media_destination($id)
                );
            }

            $res[] = $value;
        }
    }

    return $res;
}


function get_social_media_destination($dest_id)
{
    // SOCIAL MEDIA
    $fb_link        = get_additional_field( $dest_id, 'fb_link', 'destinations' );
    $twitter_link   = get_additional_field( $dest_id, 'twitter_link', 'destinations' );
    $instagram_link = get_additional_field( $dest_id, 'instagram_link', 'destinations' );
    $youtube_link   = get_additional_field( $dest_id, 'youtube_link', 'destinations' );

    // SOCIAL MEDIA HEADER
    $social_media_header  = "";
    $social_media_header .= (!empty($instagram_link) ? '<a href="'.$instagram_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/ig-header.svg" class="instagram-icon"></a>' : '');
    $social_media_header .= (!empty($fb_link) ? '<a href="'.$fb_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/fb-header.svg" class="facebook-icon"></a>' : '');
    $social_media_header .= (!empty($youtube_link) ? '<a href="'.$youtube_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/youtube-header.svg" class="youtube-icon"></a>' : '');


    // SOCIAL MEDIA FOOTER
    $social_media_footer  = "";
    $social_media_footer .= (!empty($instagram_link) ? '<a href="'.$instagram_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/ig-footer.svg" class="instagram-icon"></a>' : '');
    $social_media_footer .= (!empty($fb_link) ? '<a href="'.$fb_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/fb-footer.svg" class="facebook-icon"></a>' : '');
    $social_media_footer .= (!empty($youtube_link) ? '<a href="'.$youtube_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/youtube-footer.svg" class="youtube-icon"></a>' : '');


    // SOCIAL MEDIA CONTACT PAGE
    $social_media_default  = "";
    $social_media_default .= (!empty($instagram_link) ? '<a href="'.$instagram_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/instagram.svg" class="instagram-icon"></a>' : '');
    $social_media_default .= (!empty($fb_link) ? '<a href="'.$fb_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/fb.svg" class="facebook-icon"></a>' : '');
    $social_media_default .= (!empty($twitter_link) ? '<a href="'.$twitter_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/twitter.svg" class="twitter-icon"></a>' : '');
    $social_media_default .= (!empty($youtube_link) ? '<a href="'.$youtube_link.'" target="_blank"><img src="'.HTTP.TEMPLATE_URL.'/assets/images/youtube.svg" class="youtube-icon"></a>' : '');

    $result['social_media_default'] = $social_media_default;
    $result['social_media_footer']  = $social_media_footer;
    $result['social_media_header']  = $social_media_header;

    return $result;
}

/*
| -----------------------------------------------------------------------------
| Get Additional Data Destinations
| -----------------------------------------------------------------------------
*/
function get_additional_destination_data($post_id='')
{
    $result = array();
    if(!empty($post_id))
    {
        $email_address       = get_additional_field( $post_id, 'email_address', 'destinations' );
        $phone_number        = get_additional_field( $post_id, 'phone_number', 'destinations' );
        $fax_number          = get_additional_field( $post_id, 'fax_number', 'destinations' );
        $fb_link             = get_additional_field( $post_id, 'fb_link', 'destinations' );
        $instagram_link      = get_additional_field( $post_id, 'instagram_link', 'destinations' );
        $tripadvisor_link    = get_additional_field( $post_id, 'tripadvisor_link', 'destinations' );
        $youtube_link        = get_additional_field( $post_id, 'youtube_link', 'destinations' );
        $coordinate_location = get_additional_field( $post_id, 'coordinate_location', 'destinations' );
        $address             = get_additional_field( $post_id, 'address', 'destinations' );
        $link_direction      = get_additional_field( $post_id, 'link_direction', 'destinations' );

        add_variable('email_address', $email_address);
        add_variable('phone_number', $phone_number);
        add_variable('fax_number', $fax_number);
        add_variable('address', $address);
        add_variable('link_direction', $link_direction);

        $result = array(
            'email_address'       => $email_address,
            'phone_number'        => $phone_number,
            'fax_number'          => $fax_number,
            'fb_link'             => $fb_link,
            'instagram_link'      => $instagram_link,
            'tripadvisor_link'    => $tripadvisor_link,
            'youtube_link'        => $youtube_link,
            'coordinate_location' => $coordinate_location,
            'address'             => $address
        );
    }
    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN PAGE CONTENT LANDING PAGE
| -----------------------------------------------------------------------------
*/
function set_about_us_landing($lang='', $check_lang=false)
{
    global $db;

    add_block( 'section-about-landing', 'sal-block' );

    $q = $db->prepare_query("
        SELECT larticle_title, larticle_content, larticle_id
        FROM lumonata_articles
        WHERE larticle_status=%s AND larticle_type=%s AND lsef=%s
    ",
        "publish", "pages", "landing-page"
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);

        $id_landing      = $d['larticle_id'];
        $title_landing   = $d['larticle_title'];
        $content_landing = $d['larticle_content'];

        if($check_lang)
        {
            $title_landing_lang   = get_additional_field($id_landing, 'title_'.$lang, 'pages');
            $content_landing_lang = get_additional_field($id_landing, 'content_'.$lang, 'pages');

            $title_landing   = (empty($title_landing_lang) ? $title_landing:     $title_landing_lang);
            $content_landing = (empty($content_landing_lang) ? $content_landing: $content_landing_lang);
        }

        add_variable( 'title_landing', $title_landing );
        add_variable( 'content_landing', $content_landing );

        parse_template( 'section-about-landing', 'sal-block', false );
    }

}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN BUTTON SWITCH LANGUAGE
| -----------------------------------------------------------------------------
*/
function set_switch_language()
{
    $cek_url    = cek_url();
    $appname  = trim(get_appname());
    $language = get_articles_data('language');

    $html = "<ul class='clearfix'>";
    foreach( $language as $d )
    {

        $code_lang = $d['code_lang'];
        if(check_destination($appname))
        {
            $url = HTTP.site_url().'/'.strtolower($code_lang).'/'.$appname;
            $url_default = HTTP.site_url().'/'.$appname;

            if(get_uri_count() == 2)
            {
                $url = HTTP.site_url().'/'.strtolower($code_lang).'/'.$appname.'/'.$cek_url[1];
                $url_default = HTTP.site_url().'/'.$appname.'/'.$cek_url[1];
            }
            elseif(get_uri_count() == 3)
            {
                $url = HTTP.site_url().'/'.strtolower($code_lang).'/'.$appname.'/'.$cek_url[1].'/'.$cek_url[2];
                $url_default = HTTP.site_url().'/'.$appname.'/'.$cek_url[1].'/'.$cek_url[2];
            }
        }
        elseif(get_uri_count() > 1)
        {
            if(check_language_code($appname))
            {
                $cek_url    = cek_url();
                $dest       = $cek_url['1'];

                $url = HTTP.site_url().'/'.strtolower($code_lang).'/'.$dest;
                $url_default = HTTP.site_url().'/'.$dest;

                if(get_uri_count() == 3)
                {
                    $url = HTTP.site_url().'/'.strtolower($code_lang).'/'.$dest.'/'.$cek_url[2];
                    $url_default = HTTP.site_url().'/'.$dest.'/'.$cek_url[2];
                }
                elseif(get_uri_count() == 4)
                {
                    $url = HTTP.site_url().'/'.strtolower($code_lang).'/'.$dest.'/'.$cek_url[2].'/'.$cek_url[3];
                    $url_default = HTTP.site_url().'/'.$dest.'/'.$cek_url[2].'/'.$cek_url[3];
                }
            }
        }
        else
        {
            $url = HTTP.site_url().'/'.strtolower($code_lang);
            $url_default = HTTP.site_url();
        }


        $link = '<a href="'.$url.'">'.$code_lang.'</a>';
        if(strtolower($code_lang) == "en")
        {
            $link = '<a href="'.$url_default.'">'.$code_lang.'</a>';
        }
        elseif(strtolower($code_lang) == "cn")
        {
            $link = '<a target="_blank" href="https://weibo.com/thesamayabali">'.$code_lang.'</a>';
        }

        $html .= '<li>'.$link.'</li>';
    }

    $html .= "</ul>";

    add_variable('switch_lang', $html);
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENGECEK APAKAH KODE LANGUAGE ADA PADA DATABASE
| -----------------------------------------------------------------------------
*/
function check_language_code($code)
{
    global $db;
    $q = $db->prepare_query("
        SELECT lvalue
        FROM lumonata_additional_fields
        WHERE lkey=%s AND lvalue=%s AND lapp_name=%s
    ",
        "language_code", $code, "language"
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        return true;
    }
    return false;
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENGECEK APAKAH DESTINATION DARI URL ADA PADA DATABASE
| -----------------------------------------------------------------------------
*/
function check_destination($sef)
{
    global $db;
    $q = $db->prepare_query("
        SELECT larticle_title
        FROM lumonata_articles
        WHERE lsef=%s AND larticle_type=%s AND larticle_status=%s
    ",
        $sef, "destinations", "publish"
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        return true;
    }
    return false;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN LIST DATA BERDASARKAN POST TYPE
| -------------------------------------------------------------------------------
*/
function get_post_list( $post_type = 'blogs', $viewed = 99, $where_add = "", $destination = "", $simple=0)
{
    if(empty($viewed))
    {
        $viewed = 99;
    }

    global $db;

    extract( $_GET );

    if(!empty($destination))
    {
        $s = '
            SELECT a.*
            FROM lumonata_articles AS a
            INNER JOIN lumonata_additional_fields AS b
                ON (a.larticle_id=b.lapp_id)
            WHERE a.larticle_status=%s
                AND a.larticle_type=%s '.$where_add.'
                AND b.lkey=%s
                AND b.lvalue=%s
            ORDER BY a.lorder ASC, a.lpost_date DESC';
            $q = $db->prepare_query( $s, 'publish', $post_type, 'destination', $destination );
    }
    else
    {
        $s = 'SELECT * FROM lumonata_articles WHERE larticle_status=%s AND larticle_type=%s '.$where_add.' ORDER BY lorder ASC, lpost_date DESC';
        $q = $db->prepare_query( $s, 'publish', $post_type );
    }
    $r = $db->do_query( $q );
    $n = $db->num_rows( $r );

    $pages  = isset( $page ) ? $page : 1;
    $limit  = $viewed * ( $pages - 1 );
    $start  = $viewed * ( $pages - 1 ) + 1;

    $s2 = $s . ' LIMIT %d, %d';
    if(!empty($destination))
    {
        $q2 = $db->prepare_query( $s2, 'publish', $post_type, 'destination', $destination, $limit, $viewed );
    }
    else
    {
        $q2 = $db->prepare_query( $s2, 'publish', $post_type, $limit, $viewed );
    }
    $r2 = $db->do_query( $q2 );
    $n2 = $db->num_rows( $r2 );

    $result = array();
    if( $n > 0 )
    {
        while( $d = $db->fetch_array( $r2 ) )
        {
            if($simple == 0){
                $result[] = array(
                    'post_sef'                    => $d['lsef'],
                    'post_id'                     => $d['larticle_id'],
                    'post_date'                   => $d['lpost_date'],
                    'post_title'                  => $d['larticle_title'],
                    'post_subtitle'               => $d['larticle_subtitle'],
                    'post_content'                => $d['larticle_content'],
                    'post_permalink'              => permalink( $d['larticle_id'] ),
                    'post_permalink'              => HTTP.site_url().'/'.$destination.'/'.$d['larticle_type'].'/'.$d['lsef'].'.html',
                    'post_category'               => get_post_category( $d['larticle_id'] ),
                    'post_featured_img'           => get_featured_img( $d['larticle_id'], $post_type, false ),
                    'post_featured_img_title'     => get_additional_field( $d['larticle_id'], 'featured_img_title', $post_type ),
                    'post_featured_img_alt'       => get_additional_field( $d['larticle_id'], 'featured_img_alt', $post_type ),
                    'post_featured_img_copyright' => get_additional_field( $d['larticle_id'], 'featured_img_copyright', $post_type ),
                    'post_brief'                  => get_post_brief( $d['larticle_id'], $post_type, $d['larticle_content'] ) ,
                    'post_attachment'             => get_post_attachment( $d['larticle_id'], 'gallery' ),
                );
            }
            else
            {
                $result[] = array(
                    'post_sef'                    => $d['lsef'],
                    'post_id'                     => $d['larticle_id'],
                    'post_date'                   => $d['lpost_date'],
                    'post_title'                  => $d['larticle_title'],
                    'post_content'                => $d['larticle_content'],
                );
            }
        }

    }

    return $result;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN DETAIL DATA DARI SEF URL
| -------------------------------------------------------------------------------
*/
function get_post_detail( $sef, $type = 'blogs' )
{
    global $db;

    $d = fetch_artciles( 'sef=' . $sef . '&type=' . $type );

    $items = array();

    if( !empty($d) )
    {
        $items  = array(
            'post_sef'                    => $d['lsef'],
            'post_id'                     => $d['larticle_id'],
            'post_date'                   => $d['lpost_date'],
            'post_title'                  => $d['larticle_title'],
            'post_subtitle'               => $d['larticle_subtitle'],
            'post_content'                => $d['larticle_content'],
            'post_type'                   => $d['larticle_type'],
            'post_permalink'              => permalink( $d['larticle_id'] ),
            'post_category'               => get_post_category( $d['larticle_id'] ),
            'post_attachment'             => get_post_attachment( $d['larticle_id'], 'gallery' ),
            'post_featured_img'           => get_featured_img( $d['larticle_id'], $type, false ),
            'post_featured_img_title'     => get_additional_field( $d['larticle_id'], 'featured_img_title', $type ),
            'post_featured_img_alt'       => get_additional_field( $d['larticle_id'], 'featured_img_alt', $type ),
            'post_featured_img_copyright' => get_additional_field( $d['larticle_id'], 'featured_img_copyright', $type ),
            'post_brief'                  => get_post_brief( $d['larticle_id'], $type, $d['larticle_content'] )
        );
    }

    return $items;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN FILE ATTACHMENT BERDASARKAN ID ARTICLE
| -------------------------------------------------------------------------------
*/
function get_post_attachment( $id, $app_name = 'attachment' )
{
    global $db;

    $s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND lattach_app_name = %s ORDER BY lorder';
    $q = $db->prepare_query( $s, $id, $app_name );
    $r = $db->do_query( $q );

    $data = array();

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $filename = explode( '/', $d['lattach_loc_thumb'] );
            $filename = array_reverse( $filename );

            $data[] = array(
                'img_id'              => $d['lattach_id'],
                'img_title'           => $d['ltitle'],
                'img_alt'             => $d['lalt_text'],
                'img_caption'         => $d['lcaption'],
                'img_youtube_url'     => $d['lyoutube_url'],
                'img_photo_360'       => $d['lphoto_360'],
                'img_thumb'           => HTTP . site_url() . $d['lattach_loc_thumb'],
                'img_medium'          => HTTP . site_url() . $d['lattach_loc_medium'],
                'img_large'           => HTTP . site_url() . $d['lattach_loc_large'],
                'img_original'        => HTTP . site_url() . $d['lattach_loc'],
                'img_attach_language' => $d['lattach_language'],
            );
        }
    }

    return $data;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN CATEGORY BERDASARKAN ID ARTICLE
| -------------------------------------------------------------------------------
*/
function get_post_category( $id )
{
    global $db;

    $d = fetch_rule_relationship( 'app_id=' . $id );

    $category = array();

    foreach( $d as $id )
    {
        $rule       = fetch_rule( 'rule_id=' . $id );
        $category[] = array(
            'sef' => $rule['lsef'],
            'name' => $rule['lname']
        );
    }

    return $category;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN POST BRIEF BERDASARKAN ID ARTICLE
| -------------------------------------------------------------------------------
*/
function get_post_brief( $id, $apps, $content = '' )
{
    $brief = get_additional_field( $id, 'brief', $apps );

    if( empty( $brief ) )
    {
        if( !empty( $content ) )
        {
            $brief = limit_words( strip_tags( $content ), 40 );
        }
    }
    else
    {
        $brief = nl2br( $brief );
    }

    return $brief;
}

/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN ID DESTINATION
| -------------------------------------------------------------------------------
*/
function get_id_article_by_sef($sef, $type)
{
    global $db;

    $q = $db->prepare_query("
        SELECT larticle_id
        FROM lumonata_articles
        WHERE lsef=%s AND larticle_type=%s AND larticle_status=%s
        LIMIT 0,1
    ",
        $sef, $type, "publish"
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $article_id = "";
    if($n > 0)
    {
        $d = $db->fetch_array($r);
        $article_id = $d['larticle_id'];
    }
    return $article_id;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| GET Page Template
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_page_content($corporatedata, $sef, $data_page='', $lang='', $check_lang=false, $string_translations='')
{
    set_variable_string_translations($lang, $check_lang, $string_translations);

    $destination = $corporatedata[0]['sef'];

    if($sef == "accommodation" || $sef == "dining" || $sef == "spa" || $sef == "events" || $sef == "weddings" || $sef == "what-to-do-act-in" || $sef == "what-to-do-around")
    {
        return page_template($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    else if($sef == "special-offers")
    {
        return offers_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    else if($sef == "news" || $sef == "press-download")
    {
        return news_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    else if($sef == "contact-us")
    {
        return contact_content($destination, $corporatedata, $data_page, $lang, $check_lang);
    }
    else if($sef == "locations")
    {
        return location_content($destination, $corporatedata, $lang, $check_lang);
    }
    else if($sef == "about-us")
    {
        return about_content($destination, $corporatedata, $data_page, $lang, $check_lang);
    }
    else if($sef == "policy")
    {
        return static_content($destination, $corporatedata, $data_page, $lang, $check_lang);
    }
    else if($sef == "press-gallery" || $sef == "gallery")
    {
        return press_gallery_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    else if($sef == "press-accolades")
    {
        return accolades_content($destination, $corporatedata, $data_page, $lang, $check_lang);
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| GET Page Detail Template
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_page_detail_content($corporatedata, $sef, $data_page='', $lang='', $check_lang=false, $string_translations='')
{
    set_variable_string_translations($lang, $check_lang, $string_translations);
    $destination = $corporatedata[0]['sef'];

    if(is_accommodation_detail())
    {
        return accommodation_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_dining_detail())
    {
        return dining_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_spa_detail())
    {
        return spa_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_offers_detail())
    {
        return offers_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_events_detail())
    {
        return events_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_weddings_detail())
    {
        return weddings_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_what_to_do_detail())
    {
        return whattodo_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
    elseif(is_news_detail())
    {
        return news_detail_content($destination, $corporatedata, $data_page, $lang, $check_lang, $string_translations);
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN URL GAMBAR HERO IMAGE DARI POST TYPE SETTING META DATA BG IMAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_url_hero_image_post_setting($bg_image)
{
    $small_path  =  PLUGINS_PATH . '/custom-post/background/small';
    $medium_path =  PLUGINS_PATH . '/custom-post/background/medium';
    $large_path  =  PLUGINS_PATH . '/custom-post/background/large';
    $origin_path =  PLUGINS_PATH . '/custom-post/background';

    $bg_image_small  = file_exists( $small_path . '/' . $bg_image ) ? HTTP . site_url() . '/lumonata-plugins/custom-post/background/small/' . $bg_image : '';
    $bg_image_medium = file_exists( $medium_path . '/' . $bg_image ) ? HTTP . site_url() . '/lumonata-plugins/custom-post/background/medium/' . $bg_image : '';
    $bg_image_large  = file_exists( $large_path . '/' . $bg_image ) ? HTTP . site_url() . '/lumonata-plugins/custom-post/background/large/' . $bg_image : '';
    $bg_image_origin = file_exists( $origin_path . '/' . $bg_image ) ? HTTP . site_url() . '/lumonata-plugins/custom-post/background/' . $bg_image : '';

    $data = array(
        'bg_image'         => $bg_image_origin,
        'bg_image_small'   => $bg_image_small,
        'bg_image_large'   => $bg_image_large,
        'bg_image_medium'  => $bg_image_medium,
    );

    return $data;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| SET POST GALLERY
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_post_gallery( $gallery = array() )
{
    add_block( 'list-gallery', 'lgblock', 'page_template' );
    add_block( 'gallery-block', 'gblock', 'page_template' );

    if( !empty( $gallery ) )
    {
        foreach( $gallery as $d )
        {
            add_variable( 'img_alt', $d['img_alt'] );
            add_variable( 'large_url', $d['img_large'] );
            add_variable( 'img_title', $d['img_title'] );
            add_variable( 'thumb_url', $d['img_medium'] );

            parse_template( 'list-gallery', 'lgblock', true );
        }

        add_variable( 'gallery_css', '' );
    }
    else
    {
        add_variable( 'gallery_css', 'sr-only' );
    }

    parse_template( 'gallery-block', 'gblock', false );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| SET POST GALLERY HTML
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_post_gallery_html( $gallery = array() )
{
    $html = "";
    if( !empty( $gallery ) )
    {
        $html .= '<div class="inner">';
        foreach( $gallery as $d )
        {
            $img_large = $d['img_large'];
            $img_large = str_replace(" ", "%20", $img_large);
            $html     .= '<div class="item lazy" data-src="'.$img_large.'"><div class="loader"></div></div>';
        }
        $html .= '</div>';
    }

    return $html;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK EXPLODE STRING YANG MEMILIKI KATA AT ATAU OF
| -------------------------------------------------------------------------------------------------------------------------
*/
function explode_title_by_at_of($delimiter, $title){
    $title = explode($delimiter, strtolower($title));

    $new_title = $title;
    if(count($title) > 0)
    {
        $new_title = "";
        for($i=0; $i < count($title); $i++)
        {
            if($i+1 == count($title))
            {
                $new_title .= "<br/> <span><i>".strtolower($delimiter)."</i> ".strtoupper($title[$i])."</span>";
            }
            else
            {
                $new_title .= strtoupper($title[$i]);
            }
        }
    }

    return $new_title;
}


/*
| -------------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN RULE LIST
| -------------------------------------------------------------------------------
*/
function get_rule_list_post($post_id, $group, $rule, $element=0, $lang='', $check_lang=false)
{
    $rules = get_additional_field( $post_id, $rule, $group );
    $rules = json_decode( $rules, true );
    $content = '';


    if( is_array( $rules ) && !empty( $rules ) )
    {
        if(!empty($rules[0]))
        {
            $content .= "<ul>";
            foreach( $rules as $key => $obj )
            {
                $data = explode( ',', $obj );

                foreach( $data as $d )
                {
                    if( empty( $d ) ) continue;

                    $value = fetch_rule( 'rule_id=' . $d, true );
                    $lname = $value['lname'];

                    // LANGUAGE CHECK
                    if($check_lang)
                    {
                        $lname_lang = get_additional_rule_field($d, 'name_'.$lang, $group);
                        $lname   = (empty($lname_lang) ? $lname:     $lname_lang);
                    }

                    if( isset( $lname ) && !empty( $lname) )
                    {
                        if($element==1)
                        {
                            $content .= '<li><span>' . $lname . '</span></li>';
                        }
                        else
                        {
                            $content .= '<li>' . $lname . '</li>';
                        }
                    }
                }
            }
            $content .= "</ul>";
        }
    }

    return $content;
}


/*
| -----------------------------------------------------------------------------
| Get Instagram Photo
| -----------------------------------------------------------------------------
*/
function get_instagram_photo()
{
    $access_token = '1969218930.1677ed0.cb5cdb61402245beb3b1effcda80bd4a';
    // $access_token = get_meta_data("ig_token", 'static_setting');
    $photo_count  = 9;

    $json_link  = "https://api.instagram.com/v1/users/self/media/recent/?";
    $json_link .= "access_token={$access_token}&count={$photo_count}";

    $json = get_web_page($json_link);
    $obj  = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);

    $list = "";
    if(!empty($obj['data'])){
        $list = '
        <div class="grid">
            <div class="grid-sizer"></div>
        ';
        foreach ($obj['data'] as $post) {
            $pic_src  = str_replace('s150x150/', 's320x320/', $post['images']['standard_resolution']['url']);
            $link_src = $post['link'];
            $list    .= '<div class="grid-item"><a href="'.$post['link'].'" target="_blank"><img data-src="'.$pic_src.'" class="img-responsive lazy" alt=""></a></div>';
            // $list    .= '<li><a href="'.$post['link'].'" target="_blank"><img data-src="'.$pic_src.'" class="img-responsive lazy" alt=""></a></li>';
        }
        $list .= '
        </div>
        ';
    }

    return $list;
}


/*
| -----------------------------------------------------------------------------
| Get Social Media Photo
| -----------------------------------------------------------------------------
*/
function get_social_media_photo($destinations='', $dest_id='')
{
    $obj = array();
    // GET INSTAGRAM PHOTO
    if($destinations == "seminyak" || $destinations == "ubud")
    {
        $access_token = get_ig_access_token($dest_id);
        $photo_count  = 9;

        $json_link  = "https://api.instagram.com/v1/users/self/media/recent/?";
        $json_link .= "access_token={$access_token}&count={$photo_count}";

        $json = get_web_page($json_link);
        $obj  = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);

        $list = "";
        $list_array = array();
    }

    if(!empty(isset($obj['data']))){
        foreach ($obj['data'] as $post) {
            $pic_src  = str_replace('s150x150/', 's320x320/', $post['images']['standard_resolution']['url']);
            $link_src = $post['link'];
            $list    .= '<div class="grid-item"><a href="'.$post['link'].'" target="_blank"><img data-src="'.$pic_src.'" class="img-responsive lazy-instagram" alt=""></a></div>';
            // $list    .= '<li><a href="'.$post['link'].'" target="_blank"><img data-src="'.$pic_src.'" class="img-responsive lazy" alt=""></a></li>';

            $list_array[] = array(
                'html' => $list,
                'type' => "instagram"
            );
        }
    }



    // GET FACEBOOK PAGE FOTO SAMAYA SEMINYAK
    // $fields = "id,name,description,link,cover_photo,count";
    // $fb_page_id = "496629153768014";
    // $fbaccess_token = "EAAN9kyGqLvUBAJoRVOqZAWZANnw9lkMmVwOZCvzcBYhnpNKv0HM2ZCZBfOZBFL9kznmZAIoYJdQylJpBIo9wMWUEDzBeV2VyvysavZAPVBJU2ZBU5yg3VD9iqjpLYj3mAQY37i4MqZAAneMXwuUkf8J7LAxtqW0mv9Y1iK8ZCvJZCCs9sPkfc6AJZAERENjpj3rMNvltZAlUArWHLYpwZDZD";
    // $graphAlbLink = "https://graph.facebook.com/v2.9/{$fb_page_id}/albums?fields={$fields}&access_token={$fbaccess_token}";

    // $jsonData = file_get_contents($graphAlbLink);
    // $fbAlbumObj = json_decode($jsonData, true, 512, JSON_BIGINT_AS_STRING);

    // // Facebook albums content
    // $fbAlbumData = $fbAlbumObj['data'];

    // print_r($fbAlbumData);

    // if(!empty($fbAlbumData))
    // {
    //     foreach($fbAlbumData as $data)
    //     {
    //         $id = isset($data['id'])?$data['id']:'';
    //         $name = isset($data['name'])?$data['name']:'';
    //         $description = isset($data['description'])?$data['description']:'';
    //         $link = isset($data['link'])?$data['link']:'';
    //         $cover_photo_id = isset($data['cover_photo']['id'])?$data['cover_photo']['id']:'';
    //         $count = isset($data['count'])?$data['count']:'';

    //         $pictureLink = "photos.php?album_id={$id}&album_name={$name}";


    //         $list    .= "<div class=\"grid-item\"><a href=".$pictureLink." target=\"_blank\"><img src='https://graph.facebook.com/v2.9/{$cover_photo_id}/picture?access_token={$access_token}' class=\"img-responsive\" alt=''></a></div>";

    //         $list_array[] = array(
    //             'html' => $list,
    //             'type' => "facebook"
    //         );
    //     }
    // }

    // shuffle($list_array);

    $html_list = "";
    // SET HTML LIST SOCIAL MEDIA PHOTO
    if(!empty($list))
    {
        $html_list = '
        <div class="grid">
            <div class="grid-sizer"></div>
            '.$list.'
        </div>
        ';
    }

    return $html_list;
}


/*
| -----------------------------------------------------------------------------
| CURL HTTPS
| -----------------------------------------------------------------------------
*/
function get_web_page( $url )
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        // CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;

    return $content;
}


/*
| -----------------------------------------------------------------------------
| Get Data Locations
| -----------------------------------------------------------------------------
*/
function get_location_datas($post)
{
    global $db;

    $destination = $post['destination'];

    $s = 'SELECT a.* FROM lumonata_articles AS a INNER JOIN lumonata_additional_fields AS b ON (a.larticle_id=b.lapp_id) WHERE a.larticle_type=%s AND b.lkey=%s AND b.lvalue=%s';
    $q = $db->prepare_query($s, 'locations', 'destination', $destination);
    $r = $db->do_query($q);

    $f = array();

    if ($db->num_rows($r) > 0) {
        while ($d = $db->fetch_array($r)) {
            $title      = $d['larticle_title'];
            $loc_type   = get_additional_field($d['larticle_id'], 'loc_type', 'locations');
            $longitude  = get_additional_field($d['larticle_id'], 'longitude', 'locations');
            $latitude   = get_additional_field($d['larticle_id'], 'latitude', 'locations');
            $get_off    = get_additional_field($d['larticle_id'], 'get_off', 'locations');
            $img        = get_additional_field($d['larticle_id'], 'locations_image', 'locations');
            if (!empty($img)) {
                $url_img    = HTTP . site_url() . '/lumonata-plugins/locations/background/'.$img;
            } else {
                $url_img    = HTTP . site_url() . '/lumonata-plugins/locations/images/official.jpg';
            }


            if ($loc_type == 'Attractions') {
                $icon = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-attraction.svg';
                $icon_hover = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-attraction-hover.svg';
            } elseif ($loc_type == 'Shops') {
                $icon = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-shop.svg';
                $icon_hover = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-shop-hover.svg';
            } elseif ($loc_type == 'Restaurants') {
                $icon = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-restaurant.svg';
                $icon_hover = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-restaurant-hover.svg';
            } elseif ($loc_type == 'Misc') {
                $icon = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-misc.svg';
                $icon_hover = HTTP . site_url() . '/lumonata-plugins/locations/images/marker-misc-hover.svg';
            }

            $f[] = array(
                'title'      => $title,
                'loc_type'   => $loc_type,
                'longitude'  => $longitude,
                'latitude'   => $latitude,
                'get_off'    => $get_off,
                'img'        => $url_img,
                'icon'       => $icon,
                'icon_hover' => $icon_hover,
                'class'      => strtolower($loc_type)
          );
        }
    }
    return $f;
}

/*
| -----------------------------------------------------------------------------
| SET Other Destination
| -----------------------------------------------------------------------------
*/
function set_other_destination($appname = 'seminyak', $lang='', $check_lang=0)
{
    $lang_url = (($check_lang) ? '/'.$lang : '');
    if($appname == "ubud")
    {
        $other_dest_name = "SEMINYAK";
        $url_other       = HTTP.site_url().$lang_url.'/seminyak';
    }
    else
    {
        $other_dest_name = "UBUD";
        $url_other       = HTTP.site_url().$lang_url.'/ubud';
    }

    add_variable( 'other_dest_name', $other_dest_name );
    add_variable( 'url_other', $url_other );
}

/*
| -----------------------------------------------------------------------------
| Function untuk mendapatkan string translations
| -----------------------------------------------------------------------------
*/
function get_string_translation_lang()
{
    global $db;
    $qstring = $db->prepare_query("
        SELECT larticle_id, larticle_content
        FROM lumonata_articles
        WHERE larticle_type=%s AND larticle_status=%s
        LIMIT 0,1
    ",
        'string-translations', 'publish'
    );
    $rstring = $db->do_query($qstring);
    $nstring = $db->num_rows($rstring);

    $content_string = '';

    if($nstring > 0)
    {
        $dstring = $db->fetch_array($rstring);
        $string_id = $dstring['larticle_id'];

        $content_string = $dstring['larticle_content'];
    }

    return $content_string;
}

function set_variable_string_translations($lang='', $check_lang=false, $string_translations='')
{
    $string_translations = json_decode($string_translations, true);

    if(!empty($string_translations))
    {
        foreach($string_translations as $key=> $value)
        {
            $variable_name = "text_".str_replace(" ", "_", strtolower($key));
            $value_string = $value['en'];

            if($check_lang)
            {
                $string_lang = $value[$lang];
                $value_string  = (empty($string_lang) ? $value_string:   $string_lang);
            }

            add_variable($variable_name, $value_string);
        }
    }
}


/*
| ---------------------------------------------------------------------------------
| FUNCTION UNTUK SET VARIABLE STRING LANGUAGE UNTUK DI TAMPILKAN DI KURUNG KURAWAL
| ---------------------------------------------------------------------------------
*/
function set_string_language($translations=array(), $lang='', $check_lang=false, $string_translations='')
{
    $string_translations = attemp_actions( 'string_translations' );

    $res = array();

    $string_translatiion_decode = json_decode($string_translations, true);

    foreach($string_translatiion_decode as $key => $val)
    {
        $name_string = $key;
        $variable_name = "text_".str_replace(" ", "_", strtolower($name_string));
        $variable_name_1 = str_replace(" ", "_", strtolower($name_string));

        $value_string = '';

        if(isset($string_translatiion_decode[$key]))
        {
            $value = $string_translatiion_decode[$key];
            $value_string = $value['en'];

            if($check_lang)
            {
                $string_lang = $value[$lang];
                $value_string  = (empty($string_lang) ? $value_string:   $string_lang);
            }
        }

        add_variable($variable_name, $value_string);

        $res[$variable_name_1] = $value_string;

        // echo $value_string."<br/>";
    }

    return $res;

    // print_r(json_decode($string_translations, true));

    // if(!empty($translations))
    // {
    //     foreach($translations as $d)
    //     {
    //         $name_string = $d;
    //         $variable_name = "text_".str_replace(" ", "_", strtolower($name_string));
    //         $variable_name_1 = str_replace(" ", "_", strtolower($name_string));

    //         $value_string = get_string_translation_lang_by_name($string_translations, $name_string, $lang, $check_lang);
    //         $value_string = (empty($value_string) ? $name_string : $value_string);

    //         add_variable($variable_name, $value_string);

    //         $res[$variable_name_1] = $value_string;
    //     }
    // }

    // return $res;
}

/*
| -----------------------------------------------------------------------------
| Function untuk mendapatkan string translations sesuai name
| -----------------------------------------------------------------------------
*/
function get_string_translation_lang_by_name($string_translations, $name='', $lang='', $check_lang=false)
{
    echo $name;
    $res = '';
    if(!empty($string_translations))
    {
        $string_translations = json_decode($string_translations, true);

        if(isset($string_translations[$name]))
        {
            $value = $string_translations[$name];
            $res = $value['en'];

            if($check_lang)
            {
                $string_lang = $value[$lang];
                $res  = (empty($string_lang) ? $res:   $string_lang);
            }
        }
    }

    return $res;
}



function get_facebook_photo()
{
    $fields = "id,name,description,link,cover_photo,count";
    $fb_page_id = "317120665377062";
    $access_token = "EAAJzsId9iCEBAG0B0IXxsZChwpAD6J7rb03zhytIqYCcwj8dqNUbwjuNbgmKeaMULUnpvndK4Cgu16TSjspocHMGYv1Sn7LDSyR4iHno8t30IzFL6qGpN5heGhMOqrGGw1eclKZAAgKdVnWzljpJsLj4ZCBJnQ34AWg9wpeF2mnyLpBqZBZAg8cyNPJZBYkEYqdOZCR4341XwZDZD";
    $graphAlbLink = "https://graph.facebook.com/v2.9/{$fb_page_id}/albums?fields={$fields}&access_token={$access_token}";

    $jsonData = file_get_contents($graphAlbLink);
    $fbAlbumObj = json_decode($jsonData, true, 512, JSON_BIGINT_AS_STRING);

    // Facebook albums content
    $fbAlbumData = $fbAlbumObj['data'];

    foreach($fbAlbumData as $data)
    {
        $id = isset($data['id'])?$data['id']:'';
        $name = isset($data['name'])?$data['name']:'';
        $description = isset($data['description'])?$data['description']:'';
        $link = isset($data['link'])?$data['link']:'';
        $cover_photo_id = isset($data['cover_photo']['id'])?$data['cover_photo']['id']:'';
        $count = isset($data['count'])?$data['count']:'';

        $pictureLink = "photos.php?album_id={$id}&album_name={$name}";

        echo "<img src='https://graph.facebook.com/v2.9/{$cover_photo_id}/picture?access_token={$access_token}' alt=''>";

        // echo $pictureLink;
    }
}

/*
| -----------------------------------------------------------------------------
| Function untuk menyimpan data subscribe ke mailchimp list
| -----------------------------------------------------------------------------
*/
function mailchimp_subscribe($post_data)
{
    $string_translations = attemp_actions( 'string_translations' );

    $destination = $post_data['destination'];
    $dest_id = get_id_article_by_sef($destination, 'destinations');

    $result['status'] = "error";
    $result['message'] = "something went wrong, please try again later";

    // if(!empty($dest_id))
    // {
    //     $api_key   = get_additional_field($dest_id, 'mailchimp_api_key', 'destinations');
    //     $list_id   = get_additional_field($dest_id, 'mailchimp_list_id', 'destinations');

    //     $data = '';

    //     $MailChimp    = new MailChimp($api_key);
    //     $MailChimp->verify_ssl = false;

    //     $post_mailchimp = $MailChimp->post("lists/$list_id/members", [
    //         'email_address' => $post_data['email'],
    //         'status'        => 'pending',
    //         'merge_fields'  => [
    //             'NAME'    => $post_data['name'],
    //         ],
    //     ]);

    //     if($post_mailchimp){
    //         $result['status'] = "success";
    //         $result['message'] = "<h2>Thank you for subscribing to our newsletter</h2>";
    //     }
    // }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| Function untuk mendapatkan data country
| -----------------------------------------------------------------------------
*/
function get_data_country()
{
    global $db;

    $q = $db->prepare_query("
        SELECT lcountry, lcountry_id
        FROM lumonata_country
        WHERE llang_id=6
        ORDER BY lcountry ASC
    ");
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $country_option = "";
    if($n > 0)
    {
        while($d = $db->fetch_array($r))
        {
            $country         = $d['lcountry'];
            $country_option .= '<option value="'.$country.'">'.$country.'</option>';
        }
    }

    return $country_option;
}


/* ============================================================================================= */
// FUNCTION UNTUK CROBJOB WEB SCRAPPING DAN SAVE KE DATABASE
/* ============================================================================================= */
function scrapping_save_database(){
    global $db;

    $max_i = 2;

    $qdes = $db->prepare_query("SELECT larticle_id, larticle_title, lsef FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title ASC", "destinations", "publish");
    $rdes = $db->do_query($qdes);
    $ndes = $db->num_rows($rdes);

    if($ndes > 0)
    {
        while($ddes = $db->fetch_array($rdes))
        {
            $sef = $ddes['lsef'];
            $dest_id = $ddes['larticle_id'];
            $html = "";

            if($sef == "seminyak")
            {
                $html = file_get_contents('https://www.tripadvisor.com/Hotel_Review-g469404-d603376-Reviews-The_Samaya_Bali_Seminyak-Seminyak_Kuta_District_Bali.html');
            }
            elseif($sef == "ubud")
            {
                $html = file_get_contents('https://www.tripadvisor.com/Hotel_Review-g297701-d1810099-Reviews-The_Samaya_Bali_Ubud-Ubud_Bali.html');
            }

            $doc = new DOMDocument();
            libxml_use_internal_errors(TRUE);

            if(!empty($html)){
                $rating = "";
                $overview = "";

                $doc->loadHTML($html);
                libxml_clear_errors();

                $xpath = new DOMXPath($doc);

                // GET REVIEW COUNT
                $rows_review_count = $xpath->query('//span[@class="reviewCount"]');
                $review_count = "";
                if($rows_review_count->length > 0){
                    foreach($rows_review_count as $row_review_count){
                        $review_count = $row_review_count->nodeValue;
                    }
                }

                // GET OVERALL RATING
                $rows_overall_rating = $xpath->query('//span[@class="overallRating"]');
                $overall_rating = "";
                if($rows_overall_rating->length > 0){
                    foreach($rows_overall_rating as $row_overall_rating){
                        $overall_rating = $row_overall_rating->nodeValue;
                    }
                }

                // GET TRAVELLER CHOICE BADGE
                $rows_tc_badge = $xpath->query('//span[@class="award_text"]');

                $tc_badge = "";
                if($rows_tc_badge->length > 0){
                    foreach($rows_tc_badge as $row_tc_badge){
                        $tc_badge = $row_tc_badge->nodeValue;
                    }
                }
                $result['review_count']   = $review_count;
                $result['overall_rating'] = $overall_rating;
                $result['tc_badge']       = $tc_badge;

                $value = json_encode($result);

                set_scraaping_setting('trip_advisor_samaya', $value, $dest_id);
            }
        }
    }
}

/* ============================================================================================= */
// FUNCTION UNTUK PROSES SIMPAN DAN UPDATE META DATA SCRAPPING SETTING
/* ============================================================================================= */
function set_scraaping_setting($key, $val, $destination)
{
    global $db;

    if(is_num_scrapping_setting($key, $destination))
    {
        $s = 'UPDATE lumonata_meta_data SET lmeta_value=%s
        WHERE lmeta_name=%s AND lapp_name=%s AND lapp_id=%d';
        $q = $db->prepare_query( $s, $val, $key, 'scrapping_value', $destination );
        $r = $db->do_query( $q );
    }
    else
    {
        $s = 'INSERT INTO lumonata_meta_data(
                    lmeta_name,
                    lmeta_value,
                    lapp_name,
                    lapp_id)
                VALUES(%s,%s,%s,%d)';
        $q = $db->prepare_query( $s, $key, $val, 'scrapping_value', $destination );
        $r = $db->do_query( $q );
    }

    return true;
}


/* ============================================================================================= */
// FUNCTION UNTUK MENGECEK APAKAH META DATA SCRAPPING SETTING SUDAH ADA ATAU BELUM PADA DATABASE
/* ============================================================================================= */
function is_num_scrapping_setting($key, $destination)
{
    global $db;

    $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s AND lapp_id=%d';
    $q = $db->prepare_query( $s, $key, 'scrapping_value', $destination );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if( $n > 0 )
    {
        return true;
    }

    return false;
}

/* ============================================================================================= */
// FUNCTION UNTUK MENDAPATKAN RULE LIST
/* ============================================================================================= */
function get_rule_list_front($group, $rule, $lang='', $check_lang=false, $html=true, $with_sef=false)
{
    global $db;

    $q = $db->prepare_query(
        "SELECT lrule_id, lname, lsef FROM lumonata_rules WHERE lrule=%s AND lgroup=%s ORDER BY lorder ASC",
        $rule, $group
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $content = "";
    $content_no_html = array();

    if($n > 0)
    {
        $content .= "<ul>";
        while($d = $db->fetch_array($r))
        {
            $id    = $d['lrule_id'];
            $lname = $d['lname'];
            $lsef  = $d['lsef'];

            if($check_lang)
            {
                $lname_lang = get_additional_rule_field($id, 'name_'.$lang, $group);
                $lname   = (empty($lname_lang) ? $lname:     $lname_lang);
            }

            if( isset( $lname ) && !empty( $lname) )
            {
                if(!$html)
                {
                    if($with_sef)
                    {
                        $content_no_html[] = array(
                            'sef'  => $lsef,
                            'name' => $lname,
                            'id'   => $id
                        );
                    }
                    else
                    {
                        $content_no_html[] .= $lname;
                    }
                }
                else
                {
                    $content .= '<li>' . $lname . '</li>';
                }
            }
        }
        $content .= "</ul>";
    }

    if(!$html)
    {
        return $content_no_html;
    }

    return $content;
}


/* ============================================================================================= */
// FUNCTION UNTUK MENDAPATKAN V CODE YOUTUBE
/* ============================================================================================= */
function get_v_code_youtube($url)
{
    $url = $url;
    parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
    return $my_array_of_vars['v'];
}

function get_iframe_youtube($vcode='')
{
    $html = "";
    if(!empty($vcode))
    {
        $html = '
            <iframe id="ytplayer" type="text/html" width="420" height="345" src="https://www.youtube.com/embed/'.$vcode.'?autoplay=0&cc_load_policy=0&theme=dark&color=white&controls=0&disablekb=0&iv_load_policy=3&loop=1&modestbranding=1&rel=0&showinfo=0&html5=1&autohide=1"
            frameborder="0" allowfullscreen></iframe>
        ';
    }
    return $html;
}


function get_list_dest($lang='', $check_lang=false)
{
    // GET LIST CORPORATE
    $corporate        = get_list_corporate($lang, $check_lang);
    $destination_list = "";
    $dest_list        = "";
    $dest_hidden      = "";

    foreach( $corporate as $d )
    {
        $link = HTTP.site_url().'/'.$d['sef'];

        if($check_lang)
        {
            $link = HTTP.site_url().'/'.$lang.'/'.$d['sef'];
        }

        $destination_list .= '<li><a href="'.$link.'">'.$d['title'].'</a></li>';
        $dest_list        .= '<li data-value="'.$d['title'].'" data-hotelsname="'.$d['hotels_name'].'" data-properties="'.$d['properties_name'].'">'.$d['title'].'</li>';
        $dest_hidden      .= '<div class="booking-data-'.strtolower($d['title']).'" data-hotelsname="'.$d['hotels_name'].'" data-properties="'.$d['properties_name'].'"></div>';
    }
    add_actions( 'dest_list', $dest_list );
    add_actions( 'dest_hidden', $dest_hidden );
}


/* ============================================================================================= */
// FUNCTION UNTUK COUNT DATA OF RULE
/* ============================================================================================= */
function get_count_rule_article($id_rule, $destination='')
{
    global $db;

    $q = $db->prepare_query("
        SELECT a.lapp_id
        FROM lumonata_rule_relationship AS a
        INNER JOIN lumonata_articles AS b ON (a.lapp_id=b.larticle_id)
        INNER JOIN lumonata_additional_fields AS c ON (b.larticle_id=c.lapp_id)
        WHERE a.lrule_id=%d AND c.lkey=%s AND lvalue=%s AND lapp_name=%s
    ", $id_rule, "destination", $destination, "gallery");
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    return $n;
}
?>
