jQuery(document).ready(function () {
    button_choose_map_location();
    append_data();
});

if (jQuery('input[name=destination]').val() != "undefined") {
    var locations = function () {
        var json = null;
        var url = jQuery('input[name=site_url]').val();
        var dest = jQuery('input[name=destination]').val();

        jQuery.ajax({
            url: url + 'ajax-function/',
            type: 'POST',
            async: false,
            data: {
                pkey: 'view-locations',
                destination: dest
            },
            success: function (result) {
                json = JSON.parse(result);
            }
        });

        return json;
    }();
}


function maps_locations_page() {

    if (jQuery('.wrap-locations-page').length > 0) {
        var mapDiv = document.getElementById('map');
        var latitude = jQuery('[name=latitude]').val();
        var longitude = jQuery('[name=longitude]').val();
        var title_dest = jQuery('[name=title_destination]').val();

        var latLng = new google.maps.LatLng(latitude, longitude);

        map = new google.maps.Map(mapDiv, {
            center: latLng,
            zoom: 14,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            navigationControl: true,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            zoomControl: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.SMALL
            },
            scaleControl: true,
            scaleControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            scrollwheel: false,
            draggable: true,
            styles: [{
                    "featureType": "administrative",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#55585A"
                    }]
                },
                {
                    "featureType": "administrative.province",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry",
                    "stylers": [{
                            "lightness": "0"
                        },
                        {
                            "saturation": "0"
                        },
                        {
                            "color": "#f5f5f2"
                        },
                        {
                            "gamma": "1"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "all",
                    "stylers": [{
                            "lightness": "-3"
                        },
                        {
                            "gamma": "1.00"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry",
                    "stylers": [{
                        "color": "#E8E8E8"
                    }]
                },
                {
                    "featureType": "landscape.natural",
                    "elementType": "all",
                    "stylers": [{
                        "color": "#E8E8E8"
                    }]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [{
                            "visibility": "off"
                        },
                        {
                            "hue": "#ff0000"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#a3b8cd"
                    }]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "color": "#bae5ce"
                        },
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [{
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [{
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "color": "#fac9a9"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#787878"
                    }]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "all",
                    "stylers": [{
                            "color": "#ffffff"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry.fill",
                    "stylers": [{
                            "color": "#ffffff"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "road.highway.controlled_access",
                    "elementType": "geometry.stroke",
                    "stylers": [{
                        "color": "#dddddd"
                    }]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "all",
                    "stylers": [{
                            "visibility": "on"
                        },
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#787878"
                    }]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [{
                        "visibility": "off"
                    }]
                },
                {
                    "featureType": "road.local",
                    "elementType": "all",
                    "stylers": [{
                            "color": "#ffffff"
                        },
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#787878"
                    }]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [{
                        "visibility": "simplified"
                    }]
                },
                {
                    "featureType": "transit.station.airport",
                    "elementType": "labels.icon",
                    "stylers": [{
                            "hue": "#0a00ff"
                        },
                        {
                            "saturation": "-77"
                        },
                        {
                            "gamma": "0.57"
                        },
                        {
                            "lightness": "0"
                        }
                    ]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                        "color": "#43321e"
                    }]
                },
                {
                    "featureType": "transit.station.rail",
                    "elementType": "labels.icon",
                    "stylers": [{
                            "hue": "#ff6c00"
                        },
                        {
                            "lightness": "4"
                        },
                        {
                            "gamma": "0.75"
                        },
                        {
                            "saturation": "-68"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [{
                            "color": "#C3DBE5"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [{
                        "color": "#C3DBE5"
                    }]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [{
                            "lightness": "-49"
                        },
                        {
                            "saturation": "-53"
                        },
                        {
                            "gamma": "0.79"
                        }
                    ]
                }
            ]
        });

        var map_icon = jQuery('[name=map_icon]').val();
        var icon = {
            url: map_icon,
            scaledSize: new google.maps.Size(185, 76),
            // labelOrigin: new google.maps.Point(20, 18)
        }

        marker = new google.maps.Marker({
            position: latLng,
            map: map,
            title: title_dest,
            icon: icon
        });

        
        marker.setZIndex(2);

        setMarkers(map, locations);
    }
}

var gmarkers = [];
var imarkers = [];
var globalClose = [];
var image = [
    ['images/marker-attraction.svg'],
    ['images/marker-restaurant.svg'],
    ['images/marker-shop.svg'],
    ['images/marker-misc.svg']
];

function setMarkers(map, locations) {
    var infowindow = new google.maps.InfoWindow({
        maxWidth: 250
    });
    var marker, i;
    var closeMarker;
    var infowindow;

    var currentMark;
    var currenti;
    for (i = 0; i < locations.length; i++) {
        var imgIcon = locations[i]['icon'];
        var marker = new google.maps.Marker({
            position: {
                lat: parseFloat(locations[i]['latitude']),
                lng: parseFloat(locations[i]['longitude'])
            },
            category: locations[i]['loc_type'],
            map: map,
            zIndex: 1,
            icon: {
                url: imgIcon,
                scaledSize: new google.maps.Size(40, 40),
                labelOrigin: new google.maps.Point(20, 18)
            },
        });

        gmarkers.push(marker);
        imarkers.push(i);

        var value = 0;
        google.maps.event.addListener(marker, 'click', (function (marker, i) {

            return function () {

                jQuery('.partner').removeClass('active');

                var contentString = '<div id="gmaps-content">' +
                    '<div class="title">' +
                    locations[i]['title'] +
                    '</div>' +
                    '</div>';
                jQuery('#' + i).addClass('active');


                value = jQuery('.partner.active').attr('id');

                if (i == value) {
                    var icon_hover = {
                        url: locations[i]['icon_hover'],
                        scaledSize: new google.maps.Size(40, 40),
                        labelOrigin: new google.maps.Point(20, 18)
                    }

                    marker.setIcon(icon_hover);
                    marker.setZIndex(2);
                } else {
                    var icon = {
                        url: locations[i]['icon'],
                        scaledSize: new google.maps.Size(40, 40),
                        labelOrigin: new google.maps.Point(20, 18)
                    }

                    marker.setIcon(icon);
                    marker.setZIndex(1);
                }

                closeWindow();
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
                closeMarker = infowindow;
                globalClose.push(closeMarker);
                currentMark = this;
                currenti = i;
            }
        })(marker, i));      
    }

    google.maps.event.addListener(infowindow,'closeclick',function(){
        // console.log(currentMark);
        var icon = {
            url: locations[currenti]['icon'],
            scaledSize: new google.maps.Size(40, 40),
            labelOrigin: new google.maps.Point(20, 18)
        }
        currentMark.setIcon(icon);
        currentMark.setZIndex(1);
    });
}


function filterMarkers(category) {
    for (i = 0; i < locations.length; i++) {
        var icon = {
            url: locations[i]['icon'],
            scaledSize: new google.maps.Size(40, 40),
            labelOrigin: new google.maps.Point(20, 18)
        }

        marker = gmarkers[i];
        if (marker.category == category || category.length === 0) {
            marker.setVisible(true);
        } else {
            marker.setVisible(false);
        }

        marker.setIcon(icon);
    }
}


function button_choose_map_location() {
    jQuery('.container-cat-locations ul li a').click(function () {
        var data_filter = jQuery(this).data('filter');

        if (data_filter != "all") {
            filterMarkers(data_filter);
            append_data(data_filter);
        } else {
            filterMarkers('');
            append_data();
        }

        return false;
    });

    jQuery(document).on('click', '.partner', function (event) {
        event.preventDefault();
        jQuery('.partner').removeClass('active');
        jQuery(this).addClass('active');

        closeWindow();

        var value = jQuery(this).attr('id');
        var closedSelectetd = [];

        var infowindow = new google.maps.InfoWindow({
            maxWidth: 250
        });

        var currentMark;
        var currenti;
        for (i = 0; i < locations.length; i++) {
            marker = gmarkers[i];

            if (i == value) {

                var contentStringSelected = '<div id="gmaps-content">' +
                    '<div class="title">' +
                    locations[i]['title'] +
                    '</div>' +
                    '</div>';

                var icon_hover = {
                    url: locations[i]['icon_hover'],
                    scaledSize: new google.maps.Size(40, 40),
                    labelOrigin: new google.maps.Point(20, 18)
                }

                marker.setIcon(icon_hover);
                marker.setZIndex(3);

                infowindow.setContent(contentStringSelected);
                infowindow.open(map, marker);

                closedSelectetd = infowindow;
                globalClose.push(closedSelectetd);
                currentMark = marker;
                currenti = i;

            } else {

                var icon = {
                    url: locations[i]['icon'],
                    scaledSize: new google.maps.Size(40, 40),
                    labelOrigin: new google.maps.Point(20, 18)
                }

                marker.setIcon(icon);
                marker.setZIndex(1);
            }
        }

        google.maps.event.addListener(infowindow,'closeclick',function(){
            // console.log(currentMark);
            var icon = {
                url: locations[currenti]['icon'],
                scaledSize: new google.maps.Size(40, 40),
                labelOrigin: new google.maps.Point(20, 18)
            }
            currentMark.setIcon(icon);
            currentMark.setZIndex(1);
        });

    });
}

function closeWindow() {
    var closeTheWindows;

    if (globalClose.length != 0) {
        for (var i = 0; i < globalClose.length; i++) {
            closeTheWindows = globalClose[i];
            closeTheWindows.close();
        }
    }
}

function append_data(type) {
    jQuery('.container-location-list .scrollbar-macosx').empty();
    var number = 1;
    for (var i = 0; i < locations.length; i++) {
        if (type == undefined) {
            jQuery('.container-location-list .scrollbar-macosx').append("<div id='" + i + "' class='partner'><div class='title-partner'><span class='" + locations[i]['class'] + "'>" + number + ". " + locations[i]['title'] + "</span></div></div>");
            number++;
        } else if (type == locations[i]['loc_type']) {
            jQuery('.container-location-list .scrollbar-macosx').append("<div id='" + i + "' class='partner " + i + "'><div class='title-partner'><span class='" + locations[i]['class'] + "'>" + number + ". " + locations[i]['title'] + "</span></div></div>");
            number++;
        }
    }
}