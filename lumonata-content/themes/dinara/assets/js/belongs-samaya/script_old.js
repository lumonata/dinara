jQuery(document).ready(function () {
    header_initialize();
    initialize_aos();
    initialize_pace_loading();
    navbar_fixed_on_scroll();
    button_toggle();
    scroll_down_top();
    show_popup_gallery();
    show_booking_popup_spa();
    scroll_hero();
    slider_home_destination();
    slider_hero_detail();
    slider_page_list();
    scroll_bullet();
    menu_category_choose();
    menu_type_choose();
    about_page();
    config_popup();
    instagram_feed();
    slier_other_product();
    initialize_datepicker();
    config_menu();
    set_time_weather_header();
    contact_us_page();
    accommodation_page();
    spa_booking_config();
    config_subscribe_mailchimp_form();
    dining_booking_config();
    footer_menu();
    popup_menu();
    events_page();
    initialize_selectize();
    initialize_scrollbar();
    offers_booking_popup();
    get_weather_yahoo();
    weddings_booking_form();
    only_numeric();
    // footer_and_popup_menu();
    press_gallery_config();
    press_gallery_infinite_scroll();
    fancybox_initialize();
    menu_dining_popup();
});

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE PARALAX HEADER
| -------------------------------------------------------------------------------------------------------------------------
*/
function header_initialize(){
    // CHECK POSITION INPUT DATE RANGE PICKER HERO
    var position_date = 0;
    if(jQuery('.wrap-form-booking-hero .text-date').length > 0 ){
        var position_date = jQuery('.wrap-form-booking-hero .text-date').offset().top;
    }

    // PARALAX HEADER
    jQuery(window).on('scroll', function () {
        // jQuery('.daterangepicker').addClass('bottom_calendar');
        
        var st = $(this).scrollTop();
        var w = window.innerWidth;

        if(w >= 600){
            jQuery('header').css({
                'background-position': 'center calc(50% - ' + (st * .5) + 'px)'
            });
        }else{
            jQuery('header').css({
                'background-position': 'center calc(50% + ' + (st * .5) + 'px)'
            });
        }

        // jQuery('.wrap-list-product .container-slide-product .inner .item').css({
        //     'background-position': 'center calc(100% - ' + (st * .5) + 'px)'
        // });

        // console.log(st);

        if(st >= (position_date - 300)){
            jQuery('.page-all .daterangepicker').addClass('bottom_calendar');
        }else{
            jQuery('.page-all .daterangepicker').removeClass('bottom_calendar');
        }

    });
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE AOS
| -------------------------------------------------------------------------------------------------------------------------
*/
function initialize_aos() {
    AOS.init();
    jQuery(window).on('load', function () {
        AOS.init();
    });
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE PACE LOADING PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function initialize_pace_loading() {
    // PACEJS PAGE LOADER SETTING
    Pace.on('done', function () {
        lazy_images();
        jQuery('.wrap-page-loader').fadeOut(1000);

        // SCROLL TO RESERVATION SECTION PAGE EVENTS
        if(typeof localStorage.eventsBook != "undefined" && localStorage.eventsBook != ""){
            setTimeout(function(){
                jQuery('html, body').animate({
                    scrollTop: jQuery('.events-detail .wrap-reservation').position().top - 150,
                }, 1000, function(){
                    localStorage.eventsBook = "";
                });
            }, 800);
        }

        // SCROLL TO RESERVATION SECTION PAGE DINING
        // console.log(localStorage.diningBook );
        if(typeof localStorage.diningBook != "undefined" && localStorage.diningBook != ""){
            setTimeout(function(){
                jQuery('html, body').animate({
                    scrollTop: jQuery('.dining-detail .wrap-reservation-dining').position().top - 150,
                }, 1000, function(){
                    localStorage.diningBook = "";
                });
            }, 800);
        }
    });

    if(jQuery('.press-gallery').length <= 0)
    {
        Pace.on('start', function () {
            jQuery('.wrap-page-loader').show();
        });
    }
}

function navbar_fixed_on_scroll() {
    if (!jQuery('body').hasClass('navbar-show')) {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 78) {
                jQuery('#menunav').fadeIn(300);
            } else {
                jQuery('#menunav').fadeOut(300);
            }
        });
    }
}

function button_toggle() {
    jQuery("#open-menu, .hamburger-menu-header").click(function () {
        jQuery("#menu").fadeToggle();
        // jQuery("#menu").css("top", 0);
        return false;
    });
    jQuery(".close-menu, .popupgallery-close-container, .spa-booking-popup-container-close").click(function () {
        jQuery("#menu").fadeOut();
        jQuery("#book").fadeOut();
        jQuery(".container-popup-gallery").fadeOut(300);
        jQuery(".spa-booking-popup").fadeOut(300);
        jQuery(".offers-booking-popup").fadeOut(300);
        jQuery('.popup-reservation-dining').fadeOut(300);
        jQuery('.wrap-popup-wedding').fadeOut(300);
        return false;
    });
    jQuery("#open-book").click(function () {
        jQuery("#book").fadeIn(300, function(){
            datepicker_popup();
        });
        return false;
    });
    jQuery("#close-book").click(function () {
        jQuery("#book").fadeOut();
    });

    jQuery(".book-now-list").click(function () {
        jQuery("#book").fadeIn(300, function(){
            datepicker_popup();
        });
        return false;
    });

    jQuery('.book-now-dining').click(function () {
        jQuery('.popup-reservation-dining').fadeToggle();
    });

    jQuery(".book-now-hero-detail").click(function () {
        jQuery("#book").fadeIn(300, function(){
            datepicker_popup();
        });
        return false;
    });

    jQuery('.wrap-header-menu-text-switch-des').click(function () {
        return false;
    });
}

function scroll_down_top() {
    jQuery('.scroll-down').click(function () {
        var height_header = jQuery('header').height();
        var height_navbar = jQuery('nav#menunav').height();
        var height_half_navbar = height_navbar / 2;
        // console.log();
        jQuery('html, body').animate({
            scrollTop: height_header - height_half_navbar,
        }, 1000);
    });

    jQuery('.scroll-top').click(function () {
        jQuery('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
}


function show_popup_gallery() {
    jQuery('.button-show-popup-gallery').click(function () {
        jQuery(".container-popup-gallery").fadeIn(300);
        return false;
    });
}

function scroll_hero() {
    // LIST HERO BUTTON SCROLL
    jQuery('header .wrap-list-product-link ul li').click(function () {
        var target = jQuery(this).data("filter");
        var height_navbar = jQuery('nav.menu-header').height();
        jQuery('html, body').animate({
            scrollTop: jQuery(target).offset().top - height_navbar*2
        }, 1000);
    });


    jQuery('.scroll-hero').click(function () {
        var target = jQuery(this).attr("data-scroll");
        jQuery('html, body').animate({
            scrollTop: jQuery('#scroll-' + target).offset().top - 120
        }, 1000);

        jQuery('.dotted').removeClass('active');
        jQuery('.dotted-' + target).addClass('active');
    });


    if (jQuery('.wrap-footer').length > 0) {
        jQuery(window).scroll(function () {
            var h = jQuery(this).scrollTop();
            var height_hero = jQuery('.hero').outerHeight();
            var footer_position = jQuery('.wrap-footer').position().top;
            var height_footer = jQuery('.wrap-footer').height();

            if (h >= height_hero) {
                if (h >= (footer_position - height_footer)) {
                    jQuery('.container-dotted-scroll-page').fadeOut(300);
                } else {
                    jQuery('.container-dotted-scroll-page').fadeIn(300);
                }
            } else {
                jQuery('.container-dotted-scroll-page').fadeOut(300);
            }
        });

    }

    jQuery('.dotted').click(function () {
        var target = jQuery(this).attr("data-scroll");
        jQuery('html, body').animate({
            scrollTop: jQuery('#scroll-' + target).offset().top - 120
        }, 1000);

        jQuery('.dotted').removeClass('active');
        jQuery('.dotted-' + target).addClass('active');
    });
}

function slider_home_destination() {
    var asset_url = jQuery('[name=asset_url]').val();


    var w = window.innerWidth;

    if (w > 800) {
        // SLDIER SECTION SPECIAL OFFERS
        if (jQuery('.wrap-list-promotion .inner .list-item').length > 0) {
            var type = jQuery('.wrap-list-promotion .inner').attr("data-type");
            if(type=="spa")
            {
                jQuery('.wrap-list-promotion .inner').slick({
                    lazyLoad: 'ondemand',
                    variableWidth: false,
                    slidesToScroll: 1,
                    centerMode: false,
                    slidesToShow: 3,
                    infinite: true,
                    speed: 500,
                    appendArrows: jQuery(".wrap-list-promotion").find(".prev-next"),
                    prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
                    nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">'
                });
            }
            else
            {
                jQuery('.wrap-list-promotion .inner').slick({
                    lazyLoad: 'ondemand',
                    variableWidth: true,
                    slidesToScroll: 1,
                    centerMode: false,
                    slidesToShow: 2,
                    infinite: true,
                    speed: 500,
                    appendArrows: jQuery(".wrap-list-promotion").find(".prev-next"),
                    prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
                    nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
                });
            }

            // AFTER CHANGE
            jQuery('.wrap-list-promotion .inner').on('afterChange', function (event, slick, direction) {
                lazy_images();
            });
        }
    }


    // SLIDER EVENTS
    if (jQuery('.container-slider-wedding .inner .item').length > 0) {
        jQuery('.container-slider-wedding .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            appendArrows: jQuery(".container-slider-wedding").find(".prev-next"),
            prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
            nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
        });

        // AFTER CHANGE
        jQuery('.container-slider-wedding .inner').on('afterChange', function (event, slick, direction) {
            lazy_images();
        });
    }

    // SLIDER SECTION ACCOMMODATION
    if (jQuery('.container-list-accomodation-image .inner .item').length > 0) {
        jQuery('.container-list-accomodation-image .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: true,
            slidesToScroll: 1,
            centerMode: true,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            appendArrows: jQuery(".prev-next-dest.desktop"),
            prevArrow: '<img class="prev" src="' + asset_url + 'images/black-arrow-left.svg">',
            nextArrow: '<img class="next" src="' + asset_url + 'images/black-arrow-right.svg">',
        });

        // AFTER CHANGE
        jQuery('.container-list-accomodation-image .inner').on('afterChange', function (event, slick, direction) {
            lazy_images();
            update_text_accommodation_list_home();
        });
    }
    
    
    if(w <= 600){
        var width_dest_acco = jQuery('.wrap-list-accomodation .wrap-desc-list-acco.desktop').width();
        jQuery('.wrap-list-accomodation .container-list-accomodation-image .slick-slide > div').css('width', width_dest_acco+"px");
        // console.log(width_dest_acco);
    }

    
    var width_dest_new = jQuery('.wrap-experience-list .container-list-experience').width();
    if (jQuery('.container-list-accomodation-image-mobile .inner .item').length > 0) {
        jQuery('.container-list-accomodation-image-mobile .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: true,
            slidesToScroll: 1,
            centerMode: true,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            appendArrows: jQuery(".prev-next-dest.mobile"),
            prevArrow: '<img class="prev" src="' + asset_url + 'images/black-arrow-left.svg">',
            nextArrow: '<img class="next" src="' + asset_url + 'images/black-arrow-right.svg">',
        });

        jQuery('.container-list-accomodation-image-mobile .inner').on('afterChange', function (event, slick, direction) {
            lazy_images();
        });
    }
    jQuery('.wrap-list-accomodation .container-list-accomodation-image-mobile .slick-slide > div').css('width', width_dest_new+"px");
    

    // SLIDER TRIPADVISOR REVIEW
    if (jQuery('.con-advisor-list-testimoni .inner .item').length > 1) {
        jQuery('.con-advisor-list-testimoni .inner').slick({
            lazyLoad: 'ondemand',
            variableWidth: false,
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            appendArrows: jQuery(".con-advisor-list-testimoni .prev-next"),
            prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
            nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
        });
    }
}

function update_text_accommodation_list_home() {
    var title = jQuery('.container-list-accomodation-image .slick-track').find('.slick-active .item').data("title");
    var subtitle = jQuery('.container-list-accomodation-image .slick-track').find('.slick-active .item').data("subtitle");
    var desc = jQuery('.container-list-accomodation-image .slick-track').find('.slick-active .item').data("desc");
    var link = jQuery('.container-list-accomodation-image .slick-track').find('.slick-active .item').data("link");

    jQuery('.wrap-list-accomodation .wrap-desc-list-acco.desktop .text-title-accomodation-list').fadeOut(100, function () {
        jQuery(this).text(title).fadeIn(200);
    });

    jQuery('.wrap-list-accomodation .wrap-desc-list-acco.desktop .text-sub-title-accomodation-list').fadeOut(100, function () {
        jQuery(this).text(subtitle).fadeIn(200);
    });

    jQuery('.wrap-list-accomodation .wrap-desc-list-acco.desktop .text-description-accomodation-list').fadeOut(100, function () {
        jQuery(this).text(desc).fadeIn(200);
    });

    jQuery('.wrap-list-accomodation .wrap-desc-list-acco.desktop .contaner-explore-accomodation-list a').fadeOut(100, function () {
        jQuery(this).attr("href", link).fadeIn(200);
    });
}

function slider_hero_detail() {
    var asset_url = jQuery('[name=asset_url]').val();
    if (jQuery('.wrap-slide-hero .inner .slide-bg-hero').length > 0) {
        jQuery('.wrap-slide-hero .inner').slick({
            lazyLoad: 'ondemand',
            slidesToScroll: 1,
            centerMode: false,
            slidesToShow: 1,
            infinite: true,
            speed: 500,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            touchThreshold: 100,
            appendArrows: jQuery(".arrow-next-prev"),
            prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
            nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
        });
    }
}

function slider_page_list() {

    var asset_url = jQuery('[name=asset_url]').val();
    jQuery('.wrap-list-product .container-slide-product').each(function () {
        if (jQuery(this).find('.inner .item').length > 0) {
            jQuery(this).find('.inner').slick({
                lazyLoad: 'ondemand',
                variableWidth: false,
                slidesToScroll: 1,
                centerMode: false,
                slidesToShow: 1,
                infinite: true,
                speed: 500,
                appendArrows: jQuery(this).find(".prev-next"),
                prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
                nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
            });

            jQuery('.wrap-list-product .container-slide-product .inner').on('afterChange', function (event, slick, direction) {
                lazy_images();
            });
        }
    })

    // jQuery('.wrap-list-product').each(function(){
    //     var position = jQuery(this).position().top;
    //     var wrap_this = jQuery(this);

    //     var ij = 1;
    //     var iScrollPos = 0;
    //     var test = 1;
    //     var dj = localStorage.posIj;
    //     jQuery(window).on('scroll', function () {
    //         var st = jQuery(this).scrollTop();
    //         var iCurScrollPos = $(this).scrollTop();
    //         if (iCurScrollPos > iScrollPos) {
    //             if(st >= (position - 300)){
    //                 test = ij;
    //                 ij++;
    //                 jQuery(wrap_this).find('.container-slide-product .inner .item').css({
    //                     'background-position': '0% 0%, center calc(50% + ' + ij + 'px)'
    //                 });
    //                 localStorage.posIj = ij;
    //                 // console.log(ij);

    //             }
    //             dj = 0;
    //         } else {
    //             ij=0;
    //             console.log(dj);
    //             if(st >= (position - 100)){
    //                 dj++;
    //                 jQuery(wrap_this).find('.container-slide-product .inner .item').css({
    //                     'background-position': '0% 0%, center calc(50% + ' + dj + 'px)'
    //                 });
    //             }
    //         }

    //         iScrollPos = iCurScrollPos;

    //         console.log(this.scrollHeight);

    //         if (st + jQuery(this).innerHeight() >= this.scrollHeight) {
    //             console.log('end reached');
    //           } else if (st <= 0) {
    //             console.log('Top reached');
    //           }
    //     });
    // })
}


function scroll_bullet() {
    if (jQuery('.section-nav').length > 0) {
        var lsid;
        var menu = jQuery('.section-nav');
        var wheig = menu.outerHeight() / 2;
        var item = menu.find('a');
        var sitem = item.map(function () {
            var idx = jQuery(this).attr('href');
            var itm = jQuery(idx);
            if (itm.length) {
                return itm;
            }
        });

        jQuery(window).on('scroll', function () {
            var height_header = jQuery('header').height();
            var y = jQuery(this).scrollTop();

            var ftop = jQuery(this).scrollTop() + wheig;
            var cur = sitem.map(function () {
                if (jQuery(this).offset().top < ftop) {
                    return this;
                }
            });

            cur = cur[cur.length - 1];

            var id = (cur && cur.length ? cur[0].id : '');

            if (lsid !== id) {
                item.removeClass('active');
                item.filter('[href="#' + id + '"]').addClass('active');
            }

            if (y > height_header + 100) {
                jQuery('.section-nav').fadeIn(300);
            } else {
                jQuery('.section-nav').fadeOut(300);
            }

        });

        jQuery('.section-nav li a').on('click', function () {
            var sect = jQuery(this).attr('href');

            if (jQuery(sect).length > 0) {
                jQuery('html, body').stop().animate({
                    scrollTop: jQuery(sect).offset().top
                }, 1000);
            }

            return false;
        });
    }
}


function menu_category_choose() {
    if (jQuery('.wrap-menu-dining').length > 0) {
        var category = "";
        jQuery('.container-category-menu ul li').each(function () {
            if (jQuery(this).hasClass('active')) {
                category = jQuery(this).attr('data-filter');
            }
        });

        jQuery('.wrap-list-menu .wrap-cat-list-menu#' + category).show();

        jQuery('.container-category-menu ul li').click(function () {
            var filter = jQuery(this).attr("data-filter");
            jQuery('.container-category-menu ul li').removeClass('active');
            jQuery(this).addClass('active');

            jQuery('.wrap-list-menu .wrap-cat-list-menu').fadeOut(300);
            setTimeout(function () {
                jQuery('.wrap-list-menu .wrap-cat-list-menu#' + filter).fadeIn(500);
            }, 500);

            // menu_dining_choose();
            menu_type_choose();

        });
    }
}


function menu_type_choose() {
    if (jQuery('.wrap-menu-dining').length > 0) {
        var category = "";
        jQuery('.container-category-menu ul li').each(function () {
            if (jQuery(this).hasClass('active')) {
                category = jQuery(this).attr('data-filter');
            }
        });

        jQuery('#' + category + ' .container-menu-type ul li').each(function () {
            if (jQuery(this).hasClass('active')) {
                var filter_type = jQuery(this).attr('data-filter');
                jQuery('.dining-detail .wrap-menu-dining .wrap-list-menu .container-list-menu .list-item-menu.' + filter_type).show();
            }
        });

        jQuery('#' + category + ' .container-menu-type ul li').click(function () {
            var filter_type = jQuery(this).attr('data-filter');
            jQuery('#' + category + ' .container-menu-type ul li').removeClass('active');
            jQuery(this).addClass('active');

            jQuery('#' + category + ' .container-list-menu .list-item-menu').fadeOut(300);
            setTimeout(function () {
                jQuery('#' + category + ' .container-list-menu .list-item-menu.' + filter_type).fadeIn(300);
            }, 500);
        });
    }
}


function about_page() {
    var asset_url = jQuery('[name=asset_url]').val();
    jQuery('.container-about-destination .container-destination-list').each(function () {
        if (jQuery(this).find('.inner img').length > 0) {
            jQuery(this).find('.inner').slick({
                lazyLoad: 'ondemand',
                variableWidth: true,
                slidesToScroll: 1,
                centerMode: false,
                slidesToShow: 1,
                infinite: true,
                speed: 500,
                appendArrows: jQuery(this).find(".prev-next"),
                prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
                nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
            });

            jQuery('.container-about-destination .container-destination-list .inner').on('afterChange', function (event, slick, direction) {
                lazy_images();
            });
        }
    })
}


function slider_popup_gallery_product() {
    var asset_url = jQuery('[name=asset_url]').val();
    if (jQuery('.wrap-slide-popup .popup-slider .inner img').length > 0) {

        jQuery('.wrap-slide-popup .popup-slider .inner').slick({
            // lazyLoad: 'ondemand',
            variableWidth: true,
            slidesToScroll: 1,
            centerMode: true,
            cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
            slidesToShow: 1,
            infinite: true,
            dots: true,
            speed: 500,
            appendDots: jQuery(".dots-popup"),
            appendArrows: jQuery(".prev-next-popup"),
            prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
            nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
        });

        // BEFORE CHANGE
        jQuery('.wrap-slide-popup .popup-slider .inner').on('beforeChange', function (event, slick, direction, next_slide) {
            jQuery('.wrap-slide-popup .popup-slider .inner .slick-slide').addClass('slick-active');
        });
    }
    var tot_width_dots = 0;
    jQuery('.dots-popup ul.slick-dots li').each(function(){
        var  width_dots = jQuery(this).width();
        tot_width_dots+=width_dots+10;
    });
    var width_dots = tot_width_dots;
    var width_arrow = tot_width_dots+50;
    jQuery('.dots-popup').css("width", width_dots+"px");
    jQuery('.prev-next-popup').css("width", width_arrow+"px");
}


function config_popup() {
    jQuery('.close-popup span').click(function () {
        jQuery('.booking-popup').fadeOut(300, function(){
            jQuery(this).find(".form-value.choose-dest").removeClass("disabled");
            jQuery(this).find(".form-value.choose-dest [name=rooms]").val("");
        });
        jQuery('.spa-booking-popup').fadeOut(300);
        jQuery('.offers-booking-popup').fadeOut(300);
        jQuery('.wrap-booking-popup-dining').fadeOut(300);
        jQuery('.wrap-popup-wedding').fadeOut(300);
        jQuery('.menu-popup').fadeOut(300);
        // console.log("aa");
        jQuery('.popup-dining-list').fadeOut(300);
        // jQuery('.menu-popup').css('top', '-150%');
    });

    jQuery('.wrap-slide-popup .close-popup span').click(function(){
        jQuery('.wrap-slide-popup').fadeOut(300, function(){
            jQuery('.wrap-slide-popup .popup-slider .inner').slick("unslick");
        });
    })

    jQuery('.wrap-slide-hero .button-slider-hero .zoom-slider').click(function () {
        jQuery('.wrap-slide-popup').fadeIn(300);
        slider_popup_gallery_product();
    });

    jQuery('.header .hamburger-menu-header').click(function () {
        jQuery('.menu-popup').fadeIn(300);
        // console.log("ok");
        // jQuery('.menu-popup').css("top", 0);
    });
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT LOAD IMAGE MENGGUNAKAN LAZY JS
| -------------------------------------------------------------------------------------------------------------------------
*/
function lazy_images() {
    jQuery('.lazy').Lazy({
        placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7...",
        afterLoad: function(element) {
            jQuery('footer.wrap-footer').addClass('bg_cover_footer');
            // jQuery('div.lazy').addClass("lazy_load_hide");
        },
    });
}



function instagram_feed() {
    if (jQuery('.wrap-what-happening .wrap-instagram-feed .grid').length > 0) {
        var $grid = jQuery('.wrap-what-happening .wrap-instagram-feed .grid').packery({
            itemSelector: '.grid-item',
            percentPosition: true,
            columnWidth: '.grid-sizer'
        });

        Pace.on('done', function () {
            jQuery('.lazy-instagram').Lazy({
                afterLoad: function(element) {
                    $grid.packery();
                },
                placeholder: "data:image/gif;base64,R0lGODlhEALAPQAPzl5uLr9Nrl8e7..."
            });
        });
    }
}


function slier_other_product() {
    var asset_url = jQuery('[name=asset_url]').val();
    var w = window.innerWidth;

    if (w > 800) {
        // SLDIER SECTION SPECIAL OFFERS
        if (jQuery('.container-slide-other .inner .list-item').length > 0) {
            jQuery('.container-slide-other .inner').slick({
                lazyLoad: 'ondemand',
                variableWidth: true,
                slidesToScroll: 1,
                centerMode: false,
                slidesToShow: 2,
                infinite: true,
                speed: 500,
                cssEase: 'cubic-bezier(0.7, 0, 0.3, 1)',
                touchThreshold: 100,
                appendArrows: jQuery(".prev-next"),
                prevArrow: '<img class="prev" src="' + asset_url + 'images/arrow2.svg">',
                nextArrow: '<img class="next" src="' + asset_url + 'images/arrow1.svg">',
            });

            jQuery('.container-slide-other .inner').on('afterChange', function (event, slick, direction) {
                lazy_images();
            });
        }

        if (jQuery('.container-slide-other .inner .list-item').length < 3) {
            jQuery('.container-slide-other .container-button-nav-slide').hide();
        }
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE DATE RANGE PICKER
| -------------------------------------------------------------------------------------------------------------------------
*/
function initialize_datepicker() {
    var w = window.innerWidth;
    var h = window.innerHeight;
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();

    var output = (('' + month).length < 2 ? '0' : '') + month + '/' +
        (('' + day).length < 2 ? '0' : '') + day + d.getFullYear() + '/';

    if (jQuery('.date-range-picker').length > 0) {
        jQuery('.date-range-picker').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            },
            minDate: output,
            drops: 'up'
        });

        jQuery('.wrap-form-booking-hero input.date-range-picker').on('apply.daterangepicker', function (ev, picker) {
            jQuery(this).val(picker.startDate.format('DD MMM') + ' - ' + picker.endDate.format('DD MMM YYYY'));

            jQuery('.wrap-form-booking-hero [name=fromday]').val(picker.startDate.format('D'));
            jQuery('.wrap-form-booking-hero [name=frommonth]').val(picker.startDate.format('M'));
            jQuery('.wrap-form-booking-hero [name=fromyear]').val(picker.startDate.format('YYYY'));

            jQuery('.wrap-form-booking-hero [name=today]').val(picker.endDate.format('D'));
            jQuery('.wrap-form-booking-hero [name=tomonth]').val(picker.endDate.format('M'));
            jQuery('.wrap-form-booking-hero [name=toyear]').val(picker.endDate.format('YYYY'));
        });

        jQuery('date-range-picker').on('cancel.daterangepicker', function (ev, picker) {
            jQuery(this).val('');
        });

    }


    // DATEPICKER
    jQuery('.datepicker-ui').datepicker({
        dateFormat: 'dd MM yy',
        minDate: 0,
    });
}

function config_menu() {
    jQuery('ul li a.dropdown').click(function () {
        return false;
    });
}


function set_time_weather_header() {
    // SET TIME NOW
    var interval = setInterval(function () {
        var momentNow = moment();
        jQuery('.wrap-header-menu-text-time').html(momentNow.format('hh:mm A') + ' (+08)');
        // console.log(interval);
    }, 100);


    // SET WEATHER NOW
    // jQuery.ajax('http://api.wunderground.com/api/c6dc8e785d943109/conditions/q/AZ/Chandler.json', {
    //     dataType: 'jsonp',
    //     success: function (json) {
    //         jQuery('.wrap-header-menu-text-cloud').text(json.current_observation.temp_c + '°C');
    //     }
    // });
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK CONFIGURASI ACCOMMODATION BOOKING POPUP
| -------------------------------------------------------------------------------------------------------------------------
*/
function accommodation_booking_popup(){
    jQuery('[name=accommodation_popup_booking]').submit(function(){
        var e = 0;
        jQuery(this).find('.required').removeClass('error');

        jQuery(this).find('.required').each(function () {
            var val = jQuery(this).val();
            if (val == '') {
                jQuery(this).addClass('error');
                e++;
            }
        });

        console.log(e);
        if(e == 0){
            return true;
        }
        return false;
    });
}


function accommodation_page() {
    accommodation_booking_popup();
    w = window.innerWidth;
    if (w <= 1080) {
        jQuery(".accommodation .book-now-hero, .home-destination .book-now-hero").click(function () {
            jQuery("#book").fadeIn(300, function(){
                datepicker_popup();
            });
            return false;
        });
    }
    
    jQuery(".accommodation .wrap-list-product .text-book-now").click(function () {
        jQuery("#book").fadeIn(300, function(){
            datepicker_popup();
        });
        return false;
    });


    // DATEPICKER
    jQuery('.arrival').datepicker({
        dateFormat: 'dd MM yy',
        minDate: 0,
        onSelect: function (date) {
            var date2 = jQuery('.arrival').datepicker('getDate');
            date2.setDate(date2.getDate() + 1);

            // SET ARRIVAL DATE
            var arrival_date = jQuery('.arrival').datepicker('getDate');
            var dd = arrival_date.getDate();
            var mm = arrival_date.getMonth() + 1;
            var yy = arrival_date.getFullYear();
            arrival_date = yy + '-' + mm + '-' + dd;
            jQuery('.booking-popup .wrap-booking-form [name=arrival]').val(arrival_date);
            jQuery('.wrap-booking-acco-form [name=arrival]').val(arrival_date);

            // SET DEPARTURE DATE
            var departure_date = date2;
            var dd = departure_date.getDate();
            var mm = departure_date.getMonth() + 1;
            var yy = departure_date.getFullYear();
            departure_date = yy + '-' + mm + '-' + dd;
            jQuery('.booking-popup .wrap-booking-form [name=departure]').val(departure_date);
            jQuery('.wrap-booking-acco-form [name=departure]').val(departure_date);

            jQuery('.departure').datepicker('setDate', date2);
            jQuery('.departure').datepicker('option', 'minDate', date2);
        }
    });

    jQuery('.departure').datepicker({
        dateFormat: 'dd MM yy',
        minDate: 0,
        onSelect: function () {
            var dt1 = jQuery('.arrival').datepicker('getDate');
            var dt2 = jQuery('.departure').datepicker('getDate');

            if (dt2 < dt1) {
                var minDate = jQuery('.departure').datepicker('option', 'minDate');
                jQuery('.departure').datepicker('setDate', minDate);
            }

            var departure_date = jQuery('.departure').datepicker('getDate');
            var dd = departure_date.getDate();
            var mm = departure_date.getMonth() + 1;
            var yy = departure_date.getFullYear();
            departure_date = yy + '-' + mm + '-' + dd;
            jQuery('.booking-popup .wrap-booking-form [name=departure]').val(departure_date);
            jQuery('.wrap-booking-acco-form [name=departure]').val(departure_date);
        }
    });


    // CONFIG BOOKING POPUP
    jQuery('.booking-popup.default .form-value').click(function () {
        if(!jQuery(this).hasClass("disabled")){
            jQuery('.booking-popup .form-value .select-option').slideUp(300);
            var data_form = jQuery(this).data("form");
            if (jQuery(this).find('.select-option').is(":visible")) {
                jQuery(this).find('.select-option').slideUp(300);
            } else {
                jQuery(this).find('.select-option').slideDown(300);
            }
    
            jQuery(this).find('ul li').click(function () {
                var val = jQuery(this).data("value");
                jQuery('.booking-popup .form-value' + data_form).find('input').val(val);
    
                if (data_form == ".destination") {
                    var hotels = jQuery(this).data("hotelsname");
                    var properties = jQuery(this).data("properties");
    
                    jQuery('.booking-popup .wrap-booking-form [name=Clusternames]').val(hotels);
                    jQuery('.booking-popup .wrap-booking-form [name=Hotelnames]').val(hotels);
                    jQuery('.booking-popup .wrap-booking-form [name=properties]').val(properties);
                }
            });
        }
    });

    // IF CANCEL BOOKING BUTTON CLICK
    jQuery('.text-cancel-booking').click(function(){
        jQuery('[name=accommodation_popup_booking] input').val("");
    });
}


function datepicker_popup(){
    jQuery('.arrival-popup').datepicker({
        dateFormat: 'dd MM yy',
        minDate: 0,
        onSelect: function (date) {
            var date2 = jQuery('.arrival-popup').datepicker('getDate');
            date2.setDate(date2.getDate() + 1);

            // SET ARRIVAL DATE
            var arrival_date = jQuery('.arrival-popup').datepicker('getDate');
            var dd = arrival_date.getDate();
            var mm = arrival_date.getMonth() + 1;
            var yy = arrival_date.getFullYear();
            arrival_date = yy + '-' + mm + '-' + dd;
            jQuery('.booking-popup .wrap-booking-form [name=arrival]').val(arrival_date);

            // SET DEPARTURE DATE
            var departure_date = date2;
            var dd = departure_date.getDate();
            var mm = departure_date.getMonth() + 1;
            var yy = departure_date.getFullYear();
            departure_date = yy + '-' + mm + '-' + dd;
            jQuery('.booking-popup .wrap-booking-form [name=departure]').val(departure_date);

            jQuery('.departure-popup').datepicker('setDate', date2);
            jQuery('.departure-popup').datepicker('option', 'minDate', date2);
        }
    });

    jQuery('.departure-popup').datepicker({
        dateFormat: 'dd MM yy',
        minDate: 0,
        onSelect: function () {
            var dt1 = jQuery('.arrival-popup').datepicker('getDate');
            var dt2 = jQuery('.departure').datepicker('getDate');

            if (dt2 < dt1) {
                var minDate = jQuery('.departure-popup').datepicker('option', 'minDate');
                jQuery('.departure-popup').datepicker('setDate', minDate);
            }

            var departure_date = jQuery('.departure-popup').datepicker('getDate');
            var dd = departure_date.getDate();
            var mm = departure_date.getMonth() + 1;
            var yy = departure_date.getFullYear();
            departure_date = yy + '-' + mm + '-' + dd;
            jQuery('.booking-popup .wrap-booking-form [name=departure]').val(departure_date);
        }
    });
}

function getDate(element) {
    var date;
    try {
        date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
        date = null;
    }

    return date;
}


function contact_us_page() {
    var w = window.innerWidth;
    var scroll;
    if (jQuery('.contact-us').length > 0) {
        scroll = jQuery('.contact-us .wrap-content-contact-us .container-contact-us h1').offset().top;
    }

    if (w <= 500) {
        jQuery('.contact-us .wrap-content-contact-us .container-contact-us h1').click(function () {
            jQuery('html, body').animate({
                scrollTop: scroll - 50,
            }, 1000);
        });
    }


    // SEND INQUIRY
    jQuery('.contact-us [name=contact_form]').submit(function (e) {
        var text_send = jQuery(this).find('button.send-contact').data('text-send');
        var text_loading = jQuery(this).find('button.send-contact').data('text-loading');

        change_text_button(text_loading, "send_contact");
        change_name_form("contact", "contact_form");

        jQuery(".form-response").removeClass("alert-danger");
        jQuery(".form-response").removeClass("alert-warning");
        jQuery(".form-response").removeClass("alert-success");

        var e = 0;
        jQuery('.require_contact').removeClass('error');

        jQuery('.require_contact').each(function () {
            var val = jQuery(this).val();
            if (val == '') {
                jQuery(this).addClass('error');
                change_text_button(text_send, "send_contact");
                change_name_form("contact_form", "contact_form");
                e++;
            }
        });

        var email = jQuery("[name=email]").val()
        if (email != '') {
            if (!valid_email(email)) {
                jQuery("[name=email]").addClass('error');
                change_text_button(text_send, "send_contact");
                change_name_form("contact_form", "contact_form");
                e++;
            }
        }

        // e = 0;
        if (e == 0) {
            var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
            var dt = new Object();
            dt.name = jQuery("[name=fullname]").val();
            dt.country = jQuery("[name=country]").val();
            dt.email = jQuery("[name=email]").val();
            dt.phone = jQuery("[name=phone]").val();
            dt.find_us = jQuery("[name=find_us]").val();
            dt.recaptcha = jQuery("#g-recaptcha-response").val();
            dt.destination = jQuery("[name=destination]").val();
            dt.pkey = "contact_message";

            jQuery.post(thaUrl, dt, function (response) {
                // console.log(response);
                // return false;
                var res = jQuery.parseJSON(response);
                var status = res.status;
                var msg_response = res.message;

                if (status == 'success') {
                    jQuery(this).find('input[type=text]').val("");
                    jQuery(this).find('textarea').val("");
                    grecaptcha.reset();
                    change_text_button(text_send, "send_contact");
                    change_name_form("contact_form", "contact_form");
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-success");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                } else if (status == "warning") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-warning");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    change_text_button(text_send, "send_contact");
                    change_name_form("contact_form", "contact_form");
                } else if (status == "error") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-danger");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    change_text_button(text_send, "send_contact");
                    change_name_form("contact_form", "contact_form");
                    grecaptcha.reset();
                }
            });

            // console.log(thaUrl);
        }

        // console.log("asdasd");
        return false;
    });
}

// FUNCTION UNTUK CHANGE BUTTON TEXT INQUIRY
function change_text_button(text_button, name_button) {
    jQuery('[name=' + name_button + ']').html(text_button);
}

// FUNCTION UNTUK CHANGE BUTTON NAME INQUIRY
function change_name_form(name, class_form) {
    jQuery('.' + class_form).attr("name", name);
}

// FUNCTION TO CHECK VALID EMAIL
function valid_email(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}


/*
| -----------------------------------------------------------------------------
| START SPA BOOKING CONFIG
| -----------------------------------------------------------------------------
*/
function show_booking_popup_spa() {
    var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
    var destinations = jQuery('[name=destination]').val();

    jQuery('.book-now-spa').click(function () {
        jQuery(".spa-booking-popup").fadeIn(300);
        return false;
    });


    // IF BUTTON BOOKING NOW IN SPA PAGE LIST AND PAGE DETAIL CLICK
    jQuery('.spa .wrap-list-product .text-book-now, .spa-detail .wrap-form-booking-hero .container-button-book-now, .book-now-treatment').click(function () {
        var key = jQuery(this).data("key");

        jQuery('.spa-booking-popup').fadeIn(300, function () {
            // initialize_selectize();
            var dt = new Object();
            dt.value = key;
            dt.destination = destinations;
            dt.no_of_person = jQuery('.spa-booking-popup select[name=no-of-person]').val();
            dt.pkey = "choose_spa";

            jQuery('.spa-booking-popup').find('.loader-booking').fadeIn(300);

            jQuery.post(thaUrl, dt, function (response) {
                var res = jQuery.parseJSON(response);
                var status = res.status;
                var product = res.key;
                var cat_id = res.cat_id;
                var treat_id = res.treat_id;
                var option = res.option;
                var option_treatment = res.option_treatment;
                var price = res.treatment_price;
                var tax = res.treatment_tax;
                var total = res.treatment_total;


                if (status == "success") {
                    jQuery('select[name=treatment_category]').html(option);
                    jQuery('select[name=treatment_list]').html(option_treatment);
                    jQuery('.spa-booking-popup [name=spa] option[value=' + product + ']').prop('selected', true);
                    if (cat_id != "") {
                        jQuery('.spa-booking-popup [name=treatment_category] option[value=' + cat_id + ']').prop('selected', true);
                    }
                    if (treat_id != "") {
                        jQuery('.spa-booking-popup [name=treatment_list] option[value=' + treat_id + ']').prop('selected', true);
                    }
                    jQuery('.spa-booking-popup-reservation-amount-price span').text(price);
                    jQuery('.spa-booking-popup-reservation-amount-tax span').text(tax);
                    jQuery('.spa-booking-popup-reservation-amount-total span').text(total);
                }

                jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
            });


        });
        return false;
    });
}

function spa_booking_config() {
    var w = window.innerWidth;
    if(w <= 1080){
        jQuery('.spa .wrap-form-booking-hero form').submit(function(){
            jQuery(".spa-booking-popup").fadeToggle();
            return false;
        });
    }

    var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
    var destinations = jQuery('[name=destination]').val();

    // IF SELECT SPA CHANGE
    jQuery('.spa-booking-popup select[name=spa]').change(function (e) {
        var val = jQuery(this).val();

        if (val != "") {
            jQuery(this).removeClass('error');
            jQuery('select[name=treatment_category]').removeClass('error');
            jQuery('select[name=treatment_list]').removeClass('error');
        }

        jQuery('.spa-booking-popup').find('.loader-booking').fadeIn(300);

        var dt = new Object();
        dt.value = val;
        dt.destination = destinations;
        dt.lang = jQuery('.spa-booking-popup [name=lang]').val();
        dt.check_lang = jQuery('.spa-booking-popup [name=check_lang]').val();
        dt.no_of_person = jQuery('.spa-booking-popup select[name=no-of-person]').val();
        dt.pkey = "change_spa";

        reset_spa_treatment_category_list();

        jQuery.post(thaUrl, dt, function (response) {
            var res = jQuery.parseJSON(response);
            var status = res.status;
            var option = res.option;
            var option_treatment = res.option_treatment;
            var price = res.treatment_price;
            var tax = res.treatment_tax;
            var total = res.treatment_total;

            if (status == "success") {
                jQuery('select[name=treatment_category]').html(option);
                jQuery('select[name=treatment_list]').html(option_treatment);
                jQuery('.spa-booking-popup-reservation-amount-price span').text(price);
                jQuery('.spa-booking-popup-reservation-amount-tax span').text(tax);
                jQuery('.spa-booking-popup-reservation-amount-total span').text(total);
            }

            jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);

        });
    });



    // IF SELECT TREATMENT CATEGORY CHANGE
    jQuery('.spa-booking-popup select[name=treatment_category]').change(function (e) {
        var val = jQuery(this).val();
        jQuery('.spa-booking-popup').find('.loader-booking').fadeIn(300);

        var dt = new Object();
        dt.value = val;
        dt.destination = destinations;
        dt.lang = jQuery('.spa-booking-popup [name=lang]').val();
        dt.check_lang = jQuery('.spa-booking-popup [name=check_lang]').val();
        dt.destination = destinations;
        dt.no_of_person = jQuery('.spa-booking-popup select[name=no-of-person]').val();
        dt.pkey = "change_treatment_category";

        jQuery.post(thaUrl, dt, function (response) {
            var res = jQuery.parseJSON(response);
            var status = res.status;
            var option = res.option_treatment;
            var price = res.treatment_price;
            var tax = res.treatment_tax;
            var total = res.treatment_total;

            if (status == "success") {
                jQuery('select[name=treatment_list]').html(option);
                jQuery('.spa-booking-popup-reservation-amount-price span').text(price);
                jQuery('.spa-booking-popup-reservation-amount-tax span').text(tax);
                jQuery('.spa-booking-popup-reservation-amount-total span').text(total);
            }

            jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
        });
    });

    // IF SELECT TREATMENT LIST CHANGE
    jQuery('.spa-booking-popup select[name=treatment_list]').change(function (e) {
        var value = jQuery(this).val();
        ajax_treatment_tax_price(value);
    });


    // IF NO OF PERSON CHANGE
    jQuery('.spa-booking-popup select[name=no-of-person]').change(function (e) {
        var value = jQuery('.spa-booking-popup select[name=treatment_list]').val();
        ajax_treatment_tax_price(value);
    });


    // IF FORM CLICK SUBMIT
    jQuery('[name=booking_popup_spa]').submit(function () {
        jQuery('.spa-booking-popup').find('.loader-booking').fadeIn(300);

        jQuery(".form-response").removeClass("alert-danger");
        jQuery(".form-response").removeClass("alert-warning");
        jQuery(".form-response").removeClass("alert-success");

        var e = 0;
        jQuery('.requred-booking').removeClass('error');

        jQuery('.requred-booking').each(function () {
            var val = jQuery(this).val();
            if (val == '') {
                jQuery(this).addClass('error');
                // change_text_button(text_send, "send_contact");
                // change_name_form("contact_form", "contact_form");
                e++;
                jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
            }
        });

        var email = jQuery("[name=email]").val()
        if (email != '') {
            if (!valid_email(email)) {
                jQuery("[name=email]").addClass('error');
                // change_text_button(text_send, "send_contact");
                // change_name_form("contact_form", "contact_form");
                e++;
                jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
            }
        }

        if (e == 0) {
            var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
            var dt = new Object();
            dt.spa = jQuery("[name=spa]").val();
            dt.treatment_category = jQuery("[name=treatment_category]").val();
            dt.treatment_list = jQuery("[name=treatment_list]").val();
            dt.no_of_person = jQuery("[name=no-of-person]").val();
            dt.promotion_code = jQuery("[name=promotion_code]").val();
            dt.hours = jQuery("[name=hours]").val();
            dt.minutes = jQuery("[name=minutes]").val();
            dt.where_staying = jQuery("[name=where_staying]").val();
            dt.special_request = jQuery("[name=special_request]").val();
            dt.name = jQuery("[name=fullname]").val();
            dt.nationality = jQuery("[name=nationality]").val();
            dt.email = jQuery("[name=email]").val();
            dt.phone = jQuery("[name=phone]").val();
            dt.contact_you = jQuery("[name=contact_you]").val();
            dt.understand = 0;
            dt.destination = jQuery("[name=destination]").val();

            if (jQuery("[name=understand]").is(":checked")) {
                dt.understand = 1;
            }
            dt.recaptcha = jQuery("#g-recaptcha-response").val();
            dt.pkey = "spa_booking_inquiry";

            jQuery.post(thaUrl, dt, function (response) {

                var res = jQuery.parseJSON(response);
                var status = res.status;
                var msg_response = res.message;

                if (status == 'success') {
                    // change_text_button(text_send, "send_contact");
                    // change_name_form("contact_form", "contact_form");
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-success");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    reset_spa_booking_form();
                } else if (status == "warning") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-warning");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    // change_text_button(text_send, "send_contact");
                    // change_name_form("contact_form", "contact_form");
                    grecaptcha.reset();
                    jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
                } else if (status == "error") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-danger");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    // change_text_button(text_send, "send_contact");
                    // change_name_form("contact_form", "contact_form");
                    reset_spa_booking_form();
                }

            });
        }

        return false;
    });
}

function reset_spa_booking_form() {
    jQuery('.spa-booking-popup [name=spa] option[value=""]').prop('selected', true);
    jQuery('.spa-booking-popup [name=hours] option[value=""]').prop('selected', true);
    jQuery('.spa-booking-popup [name=minutes] option[value=""]').prop('selected', true);
    jQuery('.spa-booking-popup [name=nationality] option[value=""]').prop('selected', true);
    jQuery('.spa-booking-popup [name=promotion_code]').val('');
    jQuery('.spa-booking-popup [name=where_staying]').val('');
    jQuery('.spa-booking-popup [name=special_request]').val('');
    jQuery('.spa-booking-popup [name=fullname]').val('');
    jQuery('.spa-booking-popup [name=email]').val('');
    jQuery('.spa-booking-popup [name=phone]').val('');
    grecaptcha.reset();
    jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
}

function ajax_treatment_tax_price(val) {

    jQuery('.spa-booking-popup').find('.loader-booking').fadeIn(300);

    var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
    var destinations = jQuery('[name=destination]').val();
    var dt = new Object();
    dt.value = val;
    dt.destination = destinations;
    dt.no_of_person = jQuery('.spa-booking-popup select[name=no-of-person]').val();
    dt.pkey = "change_treatment_list";

    jQuery.post(thaUrl, dt, function (response) {
        var res = jQuery.parseJSON(response);
        var status = res.status;
        var price = res.treatment_price;
        var tax = res.treatment_tax;
        var total = res.treatment_total;

        if (status == "success") {
            jQuery('.spa-booking-popup-reservation-amount-price span').text(price);
            jQuery('.spa-booking-popup-reservation-amount-tax span').text(tax);
            jQuery('.spa-booking-popup-reservation-amount-total span').text(total);
        }

        jQuery('.spa-booking-popup').find('.loader-booking').fadeOut(300);
    });
}

function reset_spa_treatment_category_list() {
    var value = 0;
    jQuery('select[name=treatment_category]').html('<option value="">Select Treatment Category*</option>');
    jQuery('select[name=treatment_list]').html('<option value="">Select Treatment List*</option>');
    jQuery('.spa-booking-popup-reservation-amount-price span').text(value);
    jQuery('.spa-booking-popup-reservation-amount-tax span').text(value);
    jQuery('.spa-booking-popup-reservation-amount-total span').text(value);
}
/*
| -----------------------------------------------------------------------------
| END SPA BOOKING CONFIG
| -----------------------------------------------------------------------------
*/


/*
| -----------------------------------------------------------------------------
| INITIALIZE RESDIARY
| -----------------------------------------------------------------------------
*/
function res_diary_config() {
    if (jQuery("#rd-widget-frame").length > 0) {
        jQuery(function () {
            var widgetFrame = jQuery("#rd-widget-frame");
            widgetFrame.load("https://booking.resdiary.com/widget/Standard/BreezeSamaya/2370?includeJquery=false");
        })
    }
}

/*
| -----------------------------------------------------------------------------
| SUBSCRIBE MAILCHIMP CONFIG
| -----------------------------------------------------------------------------
*/
function config_subscribe_mailchimp_form() {
    jQuery('footer [name=subscribe_mailchimp]').submit(function () {
        var e = 0;
        jQuery(this).find('.required').removeClass('error');

        jQuery("footer .form-response").removeClass("alert-danger");
        jQuery("footer .form-response").removeClass("alert-warning");
        jQuery("footer .form-response").removeClass("alert-success");

        jQuery(this).find('.required').each(function () {
            var val = jQuery(this).val();
            if (val == '') {
                jQuery(this).addClass('error');
                // change_text_button(text_send, "send_contact");
                // change_name_form("contact_form", "contact_form");
                e++;
            }
        });

        var email = jQuery("[name=email]").val()
        if (email != '') {
            if (!valid_email(email)) {
                jQuery("[name=email]").addClass('error');
                // change_text_button(text_send, "send_contact");
                // change_name_form("contact_form", "contact_form");
                e++;
            }
        }

        if (e == 0) {
            var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
            var dt = new Object();
            dt.name = jQuery("[name=fullname]").val();
            dt.email = jQuery("[name=email]").val();
            dt.destination = jQuery("[name=destination]").val();
            dt.pkey = "mailchimp_subscribe";

            jQuery.post(thaUrl, dt, function (response) {
                // console.log(response);
                var res = jQuery.parseJSON(response);
                var status = res.status;
                var msg_response = res.message;


                if (status == 'success') {
                    // change_text_button(text_send, "send_contact");
                    // change_name_form("contact_form", "contact_form");
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-success");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                } else if (status == "warning") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-warning");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    // change_text_button(text_send, "send_contact");
                    // change_name_form("contact_form", "contact_form");
                } else if (status == "error") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-danger");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    // change_text_button(text_send, "send_contact");
                    // change_name_form("contact_form", "contact_form");
                    // grecaptcha.reset();
                }
            });

            // console.log(thaUrl);
        }

        return false;
    });
}


/*
| -----------------------------------------------------------------------------
| START DINING BOOKING CONFIG
| -----------------------------------------------------------------------------
*/
function dining_booking_config(){
    // CLICK BOOKING DINING POPUP

    jQuery('.dining .wrap-list-product .text-book-now').click(function () {
        localStorage.setItem("diningBook", 1);
    });

    jQuery('.dining-detail .wrap-form-booking-hero .container-button-book-now-link').click(function(){
        jQuery('html, body').animate({
            scrollTop: jQuery('.dining-detail .wrap-reservation-dining').position().top - 150,
        }, 1000);
        return false;
    });
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK FOOTER MENU
| -----------------------------------------------------------------------------
*/
function footer_menu(){
    jQuery('footer ul li.press a.dropdown').click(function(){
        jQuery('footer ul li ul').slideUp(300);
        if (!jQuery('footer ul li.press ul').is(":visible")) {
            jQuery('footer ul li.press ul').slideDown(300);
        }
        return false;
    });

    jQuery('footer ul li.what-to-do a.dropdown').click(function(){
        jQuery('footer ul li ul').slideUp(300);
        if (!jQuery('footer ul li.what-to-do ul').is(":visible")) {
            jQuery('footer ul li.what-to-do ul').slideDown(300);
        }
        return false;
    });
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK POPUP MENU
| -----------------------------------------------------------------------------
*/
function popup_menu(){
    jQuery('.menu-popup ul li.press a.dropdown').click(function(){
        jQuery('.menu-popup ul li ul').slideUp(300);
        if (!jQuery('.menu-popup ul li.press ul').is(":visible")) {
            jQuery('.menu-popup ul li.press ul').slideDown(300);
        }
        return false;
    });

    jQuery('.menu-popup ul li.what-to-do a.dropdown').click(function(){
        jQuery('.menu-popup ul li ul').slideUp(300);
        if (!jQuery('.menu-popup ul li.what-to-do ul').is(":visible")) {
            jQuery('.menu-popup ul li.what-to-do ul').slideDown(300);
        }
        return false;
    });
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK PAGE EVENTS AND EVENTS DETAIL
| -----------------------------------------------------------------------------
*/
function events_page(){
    var w = window.innerWidth;
    // CLICK BOOKING EVENT POPUP
    jQuery('.events .wrap-list-product .text-book-now').click(function () {
        localStorage.eventsBook = 1;
    });

    // CLICK BUTTON BOOK NOW IN HERO DETAIL PAGE
    jQuery('.events-detail .wrap-form-booking-hero .container-button-book-now').click(function(){
        jQuery('html, body').animate({
            scrollTop: jQuery('.events-detail .wrap-reservation').position().top - 150,
        }, 1000);
    });

    if(w <= 1080){
        jQuery('.events .wrap-form-booking-hero form').submit(function(){
            jQuery("#book").fadeIn(300, function(){
                datepicker_popup();
            });
            return false;
        });
    }
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE SELECTIZE
| -----------------------------------------------------------------------------
*/
function initialize_selectize(){
    jQuery('.selectize').selectize();
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE SCROLLBAR
| -----------------------------------------------------------------------------
*/
function initialize_scrollbar(){
    jQuery('.scrollbar-macosx').scrollbar();
}

function ucwords_js(str){
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
    return str;
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK OFFERS BOOKING POPUP
| -----------------------------------------------------------------------------
*/
var thaUrl = jQuery('input[name=site_url]').val() + 'ajax-function/';
var destinations = jQuery('[name=destination]').val();

function offers_booking_popup(){
    // BUTTON BOOK NOW CLICK
    jQuery('.special-offers .book-now-item, .special-offers-detail .book-now-hero, .special-offers-detail .book-now').click(function(){
        var val = jQuery(this).data("title");
        var form_type = jQuery(this).data("form_type");

        if(form_type == 2){
            jQuery('.offers-booking-popup').fadeIn(300, function(){
                jQuery(this).find('[name=offers-booking-popup] .subject input').val(val);
            });
        }else if(form_type == 1){
            jQuery('.booking-popup').fadeIn(300, function(){
                jQuery(this).find(".form-value.choose-dest").addClass("disabled");
                jQuery(this).find(".form-value.choose-dest [name=rooms]").val(ucwords_js(destinations));

                
                var hotels = jQuery(this).find(".booking-data-"+destinations).data("hotelsname");
                var properties = jQuery(this).find(".booking-data-"+destinations).data("properties");

                jQuery('.booking-popup .wrap-booking-form [name=Clusternames]').val(hotels);
                jQuery('.booking-popup .wrap-booking-form [name=Hotelnames]').val(hotels);
                jQuery('.booking-popup .wrap-booking-form [name=properties]').val(properties);
                // jQuery(this).find('[name=offers-booking-popup] .subject input').val(val);
            });
        }
    });

    jQuery('[name=offers-booking-popup]').find('.required').each(function () {
        jQuery(this).keydown(function(){
            jQuery(this).removeClass('error');
        });
    });

    jQuery('[name=offers-booking-popup]').submit(function(){
        var form = this;
        var e = 0;

        jQuery(form).find('.loader-booking').fadeIn(300);
        jQuery(form).find('.required').removeClass('error');

        jQuery(form).find('.required').each(function () {
            var val = jQuery(this).val();
            if (val == '') {
                jQuery(this).addClass('error');
                jQuery(form).find('.loader-booking').fadeOut(300);
                e++;
            }
        });

        var email = jQuery("[name=email]").val()
        if (email != '') {
            if (!valid_email(email)) {
                jQuery("[name=email]").addClass('error');
                e++;
                jQuery(form).find('.loader-booking').fadeOut(300);
            }
        }

        var nationality = jQuery("[name=nationality]").val()
        if (nationality == '') {
            jQuery(".selectize").addClass('error');
            jQuery(form).find('.loader-booking').fadeOut(300);
            e++;
        }

        if(e == 0){
            var dt = new Object();
            dt.subject = jQuery("[name=subject]").val();
            dt.fullname = jQuery("[name=fullname]").val();
            dt.nationality = jQuery("[name=nationality]").val();
            dt.phone = jQuery("[name=phone]").val();
            dt.email = jQuery("[name=email]").val();
            dt.message = jQuery("[name=message]").val();
            dt.destination = jQuery("[name=destination]").val();
            dt.pkey = "offers_booking_inquiry";

            jQuery.post(thaUrl, dt, function (response) {
                var res = jQuery.parseJSON(response);
                var status = res.status;
                var msg_response = res.message;

                if (status == 'success') {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-success");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    reset_offers_booking_form();
                } else if (status == "warning") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-warning");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                } else if (status == "error") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-danger");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    reset_offers_booking_form();
                }
                
                jQuery(form).find('.loader-booking').fadeOut(300);
            });
        }

        return false;
    });
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK RESET OFFERS BOOKING FORM
| -----------------------------------------------------------------------------
*/
function reset_offers_booking_form() {
    jQuery('[name=offers-booking-popup] input').val('');
    jQuery('[name=offers-booking-popup] textarea').val('');
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN CURRENT WEATHER
| -----------------------------------------------------------------------------
*/
function get_weather_yahoo(){
    var dest = jQuery('[name=destination]').val();

    if(dest == "seminyak"){
        jQuery.ajax("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20%3D%2056013402&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys", {
            dataType: 'jsonp', 
            success: function(json){
                // console.log(json);
                var temp = json.query.results.channel.item.condition.temp;
                var celcius = Math.round(convert_f_to_c(temp));
                var code = json.query.results.channel.item.condition.code;
                var text = json.query.results.channel.item.condition.text;

                jQuery('.wrap-header-menu-text-cloud').text(celcius + '°C');
                jQuery('.wrap-header-menu-text-cloud').addClass(convertToSlug(text));
            }
        });
    }else{
        jQuery.ajax("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20%3D%201048666&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys", {
            dataType: 'jsonp', 
            success: function(json){
                // console.log(json);
                var temp = json.query.results.channel.item.condition.temp;
                var celcius = Math.round(convert_f_to_c(temp));
                var code = json.query.results.channel.item.condition.code;
                var text = json.query.results.channel.item.condition.text;

                jQuery('.wrap-header-menu-text-cloud').text(celcius + '°C');
                jQuery('.wrap-header-menu-text-cloud').addClass(convertToSlug(text));
            }
        });
    }
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CONVERT STRING MENJADI SLUG
| -----------------------------------------------------------------------------
*/
function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MELAKUKAN PROSES PERHITUNGAN FAHRENHEIT TO CELCIUS
| -----------------------------------------------------------------------------
*/
function convert_f_to_c(f){
    c = (5/9) * (f - 32);
    return c;
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK WEDDINGS BOOKING FORM
| -----------------------------------------------------------------------------
*/
function weddings_booking_form(){
    // BUTTON BOOK NOW CLICK
    if(w <= 1080){
        jQuery('.weddings .wrap-form-booking-hero form').submit(function(){
            jQuery("#book").fadeIn(300, function(){
                datepicker_popup();
            });
            return false;
        });
    }

    jQuery('.weddings .wrap-list-product .text-book-now, .weddings-detail form .book-now-hero').click(function(){
        jQuery('.wrap-popup-wedding').fadeIn(300);
        return false;
    });

    // special request click
    jQuery('input[name=special_request]').click(function(){
        var value = jQuery(this).val();
        if(value == "other"){
            jQuery("input[name=other_text]").prop('disabled', false);
        }else{
            jQuery("input[name=other_text]").prop('disabled', true);
            // jQuery("input[name=other_text]").val("");
        }
    });

    jQuery('[name=booking_popup_wedding]').submit(function(){
        var form = this;
        var e = 0;

        jQuery(form).find('.loader-booking').fadeIn(300);
        jQuery(form).find('.required').removeClass('error');

        jQuery(form).find('.required').each(function () {
            var val = jQuery(this).val();
            if (val == '') {

                jQuery(this).addClass('error');
                jQuery(form).find('.loader-booking').fadeOut(300, function(){
                    jQuery('.wrap-popup-wedding').animate({
                        scrollTop: 0
                    });
                });
                e++;
            }
        });

        var email = jQuery("[name=email]").val()
        if (email != '') {
            if (!valid_email(email)) {
                jQuery("[name=email]").addClass('error');
                e++;
                jQuery(form).find('.loader-booking').fadeOut(300);
            }
        }

        var nationality = jQuery("[name=nationality]").val()
        if (nationality == '') {
            jQuery(".selectize").addClass('error');
            jQuery(form).find('.loader-booking').fadeOut(300);
            e++;
        }

        // e = 0;

        if(e == 0){
            var occasion_type = [];
            jQuery("[name=occasion_type]:checked").each(function(){
                occasion_type.push(jQuery(this).val());
            });

            var food_type = [];
            jQuery("[name=food_type]:checked").each(function(){
                food_type.push(jQuery(this).val());
            });

            var special_request = [];
            jQuery("[name=special_request]:checked").each(function(){
                special_request.push(jQuery(this).val());
            });

            var dt = new Object();
            dt.email = jQuery("[name=email]").val();
            dt.fullname = jQuery("[name=fullname]").val();
            dt.nationality = jQuery("[name=nationality]").val();
            dt.spouse_fullname = jQuery("[name=spouse_fullname]").val();
            dt.spouse_nationality = jQuery("[name=spouse_nationality]").val();
            dt.phone = jQuery("[name=phone]").val();
            dt.wedding_date = jQuery("[name=wedding_date]").val();
            dt.number_guest = jQuery("[name=number_guest]").val();
            dt.occasion_type = occasion_type;
            dt.theme_color = jQuery("[name=theme_color]").val();
            dt.food_type = food_type;
            dt.food_dinner = jQuery("[name=food_dinner]:checked").val();
            dt.reception_venue = jQuery("[name=reception_venue]:checked").val();
            dt.special_request = special_request;
            dt.other_text = jQuery("[name=other_text]").val();
            dt.another_question = jQuery("[name=another_question]").val();
            dt.recaptcha = jQuery("#g-recaptcha-response").val();
            dt.pkey = "weddings_booking_inquiry";
            dt.destination = jQuery("[name=destination]").val();


            jQuery.post(thaUrl, dt, function (response) {
                // console.log(response);
                // return false;
                var res = jQuery.parseJSON(response);
                var status = res.status;
                var msg_response = res.message;

                if (status == 'success') {
                    jQuery(".form-response").addClass("alert-success");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    reset_weddings_booking_form();
                } else if (status == "warning") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-warning");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                } else if (status == "error") {
                    jQuery(".form-response").html(msg_response);
                    jQuery(".form-response").addClass("alert-danger");
                    jQuery(".form-response").fadeIn(300).delay(3000).fadeOut('slow');
                    reset_weddings_booking_form();
                }
                
                jQuery(form).find('.loader-booking').fadeOut(300, function(){
                    jQuery(".form-response").html(msg_response);
                    jQuery('.wrap-popup-wedding').animate({
                        scrollTop: 0
                    });
                });
            });
        }

        return false;
    });
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK RESET WEDDINGS BOOKING FORM
| -----------------------------------------------------------------------------
*/
function reset_weddings_booking_form() {
    jQuery('[name=booking_popup_wedding] input').val('');
    // jQuery('[name=offers-booking-popup] textarea').val('');
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CONFIG ONLY NUMERIC
| -----------------------------------------------------------------------------
*/
function only_numeric(){
    jQuery(".is_numeric").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
}



/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CONFIG PRESS GALLERY
| -----------------------------------------------------------------------------
*/
function press_gallery_config(){
    // jQuery('.filter-gallery .click-filter').click(function(){
    //     jQuery('.filter-gallery .click-filter').find('.select-option').slideUp(300);
    //     if(jQuery(this).find('.select-option').is(":visible")){
    //         jQuery(this).find('.select-option').slideUp(300);
    //     }else{
    //         jQuery(this).find('.select-option').slideDown(300);
    //     }
    // });

    // jQuery('.con-filter-gallery-type').find('ul li').click(function(){
    //     load_filter_gallery(this);
    // });

    jQuery('.con-filter-gallery-category').find('ul li').click(function(){
        load_filter_gallery(this);
    });
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CONFIG LOAD FILTER GALLERY
| -----------------------------------------------------------------------------
*/
function load_filter_gallery(this_data){
    var url_page = jQuery("[name=page_url]").val();
    var filter = jQuery(this_data).data("filter");
    var text = jQuery(this_data).data("text");
    var filter_key = jQuery(this_data).data("key");

    if(filter_key=="type"){
        jQuery('.filter-gallery .con-filter-gallery-type').find('span').text(text);
        jQuery('.filter-gallery .con-filter-gallery-type').find('input[name="search"]').val(filter);

        var filter_other = jQuery('.con-filter-gallery-category').find("input[name=search]").val();
        var new_url = url_page+'?type='+filter+'&category='+filter_other;
    }else{
        jQuery('.filter-gallery .con-filter-gallery-category').find('span').text(text);
        jQuery('.filter-gallery .con-filter-gallery-category').find('input[name="search"]').val(filter);

        var filter_other = jQuery('.con-filter-gallery-type').find("input[name=search]").val();
        var new_url = url_page+'?type='+filter_other+'&category='+filter;
    }

    jQuery('#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .box').removeClass('show');
    setTimeout(function(){

        jQuery('.press-gallery .page-loader-gallery').fadeIn(300, function(){

            Pace.on('start', function () {
                jQuery('.pace').hide();
            });

            jQuery('.press-gallery #wrap-press-gallery-con').load(new_url + ' #container-press-gallery', function(){
    
                press_gallery_infinite_scroll(filter);
                lazy_images();

                Pace.on('start', function () {
                    jQuery('.pace').hide();
                });
    
                Pace.options = {
                    restartOnRequestAfter: false
                }
            });
        });

    }, 300);
}

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK CONFIG PRESS GALLERY INFINITE SCROLL
| -----------------------------------------------------------------------------
*/
function press_gallery_infinite_scroll(filter=''){
  
    if (jQuery('.container-offers .wrap-press-gallery').length > 0) {
        $grid_gallery = jQuery('#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery');
        $grid_gallery.packery({
            itemSelector: '#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .box',
            percentPosition: true,
            gutter: '#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .gutter-sizer'
        });
        
        $grid_gallery.packery('layout');

        // jQuery('#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .box').hide();

        if(filter==''){
            Pace.on('done', function () {
                jQuery('.press-gallery .page-loader-gallery').fadeOut(300, function(){
                    jQuery('#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .box').addClass('show');
                });
            });
        }else{
            jQuery('.press-gallery .page-loader-gallery').fadeOut(300, function(){
                jQuery('#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .box').addClass('show');
            });
        }


        if(jQuery('.pagination').length > 0){
            $grid_gallery.infiniteScroll({
                path: '.pagination .pagination__next',
                hideNav: '.pagination',
                history: false,
                status: '.page-load-status',
            });
    
            $grid_gallery.on('load.infiniteScroll', function (event, response, path) {
                // console.log(response);
                Pace.on('start', function () {
                    jQuery('.pace').hide();
                });

                var $items = $(response).find('.box');
                $grid_gallery.append($items);
                $grid_gallery.packery('appended', $items);
                jQuery('#wrap-press-gallery-con #container-press-gallery .wrap-press-gallery .box').addClass('show');
                fancybox_initialize();
                lazy_images();
            });
        }
    }
    
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE FANCYBOX
| -------------------------------------------------------------------------------------------------------------------------
*/
function fancybox_initialize(){
    if(jQuery("a.fancy-group").length > 0){
        jQuery("a.fancy-group").fancybox({
            'opacity': true,
            'overlayShow': false,
            'transitionIn': 'elastic',
            'transitionOut': 'none',
            'showNavArrows': true,
            // 'infobar' : false,
            // 'buttons' : false,
        });
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE MENU DINING POPUP
| -------------------------------------------------------------------------------------------------------------------------
*/
function menu_dining_popup(){
    var w = window.innerWidth;
    // jQuery('.dining-detail .wrap-menu-list-dining .list-menu-dining').click(function(){
    //     var filter = jQuery(this).data("filter");
    //     var title = jQuery(this).data("title");
    //     jQuery('.popup-dining-list').fadeIn(300);
    //     jQuery('.popup-dining-list .container-menu-list-dining .wrap-list-dining .menu-list').hide();
    //     jQuery('.popup-dining-list .container-menu-list-dining .wrap-list-dining #'+filter).fadeIn(300);
    //     jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul li').removeClass("active");
    //     jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul li.'+filter+'-filter').addClass("active");
    //     jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list span.choose_cat_mobile').text(title);
    // });

    jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul li').click(function(){
        jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul li').removeClass("active");
        jQuery(this).addClass("active");
        var filter = jQuery(this).data("filter");
        var title = jQuery(this).data("title");
        jQuery('.popup-dining-list .container-menu-list-dining .wrap-list-dining .menu-list').fadeOut(300, function(){
            if(w < 600){
                jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul').slideUp(300);
            }
            jQuery(window).resize(function(){
                var w = window.innerWidth;
                if(w < 600){
                    jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul').slideUp(300);
                }
            })
            jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list span.choose_cat_mobile').text(title);
            setTimeout(function(){
                jQuery('.popup-dining-list .container-menu-list-dining .wrap-list-dining #'+filter).fadeIn(300);
            }, 300);
        });
    });


    jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list .choose-type').click(function(){
        if(jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul').is(":visible")){
            jQuery(this).find('img').removeClass("rotate");
            jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul').slideUp(300);
        }else{
            jQuery(this).find('img').addClass("rotate");
            jQuery('.popup-dining-list .container-menu-list-dining .category-dining-list ul').slideDown(300, function(){
                
            });
        }
    });
}