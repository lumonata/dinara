/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK INITIALIZE GOOGLE MAPS DI CONTACT PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
var map;
var value = 0;

function initialize() {
    // CONFIG MAPS LANDING PAGE
    if (jQuery('#contact-information').length > 0) {
        var mapDiv = document.getElementById('map-ci');
        // var latitude = jQuery('[name=latitude]').val();
        // var longitude = jQuery('[name=longitude]').val();

        var latitude = -8.517912;
        var longitude = 115.259358;

        var latLng = new google.maps.LatLng(latitude, longitude);
        var w = window.innerWidth;

        zoom = 16;
        if(w <= 1440 && w >= 601){
            zoom = 15;
        }else if(w <= 600){
            zoom = 13;
        }

        map = new google.maps.Map(mapDiv, {
            center: latLng,
            zoom: zoom,
            disableDefaultUI: true,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            mapTypeControlOptions: {
                style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            navigationControl: true,
            navigationControlOptions: {
                style: google.maps.NavigationControlStyle.SMALL,
                position: google.maps.ControlPosition.LEFT_CENTER
            },
            zoomControl: true,
            scaleControl: true,
            scaleControlOptions: {
                position: google.maps.ControlPosition.BOTTOM_LEFT
            },
            scrollwheel: false,
            draggable: true,
            styles: [
                      {
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#f5f5f5"
                          }
                        ]
                      },
                      {
                        "elementType": "labels.icon",
                        "stylers": [
                          {
                            "visibility": "off"
                          }
                        ]
                      },
                      {
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#616161"
                          }
                        ]
                      },
                      {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                          {
                            "color": "#f5f5f5"
                          }
                        ]
                      },
                      {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#bdbdbd"
                          }
                        ]
                      },
                      {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#eeeeee"
                          }
                        ]
                      },
                      {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#757575"
                          }
                        ]
                      },
                      {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#e5e5e5"
                          }
                        ]
                      },
                      {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#9e9e9e"
                          }
                        ]
                      },
                      {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#ffffff"
                          }
                        ]
                      },
                      {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#757575"
                          }
                        ]
                      },
                      {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#dadada"
                          }
                        ]
                      },
                      {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#616161"
                          }
                        ]
                      },
                      {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#9e9e9e"
                          }
                        ]
                      },
                      {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#e5e5e5"
                          }
                        ]
                      },
                      {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#eeeeee"
                          }
                        ]
                      },
                      {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                          {
                            "color": "#c9c9c9"
                          }
                        ]
                      },
                      {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                          {
                            "color": "#9e9e9e"
                          }
                        ]
                      }
                    ]
        });


        var locationss = jQuery('[name=json_dest]').val();
        var locations = JSON.parse(locationss);

        var marker, i;

        var infowindow = new google.maps.InfoWindow({
            maxWidth: 200
        });

        var currentMark;
        var currenti;
        var gmark = [];
        for (i = 0; i < locations.length; i++) {
            var map_icon = jQuery('[name=map_icon]').val();
            var icon = {
                url: map_icon,
                scaledSize: new google.maps.Size(33, 42),
                // labelOrigin: new google.maps.Point(20, 18)
            }
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i]['latitude'], locations[i]['longitude']),
                map: map,
                title: locations[i]['title'],
                icon: icon
            });
            gmark.push(marker);


            google.maps.event.addListener(marker, 'click', (function (marker, i) {

                return function () {

                    // CHANGE MARKER OTHER
                    if(i==0){
                        marker_other = gmark[1];
                        var other_i = 1;
                    }else{
                        marker_other = gmark[i-1];
                        var other_i = i-1;
                    }

                    $('.each-con-info').removeClass('actv');
                    $('#'+i).addClass('actv');

                    var map_icon = jQuery('[name=map_icon_fade]').val();
                    var icon = {
                        url: map_icon,
                        scaledSize: new google.maps.Size(33, 42),
                        // labelOrigin: new google.maps.Point(20, 18)
                    }

                    var map_icon_other = jQuery('[name=map_icon]').val();
                    var icon_other = {
                        url: map_icon_other,
                        scaledSize: new google.maps.Size(33, 42),
                        // labelOrigin: new google.maps.Point(20, 18)
                    }
                    marker.setIcon(icon);
                    marker_other.setIcon(icon_other);

                    // CHANGE MARKER ON CLICK
                    // marker.setIcon(jQuery('[name=map_icon_info]').val());
                    // var contentString = '<div id="gmaps-content-landing">' +
                    //     '<div class="image-map">' +
                    //     '<img src="' + locations[i]['corporate_image'] + '" />' +
                    //     '</div>' +
                    //     '<div class="container-desc">' +
                    //     '<div class="title">' +
                    //     '<h2>' + locations[i]['title'] + '</h2>' +
                    //     '</div>' +
                    //     '<div class="address">' +
                    //     '<p>' + locations[i]['address'] + '</p>' +
                    //     '</div>' +
                    //     '<div class="telphone">' +
                    //     '<span>t. </span>' + locations[i]['phone'] +
                    //     '</div>' +
                    //     '<div class="email">' +
                    //     '<span>e. </span>' + locations[i]['email'] +
                    //     '</div>' +
                    //     '<div class="direction-explore clearfix">' +
                    //     '<a href="' + locations[i]['link_explore'] + '" class="explore">' + locations[i]['text_explore'] + '</a>' +
                    //     '<a target="_blank" href="' + locations[i]['link_direction'] + '" class="direction">' + locations[i]['text_direction'] + '</a>' +
                    //     '</div>' +
                    //     '</div>' +
                    //     '</div>';


                    // infowindow.setContent(contentString);
                    // infowindow.open(map, marker);
                    closeMarker = infowindow;
                    globalClose.push(closeMarker);

                    currentMark = this;
                    currenti = i;

                }
            })(marker, i));

        }

        google.maps.event.addListener(infowindow, 'closeclick', function () {
            var map_icon = jQuery('[name=map_icon_' + locations[currenti]['sef'] + ']').val();
            var icon = {
                url: map_icon,
                scaledSize: new google.maps.Size(219, 76),
                // labelOrigin: new google.maps.Point(20, 18)
            }
            currentMark.setIcon(icon); //removes the marker
        });
    }
}

var gmarkers = [];
var imarkers = [];
var globalClose = [];
//
//
// function set_info_window(map, locations) {
//
//     var marker, i;
//     var closeMarker;
//
//     for (i = 0; i < locations.length; i++) {
//         // var map_icon = jQuery('[name=map_icon_' + locations[i][3] + ']').val();
//
//         var marker = new google.maps.Marker({
//             position: {
//                 lat: parseFloat(locations[i][1]),
//                 lng: parseFloat(locations[i][2])
//             },
//             map: map,
//             zIndex: 1,
//             icon: {
//                 url: map_icon,
//             },
//         });
//
//         gmarkers.push(marker);
//         imarkers.push(i);
//
//
//     }
//
// }
