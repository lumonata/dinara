<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| Homepage Destination Content
| -------------------------------------------------------------------------------------------------------------------------
*/
function home_destination_content($post ,$lang='', $check_lang=false)
{
    global $db;
    $cek_url = cek_url();
    set_template( TEMPLATE_PATH . '/template/home_destination.html', 'h-template' );
    add_block('loop_review_list', 'lrlblock', 'h-template');
    add_block('loop_offers_list', 'loblock', 'h-template');
    add_block('loop_interestings_list', 'lillock', 'h-template');
    add_block('loop_accommodations_list', 'lablock', 'h-template');
    add_block('loop_experience_list', 'leblock', 'h-template');
    add_block('home-block', 'h-block', 'h-template');

    $destination_id = $post[0]['id'];
    $appname = 'dinara';

    // META DATA SETTING
    $meta_title       = get_meta_title($destination_id, 'destinations');
    $meta_title       = (empty($meta_title) ? 'Home - '.ucwords($cek_url[0]).' | '.trim(web_title()) : $meta_title);
    $meta_keywords    = get_meta_keywords($destination_id, 'destinations');
    $meta_description = get_meta_description($destination_id, 'destinations');
    $og_url           = '';
    $og_title         = '';
    $og_image         = $post[0]['background'];
    $og_site_name     = '';
    $og_description   = '';
    add_meta_data_value($meta_title, $meta_keywords, $meta_description, $og_url, $og_title, $og_image, $og_site_name, $og_description);

    add_variable( 'title_greeting', $post[0]['title_landing'] );
    add_variable( 'content_greeting', $post[0]['desc'] );
    add_variable( 'HTTP', HTTP );
    add_variable( 'SITE_URL', SITE_URL );
    add_variable( 'urip', get_uri_sef() );

    $title_hero    = title_hero_destination($post, $lang, $check_lang);
    $header_layout = header_layout($title_hero, $post[0]['background'], $lang, $check_lang);
// header_layout($title_hero, $bg_hero , $lang, $check_lang)
    // STRING TRANSLATIONS
    set_variable_string_translations($lang, $check_lang, '');
    $translations = array('Latest Promotion', 'Accommodation', 'Whats Happening At Samaya Seminyak', 'See Packages', 'Whats Happening At Samaya Ubud', 'Review', 'Reviews at Tripadvisor', 'Travellers Choice');
    $tr_string    = set_string_language($translations, $lang, $check_lang, '');

    $social_media_html = $post[0]['social_media']['social_media_default'];
    $site_url          = site_url();
    $description_dest  = $post[0]['desc'];
    $link_about        = HTTP.site_url().'/'.$appname.'/about-us';

    add_variable( 'appname_upper', strtoupper($appname) );
    add_variable( 'site_url', $site_url );
    add_variable( 'description_dest', $description_dest );
    add_variable( 'HTTP', HTTP );
    add_variable( 'link_about', $link_about );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
    add_variable( 'social_media_html', $social_media_html );

    // SECTION SPECIAL OFFERS
    $data_page_offer   = get_data_page('special-offers', $destination_id, $lang, $check_lang, '');
    $offer_title = $data_page_offer['title'];
    $offer_brief = $data_page_offer['brief'];
    add_variable( 'offer_title', $offer_title );
    add_variable( 'offer_brief', $offer_brief );

    $offers = get_post_list('special-offers', 6, '', $appname);
    if(count($offers) > 0)
    {
        foreach($offers as $d)
        {
            $id_offers        = $d['post_id'];
            $title_offers     = $d['post_title'];
            // $subtitle_offers  = $d['post_subtitle'];
            // $subtitle_offers  = (empty($subtitle_offers) ? $tr_string['latest_promotion'] : $subtitle_offers );
            $subtitle_offers  = $tr_string['latest_promotion'];
            $brief_offers     = $d['post_brief'];
            $sef_offers       = $d['post_sef'];
            $content_offers   = $d['post_content'];
            $post_link_offers = HTTP . site_url().'/'.$appname.'/special-offers/'.$sef_offers.'/';
            $image_offers     = get_featured_img( $id_offers, 'special-offers', false );
            $image_offers     = HTTP . site_url().$image_offers['medium'];

            if($check_lang)
            {
                $title_offers_lang    = get_additional_field($id_offers, 'title_'.$lang, 'special-offers');
                // $subtitle_offers_lang = get_additional_field($id_offers, 'subtitle_'.$lang, 'special-offers');
                $brief_offers_lang    = get_additional_field($id_offers, 'brief_'.$lang, 'special-offers');
                $content_offers_lang  = get_additional_field($id_offers, 'content_'.$lang, 'special-offers');

                $title_offers     = (empty($title_offers_lang) ? $title_offers:       $title_offers_lang);
                // $subtitle_offers  = (empty($subtitle_offers_lang) ? $subtitle_offers: $subtitle_offers_lang);
                $brief_offers     = (empty($brief_offers_lang) ? $brief_offers:       $brief_offers_lang);
                $content_offers   = (empty($content_offers_lang) ? $content_offers:   $content_offers_lang);
                $post_link_offers = HTTP . site_url().'/'.$lang.'/'.$appname.'/special-offers/'.$sef_offers.'/';
            }


            add_variable('title_offers', $title_offers);
            add_variable('subtitle_offers', $subtitle_offers);
            add_variable('content_offers', $content_offers);
            add_variable('brief_offers', strip_tags($brief_offers));
            add_variable('image_offers', $image_offers);
            add_variable('sef_offers', $sef_offers);
            add_variable('post_link_offers', $post_link_offers);

            parse_template('loop_offers_list', 'loblock', true);
        }

        $link_exlore_all_offers = HTTP . site_url().'/'.$appname.'/special-offers/';

        add_variable('link_exlore_all_offers', $link_exlore_all_offers);
    }


    // INTERESTING SPOT
    $interestings = get_post_list('what-to-do-around', 6, '', $appname);
    $hide_interest ='';
    if(count($interestings) > 0)
    {
      $i = 1;
      $result_acco = "";
      add_variable('toti', count($interestings));
      foreach($interestings as $d)
      {
        $id_interestings       = $d['post_id'];
        $title_interestings    = $d['post_title'];
        $subtitle_interestings = $d['post_subtitle'];
        $content_interestings  = strip_tags($d['post_content']);
        $image_interestings    = get_featured_img( $id_interestings, 'what-to-do-around', false );
        $image_interestings    = HTTP . site_url().$image_interestings['original'];

        if($check_lang)
        {
            $title_interestings_lang    = get_additional_field($id_interestings, 'title_'.$lang, 'what-to-do-around');
            $subtitle_interestings_lang = get_additional_field($id_interestings, 'subtitle_'.$lang, 'what-to-do-around');
            $content_interestings_lang  = get_additional_field($id_interestings, 'content_'.$lang, 'what-to-do-around');

            $title_interestings    = (empty($title_interestings_lang) ? $title_interestings:       $title_interestings_lang);
            $subtitle_interestings = (empty($subtitle_interestings_lang) ? $subtitle_interestings: $subtitle_interestings_lang);
            $content_interestings  = (empty($content_interestings_lang) ? $content_interestings:   $content_interestings_lang);
        }

        add_variable('title_interestings', $title_interestings);
        add_variable('subtitle_interestings', $subtitle_interestings);
        add_variable('content_interestings', string_fit($content_interestings,500));
        add_variable('image_interestings', $image_interestings);
        add_variable('numb', $i);

        if($i == 1)
        {
            $result_acco['title']    = $title_interestings;
            $result_acco['subtitle'] = $subtitle_interestings;
            $result_acco['content']  = $content_interestings;
        }

        parse_template('loop_interestings_list', 'lillock', true);

        $i++;
      }

      if(!empty($result_acco))
      {
          add_variable('title_inter', $result_acco['title']);
          add_variable('subtitle_inter', $result_acco['subtitle']);
          add_variable('content_inter', string_fit($result_acco['content'],500));
      }
    }else {
      $hide_interest = "display:none";
    }
    add_variable('hide_interest', $hide_interest);

    // SECTION INSTAGRAM
    // $access_token = get_meta_data( 'ig_token', 'static_setting' );
    // $photo_count  = 4;
    //
    // $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
    // $json_link.="access_token={$access_token}&count={$photo_count}";
    //
    // $json = file_get_contents($json_link);
    // $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);
    //
    // $iglist = "";
    //
    // foreach ($obj['data'] as $post) {
    //   add_variable('ig_link',$post['link']);
    //   add_variable('ig_pict',$post['images']['standard_resolution']['url']);
    //
    //   $iglist .='
    //             <div class="each-ig">
    //               <a href="'.$post['link'].'" target="_blank">
    //                  <img ex-src="'.$post['images']['standard_resolution']['url'].'">
    //               </a>
    //             </div>
    //             ';
    // }
    // add_variable('iglist', $iglist);

    // SECTION ACCOMODATION
    $accommodations = get_post_list('accommodation', 5, '', $appname);
    if(count($accommodations) > 0)
    {
        $i = 1;
        $result_acco = "";
        $link_viewall_acco     = HTTP . site_url().'/'.$appname.'/accommodation/';
        if ($check_lang) {
          $link_viewall_acco     = HTTP . site_url().'/'.$lang.'/'.$appname.'/accommodation/';
        }

        add_variable('tot', count($accommodations));
        add_variable('link_viewall_acco', $link_viewall_acco);

        foreach($accommodations as $d)
        {

            $id_accommodations       = $d['post_id'];
            $title_accommodations    = $d['post_title'];
            // $subtitle_accommodations = $d['post_subtitle'];
            // $subtitle_accommodations = (empty($subtitle_accommodations) ? $tr_string['accommodations']: $subtitle_accommodations);
            $subtitle_accommodations = $tr_string['accommodation'];
            $brief_accommodations    = strip_tags($d['post_brief']);
            $content_accommodations  = strip_tags($d['post_content']);
            $sef_accommodations      = $d['post_sef'];
            $image_accommodations    = get_featured_img( $id_accommodations, 'accommodation', false );
            $image_accommodations    = HTTP . site_url().$image_accommodations['medium'];
            $link_accommodations     = HTTP . site_url().'/'.$appname.'/accommodation/'.$sef_accommodations.'/';

            if($check_lang)
            {
                $title_accommodations_lang    = get_additional_field($id_accommodations, 'title_'.$lang, 'accommodation');
                $subtitle_accommodations_lang = get_additional_field($id_accommodations, 'subtitle_'.$lang, 'accommodation');
                $brief_accommodations_lang    = strip_tags(get_additional_field($id_accommodations, 'brief_'.$lang, 'accommodation'));
                $content_accommodations_lang  = get_additional_field($id_accommodations, 'content_'.$lang, 'accommodation');

                $title_accommodations    = (empty($title_accommodations_lang) ? $title_accommodations:       $title_accommodations_lang);
                $subtitle_accommodations = (empty($subtitle_accommodations_lang) ? $subtitle_accommodations: $subtitle_accommodations_lang);
                $brief_accommodations    = (empty($brief_accommodations_lang) ? $brief_accommodations:       $brief_accommodations_lang);
                $content_accommodations  = (empty($content_accommodations_lang) ? $content_accommodations:   $content_accommodations_lang);
                $link_accommodations     = HTTP . site_url().'/'.$lang.'/'.$appname.'/accommodation/'.$sef_accommodations.'/';
            }


            add_variable('title_accommodations', $title_accommodations);
            add_variable('subtitle_accommodations', $subtitle_accommodations);
            add_variable('content_accommodations', string_fit($content_accommodations,130));
            add_variable('brief_accommodations', $brief_accommodations);
            add_variable('image_accommodations', $image_accommodations);
            add_variable('sef_accommodations', $sef_accommodations);
            add_variable('link_accommodations', $link_accommodations);
            add_variable('numb', $i);

            if($i == 1)
            {
                $result_acco['title']    = $title_accommodations;
                $result_acco['subtitle'] = $subtitle_accommodations;
                $result_acco['brief']    = $brief_accommodations;
                $result_acco['content']  = $content_accommodations;
                $result_acco['link']     = $link_accommodations;
            }

            parse_template('loop_accommodations_list', 'lablock', true);

            $i++;
        }

        if(!empty($result_acco))
        {
            add_variable('title_acco', $result_acco['title']);
            add_variable('subtitle_acco', $result_acco['subtitle']);
            add_variable('content_acco', string_fit($result_acco['content'],130));
            add_variable('brief_acco', $result_acco['brief']);
            add_variable('link_acco', $result_acco['link']);
        }
    }


    $data_page_exp   = get_data_page('experience', $destination_id, $lang, $check_lang, '');
    $exp_title = $data_page_exp['title'];
    $exp_brief = $data_page_exp['brief'];
    add_variable( 'exp_title', $exp_title );
    add_variable( 'exp_brief', $exp_brief );

    $hide_exp= '';
    $experience = get_post_list('experience', 6, '', $appname);
    if(count($experience) > 0)
    {
        foreach($experience as $d)
        {
            $id_experience      = $d['post_id'];
            $title_experience   = $d['post_title'];
            $sef_experience     = $d['post_sef'];
            $brief_experience   = $d['post_brief'];
            $content_experience = $d['post_content'];
            $link_experience    = get_additional_field($id_experience,'link_experience','experience');

            if($check_lang)
            {
                $title_experience_lang   = get_additional_field($id_experience, 'title_'.$lang, 'experience');
                $brief_experience_lang   = get_additional_field($id_experience, 'brief_'.$lang, 'experience');
                $content_experience_lang = get_additional_field($id_experience, 'content_'.$lang, 'experience');
                $link_experience_lang    = get_additional_field($id_experience,'link_experience_'.$lang,'experience');

                $title_experience        = (empty($title_experience_lang) ? $title_experience:     $title_experience_lang);
                $brief_experience        = (empty($brief_experience_lang) ? $brief_experience:     $brief_experience_lang);
                $content_experience      = (empty($content_experience_lang) ? $content_experience: $content_experience_lang);
                $link_experience         = (empty($link_experience_lang) ? $link_experience:         $link_experience_lang);
            }


            $link_experience    = str_replace("http://", HTTP, $link_experience);
            $link_experience    = str_replace("https://", HTTP, $link_experience);

            $image_experience   = get_featured_img( $id_experience, 'experience', false );
            $image_experience   = HTTP . site_url().$image_experience['medium'];

            add_variable('title_experience', $title_experience);
            add_variable('content_experience', string_fit($content_experience,99));
            add_variable('brief_experience', $brief_experience);
            add_variable('image_experience', $image_experience);
            add_variable('sef_experience', $sef_experience);
            add_variable('link_experience', $link_experience);

            parse_template('loop_experience_list', 'leblock', true);
        }
    }else {
      $hide_exp = 'display:none';
    }

    add_variable('hide_exp', $hide_exp);


    // SECTION TRIP ADVISOR
    // NOTE: GET CATEGORY
    $q  = $db->prepare_query( "SELECT * FROM lumonata_rules
                               WHERE lrule =%s AND lgroup =%s AND lparent=%d
                               ORDER BY lorder ASC","categories","tripadvisor-review","0" );
    $r  = $db->do_query( $q );
    $cat_list = "";

    while( $d = $db->fetch_array( $r ) ){
      $cat_list .='<a href="'.HTTP.SITE_URL.'/'.$appname.'/each-review?cat='.$d['lsef'].'">'.$d['lname'].'</a>';
    }

    add_variable( 'cat_review', $cat_list );

    $reviews = get_post_list('tripadvisor-review', 6, '', $appname, 1);
    // print_r($reviews);
    $tripad_review_html = "";
    if(!empty($reviews))
    {
        foreach($reviews as $tr)
        {
            $review_id         = $tr['post_id'];
            $review_title      = $tr['post_title'];
            $review_desc       = $tr['post_content'];
            $traveller_name    = get_additional_field( $review_id, 'traveller_name', 'tripadvisor-review' );
            $traveller_country = get_additional_field( $review_id, 'country', 'tripadvisor-review' );
            $traveller_link = get_additional_field( $review_id, 'traveller_link', 'tripadvisor-review' );

            if($check_lang)
            {
                $review_title_lang    = get_additional_field($review_id, 'title_'.$lang, 'tripadvisor-review');
                $review_content_lang    = get_additional_field($review_id, 'content_'.$lang, 'tripadvisor-review');
                $traveller_name_lang    = get_additional_field($review_id, 'traveller_name_'.$lang, 'tripadvisor-review');
                $country_lang    = get_additional_field($review_id, 'country_'.$lang, 'tripadvisor-review');

                $review_title    = (empty($review_title_lang) ? $review_title:       $review_title_lang);
                $review_desc    = (empty($review_content_lang) ? $review_desc:       $review_content_lang);
                $traveller_name    = (empty($traveller_name_lang) ? $traveller_name:       $traveller_name_lang);
                $traveller_country    = (empty($country_lang) ? $traveller_country:       $country_lang);
            }

            $rate = get_additional_field( $review_id, 'rates', 'tripadvisor-review' );
            $rdate = get_additional_field( $review_id, 'rdate', 'tripadvisor-review' );

            $rate = (empty($rate) ? 'b5': $rate);

            $newDate = '';
            if (!empty($rdate)) {
              $newDate = date("M d, Y", strtotime($rdate));
            }
            add_variable('rdate', $newDate);


            add_variable('title_reviews', $review_title);
            add_variable('thelinks', $traveller_link);
            add_variable('rates', $rate);
            add_variable('desc_reviews', strip_tags($review_desc));
            add_variable('name_reviews', $traveller_name);
            add_variable('rule', get_rule($review_id));

            $rule_id = get_rule_id_category(get_post_category($review_id)[0]['sef'],'tripadvisor-review');
            $c_ = 'link_ota_monkeyforest';

            if ($appname == 'nyuh-kuning') { $c_ = 'link_ota_nyuhkuning'; }
            $lk = get_additional_rule_field( $rule_id, $c_, 'tripadvisor-review' );
            add_variable('ota_l', $lk);

            parse_template('loop_review_list', 'lrlblock', true);
        }
    }

    // NOTE: STRING TRANSLATION
    $translations = array('book now',
                          'discover' ,
                          'view all offers',
                          'fint out more',
                          'accommodation',
                          'next',
                          'prev',
                          'view all',
                          'interesting spot',
                          'what on the ubud village',
                          'instagram desc',
                          'see more in',
                          'reviews',
                          'all');
    $tr_string    = set_string_language($translations, $lang, $check_lang, '');
    add_variable('s_book_now', $tr_string['book_now']);
    add_variable('s_discover', $tr_string['discover']);
    add_variable('s_view_all_offers', $tr_string['view_all_offers']);
    add_variable('s_fint_out_more', $tr_string['fint_out_more']);
    add_variable('s_accommodation', $tr_string['accommodation']);
    add_variable('s_next', $tr_string['next']);
    add_variable('s_prev', $tr_string['prev']);
    add_variable('s_view_all', $tr_string['view_all']);
    add_variable('s_interesting_spot', $tr_string['interesting_spot']);
    add_variable('s_what_on_the_ubud_village', $tr_string['what_on_the_ubud_village']);
    add_variable('s_instagram_desc', $tr_string['instagram_desc']);
    add_variable('s_see_more_in', $tr_string['see_more_in']);
    add_variable('s_reviews', $tr_string['reviews']);
    add_variable('s_all', $tr_string['all']);

    parse_template( 'home-block', 'h-block', false );

    return return_template( 'h-template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Title Homepage Destination Hero
| -------------------------------------------------------------------------------------------------------------------------
*/
function title_hero_destination($post, $lang='', $check_lang=false)
{
    $post_id = $post[0]['id'];
    $title   = $post[0]['title'];

    $title_home_page = get_additional_field($post_id, 'title_home_page', 'destinations');
    $subtitle_home_page = get_additional_field($post_id, 'subtitle_home_page', 'destinations');
    $brief = $post[0]['brief'];

    if($check_lang)
    {
        $brief_lang    = get_additional_field($post_id, 'brief_'.$lang, 'destinations');
        $title_home_page_lang = get_additional_field($post_id, 'title_home_page_'.$lang, 'destinations');
        $subtitle_home_page_lang = get_additional_field($post_id, 'subtitle_home_page_'.$lang, 'destinations');

        $brief    = (empty($brief_lang) ? $brief:       $brief_lang);
        $title_home_page    = (empty($title_home_page_lang) ? $title_home_page:       $title_home_page_lang);
        $subtitle_home_page = (empty($subtitle_home_page_lang) ? $subtitle_home_page: $subtitle_home_page_lang);
    }

    $html = '
        <div class="hgreet">
          <div class="withlove flex">
            <h1>'.$title_home_page.'</h1>
            <div class="embel-embel flex">
              <a class="b-lazy h-disc"
                 data-src="'.HTTP.TEMPLATE_URL.'/assets/images/circle_arrow.svg">
                DISCOVER
              </a>
            </div>
          </div>
          <p>'.$subtitle_home_page.'</p>
          <div class="embel-embel flex f-mob">
            <a class="b-lazy h-disc"
               data-src="'.HTTP.TEMPLATE_URL.'/assets/images/circle_arrow.svg">
              DISCOVER
            </a>
          </div>
        </div>
    ';
    return $html;
}

?>
