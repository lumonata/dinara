<?php
/*
	Title: Samaya Template
	Preview: -
	Author: Tantri Mindrawan
	Author Url: http://www.lumonata.com
	Description: Custom template for samaya
*/

header( 'Access-Control-Allow-Origin: *' );

require_once 'functions.php';

$string_translations = get_string_translation_lang();
$page_404            = 0;
$version             = "?v=1.0.0";
add_actions( 'version_js_css', $version );

set_template( TEMPLATE_PATH . '/index.html' );

if( is_ajax_page() )
{
    set_template( TEMPLATE_PATH . '/index.html' );
    add_block( 'main-block', 'm-block' );
    add_variable( 'content-area', ajax_page_content() );

}
// elseif(is_home() || is_language_home())
// {
//
//     set_template( TEMPLATE_PATH . '/template.html' );
//
//     $cek_url        = cek_url();
//     $lang           = $cek_url['0'];
//     $destination    = 'monkey-forest';
//     $check_lang     = check_language_code($lang);
//     $corporatedata  = get_articles_data('destinations', $destination, $lang, $check_lang);
//     $appname        = trim(get_appname());
//     $destination_id = $corporatedata[0]['id'];
//
//     set_switch_language();
//     home_destination_content($corporatedata, $appname, $destination_id, $lang, $check_lang, $string_translations);
//
//     add_block( 'main-block', 'm-block' );
//     // set_variable_string_translations($lang, $check_lang, $string_translations);
//
//     $site_url = HTTP.site_url();
//     $appcls   = 'home-page';
//
//     // CONFIG META DATA
//     $meta_title       = get_meta_data( 'meta_title', 'global_setting' );
//     $meta_keywords    = get_meta_keywords_tag(get_meta_data( 'meta_keywords', 'global_setting' ));
//     $meta_description = get_meta_data( 'meta_description', 'global_setting' );
//     $og_url           = '';
//     $og_title         = '';
//     $og_image         = '';
//     $og_site_name     = '';
//     $og_description   = '';
//
//     add_meta_data_value($meta_title, $meta_keywords, $meta_description, $og_url, $og_title, $og_image, $og_site_name, $og_description);
// }
// else
// {
//     $appname             = trim(get_appname());
//
//     $cek_url             = cek_url();
//     $appcls              = '';
//     $site_url            = HTTP.site_url().'/'.$appname;
//     $thecontent          = "";
//     $title_hero          = "";
//     $bg_hero             = "";
//     $header_layout       = "";
//     $social_media_footer = "";
//     $lang                = "";
//     $check_lang          = false;
//     $name_logo           = $appname;
//     $header_menu         = "";
//     $footer_menu         = "";
//
//     set_template( TEMPLATE_PATH . '/template.html' );
//
//     add_block( 'main-block', 'm-block' );
//
//     if(check_language_code($appname) || check_destination($appname))
//     {
//         $corporatedata  = "";
//         $class_body_add = "";
//
//         $lang       = $cek_url['0'];
//         $check_lang = check_language_code($lang);
//
//         $language_page = "";
//         if($check_lang)
//         {
//             $class_body_add = $lang;
//             $language_page = $lang;
//             add_variable('language_page', $language_page);
//         }
//
//         if(check_destination($appname))
//         {
//             $corporatedata = get_articles_data('destinations', $appname, $lang, $check_lang);
//             $header_menu   = the_menus( 'menuset='.ucfirst($appname).' Header&show_title=false', $lang, $check_lang );
//             $footer_menu   = the_menus( 'menuset='.ucfirst($appname).' Footer&show_title=false', $lang, $check_lang );
//         }
//         elseif(check_destination($cek_url[1]))
//         {
//             $corporatedata = get_articles_data('destinations', $cek_url[1], $lang, $check_lang);
//             $header_menu   = the_menus( 'menuset='.ucfirst($cek_url[1]).' Header&show_title=false', $lang, $check_lang );
//             $footer_menu   = the_menus( 'menuset='.ucfirst($cek_url[1]).' Footer&show_title=false', $lang, $check_lang );
//         }
//
//         if(!empty($corporatedata))
//         {
//
//             $appsef               = get_uri_sef();
//             $destination_id       = $corporatedata[0]['id'];
//             $additional_corporate = get_additional_destination_data($destination_id);
//
//             $properties_name = get_additional_field($destination_id,'fb_properties_name','destinations');
//             $hotels_name     = get_additional_field($destination_id,'fb_hotels_name','destinations');
//             add_actions('properties_name', $properties_name);
//             add_actions('hotels_name', $hotels_name);
//
//             if(get_uri_count() == 1)
//             {
//                 $bg_hero           = $corporatedata[0]['bg_homepage'];
//                 $appcls            = "home-destination page-$appname $class_body_add page-all";
//                 $title_hero        = title_hero_destination($corporatedata);
//                 $thecontent        = home_destination_content($corporatedata, $appname, $destination_id, $lang, $check_lang, $string_translations);
//                 $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
//
//                 // POPUP NEWSLETTER
//                 show_popup_newsletter_html($destination_id, $lang, $check_lang);
//             }
//             elseif(get_uri_count() == 2)
//             {
//                 // MENGECEK URL ARRAY PERTAMA APAKAH TERMASUK DALAM DESTINATION DATA ATAU TIDAK
//                 if(check_destination($cek_url[1]))
//                 {
//
//                     $appname       = $cek_url[1];
//                     $name_logo     = $appname;
//                     $bg_hero       = $corporatedata[0]['bg_homepage'];
//                     $appcls        = "home-destination page-$appname $class_body_add page-all";
//                     $title_hero    = title_hero_destination($corporatedata, $lang, $check_lang);
//                     $thecontent    = home_destination_content($corporatedata, $appname, $destination_id, $lang, $check_lang, $string_translations);
//                     $header_layout = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
//
//                     // POPUP NEWSLETTER
//                     show_popup_newsletter_html($destination_id, $lang, $check_lang);
//                 }
//                 else
//                 {
//
//                     // if($appsef == 'gallery')
//                     // {
//                     //     $appsef = "press-gallery";
//                     // }
//                     $appcls     = "$appsef page-$appname page $class_body_add page-all";
//                     $data_page  = get_data_page($appsef, $destination_id, $lang, $check_lang, $string_translations);
//                     $thecontent = get_page_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
//                     // PAGES
//                     if(!empty($data_page))
//                     {
//
//                         $title_hero    = $data_page['title_hero_template'];
//                         $bg_hero       = $data_page['bg_hero'];
//
//                         if($appsef != "each-review" && $appsef != "contact-information" && $appsef != 'press-download' && $appsef != 'press-gallery' && $appsef != 'press-accolades' && $appsef != 'contact-us' && $appsef != 'locations')
//                         {
//
//                             $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
//                         }
//                         else
//                         {
//                             get_list_dest($lang, $check_lang);
//                             $appcls     = "$appsef page-$appname page navbar-show page-all";
//                         }
//                     }
//                     else
//                     {
//                         if($appsef!="locations")
//                         {
//                             $appcls     = "page-404 page-all";
//                             $page_404   = 1;
//                         }
//                     }
//                 }
//             }
//             elseif(get_uri_count() == 3)
//             {
//
//                 if(check_destination($cek_url[1]))
//                 {
//                     if($appsef == 'gallery')
//                     {
//                         $appsef = "press-gallery";
//                     }
//
//                     $appname    = $cek_url[1];
//                     $name_logo  = $appname;
//                     $appcls     = "$appsef page-$appname page $class_body_add page-all";
//                     $data_page  = get_data_page($appsef, $destination_id, $lang, $check_lang, $string_translations);
//                     $thecontent = get_page_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
//
//                     if(!empty($data_page))
//                     {
//                         $title_hero    = $data_page['title_hero_template'];
//                         $bg_hero       = $data_page['bg_hero'];
//
//                         if($appsef != "special-offers" && $appsef != "news" && $appsef != 'press-download' && $appsef != 'press-gallery' && $appsef != 'gallery' && $appsef != 'press-accolades' && $appsef != 'contact-us' && $appsef != 'locations')
//                         {
//                             $header_layout     = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
//                         }
//                         else
//                         {
//                             get_list_dest($lang, $check_lang);
//                             $appcls     = "$appsef page-$appname page navbar-show $class_body_add page-all";
//                         }
//                     }
//                     else
//                     {
//                         if($appsef!="locations")
//                         {
//                             $appcls     = "page-404 page-all";
//                             $page_404   = 1;
//                         }
//                     }
//                 }
//                 else
//                 {
//                     // detail accomodation dan dining belongs here
//                     $type_product = $cek_url[1];
//                     $appcls       = "page-$appname page-detail $type_product-detail $class_body_add page-all";
//                     $data_page    = get_data_detail_page($appsef, $appname, $type_product, $lang, $check_lang, $string_translations);
//                     $thecontent   = get_page_detail_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
//
//                     if(!empty($data_page))
//                     {
//                         $title_hero    = $data_page['title_hero_template'];
//                         $bg_hero       = $data_page['bg_hero'];
//                         $header_layout = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
//                     }
//                     else
//                     {
//                         $appcls     = "page-404 page-all";
//                         $page_404   = 1;
//                     }
//                 }
//             }
//             elseif(get_uri_count() == 4)
//             {
//                 if(check_destination($cek_url[1]))
//                 {
//                     $appname      = $cek_url[1];
//                     $name_logo    = $appname;
//                     $type_product = $cek_url[2];
//                     $appcls       = "page-$appname page-detail $type_product-detail $class_body_add page-all";
//                     $data_page    = get_data_detail_page($appsef, $appname, $type_product, $lang, $check_lang, $string_translations);
//                     $thecontent   = get_page_detail_content($corporatedata, $appsef, $data_page, $lang, $check_lang, $string_translations);
//
//                     if(!empty($data_page))
//                     {
//                         $title_hero    = $data_page['title_hero_template'];
//                         $bg_hero       = $data_page['bg_hero'];
//                         $header_layout = header_layout($title_hero, $bg_hero, $appname, $header_menu, $corporatedata, $lang, $check_lang);
//                     }
//                     else
//                     {
//                         $appcls     = "page-404 page-all";
//                         $page_404   = 1;
//                     }
//                 }
//                 else
//                 {
//                     $appcls     = "page-404 page-all";
//                     $page_404   = 1;
//                 }
//             }
//             else
//             {
//                 $appname = check_destination($cek_url[1]) ? $cek_url[1] : $appname;
//                 $appcls   = "page-404 page-all";
//                 $page_404 = 1;
//             }
//
//             if(!$page_404)
//             {
//                 $social_media_footer = $corporatedata[0]['social_media']['social_media_footer'];
//
//                 set_other_destination($appname, $lang, $check_lang);
//
//                 // FOOTER IMAGE
//                 $footer_image = get_additional_field($destination_id,'footer_image','destinations');
//                 $footer_image = HTTP.SITE_URL.'/lumonata-plugins/destinations/footer/large/'.$footer_image;
//                 add_variable( 'footer_image', $footer_image );
//             }
//         }
//         else
//         {
//             $appcls     = "page-404 page-all";
//             $page_404   = 1;
//             $appname    = "";
//         }
//     }
//     else
//     {
//         $lang       = $cek_url['0'];
//         $check_lang = check_language_code($lang);
//         $appcls     = "page-404 page-all";
//         $page_404   = 1;
//         $appname    = "";
//     }
//
//     $thecontent  = ($page_404) ? get_page_404($lang, $check_lang, $string_translations, $appname): $thecontent;
//     $home_url    = ($check_lang) ? HTTP.site_url().'/'.$lang.'/'.$appname: HTTP.site_url().'/'.$appname;
//     $mobile_menu = $footer_menu;
//
//     add_variable( 'header_layout', $header_layout );
//     add_variable( 'content_area', $thecontent );
//     add_variable( 'appname_upper', strtoupper($appname) );
//     add_variable( 'appname', $appname );
//
//     $footdes = "The Ubud Village Monkey Forest";
//     $data_logo = '
//       <img class="hero-logo center-relative" src="'.HTTP.TEMPLATE_URL.'/assets/images/logo/Ubud-Resort-and-Spa-white.png" alt="logo Ubud Village">
//       <img class="hero-logo center-relative" src="'.HTTP.TEMPLATE_URL.'/assets/images/logo/Ubud-Resort-and-Spa.png" alt="logo Ubud Village">
//     ';
//     $code_propery = "8498";
//     if ($appname == "monkey-forest") {
//       $footdes = "The Ubud Village Nyuh Kuning";
//       $data_logo = '
//         <img class="hero-logo center-relative" src="'.HTTP.TEMPLATE_URL.'/assets/images/logo/ubud-hotel-white.png" alt="logo Ubud Village">
//         <img class="hero-logo center-relative" src="'.HTTP.TEMPLATE_URL.'/assets/images/logo/ubud-hotel.png" alt="logo Ubud Village">
//       ';
//       $code_propery = "7905";
//     }
//     add_variable( 'foot_destination', $footdes );
//     add_variable( 'code_des', $code_propery );
//     add_variable( 'logo', $data_logo );
//
//     add_variable( 'name_logo', $name_logo );
//     add_variable( 'appname_ucwords', ucwords($appname) );
//     add_variable( 'header_menu', $header_menu );
//     add_variable( 'footer_menu', $footer_menu );
//     add_variable( 'mobile_popup_menu', $mobile_menu );
//     add_variable( 'home_url', $home_url );
//     add_variable( 'social_media_footer', $social_media_footer );
//     add_variable( 'booking_popup_accommodation', attemp_actions( 'booking_popup_accommodation' ) );
//     add_variable( 'booking_popup_spa', attemp_actions( 'booking_popup_spa' ) );
//     add_variable( 'booking_popup_wedding', attemp_actions( 'booking_popup_wedding' ) );
//     add_variable( 'booking_popup_events', attemp_actions( 'booking_popup_events' ) );
//     add_variable( 'booking_popup_offers', attemp_actions( 'booking_popup_offers' ) );
//     // add_variable( 'booking_popup_dinings', attemp_actions( 'booking_popup_dinings' ) );
// }

$cek_url        = cek_url();
$lang           = $cek_url['0'];
$check_lang     = check_language_code($lang);
$header_menu    = the_menus( 'menuset=Menu Header&show_title=false', $lang, $check_lang );
$footer_menu    = the_menus( 'menuset=Menu Footer&show_title=false', $lang, $check_lang );

add_variable( 'content_area', set_content($lang , $check_lang) );
add_variable( 'header_menu', $header_menu );
add_variable( 'footer_menu', $footer_menu );

// GOOGLE TAG MANAGER
$google_tag_manager = get_meta_data("google_tag_manager", 'static_setting');
$google_tag_manager_noscript = get_meta_data("google_tag_manager_noscript", 'static_setting');

// NOTE: AR AR AR AR AR ~
$sage = '';
if (get_uri_sef() == 'contact-information' || get_uri_sef() == 'each-review') {
  $sage = 'sage-mode';
}
add_variable( 'sage', $sage );

// END ~~
$currentlang = '';
$langu       = $cek_url['0'];
$ccheck_langu = check_language_code($langu);
if ($ccheck_langu) {
  $currentlang = $langu;
}else{
  $currentlang ='en';
}
add_variable( 'current_lang', $currentlang );
add_variable( 'google_tag_manager', $google_tag_manager );
add_variable( 'google_tag_manager_noscript', $google_tag_manager_noscript );
add_variable( 'year', date( 'Y' ) );
add_variable( 'urisef', get_uri_sef());
// add_variable( 'body_class', $appcls );
add_variable( 'web_title', web_title() );
add_variable( 'tagline', web_tagline() );
add_variable( 'meta_description', attemp_actions( 'meta_description' ) );
add_variable( 'meta_keywords', attemp_actions( 'meta_keywords' ) );
add_variable( 'meta_title', attemp_actions( 'meta_title' ) );
add_variable( 'og_url', attemp_actions( 'og_url' ) );
add_variable( 'og_title', attemp_actions( 'og_title' ) );
add_variable( 'og_image', attemp_actions( 'og_image' ) );
add_variable( 'og_site_name', attemp_actions( 'og_site_name' ) );
add_variable( 'og_description', attemp_actions( 'og_description' ) );
add_variable( 'version', $version );
add_variable( 'site_url', site_url() );
add_variable( 'HTTP', HTTP );
add_variable( 'web_url', HTTP.site_url() );
add_variable( 'template_url', TEMPLATE_URL );
add_variable( 'assets_url', TEMPLATE_URL.'/assets' );
add_variable( 'include-js', attemp_actions( 'include-js' ) );
add_variable( 'include-js-contact', attemp_actions( 'include-js-contact' ) );
add_variable( 'include-css', attemp_actions( 'include-css' ) );
add_variable( 'code_rest_diary', attemp_actions( 'code_rest_diary' ) );
add_variable( 'code_rest_diary_page_list', attemp_actions( 'code_rest_diary_page_list' ) );

// NOTE: link property
$pl_viasa = get_meta_data( 'pl_viasa', 'static_setting' );
$pl_dinara= get_meta_data( 'pl_dinara', 'static_setting' );

add_variable( 'pl_viasa', $pl_viasa );
add_variable( 'pl_dinara', $pl_dinara );
parse_template( 'main-block', 'm-block' );

print_template();
?>
