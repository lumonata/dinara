<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MENAMPILKAN TITLE HERO PADA PAGE
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_data_page($appsef, $destination_id, $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    // STRING TRANSLATIONS
    $translations = array('Accommodation', 'Dining', 'Spa');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $result = array();

    if($appsef == "about-us"  || $appsef == "contact-information" || $appsef == "news" || $appsef == "press-download" || $appsef == "contact-us" || $appsef == "press-gallery" || $appsef == "press-accolades")
    {
        // if($appsef == "gallery")
        // {
        //     $appsef = "press-gallery";
        // }
        $data_page = get_articles_data('pages', $appsef);
    }
    else if($appsef == "about")
    {
        $data_page = "about";
    }
    else if ($appsef == "each-review") {
        $data_page = "about";
    }
    else
    {
        $data_page = get_meta_data('post_type_setting', $appsef, $destination_id);
    }

    if(!empty($data_page))
    {
        if($appsef == "about-us" || $appsef == "contact-information" || $appsef == "news" || $appsef == "press-download" || $appsef == "press-gallery" || $appsef == "press-accolades" || $appsef == "contact-us")
        {

            // if($appsef == "gallery")
            // {
            //     $appsef = "press-gallery";
            // }

            $page_id     = $data_page[0]['id'];
            $title       = $data_page[0]['title'];
            $subtitle    = $data_page[0]['subtitle'];
            $description = $data_page[0]['desc'];
            $metatitle   = $data_page[0]['meta_title'];
            $metakey     = $data_page[0]['meta_keywords'];
            $metadesc    = $data_page[0]['meta_description'];
            $brief       = $data_page[0]['brief'];
            // print_r($data_page);

            if($check_lang)
            {
                $title_lang       = get_additional_field($page_id, 'title_'.$lang, 'pages');
                $subtitle_lang    = get_additional_field($page_id, 'subtitle_'.$lang, 'pages');
                $description_lang = get_additional_field($page_id, 'content_'.$lang, 'pages');
                $meta_title_lang  = get_additional_field($page_id, 'meta_title_'.$lang, 'pages');
                $meta_key_lang    = get_additional_field($page_id, 'meta_keywords_'.$lang, 'pages');
                $meta_desc_lang   = get_additional_field($page_id, 'meta_description_'.$lang, 'pages');

                $title       = (empty($title_lang) ? $title:             $title_lang);
                $subtitle    = (empty($subtitle_lang) ? $subtitle:       $subtitle_lang);
                $description = (empty($description_lang) ? $description: $description_lang);
                $metatitle   = (empty($meta_title_lang) ? $metatitle:    $meta_title_lang);
                $metakey     = (empty($meta_key_lang) ? $metakey:        $meta_key_lang);
                $metadesc    = (empty($meta_desc_lang) ? $metadesc:      $meta_desc_lang);
            }

            $bg_image    = get_featured_img( $page_id, 'pages', false );
            $bg_hero     = (!empty($bg_image) ? HTTP.site_url().$bg_image['large'] : "");
        }
        else if($appsef == "about")
        {
	          $bg_image    = get_additional_field($destination_id, 'bg_image_policy','destinations');
            $title       = get_additional_field($destination_id, 'title_policy', 'destinations');
            $subtitle    = get_additional_field($destination_id, 'sub_title_policy', 'destinations');
            $description = get_additional_field($destination_id, 'description_policy', 'destinations');
            $metatitle   = get_additional_field($destination_id, 'meta_title_policy', 'destinations');
            $metakey     = get_additional_field($destination_id, 'meta_keywords_policy', 'destinations');
            $metadesc    = get_additional_field($destination_id, 'meta_description_policy', 'destinations');

            if($check_lang)
            {
                $title_lang       = get_additional_field($destination_id, 'title_policy_'.$lang, 'destinations');
                $subtitle_lang    = get_additional_field($destination_id, 'sub_title_policy_'.$lang, 'destinations');
                $description_lang = get_additional_field($destination_id, 'description_policy_'.$lang, 'destinations');

                $meta_title_lang  = get_additional_field($destination_id, 'meta_title_policy_'.$lang, 'destinations');
                $meta_key_lang    = get_additional_field($destination_id, 'meta_keywords_policy_'.$lang, 'destinations');
                $meta_desc_lang   = get_additional_field($destination_id, 'meta_description_policy_'.$lang, 'destinations');

                $title       = (empty($title_lang) ? $title:             $title_lang);
                $subtitle    = (empty($subtitle_lang) ? $subtitle:       $subtitle_lang);
                $description = (empty($description_lang) ? $description: $description_lang);
                $metatitle   = (empty($meta_title_lang) ? $metatitle:    $meta_title_lang);
                $metakey     = (empty($meta_key_lang) ? $metakey:        $meta_key_lang);
                $metadesc    = (empty($meta_desc_lang) ? $metadesc:      $meta_desc_lang);

            }
            $brief       = '';
            $subtitle    = '';
            $bg_hero = HTTP.SITE_URL.'/lumonata-plugins/destinations/bg_image_policy/'.$bg_image;
        }
        else if($appsef == "each-review")
        {
          $page_id     = '';
          $title       = '';
          $subtitle    = '';
          $description = '';
          $metatitle   = '';
          $metakey     = '';
          $metadesc    = '';
          $brief       = '';
          $bg_image    = '';
          $bg_hero     = '';
        }
        else
        {
            $post_type_setting = json_decode($data_page, true);
            $title             = $post_type_setting['title'];
            $description       = $post_type_setting['description'];
            $subtitle          = $post_type_setting['subtitle'];
            $metatitle         = $post_type_setting['meta_title'];
            $metakey           = $post_type_setting['meta_keywords'];
            $metadesc          = $post_type_setting['meta_description'];
            $pdf_file          = (isset($post_type_setting['pdf_file_spa_menu']) ? $post_type_setting['pdf_file_spa_menu']: '' );
            $code_rest_diary   = (isset($post_type_setting['code_rest_diary']) ? $post_type_setting['code_rest_diary'] : '' );
            $brief             = $post_type_setting['brief'];
            // $subtitle          = $tr_string[$appsef];

            if($check_lang)
            {
                $title_lang             = (isset($post_type_setting['title_'.$lang]) ? $post_type_setting['title_'.$lang] : '' );
                $subtitle_lang          = (isset($post_type_setting['subtitle_'.$lang]) ? $post_type_setting['subtitle_'.$lang] : '');
                $description_lang       = (isset($post_type_setting['description_'.$lang]) ? $post_type_setting['description_'.$lang] : '');
                $meta_title_lang        = (isset($post_type_setting['meta_title_'.$lang]) ? $post_type_setting['meta_title_'.$lang] : '' );
                $meta_key_lang          = (isset($post_type_setting['meta_keywords_'.$lang]) ? $post_type_setting['meta_keywords_'.$lang] : '' );
                $meta_desc_lang         = (isset($post_type_setting['meta_description_'.$lang]) ? $post_type_setting['meta_description_'.$lang] : '' );
                $pdf_file_lang          = (isset($post_type_setting['pdf_file_spa_menu_'.$lang]) ? $post_type_setting['pdf_file_spa_menu_'.$lang] : '' );
                $brief_lang             = (isset($post_type_setting['brief_'.$lang]) ? $post_type_setting['brief_'.$lang] : '' );

                $title          = (empty($title_lang) ? $title:       $title_lang);
                $subtitle       = (empty($subtitle_lang) ? $subtitle: $subtitle_lang);
                $description    = (empty($description_lang) ? $description:   $description_lang);
                $metatitle      = (empty($meta_title_lang) ? $metatitle:   $meta_title_lang);
                $metakey        = (empty($meta_key_lang) ? $metakey:   $meta_key_lang);
                $metadesc       = (empty($meta_desc_lang) ? $metadesc:   $meta_desc_lang);
                $pdf_file       = (empty($pdf_file_lang) ? $pdf_file:   $pdf_file_lang);
                $brief          = (empty($brief_lang) ? $brief:   $brief_lang);
            }

            $bg_image          = $post_type_setting['bg_image'];
            $url_bg            = get_url_hero_image_post_setting($bg_image);
            $bg_hero           = $url_bg['bg_image_large'];

            // $bg_dtimage          = $post_type_setting['bg_dtimage'];
            // $url_bgdt            = get_url_hero_image_post_setting($bg_dtimage);
            // $bg_dthero           = $url_bgdt['bg_image_large'];

            $result['pdf_file']        = $pdf_file;
            $result['code_rest_diary'] = $code_rest_diary;
        }

        // $new_title = '<h1 class="text _text text-title-page-1">'.$title.'</h1>';
        //
        // if($appsef != "events" && $appsef != "policy")
        // {
        //     if($appsef == "what-to-do-act-in")
        //     {
        //         if(strpos(strtolower($title), ' in '))
        //         {
        //             $explode_title = explode(" in ", strtolower($title));
        //             $title_1       = $explode_title[0]." IN";
        //             $title_2       = $explode_title[1];
        //             $new_title     = '<h1 class="text _text text-title-page-1">'.strtoupper($title_1).'<br/>'.strtoupper($title_2).'</h1>';
        //         }
        //     }
        //     elseif($appsef == "about-us")
        //     {
        //         if(strpos(strtolower($title), ' of '))
        //         {
        //             $explode_title = explode(" of ", strtolower($title));
        //             $title_1       = $explode_title[0]." OF";
        //             $title_2       = $explode_title[1];
        //             $new_title     = '<h1 class="text _text text-title-page-1">'.strtoupper($title_1).'<br/>'.strtoupper($title_2).'</h1>';
        //         }
        //     }
        //     else
        //     {
        //         $explode_title = explode(" ", $title);
        //         $title_1       = str_replace(" ".end($explode_title), "", $title);
        //         $title_2       = end($explode_title);
        //         $new_title     = '<h1 class="text _text text-title-page-1">'.$title_1.'<br/>'.$title_2.'</h1>';
        //
        //         if($check_lang)
        //         {
        //             $new_title     = '<h1 class="text _text text-title-page-1">'.$title.'</h1>';
        //         }
        //     }
        // }

        $result['title_hero_template'] = '
            <div class="hgreet">
              <div class="withlove flex">
                <h1>'.$title.'</h1>
                <div class="embel-embel flex">
                  <a class="b-lazy h-disc"
                     data-src="'.HTTP.TEMPLATE_URL.'/assets/images/circle_arrow.svg">
                    DISCOVER
                  </a>
                </div>
              </div>
              <p>'.$subtitle.'</p>

              <div class="embel-embel flex f-mob">
                <a class="b-lazy h-disc"
                   data-src="'.HTTP.TEMPLATE_URL.'/assets/images/circle_arrow.svg">
                  DISCOVER
                </a>
              </div>
            </div>


        ';
        $result['bg_hero']  = $bg_hero;
        // $result['bg_dthero']  = $bg_dthero;
        $result['og_image'] = $bg_hero;

        if($appsef == "events")
        {
            $title = $subtitle;
        }

        $result['title']       = $title;
        $result['description'] = $description;
        $result['subtitle']    = $subtitle;
        $result['type']        = $appsef;
        $result['metatitle']   = $metatitle;
        $result['metakey']     = $metakey;
        $result['metadesc']    = $metadesc;
        $result['brief']       = $brief;
    }

    return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function page_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    global $db;
    $version = attemp_actions('version_js_css');

    // STRING TRANSLATIONS
    $translations = array('Single', 'King Size', 'Double', 'Double Twin', 'Villa Size', 'Bed Type', 'Maximum Capacity', 'Pool Size', 'Villa Benefits', 'Book Now', 'Reserve Table', 'Details', 'Spa Menu', 'Explore', 'Make an Inquiry');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    set_template( TEMPLATE_PATH . '/template/page.html', 'page_template' );
    add_block( 'acco-block', 'ablock', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'loop-list-only-spa-block', 'llosblock', 'page_template' );
    add_block( 'page-list-only-spa-block', 'plosblock', 'page_template' );
    add_block( 'page-list-block', 'plblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title_page       = $data_page['title'];
    $description_page = $data_page['description'];
    $code_rest_diary  = $data_page['code_rest_diary'];

    add_actions( 'code_rest_diary_page_list', $code_rest_diary );

    add_variable( 'title_page', $title_page );
    add_variable( 'description_page', $description_page );

    $appsef        = get_uri_sef();
    $product       = get_post_list($appsef, '', '', $destination);
    $key           = 1;
    $nav_li        = "";
    $option_spa    = "";
    $option_events = "";


    // $page_url = HTTP . site_url().'/'.$destination.'/'.$appsef.'/';
    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/' : HTTP . site_url().'/'.$destination.'/'.$appsef.'/');

    config_meta_data($page_url, $data_page);

    if($appsef == "dining")
    {
        if($destination=="seminyak")
        {
            $dining_link = 'https://www.resdiary.com/restaurant/breezesamaya';
        }
        else
        {
            $dining_link = 'https://www.resdiary.com/restaurant/sweptawaysamaya';
        }
    }


    if(count($product) > 0)
    {

        add_actions( 'list_link_product_hero', 'make_list_product_hero', $product, $lang, $check_lang );

        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_brief      = $d['post_brief'];
            $post_content    = $d['post_content'];
            $post_sef        = $d['post_sef'];
            $post_attachment = $d['post_attachment'];
            $post_link       = HTTP . site_url().'/'.$destination.'/'.$appsef.'/'.$post_sef.'.html';
            $post_gallery    = set_post_gallery_html($post_attachment);
            $nav_li          .= '<li class="' . ( $key == 1 ? 'active': '' ) . '"><a href="#'.$post_sef.'"></a></li>';
            $class_scroll    = "paralax-$key";
            $featured_image  = HTTP.site_url().'/'.$d['post_featured_img']['large'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, $appsef);
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, $appsef);

                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/'.$post_sef.'.html';
            }


            // echo strtolower($post_title);
            if($appsef!="spa")
            {
                if(strpos(strtolower($post_title), ' of '))
                {
                    $post_title = explode_title_by_at_of(" of ", strtolower($post_title));
                }
                elseif(strpos(strtolower($post_title), ' at '))
                {
                    $post_title = explode_title_by_at_of(" at ", strtolower($post_title));
                }
            }

            if($appsef=="accommodation")
            {

                $size         = get_additional_field( $post_id, 'size', $appsef );
                // $occupancy = get_additional_field( $post_id, 'occupancy', $appsef );
                $bed          = get_additional_field( $post_id, 'bed', $appsef );
                $pool_size    = get_additional_field( $post_id, 'pool_size', $appsef );

                if($check_lang)
                {
                    $str_replace_bed = strtolower(str_replace("/ ", "", $bed));
                    $str_replace_bed = strtolower(str_replace(" ", "_", $str_replace_bed));
                    $bed_lang        = $tr_string[$str_replace_bed];
                    $size_lang       = get_additional_field( $post_id, 'size_'.$lang, $appsef );
                    // $occupancy_lang  = get_additional_field( $post_id, 'occupancy_'.$lang, $appsef );
                    $pool_size_lang  = get_additional_field($post_id, 'pool_size_'.$lang, $appsef);

                    $size         = (empty($size_lang) ? $size:           $size_lang);
                    $pool_size    = (empty($pool_size_lang) ? $pool_size: $pool_size_lang);
                    // $occupancy = (empty($occupancy_lang) ? $occupancy: $occupancy_lang);
                    $bed          = (empty($bed_lang) ? $bed:             $bed_lang);
                }

                $villa_details = "";
                if(!empty($size))
                {
                    $villa_details .= '
                        <div class="container container-detail-acco clearfix">
                            <p class="text _text text-label">'.$tr_string['villa_size'].'</p>
                            <p class="text _text text-value">'.$size.'</p>
                        </div>
                    ';
                }
                if(!empty($bed))
                {
                    $villa_details .= '
                        <div class="container container-detail-acco clearfix">
                            <p class="text _text text-label">'.$tr_string['bed_type'].'</p>
                            <p class="text _text text-value">'.$bed.'</p>
                        </div>
                    ';
                }
                if(!empty($pool_size))
                {
                    $villa_details .= '
                        <div class="container container-detail-acco clearfix">
                            <p class="text _text text-label">'.$tr_string['pool_size'].'</p>
                            <p class="text _text text-value">'.$pool_size.'</p>
                        </div>
                    ';
                }

                add_variable('villa_details', $villa_details);

                parse_template('acco-block', 'ablock', false);
            }

            $link_details = empty($post_content) ? '' : '<a href="'.$post_link.'" class="text link-detail-list-product">'.$tr_string['details'].'</a>';
            $fleft_on = empty($post_content) ? 'fleft' : '';

            $link_details_spa = "";
            if($appsef == "spa")
            {
                $treatment_pdf = get_additional_field( $post_id, 'treatment_pdf', 'spa' );
                $link_pdf      = $post_link;

                if(!empty($treatment_pdf) && file_exists( PLUGINS_PATH . '/treatments/pdf/' . $treatment_pdf ))
                {
                    $link_pdf = URL_PLUGINS . 'treatments/pdf/' . $treatment_pdf;
                }

                $option_spa  .= '<option value="'.$post_id.'">'.ucwords(strtolower($post_title)).'</option>';
                $link_details = '<a href="'.$link_pdf.'" class="text link-detail-list-product" target="_blank">'.$tr_string['spa_menu'].'</a>';
                $link_details_spa = empty($post_content) ? '' : '<p class="text _text list-item-text-explore" data-aos="fade-up">'.$tr_string['explore'].'</p>';

                $post_link = empty($post_content) ? '#' : $post_link;
            }

            add_variable('post_title', $post_title);
            add_variable('post_brief', $post_brief);
            add_variable('post_gallery', $post_gallery);
            add_variable('post_link', $post_link);
            add_variable('container_id', $post_sef);
            add_variable('class_scroll', $class_scroll);
            add_variable('link_details', $link_details);
            add_variable('link_details_spa', $link_details_spa);
            add_variable('featured_image', $featured_image);

            // SET BASE64
            $val_array = array(
                'key'   => $post_id,
                'title' => $post_title
            );
            $values_json = base64_encode( json_encode( $val_array ) );
            add_variable('values_json', $values_json);

            if($appsef == "events")
            {
                $option_events  .= '<option value="'.ucwords(strtolower($post_title)).'">'.ucwords(strtolower($post_title)).'</option>';
                // $button_book_now_list = '
                //     <a href="'.$post_link.'" class="text _text text-book-now '.$fleft_on.'"><p>'.$tr_string['book_now'].'</p> </a>
                // ';
                $button_book_now_list = '';
            }
            elseif($appsef == "dining")
            {
                $button_book_now_list = "";
                $code_rest_diary    = get_additional_field( $post_id, 'code_rest_diary', 'dining' );
                if(!empty($code_rest_diary))
                {
                    $button_book_now_list = '
                        <a href="'.$post_link.'" class="text _text text-book-now '.$fleft_on.'"><p>'.$tr_string['reserve_table'].'</p> </a>
                    ';
                }
            }
            else
            {
                $button_book_now_list = '
                    <button class="text _text text-book-now '.$fleft_on.'" data-type="'.$appsef.'" data-key="'.$values_json.'"><p>'.$tr_string['book_now'].'</p> </button>
                ';
            }
            add_variable('button_book_now_list', $button_book_now_list);

            if($appsef != "spa")
            {
                parse_template('loop-list-block', 'llblock', true);
            }
            else
            {
                parse_template('loop-list-only-spa-block', 'llosblock', true);
            }
            $key++;
        }
        add_variable('nav_li', $nav_li);
    }

    $destination_id = $corporatedata[0]['id'];

    if($appsef == "spa")
    {
        $pdf_file_spa_menu = isset($data_page['pdf_file']) ? $data_page['pdf_file'] : '';

        $folder_lang = "";
        if($check_lang)
        {
            $folder_lang = $lang.'/';
        }

        if(!empty($pdf_file_spa_menu) && file_exists(PLUGINS_PATH . '/custom-post/pdf/'.$folder_lang.$pdf_file_spa_menu))
        {
            $download_spa_menu_link = URL_PLUGINS . 'custom-post/pdf/'.$folder_lang.$pdf_file_spa_menu;
            $download_spa_menu_link = '<a class="spa-menu-pdf-link" href="'.$download_spa_menu_link.'" target="_blank"><span>'.$tr_string['spa_menu'].'</span></a>';
            add_variable( 'download_spa_menu_link', $download_spa_menu_link );
        }

        add_actions( 'booking_popup_spa', 'booking_popup_spa_content', $destination, $destination_id );
        add_actions( 'option_spa', $option_spa );
        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );

        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix" data-key="">
                <p class="text _text text-book-now">'.$tr_string['book_now'].'</p>
            </button>
        ';

        add_actions('button_book_hero', $button_book_hero);
        // add_actions( 'booking_popup_dinings', 'booking_popup_dinings_content' );
        add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
        add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );
    }
    elseif($appsef == "weddings")
    {
        add_actions( 'booking_popup_wedding', 'booking_popup_wedding_content', $destination, $destination_id );
        add_actions( 'option_spa', $option_spa );
        add_actions( 'lang', $lang );
        add_actions( 'check_lang', $check_lang );
    }
    elseif($appsef == "events")
    {
        add_actions( 'option_events', $option_events );
        $button_book_hero = '
            <button class="container container-button-book-now book-now-hero clearfix" data-key="">
                <p class="text _text text-book-now">'.$tr_string['book_now'].'</p>
            </button>
        ';
        add_actions('button_book_hero', $button_book_hero);
        add_actions( 'booking_popup_events', 'booking_popup_events_content' );
        add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
        add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

        $book_now_under_description = '<a href="#" class="btn-book-now-events">'.$tr_string['make_an_inquiry'].'</a>';
        add_variable('book_now_under_description', $book_now_under_description);
    }
    elseif($appsef == "dining")
    {
        // BUTTON BOOK NOW
        $button_book_hero = '
            <a href="'.$dining_link.'" class="container container-button-book-now-link book-now-hero clearfix" target="_blank">
                <p class="text _text text-book-now">'.$tr_string['reserve_table'].'</p>
            </a>
        ';

        add_actions('button_book_hero', $button_book_hero);
        // add_actions( 'booking_popup_dinings', 'booking_popup_dinings_content' );

        $html_rest_diary = '<div id="rd-widget-frame" style="max-width: 600px; margin: auto;"></div>';
        add_variable('html_rest_diary', $html_rest_diary);
    }

    add_variable('appsef', $appsef);

    if($appsef != "spa")
    {
        parse_template( 'page-list-block', 'plblock', false );
    }
    else
    {
        parse_template( 'page-list-only-spa-block', 'plosblock', false );
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK CONTACT INFORMATION TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function coninfo_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/contact-information.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );


    $translations = array('Explore More', 'Direction');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $app_name = trim( get_appname() );

    $lang_url = "";
    if(!empty($app_name))
    {
        $lang_url = $app_name.'/';
    }

    $corporate   = get_articles_data('destinations');
    $dest = array();
    $destination_list = "";
    $dest_list = "";
    $i = 0;
    foreach( $corporate as $d )
    {
        $id                  = $d['id'];
        $title               = $d['title'];
        $content             = $d['desc'];
        $brief               = $d['brief'];
        $content             = get_additional_field($id,'content_'.$lang,'destinations');
        $address             = get_additional_field($id,'address','destinations');
        $phone               = get_additional_field($id,'phone_number','destinations');
        $fax                 = get_additional_field($id,'fax_number','destinations');
        $email               = get_additional_field($id,'email_address','destinations');
        $link_direction      = get_additional_field($id,'link_direction','destinations');
        $coordinate_location = get_additional_field($id,'coordinate_location','destinations');
        $coordinate_location = explode(',', $coordinate_location);

        // FAST BOOKING ACCOMMODATION SETTING
        $properties_name = get_additional_field($id,'fb_properties_name','destinations');
        $hotels_name     = get_additional_field($id,'fb_hotels_name','destinations');
        $corporate_image = get_additional_field($id,'corporate_image','destinations');

        if($check_lang)
        {
            $title_lang   = get_additional_field($id,'title_'.$lang,'destinations');
            $content_lang = get_additional_field($id,'content_'.$lang,'destinations');
            $brief_lang   = get_additional_field($id,'brief_'.$lang,'destinations');

            $title   = (empty($title_lang) ? $title:     $title_lang);
            $content = (empty($content_lang) ? $content: $content_lang);
            $brief   = (empty($brief_lang) ? $brief:     $brief_lang);
        }

        add_variable( 'sef', $d['sef'] );
        // print_r($d);
        add_variable( 'text', $title );
        add_variable( 'content', $content );
        add_variable( 'brief', $brief );
        add_variable( 'background', $d['background'] );
        add_variable( 'link', HTTP.site_url() . '/' . $lang_url . $d['sef'] );

        $destination_list .= '<li data-hotelsname="'.$hotels_name.'" data-properties="'.$properties_name.'">'.$title.'</li>';


        // NOTE: AR AR AR AR AR AR ~~
        add_variable( 'title_each', $d['title_landing'] );
        add_variable( 'address_each', $address );
        add_variable( 'email_each', $email );
        add_variable( 'phone_each', $phone );
        add_variable( 'link_direct', $link_direction );
        add_variable( 'fax_each', $fax );
        add_variable( 'i', $i );
        $i++;

        // SET DESTINATIONS DATA FOR GOOGLE MAPS
        $dest[] = array(
            'title'           => 'The Samaya '.ucwords($d['sef']),
            'address'         => $address,
            'sef'             => $d['sef'],
            'email'           => $email,
            'phone'           => $phone,
            'text_explore'    => $tr_string['explore_more'],
            'text_direction'  => $tr_string['direction'],
            'latitude'        => $coordinate_location[0],
            'longitude'       => $coordinate_location[1],
            'link_direction'  => $link_direction,
            'corporate_image' => HTTP.site_url() . '/lumonata-plugins/destinations/background/'.$corporate_image,
            'link_explore'    => HTTP.site_url() . '/' . $lang_url . $d['sef'],
        );

        parse_template( 'loop-list-block', 'llblock', true );
    }

    add_actions( 'dest_list', $dest_list );
    add_variable( 'destination_list', $destination_list );
    add_variable('dest_data', json_encode($dest));

    add_actions( 'include-js-contact', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/contact-information-maps.js', 'async defer' );
    add_actions( 'include-js-contact', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U&callback=initialize', 'async defer' );
    parse_template( 'page-block', 'pblock', false );
    return return_template( 'page_template' );

}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK CONTACT INFORMATION TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function about_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/about.html', 'page_template' );
    // add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );


    $translations = array('Explore More', 'Direction');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $app_name = trim( get_appname() );

    $lang_url = "";
    if(!empty($app_name))
    {
        $lang_url = $app_name.'/';
    }

    $dt = $corporatedata[0];
    $id = $dt['id'];

    $subtitle    = get_additional_field($id, 'sub_title_policy', 'destinations');
    if($check_lang)
    {
        $subtitle_lang  = get_additional_field($destination_id, 'sub_title_policy_'.$lang, 'destinations');
        $subtitle       = (empty($subtitle_lang) ? $subtitle:       $subtitle_lang);
    }

    add_variable('story', $data_page['description']);
    add_variable('hospitality', $subtitle);

    parse_template( 'page-block', 'pblock', false );
    return return_template( 'page_template' );

}



/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK DINING TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function dining_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false,$string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/dining.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);
    add_variable( 'HTTP', HTTP );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $appsef        = get_uri_sef();
    $product       = get_post_list($appsef, '', '', $destination);
    $book_form     = '';
    $reserve_button= '';

    $translations = array('reservation', 'see details');
    $tr_string    = set_string_language($translations, $lang, $check_lang,$string_translations);
    add_variable( 's_seedetails', $tr_string['see_details'] );
    if ($appsef == "dining") {
      $reserve_button= '<a class="dn-reserve">'.$tr_string['reservation'].'</a>';
      $book_form     = '

      ';
    }

    add_variable('appsef', $appsef);
    add_variable('book_form', $book_form);
    add_variable('reserve_button', $reserve_button);

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/' : HTTP . site_url().'/'.$destination.'/'.$appsef.'/');
    config_meta_data($page_url, $data_page);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            $post_content   = $d['post_content'];
            $post_brief     = $d['post_brief'];
            $post_link      = HTTP . site_url().'/'.$destination.'/'.$appsef.'/'.$d['post_sef'].'/';

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, $appsef);
                $post_content_lang = get_additional_field($post_id, 'content_'.$lang, $appsef);
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/'.$d['post_sef'].'/';

                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_content      = (empty($post_content_lang) ? $post_content: $post_content_lang);
            }

            $featured_image = get_featured_img( $post_id, $appsef, false );

            $fi_s = HTTP . site_url().$featured_image['small'];
            $fi_l = HTTP . site_url().$featured_image['original'];

            // if (empty($post_brief)) {
            //   $post_brief = "asem";
            // }else {
            //   // code...
            // }

            add_variable('post_title', $post_title);
            add_variable('post_content', strip_tags($post_brief));
            add_variable('featured_small', $fi_s);
            add_variable('featured_origin', $fi_l);
            add_variable('post_link', $post_link);

            //gallery bro
            $galeku = get_post_attachment( $post_id, 'gallery' );
            $gale_content = '';
            foreach ($galeku as $galer) {
              $gale_content .= '
                <div class="each-g">
                     <a href="'.$post_link.'">
                       <img ex-src="'.$galer['img_original'].'" src="'.$galer['img_thumb'].'">
                     </a>
                </div>
              ';
            };

            add_variable('boss', $gale_content);

            parse_template('loop-list-block', 'llblock', true);
        }
    }

    // add_actions( 'include-js-contact', 'get_custom_javascript', 'https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js', 'async defer' );
    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );

}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK GALLERY NEW TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function gallery_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/gallery.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $appsef        = get_uri_sef();
    $product       = get_post_list($appsef, '', '', $destination);

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/gallery/' : HTTP . site_url().'/'.$destination.'/gallery/');
    config_meta_data($page_url, $data_page);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            $post_content     = $d['post_content'];
            $post_link       = HTTP . site_url().'/'.$destination.'/'.$appsef.'/'.$d['post_sef'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'dining');
                $post_content_lang = get_additional_field($post_id, 'content_'.$lang, 'dining');
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$appsef.'/'.$d['post_sef'];

                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_content      = (empty($post_content_lang) ? $post_content: $post_content_lang);
            }

            $featured_image = get_featured_img( $post_id, $appsef, false );

            $fi_s = HTTP . site_url().$featured_image['small'];
            $fi_l = HTTP . site_url().$featured_image['original'];

            add_variable('post_title', $post_title);
            add_variable('post_content', $post_content);
            add_variable('featured_small', $fi_s);
            add_variable('featured_origin', $fi_l);
            add_variable('post_link', $post_link);

            //gallery bro
            $galeku = get_post_attachment( $post_id, 'gallery' );
            $gale_content = '
              <div class="slide-bg-hero bg-cover-set b-lazy"
                   data-src="'.$fi_l.'"
                   numb="1"
                   style="background-image: url('.$fi_s.');">
              </div>
            ';
            $p = 2;
            foreach ($galeku as $galer) {
              // print_r($galer);
              $gale_content .= '
                <div class="slide-bg-hero bg-cover-set b-lazy"
                     data-src="'.$galer['img_original'].'"
                     numb="'.$p.'"
                     style="background-image: url('.$galer['img_thumb'].');">
                </div>
              ';

              $p++;
            };

            add_variable('boss', $gale_content);

            parse_template('loop-list-block', 'llblock', true);
        }
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );

}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK SPA TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function spa_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false ,$string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/spa.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $pdf_file    = $data_page['pdf_file'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/spa/' : HTTP . site_url().'/'.$destination.'/spa/');
    config_meta_data($page_url, $data_page);

    // $where_add = " AND a.lstatus_accolades=1";
    $desc_ctn = '';
    if (!empty($description)) {
      $desc_ctn = '
      <h2 class="capt">'.$title.'</h2>
      <div class="sum-con">
        '.$description.'
      </div>
      ';
    }
    add_variable( 'post_content', $desc_ctn);

    $translations = array('download as pdf',
                          'menu',
                          'reservation details',
                          'contact details',
                          'spa location',
                          'treatment category',
                          'treatment list',
                          'no of person',
                          'date and time',
                          'where are you staying',
                          'special request',
                          'optional',
                          'full name',
                          'nationality',
                          'email',
                          'mobile pref phone number',
                          'prefer us to contact you',
                          'phone',
                          'form spa policy',
                          'submit',
                          'tax desc spa',
                          'other',
                          'person',
                          'persons',
                          'hours',
                          'minutes',
                        );
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    add_variable( 's_menu', $tr_string['menu'] );
    add_variable( 's_reservation_details', $tr_string['reservation_details'] );
    add_variable( 's_contact_details', $tr_string['contact_details'] );
    add_variable( 's_spa_location', $tr_string['spa_location'] );
    add_variable( 's_treatment_category', $tr_string['treatment_category'] );
    add_variable( 's_treatment_list', $tr_string['treatment_list'] );
    add_variable( 's_no_of_person', $tr_string['no_of_person'] );
    add_variable( 's_date_and_time', $tr_string['date_and_time'] );
    add_variable( 's_where_are_you_staying', $tr_string['where_are_you_staying'] );
    add_variable( 's_special_request', $tr_string['special_request'] );
    add_variable( 's_optional', $tr_string['optional'] );
    add_variable( 's_full_name', $tr_string['full_name'] );
    add_variable( 's_nationality', $tr_string['nationality'] );
    add_variable( 's_email', $tr_string['email'] );
    add_variable( 's_mobile_pref_phone_number', $tr_string['mobile_pref_phone_number'] );
    add_variable( 's_prefer_us_to_contact_you', $tr_string['prefer_us_to_contact_you'] );
    add_variable( 's_phone', $tr_string['phone'] );
    add_variable( 's_form_spa_policy', $tr_string['form_spa_policy'] );
    add_variable( 's_submit', $tr_string['submit'] );
    add_variable( 's_tax_desc_spa', $tr_string['tax_desc_spa'] );
    add_variable( 's_other', $tr_string['other'] );
    add_variable( 's_person', $tr_string['person'] );
    add_variable( 's_persons', $tr_string['persons'] );
    add_variable( 's_hours', $tr_string['hours'] );
    add_variable( 's_minutes', $tr_string['minutes'] );
    
    $menu_pdf_download = "";
    if(!empty($pdf_file) && file_exists( PLUGINS_PATH . '/custom-post/pdf/' . $pdf_file ))
    {
        $menu_pdf_download = '
            <a href="'.URL_PLUGINS.'custom-post/pdf/' . $pdf_file.'" target="_blank">'.$tr_string['download_as_pdf'].'</a>
        ';
    }
    add_variable('pdf_mn', $menu_pdf_download);

    $product     = get_post_list('spa', '', '', $destination);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $the_list = get_treatment_category_list($post_id,$lang,$check_lang);
            $list_content ='';
            foreach ($the_list as $tl){
              $list_content .= $tl;
            }
        }

        add_variable('list_treatment', $list_content);
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );

}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK FILTER REVIEW DI HOME TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function fillter_reviews_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;
    set_template( TEMPLATE_PATH . '/template/filter/home-review-filter.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $cat = $_GET['cat'];

    $reviews = get_post_list('tripadvisor-review', 56, '', $destination, 1);

    $tripad_review_html = "";
    if(!empty($reviews))
    {
      foreach($reviews as $tr)
      {
          $review_id         = $tr['post_id'];
          $rule              = get_rule($review_id);
          $review_title      = $tr['post_title'];
          $review_desc       = $tr['post_content'];
          $traveller_name    = get_additional_field( $review_id, 'traveller_name', 'tripadvisor-review' );
          $traveller_country = get_additional_field( $review_id, 'country', 'tripadvisor-review' );

          if($check_lang)
          {
              $review_title_lang    = get_additional_field($review_id, 'title_'.$lang, 'tripadvisor-review');
              $review_content_lang    = get_additional_field($review_id, 'content_'.$lang, 'tripadvisor-review');
              $traveller_name_lang    = get_additional_field($review_id, 'traveller_name_'.$lang, 'tripadvisor-review');
              $country_lang    = get_additional_field($review_id, 'country_'.$lang, 'tripadvisor-review');

              $review_title    = (empty($review_title_lang) ? $review_title:       $review_title_lang);
              $review_desc    = (empty($review_content_lang) ? $review_desc:       $review_content_lang);
              $traveller_name    = (empty($traveller_name_lang) ? $traveller_name:       $traveller_name_lang);
              $traveller_country    = (empty($country_lang) ? $traveller_country:       $country_lang);
          }

          $rate = get_additional_field( $review_id, 'rates', 'tripadvisor-review' );
          $rdate = get_additional_field( $review_id, 'rdate', 'tripadvisor-review' );

          $rate = (empty($rate) ? 'b5':       $rate);
          $newDate = '';
          if (!empty($rdate)) {
            $newDate = date("M d, Y", strtotime($rdate));
          }

          $rule_id = get_rule_id_category(get_post_category($review_id)[0]['sef'],'tripadvisor-review');
          $c_ = 'link_ota_monkeyforest';

          if ($destination == 'nyuh-kuning') { $c_ = 'link_ota_nyuhkuning'; }
          $lk = get_additional_rule_field( $rule_id, $c_, 'tripadvisor-review' );

          $checkrule = str_replace(array(":",",","*",".","-"), "", $rule);
          if ($checkrule == strtoupper($cat)) {
            $tripad_review_html .= '

              <div class="each-reviews">
                <span onclick="window.open(\''.$lk.'\', \'_blank\')">'.$rule.'</span>
                <h3 onclick="window.open(\''.$lk.'\', \'_blank\')">'.$review_title.'</h3>
                <p>
                  '.strip_tags($review_desc).'
                </p>
                <div onclick="window.open(\''.$lk.'\', \'_blank\')" class="review-stars '.$rate.'">
                  <img class="noselect b-lazy" data-src="'.HTTP.TEMPLATE_URL.'/assets/images/stars.svg">
                </div>
                <strong onclick="window.open(\''.$lk.'\', \'_blank\')">'.$traveller_name.'</strong>
                <i onclick="window.open(\''.$lk.'\', \'_blank\')">'.$newDate.'</i>

                <a href="'.$lk.'">SEE MORE IN
                  <b class="b-lazy"
                     data-src="'.HTTP.TEMPLATE_URL.'/assets/images/expand_icon.svg">
                     '.$rule.'
                  </b>
                </a>
              </div>
            ';
          }
      }

      add_variable( 'each-review-bycat', $tripad_review_html );
    }

    parse_template( 'page-block', 'pblock', false );
    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK ACCOMODATION TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function accommodation_template($destination, $corporatedata, $data_page, $lang='', $check_lang=false,$string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/accommodation.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);
    add_variable( 'HTTP', HTTP );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    add_variable('juru-kunci', $destination);

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/accommodation/' : HTTP . site_url().'/'.$destination.'/accommodation/');
    config_meta_data($page_url, $data_page);

    // $where_add = " AND a.lstatus_accolades=1";
    $product     = get_post_list('accommodation', '', '', $destination);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            // $post_content     = $d['post_content'];
            $post_link       = HTTP . site_url().'/'.$destination.'/accommodation/'.$d['post_sef'].'/';

            $rmsize = get_additional_field($post_id, 'size' , 'accommodation');
            $bedtype= get_additional_field($post_id, 'bed' , 'accommodation');
            $max_occ= get_additional_field($post_id, 'doccupancy' , 'accommodation');
            $mathrm = get_additional_field($post_id, 'bathrm' , 'accommodation');

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'accommodation');
                // $post_content_lang = get_additional_field($post_id, 'content_'.$lang, 'accommodation');
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/accommodation/'.$d['post_sef'];

                $rmsize_lang = get_additional_field($post_id, 'size_'.$lang , 'accommodation');
                $bedtype_lang= get_additional_field($post_id, 'bed_'.$lang , 'accommodation');
                $max_occ_lang= get_additional_field($post_id, 'doccupancy_'.$lang , 'accommodation');
                $mathrm_lang = get_additional_field($post_id, 'bathrm_'.$lang , 'accommodation');

                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                // $post_content      = (empty($post_content_lang) ? $post_content: $post_content_lang);

                $rmsize  = (empty($rmsize_lang) ? $rmsize: $rmsize_lang);
                $bedtype = (empty($bedtype_lang) ? $bedtype: $bedtype_lang);
                $max_occ = (empty($max_occ_lang) ? $max_occ: $max_occ_lang);
                $mathrm  = (empty($mathrm_lang) ? $mathrm: $mathrm_lang);
            }

            $featured_image = get_featured_img( $post_id, 'accommodation', false );
            $featured_image = HTTP . site_url().$featured_image['medium'];


            add_variable('post_title', $post_title);
            // add_variable('post_content', $post_content);
            add_variable('featured_image', $featured_image);
            add_variable('post_link', $post_link);

            add_variable('roomsize', $rmsize);
            add_variable('bedtype', $bedtype);
            add_variable('maxoccupancy', $max_occ);
            add_variable('bathroom', $mathrm);


            parse_template('loop-list-block', 'llblock', true);
        }
    }

    // NOTE:  STRING TRANSLATION
    $translations = array('details',
                    'book now' ,
                    'accommodation',
                    'room size',
                    'bed type',
                    'max occupancy',
                    'bathrooms');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    add_variable('s_details', strtoupper($tr_string['details']));
    add_variable('s_book_now', strtoupper($tr_string['book_now']));
    add_variable('s_accommodation', strtoupper($tr_string['accommodation']));
    add_variable('s_room_size', $tr_string['room_size']);
    add_variable('s_bed_type', $tr_string['bed_type']);
    add_variable('s_max_occupancy', $tr_string['max_occupancy']);
    add_variable('s_bathrooms', $tr_string['bathrooms']);

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );

}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK MEMBUAT BUTTON LIST PRODUCT UNTUK DI TAMPILKAN PADA HERO
| -------------------------------------------------------------------------------------------------------------------------
*/
function make_list_product_hero($product, $lang='', $check_lang=false)
{
    $appsef  = get_uri_sef();

    $html = '
    <div class="wrap-list-product-link">
        <ul>';
        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_sef        = $d['post_sef'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, $appsef);
                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
            }

            $html .='<li data-filter="#'.$post_sef.'"><div class="bullet"></div><span>'.$post_title.'</span></li>';
        }

    $html .= '
        </ul>
    </div>
    ';

    return $html;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK OFFER TEMPLATE
| -------------------------------------------------------------------------------------------------------------------------
*/
function offers_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/offers.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('book now','details','rate per night','offers');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);
    add_variable('s_booknow', strtoupper($tr_string['book_now']));
    add_variable('s_details', strtoupper($tr_string['details']));
    add_variable('s_rpn', $tr_string['rate_per_night']);
    add_variable('s_offers', $tr_string['offers']);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];
    $product     = get_post_list('special-offers', '', '', $destination);

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);


    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/' : HTTP . site_url().'/'.$destination.'/special-offers/');
    config_meta_data($page_url, $data_page);

    if(count($product) > 0)
    {
        foreach($product as $d)
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_brief      = $d['post_brief'];
            $post_sef        = $d['post_sef'];
            $booking_link    = get_additional_field($post_id, 'booking_link', 'special-offers');
            $label_link      = get_additional_field($post_id, 'label_link', 'special-offers');
            $form_type       = get_additional_field($post_id, 'form_type', 'special-offers');
            $label_link      = ((empty($label_link)) ? $tr_string['book_now'] : $label_link);
            $post_link       = HTTP . site_url().'/'.$destination.'/special-offers/'.$post_sef.'/';

            $form_type = (empty($form_type) ? 2 : $form_type );

            $featured_image  = get_featured_img( $post_id, 'special-offers', false );
            $featured_image  = HTTP . site_url().$featured_image['medium'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'special-offers');
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, 'special-offers');

                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link       = HTTP . site_url().'/'.$lang.'/'.$destination.'/special-offers/'.$post_sef.'/';
            }

            if(empty($booking_link))
            {
                $button_book = '<button class="book-now-item" data-title="'.ucwords(strtolower($post_title)).'" data-form_type="'.$form_type.'">'.$label_link.'</button>';
            }
            else
            {
                $button_book = '<a href="'.$booking_link.'" class="book-now-item-link">'.$label_link.'</a>';
            }

            // $permalink = HTTP . site_url().'/'.$destination.'/offers/'.$sef_offer.'.html';

            add_variable('post_title', $post_title);
            add_variable('post_link', $post_link);
            add_variable('post_brief', $post_brief);
            add_variable('button_book', $button_book);
            add_variable('featured_image', $featured_image);

            parse_template('loop-list-block', 'llblock', true);
        }
    }

    add_actions( 'booking_popup_offers', 'booking_popup_offers_content' );
    add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE NEWS CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function news_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/news.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('Download', 'Read More');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];

    $press_type = "";
    if($type == "news")
    {
        $where_add = " AND a.lstatus_accolades=0";
        $product     = get_post_list('news', '', $where_add, $destination);
        $press_type = "news";
    }
    elseif($type == "press-download")
    {
        $where_add = " AND a.lstatus_file_download=1 AND a.lstatus_accolades=0";
        $product     = get_post_list('news', '', $where_add, $destination);
        $press_type = "press-download";
    }

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);


    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$type.'/' : HTTP . site_url().'/'.$destination.'/'.$type.'/');
    config_meta_data($page_url, $data_page);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            $post_brief     = $d['post_brief'];
            $post_sef       = $d['post_sef'];
            $post_date      = date("M d Y", strtotime($d['post_date']));
            $post_link      = HTTP . site_url().'/'.$destination.'/'.$type.'/'.$post_sef.'.html';
            $featured_image = get_featured_img( $post_id, 'news', false );
            $featured_image = HTTP . site_url().$featured_image['medium'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'news');
                $post_brief_lang = get_additional_field($post_id, 'brief_'.$lang, 'news');

                $post_title = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
                $post_link  = HTTP . site_url().'/'.$lang.'/'.$destination.'/'.$type.'/'.$post_sef.'.html';
            }


            $button = "";
            if($press_type == "news")
            {
                $button = '
                    <div class="detail-btn-item">
                        <a href="'.$post_link.'">'.$tr_string['read_more'].'</a>
                    </div>
                ';
            }
            elseif($press_type == "press-download")
            {
                $file_download = get_additional_field( $post_id, 'file_download', 'news' );

                if( !empty( $file_download ) && file_exists( PLUGINS_PATH . '/additional/file_download/' . $file_download ) )
                {
                    $button = '
                        <div class="detail-download-item">
                            <a target="_blank" href="'.URL_PLUGINS . 'additional/file_download/' . $file_download.'">'.$tr_string['download'].'</a>
                        </div>
                    ';
                }
            }

            add_variable('post_title', $post_title);
            add_variable('post_date', $post_date);
            add_variable('post_brief', $post_brief);
            add_variable('featured_image', $featured_image);
            add_variable('button', $button);
            add_variable('post_link', $post_link);

            parse_template('loop-list-block', 'llblock', true);
        }
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE PRESS ACCOLADES CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function accolades_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/accolades.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);

    $where_add = " AND a.lstatus_accolades=1";
    $product     = get_post_list('news', '', $where_add, $destination);

    if(!empty($product))
    {
        foreach($product as $d)
        {
            $post_id        = $d['post_id'];
            $post_title     = $d['post_title'];
            $post_brief     = $d['post_brief'];

            if($check_lang)
            {
                $post_title_lang = get_additional_field($post_id, 'title_'.$lang, 'news');
                $post_brief_lang = get_additional_field($post_id, 'content_'.$lang, 'news');

                $post_title      = (empty($post_title_lang) ? $post_title: $post_title_lang);
                $post_brief      = (empty($post_brief_lang) ? $post_brief: $post_brief_lang);
            }

            $featured_image = get_featured_img( $post_id, 'news', false );
            $featured_image = HTTP . site_url().$featured_image['medium'];


            add_variable('post_title', $post_title);
            add_variable('post_brief', $post_brief);
            add_variable('featured_image', $featured_image);

            parse_template('loop-list-block', 'llblock', true);
        }
    }

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );

}

/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE PRESS GALLERY CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function press_gallery_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false, $string_translations='')
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/press_gallery.html', 'page_template' );
    add_block( 'loop-list-block', 'llblock', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    // STRING TRANSLATIONS
    $translations = array('All Photos', 'Photo', 'Video', 'Photo 360');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    $title       = $data_page['title'];
    $subtitle    = $data_page['subtitle'];
    $description = $data_page['description'];
    $type        = $data_page['type'];

    add_variable('title', $title);
    add_variable('subtitle', $subtitle);
    add_variable('description', $description);

    add_variable( 'HTTP', HTTP );
    add_variable( 'web_url', HTTP.site_url() );
    add_variable( 'template_url', TEMPLATE_URL );
    add_variable( 'assets_url', TEMPLATE_URL.'/assets' );

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/press-gallery/' : HTTP . site_url().'/'.$destination.'/press-gallery/');
    config_meta_data($page_url, $data_page);

    add_variable('page_url', $page_url);


    // GET CATEGORY GALLERY
    $category      = get_rule_list_front("gallery", "categories", $lang, $check_lang, false, true);
    if(!empty($category))
    {
        $category_html = "<ul>";
        // $category_html .= "<li data-filter='all' data-text='".$tr_string['all_photos']."'>".$tr_string['all_photos']."</li>";

        foreach($category as $dc)
        {
            if(get_count_rule_article($dc['id'], $destination) > 0)
            {
                $category_html .= '<li data-filter="'.$dc['sef'].'" data-text="'.$dc['name'].'" data-key="category">'.$dc['name'].'</li>';
            }
        }

        $category_html .= "</ul>";

        add_variable('category_html', $category_html);
    }

    $where_add  = "";
    $type       = (isset($_GET['type']) ? $_GET['type'] : 'all');
    $category   = (isset($_GET['category']) ? $_GET['category'] : 'all');

    $type_new = "All";
    $inner_join_attachment = "INNER JOIN lumonata_attachment AS d ON (a.larticle_id=d.larticle_id)";

    add_variable('type_search', $type);
    add_variable('type_new', $type_new);

    // WHERE ADD IF CATEGORY NOT EMPTY
    if(!empty($category))
    {
        if($category != "all")
        {
            $rule_id = get_rule_id_category($category, 'gallery');
            if(!empty($rule_id))
            {
                $where_add .= " AND b.lrule_id=$rule_id";
            }
        }
    }

    // PAGING
    $page     = isset($_GET['page']) ? $_GET['page'] : 1;
    $plimit   = 3;
    $viewed   = $plimit;
    $limit    = $viewed * ($page - 1);
    $start    = $limit + 1;

    // ALL LIST GALLERY
    $qallgallery = $db->prepare_query("
        SELECT DISTINCT a.larticle_id AS post_id, a.larticle_title AS post_title
        FROM lumonata_articles AS a
        LEFT JOIN lumonata_rule_relationship AS b
            ON (a.larticle_id=b.lapp_id)
        INNER JOIN lumonata_additional_fields AS c
            ON (a.larticle_id=c.lapp_id)
        $inner_join_attachment
        WHERE a.larticle_type=%s AND a.larticle_status=%s AND c.lkey=%s AND c.lvalue=%s $where_add
        ORDER BY a.lorder ASC
    ",
        "gallery", "publish", 'destination', $destination
    );
    $rallgallery = $db->do_query($qallgallery);
    $nallgallery = $db->num_rows($rallgallery);

    // GET LIST GALLERY
    $qgallery = $db->prepare_query("
        SELECT DISTINCT a.larticle_id AS post_id, a.larticle_title AS post_title
        FROM lumonata_articles AS a
        LEFT JOIN lumonata_rule_relationship AS b
            ON (a.larticle_id=b.lapp_id)
        INNER JOIN lumonata_additional_fields AS c
            ON (a.larticle_id=c.lapp_id)
        $inner_join_attachment
        WHERE a.larticle_type=%s AND a.larticle_status=%s AND c.lkey=%s AND c.lvalue=%s $where_add
        ORDER BY a.lorder ASC
    ",
        "gallery", "publish", 'destination', $destination, $limit, $viewed
    );

    $rgallery = $db->do_query($qgallery);
    $ngallery = $db->num_rows($rgallery);


    $choose_media = array();

    if($ngallery > 0)
    {
        // $i = 1;
        $list_gallery = array();
        while($d = $db->fetch_array($rgallery))
        {
            $post_id         = $d['post_id'];
            $post_title      = $d['post_title'];
            $post_attachment = get_post_attachment( $post_id, 'gallery' );

            if(!empty($post_attachment))
            {
                foreach($post_attachment as $dp)
                {
                    $title_gallery       = $dp['img_title'];
                    $youtube_url         = $dp['img_youtube_url'];
                    $photo360            = $dp['img_photo_360'];
                    $featured_image      = $dp['img_large'];
                    $type_list           = "photo";
                    $class_icon          = '<div class="zoom icon-gallery"></div>';
                    $vcode               = "";
                    $iframe_youtube      = "";
                    $link_fancybox       = $dp['img_original'];
                    $img_attach_language = json_decode($dp['img_attach_language'], true);

                    if($check_lang)
                    {
                        $title_gallery_lang = $img_attach_language[$lang];
                        $title_gallery      = empty($title_gallery_lang) ? $title_gallery : $title_gallery_lang;
                    }

                    $title_image         = '<h3>'.$title_gallery.'</h3>';

                    if(!empty($featured_image))
                    {
                        if(!isset($choose_media['image']))
                        {
                            $choose_media['image'] = '<li data-filter="photo" data-text="'.$tr_string['photo'].'" data-key="type">'.$tr_string['photo'].'</li>';
                        }
                    }

                    if(!empty($youtube_url))
                    {
                        $vcode          = get_v_code_youtube($youtube_url);
                        $iframe_youtube = get_iframe_youtube($vcode);
                        $video_embed    = 'https://www.youtube.com/embed/'.$vcode.'?autoplay=1&cc_load_policy=0&theme=dark&color=white&controls=0&disablekb=0&iv_load_policy=3&loop=1&modestbranding=1&rel=0&showinfo=0&html5=1&autohide=1';
                        $link_fancybox  = $youtube_url;
                        $title_image    = "";
                        $class_icon     = '<div class="play-video icon-gallery"></div>';
                        $type_list      = "video";
                        if(!isset($choose_media['video']))
                        {
                            $choose_media['video'] = '<li data-filter="video" data-text="'.$tr_string['video'].'" data-key="type">'.$tr_string['video'].'</li>';
                        }
                    }

                    if(!empty($photo360))
                    {
                        $class_icon    = '<div class="photo360 icon-gallery"></div>';
                        $type_list     = "photo-360";
                        $link_fancybox = $photo360;
                        $title_image   = "";
                        if(!isset($choose_media['photo_360']))
                        {
                            $choose_media['photo_360'] = '<li data-filter="photo-360" data-text="'.$tr_string['photo_360'].'" data-key="type">'.$tr_string['photo_360'].'</li>';
                        }
                    }

                    if($type != "all")
                    {
                        if($type == "photo")
                        {
                            if(empty($youtube_url) && empty($photo360))
                            {
                                $list_gallery[]   = '
                                    <a href="'.$link_fancybox.'" rel="fancy-group" class="fancy-group" data-caption="'.$title_gallery.'" data-fancybox="images">
                                        <div class="container list-item clearfix">
                                            <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                            <div class="overlay"></div>
                                            '.$class_icon.'
                                            '.$title_image.'
                                        </div>
                                    </a>
                                ';
                            }
                        }
                        else if($type == "photo-360")
                        {
                            if(!empty($photo360))
                            {
                                $list_gallery[]   = '
                                    <a href="'.$link_fancybox.'" target="_blank" data-caption="'.$title_gallery.'">
                                        <div class="container list-item clearfix">
                                            <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                            <div class="overlay"></div>
                                            '.$class_icon.'
                                            '.$title_image.'
                                        </div>
                                    </a>
                                ';
                            }
                        }
                        else if($type == "video")
                        {
                            if(!empty($youtube_url))
                            {
                                $list_gallery[]   = '
                                    <a href="'.$link_fancybox.'" rel="fancy-group" class="fancy-group" data-caption="'.$title_gallery.'" data-fancybox="images">
                                        <div class="container list-item clearfix">
                                            <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                            <div class="overlay"></div>
                                            '.$class_icon.'
                                            '.$title_image.'
                                        </div>
                                    </a>
                                ';
                            }
                        }
                    }
                    else
                    {
                        if($type_list == "photo-360")
                        {
                            $html_list_gallery = '
                            <a href="'.$link_fancybox.'" target="_blank" data-caption="'.$title_gallery.'">
                                <div class="container list-item clearfix">
                                    <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                    <div class="overlay"></div>
                                    '.$class_icon.'
                                    '.$title_image.'
                                </div>
                            </a>
                            ';
                        }
                        else
                        {
                            $html_list_gallery = '
                            <a href="'.$link_fancybox.'" rel="fancy-group" class="fancy-group" data-caption="'.$title_gallery.'" data-fancybox="images">
                                <div class="container list-item clearfix">
                                    <div class="element container-image lazy" data-src="'.$featured_image.'"></div>
                                    <div class="overlay"></div>
                                    <div class="loader"></div>
                                    '.$class_icon.'
                                    '.$title_image.'
                                </div>
                            </a>
                            ';
                        }
                        $list_gallery[] = $html_list_gallery;
                    }
                }
            }

        }

        // CONFIGURE CHOOSE MEDIA
        $choose_media_html = "";
        foreach($choose_media as $key=>$value)
        {
            $choose_media_html .= $value;
        }
        add_variable('choose_media_html', $choose_media_html);

        // LIST GALLERY
        foreach($list_gallery as $lg)
        {
            add_variable('list_gallery', $lg);
            parse_template( 'loop-list-block', 'llblock', true );
        }


        $add_url = "";
        if(!empty($type) && !empty($category))
        {
            $add_url = "&type=$type&category=$category";
        }

        add_variable('paging', getPaginationString($page, $nallgallery, $viewed, $plimit, $page_url, "?page=", $add_url));
    }

    // INCLUDE JS
    add_actions( 'include-js', 'get_custom_javascript', 'https://unpkg.com/packery@2/dist/packery.pkgd.min.js'.$version, 'defer' );
    add_actions( 'include-js', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/jquery.fancybox.min.js'.$version, 'defer' );
    add_actions( 'include-js', 'get_custom_javascript', 'https://unpkg.com/infinite-scroll@3/dist/infinite-scroll.pkgd.min.js'.$version, 'defer' );
    add_actions( 'include-css', 'get_custom_css', HTTP.TEMPLATE_URL.'/assets/css/jquery.fancybox.min.css'.$version );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE CONTACT CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function contact_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/contact.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    $dest_id            = $corporatedata[0]['id'];
    $cordinate_location = get_additional_field( $dest_id, 'coordinate_location', 'destinations' );
    $address            = get_additional_field( $dest_id, 'address', 'destinations' );
    $phone_number       = get_additional_field( $dest_id, 'phone_number', 'destinations' );
    $email_address      = get_additional_field( $dest_id, 'email_address', 'destinations' );
    $social_media_html  = $corporatedata[0]['social_media']['social_media_default'];
    $captcha_site_key   = get_meta_data( 'r_public_key', 'static_setting' );

    add_actions( 'include-js-contact', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/maps_contact.min.js'.$version , 'async defer' );
    add_actions( 'include-js-contact', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U&callback=initialize', 'async defer' );
    add_actions( 'include-css', 'get_custom_css', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.min.css'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js'.$version );

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/contact-us/' : HTTP . site_url().'/'.$destination.'/contact-us/');
    config_meta_data($page_url, $data_page);

    $latitude  = "";
    $longitude = "";
    $country_option = get_data_country();

    if(!empty($cordinate_location))
    {
        $cordinate = explode(",", $cordinate_location);
        $latitude  = $cordinate[0];
        $longitude = $cordinate[1];
    }

    $maps_icon = HTTP.TEMPLATE_URL.'/assets/images/maps_icon.png';

    add_variable('latitude', $latitude);
    add_variable('longitude', $longitude);
    add_variable('maps_icon', $maps_icon);
    add_variable('destination', $destination);
    add_variable('title_dest', "The Samaya ".ucwords($destination));
    add_variable('address', $address);
    add_variable('phone_number', $phone_number);
    add_variable('email_address', $email_address);
    add_variable('social_media_html', $social_media_html);
    add_variable('captcha_site_key', $captcha_site_key);
    add_variable( 'country_option', $country_option );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE LOCATION CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function location_content($destination, $corporatedata, $lang='', $check_lang=false)
{
    global $db;
    $version = attemp_actions('version_js_css');

    set_template( TEMPLATE_PATH . '/template/location.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );

    add_actions( 'include-js', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/scrollbar/jquery.scrollbar.min.js'.$version, 'async defer' );
    add_actions( 'include-js', 'get_custom_javascript', HTTP.TEMPLATE_URL.'/assets/js/locations.min.js'.$version );
    add_actions( 'include-js', 'get_custom_javascript', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDFlVTwZqnOJhc3LsnE9pKHdgajhbB9S2U&callback=maps_locations_page', 'async defer' );


    $dest_id            = $corporatedata[0]['id'];
    $social_media_html  = $corporatedata[0]['social_media']['social_media_default'];

    $cordinate_location = get_additional_field( $dest_id, 'coordinate_location', 'destinations' );
    $address            = get_additional_field( $dest_id, 'address', 'destinations' );
    $phone_number       = get_additional_field( $dest_id, 'phone_number', 'destinations' );
    $email_address      = get_additional_field( $dest_id, 'email_address', 'destinations' );
    $title_page         = get_additional_field( $dest_id, 'location_title', 'destinations' );
    $desc_page          = get_additional_field( $dest_id, 'location_text', 'destinations' );
    $meta_title         = get_additional_field( $dest_id, 'location_meta_title', 'destinations' );
    $meta_keywords      = get_additional_field( $dest_id, 'location_meta_keywords', 'destinations' );
    $meta_description   = get_additional_field( $dest_id, 'location_meta_description', 'destinations' );

    // CHANGE LANGUAGE DATA
    if($check_lang)
    {
        $title_page_lang       = get_additional_field( $dest_id, 'location_title_'.$lang, 'destinations' );
        $desc_page_lang        = get_additional_field( $dest_id, 'location_text_'.$lang, 'destinations' );
        $meta_title_lang       = get_additional_field( $dest_id, 'location_meta_title_'.$lang, 'destinations' );
        $meta_keywords_lang    = get_additional_field( $dest_id, 'location_meta_keywords_'.$lang, 'destinations' );
        $meta_description_lang = get_additional_field( $dest_id, 'location_meta_description_'.$lang, 'destinations' );

        $title_page       = (empty($title_page_lang) ? $title_page:             $title_page_lang);
        $desc_page        = (empty($desc_page_lang) ? $desc_page:               $desc_page_lang);
        $meta_title       = (empty($meta_title_lang) ? $meta_title:             $meta_title_lang);
        $meta_keywords    = (empty($meta_keywords_lang) ? $meta_keywords:       $meta_keywords_lang);
        $meta_description = (empty($meta_description_lang) ? $meta_description:    $meta_description_lang);
    }

    $latitude  = "";
    $longitude = "";
    if(!empty($cordinate_location))
    {
        $cordinate = explode(",", $cordinate_location);
        $latitude  = $cordinate[0];
        $longitude = $cordinate[1];
    }

    $maps_icon = HTTP.TEMPLATE_URL.'/assets/images/marker_location_'.$destination.'_new.png';
    $data_page = array();

    $data_page['title']       = $title_page;
    $data_page['description'] = $desc_page;
    $data_page['metatitle']   = $meta_title;
    $data_page['metakey']     = $meta_keywords;
    $data_page['metadesc']    = $meta_description;
    $data_page['bg_hero']     = "";

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/locations/' : HTTP . site_url().'/'.$destination.'/locations/');
    config_meta_data($page_url, $data_page);

    add_variable('title_page', $title_page);
    add_variable('desc_page', $desc_page);
    add_variable('latitude', $latitude);
    add_variable('longitude', $longitude);
    add_variable('maps_icon', $maps_icon);
    add_variable('destination', $destination);
    add_variable('title_dest', "The Samaya ".ucwords($destination));
    add_variable('address', $address);
    add_variable('phone_number', $phone_number);
    add_variable('email_address', $email_address);
    add_variable('social_media_html', $social_media_html);
    add_variable( 'HTTP', HTTP );
    add_variable( 'site_url', site_url() );

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE STATIC CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
function static_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
{
    global $db;

    set_template( TEMPLATE_PATH . '/template/static_page.html', 'page_template' );
    add_block( 'page-block', 'pblock', 'page_template' );
    // print_r($data_page);
    $description_dest = $data_page['description'];
    // $description_dest = '';
    add_variable('description_dest', $description_dest);

    $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/about-us/' : HTTP . site_url().'/'.$destination.'/about-us/');
    config_meta_data($page_url, $data_page);

    parse_template( 'page-block', 'pblock', false );

    return return_template( 'page_template' );
}


/*
| -------------------------------------------------------------------------------------------------------------------------
| FUNCTION UNTUK PAGE TEMPLATE ABOUT CONTENT
| -------------------------------------------------------------------------------------------------------------------------
*/
// function about_content($destination, $corporatedata, $data_page, $lang='', $check_lang=false)
// {
//     global $db;
//
//     set_template( TEMPLATE_PATH . '/template/about.html', 'page_template' );
//     add_block( 'loop-dest-block', 'ldblock', 'page_template' );
//     add_block( 'page-block', 'pblock', 'page_template' );
//
//     $description_dest = $corporatedata[0]['desc'];
//     add_variable('description_dest', $description_dest);
//
//     $page_url = (($check_lang) ? HTTP . site_url().'/'.$lang.'/'.$destination.'/about-us/' : HTTP . site_url().'/'.$destination.'/about-us/');
//     config_meta_data($page_url, $data_page);
//
//     // GET DESTINATION LIST
//     $destination_list     = get_post_list('destinations', 6, '');
//     if(!empty($destination_list))
//     {
//         foreach($destination_list as $d)
//         {
//             $post_sef = $d['post_sef'];
//
//             if($post_sef == $destination)
//             {
//                 add_variable('title_dest', $d['post_title']);
//                 add_variable('content_dest', $d['post_content']);
//
//                 $gallery_image = $d['post_attachment'];
//                 $img_list      = "";
//                 if(!empty($gallery_image))
//                 {
//                     foreach($gallery_image as $d)
//                     {
//                         $img_medium = $d['img_medium'];
//                         $img_list .= '<img class="lazy" data-src="'.$img_medium.'" />';
//                     }
//                 }
//                 add_variable('img_list', $img_list);
//
//                 parse_template( 'loop-dest-block', 'ldblock', true );
//             }
//         }
//     }
//
//     // GET ACCOLADES LIST
//     // $accolades_html     = "";
//     // $qacco = $db->prepare_query("
//     //     SELECT larticle_id
//     //     FROM lumonata_articles
//     //     WHERE larticle_type=%s AND larticle_status=%s
//     //     ORDER BY lorder ASC
//     // ",
//     //     "accolades", "publish"
//     // );
//     // $racco = $db->do_query($qacco);
//     // $nacco = $db->num_rows($racco);
//
//     // $accolades_list     = get_post_list('accolades', 6, '', $destination);
//     // if(!empty($accolades_list))
//     // {
//     //     foreach($accolades_list as $dacco)
//     //     {
//     //         $featured_img = get_featured_img( $dacco['post_id'], 'accolades', false );
//     //         $accolades_html .= '<img src="'.HTTP.site_url().$featured_img['medium'].'" />';
//     //     }
//     // }
//
//     // add_variable('accolades_html', $accolades_html);
//
//     parse_template( 'page-block', 'pblock', false );
//
//     return return_template( 'page_template' );
// }
//
//

/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK AJAX CONTACT FORM INQUIRY
| -----------------------------------------------------------------------------
*/
function contact_form_inquiry($post_data)
{
    if (isset($post_data['recaptcha']) && !empty($post_data['recaptcha']))
    {

        $params             = array();
        $params['remoteip'] = $_SERVER['REMOTE_ADDR'];
        $params['secret']   = get_meta_data('r_secret_key', 'static_setting');
        $params['response'] = (!empty($post_data['recaptcha']) ? urlencode($post_data['recaptcha']) : '');

        $prm_string = http_build_query($params);
        $requestURL = 'https://www.google.com/recaptcha/api/siteverify?' . $prm_string;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $requestURL,
            CURLOPT_SSL_VERIFYPEER => false
        ));

        $data = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($data);

        if ($response->success)
        {
            $fullname = ucwords($post_data['name']);
            $email    = $post_data['email'];
            $country  = $post_data['country'];
            $phone    = $post_data['phone'];
            $find_us  = $post_data['find_us'];

            $subject     = "Contact Form - Samaya - $fullname";
            $destination = $post_data['destination'];
            $dest_id     = get_id_article_by_sef($destination, 'destinations');
            $cont_email  = get_additional_field($dest_id, 'email_address', 'destinations');
            $web_name    = get_meta_data( 'web_title', 'static_setting' );

            empty( $message ) ? $message = "-" : $message = $message;

            $msg_body        = '
                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="content" style="padding-left: 40px;padding-right: 40px;padding-top: 40px;padding-bottom: 20px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;font-size: 13px;color: #666;">
                                    You have a new contact message from the website. Please check the detail below for your reference.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Full Name
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$fullname.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Country
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$country.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Phone
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$phone.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    Email
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$email.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td align="center" valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="detail" style="padding-left: 40px;padding-right: 40px;padding-bottom: 30px;">
                            <tr>
                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 170px;line-height: 1.38;font-size: 13px;color: #666;">
                                    How did you find us?
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;width: 20px;line-height: 1.38;font-size: 13px;color: #666;">
                                    :
                                </td>

                                <td align="left" valign="top" style="font-family: \'sans-serif\', sans-serif;font-weight: 400;line-height: 1.38;font-size: 13px;color: #666;">
                                    '.$find_us.'
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            ';

            $send_email = send_email($email, $fullname, $cont_email, $web_name, $subject, $msg_body);

            if($send_email)
            {
                $result['status']   = 'success';
                $result['message']  = '<h3><b>Success!</b> Your inquiry has been sent.</h3>';
            }
            else
            {
                $result['status']   = 'error';
                $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
            }
        }
        else
        {
            $result['status']   = 'error';
            $result['message']  = '<h3><b>Danger!</b> Sorry your message cannot be send, please try again later.</h3>';
        }
    }
    else
    {
        $result['status']   = 'warning';
        $result['message']  = '<h3><b>Warning!</b> Please check the reCaptcha.</h3>';
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NAMA SPA BERDASARKAN ID
| -----------------------------------------------------------------------------
*/
function get_name_spa_by_id($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT larticle_title
        FROM lumonata_articles
        WHERE larticle_id=%d AND larticle_type=%s
    ", $id, "spa");
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);
        return $d['larticle_title'];
    }
    return '';
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NAMA CATEGORY TREATMENT BERDASARKAN ID
| -----------------------------------------------------------------------------
*/
function get_name_treatment_category_by_id($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT lctreatment_name
        FROM lumonata_treatment_category
        WHERE lctreatment_id=%d
    ", $id);
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);
        return $d['lctreatment_name'];
    }

    return '';
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN NAMA TREATMENT BERDASARKAN ID
| -----------------------------------------------------------------------------
*/
function get_name_treatment_by_id($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT ltreatment_name
        FROM lumonata_treatment
        WHERE ltreatment_id=%d
    ", $id);
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $d = $db->fetch_array($r);
        return $d['ltreatment_name'];
    }

    return '';
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN TREATMENT CATEGORY BERDASARKAN SPA ID
| -----------------------------------------------------------------------------
*/
function get_treatment_category_list($id, $lang='', $check_lang=false)
{
    $string_translations = get_string_translation_lang();
    $translations = array('start from');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    global $db;

    $q = $db->prepare_query("
        SELECT lctreatment_id, lctreatment_name, lctreatment_desc
        FROM lumonata_treatment_category
        WHERE larticle_id=%d
    ",
        $id
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $result['html'] = "";

    if($n > 0)
    {
        $option_category = '';

        $i               = 0;
        $cat_id          = "";

        while($d = $db->fetch_array($r))
        {
            // $category_id   = $d['lctreatment_id'];
            $category_name = ucwords(strtoupper($d['lctreatment_name']));
            // print_r($d);
            $li_desc         = '';
            if(!empty($d['lctreatment_desc'])) {
              $li_desc ='
              <li>
                '.$d['lctreatment_desc'].'
              </li>
              ';
            }
            $option_category .= '
            <div class="con-mn">
              <div class="lhead flex">
                <h3>'.$category_name.'</h3>
                <p class="spa-pr">
                  '.$tr_string['start_from'].'
                  <strong>IDR 280.000</strong>
                </p>
              </div>
              <div class="roar">
                <ul>

                    '.
                    $li_desc.
                    get_treatment_list($d['lctreatment_id'], $lang, $check_lang).'
                </ul>
              </div>

            </div>
            '
            ;

            // if($i == 0)
            // {
            //     $cat_id = $category_id;
            // }

            $i++;
        }

        $result['html'] = $option_category;
        // $result['category_id'] = $cat_id;
    }

    return $result;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN TREATMENT LIST BERDASARKAN TREATMENT CATEGORY ID
| -----------------------------------------------------------------------------
*/
function get_treatment_list($cat_id, $lang='', $check_lang=false)
{
    $string_translations = get_string_translation_lang();
    $translations = array('Select Treatment Category', 'Select Treatment List');
    $tr_string    = set_string_language($translations, $lang, $check_lang, $string_translations);

    global $db;

    $q = $db->prepare_query("
        SELECT ltreatment_id, ltreatment_name, ltreatment_price
        FROM lumonata_treatment
        WHERE lctreatment_id=%d
    ",
        $cat_id
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);

    $option_treatment = '';
    // $result['html'] = "";

    if($n > 0)
    {
        $i     = 0;
        $price = 0;
        // $tax   = 0;

        while($d = $db->fetch_array($r))
        {
            $treatment_id       = $d['ltreatment_id'];
            $treatment_name     = ucwords(strtoupper($d['ltreatment_name']));
            $treatment_price    = $d['ltreatment_price'];

            $option_treatment   .= '
            <li class="flex">
              <p class="ii flex">
                <span>'.$treatment_name.'</span>
                <span>IDR '.$treatment_price.' </span>
              </p>
              <a class="libtn" treat_id="'.$treatment_id.'">BOOK NOW</a>
            </li>
            ';


            $i++;
        }

        // $result['html']  = $option_treatment;
        // $result['price'] = $price;
        // $result['tax']   = $tax;
    }

    return $option_treatment;
}


/*
| -----------------------------------------------------------------------------
| FUNCTION UNTUK MENDAPATKAN TREATMENT PRICE & TAX BERDASARKAN TREATMENT LIST ID
| -----------------------------------------------------------------------------
*/
function get_treatment_price_tax($id)
{
    global $db;

    $q = $db->prepare_query("
        SELECT ltreatment_id, ltreatment_name, ltreatment_price, ltreatment_tax
        FROM lumonata_treatment
        WHERE ltreatment_id=%d
    ",
        $id
    );
    $r = $db->do_query($q);
    $n = $db->num_rows($r);


    $result['price'] = "";
    $result['tax'] = "";

    if($n > 0)
    {
        $i = 0;
        $treatment_price = 0;
        $treatment_tax = 0;
        while($d = $db->fetch_array($r))
        {
            $treatment_price = $d['ltreatment_price'];
            $treatment_tax   = $d['ltreatment_tax'];
            $treatment_tax = (($treatment_tax != 0) ? (($treatment_price * ($treatment_tax/100))) : 0 );
        }

        $result['price'] = $treatment_price;
        $result['tax']   = $treatment_tax;
    }

    return $result;
}
?>
