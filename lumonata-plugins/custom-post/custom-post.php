<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
Plugin Name: Lumonata Custom Application
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding custom post type
Author: Ngurah Rai
Author URL: http://lumonata.com/
Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

$ssl_option = get_meta_data('ssl_option');
$http       = $ssl_option==1?'https://':'http://';

//-- Add Actions
add_actions( 'header_elements', 'get_custom_css', $http . site_url() . '/lumonata-plugins/custom-post/css/menu.css' );
add_actions( 'post-ajax_page', 'execute_post_type_ajax_request' );

/*
| -------------------------------------------------------------------------------------------------------------------------
Fungsi ini digunakan untuk membuat custom post type dan menampilkannya ke dalam list menu
| -------------------------------------------------------------------------------------------------------------------------
*/
$apps_array = array(
    'special-offers'     => 'Offers',
    'accommodation'      => 'Accommodation',
    'experience'         => 'Experience (Why Us)',
    'what-to-do-around'  => 'Interesting Spot',
    'tripadvisor-review' => 'Reviews',
    'dining'             => 'Dining',
    'spa'                => 'Spa',
    'activities'         => 'Activities',
    'gallery'            => 'Gallery',
    'facilities'         => 'Facilities',

    // 'events'             => 'Events',
    // 'weddings'           => 'Weddings',
    // 'news'               => 'Press',
    // 'what-to-do-act-in'  => 'What To Do Act In',

    // 'accolades'         => 'Accolades'
);

foreach( $apps_array as $key => $apps )
{
    add_privileges( 'administrator', $key, 'insert' );
    add_privileges( 'administrator', $key, 'update' );
    add_privileges( 'administrator', $key, 'delete' );

    if( is_array( $apps ) )
    {
        add_main_menu( array( $key => $apps['title'] ) );
        add_sub_menu( $key, $apps['submenu'] );

        foreach( $apps['submenu'] as $idx => $sub )
        {
            //tambah tab setting
            $tab_arr = array( 'blogs' => $apps, 'setting' => 'Setting' );
            $tab_arr = array( 'blogs' => $apps );
            $act_prm = array(
                array(
                    'var' => 'header_elements',
                    'function' => 'get_custom_css',
                    'param' =>  $http . site_url() . '/lumonata-plugins/custom-post/css/style.css'
                )
            );

            add_actions( $idx, 'get_admin_article', $idx, $sub . '|' . $sub, $tab_arr, true, false, $act_prm );
            add_actions( 'plugin_menu_set', '<option value="'.$idx.'">'.$sub.'</option>' );
            add_actions( 'setting_article_extends', 'set_setting_form', $tab_arr );
        }
    }
    else
    {
        if( $key == 'accommodation' )
        {
            $tab_arr = array( 'blogs' => $apps, 'amenities' => 'Amenities', 'setting' => 'Setting', 'type' => 'accommodation');

            add_actions( 'amenities_article_extends', 'set_amenities_data', $tab_arr );
        }
        elseif( $key == 'events' || $key == 'weddings' )
        {
            if($key == "events")
            {
                $tab_arr = array( 'blogs' => $apps, 'amenities' => 'Amenities', 'terms_conditions' => 'Terms & Conditions', 'setting' => 'Setting', 'type' => 'events');
            }
            else
            {
                $tab_arr = array( 'blogs' => $apps, 'amenities' => 'Amenities', 'terms_conditions' => 'Terms & Conditions', 'occasion_type' => 'Occassion Type', 'special_request' => 'Special Request', 'setting' => 'Setting', 'type' => 'weddings');


                if($tab_arr['occasion_type'] == "Occassion Type")
                {
                    add_actions( 'occasion_type_article_extends', 'set_occasion_data', $tab_arr );
                }

                if($tab_arr['special_request'] == "Special Request")
                {
                    add_actions( 'special_request_article_extends', 'set_special_request_data', $tab_arr );
                }
            }

            if($tab_arr['amenities'] == "Amenities")
            {
                add_actions( 'amenities_article_extends', 'set_amenities_data', $tab_arr );
            }
            if($tab_arr['terms_conditions'] == "Terms & Conditions")
            {
                add_actions( 'terms_conditions_article_extends', 'set_terms_data', $tab_arr );
            }
        }
        elseif( $key == 'special-offers' )
        {
            $tab_arr = array( 'blogs' => $apps, 'additional_value' => 'Additional Value Add', 'terms_conditions' => 'Terms & Conditions', 'setting' => 'Setting', 'type' => 'special-offers');

            if($tab_arr['additional_value'] == "Additional Value Add")
            {
                add_actions( 'additional_value_article_extends', 'set_amenities_data', $tab_arr );
            }
            if($tab_arr['terms_conditions'] == "Terms & Conditions")
            {
                add_actions( 'terms_conditions_article_extends', 'set_terms_data', $tab_arr );
            }
        }
        else
        {
            $tab_arr = array( 'blogs' => $apps, 'setting' => 'Setting' );
            // $tab_arr = array( 'blogs' => $apps );
        }

        if( $key == 'news' )
        {
            add_actions( $key . '_post_date', $key . '_post_date_view' );
        }

        // if( $key == 'dining' )
        // {
        //     add_actions( $key . '_post_date', $key . '_post_date_view' );
        //     $tab_arr = array( 'blogs' => $apps);
        // }

        // if($key == 'experience'){
        //     add_actions( $key . '_post_date', $key . '_post_date_view' );
        // }
        // $tab_arr = array( 'blogs' => $apps, 'setting' => 'Setting');


        $act_prm = array(
            array(
                'var' => 'header_elements',
                'function' => 'get_custom_css',
                'param' => $http . site_url() . '/lumonata-plugins/custom-post/css/style.css'
            )
        );

        add_main_menu( array( $key => $apps ) );

        add_actions( $key, 'get_admin_article', $key, $apps . '|' . $apps, $tab_arr, true, true, $act_prm );
        add_actions( 'plugin_menu_set', '<option value="'.$key.'">'.$apps.'</option>' );
        add_actions( 'setting_article_extends', 'set_setting_form', $tab_arr );
    }
}

add_actions( 'plugin_menu_post_type', 'set_plugin_menu_post_type', $apps_array );

function set_plugin_menu_post_type( $apps=array() )
{
    return $apps;
}

function news_post_date_view()
{
    return 'news-post-date';
}

function events_post_date_view()
{
    return 'events-post-date';
}

function set_setting_form( $tabs )
{
    global $db;

    $ssl_option = get_meta_data('ssl_option');
    $http       = $ssl_option==1?'https://':'http://';

    $app_url   = isset( $_GET['sub'] ) ? $_GET['state'] . '&sub=' .$_GET['sub'] : $_GET['state'];
    $app_name  = isset( $_GET['sub'] ) ? $_GET['sub'] : $_GET['state'];
    $app_title = is_array( $tabs['blogs'] ) ? $tabs['blogs']['title'] : $tabs['blogs'];
    $message   = save_setting_data( $app_name, $app_title );

    $button = '
	<li>' . button( 'button=save_changes&label=Save' ) . '</li>
    <li>' . button( 'button=cancel', get_state_url( $app_url . '&tab=blogs' ) ) . '</li>';


    set_template( PLUGINS_PATH . '/custom-post/setting-form.html', 'setting_form_template' );
    add_block( 'setting_form_destination', 'set_form_des', 'setting_form_template' );
    add_block( 'setting_form', 'set_form', 'setting_form_template' );

    add_variable( 'language_button', attemp_actions( 'page_post_language_button' ) );

    add_variable( 'app_title', $app_title . ' Setting' );
    add_variable( 'tab', set_tabs( $tabs, 'setting' ) );
    add_variable( 'button', $button );
    add_variable( 'message', $message );
    add_variable( 'app_name', $app_name );

    if(check_active_plugin_destination()){

        // get data destination
        $qdes = $db->prepare_query("SELECT larticle_id, larticle_title, lsef FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title ASC", "destinations", "publish");
        $rdes = $db->do_query($qdes);

        while($ddes = $db->fetch_array($rdes)){
            $dest_title = ucwords($ddes['larticle_title']);
            $dest_id    = $ddes['larticle_id'];
            $array_name = "[$dest_id]";
            $sef_des    = $ddes['lsef'];

            add_variable( 'dest_title', $dest_title );
            add_variable( 'dest_id', $dest_id );
            add_variable( 'array_name', $array_name );

            $d = get_costom_post_setting( $app_name, '', $dest_id );

            add_variable( 'title', $d['title'] );
            add_variable( 'subtitle', $d['subtitle'] );
            add_variable( 'bg_image', $d['bg_image_small'] );
            add_variable( 'bg_dtimage', $d['bg_dtimage_small'] );
            add_variable( 'description', $d['description'] );
            add_variable( 'brief', $d['brief'] );
            add_variable( 'meta_title', $d['meta_title'] );
            add_variable( 'meta_keywords', $d['meta_keywords'] );
            add_variable( 'meta_description', $d['meta_description'] );
            add_variable( 'class_dest_id', "-".$dest_id );
            add_variable( 'sef_des', $sef_des );



            $content_add = "";
            if($app_name == "spa")
            {
                $text_additional_book = $d['text_additional_book'];
                $pdf_file_spa_menu    = $d['pdf_file_spa_menu'];

                $content_add = '
                    <fieldset>
                        <p>Text Additional Booking Popup</p>
                        <textarea class="textarea tinymce" name="text_additional_book'.$array_name.'" autocomplete="off">'.$text_additional_book.'</textarea>
                    </fieldset>
                ';

                if(!empty($pdf_file_spa_menu) && file_exists(PLUGINS_PATH . '/custom-post/pdf/'.$pdf_file_spa_menu))
                {
                    $content_add .= '
                    <fieldset>
                        <p style="margin-bottom: 0px;">Current PDF File Spa Menu</p>
                        <a href="'.URL_PLUGINS . 'custom-post/pdf/' . $pdf_file_spa_menu.'" target="_blank">'.$pdf_file_spa_menu.'</a>
                    </fieldset>
                    ';
                }

                $content_add .= '
                    <fieldset>
                        <p>PDF File Spa Menu</p>
                        <input type="file" class="pdf-upload-'.$dest_id.'" name="pdf_file_spa_menu'.$array_name.'" data-id="'.$dest_id.'" autocomplete="off" />
                    </fieldset>
                ';

            }
            else if($app_name == "dining")
            {
                // $code_rest_diary    = $d['code_rest_diary'];
                // $content_add = '
                //     <fieldset>
                //         <p>Code Rest Diary</p>
                //         <textarea class="textarea" rows="10" name="code_rest_diary'.$array_name.'" autocomplete="off">'.$code_rest_diary.'</textarea>
                //     </fieldset>
                // ';
            }

            add_variable( 'content_add', $content_add );


            // if(isset($_GET['state']) == "events")
            // {
            //     $galery_image = get_gallery_image_content('','','', '-'.$sef_des);
            //     add_variable( 'galery_image_setting', $galery_image );
            // }

            parse_template( 'setting_form_destination', 'set_form_des', true );
        }

    }else{
        $d = get_costom_post_setting( $app_name );

        add_variable( 'title', $d['title'] );
        add_variable( 'subtitle', $d['subtitle'] );
        add_variable( 'bg_image', $d['bg_image_small'] );
        add_variable( 'bg_dtimage', $d['bg_dtimage_small'] );
        add_variable( 'description', $d['description'] );
        add_variable( 'brief', $d['brief'] );
        add_variable( 'meta_title', $d['meta_title'] );
        add_variable( 'meta_keywords', $d['meta_keywords'] );
        add_variable( 'meta_description', $d['meta_description'] );

        parse_template( 'setting_form_destination', 'set_form_des', false );

    }

    add_variable( 'language_data', attemp_actions( 'post_type_setting_language_data' ) );

    add_variable( 'site_url', site_url() );
    add_variable( 'HTTP', $http );
    add_variable( 'pdf_file', get_costom_post_pdf_file( $app_name ) );

    add_actions( 'section_title', $app_title );

    parse_template( 'setting_form', 'set_form', false );

    return return_template( 'setting_form_template' );
}

function get_post_type_setting($dest_id, $type, $key='')
{
	$empty = array('title'=>'','description'=>'','background'=>'');
	$value = get_meta_data('post_type_setting', $type, $dest_id);

	if(empty($value)) return $empty;

	$value = json_decode($value,true);
	if($value === null && json_last_error() !== JSON_ERROR_NONE)
	{
	    return $empty;
	}
	else
	{
		return $value;
	}
}

function set_amenities_data( $tabs )
{
    if($tabs['type'] == 'accommodation')
    {
        return get_admin_rule( 'amenities', 'accommodation', 'Accommodation', $tabs );
    }
    elseif($tabs['type'] == 'events')
    {
        return get_admin_rule( 'amenities', 'events', 'Events', $tabs );
    }
    elseif($tabs['type'] == 'weddings')
    {
        return get_admin_rule( 'amenities', 'weddings', 'Weddings', $tabs );
    }
    elseif($tabs['type'] == 'special-offers')
    {
        return get_admin_rule( 'additional_value', 'special-offers', 'Special offers', $tabs );
    }
}


function set_terms_data($tabs)
{
    if($tabs['type'] == 'special-offers')
    {
        return get_admin_rule( 'terms_conditions', 'special-offers', 'Special offers', $tabs );
    }
    elseif($tabs['type'] == 'events')
    {
        return get_admin_rule( 'terms_conditions', 'events', 'Events', $tabs );
    }
    elseif($tabs['type'] == 'weddings')
    {
        return get_admin_rule( 'terms_conditions', 'weddings', 'Weddings', $tabs );
    }
}

function set_occasion_data($tabs)
{
    if($tabs['type'] == 'weddings')
    {
        return get_admin_rule( 'occasion_type', 'weddings', 'Weddings', $tabs );
    }
}

function set_special_request_data($tabs)
{
    if($tabs['type'] == 'weddings')
    {
        return get_admin_rule( 'special_request', 'weddings', 'Weddings', $tabs );
    }
}

function save_setting_data( $app_name, $app_title )
{
    $message = '';

    if( is_save_changes() )
    {
        // print_r($_POST['all_lang']);
        // exit();
        // print_r($_POST['title']);
        $success = 0;
        if(is_array($_POST['title'])){
            foreach($_POST['title'] as $index => $val){
                $bg_image = set_custom_post_bg_image( $app_name, $index );
                $bg_dtimage = set_custom_post_bg_dtimage( $app_name, $index );
                $pdf_file_spa_menu = set_custom_pdf_file_spa_menu($app_name, $index);
                // $bg_image = "";
                $meta_value = array(
                    'bg_image'             => $bg_image,
                    'bg_dtimage'             => $bg_dtimage,
                    'pdf_file_spa_menu'    => $pdf_file_spa_menu,
                    'title'                => $_POST['title'][$index],
                    'subtitle'             => $_POST['subtitle'][$index],
                    'description'          => $_POST['description'][$index],
                    'brief'                => $_POST['brief'][$index],
                    'text_additional_book' => (isset($_POST['text_additional_book'][$index]) ? $_POST['text_additional_book'][$index]: ''),
                    'code_rest_diary'      => (isset($_POST['code_rest_diary'][$index]) ? $_POST['code_rest_diary'][$index]: ''),
                    'meta_title'           => $_POST['meta_title'][$index],
                    'meta_keywords'        => $_POST['meta_keywords'][$index],
                    'meta_description'     => $_POST['meta_description'][$index]
                );

                if(isset($_POST['all_lang']) && is_array($_POST['all_lang'])){
                    foreach($_POST['all_lang'] as $lang){
                        $pdf_file_spa_menu = set_custom_pdf_file_spa_menu($app_name, $index, $lang);

                        $meta_value += array(
                            'title_'.$lang                => $_POST['title_'.$lang][$index],
                            'subtitle_'.$lang             => $_POST['subtitle_'.$lang][$index],
                            'description_'.$lang          => $_POST['description_'.$lang][$index],
                            'brief_'.$lang                => $_POST['brief_'.$lang][$index],
                            'text_additional_book_'.$lang => (isset($_POST['text_additional_book_'.$lang][$index]) ? $_POST['text_additional_book_'.$lang][$index]: '' ),
                            'pdf_file_spa_menu_'.$lang    => $pdf_file_spa_menu,
                            'meta_title_'.$lang           => $_POST['meta_title_'.$lang][$index],
                            'meta_description_'.$lang     => $_POST['meta_description_'.$lang][$index],
                            'meta_keywords_'.$lang        => $_POST['meta_keywords_'.$lang][$index]
                        );
                    }
                }

                // print_r($meta_value);
                if(update_meta_data( 'post_type_setting', json_encode( $meta_value ), $app_name, $index)){
                    $success++;
                }
            }
        }else{
            $bg_image   = set_custom_post_bg_image( $app_name );
            $bg_dtimage = set_custom_post_bg_dtimage( $app_name );
            $meta_value = array(
                'bg_image'             => $bg_image,
                'bg_dtimage'             => $bg_dtimage,
                'title'                => $_POST['title'],
                'subtitle'             => $_POST['subtitle'],
                'description'          => $_POST['description'],
                'brief'                => $_POST['brief'],
                'text_additional_book' => $_POST['text_additional_book'],
                'code_rest_diary'      => $_POST['code_rest_diary'],
                'meta_title'           => $_POST['meta_title'],
                'meta_keywords'        => $_POST['meta_keywords'],
                'meta_description'     => $_POST['meta_description']
            );
            if( update_meta_data( 'post_type_setting', json_encode( $meta_value ), $app_name ) )
            {
                $success++;
            }
        }
        // $pdf_menu   = set_custom_post_pdf( $app_name );


        if( $success > 0 )
        {
            $message = '<div class="message success">' . $app_title . ' setting data has been successfully edited.</div>';
        }
        else
        {
            $message = '<div class="message error">Something wrong, please try again.</div>';
        }
    }

    return $message;
}

function get_costom_post_bg_image( $app_name )
{
    $ssl_option = get_meta_data('ssl_option');
    $http       = $ssl_option==1?'https://':'http://';

    $meta = get_meta_data( 'post_type_setting', $app_name );
    $meta = json_decode( $meta, true );

    if( !empty( $meta ) && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $meta['bg_image'] ) && !empty( $meta['bg_image'] ) )
        {
        	return  $http . site_url() . '/lumonata-plugins/custom-post/background/medium/' . $meta['bg_image'];
        }
    }
}

function get_costom_post_pdf_file( $app_name )
{
    $meta = get_meta_data( 'post_type_setting', $app_name );
    $meta = json_decode( $meta, true );
    $pdf  = '';

    if( !empty( $meta ) && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $meta['pdf'] ) && !empty( $meta['pdf'] ) )
        {
            $pdf = '
			<p class="pdf-file">
				' . $meta['pdf'] . '
				<a class="link">X</a>
			</p>';
        }
    }

    return $pdf;
}

function get_costom_post_setting( $type = 'blogs', $field = '', $app_id=0 )
{
    $meta = get_meta_data( 'post_type_setting', $type, $app_id );
    $meta = json_decode( $meta, true );
    $data = array();

    $ssl_option = get_meta_data('ssl_option');
    $http       = $ssl_option==1?'https://':'http://';


    if( !empty( $meta ) && json_last_error() === JSON_ERROR_NONE )
    {
        $m_title = isset( $meta['meta_title'] ) && !empty( $meta['meta_title'] ) ? $meta['meta_title'] : '';
        $m_kwrds = isset( $meta['meta_keywords'] ) && !empty( $meta['meta_keywords'] ) ? $meta['meta_keywords'] : '';
        $m_desc  = isset( $meta['meta_description'] ) && !empty( $meta['meta_description'] ) ? $meta['meta_description'] : '';

        $title   = isset( $meta['title'] ) && !empty( $meta['title'] ) ? $meta['title'] : '';$subtitle   = isset( $meta['subtitle'] ) && !empty( $meta['subtitle'] ) ? $meta['subtitle'] : '';
        $desc    = isset( $meta['description'] ) && !empty( $meta['description'] ) ? $meta['description'] : '';
        $brief   = isset( $meta['brief'] ) && !empty( $meta['brief'] ) ? $meta['brief'] : '';

        $text_additional_book = "";
        $pdf_file_spa_menu    = "";
        $code_rest_diary      = "";

        if($type == "spa")
        {
            $text_additional_book   = isset( $meta['text_additional_book'] ) && !empty( $meta['text_additional_book'] ) ? $meta['text_additional_book'] : '';
            $pdf_file_spa_menu   = isset( $meta['pdf_file_spa_menu'] ) && !empty( $meta['pdf_file_spa_menu'] ) ? $meta['pdf_file_spa_menu'] : '';
        }
        elseif($type == "dining")
        {
            $code_rest_diary   = isset( $meta['code_rest_diary'] ) && !empty( $meta['code_rest_diary'] ) ? $meta['code_rest_diary'] : '';
        }

        if( isset( $meta['bg_image'] ) && !empty( $meta['bg_image'] ) )
        {
            $small_path  =  PLUGINS_PATH . '/custom-post/background/small';
            $medium_path =  PLUGINS_PATH . '/custom-post/background/medium';
            $large_path  =  PLUGINS_PATH . '/custom-post/background/large';
            $origin_path =  PLUGINS_PATH . '/custom-post/background';

            $bg_image_small  = file_exists( $small_path . '/' . $meta['bg_image'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/small/' . $meta['bg_image'] : '';
            $bg_image_medium = file_exists( $medium_path . '/' . $meta['bg_image'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/medium/' . $meta['bg_image'] : '';
            $bg_image_large  = file_exists( $large_path . '/' . $meta['bg_image'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/large/' . $meta['bg_image'] : '';
            $bg_image_origin = file_exists( $origin_path . '/' . $meta['bg_image'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/' . $meta['bg_image'] : '';
        }
        else
        {
            $bg_image_small  = '';
            $bg_image_medium = '';
            $bg_image_large  = '';
            $bg_image_origin = '';
        }

        if( isset( $meta['bg_dtimage'] ) && !empty( $meta['bg_dtimage'] ) )
        {
            $small_path  =  PLUGINS_PATH . '/custom-post/background/small';
            $medium_path =  PLUGINS_PATH . '/custom-post/background/medium';
            $large_path  =  PLUGINS_PATH . '/custom-post/background/large';
            $origin_path =  PLUGINS_PATH . '/custom-post/background';

            $bg_dtimage_small  = file_exists( $small_path . '/' . $meta['bg_dtimage'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/small/' . $meta['bg_dtimage'] : '';
            $bg_dtimage_medium = file_exists( $medium_path . '/' . $meta['bg_dtimage'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/medium/' . $meta['bg_dtimage'] : '';
            $bg_dtimage_large  = file_exists( $large_path . '/' . $meta['bg_dtimage'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/large/' . $meta['bg_dtimage'] : '';
            $bg_dtimage_origin = file_exists( $origin_path . '/' . $meta['bg_dtimage'] ) ? $http . site_url() . '/lumonata-plugins/custom-post/background/' . $meta['bg_dtimage'] : '';
        }
        else
        {
            $bg_dtimage_small  = '';
            $bg_dtimage_medium = '';
            $bg_dtimage_large  = '';
            $bg_dtimage_origin = '';
        }

        $data   = array(
            'title'                => $title,
            'subtitle'             => $subtitle,
            'description'          => $desc,
            'brief'                => $brief,
            'meta_title'           => $m_title,
            'meta_keywords'        => $m_kwrds,
            'meta_description'     => $m_desc,
            'bg_image'             => $bg_image_origin,
            'bg_image_small'       => $bg_image_small,
            'bg_image_large'       => $bg_image_large,
            'bg_image_medium'      => $bg_image_medium,

            'bg_dtimage'             => $bg_dtimage_origin,
            'bg_dtimage_small'       => $bg_dtimage_small,
            'bg_dtimage_large'       => $bg_dtimage_large,
            'bg_dtimage_medium'      => $bg_dtimage_medium,

            'text_additional_book' => $text_additional_book,
            'pdf_file_spa_menu'    => $pdf_file_spa_menu,
            'code_rest_diary'      => $code_rest_diary
        );
    }
    else
    {
         $data   = array(
            'pdf'                  => '',
            'title'                => '',
            'subtitle'             => '',
            'btitle'               => '',
            'balt'                 => '',
            'bcopy'                => '',
            'brief'                => '',
            'description'          => '',
            'meta_title'           => '',
            'meta_keywords'        => '',
            'meta_description'     => '',
            'bg_image'             => '',
            'bg_image_small'       => '',
            'bg_image_large'       => '',
            'bg_image_medium'      => '',
            'text_additional_book' => '',
            'pdf_file_spa_menu'    => '',
            'code_rest_diary'      => '',
        );
    }

    if( empty( $field ) )
    {
        return $data;
    }
    else
    {
        return $data[$field];
    }
}

function get_post_by_type_list( $type )
{
    global $db;

    $result = array();

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type = %s AND larticle_status = %s ORDER BY lorder';
    $q = $db->prepare_query( $s, $type, 'publish' );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        while( $d = $db->fetch_array( $r ) )
        {
            $result[] = $d;
        }
    }

    return $result;
}

function set_custom_post_bg_image( $apps, $index=0 )
{
    $meta = get_meta_data( 'post_type_setting', $apps, $index );
    $meta = json_decode( $meta, true );

    $bg_image = isset( $meta['bg_image'] ) ? $meta['bg_image'] : '';

    if( $_FILES['bg_image']['error'][$index]==0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/custom-post/background/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/' );
        }

        if( !file_exists( PLUGINS_PATH . '/custom-post/background/large/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/large/' );
        }

        if( !file_exists( PLUGINS_PATH . '/custom-post/background/medium/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/medium/' );
        }

        if( !file_exists( PLUGINS_PATH . '/custom-post/background/small/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/small/' );
        }

        $file_name   = $_FILES['bg_image']['name'][$index];
        $file_size   = $_FILES['bg_image']['size'][$index];
        $file_type   = $_FILES['bg_image']['type'][$index];
        $file_source = $_FILES['bg_image']['tmp_name'][$index];

        $bg_image = '';

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                $fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
                $fix_file_name = $fix_file_name.$file_ext;

                $destination = PLUGINS_PATH . '/custom-post/background/' . $fix_file_name;
                $destination_small = PLUGINS_PATH . '/custom-post/background/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/custom-post/background/large/' . $fix_file_name;
                $destination_medium = PLUGINS_PATH . '/custom-post/background/medium/' . $fix_file_name;

                $is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
                $is_upload = upload( $file_source, $destination );

                if( $is_upload && $is_resize )
                {
                    $bg_image = $fix_file_name;

                    if( isset( $meta['bg_image'] ) && !empty( $meta['bg_image'] ) )
                    {
                        if( file_exists( PLUGINS_PATH . '/custom-post/background/large/' . $meta['bg_image'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/large/' . $meta['bg_image'] );
                        }

                        if( file_exists( PLUGINS_PATH . '/custom-post/background/medium/' . $meta['bg_image'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/medium/' . $meta['bg_image'] );
                        }

                        if( file_exists( PLUGINS_PATH . '/custom-post/background/small/' . $meta['bg_image'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/small/' . $meta['bg_image'] );
                        }

                        if( file_exists( PLUGINS_PATH . '/custom-post/background/' . $meta['bg_image'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/' . $meta['bg_image'] );
                        }
                    }
                }
            }
        }
    }

    return $bg_image;
}

function set_custom_post_bg_dtimage( $apps, $index=0 )
{
    $meta = get_meta_data( 'post_type_setting', $apps, $index );
    $meta = json_decode( $meta, true );

    $bg_image = isset( $meta['bg_dtimage'] ) ? $meta['bg_dtimage'] : '';

    if( $_FILES['bg_dtimage']['error'][$index]==0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/custom-post/background/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/' );
        }

        if( !file_exists( PLUGINS_PATH . '/custom-post/background/large/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/large/' );
        }

        if( !file_exists( PLUGINS_PATH . '/custom-post/background/medium/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/medium/' );
        }

        if( !file_exists( PLUGINS_PATH . '/custom-post/background/small/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/background/small/' );
        }

        $file_name   = $_FILES['bg_dtimage']['name'][$index];
        $file_size   = $_FILES['bg_dtimage']['size'][$index];
        $file_type   = $_FILES['bg_dtimage']['type'][$index];
        $file_source = $_FILES['bg_dtimage']['tmp_name'][$index];

        $bg_image = '';

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'image' ) )
            {
                $fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				        $file_ext      = file_name_filter($file_name,true);
                $fix_file_name = $fix_file_name.$file_ext;

                $destination = PLUGINS_PATH . '/custom-post/background/' . $fix_file_name;
                $destination_small = PLUGINS_PATH . '/custom-post/background/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/custom-post/background/large/' . $fix_file_name;
                $destination_medium = PLUGINS_PATH . '/custom-post/background/medium/' . $fix_file_name;

                $is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
                $is_upload = upload( $file_source, $destination );

                if( $is_upload && $is_resize )
                {
                    $bg_image = $fix_file_name;

                    if( isset( $meta['bg_dtimage'] ) && !empty( $meta['bg_dtimage'] ) )
                    {
                        if( file_exists( PLUGINS_PATH . '/custom-post/background/large/' . $meta['bg_dtimage'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/large/' . $meta['bg_dtimage'] );
                        }

                        if( file_exists( PLUGINS_PATH . '/custom-post/background/medium/' . $meta['bg_dtimage'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/medium/' . $meta['bg_dtimage'] );
                        }

                        if( file_exists( PLUGINS_PATH . '/custom-post/background/small/' . $meta['bg_dtimage'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/small/' . $meta['bg_dtimage'] );
                        }

                        if( file_exists( PLUGINS_PATH . '/custom-post/background/' . $meta['bg_dtimage'] ) )
                        {
                            unlink( PLUGINS_PATH . '/custom-post/background/' . $meta['bg_dtimage'] );
                        }
                    }
                }
            }
        }
    }

    return $bg_image;
}

function set_custom_pdf_file_spa_menu( $apps, $index=0, $lang='' )
{
    $meta = get_meta_data( 'post_type_setting', $apps, $index );
    $meta = json_decode( $meta, true );

    $lang_name = "";
    if(!empty($lang))
    {
        $lang_name = "_".$lang;
    }

    $pdf_file_spa_menu = isset( $meta['pdf_file_spa_menu'.$lang_name] ) ? $meta['pdf_file_spa_menu'.$lang_name] : '';

    if(isset($_FILES['pdf_file_spa_menu'.$lang_name]))
    {
        if( $_FILES['pdf_file_spa_menu'.$lang_name]['error'][$index]==0 )
        {
            //-- CHECK FOLDER EXIST
            if( !file_exists( PLUGINS_PATH . '/custom-post/pdf/' ) )
            {
                mkdir( PLUGINS_PATH . '/custom-post/pdf/' );
            }

            if(!empty($lang))
            {
                if( !file_exists( PLUGINS_PATH . '/custom-post/pdf/'.$lang ) )
                {
                    mkdir( PLUGINS_PATH . '/custom-post/pdf/'.$lang );
                }
            }



            $file_name   = $_FILES['pdf_file_spa_menu'.$lang_name]['name'][$index];
            $file_size   = $_FILES['pdf_file_spa_menu'.$lang_name]['size'][$index];
            $file_type   = $_FILES['pdf_file_spa_menu'.$lang_name]['type'][$index];
            $file_source = $_FILES['pdf_file_spa_menu'.$lang_name]['tmp_name'][$index];

            $pdf_file_spa_menu = '';

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'pdf' ) )
                {
                    $fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
                    $file_ext      = file_name_filter($file_name,true);
                    $fix_file_name = $fix_file_name.$file_ext;

                    if(!empty($lang))
                    {
                        $destination = PLUGINS_PATH . '/custom-post/pdf/'.$lang.'/'.$fix_file_name;
                    }
                    else
                    {
                        $destination = PLUGINS_PATH . '/custom-post/pdf/' . $fix_file_name;
                    }

                    $is_upload = upload( $file_source, $destination );

                    if( $is_upload )
                    {
                        $pdf_file_spa_menu = $fix_file_name;

                        if( isset( $meta['pdf_file_spa_menu'.$lang_name] ) && !empty( $meta['pdf_file_spa_menu'.$lang_name] ) )
                        {
                            if(!empty($lang))
                            {
                                if( file_exists( PLUGINS_PATH . '/custom-post/pdf/'.$lang.'/'.$meta['pdf_file_spa_menu'] ) )
                                {
                                    unlink( PLUGINS_PATH . '/custom-post/pdf/'.$lang.'/'.$meta['pdf_file_spa_menu'] );
                                }
                            }
                            else
                            {
                                if( file_exists( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf_file_spa_menu'] ) )
                                {
                                    unlink( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf_file_spa_menu'] );
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return $pdf_file_spa_menu;
}


function delete_custom_post_bg_image( $app_name = '', $app_id=0 )
{
    $meta = get_meta_data( 'post_type_setting', $app_name, $app_id );
    $meta = json_decode( $meta, true );

    if( !empty( $meta ) && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $meta['bg_image'] ) && !empty( $meta['bg_image'] ) )
        {
        	if( file_exists( PLUGINS_PATH . '/custom-post/background/large/' . $meta['bg_image'] ) )
        	{
            	unlink( PLUGINS_PATH . '/custom-post/background/large/' . $meta['bg_image'] );
        	}

        	if( file_exists( PLUGINS_PATH . '/custom-post/background/medium/' . $meta['bg_image'] ) )
        	{
            	unlink( PLUGINS_PATH . '/custom-post/background/medium/' . $meta['bg_image'] );
        	}

            if( file_exists( PLUGINS_PATH . '/custom-post/background/small/' . $meta['bg_image'] ) )
            {
                unlink( PLUGINS_PATH . '/custom-post/background/small/' . $meta['bg_image'] );
            }

        	if( file_exists( PLUGINS_PATH . '/custom-post/background/' . $meta['bg_image'] ) )
        	{
            	unlink( PLUGINS_PATH . '/custom-post/background/' . $meta['bg_image'] );
        	}

        	$meta['bg_image'] = '';

        	if( update_meta_data( 'post_type_setting', json_encode( $meta ), $app_name, $app_id ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

function set_custom_post_pdf( $apps )
{
    $meta = get_meta_data( 'post_type_setting', $apps );
    $meta = json_decode( $meta, true );

    if( $_FILES['menu_pdf']['error']==0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/custom-post/pdf/' ) )
        {
            mkdir( PLUGINS_PATH . '/custom-post/pdf/' );
        }

        $file_name   = $_FILES['menu_pdf']['name'];
        $file_size   = $_FILES['menu_pdf']['size'];
        $file_type   = $_FILES['menu_pdf']['type'];
        $file_source = $_FILES['menu_pdf']['tmp_name'];

        $pdf = '';

        if( is_allow_file_size( $file_size ) )
        {
            if( is_allow_file_type( $file_type, 'pdf' ) )
            {
                $destination = PLUGINS_PATH . '/custom-post/pdf/' . $file_name;

                if( upload( $file_source, $destination ) )
                {
                    $pdf = $file_name;

                    if( isset( $meta['pdf'] ) && !empty( $meta['pdf'] ) && file_exists( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf'] ) )
                    {
                        unlink( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf'] );
                    }
                }
            }
        }
    }
    else
    {
        $pdf = isset( $meta['pdf'] ) ? $meta['pdf'] : '';
    }

    return $pdf;
}

function delete_custom_post_pdf_file( $app_name = '' )
{
    $meta = get_meta_data( 'post_type_setting', $app_name );
    $meta = json_decode( $meta, true );

    if( !empty( $meta ) && json_last_error() === JSON_ERROR_NONE )
    {
        if( isset( $meta['pdf'] ) && !empty( $meta['pdf'] ) && file_exists( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf'] ) )
        {
        	if( file_exists( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf'] ) )
        	{
            	unlink( PLUGINS_PATH . '/custom-post/pdf/' . $meta['pdf'] );
        	}

        	$meta['pdf'] = '';

            if( update_meta_data( 'post_type_setting', json_encode( $meta ), $app_name ) )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}


function execute_post_type_ajax_request()
{
    if( $_POST['ajax_key'] == 'delete-pdf-file' )
    {
        if( delete_custom_post_pdf_file( $_POST['app_name'] ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"faileda"}';
        }
    }

    if( $_POST['ajax_key'] == 'delete-bg-image' )
    {
        echo '{"result":"aaa"}';
        exit;
        if( delete_custom_post_bg_image( $_POST['app_name'], $_POST['dest_id'] ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"faileda"}';
        }
    }

    exit;
}

?>
