<?php

add_actions( 'treatment-ajax_page', 'execute_treatment_ajax_request' );

//-- Add the meta data input interface
//-- into each proccess in administrator area. 
$apps_array = array( 'spa' );

if( is_edit_all() && !( is_save_draft() || is_publish() ) )
{
    foreach( $_POST[ 'select' ] as $index => $post_id )
    {
        foreach( $apps_array as $apps )
        {
            add_actions( $apps . '_additional_filed_' . $index, 'treatment_data_func', $apps );
            add_actions( $apps . '_additional_delete_' . $index, 'treatment_delete_func', $apps );
        }
    }
}
else
{
    foreach( $apps_array as $apps )
    {
        add_actions( $apps . '_additional_filed', 'treatment_data_func', $apps );
        add_actions( $apps . '_additional_delete', 'treatment_delete_func', $apps );
    }
}

set_previous_treatment_data();

function set_previous_treatment_data()
{
    global $db;

    //-- Create Table lumonata_treatment_category
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_treatment_category` (
            `lctreatment_id` bigint(20) NOT NULL AUTO_INCREMENT,
            `larticle_id` bigint(20) NOT NULL,
            `lctreatment_parent` bigint(20) DEFAULT NULL,
            `lctreatment_name` varchar(100) DEFAULT NULL,
            `lctreatment_desc` longtext,
            `lorder` bigint(20) DEFAULT "0",
        PRIMARY KEY (`lctreatment_id`)
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Create Table lumonata_treatment
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_treatment` (
            `ltreatment_id` bigint(20) NOT NULL AUTO_INCREMENT,
            `lctreatment_id` bigint(20) NOT NULL,
            `ltreatment_name` varchar(150) DEFAULT NULL,
            `ltreatment_desc` text,
            `ltreatment_type` longtext,
            `ltreatment_price` varchar(150) NOT NULL DEFAULT "0",
            `lorder` bigint(20) NOT NULL DEFAULT "0",
        PRIMARY KEY (`ltreatment_id`), KEY `lctreatment_id` (`lctreatment_id`),
        CONSTRAINT `lumonata_treatment_ibfk_1` FOREIGN KEY (`lctreatment_id`) REFERENCES `lumonata_treatment_category` (`lctreatment_id`) ON UPDATE NO ACTION
    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );
}

function treatment_data_func( $apps )
{
    global $thepost;
    
    $i       = $thepost->post_index;
    $post_id = $thepost->post_id;
    
    if( is_publish() )
    {
        treatment_sync_func( $post_id, $i );
    }

    set_template( PLUGINS_PATH . '/treatment/treatment-form.html', 'treatment_template_' . $i );

    add_block( 'treatment_block', 'adt', 'treatment_template_' . $i );

    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'post_id', $post_id );
    add_variable( 'site_url', SITE_URL );
    add_variable( 'ajax_url', SITE_URL . '/treatment-ajax/' );
    add_variable( 'admin_url', SITE_URL . '/lumonata-admin' );
    add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/treatment' );
    add_variable( 'language_button_menus', attemp_actions( 'menus_language_button' ) );
    add_variable( 'treatment_list_language_data', attemp_actions( 'treatment_list_language_data' ) );
    add_variable( 'mtreatment_category_language_data', attemp_actions( 'treatment_category_language_data' ) );
    
    add_variable( 'action_type', $_GET[ 'prc' ] );
    
    parse_template( 'treatment_block', 'adt', false );

    return return_template( 'treatment_template_' . $i );
}

function treatment_sync_func( $post_id, $index )
{
    global $db;

    if( empty( $post_id ) )
    {
        return;
    }

    // $s = 'UPDATE lumonata_menu_category SET larticle_id = %d WHERE larticle_id = %d';
    // $q = $db->prepare_query( $s, $post_id, $_POST[ 'post_id' ][ $index ] );
    // $r = $db->do_query( $q );
}

function treatment_delete_func( $apps )
{
    global $db;
    
    if( isset( $_POST[ 'id' ] ) && !empty( $_POST[ 'id' ] ) )
    {
        if( is_array( $_POST[ 'id' ] ) )
        {
            foreach( $_POST[ 'id' ] as $post_id )
            {
                $s = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE larticle_id = %d';
                $q = $db->prepare_query( $s, $post_id );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        $s2 = 'DELETE FROM lumonata_menu WHERE lcmenu_id = %d';
                        $q2 = $db->prepare_query( $s2, $d['lcmenu_id'] );
                        
                        if( $db->do_query( $q2 ) )
                        {
                            $s3 = 'DELETE FROM lumonata_menu_category WHERE larticle_id = %d';
                            $q3 = $db->prepare_query( $s3, $post_id );
                            
                            $db->do_query( $q3 );
                        }
                    }
                }
            }
        }
        else
        {
            $s = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE larticle_id = %d';
            $q = $db->prepare_query( $s, $_POST[ 'id' ] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $s2 = 'DELETE FROM lumonata_menu WHERE lcmenu_id = %d';
                    $q2 = $db->prepare_query( $s2, $d['lcmenu_id'] );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $s3 = 'DELETE FROM lumonata_menu_category WHERE larticle_id = %d';
                        $q3 = $db->prepare_query( $s3, $_POST[ 'id' ] );
                        
                        $db->do_query( $q3 );
                    }
                }
            }
        }
    }
}

function get_additional_treatment_field( $app_id, $key, $app_name )
{
    global $db;
    $sql = $db->prepare_query( "SELECT lvalue
					FROM
					lumonata_additional_treatment_fields
					WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s", $app_id, $key, $app_name ); //echo $sql;
    $r   = $db->do_query( $sql ); 
    $d   = $db->fetch_array( $r );
    return stripslashes( $d['lvalue'] );
    
}



function get_main_treatment( $post_id, $lang_code )
{
    global $db;

    $s = 'SELECT *
          FROM lumonata_treatment
          WHERE larticle_id = %d ORDER BY lorder';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data_name     = "";
            $data_desc     = "";
            $data_duration =  "";

            if(!empty($lang_code))
            {
                $code_lang       = explode(",", $lang_code);
                $data_name[]     = "";
                $data_desc[]     = "";
                $data_duration[] = "";

                for($i=0;$i<count($code_lang);$i++)
                {
                    $cl = $code_lang[$i];
                    $lkey_name     = "ltreatment_name_".$cl;
                    $lkey_desc     = "ltreatment_desc_".$cl;
                    $lkey_duration = "ltreatment_duration_".$cl;

                    $data_name["treatment_name_".$cl] = array(
                        'value' => get_additional_treatment_field($d['ltreatment_id'], $lkey_name, 'treatment_list')
                    );

                    $data_desc["treatment_desc_".$cl] = array(
                        'value' => get_additional_treatment_field($d['ltreatment_id'], $lkey_desc, 'menu_list')
                    );

                    $data_duration["treatment_duration_".$cl] = array(
                        'value' => get_additional_treatment_field($d['ltreatment_id'], $lkey_desc, 'menu_list')
                    );
                }
            }

            $data[] = array( 
                'ltreatment_id'       => $d['ltreatment_id'], 
                'ltreatment_name'     => $d['ltreatment_name'],
                'ltreatment_desc'     => $d['ltreatment_desc'],
                'ltreatment_duration' => $d['ltreatment_duration'],
                'ltreatment_price'    => $d['ltreatment_price'],
                'lang_name'           => $data_name,
                'lang_desc'           => $data_desc,
                'lang_duration'       => $data_duration
            );
        }

        return $data;
    }
}

function save_main_treatment( $post_id, $name, $description = '', $duration='', $price = 0, $code_lang='', $treatment_name_lang, $treatment_des_lang, $treatment_duration_lang )
{
    global $db;

    $s = 'INSERT INTO lumonata_treatment( larticle_id, ltreatment_name, ltreatment_desc, ltreatment_duration, ltreatment_price ) VALUES( %d, %s, %s, %s, %d )';
    $q = $db->prepare_query( $s, $post_id, $name, $description, $duration, $price );
    
    if( reset_order_id( 'lumonata_treatment' ) )
    {
        if( $db->do_query( $q ) )
        {
            $mysql_insert_id = mysql_insert_id();

            // add to additional data for treatment language
            if(!empty($code_lang))
            {
                $code_lang = explode(",",$code_lang);
                for($i=0;$i<count($code_lang);$i++)
                {
                    $name_lang      = (empty($treatment_name_lang[$code_lang[$i]]) ? $name : $treatment_name_lang[$code_lang[$i]]);
                    $des_lang       = (empty($treatment_des_lang[$code_lang[$i]]) ? $description : $treatment_des_lang[$code_lang[$i]]);
                    $duration_lang  = (empty($treatment_duration_lang[$code_lang[$i]]) ? $duration : $treatment_duration_lang[$code_lang[$i]]);

                    $insert_name     = insert_additional_treatment_fields($mysql_insert_id, "ltreatment_name_".$code_lang[$i], $name_lang, "treatment_list");
                    $insert_desc     = insert_additional_treatment_fields($mysql_insert_id, "ltreatment_desc_".$code_lang[$i], $des_lang, "treatment_list");
                    $insert_duration = insert_additional_treatment_fields($mysql_insert_id, "ltreatment_duration_".$code_lang[$i], $duration_lang, "treatment_list");
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_main_treatment( $menu_id, $category, $name, $description = '', $type = array(), $price = 0, $code_lang='', $menu_name_lang, $menu_des_lang )
{
    global $db;

    if( empty( $type ) )
    {
        $s = 'UPDATE lumonata_menu SET 
                lcmenu_id = %s, 
                lmenu_name = %s,
                lmenu_desc = %s,
                lmenu_price = %s
              WHERE lmenu_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, $price, $menu_id );
    }
    else
    {
        $s = 'UPDATE lumonata_menu SET 
                lcmenu_id = %s, 
                lmenu_name = %s,
                lmenu_desc = %s,
                lmenu_type = %s,
                lmenu_price = %s
              WHERE lmenu_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price, $menu_id );
    }
    
    if( $db->do_query( $q ) )
    {
        if(!empty($code_lang))
        {
            $code_lang = explode(",",$code_lang);
            for($i=0;$i<count($code_lang);$i++)
            {
                $name_lang = (empty($menu_name_lang[$code_lang[$i]]) ? $name : $menu_name_lang[$code_lang[$i]]);

                $des_lang = (empty($menu_des_lang[$code_lang[$i]]) ? $description : $menu_des_lang[$code_lang[$i]]);

                // DELETE DATA
                $delete_name = delete_additional_treatment_fields($menu_id, "lmenu_name_".$code_lang[$i], "menu_list");
                $delete_desc = delete_additional_treatment_fields($menu_id, "lmenu_desc_".$code_lang[$i], "menu_list");

                // INSERT DATA
                $insert_name = insert_additional_treatment_fields($menu_id, "lmenu_name_".$code_lang[$i], $name_lang, "menu_list");
                $insert_desc = insert_additional_treatment_fields($menu_id, "lmenu_desc_".$code_lang[$i], $des_lang, "menu_list");
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_main_treatment( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_treatment SET lorder = %d WHERE ltreatment = %d';
            $q = $db->prepare_query( $s, $i, $obj['id'] );
            $r = $db->do_query( $q );
        }

        return true;
    }
}

function delete_main_treatment( $menu_id )
{
    global $db;

    $s = 'DELETE FROM lumonata_treatment WHERE ltreatment_id = %d';
    $q = $db->prepare_query( $s, $menu_id );
    
    if( $db->do_query( $q ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function execute_treatment_ajax_request()
{
    if( is_ajax_request() )
    {
        if( $_POST[ 'ajax_key' ] == 'get-main-treatment' )
        {
            $data = get_main_treatment( $_POST['post_id'], $_POST['lang_code'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-main-treatment' )
        {

            if( save_main_treatment( $_POST['post_id'], $_POST['name'], $_POST['desc'], $_POST['duration'], $_POST['price'], $_POST['lang_code'], $_POST['treatment_name_lang'], $_POST['treatment_des_lang'], $_POST['treatment_duration_lang'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-main-menu' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( update_main_treatment( $_POST['menu_id'], $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['type'], $_POST['price'], $_POST['lang_code'], $_POST['menu_name_lang'], $_POST['menu_des_lang'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-main-treatment' )
        {
            if( reorder_main_treatment( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-main-menu' )
        {
            if( delete_main_treatment( $_POST['menu_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }
    }
    
    exit;
}

function insert_additional_treatment_fields($app_id, $key, $value, $app_name)
{
    global $db;

    $q = $db->prepare_query("INSERT INTO lumonata_additional_treatment_fields VALUES(%d, %s, %s, %s)", $app_id, $key, $value, $app_name);

    return $db->do_query($q);
}

function delete_additional_treatment_fields($app_id, $key='', $app_name)
{
    global $db;

    if(!empty($key))
    {
        $q = $db->prepare_query("DELETE FROM lumonata_additional_treatment_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s", $app_id, $key, $app_name);
    }
    else
    {
        $q = $db->prepare_query("DELETE FROM lumonata_additional_treatment_fields WHERE lapp_id=%d AND lapp_name=%s", $app_id, $app_name);
    }

    return $db->do_query($q);
}

?>