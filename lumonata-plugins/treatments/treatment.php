<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
Plugin Name: Lumonata Spa treatments
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding spa treatments.
Author: Ngurah Rai
Author URL: http://lumonata.com/
Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

add_actions( 'treatments-ajax_page', 'execute_treatments_ajax_request' );

//-- Add the meta data input interface
//-- into each proccess in administrator area. 
$apps_array = array( 'spa' );

if( is_edit_all() && !( is_save_draft() || is_publish() ) )
{
    foreach( $_POST[ 'select' ] as $index => $post_id )
    {
        foreach( $apps_array as $apps )
        {
            add_actions( $apps . '_additional_filed_' . $index, 'treatments_data_func', $apps );
            add_actions( $apps . '_additional_delete_' . $index, 'treatments_delete_func', $apps );
        }
    }
}
else
{
    foreach( $apps_array as $apps )
    {
        add_actions( $apps . '_additional_filed', 'treatments_data_func', $apps );
        add_actions( $apps . '_additional_delete', 'treatments_delete_func', $apps );
    }
}

set_previous_treatment_data();

function set_previous_treatment_data()
{
    global $db;

    //-- Create Table lumonata_treatment_category
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_treatment_category` (
              `lctreatment_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `larticle_id` bigint(20) NOT NULL,
              `lctreatment_parent` bigint(20) DEFAULT NULL,
              `lctreatment_name` varchar(100) DEFAULT NULL,
              `lctreatment_desc` longtext DEFAULT NULL,
              `lorder` bigint(20) DEFAULT "0",
          PRIMARY KEY (`lctreatment_id`)
         ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Create Table lumonata_treatment
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_treatment` (
              `ltreatment_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `lctreatment_id` bigint(20) NOT NULL,
              `ltreatment_name` varchar(150) DEFAULT NULL,
              `ltreatment_desc` text,
              `ltreatment_price` varchar(150) NOT NULL DEFAULT "0",
              `lorder` bigint(20) NOT NULL DEFAULT "0",
          PRIMARY KEY (`ltreatment_id`), KEY `lctreatment_id` (`lctreatment_id`),
          CONSTRAINT `lumonata_treatment_ibfk_1` FOREIGN KEY (`lctreatment_id`) REFERENCES `lumonata_treatment_category` (`lctreatment_id`) ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Count data on table lumonata_treatment_category
    $q = 'SELECT COUNT(lctreatment_id) AS ctreatment_count FROM lumonata_treatment_category';
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    //-- Count data on table lumonata_treatment
    $q2 = 'SELECT COUNT(ltreatment_id) AS treatment_count FROM lumonata_treatment';
    $r2 = $db->do_query( $q2 );
    $d2 = $db->fetch_array( $r2 );

    //-- If data empty run the script
    if( $d['ctreatment_count'] == 0 && $d2['treatment_count'] == 0 )
    {
        $s = 'SELECT
                b.lvalue,
                a.larticle_id,
                a.larticle_title
              FROM lumonata_articles AS a
              LEFT JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
              WHERE b.lkey = %s';
        $q = $db->prepare_query( $s, 'food_drink_treatment_category' );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            if( empty( $d['lvalue'] ) )
            {
                continue;
            }

            $objects = json_decode( $d['lvalue'], true );

            if ( $objects !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $objects as $obj )
                {
                    $s2 = 'INSERT INTO lumonata_treatment_category( larticle_id, lctreatment_parent, lctreatment_name, lorder ) VALUES( %d, %d, %s, %d )';
                    $q2 = $db->prepare_query( $s2, $d['larticle_id'], 0, $obj['name'], $i );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $lctreatment_id = mysql_insert_id();

                        if( isset( $obj['child'] ) && !empty( $obj['child'] ) )
                        {
                            $ii = 0;

                            foreach( $obj['child'] as $child )
                            {
                                $s3 = 'INSERT INTO lumonata_treatment_category( larticle_id, lctreatment_parent, lctreatment_name, lorder ) VALUES( %d, %d, %s, %d )';
                                $q3 = $db->prepare_query( $s3, $d['larticle_id'], $lctreatment_id, $child['name'], $ii );

                                $db->do_query( $q3 );

                                $ii++;
                            }
                        }
                    }

                    $i++;
                }
            }
        }

        $s = 'SELECT
                b.lvalue,
                a.larticle_id,
                a.larticle_title
              FROM lumonata_articles AS a
              LEFT JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
              WHERE b.lkey = %s';
        $q = $db->prepare_query( $s, 'food_drink_treatment' );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            if( empty( $d['lvalue'] ) )
            {
                continue;
            }

            $objects = json_decode( $d['lvalue'], true );

            if ( $objects !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $objects as $obj )
                {
                    $ct_arr = explode( '|', $obj['category'] );
                    $lcname = trim( end( $ct_arr ) );                

                    $s2 = 'SELECT lctreatment_id FROM lumonata_treatment_category WHERE lctreatment_name = %s AND larticle_id = %d';
                    $q2 = $db->prepare_query( $s2, $lcname, $d['larticle_id'] );
                    $r2 = $db->do_query( $q2 );

                    if( $db->num_rows( $r2 ) )
                    {
                        $d2 = $db->fetch_array( $r2 );

                        $s3 = 'INSERT INTO lumonata_treatment( lctreatment_id, ltreatment_name, ltreatment_desc, ltreatment_price, lorder ) VALUES( %d, %s, %s, %s, %d )';
                        $q3 = $db->prepare_query( $s3, $d2['lctreatment_id'], $obj['name'], $obj['desc'], $obj['price'], $i );
                        
                        $db->do_query( $q3 );
                    }

                    $i++;
                }
            }
        }
    }
}

function treatments_data_func( $apps )
{
    global $thepost;
    
    $i       = $thepost->post_index;
    $post_id = $thepost->post_id;
    
    if( is_publish() )
    {
        treatments_sync_func( $post_id, $i );
        set_treatment_pdf( $post_id, $apps );
    }

    set_template( PLUGINS_PATH . '/treatments/treatments-form.html', 'treatments_template_' . $i );

    add_block( 'treatments_block', 'adt', 'treatments_template_' . $i );

    $current_file = "";
    $old_pdf      = get_additional_field( $post_id, 'treatment_pdf', $apps );
    if( !empty( $old_pdf ) && file_exists( PLUGINS_PATH . '/treatments/pdf/' . $old_pdf ) )
    {
        $current_file = '
            <div class="treatment-pdf-file">
                <p>Current File</p>
                <a href="'.URL_PLUGINS . 'treatments/pdf/' . $old_pdf.'">'.$old_pdf.'</a>
                <span class="delete-pdf"><img src="'.URL_PLUGINS . 'menus/images/delete.png" /></span>
            </div>
        ';
    }
    add_variable( 'current_file', $current_file );

    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'post_id', $post_id );
    add_variable( 'site_url', SITE_URL );
    add_variable( 'ajax_url', SITE_URL . '/treatments-ajax/' );
    add_variable( 'admin_url', SITE_URL . '/lumonata-admin' );
    add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/treatments' );
    add_variable( 'language_button_treatments', attemp_actions( 'menus_language_button' ) );
    add_variable( 'treatment_list_language_data', attemp_actions( 'treatment_list_language_data' ) );
    add_variable( 'treatment_category_language_data', attemp_actions( 'treatment_category_language_data' ) );

    $tax_default_spa = get_additional_field( 24, 'tax_default_spa', 'destinations' );
    add_variable( 'tax_default_spa', $tax_default_spa );
    
    add_variable( 'action_type', $_GET[ 'prc' ] );
    
    parse_template( 'treatments_block', 'adt', false );

    return return_template( 'treatments_template_' . $i );
}

function treatments_sync_func( $post_id, $index )
{
    global $db;

    if( empty( $post_id ) )
    {
        return;
    }

    $s = 'UPDATE lumonata_treatment_category SET larticle_id = %d WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $post_id, $_POST[ 'post_id' ][ $index ] );
    $r = $db->do_query( $q );
}

function treatments_delete_func( $apps )
{
    global $db;
    
    if( isset( $_POST[ 'id' ] ) && !empty( $_POST[ 'id' ] ) )
    {
        if( is_array( $_POST[ 'id' ] ) )
        {
            foreach( $_POST[ 'id' ] as $post_id )
            {
                $s = 'SELECT lctreatment_id FROM lumonata_treatment_category WHERE larticle_id = %d';
                $q = $db->prepare_query( $s, $post_id );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        $s2 = 'DELETE FROM lumonata_treatment WHERE lctreatment_id = %d';
                        $q2 = $db->prepare_query( $s2, $d['lctreatment_id'] );
                        
                        if( $db->do_query( $q2 ) )
                        {
                            $s3 = 'DELETE FROM lumonata_treatment_category WHERE larticle_id = %d';
                            $q3 = $db->prepare_query( $s3, $post_id );
                            
                            $db->do_query( $q3 );
                        }
                    }
                }
            }
        }
        else
        {
            $s = 'SELECT lctreatment_id FROM lumonata_treatment_category WHERE larticle_id = %d';
            $q = $db->prepare_query( $s, $_POST[ 'id' ] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $s2 = 'DELETE FROM lumonata_treatment WHERE lctreatment_id = %d';
                    $q2 = $db->prepare_query( $s2, $d['lctreatment_id'] );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $s3 = 'DELETE FROM lumonata_treatment_category WHERE larticle_id = %d';
                        $q3 = $db->prepare_query( $s3, $_POST[ 'id' ] );
                        
                        $db->do_query( $q3 );
                    }
                }
            }
        }
    }
}

function set_treatment_pdf( $post_id, $apps )
{
    if( empty( $post_id ) )
    {
        return;
    }
    
    $old_pdf      = get_additional_field( $post_id, 'treatment_pdf', $apps );
    
    if( isset( $_FILES[ 'treatment_pdf' ] ) && $_FILES[ 'treatment_pdf' ][ 'error' ] == 0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/treatments/pdf/' ) )
        {
            mkdir( PLUGINS_PATH . '/treatments/pdf/' );
        }
        
        $file_name   = $_FILES[ 'treatment_pdf' ][ 'name' ];
        $file_size   = $_FILES[ 'treatment_pdf' ][ 'size' ];
        $file_type   = $_FILES[ 'treatment_pdf' ][ 'type' ];
        $file_source = $_FILES[ 'treatment_pdf' ][ 'tmp_name' ];

        $pdf = '';
        
        if( is_allow_file_size( $file_size, 3145728 ) )
        {
            if( is_allow_file_type( $file_type, 'pdf' ) )
            {
                $fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);				
                $fix_file_name = $fix_file_name.$file_ext;
                
                $destination = PLUGINS_PATH . '/treatments/pdf/' . $fix_file_name;
                
                if( upload( $file_source, $destination ) )
                {
                    $pdf = $fix_file_name;
                    
                    if( !empty( $old_pdf ) && file_exists( PLUGINS_PATH . '/treatments/pdf/' . $old_pdf ) )
                    {
                        unlink( PLUGINS_PATH . '/treatments/pdf/' . $old_pdf );
                    }
                }
            }
        }
    }
    else
    {
        $pdf = $old_pdf;
    }
    
    edit_additional_field( $post_id, 'treatment_pdf', $pdf, $apps );
}


function get_additional_treatment_field( $app_id, $key, $app_name )
{
    global $db;
    $sql = $db->prepare_query( "SELECT lvalue
					FROM
					lumonata_additional_treatment_fields
					WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s", $app_id, $key, $app_name ); //echo $sql;
    $r   = $db->do_query( $sql ); 
    $d   = $db->fetch_array( $r );
    return stripslashes( $d['lvalue'] );
    
}

function get_treatment_category_option( $post_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_treatment_category WHERE larticle_id = %d ORDER BY lctreatment_parent, lorder ASC';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );
    
    $options  = '';

    if( $db->num_rows( $r ) > 0 )
    {
        $dt = array();

        while( $d = $db->fetch_array( $r ) )
        {
            // $
            $dt[] = array( 
                'id'         => $d['lctreatment_id'], 
                'name'       => $d['lctreatment_name'],
                'article_id' => $d['larticle_id'],
                'parent_id'  => $d['lctreatment_parent'] 
            );
        }

        $data = build_tree( $dt );

        foreach( $data as $d )
        {
            $options .= '<option value="' . $d['id'] . '">' . $d['name'] . '</option>';

            if( isset( $d['child'] ) )
            {
                foreach( $d['child'] as $c )
                {
                    $options .= '<option class="with-tabs" value="' . $c['id'] . '">' . $c['name'] . '</option>';
                }
            }
        }
    }
    
    return $options;
}

function delete_pdf_treatment_file( $post_id = '', $app_name = '' )
{
    if( empty( $post_id ) )
    {
        return true;
    }
    
    $pdf = get_additional_field( $post_id, 'treatment_pdf', $app_name );
    
    if( empty( $pdf ) )
    {
        return true;
    }
    
    if( file_exists( PLUGINS_PATH . '/treatments/pdf/' . $pdf ) )
    {
        unlink( PLUGINS_PATH . '/treatments/pdf/' . $pdf );
    }
    
    if( edit_additional_field( $post_id, 'treatment_pdf', '', $app_name ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}



function get_category_treatment( $post_id, $lang_code )
{
    global $db;

    $s = 'SELECT * FROM lumonata_treatment_category WHERE larticle_id = %d ORDER BY lorder ASC';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $dt = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data_add  = "";
            $data_desc = "";
            if(!empty($lang_code))
            {
                $code_lang   = explode(",", $lang_code);
                $data_add[]  = "";
                $data_desc[] = "";
                for($i=0;$i<count($code_lang);$i++)
                {
                    $cl = $code_lang[$i];
                    $lkey = "lctreatment_name_".$cl;
                    $lkey_desc = "lctreatment_desc_".$cl;

                    $data_add["category_name_".$cl] = array(
                        'value' => get_additional_treatment_field($d['lctreatment_id'], $lkey, 'treatment_category')
                    );

                    $data_desc["category_desc_".$cl] = array(
                        'value' => get_additional_treatment_field($d['lctreatment_id'], $lkey_desc, 'treatment_category')
                    );
                }
            }
            $dt[] = array( 
                'id'         => $d['lctreatment_id'], 
                'name'       => $d['lctreatment_name'],
                'desc'       => $d['lctreatment_desc'],
                'article_id' => $d['larticle_id'],
                'parent_id'  => $d['lctreatment_parent'],
                'lang_cat'   => $data_add,
                'lang_desc'  => $data_desc
            );
        }

        $data = build_tree( $dt );

        return $data;
    }
}

function save_category_treatment( $post_id, $cat_name, $cat_desc, $code_lang='', $value_lang, $desc_lang )
{
    global $db;

    $s = 'INSERT INTO lumonata_treatment_category( larticle_id, lctreatment_parent, lctreatment_name, lctreatment_desc ) VALUES( %d, %d, %s, %s )';
    $q = $db->prepare_query( $s, $post_id, 0, $cat_name, $cat_desc );

    if( reset_order_id( 'lumonata_treatment_category' ) )
    {
        if( $db->do_query( $q ) )
        {
            $mysql_insert_id = mysql_insert_id();
            // add to additional data for treatment category language
            if(!empty($code_lang))
            {
                $code_lang = explode(",",$code_lang);
                for($i=0;$i<count($code_lang);$i++)
                {
                    $cat_lang = (empty($value_lang[$code_lang[$i]]) ? $cat_name : $value_lang[$code_lang[$i]]);

                    $cat_desc_lang = (empty($desc_lang[$code_lang[$i]]) ? $cat_desc : $desc_lang[$code_lang[$i]]);

                    $insert_cat      = insert_additional_treatment_fields($mysql_insert_id, "lctreatment_name_".$code_lang[$i], $cat_lang, 'treatment_category');

                    $insert_cat_desc = insert_additional_treatment_fields($mysql_insert_id, "lctreatment_desc_".$code_lang[$i], $cat_desc_lang, 'treatment_category');
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_category_treatment( $cat_id, $cat_name, $cat_desc='', $code_lang='', $value_lang, $desc_lang )
{
    global $db;

    $s = 'UPDATE lumonata_treatment_category SET lctreatment_name = %s, lctreatment_desc = %s WHERE lctreatment_id = %d';
    $q = $db->prepare_query( $s, $cat_name, $cat_desc, $cat_id );
    
    if( $db->do_query( $q ) )
    {
        if(!empty($code_lang))
        {
            $code_lang = explode(",",$code_lang);
            for($i=0;$i<count($code_lang);$i++)
            {
                $cat_lang = (empty($value_lang[$code_lang[$i]]) ? $cat_name : $value_lang[$code_lang[$i]]);

                $cat_lang_desc = (empty($desc_lang[$code_lang[$i]]) ? $cat_desc : $desc_lang[$code_lang[$i]]);

                // DELETE DATA
                $delete_cat = delete_additional_treatment_fields($cat_id, "lctreatment_name_".$code_lang[$i], 'treatment_category');
                $delete_cat_desc = delete_additional_treatment_fields($cat_id, "lctreatment_desc_".$code_lang[$i], 'treatment_category');

                // INSERT DATA
                $insert_cat = insert_additional_treatment_fields($cat_id, "lctreatment_name_".$code_lang[$i], $cat_lang, 'treatment_category');
                $insert_cat_desc = insert_additional_treatment_fields($cat_id, "lctreatment_desc_".$code_lang[$i], $cat_lang_desc, 'treatment_category');
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_category_treatment( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_treatment_category SET lorder = %d, lctreatment_parent = %d WHERE lctreatment_id = %d';
            $q = $db->prepare_query( $s, $i, 0, $obj['id'] );
            $r = $db->do_query( $q );

            if( isset( $obj['children'] ) )
            {
                foreach( $obj['children'] as $idx => $child )
                {
                    $s = 'UPDATE lumonata_treatment_category SET lorder = %d, lctreatment_parent = %d WHERE lctreatment_id = %d';
                    $q = $db->prepare_query( $s, $idx, $obj['id'], $child['id'] );
                    $r = $db->do_query( $q );
                }
            }
        }

        return true;
    }
}

function delete_category_treatment( $cat_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_treatment_category WHERE lctreatment_id = %d OR lctreatment_parent = %d';
    $q = $db->prepare_query( $s, $cat_id, $cat_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $id = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $id[] = $d['lctreatment_id'];
        }

        $q2 = 'SELECT * FROM lumonata_treatment WHERE lctreatment_id IN( ' . implode( ',', $id ) . ' )';
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );

        if( $n2 > 0 )
        {
            return false;
        }
        else
        {
            $s3 = 'DELETE FROM lumonata_treatment_category WHERE lctreatment_id = %d';
            $q3 = $db->prepare_query( $s3, $cat_id );
            
            if( $db->do_query( $q3 ) )
            {
                $delete_cat = delete_additional_treatment_fields($cat_id, '', 'treatment_category');

                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        $s2 = 'DELETE FROM lumonata_treatment_category WHERE lctreatment_id = %d';
        $q2 = $db->prepare_query( $s2, $cat_id );
        
        if( $db->do_query( $q2 ) )
        {
            $delete_cat = delete_additional_treatment_fields($cat_id, '', 'treatment_category');

            return true;
        }
        else
        {
            return false;
        } 
    }
}

function get_main_treatment( $post_id, $lang_code )
{
    global $db;

    $s = 'SELECT *, ( 
                SELECT lctreatment_name 
                FROM lumonata_treatment_category AS a2 
                WHERE a2.lctreatment_id = b.lctreatment_parent 
            ) AS lctreatment_parent_name
          FROM lumonata_treatment AS a
          LEFT JOIN lumonata_treatment_category AS b ON a.lctreatment_id = b.lctreatment_id
          WHERE b.larticle_id = %d ORDER BY a.lorder';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data_name = "";
            $data_desc = "";
            if(!empty($lang_code))
            {
                $code_lang   = explode(",", $lang_code);
                $data_name[] = "";
                $data_desc[] = "";
                for($i=0;$i<count($code_lang);$i++)
                {
                    $cl = $code_lang[$i];
                    $lkey_name = "ltreatment_name_".$cl;
                    $lkey_desc = "ltreatment_desc_".$cl;

                    $data_name["treatment_name_".$cl] = array(
                        'value' => get_additional_treatment_field($d['ltreatment_id'], $lkey_name, 'treatment_list')
                    );

                    $data_desc["treatment_desc_".$cl] = array(
                        'value' => get_additional_treatment_field($d['ltreatment_id'], $lkey_desc, 'treatment_list')
                    );
                }
            }

            $data[] = array( 
                'ltreatment_id'    => $d['ltreatment_id'], 
                'lctreatment_id'   => $d['lctreatment_id'],
                'ltreatment_name'  => $d['ltreatment_name'],
                'ltreatment_desc'  => $d['ltreatment_desc'],
                'ltreatment_price' => $d['ltreatment_price'],
                'ltreatment_tax'   => $d['ltreatment_tax'],
                'lctreatment_name' => ( $d['lctreatment_parent_name'] != '' ? $d['lctreatment_parent_name'] . ' | ' . $d['lctreatment_name']: $d['lctreatment_name'] ),
                'lang_name'        => $data_name,
                'lang_desc'        => $data_desc
            );
        }

        return $data;
    }
}

function save_main_treatment( $category, $name, $description = '', $price = 0, $code_lang='', $treatment_name_lang, $treatment_des_lang, $tax=0 )
{
    global $db;

    // if( empty( $type ) )
    // {
        $s = 'INSERT INTO lumonata_treatment( lctreatment_id, ltreatment_name, ltreatment_desc, ltreatment_price, ltreatment_tax ) VALUES( %d, %s, %s, %d, %d )';
        $q = $db->prepare_query( $s, $category, $name, $description, $price, $tax );
    // }
    // else
    // {
    //     $s = 'INSERT INTO lumonata_treatment( lctreatment_id, ltreatment_name, ltreatment_desc, ltreatment_type, ltreatment_price ) VALUES( %d, %s, %s, %s, %s )';
    //     $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price );
    // }
    
    if( reset_order_id( 'lumonata_treatment' ) )
    {
        if( $db->do_query( $q ) )
        {
            $mysql_insert_id = mysql_insert_id();

            // add to additional data for treatment language
            if(!empty($code_lang))
            {
                $code_lang = explode(",",$code_lang);
                for($i=0;$i<count($code_lang);$i++)
                {
                    $name_lang = (empty($treatment_name_lang[$code_lang[$i]]) ? $name : $treatment_name_lang[$code_lang[$i]]);

                    $des_lang = (empty($treatment_des_lang[$code_lang[$i]]) ? $description : $treatment_des_lang[$code_lang[$i]]);

                    $insert_name = insert_additional_treatment_fields($mysql_insert_id, "ltreatment_name_".$code_lang[$i], $name_lang, "treatment_list");

                    $insert_desc = insert_additional_treatment_fields($mysql_insert_id, "ltreatment_desc_".$code_lang[$i], $des_lang, "treatment_list");
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_main_treatment( $treatment_id, $category, $name, $description = '', $price = 0, $code_lang='', $treatment_name_lang, $treatment_des_lang, $tax=0 )
{
    global $db;

    // if( empty( $type ) )
    // {
        $s = 'UPDATE lumonata_treatment SET 
                lctreatment_id = %s, 
                ltreatment_name = %s,
                ltreatment_desc = %s,
                ltreatment_price = %d,
                ltreatment_tax = %d
              WHERE ltreatment_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, $price, $tax, $treatment_id );
    // }
    // else
    // {
    //     $s = 'UPDATE lumonata_treatment SET 
    //             lctreatment_id = %s, 
    //             ltreatment_name = %s,
    //             ltreatment_desc = %s,
    //             ltreatment_type = %s,
    //             ltreatment_price = %s
    //           WHERE ltreatment_id = %d';
    //     $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price, $treatment_id );
    // }
    
    if( $db->do_query( $q ) )
    {
        if(!empty($code_lang))
        {
            $code_lang = explode(",",$code_lang);
            for($i=0;$i<count($code_lang);$i++)
            {
                $name_lang = (empty($treatment_name_lang[$code_lang[$i]]) ? $name:       $treatment_name_lang[$code_lang[$i]]);
                $des_lang  = (empty($treatment_des_lang[$code_lang[$i]]) ? $description: $treatment_des_lang[$code_lang[$i]]);

                // DELETE DATA
                $delete_name = delete_additional_treatment_fields($treatment_id, "ltreatment_name_".$code_lang[$i], "treatment_list");
                $delete_desc = delete_additional_treatment_fields($treatment_id, "ltreatment_desc_".$code_lang[$i], "treatment_list");

                // INSERT DATA
                $insert_name = insert_additional_treatment_fields($treatment_id, "ltreatment_name_".$code_lang[$i], $name_lang, "treatment_list");
                $insert_desc = insert_additional_treatment_fields($treatment_id, "ltreatment_desc_".$code_lang[$i], $des_lang, "treatment_list");
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_main_treatment( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_treatment SET lorder = %d WHERE ltreatment_id = %d';
            $q = $db->prepare_query( $s, $i, $obj['id'] );
            $r = $db->do_query( $q );
        }

        return true;
    }
}

function delete_main_treatment( $treatment_id )
{
    global $db;

    $s = 'DELETE FROM lumonata_treatment WHERE ltreatment_id = %d';
    $q = $db->prepare_query( $s, $treatment_id );
    
    if( $db->do_query( $q ) )
    {
        $delete_cat = delete_additional_treatment_fields($treatment_id, '', 'treatment_list');
        return true;
    }
    else
    {
        return false;
    }
}

function execute_treatments_ajax_request()
{    
    if( is_ajax_request() )
    {
        if( $_POST[ 'ajax_key' ] == 'get-category-treatment' )
        {
            $data = get_category_treatment( $_POST['post_id'], $_POST['lang_code'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-category-treatment' )
        {
            if( delete_category_treatment( $_POST['cat_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-category-treatment' )
        {
            if( save_category_treatment( $_POST['post_id'], $_POST['cat_name'], $_POST['cat_desc'], $_POST['lang_code'], $_POST['category_lang'], $_POST['category_lang_desc'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-category-treatment' )
        {
            if( update_category_treatment( $_POST['cat_id'], $_POST['cat_name'], $_POST['cat_desc'], $_POST['lang_code'], $_POST['category_lang'], $_POST['category_lang_desc'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-category-treatment' )
        {
            if( reorder_category_treatment( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'get-category-treatment-option' )
        {
            $options = get_treatment_category_option( $_POST['post_id'] );

            if( empty( $options ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $options ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'get-main-treatment' )
        {
            $data = get_main_treatment( $_POST['post_id'], $_POST['lang_code'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-main-treatment' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( save_main_treatment( $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['price'], $_POST['lang_code'], $_POST['treatment_name_lang'], $_POST['treatment_des_lang'], $_POST['tax'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-main-treatment' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( update_main_treatment( $_POST['treatment_id'], $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['price'], $_POST['lang_code'], $_POST['treatment_name_lang'], $_POST['treatment_des_lang'], $_POST['tax'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-main-treatment' )
        {
            if( reorder_main_treatment( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-main-treatment' )
        {
            if( delete_main_treatment( $_POST['treatment_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-pdf-treatment-file' )
        {
            if( delete_pdf_treatment_file( $_POST['post_id'], 'spa' ))
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }
    }
    
    exit;
}

function insert_additional_treatment_fields($app_id, $key, $value, $app_name)
{
    global $db;

    $q = $db->prepare_query("INSERT INTO lumonata_additional_treatment_fields VALUES(%d, %s, %s, %s)", $app_id, $key, $value, $app_name);

    return $db->do_query($q);
}

function delete_additional_treatment_fields($app_id, $key='', $app_name)
{
    global $db;

    if(!empty($key))
    {
        $q = $db->prepare_query("DELETE FROM lumonata_additional_treatment_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s", $app_id, $key, $app_name);
    }
    else
    {
        $q = $db->prepare_query("DELETE FROM lumonata_additional_treatment_fields WHERE lapp_id=%d AND lapp_name=%s", $app_id, $app_name);
    }

    return $db->do_query($q);
}

?>