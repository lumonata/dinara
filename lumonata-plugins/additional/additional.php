<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
Plugin Name: Lumonata Additional Data
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding addtional field in each post and page.
Author: Ngurah Rai
Author URL: http://lumonata.com/
Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

//-- Add Actions
add_actions( 'additional-ajax_page', 'run_additional_ajax_request' );
add_actions( 'page_additional_data', 'additional_data_page_func' );

//-- Add the meta data input interface
//-- into each proccess in administrator area.
$apps_array = array(
    'accommodation',
    'activities',
    'dining',
    'spa',
    'news',
    'gallery',
    'special-offers',
    'experience',
    'facilities',
    'pages',
    'events',
    'weddings',
    'what-to-do-act-in',
    'what-to-do-around',
    'accolades',
    'tripadvisor-review'
);


// foreach( $apps_array as $apps )
// {
//     add_actions( $apps . '_additional_filed', 'additional_data_apps_func', $apps );
//     add_actions( $apps . '_additional_delete', 'additional_delete_apps_func', $apps );
//     add_actions( $apps . '_category_additional_filed', 'rule_additional_data_apps_func', $apps );

//     add_privileges( 'administrator', $apps, 'request' );
// }

foreach( $apps_array as $key => $appss )
{
    if ($key == 'blogs') {
        add_actions( $key . '_news', $key . '_news_view' );
    }
}

if(is_edit_all() && !(is_save_draft() || is_publish()))
{
    foreach($_POST['select'] as $index=>$post_id)
    {
		foreach($apps_array as $apps)
		{
			add_actions($apps.'_additional_filed_'.$index, 'additional_data_apps_func', $apps);
			add_actions($apps.'_additional_delete', 'additional_delete_apps_func', $apps);
		}

        add_actions('page_additional_data_'.$index,'additional_data','Additional Data','page_additional_data_func');
    }
}
else
{
	foreach($apps_array as $apps)
	{
		add_actions($apps.'_additional_filed', 'additional_data_apps_func', $apps);
		add_actions($apps.'_additional_delete', 'additional_delete_apps_func', $apps);
    add_actions($apps.'_category_additional_filed', '_category_additional', $apps);
    }

    if(isset($_GET['state']) != 'pages' AND isset($_GET['state']) != 'accolades')
    {
        add_actions('page_additional_data','additional_data','Additional Data','page_additional_data_func');
    }

}

function _category_additional($apps)
{
  if ($apps == "tripadvisor-review") {
    global $db;
    $content    = '';
    $option     = isset($_GET['id'] ) ? get_additional_rule_field( $_GET['id'], 'link_ota_monkeyforest', $apps ) : "";
    $val        = ( !empty( $option ) ? $option : "" );
    $content    = '
        <fieldset>
            <p>URL OTA Monkey Forest</p>
            <input type="text" class="textbox" name="additional_fields[link_ota_monkeyforest][' . 0 . ']" placeholder="Type here" autocomplete="off" value="' . $val . '" />
        </fieldset>
    ';

    $option2     = isset($_GET['id'] ) ? get_additional_rule_field( $_GET['id'], 'link_ota_nyuhkuning', $apps ) : "";
    $val2        = ( !empty( $option2 ) ? $option2 : "" );
    $content    .= '
        <fieldset>
            <p>URL OTA Monkey Forest</p>
            <input type="text" class="textbox" name="additional_fields[link_ota_nyuhkuning][' . 0 . ']" placeholder="Type here" autocomplete="off" value="' . $val2 . '" />
        </fieldset>
    ';

    return $content;
  }
}

function page_additional_data_func()
{
    global $thepost;

    $i = $thepost->post_index;
    $post_id = $thepost->post_id;

    $content = '
	<link rel="stylesheet" type="text/css" href="http://'.SITE_URL.'/lumonata-plugins/additional/style.css">
	<script src="http://'.SITE_URL.'/lumonata-plugins/additional/script.js"></script>

	<input type="hidden" name="plugin_url" value="'.SITE_URL.'/lumonata-plugins/additional/">
	<input type="hidden" name="ajax_url" value="'.SITE_URL.'/additional-ajax/">
	<input type="hidden" name="app_name" value="pages">
	<div class="boxs-wrapps">
    	<div class="boxs">
	    	<label>Destination Page : </label>
	    	<select name="additional_fields[destination]['.$i.']" />
	    		'.get_destination_option($post_id, 'pages').'
	    	</select>
    	</div>
    </div>';

    return $content;
}

function additional_data_page_func()
{
    global $thepost, $db;

    $i  = $thepost->post_index;
    $id = isset( $_GET['id'] ) ? $_GET['id'] : time();

    set_template( PLUGINS_PATH . '/additional/additional-form.html', 'additional_template_' . $i );
    add_block( 'additional_block', 'adt', 'additional_template_' . $i );

    add_variable( 'i', $i );
    add_variable( 'app_name', 'pages' );
    add_variable( 'post_id', $id );
    add_variable( 'site_url', site_url() );
    add_variable( 'plugin_url', site_url() . '/lumonata-plugins/additional' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'version', "?v=1.0.2" );

    add_variable( 'featured_img', get_featured_img( $id, 'pages' ) );
    add_variable( 'featured_img_title', get_additional_field( $id, 'featured_img_title', 'pages' ) );
    add_variable( 'featured_img_alt', get_additional_field( $id, 'featured_img_alt', 'pages' ) );
    add_variable( 'featured_img_copyright', get_additional_field( $id, 'featured_img_copyright', 'pages' ) );

    add_variable( 'brief', get_additional_field( $id, 'brief', 'pages' ) );
    add_variable( 'custom_field', get_custom_field_content( $i, $id, 'pages' ) );
    add_variable( 'additional_field', get_additional_field_content( $i, $id, 'pages' ) );

    // add_variable( 'featured_news', featured_news( $i, $id, 'news' ));
    add_variable( 'action_type', $_GET['prc'] );
    // add_variable( 'hide_featured_news', 'display:none' );
    add_variable( 'hide_link_experience', 'display:none' );

    parse_template( 'additional_block', 'adt', false );

    return return_template( 'additional_template_' . $i );
}

function featured_news( $index=0, $post_id, $type='news' )
{
    global $db;
    // echo $post_id;
    // $list = 'aha';
    $content    = '';
    $option     = get_additional_field( $post_id, 'featured_news', 'news' );
    $val        = ( !empty( $option ) ? $option : 0 );
    $content    = '
                    <input type="radio" name="additional_fields[featured_news][' . $index . ']" value="1" ' . ( $val == 1 ? 'checked' : '' ) . '> Yes
                    <input type="radio" name="additional_fields[featured_news][' . $index . ']" value="0" ' . ( $val == 0 ? 'checked' : '' ) . '> No
                ';

    $content    .= '
                    </select>';

    return $content;
}


function form_upload_pdf($index=0, $post_id, $type='')
{
    $old_pdf      = get_additional_field( $post_id, 'file_download', $type );
    if( isset( $_FILES[ 'file_download' ] ) && $_FILES[ 'file_download' ][ 'error' ] == 0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/additional/file_download/' ) )
        {
            mkdir( PLUGINS_PATH . '/additional/file_download/' );
        }

        $file_name   = $_FILES[ 'file_download' ][ 'name' ];
        $file_size   = $_FILES[ 'file_download' ][ 'size' ];
        $file_type   = $_FILES[ 'file_download' ][ 'type' ];
        $file_source = $_FILES[ 'file_download' ][ 'tmp_name' ];

        $pdf = '';

        if( is_allow_file_size( $file_size, 3145728 ) )
        {
            if( is_allow_file_type( $file_type, 'pdf' ) )
            {
                $time = time();
                $destination = PLUGINS_PATH . '/additional/file_download/' . $time.'-'.$file_name;

                if( upload( $file_source, $destination ) )
                {
                    $pdf = $time.'-'.$file_name;

                    if( !empty( $old_pdf ) && file_exists( PLUGINS_PATH . '/additional/file_download/' . $old_pdf ) )
                    {
                        unlink( PLUGINS_PATH . '/additional/file_download/' . $old_pdf );
                    }

                    update_status_file_download_article($post_id);
                }
            }
        }
    }
    else
    {
        $pdf = $old_pdf;
    }
    // echo $pdf;
    // exit();
    if ($post_id != 0) {
      edit_additional_field( $post_id, 'file_download', $pdf, $type );
    }

    $new_pdf      = get_additional_field( $post_id, 'file_download', $type );

    $content = '
    <div class="additional_data">
        <h2>File Download</h2>
        <div class="additional_content additional-file-download">';

        if( !empty( $new_pdf ) && file_exists( PLUGINS_PATH . '/additional/file_download/' . $new_pdf ) )
        {
            $content .='
            <div class="slider">
                <p>Current File: </p>
                <a href="'.URL_PLUGINS . 'additional/file_download/' . $new_pdf.'">'.$new_pdf.'</a> <br/>
            </div>
            ';
        }

    $content .= '
            <div class="slider">
                <p>Select a file on your computer. (Recommended file type .pdf with maximum upload size <b>2MB</b> ):</p>
                <input type="file" name="file_download" autocomplete="off" />
            </div>
        </div>
    </div>
    ';

    return $content;
}


function update_status_file_download_article($post_id)
{
    global $db;

    $q = $db->prepare_query("
        UPDATE lumonata_articles
        SET lstatus_file_download=%d
        WHERE larticle_id=%d
    ", 1, $post_id);
    $r = $db->do_query($q);
}


function additional_offer($index=0, $post_id, $type='special-offers'){
    global $db;


    $booking_link = get_additional_field( $post_id, 'booking_link', $type );
    $booking_link = ( !empty( $booking_link ) ? $booking_link: "" );

    $label_link = get_additional_field( $post_id, 'label_link', $type );
    $label_link = ( !empty( $label_link ) ? $label_link: "" );

    /*
        FORM TYPE 1 = BOOKING ENGINE FORM
        FORM TYPE 2 = INQUIRY FORM
    */
    $form_type = get_additional_field( $post_id, 'form_type', $type );
    $form_type = ( !empty( $form_type ) ? $form_type: 0 );

    $content = '
        <div class="additional_data">
            <h2>Booking Link</h2>
            <div class="additional_content additional-news">
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <div class="left">
                            <p>Booking Link : </p>
                        </div>
                        <div class="right">
                            <input type="text" class="textbox" name="additional_fields[booking_link][' . $index . ']" placeholder="" autocomplete="off" value="' . $booking_link . '" />
                            <p><i>note: if empty, system will show popup booking</i></p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <div class="left">
                            <p>Label text for this link : </p>
                        </div>
                        <div class="right">
                            <input type="text" class="textbox" name="additional_fields[label_link][' . $index . ']" placeholder="" autocomplete="off" value="' . $label_link . '" />
                            <p><i>note: default value "Book Now"</i></p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <div class="left">
                            <p>Form Type : </p>
                        </div>
                        <div class="right">
                            <select name="additional_fields[form_type]['.$index.']">
                                <option value="0" '.($form_type=='0' ? 'selected' : '').'>-- Choose --</option>
                                <option value="1" '.($form_type=='1' ? 'selected' : '').'>Booking Engine Form</option>
                                <option value="2" '.($form_type=='2' ? 'selected' : '').'>Inquiry Form</option>
                            </select>
                            <p><i>note: if empty, system will show popup inquiry form</i></p>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    ';


    $content .= '
        <div class="additional_data">
            <h2>Google & Facebook Conversion</h2>
            <div class="additional_content additional-news">
            <p class="desc_additional">Use this conversion tracking option, when the Call To Action button is not directed to your Booking Engine</p>';

        $track_conversion = get_additional_field( $post_id, 'track_conversion', $type );
        $track_conversion = ( !empty( $track_conversion ) ? $track_conversion: "0" );

        $conversion_currency = get_additional_field( $post_id, 'conversion_currency', $type );
        $conversion_currency = ( !empty( $conversion_currency ) ? $conversion_currency: "" );

        $conversion_value = get_additional_field( $post_id, 'conversion_value', $type );
        $conversion_value = ( !empty( $conversion_value ) ? $conversion_value: "" );

        $landing_page_url = get_additional_field( $post_id, 'landing_page_url', $type );
        $landing_page_url = ( !empty( $landing_page_url ) ? $landing_page_url: "" );

        $content .= '
        <div class="boxs-wrapp">
            <div class="boxs">
                <div class="left">
                    <p>Track coversion? : </p>
                </div>
                <div class="right">
                    <select name="additional_fields[track_conversion]['.$index.']" class="width15">
                        <option value="1" '.($track_conversion=='1' ? 'selected' : '').'>Yes</option>
                        <option value="0" '.($track_conversion=='0' ? 'selected' : '').'>No</option>
                    </select>
                </div>
                <div class="clear"></div>
	    	</div>
        </div>
        <div class="boxs-wrapp">
            <div class="boxs">
                <div class="left">
                    <p>Select Conversion Currency? : </p>
                </div>
                <div class="right">
                    <select name="additional_fields[conversion_currency]['.$index.']" class="width15">
                        <option value="IDR" '.($conversion_currency=='IDR' ? 'selected' : '').'>IDR</option>
                        <option value="USD" '.($conversion_currency=='USD' ? 'selected' : '').'>USD</option>
                    </select>
                </div>
                <div class="clear"></div>
	    	</div>
        </div>
        <div class="boxs-wrapp conversion">
	    	<div class="boxs">
                <div class="left">
                    <p>Input conversion value : </p>
                </div>
                <div class="right">
                    <input type="text" class="textbox" name="additional_fields[conversion_value][' . $index . ']" placeholder="" autocomplete="off" value="' . $conversion_value . '" />
                </div>
                <div class="clear"></div>
	    	</div>
        </div>
        <div class="boxs-wrapp landing_page_url">
	    	<div class="boxs">
                <div class="left">
                    <p>Landing page url : </p>
                </div>
                <div class="right">
                    <input type="text" class="textbox" name="additional_fields[landing_page_url][' . $index . ']" placeholder="" autocomplete="off" value="' . $landing_page_url . '" />
                    <span><a href="'.$landing_page_url.'">View Page</a></span>
                </div>
                <div class="clear"></div>
	    	</div>
	    </div>
        ';

    $content .= '
            </div>
        </div>
    ';

    return $content;
}


function additional_news($index=0, $post_id, $type='news')
{
    global $db;

    $q = $db->prepare_query("
        SELECT lstatus_accolades
        FROM lumonata_articles
        WHERE larticle_type=%s AND larticle_status=%s AND larticle_id=%d
    ",
        "news", "publish", $post_id
    );
    $r = $db->do_query($q);
    $d = $db->fetch_array($r);

    $press_accolades = $d['lstatus_accolades'];
    $press_accolades = ( !empty( $press_accolades ) ? $press_accolades: "0" );

    $content = '
    <div class="additional_data">
        <h2>Setting Press Accolades</h2>
        <div class="additional_content additional-news">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <div class="left">
                        <p>Press Accolades? : </p>
                    </div>
                    <div class="right">
                        <select name="status_accolades['.$index.']" class="width15">
                            <option value="1" '.($press_accolades=='1' ? 'selected' : '').'>Yes</option>
                            <option value="0" '.($press_accolades=='0' ? 'selected' : '').'>No</option>
                        </select>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>';

    return $content;
}


function link_experience($index=0, $post_id, $type='experience'){
    global $db;
    // echo $post_id;
    // $list = 'aha';
    $content    = '';
    $option     = get_additional_field( $post_id, 'link_experience', 'experience' );
    $val        = ( !empty( $option ) ? $option : "" );
    $content    = '
        <fieldset>
            <p>URL</p>
            <input type="text" class="textbox" name="additional_fields[link_experience][' . $index . ']" placeholder="Link Experience" autocomplete="off" value="' . $val . '" />
        </fieldset>
    ';


    $option     = get_additional_field( $post_id, 'link_target', 'experience' );
    $val        = ( !empty( $option ) ? $option : "" );
    $content    .= '
        <fieldset>
            <p>Target Link</p>
            <input type="radio" name="additional_fields[link_target][' . $index . ']" value="1" ' . ( $val == 1 ? 'checked' : '' ) . '> New Window
            <input type="radio" name="additional_fields[link_target][' . $index . ']" value="0" ' . ( $val == 0 ? 'checked' : '' ) . '> Same Window
        </fieldset>
    ';

    return $content;
}

function rule_additional_data_apps_func( $apps )
{
    global $thepost;

    $i  = $thepost->post_index;
    $id = isset( $_GET['id'] ) ? $_GET['id'] : time();

    set_template( PLUGINS_PATH . '/additional/rule-additional-form.html', 'rule_additional_template_' . $i );
    add_block( 'rule_additional_block', 'radt', 'rule_additional_template_' . $i );

    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'rule_id', $id );
    add_variable( 'site_url', site_url() );
    add_variable( 'plugin_url', site_url() . '/lumonata-plugins/additional' );
    add_variable( 'HTTP', HTTP );

    add_variable( 'featured_img', get_rule_featured_img( $id, $apps ) );
    add_variable( 'action_type', $_GET['prc'] );

    parse_template( 'rule_additional_block', 'radt', false );

    return return_template( 'rule_additional_template_' . $i );
}

function additional_data_apps_func( $apps )
{
    global $thepost;

    $i  = $thepost->post_index;
    $id = $thepost->post_id;

    set_template( PLUGINS_PATH . '/additional/additional-form.html', 'additional_template_' . $i );
    add_block( 'additional_block', 'adt', 'additional_template_' . $i );

    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'post_id', $id );
    add_variable( 'site_url', site_url() );
    add_variable( 'plugin_url', site_url() . '/lumonata-plugins/additional' );
    add_variable( 'HTTP', HTTP );
    add_variable( 'version', "?v=1.0.2" );

    add_variable( 'featured_img', get_featured_img( $id, $apps ) );
    add_variable( 'featured_img_title', get_additional_field( $id, 'featured_img_title', $apps ) );
    add_variable( 'featured_img_alt', get_additional_field( $id, 'featured_img_alt', $apps ) );
    add_variable( 'featured_img_copyright', get_additional_field( $id, 'featured_img_copyright', $apps ) );

    add_variable( 'brief', get_additional_field( $id, 'brief', $apps ) );
    add_variable( 'custom_field', get_custom_field_content( $i, $id, $apps ) );
    add_variable( 'additional_field', get_additional_field_content( $i, $id, $apps ) );
    add_variable( 'action_type', $_GET['prc'] );
    // add_variable( 'featured_news', featured_news( $i, $id, 'news' ));
    // add_variable( 'hide_featured_news', 'display:none' );
    add_variable( 'hide_link_experience', 'display:none' );
    // add_variable('destination_opt',get_destination_option($id, $apps));
    add_variable('destination_form_choose', destination_form_choose($id, $apps));

    if($apps == 'experience'){
        add_variable( 'link_experience', link_experience( $i, $id, 'experience' ));
    }

    if($apps == 'special-offers'){
        // add_variable( 'additional_offer', additional_offer( $i, $id, 'special-offers' ));
    }

    if($apps == 'news'){
        add_variable( 'additional_news', additional_news( $i, $id, 'news' ));
    }

    // echo $apps;
    if ($apps == 'news') {
        add_variable( 'hide_featured_news', 'display:block' );
    }

    if($apps == 'experience'){
        add_variable( 'hide_link_experience', 'display:block' );
    }

    if($apps == 'tripadvisor-review'){
        add_variable( 'hide_brief', 'display:none' );
    }

    if($apps == 'gallery'){
        add_variable( 'hide_gallery_list', 'display:none' );
    }

    if($apps == 'dining'){
        add_variable( 'form_upload_pdf', form_upload_pdf($i, $id, 'dining') );
    }

    if($apps == 'activities'){
        add_variable( 'form_upload_pdf', form_upload_pdf($i, $id, 'activities') );
    }

    if($apps == 'accolades'){
        add_variable( 'hide_destination_additional', 'display:none' );
    }

    if($apps == 'accommodation'){
        // add_variable( 'vila_map_location', get_form_villa_map_upload($id, $apps));
    }

    // if($apps == 'pages'){
        // add_variable( 'hide_destination_additional', 'display:none' );
    // }

    parse_template( 'additional_block', 'adt', false );

    return return_template( 'additional_template_' . $i );
}


function destination_form_choose($post_id, $apps)
{
    $destination_opt = get_destination_option($post_id, $apps);

    $html = '
    <div class="additional_data">
        <h2>Destination</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <label>Select your destination page for this post : </label>
                    <select name="additional_fields[destination][{i}]" class="dest-opt" />
                        '.$destination_opt.'
                    </select>
                </div>
            </div>
        </div>
    </div>
    ';

    return $html;
}

function additional_delete_apps_func( $apps )
{
    global $thepost;

    if( isset( $_POST['id'] ) && !empty( $_POST['id'] ) )
    {
        if( is_array( $_POST['id'] ) )
        {
            foreach( $_POST['id'] as $post_id )
            {
                $featured = get_additional_field( $post_id, 'featured_img', $apps );

                if( !empty( $featured ) )
                {
                    $img = json_decode( $featured, true );

                    if( !empty( $img ) )
                    {
                        if( file_exists( ROOT_PATH . $img['original'] ) )
                        {
                            unlink( ROOT_PATH . $img['original'] );
                        }

                        if( file_exists( ROOT_PATH . $img['medium'] ) )
                        {
                            unlink( ROOT_PATH . $img['medium'] );
                        }

                        if( file_exists( ROOT_PATH . $img['thumb'] ) )
                        {
                            unlink( ROOT_PATH . $img['thumb'] );
                        }

                        if( file_exists( ROOT_PATH . $img['large'] ) )
                        {
                            unlink( ROOT_PATH . $img['large'] );
                        }

                        delete_additional_field_with_key( $post_id, 'featured_img', $apps );
                    }
                }
            }
        }
        else
        {
            $featured = get_additional_field( $_POST['id'], 'featured_img', $apps );

            if( !empty( $featured ) )
            {
                $img = json_decode( $featured, true );

                if( !empty( $img ) )
                {
                    if( file_exists( ROOT_PATH . $img['original'] ) )
                    {
                        unlink( ROOT_PATH . $img['original'] );
                    }

                    if( file_exists( ROOT_PATH . $img['medium'] ) )
                    {
                        unlink( ROOT_PATH . $img['medium'] );
                    }

                    if( file_exists( ROOT_PATH . $img['thumb'] ) )
                    {
                        unlink( ROOT_PATH . $img['thumb'] );
                    }

                    if( file_exists( ROOT_PATH . $img['large'] ) )
                    {
                        unlink( ROOT_PATH . $img['large'] );
                    }

                    delete_additional_field_with_key( $_POST['id'], 'featured_img', $apps );
                }
            }
        }
    }
}

function get_featured_img( $post_id, $apps, $html = true )
{
    $content  = $html ? '' : array();
    $featured = get_additional_field( $post_id, 'featured_img', $apps );

    if( !empty( $featured ) )
    {
        $img = json_decode( $featured, true );

        if( !empty( $img ) )
        {
            if( $html )
            {
                $content .= '
                <div class="box">
                    <img src="' . HTTP . site_url() . $img['medium'] . '" />
                    <div class="overlay">
                        <a data-post-id="' . $post_id . '" class="img-delete" title="Delete">
                            <img src="' . HTTP  . site_url() . '/lumonata-plugins/additional/images/delete.png">
                        </a>
                    </div>
                </div>';
            }
            else
            {
                $content = array(
                    'original' => $img['original'],
                    'medium' => $img['medium'],
                    'large' => $img['large'],
                    'small' => $img['small'],
                    'thumb' => $img['thumb']
                );
            }
        }
    }

    return $content;
}

function get_rule_featured_img( $rule_id, $apps, $html = true )
{
    $content  = $html ? '' : array();
    $featured = get_rule_additional_field( $rule_id, 'featured_img', $apps );

    if( !empty( $featured ) )
    {
        $img = json_decode( $featured, true );

        if( !empty( $img ) )
        {
            if( $html )
            {
                $content .= '
                <div class="box">
                    <img src="' . HTTP . site_url() . $img['medium'] . '" />
                    <div class="overlay">
                        <a data-post-id="' . $rule_id . '" class="img-delete" title="Delete">
                            <img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png">
                        </a>
                    </div>
                </div>';
            }
            else
            {
                $content = array(
                    'original' => $img['original'],
                    'medium' => $img['medium'],
                    'large' => $img['large'],
                    'small' => $img['small'],
                    'thumb' => $img['thumb']
                );
            }
        }
    }

    return $content;
}

function get_additional_field_content( $i, $post_id, $apps )
{
    global $db;

    $s = 'SELECT lvalue FROM lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s';
    $q = $db->prepare_query( $s, $post_id, 'custom_field', $apps );
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    $fields = json_decode( $d['lvalue'], true );

    $content = '
    <div class="additional_data post-additional-data-wrapp">
        <h2>Additional Data</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <div class="new-field-box clearfix">
                        <input type="text" class="field textbox" name="field_label" autocomplete="off" value="" placeholder="Field label" />
                        <input type="hidden" class="field textbox" name="field_name" autocomplete="off" value="" placeholder="Field name" />
                        <select class="field select-opt" name="field_type" autocomplete="off">
                            <option value="2">Tinymce</option>
                            <option value="0">Text Field</option>
                            <option value="1">Textarea</option>
                        </select>
                        <a class="add-new-field">Add New Field</a>
                    </div>
                    <div class="new-field-list clearfix">
                        <ol>';

                        if( !empty( $fields ) && is_array( $fields ) )
                        {
                            foreach( $fields as $name => $d )
                            {
                                if( $d['type'] == 0 )
                                {
                                    $content .= '
                                    <li>
                                        <div class="field-wrapp">
                                            <p><span>' . $d['label'] . '<a class="delete-field" data-name="' . $name . '"><img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                                            <div class="inner">
                                                <input type="text" class="textbox" name="additional_fields[custom_field][' . $i . '][' . $name . '][' . $d['label'] . '][' . $d['type'] . ']" autocomplete="off" value="' . $d['value'] . '" />
                                            </div>
                                        </div>
                                    </li>';
                                }
                                elseif( $d['type'] == 1 )
                                {
                                    $content .= '
                                    <li>
                                        <div class="field-wrapp">
                                            <p>' . $d['label'] . '<a class="delete-field" data-name="' . $name . '"><img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                                            <div class="inner">
                                                <textarea class="textarea" name="additional_fields[custom_field][' . $i . '][' . $name . '][' . $d['label'] . '][' . $d['type'] . ']" rows="10" autocomplete="off">' . $d['value'] . '</textarea>
                                            </div>
                                        </div>
                                    </li>';
                                }
                                elseif( $d['type'] == 2 )
                                {
                                    $content .= '
                                    <li>
                                        <div class="field-wrapp">
                                            <p>' . $d['label'] . '<a class="delete-field" data-name="' . $name . '"><img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                                            <div class="inner">
                                                <textarea id="tinymce-' . $name . '-' . time() . '" class="textarea tinymce" name="additional_fields[custom_field][' . $i . '][' . $name . '][' . $d['label'] . '][' . $d['type'] . ']" autocomplete="off">' . $d['value'] . '</textarea>
                                            </div>
                                        </div>
                                    </li>';
                                }
                            }
                        }

                        $content .= '
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>';

    return $content;
}

function get_custom_field_content( $i, $post_id, $apps, $lang=false, $code_lang='' )
{
    global $db;

    $content = '';

    if(!$lang)
    {
        if( in_array( $apps, array( 'accommodation', 'dining', 'pages', 'spa', 'events', 'weddings', 'what-to-do-act-in', 'what-to-do-around', 'gallery' ) ) )
        {
            $content .= get_gallery_image_content( $i, $post_id, $apps );
        }

        // if( in_array( $apps, array( 'dining'  ) ) )
        // {
        //     $content .= get_code_rest_diary_content($i, $post_id, $apps );
        // }

        // if( in_array( $apps, array( 'gallery'  ) ) )
        // {
        //     $content .= get_youtube_url($i, $post_id, $apps );
        //     $content .= get_photo360($i, $post_id, $apps );
        // }
    }
    else
    {
        $code_lang = "_".$code_lang;
    }

    if( $apps == 'accommodation' )
    {
        //-- Group
        $param = array(
            'group' => 'Additional Data',
            'field' => array(
                 array(
                    'name'  => 'size',
                    'label' => 'Villa Size',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'bathrm',
                    'label' => 'Bathroom',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'doccupancy',
                    'label' => 'Max Occupancy',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'occupancy',
                    'label' => 'Max Occupancy Desc',
                    'type'  => 'textarea',
                    'class' => 'tinymce'
                ),

            )
        );

        $content .= get_group_field( $i, $post_id, $apps, $param, $code_lang );

        if(!$lang){
            //-- Amenities
            $r = fetch_rule( 'rule=amenities&group=accommodation', false );
            $o = array();

            if( $db->num_rows( $r) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $o[ $d['lrule_id'] ] = $d['lname'];
                }
            }

            $param = array(
                'name'     => 'amenities',
                'label'    => 'Amenities',
                'desc'     => 'Select amenities for this accommodation',
                'multiple' => true,
                'option'   => $o
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );

            //-- Bed Type
            $param = array(
                'name'     => 'bed',
                'label'    => 'Bed Type',
                'multiple' => false,
                'option'   => array( 'single' => 'Single', 'double' => 'Double', 'double / twin' => 'Double/Twin', 'king size' => 'King Size' )
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );
        }
    }

    if($apps == 'special-offers')
    {
        //-- Group
        $param = array(
            'group' => 'Additional Data',
            'field' => array(
                array(
                    'name'  => 'price',
                    'label' => 'Price',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'wyget',
                    'label' => 'What You Get',
                    'type'  => 'textbox',
                    'class' => 'tinymce'
                ),
                array(
                    'name'  => 'wyhave',
                    'label' => 'What You Have',
                    'type'  => 'textbox',
                    'class' => 'tinymce'
                )
            )
        );

        $content .= get_group_field( $i, $post_id, $apps, $param, $code_lang );

        if(!$lang){
            //-- additional value add
            $r = fetch_rule( 'rule=additional_value&group=special-offers', false );
            $o = array();

            if( $db->num_rows( $r) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $o[ $d['lrule_id'] ] = $d['lname'];
                }
            }

            $param = array(
                'name'     => 'additional_value',
                'label'    => 'Additional Value Add',
                'desc'     => 'Select additional value add for this special offer',
                'multiple' => true,
                'option'   => $o
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );

            //-- terms and condition
            $r = fetch_rule( 'rule=terms_conditions&group=special-offers', false );
            $o = array();

            if( $db->num_rows( $r) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $o[ $d['lrule_id'] ] = $d['lname'];
                }
            }

            $param = array(
                'name'     => 'terms_conditions',
                'label'    => 'Terms & Conditions',
                'desc'     => 'Select terms and conditions for this special offer',
                'multiple' => true,
                'option'   => $o
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );
        }
    }

    if($apps == 'tripadvisor-review')
    {
        //-- Group
        $param = array(
            'group' => 'Traveller Review Data',
            'field' => array(
                array(
                    'name'  => 'traveller_name',
                    'label' => 'Traveller Name',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'rates',
                    'label' => 'Rate Review',
                    'type'  => 'option'
                ),
                array(
                    'name'  => 'rdate',
                    'label' => 'Date',
                    'type'  => 'date'
                ),
                // array(
                //     'name'  => 'traveller_link',
                //     'label' => 'Link reviews - please include http:// or https://',
                //     'type'  => 'textbox'
                // ),
            )
        );

        $content .= get_group_field( $i, $post_id, $apps, $param, $code_lang );
    }

    if($apps == 'activities')
    {
        //-- Group
        $param = array(
            'group' => 'Activities detail',
            'field' => array(
                array(
                    'name'  => 'ac_schedule',
                    'label' => 'Schedule',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'ac_time',
                    'label' => 'Time',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'ac_location',
                    'label' => 'Location',
                    'type'  => 'textbox'
                )
            )
        );

        $content .= get_group_field( $i, $post_id, $apps, $param, $code_lang );
    }

    if($apps == 'dining')
    {
        //-- Group
        $param = array(
            'group' => 'Additional Data',
            'field' => array(
                array(
                    'name'  => 'opening_hours',
                    'label' => 'Opening Hours',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'cusines',
                    'label' => 'Cusines',
                    'type'  => 'textbox'
                ),
                array(
                    'name'  => 'price_rate',
                    'label' => 'Price Rate',
                    'type'  => 'textbox'
                )
            )
        );

        $content .= get_group_field( $i, $post_id, $apps, $param, $code_lang );
    }

    if($apps == 'events' || $apps == 'weddings')
    {
        $param = array(
            'group' => 'Additional Data',
            'field' => array(
                array(
                    'name'  => 'price',
                    'label' => 'Price',
                    'type'  => 'textbox'
                )
            )
        );

        $content .= get_group_field( $i, $post_id, $apps, $param, $code_lang );

        if(!$lang){
            //-- Amenities
            $r = fetch_rule( 'rule=amenities&group='.$apps, false );
            $o = array();

            if( $db->num_rows( $r) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $o[ $d['lrule_id'] ] = $d['lname'];
                }
            }

            $param = array(
                'name'     => 'inclusion',
                'label'    => 'Inclusion',
                'desc'     => 'Select inclusion for this '.$apps,
                'multiple' => true,
                'option'   => $o
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );

            $param = array(
                'name'     => 'additional_service',
                'label'    => 'Additional service',
                'desc'     => 'Select additional service for this '.$apps,
                'multiple' => true,
                'option'   => $o
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );

            //-- terms and condition
            $r = fetch_rule( 'rule=terms_conditions&group='.$apps, false );
            $o = array();

            if( $db->num_rows( $r) > 0 )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $o[ $d['lrule_id'] ] = $d['lname'];
                }
            }

            $param = array(
                'name'     => 'terms_conditions',
                'label'    => 'Terms & Conditions',
                'desc'     => 'Select terms and conditions for this '.$apps,
                'multiple' => true,
                'option'   => $o
            );

            $content .= get_select_list_field( $i, $post_id, $apps, $param );
        }
    }

    if( $apps == 'pages' )
    {
        //-- Parent Option List
        $option = array();
        $result = fetch_artciles( 'type=pages', false );

        $option[0] = 'No Parent';

        while ( $d = $db->fetch_array( $result ) )
        {
            if( $d['larticle_id'] != $post_id )
            {
                $option[ $d['larticle_id'] ] = $d['larticle_title'];
            }
        }
    }

    return $content;
}

function get_select_list_field( $i, $post_id, $apps, $param = array() )
{
    $data = get_additional_field( $post_id, $param['name'], $apps );

    $content = '
    <div class="additional_data additional-select-wrapp">
        <h2>' . $param['label'] . '</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">' .
                    ( isset( $param['desc'] ) ? '<p>' . $param['desc'] . '<p>' : '' );

                    if( $param['multiple'] )
                    {
                        $data     = json_decode( $data, true );
                        $data     = is_array( $data ) ? implode(',', $data) : $data;
                        $content .= '
                        <input id="' . $param['name'] . '_text_' .  $i . '" type="hidden" name="additional_fields[' . $param['name'] . '][' . $i . '][]" value="' . $data . '" />
                        <a id="' . $param['name'] . '_trigger_' .  $i . '"></a>
                        <select id="' . $param['name'] . '_' .  $i . '" class="select-opt multiple" autocomplete="off" multiple>';

                            foreach( $param['option'] as $val => $label )
                            {
                               $content .= '<option value="' . $val . '">' . $label . '</option>';
                            }

                            $content .= '
                        </select>
                        <script type="text/javascript">
                            jQuery("#' . $param['name'] . '_' .  $i . '").chosen({ disable_search_threshold:10 });

                            var select  = $("#' . $param['name'] . '_' .  $i . '").get(0);
                            var textval = jQuery("#' . $param['name'] . '_text_' .  $i . '").val().split(",");

                            ChosenOrder.setSelectionOrder(select, textval, true);

                            jQuery("#' . $param['name'] . '_' .  $i . '").change(function(e){
                                var select_new  = $("#' . $param['name'] . '_' .  $i . '").get(0);

                                setTimeout(function(){
                                    var selection = ChosenOrder.getSelectionOrder(select_new);
                                    var dataval   = selection.join(",");
                                    console.log(dataval);

                                    jQuery("#' . $param['name'] . '_text_' .  $i . '").val(dataval);
                                }, 100);
                            });
                        </script>';
                    }
                    else
                    {
                        $content .= '
                        <select id="' . $param['name'] . '_' .  $i . '" class="select-opt single" name="additional_fields[' . $param['name'] . '][' . $i . ']" autocomplete="off">';

                            foreach( $param['option'] as $val => $label )
                            {
                               $content .= '<option value="' . $val . '" ' . ( $val == $data ? 'selected' : '' ) . ' >' . $label . '</option>';
                            }

                            $content .= '
                        </select>
                        <script type="text/javascript">
                            jQuery("#' . $param['name'] . '_' .  $i . '").chosen({ disable_search_threshold:10 });
                        </script>';
                    }

                    $content .= '
                </div>
            </div>
        </div>
    </div>';

    return $content;
}

function get_textbox_field( $i, $post_id, $apps, $param = array() )
{
    foreach( $param as $name => $title )
    {
        $data = get_additional_field( $post_id, $name, $apps );

        return '
        <div class="additional_data">
            <h2>' . $title . '</h2>
            <div class="additional_content">
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <input type="text" class="textbox" name="additional_fields[' . $name . '][' . $i . ']" autocomplete="off" value="' . $data . '" />
                    </div>
                </div>
            </div>
        </div>';
    }
}

function get_textarea_field( $i, $post_id, $apps, $param = array() )
{
    foreach( $param as $name => $title )
    {
        $data = get_additional_field( $post_id, $name, $apps );

        return '
        <div class="additional_data">
            <h2>' . $title . '</h2>
            <div class="additional_content">
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <textarea class="textarea" name="additional_fields[' . $name . '][' . $i . ']" autocomplete="off" rows="10">' . $data . '</textarea>
                    </div>
                </div>
            </div>
        </div>';
    }
}

function get_tinymce_field( $i, $post_id, $apps, $param = array() )
{
    foreach( $param as $name => $title )
    {
        $data = get_additional_field( $post_id, $name, $apps );

        return '
        <div class="additional_data">
            <h2>' . $title . '</h2>
            <div class="additional_content">
                <div class="boxs-wrapp">
                    <div class="boxs">
                        <textarea class="textarea tinymce" name="additional_fields[' . $name . '][' . $i . ']" autocomplete="off">' . $data . '</textarea>
                    </div>
                </div>
            </div>
        </div>';
    }
}

function get_group_field( $i, $post_id, $apps, $param = array(), $code_lang='' )
{
    if( is_array( $param ) && !empty( $param ) )
    {
        $content = '
        <div class="additional_data">
            <h2>' . $param['group'] . '</h2>
            <div class="additional_content">
                <div class="boxs-wrapp">
                    <div class="boxs">';

                    foreach( $param['field'] as $obj )
                    {
                        $class_group = "";
                        if(isset($obj['class']))
                        {
                            $class_group = $obj['class'];
                        }
                        $data = get_additional_field( $post_id, $obj['name'].$code_lang, $apps );

                        if( $obj['type'] == 'textbox' )
                        {
                            $field = '<input type="text" class="textbox '.$class_group.'" name="additional_fields[' . $obj['name'].$code_lang. '][' . $i . ']" autocomplete="off" value="' . $data . '" />';
                        }
                        else if( $obj['type'] == 'option' ){
                            $field = '
                            <select name="additional_fields[' . $obj['name'].$code_lang. '][' . $i . ']" class="dest-opt" />
                              <option value="bs" '.($data=='bs' ? 'selected' : '').'>.5 Star</option>
                              <option value="b1" '.($data=='b1' ? 'selected' : '').'>1 Star</option>;
                              <option value="b1s" '.($data=='b1s' ? 'selected' : '').'>1.5 Stars</option>;
                              <option value="b2" '.($data=='b2' ? 'selected' : '').'>2 Stars</option>;
                              <option value="b2s" '.($data=='b2s' ? 'selected' : '').'>2.5 Stars</option>;
                              <option value="b3" '.($data=='b3' ? 'selected' : '').'>3 Stars</option>;
                              <option value="b3s" '.($data=='b3s' ? 'selected' : '').'>3.5 Stars</option>;
                              <option value="b4" '.($data=='b4' ? 'selected' : '').'>4 Stars</option>;
                              <option value="b4s" '.($data=='b4s' ? 'selected' : '').'>4.5 Stars</option>;
                              <option value="b5" '.($data=='b5' ? 'selected' : '').'>5 Stars</option>;
                            </select>
                            ';
                        }
                        else if ($obj['type'] == 'date') {
                          $field = '<input type="date" class="textbox '.$class_group.'" name="additional_fields[' . $obj['name'].$code_lang. '][' . $i . ']" autocomplete="off" value="' . $data . '" />';
                        }
                        else
                        {
                            $field = '<textarea class="textarea '.$class_group.'" name="additional_fields[' . $obj['name'].$code_lang . '][' . $i . ']" autocomplete="off" rows="10">' . $data . '</textarea>';
                        }

                        $content .= '<p>' . $obj['label'] . '<br></br>' . $field . '</p>';
                    }

                    $content .= '
                    </div>
                </div>
            </div>
        </div>';

        return $content;
    }
}

function get_code_rest_diary_content($i, $post_id, $apps, $sef_des='')
{

    $code_rest_diary = get_additional_field( $post_id, 'code_rest_diary', $apps );

    $html = '
    <div class="additional_data">
        <h2>Rest Diary Setting</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <p>
                        Code Rest Diary<br><br>
                        <textarea class="textarea" name="additional_fields[code_rest_diary][0]" autocomplete="off" rows="10">'.$code_rest_diary.'</textarea>
                    </p>
                </div>
            </div>
        </div>
    </div>
    ';
    return $html;
}

function get_youtube_url($i, $post_id, $apps, $sef_des='')
{

    $youtube_url = get_additional_field( $post_id, 'youtube_url', $apps );

    $html = '
    <div class="additional_data">
        <h2>Youtube URL</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <p>
                        Youtube URL<br><br>
                        <input type="text" class="textbox" name="additional_fields[youtube_url][0]" autocomplete="off" value="'.$youtube_url.'" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    ';
    return $html;
}

function get_photo360($i, $post_id, $apps, $sef_des='')
{

    $google_photo_360 = get_additional_field( $post_id, 'google_photo_360', $apps );

    $html = '
    <div class="additional_data">
        <h2>Photo 360 Link</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <p>
                        Photo 360 Link<br><br>
                        <input type="text" class="textbox" name="additional_fields[google_photo_360][0]" autocomplete="off" value="'.$google_photo_360.'" />
                    </p>
                </div>
            </div>
        </div>
    </div>
    ';
    return $html;
}


function get_gallery_image_content( $i, $post_id, $apps, $sef_des='' )
{
    $hidden = 'style="display:none;"';
    if($apps == "gallery"){
        $hidden = "";
    }

    $get_language_data = get_language_data();
    $language_code = "";
    $language_name = "";
    if(!empty($get_language_data))
    {
        $language_code = $get_language_data['code_lang'];
        $language_name = $get_language_data['title_lang'];
    }

    $html = '
    <div class="additional_data">
        <h2>Gallery Image</h2>
        <div class="additional_content additional-gallery-data">
            <div id="block-gallery" class="block-gallery">
                <div id="dropzone">
                    <div class="dropzone needsclick dz-clickable" id="gallery-upload'.$sef_des.'">
                        <div class="dz-message needsclick">
                            Drop files here or click to upload.<br>
                            <span class="note needsclick">( maximum size for upload image is 2MB )</span>
                        </div>
                    </div>
                </div>

                <div id="preview-template'.$sef_des.'" class="dz-preview dz-file-preview" style="display:none;">
                    <div class="dz-preview dz-file-preview">
                        <div class="dz-image"><img data-dz-thumbnail data-dz-thumbnail-id/></div>
                        <div class="dz-details">
                            <a class="dz-edit">
                                <img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/edit.png" />
                            </a>
                            <a class="dz-delete">
                                <img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" />
                            </a>
                        </div>
                        <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                        <div class="dz-detail-box">
                            <div class="detail">
                                <fieldset class="title-image">
                                    <p>Image Title</p>
                                    <input type="text" name="gallery_title" value="" class="textbox" />
        ';
                                    if(!empty($language_code))
                                    {
                                        $html .='
                                            <button type="button" class="button config_lang">Configuration Multi Language</button>
                                            <div class="box-img-title-lang">
                                        ';
                                        $z = 0;
                                        foreach($language_code as $lang)
                                        {
                                            $html .= '
                                                <fieldset>
                                                    <p>Image Title ('.$language_name[$z].')</p>
                                                    <input type="text" name="gallery_title_lang['.$lang.']" value="" data-lang="'.$lang.'" class="textbox img_title_lang img_title_lang_'.$lang.'" />
                                                </fieldset>
                                            ';
                                            $z++;
                                        }

                                        $html .= '</div>';

                                    }
        $html .= '
                                </fieldset>
                                <fieldset>
                                    <p>Image Alternate text</p>
                                    <input type="text" name="gallery_alt" value="" class="textbox" />
                                </fieldset>
                                <fieldset>
                                    <p>Image Copyright</p>
                                    <input type="text" name="gallery_caption" value="" class="textbox" />
                                </fieldset>
                                <fieldset '.$hidden.'>
                                    <p>Youtube URL</p>
                                    <input type="text" name="youtube_url" value="" class="textbox" />
                                </fieldset>
                                <fieldset '.$hidden.'>
                                    <p>Photo 360</p>
                                    <input type="text" name="photo_360" value="" class="textbox" />
                                </fieldset>
                                <fieldset>
                                    <input type="button" class="save-detail" value="Save"/>
                                    <input type="button" class="close-detail" value="Close"/>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="note">Recommended resolution for sideshow image is <b>1024px * 768px</b>, with maximum upload size <b>2MB</b></p>
        </div>
    </div>
    ';
    return $html;
}

function run_additional_ajax_request()
{
    global $db;

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-featured-images' )
    {
        if( isset( $_FILES['featured_img'] ) && $_FILES['featured_img']['error']==0 )
        {
            $post_id     = $_POST['post_id'];
            $file_name   = $_FILES['featured_img']['name'];
            $file_size   = $_FILES['featured_img']['size'];
            $file_type   = $_FILES['featured_img']['type'];
            $file_source = $_FILES['featured_img']['tmp_name'];

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'image' ) )
                {
                    $file_ext  = file_name_filter( $file_name, true );
                    $file_name = file_name_filter( $file_name );

                    $file_name_t = $file_name . '-' . time() . '-thumbnail' . $file_ext;
                    $file_name_s = $file_name . '-' . time() . '-small' . $file_ext;
                    $file_name_m = $file_name . '-' . time() . '-medium' . $file_ext;
                    $file_name_l = $file_name . '-' . time() . '-large' . $file_ext;
                    $file_name   = $file_name . '-' . time() . $file_ext;

                    if( !file_exists( PLUGINS_PATH . '/additional/featured/' ) )
                    {
                        mkdir( PLUGINS_PATH . '/additional/featured/' );
                    }

                    $destination_t = PLUGINS_PATH . '/additional/featured/' . $file_name_t;
                    $destination_s = PLUGINS_PATH . '/additional/featured/' . $file_name_s;
                    $destination_m = PLUGINS_PATH . '/additional/featured/' . $file_name_m;
                    $destination_l = PLUGINS_PATH . '/additional/featured/' . $file_name_l;
                    $destination   = PLUGINS_PATH . '/additional/featured/' . $file_name;

                    $destination_save_t = '/lumonata-plugins/additional/featured/' . $file_name_t;
                    $destination_save_s = '/lumonata-plugins/additional/featured/' . $file_name_s;
                    $destination_save_m = '/lumonata-plugins/additional/featured/' . $file_name_m;
                    $destination_save_l = '/lumonata-plugins/additional/featured/' . $file_name_l;
                    $destination_save   = '/lumonata-plugins/additional/featured/' . $file_name;

                    upload_crop( $file_source, $destination_t, $file_type, thumbnail_image_width(), thumbnail_image_height(), true );
                    upload_resize( $file_source, $destination_s, $file_type, thumbnail_image_width(), thumbnail_image_height() );
                    upload_resize( $file_source, $destination_m, $file_type, medium_image_width(), medium_image_height() );
                    upload_resize( $file_source, $destination_l, $file_type, large_image_width(), large_image_height() );
                    upload( $file_source, $destination );

                    $cat = get_additional_field( $post_id, 'featured_img', $_POST['app_name'] );
                    $arr = array(
                        'thumb' => $destination_save_t,
                        'small' => $destination_save_s,
                        'medium' => $destination_save_m,
                        'large' => $destination_save_l,
                        'original' => $destination_save
                    );

                    if( empty( $cat ) )
                    {
                        if( add_additional_field( $post_id, 'featured_img', json_encode( $arr ), $_POST['app_name'] ) )
                        {
                            echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name_m . '"}';
                        }
                    }
                    else
                    {
                        $img = json_decode( $cat, true );

                        if( !empty( $img ) )
                        {
                            if( file_exists( ROOT_PATH . $img['original'] ) )
                            {
                                unlink( ROOT_PATH . $img['original'] );
                            }

                            if( file_exists( ROOT_PATH . $img['large'] ) )
                            {
                                unlink( ROOT_PATH . $img['large'] );
                            }

                            if( file_exists( ROOT_PATH . $img['medium'] ) )
                            {
                                unlink( ROOT_PATH . $img['medium'] );
                            }

                            if( file_exists( ROOT_PATH . $img['small'] ) )
                            {
                                unlink( ROOT_PATH . $img['small'] );
                            }

                            if( file_exists( ROOT_PATH . $img['thumb'] ) )
                            {
                                unlink( ROOT_PATH . $img['thumb'] );
                            }
                        }

                        if( edit_additional_field( $post_id, 'featured_img', json_encode( $arr ), $_POST['app_name'] ) )
                        {
                            echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name_s . '"}';
                        }
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"File is not an image"}';
                }
            }
            else
            {
                echo '{"result":"failed","error":"Maximum file size is 2MB"}';
            }
        }
    }


    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-map-location-images' )
    {
        if( isset( $_FILES['map_location_img'] ) && $_FILES['map_location_img']['error']==0 )
        {
            $post_id     = $_POST['post_id'];
            $file_name   = $_FILES['map_location_img']['name'];
            $file_size   = $_FILES['map_location_img']['size'];
            $file_type   = $_FILES['map_location_img']['type'];
            $file_source = $_FILES['map_location_img']['tmp_name'];

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'image' ) )
                {
                    $file_ext  = file_name_filter( $file_name, true );
                    $file_name = file_name_filter( $file_name );

                    $file_name_t = $file_name . '-' . time() . '-thumbnail' . $file_ext;
                    $file_name_s = $file_name . '-' . time() . '-small' . $file_ext;
                    $file_name_m = $file_name . '-' . time() . '-medium' . $file_ext;
                    $file_name_l = $file_name . '-' . time() . '-large' . $file_ext;
                    $file_name   = $file_name . '-' . time() . $file_ext;

                    if( !file_exists( PLUGINS_PATH . '/additional/map_location_img/' ) )
                    {
                        mkdir( PLUGINS_PATH . '/additional/map_location_img/' );
                    }

                    $destination_t = PLUGINS_PATH . '/additional/map_location_img/' . $file_name_t;
                    $destination_s = PLUGINS_PATH . '/additional/map_location_img/' . $file_name_s;
                    $destination_m = PLUGINS_PATH . '/additional/map_location_img/' . $file_name_m;
                    $destination_l = PLUGINS_PATH . '/additional/map_location_img/' . $file_name_l;
                    $destination   = PLUGINS_PATH . '/additional/map_location_img/' . $file_name;

                    $destination_save_t = '/lumonata-plugins/additional/map_location_img/' . $file_name_t;
                    $destination_save_s = '/lumonata-plugins/additional/map_location_img/' . $file_name_s;
                    $destination_save_m = '/lumonata-plugins/additional/map_location_img/' . $file_name_m;
                    $destination_save_l = '/lumonata-plugins/additional/map_location_img/' . $file_name_l;
                    $destination_save   = '/lumonata-plugins/additional/map_location_img/' . $file_name;

                    upload_crop( $file_source, $destination_t, $file_type, thumbnail_image_width(), thumbnail_image_height(), true );
                    upload_resize( $file_source, $destination_s, $file_type, thumbnail_image_width(), thumbnail_image_height() );
                    upload_resize( $file_source, $destination_m, $file_type, medium_image_width(), medium_image_height() );
                    upload_resize( $file_source, $destination_l, $file_type, large_image_width(), large_image_height() );
                    upload( $file_source, $destination );

                    $cat = get_additional_field( $post_id, 'map_location_img', $_POST['app_name'] );
                    $arr = array(
                        'thumb' => $destination_save_t,
                        'small' => $destination_save_s,
                        'medium' => $destination_save_m,
                        'large' => $destination_save_l,
                        'original' => $destination_save
                    );

                    if( empty( $cat ) )
                    {
                        if( add_additional_field( $post_id, 'map_location_img', json_encode( $arr ), $_POST['app_name'] ) )
                        {
                            echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name_m . '"}';
                        }
                    }
                    else
                    {
                        $img = json_decode( $cat, true );

                        if( !empty( $img ) )
                        {
                            if( file_exists( ROOT_PATH . $img['original'] ) )
                            {
                                unlink( ROOT_PATH . $img['original'] );
                            }

                            if( file_exists( ROOT_PATH . $img['large'] ) )
                            {
                                unlink( ROOT_PATH . $img['large'] );
                            }

                            if( file_exists( ROOT_PATH . $img['medium'] ) )
                            {
                                unlink( ROOT_PATH . $img['medium'] );
                            }

                            if( file_exists( ROOT_PATH . $img['small'] ) )
                            {
                                unlink( ROOT_PATH . $img['small'] );
                            }

                            if( file_exists( ROOT_PATH . $img['thumb'] ) )
                            {
                                unlink( ROOT_PATH . $img['thumb'] );
                            }
                        }

                        if( edit_additional_field( $post_id, 'map_location_img', json_encode( $arr ), $_POST['app_name'] ) )
                        {
                            echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name_s . '"}';
                        }
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"File is not an image"}';
                }
            }
            else
            {
                echo '{"result":"failed","error":"Maximum file size is 2MB"}';
            }
        }
    }


    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-featured-images' )
    {
        if( empty( $_POST['post_id'] ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            $featured = get_additional_field( $_POST['post_id'], 'featured_img', $_POST['app_name'] );

            if( !empty( $featured ) )
            {
                $img = json_decode( $featured, true );

                if( !empty( $img ) )
                {
                    if( file_exists( ROOT_PATH . $img['original'] ) )
                    {
                        unlink( ROOT_PATH . $img['original'] );
                    }

                    if( file_exists( ROOT_PATH . $img['large'] ) )
                    {
                        unlink( ROOT_PATH . $img['large'] );
                    }

                    if( file_exists( ROOT_PATH . $img['medium'] ) )
                    {
                        unlink( ROOT_PATH . $img['medium'] );
                    }

                    if( file_exists( ROOT_PATH . $img['small'] ) )
                    {
                        unlink( ROOT_PATH . $img['small'] );
                    }

                    if( file_exists( ROOT_PATH . $img['thumb'] ) )
                    {
                        unlink( ROOT_PATH . $img['thumb'] );
                    }

                    if( delete_additional_field_with_key( $_POST['post_id'], 'featured_img', $_POST['app_name'] ) )
                    {
                        echo '{"result":"success"}';
                    }
                    else
                    {
                        echo '{"result":"failed"}';
                    }
                }
                else
                {
                    echo '{"result":"failed"}';
                }
            }
            else
            {
                echo '{"result":"failed"}';
            }
        }
    }


    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-map-location-images' )
    {
        if( empty( $_POST['post_id'] ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            $featured = get_additional_field( $_POST['post_id'], 'map_location_img', $_POST['app_name'] );

            if( !empty( $featured ) )
            {
                $img = json_decode( $featured, true );

                if( !empty( $img ) )
                {
                    if( file_exists( ROOT_PATH . $img['original'] ) )
                    {
                        unlink( ROOT_PATH . $img['original'] );
                    }

                    if( file_exists( ROOT_PATH . $img['large'] ) )
                    {
                        unlink( ROOT_PATH . $img['large'] );
                    }

                    if( file_exists( ROOT_PATH . $img['medium'] ) )
                    {
                        unlink( ROOT_PATH . $img['medium'] );
                    }

                    if( file_exists( ROOT_PATH . $img['small'] ) )
                    {
                        unlink( ROOT_PATH . $img['small'] );
                    }

                    if( file_exists( ROOT_PATH . $img['thumb'] ) )
                    {
                        unlink( ROOT_PATH . $img['thumb'] );
                    }

                    if( delete_additional_field_with_key( $_POST['post_id'], 'map_location_img', $_POST['app_name'] ) )
                    {
                        echo '{"result":"success"}';
                    }
                    else
                    {
                        echo '{"result":"failed"}';
                    }
                }
                else
                {
                    echo '{"result":"failed"}';
                }
            }
            else
            {
                echo '{"result":"failed"}';
            }
        }
    }


    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'get-gallery-image' )
    {
        $s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND mime_type LIKE %s AND lattach_app_name = %s ORDER BY lorder';
        $q = $db->prepare_query( $s, $_POST['post_id'], '%image%', 'gallery' );
        $r = $db->do_query( $q );

        $data = array();
        $data_image = array();

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $filename = explode( '/', $d['lattach_loc_thumb'] );
                $filename = array_reverse( $filename );

                $attach_language = json_decode($d['lattach_language']);

                $data_image = array(
                    'img_id'              => $d['lattach_id'],
                    'img_title'           => $d['ltitle'],
                    'img_alt'             => $d['lalt_text'],
                    'img_caption'         => $d['lcaption'],
                    'img_youtube_url'     => $d['lyoutube_url'],
                    'img_photo_360'       => $d['lphoto_360'],
                    'img_attach_language' => $attach_language,
                    'img_thumb'           => HTTP . site_url() . '/lumonata-plugins/additional/mthumb.php?src=' . HTTP . site_url() . $d['lattach_loc_thumb'] . '&w=120&h=120'
                );

                // if(!empty($attach_language))
                // {
                //     foreach($attach_language as $key => $value)
                //     {
                //         $data_image += array(
                //             'img_title_'.$key => $value
                //         );
                //     }
                // }

                $data[] = $data_image;
            }
        }

        if( empty( $data ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            $prm['result'] = 'success';
            $prm['data']   = $data;

            echo json_encode( $prm );
        }
    }

    // GET GALLERY IMAGE DESTINATION
    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'get-gallery-dest' )
    {
		$s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND mime_type LIKE %s AND lattach_app_name = %s ORDER BY lorder';
        $q = $db->prepare_query( $s, $_POST['post_id'], '%image%', 'gallery' );
        $r = $db->do_query( $q );

        $data = array();

        if( $db->num_rows( $r ) > 0 )
        {
            while( $d = $db->fetch_array( $r ) )
            {
                $filename = explode( '/', $d['lattach_loc_thumb'] );
                $filename = array_reverse( $filename );

                $data[] = array(
                    'img_id' => $d['lattach_id'],
                    'img_title' => $d['ltitle'],
                    'img_alt' => $d['lalt_text'],
                    'img_caption' => $d['lcaption'],
                    'img_thumb' =>  HTTP . site_url() . '/lumonata-plugins/destinations/mthumb.php?src=' . HTTP . site_url() . $d['lattach_loc_thumb'] . '&w=120&h=120'
                );
            }
        }

        if( empty( $data ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            $prm['result'] = 'success';
            $prm['data']   = $data;

            echo json_encode( $prm );
        }
	}

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-gallery-image' )
    {
        if( isset( $_FILES['file']['name'] ) && $_FILES['file']['error']==0  )
        {
            $file_name   = $_FILES['file']['name'];
            $file_size   = $_FILES['file']['size'];
            $file_type   = $_FILES['file']['type'];
            $file_source = $_FILES['file']['tmp_name'];

            if( !empty( $file_name ) )
            {
                if( is_allow_file_size( $file_size ) )
                {
                    if( is_allow_file_type( $file_type, 'image' ) )
                    {
                        $file_ext  = file_name_filter( $file_name, true );
                        $file_name = file_name_filter( $file_name );
                        $file_name = $file_name . '-' . time() . $file_ext;

                        if( !file_exists( PLUGINS_PATH . '/additional/gallery/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/additional/gallery/' );
                        }

                        if( !file_exists( PLUGINS_PATH . '/additional/gallery/large/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/additional/gallery/large/' );
                        }

                        if( !file_exists( PLUGINS_PATH . '/additional/gallery/medium/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/additional/gallery/medium/' );
                        }

                        if( !file_exists( PLUGINS_PATH . '/additional/gallery/thumb/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/additional/gallery/thumb/' );
                        }

                        $ldestination = PLUGINS_PATH . '/additional/gallery/large/' . $file_name;
                        $mdestination = PLUGINS_PATH . '/additional/gallery/medium/' . $file_name;
                        $tdestination = PLUGINS_PATH . '/additional/gallery/thumb/' . $file_name;
                        $fdestination = PLUGINS_PATH . '/additional/gallery/' . $file_name;

                        $crop   = upload_resize( $file_source, $tdestination, $file_type, 450, 450 );
                        $resize = upload_resize( $file_source, $mdestination, $file_type, 780, 780 );
                        $resize = upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 );
                        $upload = upload( $file_source, $fdestination );

                        if( $resize && $upload )
                        {
                            $loc_origin = '/lumonata-plugins/additional/gallery/' . $file_name;
                            $loc_thumb  = '/lumonata-plugins/additional/gallery/thumb/' . $file_name;
                            $loc_medium = '/lumonata-plugins/additional/gallery/medium/' . $file_name;
                            $loc_large  = '/lumonata-plugins/additional/gallery/large/' . $file_name;

                            if( insert_attachment( $_POST['post_id'], $file_name, $file_type, $loc_origin, $loc_thumb, $loc_medium, $loc_large, null, null, 'gallery' ) )
                            {
                                $attach_id = mysql_insert_id();

                                if( empty($attach_id) )
                                {
                                    echo '{"result":"failed"}';
                                }
                                else
                                {
                                    $s = 'SELECT * FROM lumonata_attachment WHERE lattach_id = %d';
                                    $q = $db->prepare_query( $s, $attach_id );
                                    $r = $db->do_query( $q );
                                    $d = $db->fetch_array( $r );

                                    echo json_encode(
                                        array(
                                            'result' => 'success',
                                            'attach_id' => $attach_id,
                                            'title' => $d['ltitle'],
                                            'alt' => $d['lalt_text'],
                                            'caption' => $d['lcaption']
                                        )
                                    );
                                }
                            }
                        }
                        else
                        {
                            echo '{"result":"failed"}';
                        }
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"File is not an image"}';
                }
            }
            else
            {
                echo '{"result":"failed","error":"Maximum file size is 2MB"}';
            }
        }
    }


    // UPLOAD GALLERY DEST
    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-gallery-dest' )
    {
        if( isset( $_FILES['file']['name'] ) && $_FILES['file']['error']==0  )
        {
            $file_name   = $_FILES['file']['name'];
            $file_size   = $_FILES['file']['size'];
            $file_type   = $_FILES['file']['type'];
            $file_source = $_FILES['file']['tmp_name'];

            if( !empty( $file_name ) )
            {
                if( is_allow_file_size( $file_size ) )
                {
                    if( is_allow_file_type( $file_type, 'image' ) )
                    {
                        $file_ext  = file_name_filter( $file_name, true );
                        $file_name = file_name_filter( $file_name );
                        $file_name = $file_name . '-' . time() . $file_ext;

                        if( !file_exists( PLUGINS_PATH . '/destinations/gallery/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/destinations/gallery/' );
                        }

                        if( !file_exists( PLUGINS_PATH . '/destinations/gallery/large/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/destinations/gallery/large/' );
                        }

                        if( !file_exists( PLUGINS_PATH . '/destinations/gallery/medium/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/destinations/gallery/medium/' );
                        }

                        if( !file_exists( PLUGINS_PATH . '/destinations/gallery/thumb/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/destinations/gallery/thumb/' );
                        }

                        $ldestination = PLUGINS_PATH . '/destinations/gallery/large/' . $file_name;
                        $mdestination = PLUGINS_PATH . '/destinations/gallery/medium/' . $file_name;
                        $tdestination = PLUGINS_PATH . '/destinations/gallery/thumb/' . $file_name;
                        $fdestination = PLUGINS_PATH . '/destinations/gallery/' . $file_name;

                        $crop   = upload_resize( $file_source, $tdestination, $file_type, 450, 450 );
                        $resize = upload_resize( $file_source, $mdestination, $file_type, 780, 780 );
                        $resize = upload_resize( $file_source, $ldestination, $file_type, 1920, 1920 );
                        $upload = upload( $file_source, $fdestination );

                        if( $resize && $upload )
                        {
                            $loc_origin = '/lumonata-plugins/destinations/gallery/' . $file_name;
                            $loc_thumb  = '/lumonata-plugins/destinations/gallery/thumb/' . $file_name;
                            $loc_medium = '/lumonata-plugins/destinations/gallery/medium/' . $file_name;
                            $loc_large  = '/lumonata-plugins/destinations/gallery/large/' . $file_name;

                            if( insert_attachment( $_POST['post_id'], $file_name, $file_type, $loc_origin, $loc_thumb, $loc_medium, $loc_large, null, null, 'gallery' ) )
                            {
                                $attach_id = mysql_insert_id();

                                if( empty($attach_id) )
                                {
                                    echo '{"result":"failed"}';
                                }
                                else
                                {
                                    $s = 'SELECT * FROM lumonata_attachment WHERE lattach_id = %d';
                                    $q = $db->prepare_query( $s, $attach_id );
                                    $r = $db->do_query( $q );
                                    $d = $db->fetch_array( $r );

                                    echo json_encode(
                                        array(
                                            'result'    => 'success',
                                            'attach_id' => $attach_id,
                                            'title'     => $d['ltitle'],
                                            'alt'       => $d['lalt_text'],
                                            'caption'   => $d['lcaption']
                                        )
                                    );
                                }
                            }
                        }
                        else
                        {
                            echo '{"result":"failed"}';
                        }
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"File is not an image"}';
                }
            }
            else
            {
                echo '{"result":"failed","error":"Maximum file size is 2MB"}';
            }
        }
    }


    if( isset($_POST['ajax_key']) && $_POST['ajax_key'] =='edit-gallery-info' )
    {
        if( edit_attachment( $_POST['attach_id'], $_POST['title'], null, $_POST['alt'], $_POST['caption'], $_POST['youtube_url'], $_POST['photo_360'], $_POST['img_title_lang'] ) )
        {
            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] =='delete-gallery-image' )
    {
        if( delete_attachment( $_POST['attach_id'] ) )
        {
            delete_additional_field( $_POST['attach_id'], 'attachment' );

            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] =='reorder-gallery-image' )
    {
        if( isset( $_POST['value'] ) && !empty( $_POST['value'] ) )
        {
            $value = json_decode( $_POST['value'], true );

            foreach( $value as $i=>$val )
            {
                $s = 'UPDATE lumonata_attachment SET lorder=%d WHERE lattach_id=%d AND larticle_id=%d';
                $q = $db->prepare_query( $s, $i, $val, $_POST['id'] );
                $r = $db->do_query( $q );
            }

            echo '{"result":"success"}';
        }
        else
        {
            echo '{"result":"failed"}';
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'upload-rule-featured-images' )
    {
        if( isset( $_FILES['featured_img'] ) && $_FILES['featured_img']['error']==0 )
        {
            $rule_id     = $_POST['rule_id'];
            $file_name   = $_FILES['featured_img']['name'];
            $file_size   = $_FILES['featured_img']['size'];
            $file_type   = $_FILES['featured_img']['type'];
            $file_source = $_FILES['featured_img']['tmp_name'];

            if( is_allow_file_size( $file_size ) )
            {
                if( is_allow_file_type( $file_type, 'image' ) )
                {
                    $file_ext  = file_name_filter( $file_name, true );
                    $file_name = file_name_filter( $file_name );

                    $file_name_t = $file_name . '-' . time() . '-thumbnail' . $file_ext;
                    $file_name_s = $file_name . '-' . time() . '-small' . $file_ext;
                    $file_name_m = $file_name . '-' . time() . '-medium' . $file_ext;
                    $file_name_l = $file_name . '-' . time() . '-large' . $file_ext;
                    $file_name   = $file_name . '-' . time() . $file_ext;

                    if( !file_exists( PLUGINS_PATH . '/additional/featured/' ) )
                    {
                        mkdir( PLUGINS_PATH . '/additional/featured/' );
                    }

                    $destination_t = PLUGINS_PATH . '/additional/featured/' . $file_name_t;
                    $destination_s = PLUGINS_PATH . '/additional/featured/' . $file_name_s;
                    $destination_m = PLUGINS_PATH . '/additional/featured/' . $file_name_m;
                    $destination_l = PLUGINS_PATH . '/additional/featured/' . $file_name_l;
                    $destination   = PLUGINS_PATH . '/additional/featured/' . $file_name;

                    $destination_save_t = '/lumonata-plugins/additional/featured/' . $file_name_t;
                    $destination_save_s = '/lumonata-plugins/additional/featured/' . $file_name_s;
                    $destination_save_m = '/lumonata-plugins/additional/featured/' . $file_name_m;
                    $destination_save_l = '/lumonata-plugins/additional/featured/' . $file_name_l;
                    $destination_save   = '/lumonata-plugins/additional/featured/' . $file_name;

                    upload_crop( $file_source, $destination_t, $file_type, thumbnail_image_width(), thumbnail_image_height(), true );
                    upload_resize( $file_source, $destination_s, $file_type, thumbnail_image_width(), thumbnail_image_height() );
                    upload_resize( $file_source, $destination_m, $file_type, medium_image_width(), medium_image_height() );
                    upload_resize( $file_source, $destination_l, $file_type, large_image_width(), large_image_height() );
                    upload( $file_source, $destination );

                    $cat = get_rule_additional_field( $rule_id, 'featured_img', $_POST['app_name'] );
                    $arr = array(
                        'thumb' => $destination_save_t,
                        'small' => $destination_save_s,
                        'medium' => $destination_save_m,
                        'large' => $destination_save_l,
                        'original' => $destination_save
                    );

                    if( empty( $cat ) )
                    {
                        if( add_rule_additional_field( $rule_id, 'featured_img', json_encode( $arr ), $_POST['app_name'] ) )
                        {
                            echo '{"result":"success","rule_id":"' . $rule_id . '","filename":"' . $file_name_m . '"}';
                        }
                    }
                    else
                    {
                        $img = json_decode( $cat, true );

                        if( !empty( $img ) )
                        {
                            if( file_exists( ROOT_PATH . $img['original'] ) )
                            {
                                unlink( ROOT_PATH . $img['original'] );
                            }

                            if( file_exists( ROOT_PATH . $img['large'] ) )
                            {
                                unlink( ROOT_PATH . $img['large'] );
                            }

                            if( file_exists( ROOT_PATH . $img['medium'] ) )
                            {
                                unlink( ROOT_PATH . $img['medium'] );
                            }

                            if( file_exists( ROOT_PATH . $img['small'] ) )
                            {
                                unlink( ROOT_PATH . $img['small'] );
                            }

                            if( file_exists( ROOT_PATH . $img['thumb'] ) )
                            {
                                unlink( ROOT_PATH . $img['thumb'] );
                            }
                        }

                        if( edit_rule_additional_field( $rule_id, 'featured_img', json_encode( $arr ), $_POST['app_name'] ) )
                        {
                            echo '{"result":"success","rule_id":"' . $rule_id . '","filename":"' . $file_name_s . '"}';
                        }
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"File is not an image"}';
                }
            }
            else
            {
                echo '{"result":"failed","error":"Maximum file size is 2MB"}';
            }
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-rule-featured-images' )
    {
        if( empty( $_POST['rule_id'] ) )
        {
            echo '{"result":"failed"}';
        }
        else
        {
            $featured = get_rule_additional_field( $_POST['rule_id'], 'featured_img', $_POST['app_name'] );

            if( !empty( $featured ) )
            {
                $img = json_decode( $featured, true );

                if( !empty( $img ) )
                {
                    if( file_exists( ROOT_PATH . $img['original'] ) )
                    {
                        unlink( ROOT_PATH . $img['original'] );
                    }

                    if( file_exists( ROOT_PATH . $img['large'] ) )
                    {
                        unlink( ROOT_PATH . $img['large'] );
                    }

                    if( file_exists( ROOT_PATH . $img['medium'] ) )
                    {
                        unlink( ROOT_PATH . $img['medium'] );
                    }

                    if( file_exists( ROOT_PATH . $img['small'] ) )
                    {
                        unlink( ROOT_PATH . $img['small'] );
                    }

                    if( file_exists( ROOT_PATH . $img['thumb'] ) )
                    {
                        unlink( ROOT_PATH . $img['thumb'] );
                    }

                    if( delete_rule_additional_field_with_key( $_POST['rule_id'], 'featured_img', $_POST['app_name'] ) )
                    {
                        echo '{"result":"success"}';
                    }
                    else
                    {
                        echo '{"result":"failed"}';
                    }
                }
                else
                {
                    echo '{"result":"failed"}';
                }
            }
            else
            {
                echo '{"result":"failed"}';
            }
        }
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'add-new-post-field' )
    {
        $content = '';
        $fname   = generate_field_name( $_POST['flabel'] );

        if( $_POST['ftype'] == 0 )
        {
            $content = '
            <li>
                <div class="field-wrapp">
                    <p>' . $_POST['flabel'] . '<a class="delete-field" data-name="' . $fname . '"><img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                    <div class="inner">
                        <input type="text" class="textbox" name="additional_fields[custom_field][' . $_POST['index'] . '][' . $fname . '][' . $_POST['flabel'] . '][' . $_POST['ftype'] . ']" autocomplete="off" value="" />
                    </div>
                </div>
            </li>';
        }
        elseif( $_POST['ftype'] == 1 )
        {
            $content = '
            <li>
                <div class="field-wrapp">
                    <p>' . $_POST['flabel'] . '<a class="delete-field" data-name="' . $fname . '"><img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                    <div class="inner">
                        <textarea class="textarea" name="additional_fields[custom_field][' . $_POST['index'] . '][' . $fname . '][' . $_POST['flabel'] . '][' . $_POST['ftype'] . ']" autocomplete="off"></textarea>
                    </div>
                </div>
            </li>';
        }
        elseif( $_POST['ftype'] == 2 )
        {
            $content = '
            <li>
                <div class="field-wrapp">
                    <p>' . $_POST['flabel'] . '<a class="delete-field" data-name="' . $fname . '"><img src="' . HTTP . site_url() . '/lumonata-plugins/additional/images/delete.png" /></a></p>
                    <div class="inner">
                        <textarea class="textarea tinymce" name="additional_fields[custom_field][' . $_POST['index'] . '][' . $fname . '][' . $_POST['flabel'] . '][' . $_POST['ftype'] . ']" autocomplete="off"></textarea>
                    </div>
                </div>
            </li>';
        }

        echo $content;
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'delete-post-field' )
    {
        $s = 'SELECT lvalue FROM lumonata_additional_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s';
        $q = $db->prepare_query( $s, $_POST['post_id'], 'custom_field', $_POST['app_name'] );
        $r = $db->do_query( $q );
        $d = $db->fetch_array( $r );

        $fields = json_decode( $d['lvalue'], true );

        if( !empty( $fields ) && is_array( $fields ) )
        {
            if( isset( $fields[ $_POST['field'] ] ) )
            {
                unset( $fields[ $_POST['field'] ] );
            }

            if( edit_additional_field( $_POST['post_id'], 'custom_field', json_encode( $fields, JSON_HEX_QUOT | JSON_HEX_TAG ), $_POST['app_name'] ) )
            {
                echo '{"result":"success"}';
            }
            else
            {
                echo '{"result":"failed"}';
            }
        }
        else
        {
            echo '{"result":"success"}';
        }
    }

    //iput add

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'next-page' )
    {
        $page = $_POST['page'];
        $_SESSION['pagging_news'] = $page;

        $return['url'] =  HTTP . SITE_URL. '/news-blog/';
        echo json_encode($return);
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'search-newsblog' )
    {
        $text = $_POST['text'];
        $active_url = $_POST['active_url'];
        $topicid = $_POST['topicid'];

        if ($topicid == '') {
            $return['url'] =  HTTP . SITE_URL.'/'.$active_url.'/'.$text;
        }
        else{
            $return['url'] = HTTP . SITE_URL.'/'.$active_url.'/'.$topicid.'/'.$text;
        }

        echo json_encode($return);
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'see-more-news' )
    {
        $idlastnews     = $_POST['idlastnews'];
        $view           = $_POST['view'];
        $search_value   = $_POST['search_value'];
        $cat_value      = $_POST['cat_value'];
        $content        = '';
        $button         = '';

        // hasil search di kategori all news
        if ($search_value != '' && $cat_value != '') {
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, $cat_value, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%' ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, $cat_value, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%', $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        // kategori all news
        else if ($search_value == '' && $cat_value != '') {
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, $cat_value ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, $cat_value , $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        // hasil search all news
        else if ($search_value != '') {
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%' ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%', $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        // all news
        else{
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'news', 'brief', 'news', 'categories', $idlastnews, $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        if ($n > 0) {

            $content .= '<div class="container-all-news-list new clearfix">';

            while ($d = $db->fetch_array($r)) {
                $idart = $d['larticle_id'];

                $date = date('M d, Y', strtotime($d['lpost_date']));

                $content .= '<div class="news-list clearfix">
                                <div class="content-article clearfix">
                                    <a href="' . HTML .SITE_URL.'/news/'.$d['lsef'].'.html" class="title">'.$d['larticle_title'].'</a>
                                    <p class="date">'.$date.'</p>
                                    <p class="divider">•</p>
                                    <p class="topic">'.ucwords(strtolower($d['lname'])).'</p>
                                    <div class="brief clearfix">
                                        <p>'.$d['lvalue'].'</p>
                                    </div>
                                    <a href="' . HTML .SITE_URL.'/news/'.$d['lsef'].'.html" class="read-more">Read More</a>
                                </div>
                            </div>';
            }

            $content .= '</div>';

            if ($nc > $showLimit) {
                $next_view = $view;
                $button .= '<a class="load-more" onclick="see_more_news('.$idart.','.$next_view.')">See More News</a>';
            }
        }

        $return['content'] = $content;
        $return['button'] = $button;

        echo json_encode($return);
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'see-more-blog' )
    {
        $idlastblog     = $_POST['idlastblog'];
        $view           = $_POST['view'];
        $search_value   = $_POST['search_value'];
        $cat_value      = $_POST['cat_value'];
        $content        = '';
        $button         = '';


        // hasil search di kategori all news
        if ($search_value != '' && $cat_value != '') {
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, $cat_value, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%' ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, $cat_value, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%', $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        // kategori all news
        else if ($search_value == '' && $cat_value != '') {
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, $cat_value ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d AND r.lsef = %s
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, $cat_value , $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        // hasil search all news
        else if ($search_value != '') {
            //count all rows except already displayed

            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%' ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc); //echo $nc;

            $showLimit = $view;

            //get rows query

            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                  INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                  AND (a.larticle_title LIKE %s OR a.larticle_content LIKE %s OR b.lvalue LIKE %s)
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, '%'.$search_value.'%', '%'.$search_value.'%', '%'.$search_value.'%', $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r ); //echo $n;
        }

        else{

            //count all rows except already displayed

            /*$sc = 'SELECT COUNT(a.larticle_id) FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_additional_fields c ON c.lapp_id = a.larticle_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND c.lkey=%s AND c.lapp_name=%s AND a.larticle_id < %d
                  ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'blogs', 'brief', 'blogs', 'related_news', 'blogs', $idlastblog ); */
            $sc = 'SELECT a.larticle_id FROM lumonata_articles a
                INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                ORDER BY a.lpost_date DESC';

            $qc = $db->prepare_query( $sc, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog ); //echo $qc;
            $rc = $db->do_query($qc);
            $nc = $db->num_rows($rc);

            $showLimit = $view;

            //get rows query

            /*$s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue brief, c.lvalue related_news FROM lumonata_articles a
                  INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                  INNER JOIN lumonata_additional_fields c ON c.lapp_id = a.larticle_id
                  WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND c.lkey=%s AND c.lapp_name=%s AND a.larticle_id < %d
                  ORDER BY a.lpost_date DESC
                  LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'blogs', 'brief', 'blogs', 'related_news', 'blogs', $idlastblog, $showLimit );*/
            $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lpost_date, a.lsef, b.lvalue, r.lname FROM lumonata_articles a
                INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
                INNER JOIN lumonata_rule_relationship rr ON a.larticle_id = rr.lapp_id
                INNER JOIN lumonata_rules r ON rr.lrule_id = r.lrule_id
                WHERE a.larticle_status=%s AND a.larticle_type=%s AND b.lkey=%s AND b.lapp_name=%s AND r.lrule=%s AND a.larticle_id < %d
                ORDER BY a.lpost_date DESC
                LIMIT %d';

            $q = $db->prepare_query( $s, 'publish', 'blogs', 'brief', 'blogs', 'categories', $idlastblog, $showLimit ); //echo $q;

            $r = $db->do_query( $q );
            $n = $db->num_rows( $r );

        }

        if ($n > 0) {

            $content .= '<div class="container-all-news-list new clearfix">';

            while ($d = $db->fetch_array($r)) {
                $idart = $d['larticle_id'];

                $date = date('M d, Y', strtotime($d['lpost_date']));

                // $nid        = json_decode($d['related_news'], true);

                // $rule_id    = get_rule_relationship_rule_id( $nid[0] ); //echo $rule_id;
                // $rule_name  = get_news_rule_name_by_id( $rule_id );

                $brief      = $d['lvalue'];
                $con_post   = $d['larticle_content'];

                if (empty( $brief )) {
                    $cont_ex    = explode('.</p>', $con_post, 2);
                    $brief      = $cont_ex[0];
                }
                else{
                    $brief = $brief;
                }

                $content .= '<div class="news-list clearfix">
                                <div class="content-article clearfix">
                                    <a href="' . HTML .SITE_URL.'/blogs/'.$d['lsef'].'.html" class="title">'.$d['larticle_title'].'</a>
                                    <p class="date">'.$date.'</p>
                                    <p class="divider">•</p>
                                    <p class="topic">'.ucwords(strtolower($d['lname'])).'</p>
                                    <div class="brief clearfix">
                                        <p>'.$brief.'</p>
                                    </div>
                                    <a href="' . HTML .SITE_URL.'/blogs/'.$d['lsef'].'.html" class="read-more">Read More</a>
                                </div>
                            </div>';
            }

            $content .= '</div>';

            if ($nc > $showLimit) {
                $next_view = $view+3;
                $button .= '<a class="load-more" onclick="see_more_blogs('.$idart.','.$next_view.')">See More Blogs</a>';
            }
        }

        $return['content'] = $content;
        $return['button'] = $button;

        echo json_encode($return);
    }

    if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'see-more-past-events' )
    {
        $idlastevent    = $_POST['idlastevent'];
        $view           = $_POST['view'];
        $content        = '';
        $button         = '';

        $datetoday = date( 'Y-m-d', time() );

        //count all rows except already displayed

        $sc = 'SELECT larticle_id, larticle_title, larticle_content, lstart_date, lend_date, lsef
               FROM lumonata_articles
               WHERE larticle_status=%s AND larticle_type=%s AND lstart_date<%s AND larticle_id < %d
               ORDER BY lstart_date DESC';

        $qc = $db->prepare_query( $sc, 'publish', 'events', $datetoday.' 00:00:00', $idlastevent );
        $rc = $db->do_query($qc);
        $nc = $db->num_rows($rc);

        $showLimit = $view;

        //get rows query

        $s = 'SELECT a.larticle_id, a.larticle_title, a.larticle_content, a.lstart_date, a.lend_date, a.lsef, b.lvalue brief
              FROM lumonata_articles a
              INNER JOIN lumonata_additional_fields b ON b.lapp_id = a.larticle_id
              WHERE a.larticle_status=%s AND a.larticle_type=%s AND a.lstart_date<%s AND a.larticle_id < %d AND b.lkey=%s AND b.lapp_name=%s
              ORDER BY a.lstart_date DESC
              LIMIT %d';

        $q = $db->prepare_query( $s, 'publish', 'events', $datetoday.' 00:00:00', $idlastevent, 'brief', 'events', $showLimit ); //echo $q;

        $r = $db->do_query( $q );
        $n = $db->num_rows( $r );

        if ($n > 0) {

            $content .= '<div class="container-event-list new clearfix">';

            while ($d = $db->fetch_array($r)) {
                $idart = $d['larticle_id'];

                $datestart  = date('d' ,strtotime($d['lstart_date']));
                $monstart   = date('F' ,strtotime($d['lstart_date']));
                $yearstart  = date('Y' ,strtotime($d['lstart_date']));
                $dateend    = date('d' ,strtotime($d['lend_date']));
                $monend     = date('F' ,strtotime($d['lend_date']));
                $yearend    = date('Y' ,strtotime($d['lend_date']));

                if ($yearend != $yearstart) {
                    $datetext = $monstart.' '.$datestart.', '.$yearstart.' - '.$monend.' '.$dateend.', '.$yearend;
                }
                else{
                    if ($monend != $monstart) {
                        $datetext = $monstart.' '.$datestart.' – '.$monend.' '.$dateend.', '.$yearstart;
                    }
                    else if ($datestart == $dateend) {
                        $datetext = $monstart.' '.$datestart.', '.$yearstart;
                    }
                    else {
                        $datetext = $monstart.' '.$datestart.' – '.$dateend.', '.$yearstart;
                    }
                }

                $brief      = $d['brief'];
                $con_post   = $d['larticle_content'];

                if (empty( $brief )) {
                    $cont_ex    = explode('.</p>', $con_post, 2);
                    $brief      = $cont_ex[0];
                }
                else{
                    $brief = $brief;
                }

                $content .= '<div class="event-list clearfix">
                                <p class="header-article">'.$datetext.'</p>
                                <div class="content-article clearfix">
                                    <a href="' . HTML .SITE_URL.'/events/'.$d['lsef'].'.html" class="article-title">'.$d['larticle_title'].'</a>
                                    <div class="article-brief clearfix">
                                        <p>'.$brief.'</p>
                                    </div>
                                    <a href="' . HTML .SITE_URL.'/events/'.$d['lsef'].'.html" class="read-more">Read More</a>
                                </div>
                            </div>';
            }

            $content .= '</div>';

            if ($nc > $showLimit) {
                $next_view = $view+5;
                $button .= '<a class="load-more" onclick="see_more_past_event('.$idart.','.$next_view.')">See More Past Events</a>';
            }
        }

        $return['content'] = $content;
        $return['button'] = $button;

        echo json_encode($return);
    }

    // if( isset( $_POST['ajax_key'] ) && $_POST['ajax_key'] == 'get-gallery-dest' )
    // {
	// 	echo '{"result":"failed"}';
	// }

    exit;
}


function get_destination_option($post_id, $apps)
{
    $destination = get_additional_field($post_id, 'destination', $apps);

    return '<option value="dinara" selected >Dinara</option>';
}


function get_form_villa_map_upload($id, $apps)
{
    $map_image = get_map_location_image($id, $apps);

    $html = '
    <div class="additional_data">
        <h2>Map Location Image</h2>
        <div class="additional_content additional-featured-img">
            <div class="slider">
                <h3>Select an image file on your computer. (Recommended maximum upload size <b>2MB</b> ):</h3>
                <div class="villa-location-img clearfixed">'.$map_image.'</div>
                <input type="file" name="map_location_img" autocomplete="off" />
            </div>
        </div>
    </div>
    ';

    return $html;
}


function get_map_location_image( $post_id, $apps, $html = true )
{
    $content  = $html ? '' : array();
    $featured = get_additional_field( $post_id, 'map_location_img', $apps );

    if( !empty( $featured ) )
    {
        $img = json_decode( $featured, true );

        if( !empty( $img ) )
        {
            if( $html )
            {
                $content .= '
                <div class="box">
                    <img src="' . HTTP . site_url() . $img['medium'] . '" />
                    <div class="overlay">
                        <a data-post-id="' . $post_id . '" class="img-delete" title="Delete">
                            <img src="' . HTTP  . site_url() . '/lumonata-plugins/additional/images/delete.png">
                        </a>
                    </div>
                </div>';
            }
            else
            {
                $content = array(
                    'original' => $img['original'],
                    'medium' => $img['medium'],
                    'large' => $img['large'],
                    'small' => $img['small'],
                    'thumb' => $img['thumb']
                );
            }
        }
    }

    return $content;
}

?>
