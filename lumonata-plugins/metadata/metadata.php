<?php

/*
Plugin Name: Lumonata Meta Data
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding Meta Data in each post like Meta Title, Meta Keywords and Meta Description.
Author: Wahya Biantara
Author URL: http://wahya.biantara.com/
Version: 1.0.1
*/

/*
* This function is used to add meta data additional fields on each articles and pages applications.
* 
* @since 1.0.0
* 
* @return string Will return the input type of meta title, meta keywords and meta description on each time you try to update or insert artilces
* */
add_apps_privileges('metadata', 'Meta Data');

function meta_data()
{
    global $thepost;

    $i       = $thepost->post_index;
    $post_id = $thepost->post_id;

    $mtitle = '';
    $mkey   = '';
	$mdes   = '';
	
	if( is_edit() || is_edit_all() )
	{
		$metaKey = $_GET['state'] == 'applications' ? $_GET['sub'] : $_GET['state'];
		$mtitle  = get_additional_field( $post_id, 'meta_title', $metaKey );
		$mkey    = get_additional_field( $post_id, 'meta_keywords', $metaKey );
		$mdes    = get_additional_field( $post_id, 'meta_description', $metaKey );

		return '
		<div class="clearfix" style="padding:5px;">
			<p>
				Title:<br /><br />
				<input type="text" class="textbox forclone" name="additional_fields[meta_title]['.$i.']" value="'.$mtitle.'" rel="input" data-name="meta_title" autocomplete="off" />
			</p>
			<p>
				Description:<br /><br />
				<textarea class="textarea forclone" name="additional_fields[meta_description]['.$i.']" rel="textarea" data-name="meta_description" autocomplete="off">'.$mdes.'</textarea>
			</p>
			<p>
				Keywords:<br /><br />
				<input type="text" class="textbox forclone" name="additional_fields[meta_keywords]['.$i.']" value="'.$mkey.'" rel="input" data-name="meta_keywords" autocomplete="off" />
			</p>
		</div>';
	}
	else
	{
		return '
		<div class="clearfix" style="padding:5px;">
			<p>
				Title:<br /><br />
				<input type="text" class="textbox forclone" name="additional_fields[meta_title]['.$i.']" rel="input" data-name="meta_title" autocomplete="off" />
			</p>
			<p>
				Description:<br /><br />
				<textarea class="textarea forclone" name="additional_fields[meta_description]['.$i.']" rel="textarea" data-name="meta_description" autocomplete="off"></textarea>
			</p>
			<p>
				Keywords:<br /><br />
				<input type="text" class="textbox forclone" name="additional_fields[meta_keywords]['.$i.']" rel="input" data-name="meta_keywords" autocomplete="off" />
			</p>
		</div>';
	}
}

/*
* This function is used to get the Meta Title on each articles that exist on lumonata_additional_fileds table
* 
* @since 1.0.0
* 
* @return string Return the meta title on each article
* 
*/
function get_meta_title($id='',$appname='')
{
	$id = (empty($id) ? post_to_id() : $id);
	$appname = (empty($appname) ? get_appname() : $appname);

	if(!empty($id))
	{
		return strip_tags(get_additional_field($id,'meta_title',$appname));
	}
}
// function get_meta_title( $title='' )
// {
// 	return "asdad";
// 	if( empty( $title ) )
// 	{
// 		$id = post_to_id();
		

// 	    if( !empty( $id ) )
// 	    {
// 	        return strip_tags( get_additional_field( $id, 'meta_title', get_appname() ) );
// 	    }	
// 	}
// 	else
// 	{
// 		return strip_tags( $title );	
// 	}
// }

/*
* This function is used to get the Meta Keywords on each articles that exist on lumonata_additional_fileds table
* 
* @since 1.0.0
* 
* @return string Return the meta keywords on each article
* 
*/
function get_meta_keywords($id='',$appname='')
{
	$id = (empty($id) ? post_to_id() : $id);
	$appname = (empty($appname) ? get_appname() : $appname);

	if(!empty($id))
	{
		$meta_keywords = strip_tags(get_additional_field($id,'meta_keywords',$appname));

		if(!empty($meta_keywords))
		{
			return '<meta name="keywords" content="'.$meta_keywords.'" />';
		}
	}
} 
// function get_meta_keywords( $keywords='' )
// {
// 	if( empty( $keywords ) )
// 	{
// 	    $id = post_to_id();

// 	    if( !empty( $id ) )
// 	    {
// 	        $meta_keywords = strip_tags( get_additional_field( $id, 'meta_keywords', get_appname() ) );

// 	        if( !empty( $meta_keywords ) )
// 	        {
// 	            return '<meta name="keywords" value="' . $meta_keywords . '" />';
// 	        }
// 	    }
// 	}
// 	else
// 	{
// 		return '<meta name="keywords" value="' . strip_tags( $keywords ) . '" />';
// 	}
// }

function get_meta_description_tag($value){
	return '<meta name="description" content="'.$value.'" />';
}

function get_meta_keywords_tag($value){
	return '<meta name="keywords" content="'.$value.'" />';
}

/*
* This function is used to get the Meta Description on each articles that exist on lumonata_additional_fileds table
* 
* @since 1.0.0
* 
* @return string Return the meta description on each article
* 
*/
function get_meta_description($id='',$appname='')
{
	$id = (empty($id) ? post_to_id() : $id);
	$appname = (empty($appname) ? get_appname() : $appname);

	if(!empty($id))
	{
		$meta_description = strip_tags(get_additional_field($id,'meta_description',$appname));

		if(!empty($meta_description))
		{
			return $meta_description;
		}
	}
}
// function get_meta_description( $description='' )
// {
// 	if( empty( $description ) )
// 	{
// 	    $id = post_to_id();

// 	    if( !empty( $id ) )
// 	    {
// 	        $meta_description = strip_tags( get_additional_field( $id, 'meta_description', get_appname() ) );

// 	        if( !empty( $meta_description ) )
// 	        {
// 	            return '<meta name="description" value="' . $meta_description . '" />';
// 	        }
// 	    }
// 	}
// 	else
// 	{
// 		return '<meta name="description" value="' . strip_tags( $description ) . '" />';
// 	}
// }

function get_meta_og_url($og_url)
{
	$result = (empty($og_url) ? '' : '<meta property="og:url" content="'.$og_url.'" />');
	return $result;
}

function get_meta_og_title($og_title)
{
	$result = (empty($og_title) ? '' : '<meta property="og:title" content="'.$og_title.'" />');
	return $result;
}

function get_meta_og_image($og_image)
{
	
	$result = (empty($og_image) ? '<meta property="og:image" content="'.HTTP.TEMPLATE_URL.'/images/logo-1color.png" />' : '<meta property="og:image" content="'.$og_image.'" />');
	return $result;
}

function get_meta_og_site_name($og_site_name)
{
	$result = (empty($og_site_name) ? '' : '<meta property="og:site_name" content="'.rtrim($og_site_name).'" />');
	return $result;
}

function get_meta_og_description($og_description)
{
	$result = (empty($og_description) ? '<meta property="og:description" content="" />' : '<meta property="og:description" content="'.strip_tags($og_description).'" />');
	return $result;
}

/*
* This function is used to set meta data that will shown in home page. This application will be appear in Applications Sub Menu
* 
* @since 1.0.0
* 
* @return string Return the input area of meta data and will save it into lumonata_meta_data table
* 
*/
function set_home_meta_data()
{
	$alert = '';

    if( isset( $_POST['save_changes'] ) )
    {
        foreach( $_POST as $key => $val )
        {
            if( $key != "save_changes" )
            {
                if( find_meta_data( $key ) )
                {
                    $update = update_meta_data( $key, $val );
                }
                else
                {
                    $update = set_meta_data( $key, $val );
                }
            }
        }

        if( $update )
        {
            $alert = '<div class="alert_green_form">Meta data has been updated.</div>';
        }
        else
        {
            $alert = '<div class="alert_green_form">Meta data failed to update.</div>';
        }
    }

    $meta_title       = get_meta_data( 'meta_title' );
    $meta_keywords    = get_meta_data( 'meta_keywords' );
    $meta_description = get_meta_data( 'meta_description' );

    add_actions( 'section_title', 'Meta Data' );

    $meta_content = '
    <h1>Meta Data</h1>
	<p>This settings will change your meta data on your home page</p>
	<div class="tab_container">
		<div class="single_content">
			'.$alert.'
			<form method="post" action="#">
				<fieldset>
					<p><label>Meta Title:</label></p>
					<input type="text" id="meta_title" name="meta_title" value="'.( empty( $meta_title ) ? '' : $meta_title ).'" class="textbox" />
			    </fieldset>
			    <fieldset>
					<p><label>Meta Keywords:</label></p>
					<input type="text" id="meta_keywords" name="meta_keywords" value="'.( empty( $meta_keywords ) ? '' : $meta_keywords ).'" class="textbox" />
			    </fieldset>
			    <fieldset>
					<p><label>Meta Description:</label></p>
					<textarea class="textarea" type="text" name="meta_description">'.( empty( $meta_description ) ? '' : $meta_description ).'</textarea>
			    </fieldset>
			    <div class="button_wrapper clearfix">
					<ul class="button_navigation">
						<li>' . save_changes_botton() . '</li>
					</ul>
			    </div>
			</form>
		</div>
	</div>';

	return $meta_content;
}

/*
* This is the first important step that you have to do.
* Who are allowed to access the application
*/

/*
* Add the meta data input interface into each proccess in administrator area.
*/
if( is_edit_all() && !( is_save_draft() || is_publish() ) )
{
    foreach( $_POST['select'] as $index => $post_id )
    {
        add_actions( 'articles_additional_data_' . $index, 'additional_data', 'Meta Data', 'meta_data' );
        add_actions( 'page_additional_data_' . $index, 'additional_data', 'Meta Data', 'meta_data' );
    }
}
else
{
	add_actions( 'articles_additional_data', 'additional_data', 'Meta Data', 'meta_data' );
    add_actions( 'page_additional_data', 'additional_data', 'Meta Data', 'meta_data' );
    // add_actions( 'page_language_data', 'additional_data', 'Meta Data', 'meta_data' );
}

/* 
* Select the conditions where to set the meta data.
*/
if( is_home() )
{
    $meta_title       = get_meta_data( 'meta_title' );
    $meta_keywords    = get_meta_data( 'meta_keywords' );
    $meta_description = get_meta_data( 'meta_description' );

    add_actions( 'meta_title', get_meta_title( $meta_title ) );
    add_actions( 'meta_keywords', get_meta_keywords( $meta_keywords ) );
    add_actions( 'meta_description', get_meta_description( $meta_description ) );
}

/* 
* Add sub menu under applications menu
*/
add_apps_menu( array( 'metadata' => 'Meta Data' ) );

/*
* After adding sub menu, Now we have to add actions that will taking the process to set the meta data for the home page
* Please keep in mind that the name of the menu variable(metadata), must be the same with the name of actions varibale bellow
*/
add_actions( 'metadata', 'set_home_meta_data' );

?>