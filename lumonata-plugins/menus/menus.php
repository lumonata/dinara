<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
Plugin Name: Lumonata Restaurant Menus
Plugin URL: http://lumonata.com/
Description: This plugin is use for adding restaurant menus.
Author: Ngurah Rai
Author URL: http://lumonata.com/
Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

add_actions( 'menus-ajax_page', 'execute_menus_ajax_request' );

//-- Add the meta data input interface
//-- into each proccess in administrator area. 
$apps_array = array( 'dining' );

if( is_edit_all() && !( is_save_draft() || is_publish() ) )
{
    foreach( $_POST[ 'select' ] as $index => $post_id )
    {
        foreach( $apps_array as $apps )
        {
            add_actions( $apps . '_additional_filed_' . $index, 'menus_data_func', $apps );
            add_actions( $apps . '_additional_delete_' . $index, 'menus_delete_func', $apps );
        }
    }
}
else
{
    foreach( $apps_array as $apps )
    {
        add_actions( $apps . '_additional_filed', 'menus_data_func', $apps );
        add_actions( $apps . '_additional_delete', 'menus_delete_func', $apps );
    }
}

set_previous_menu_data();

function set_previous_menu_data()
{
    global $db;

    //-- Create Table lumonata_menu_category
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_menu_category` (
              `lcmenu_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `larticle_id` bigint(20) NOT NULL,
              `lcmenu_parent` bigint(20) DEFAULT NULL,
              `lcmenu_name` varchar(100) DEFAULT NULL,
              `lorder` bigint(20) DEFAULT "0",
          PRIMARY KEY (`lcmenu_id`)
         ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Create Table lumonata_menu
    $q = 'CREATE TABLE IF NOT EXISTS `lumonata_menu` (
              `lmenu_id` bigint(20) NOT NULL AUTO_INCREMENT,
              `lcmenu_id` bigint(20) NOT NULL,
              `lmenu_name` varchar(150) DEFAULT NULL,
              `lmenu_desc` text,
              `lmenu_type` longtext,
              `lmenu_price` varchar(150) NOT NULL DEFAULT "0",
              `lorder` bigint(20) NOT NULL DEFAULT "0",
          PRIMARY KEY (`lmenu_id`), KEY `lcmenu_id` (`lcmenu_id`),
          CONSTRAINT `lumonata_menu_ibfk_1` FOREIGN KEY (`lcmenu_id`) REFERENCES `lumonata_menu_category` (`lcmenu_id`) ON UPDATE NO ACTION
        ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8';
    $r = $db->do_query( $q );

    //-- Count data on table lumonata_menu_category
    $q = 'SELECT COUNT(lcmenu_id) AS cmenu_count FROM lumonata_menu_category';
    $r = $db->do_query( $q );
    $d = $db->fetch_array( $r );

    //-- Count data on table lumonata_menu
    $q2 = 'SELECT COUNT(lmenu_id) AS menu_count FROM lumonata_menu';
    $r2 = $db->do_query( $q2 );
    $d2 = $db->fetch_array( $r2 );

    //-- If data empty run the script
    if( $d['cmenu_count'] == 0 && $d2['menu_count'] == 0 )
    {
        $s = 'SELECT
                b.lvalue,
                a.larticle_id,
                a.larticle_title
              FROM lumonata_articles AS a
              LEFT JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
              WHERE b.lkey = %s';
        $q = $db->prepare_query( $s, 'food_drink_menu_category' );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            if( empty( $d['lvalue'] ) )
            {
                continue;
            }

            $objects = json_decode( $d['lvalue'], true );

            if ( $objects !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $objects as $obj )
                {
                    $s2 = 'INSERT INTO lumonata_menu_category( larticle_id, lcmenu_parent, lcmenu_name, lorder ) VALUES( %d, %d, %s, %d )';
                    $q2 = $db->prepare_query( $s2, $d['larticle_id'], 0, $obj['name'], $i );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $lcmenu_id = mysql_insert_id();

                        if( isset( $obj['child'] ) && !empty( $obj['child'] ) )
                        {
                            $ii = 0;

                            foreach( $obj['child'] as $child )
                            {
                                $s3 = 'INSERT INTO lumonata_menu_category( larticle_id, lcmenu_parent, lcmenu_name, lorder ) VALUES( %d, %d, %s, %d )';
                                $q3 = $db->prepare_query( $s3, $d['larticle_id'], $lcmenu_id, $child['name'], $ii );

                                $db->do_query( $q3 );

                                $ii++;
                            }
                        }
                    }

                    $i++;
                }
            }
        }

        $s = 'SELECT
                b.lvalue,
                a.larticle_id,
                a.larticle_title
              FROM lumonata_articles AS a
              LEFT JOIN lumonata_additional_fields AS b ON a.larticle_id = b.lapp_id
              WHERE b.lkey = %s';
        $q = $db->prepare_query( $s, 'food_drink_menu' );
        $r = $db->do_query( $q );

        while( $d = $db->fetch_array( $r ) )
        {
            if( empty( $d['lvalue'] ) )
            {
                continue;
            }

            $objects = json_decode( $d['lvalue'], true );

            if ( $objects !== null && json_last_error() === JSON_ERROR_NONE )
            {
                $i = 0;

                foreach( $objects as $obj )
                {
                    $ct_arr = explode( '|', $obj['category'] );
                    $lcname = trim( end( $ct_arr ) );                

                    $s2 = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE lcmenu_name = %s AND larticle_id = %d';
                    $q2 = $db->prepare_query( $s2, $lcname, $d['larticle_id'] );
                    $r2 = $db->do_query( $q2 );

                    if( $db->num_rows( $r2 ) )
                    {
                        $d2 = $db->fetch_array( $r2 );

                        $s3 = 'INSERT INTO lumonata_menu( lcmenu_id, lmenu_name, lmenu_desc, lmenu_price, lorder ) VALUES( %d, %s, %s, %s, %d )';
                        $q3 = $db->prepare_query( $s3, $d2['lcmenu_id'], $obj['name'], $obj['desc'], $obj['price'], $i );
                        
                        $db->do_query( $q3 );
                    }

                    $i++;
                }
            }
        }
    }
}

function menus_data_func( $apps )
{
    global $thepost;
    
    $i       = $thepost->post_index;
    $post_id = $thepost->post_id;
    
    if( is_publish() )
    {
        menus_sync_func( $post_id, $i );
        set_menu_pdf( $post_id, $apps );
    }

    set_template( PLUGINS_PATH . '/menus/menus-form.html', 'menus_template_' . $i );

    add_block( 'menus_block', 'adt', 'menus_template_' . $i );

    $current_file = "";
    $old_pdf      = get_additional_field( $post_id, 'menu_pdf', $apps );
    if( !empty( $old_pdf ) && file_exists( PLUGINS_PATH . '/menus/pdf/' . $old_pdf ) )
    {
        $current_file = '
            <div class="menu-pdf-file file-pdf-current">
                <p>Current File</p>
                <a href="'.URL_PLUGINS . 'menus/pdf/' . $old_pdf.'">'.$old_pdf.'</a>
                <span class="delete-pdf"><img src="'.URL_PLUGINS . 'menus/images/delete.png" /></span>
            </div>
        ';
    }
    add_variable( 'current_file', $current_file );

    add_variable( 'category_id_before', time() );
    add_variable( 'i', $i );
    add_variable( 'app_name', $apps );
    add_variable( 'post_id', $post_id );
    add_variable( 'site_url', SITE_URL );
    add_variable( 'ajax_url', SITE_URL . '/menus-ajax/' );
    add_variable( 'admin_url', SITE_URL . '/lumonata-admin' );
    add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/menus' );
    add_variable( 'language_button_menus', attemp_actions( 'menus_language_button' ) );
    add_variable( 'menu_list_language_data', attemp_actions( 'menu_list_language_data' ) );
    add_variable( 'menu_category_language_data', attemp_actions( 'menu_category_language_data' ) );
    
    add_variable( 'action_type', $_GET[ 'prc' ] );
    
    parse_template( 'menus_block', 'adt', false );

    return return_template( 'menus_template_' . $i );
}

function menus_sync_func( $post_id, $index )
{
    global $db;

    if( empty( $post_id ) )
    {
        return;
    }

    $s = 'UPDATE lumonata_menu_category SET larticle_id = %d WHERE larticle_id = %d';
    $q = $db->prepare_query( $s, $post_id, $_POST[ 'post_id' ][ $index ] );
    $r = $db->do_query( $q );
}

function menus_delete_func( $apps )
{
    global $db;
    
    if( isset( $_POST[ 'id' ] ) && !empty( $_POST[ 'id' ] ) )
    {
        if( is_array( $_POST[ 'id' ] ) )
        {
            foreach( $_POST[ 'id' ] as $post_id )
            {
                $s = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE larticle_id = %d';
                $q = $db->prepare_query( $s, $post_id );
                $r = $db->do_query( $q );

                if( $db->num_rows( $r ) )
                {
                    while( $d = $db->fetch_array( $r ) )
                    {
                        $s2 = 'DELETE FROM lumonata_menu WHERE lcmenu_id = %d';
                        $q2 = $db->prepare_query( $s2, $d['lcmenu_id'] );
                        
                        if( $db->do_query( $q2 ) )
                        {
                            $s3 = 'DELETE FROM lumonata_menu_category WHERE larticle_id = %d';
                            $q3 = $db->prepare_query( $s3, $post_id );
                            
                            $db->do_query( $q3 );
                        }
                    }
                }
            }
        }
        else
        {
            $s = 'SELECT lcmenu_id FROM lumonata_menu_category WHERE larticle_id = %d';
            $q = $db->prepare_query( $s, $_POST[ 'id' ] );
            $r = $db->do_query( $q );

            if( $db->num_rows( $r ) )
            {
                while( $d = $db->fetch_array( $r ) )
                {
                    $s2 = 'DELETE FROM lumonata_menu WHERE lcmenu_id = %d';
                    $q2 = $db->prepare_query( $s2, $d['lcmenu_id'] );
                    
                    if( $db->do_query( $q2 ) )
                    {
                        $s3 = 'DELETE FROM lumonata_menu_category WHERE larticle_id = %d';
                        $q3 = $db->prepare_query( $s3, $_POST[ 'id' ] );
                        
                        $db->do_query( $q3 );
                    }
                }
            }
        }
    }
}

function set_menu_pdf( $post_id, $apps )
{
    if( empty( $post_id ) )
    {
        return;
    }
    
    $old_pdf      = get_additional_field( $post_id, 'menu_pdf', $apps );
    
    if( isset( $_FILES[ 'menu_pdf' ] ) && $_FILES[ 'menu_pdf' ][ 'error' ] == 0 )
    {
        //-- CHECK FOLDER EXIST
        if( !file_exists( PLUGINS_PATH . '/menus/pdf/' ) )
        {
            mkdir( PLUGINS_PATH . '/menus/pdf/' );
        }
        
        $file_name   = $_FILES[ 'menu_pdf' ][ 'name' ];
        $file_size   = $_FILES[ 'menu_pdf' ][ 'size' ];
        $file_type   = $_FILES[ 'menu_pdf' ][ 'type' ];
        $file_source = $_FILES[ 'menu_pdf' ][ 'tmp_name' ];

        $pdf = '';
        
        if( is_allow_file_size( $file_size, 3145728 ) )
        {
            if( is_allow_file_type( $file_type, 'pdf' ) )
            {
                $fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);				
                $fix_file_name = $fix_file_name.$file_ext;

                $destination = PLUGINS_PATH . '/menus/pdf/' . $fix_file_name;
                
                if( upload( $file_source, $destination ) )
                {
                    $pdf = $fix_file_name;
                    
                    if( !empty( $old_pdf ) && file_exists( PLUGINS_PATH . '/menus/pdf/' . $old_pdf ) )
                    {
                        unlink( PLUGINS_PATH . '/menus/pdf/' . $old_pdf );
                    }
                }
            }
        }
    }
    else
    {
        $pdf = $old_pdf;
    }
    
    edit_additional_field( $post_id, 'menu_pdf', $pdf, $apps );
}


function get_additional_menu_field( $app_id, $key, $app_name )
{
    global $db;
    $sql = $db->prepare_query( "SELECT lvalue
					FROM
					lumonata_additional_menu_fields
					WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s", $app_id, $key, $app_name ); //echo $sql;
    $r   = $db->do_query( $sql ); 
    $d   = $db->fetch_array( $r );
    
    return stripslashes( $d['lvalue'] );
    
}

function get_menu_category_option( $post_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_menu_category WHERE larticle_id = %d ORDER BY lcmenu_parent, lorder ASC';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );
    
    $options  = '';

    if( $db->num_rows( $r ) > 0 )
    {
        $dt = array();

        while( $d = $db->fetch_array( $r ) )
        {
            // $
            $dt[] = array( 
                'id'         => $d['lcmenu_id'], 
                'name'       => $d['lcmenu_name'],
                'article_id' => $d['larticle_id'],
                'parent_id'  => $d['lcmenu_parent'] 
            );
        }

        $data = build_tree( $dt );

        foreach( $data as $d )
        {
            $options .= '<option value="' . $d['id'] . '">' . $d['name'] . '</option>';

            if( isset( $d['child'] ) )
            {
                foreach( $d['child'] as $c )
                {
                    $options .= '<option class="with-tabs" value="' . $c['id'] . '">' . $c['name'] . '</option>';
                }
            }
        }
    }
    
    return $options;
}

function delete_pdf_menu_file( $post_id = '', $app_name = '' )
{
    if( empty( $post_id ) )
    {
        return true;
    }
    
    $pdf = get_additional_field( $post_id, 'menu_pdf', $app_name );
    
    if( empty( $pdf ) )
    {
        return true;
    }


    if( file_exists( PLUGINS_PATH . '/menus/pdf/' . $pdf ) )
    {
        unlink( PLUGINS_PATH . '/menus/pdf/' . $pdf );
    }
    
    
    if( edit_additional_field( $post_id, 'menu_pdf', '', $app_name ) )
    {
        return true;
    }
    else
    {
        return false;
    }
}

function get_category_menu( $post_id, $lang_code )
{
    global $db;

    $s = 'SELECT * FROM lumonata_menu_category WHERE larticle_id = %d ORDER BY lorder ASC';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $dt = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data_add = "";
            if(!empty($lang_code))
            {
                $code_lang = explode(",", $lang_code);
                $data_add[] = "";
                for($i=0;$i<count($code_lang);$i++)
                {
                    $cl = $code_lang[$i];
                    $lkey = "lcmenu_name_".$cl;
                    $data_add["category_name_".$cl] = array(
                        'value' => get_additional_menu_field($d['lcmenu_id'], $lkey, 'menu_category')
                    );
                }
            }

            $images = get_additional_menu_field($d['lcmenu_id'], 'category_menu_image', 'menu_category');
            if(!empty($images))
            {
                $images = json_decode($images, true);
                $images = HTTP.SITE_URL.$images['thumb']; 
            }


            // PDF MENU DEFAULT
            $pdf      = get_additional_menu_field($d['lcmenu_id'], 'category_menu_pdf', 'menu_category');
            $pdf_file = "";
            $pdf_url  = "";
            if(!empty($pdf) && file_exists( ROOT_PATH .$pdf ))
            {
                $pdf_file = str_replace("/lumonata-plugins/menus/category_menu_pdf/", "", $pdf);
                $pdf_url = $pdf;
            }

            // PDF MENU JP
            $pdf_jp      = get_additional_menu_field($d['lcmenu_id'], 'category_menu_pdf_jp', 'menu_category');
            $pdf_file_jp = "";
            $pdf_url_jp  = "";
            if(!empty($pdf_jp) && file_exists( ROOT_PATH .$pdf_jp ))
            {
                $pdf_file_jp = str_replace("/lumonata-plugins/menus/category_menu_pdf/jp/", "", $pdf_jp);
                $pdf_url_jp = $pdf_jp;
            }

            // PDF MENU CN
            $pdf_cn      = get_additional_menu_field($d['lcmenu_id'], 'category_menu_pdf_cn', 'menu_category');
            $pdf_file_cn = "";
            $pdf_url_cn  = "";
            if(!empty($pdf_cn) && file_exists( ROOT_PATH .$pdf_cn ))
            {
                $pdf_file_cn = str_replace("/lumonata-plugins/menus/category_menu_pdf/cn/", "", $pdf_cn);
                $pdf_url_cn = $pdf_cn;
            }

            $dt[] = array( 
                'id'          => $d['lcmenu_id'], 
                'name'        => $d['lcmenu_name'],
                'article_id'  => $d['larticle_id'],
                'parent_id'   => $d['lcmenu_parent'],
                'lang_cat'    => $data_add,
                'images'      => $images,
                'pdf_file'    => $pdf_file,
                'pdf_url'     => $pdf_url,
                'pdf_file_jp' => $pdf_file_jp,
                'pdf_url_jp'  => $pdf_url_jp,
                'pdf_file_cn' => $pdf_file_cn,
                'pdf_url_cn'  => $pdf_url_cn
            );
        }

        $data = build_tree( $dt );

        return $data;
    }
}

function save_category_menu( $post_id, $cat_name, $code_lang='', $value_lang, $category_id )
{
    global $db;

    $s = 'INSERT INTO lumonata_menu_category( larticle_id, lcmenu_parent, lcmenu_name ) VALUES( %d, %d, %s )';
    $q = $db->prepare_query( $s, $post_id, 0, $cat_name );

    if( reset_order_id( 'lumonata_menu_category' ) )
    {
        if( $db->do_query( $q ) )
        {
            $mysql_insert_id = mysql_insert_id();
            // add to additional data for menu category language
            if(!empty($code_lang))
            {
                $code_lang = explode(",",$code_lang);
                for($i=0;$i<count($code_lang);$i++)
                {
                    $cat_lang = (empty($value_lang[$code_lang[$i]]) ? $cat_name : $value_lang[$code_lang[$i]]);

                    $insert_cat = insert_additional_menu_fields($mysql_insert_id, "lcmenu_name_".$code_lang[$i], $cat_lang, 'menu_category');

                    additional_field_menu_sync($category_id, $mysql_insert_id);
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_category_menu( $cat_id, $cat_name, $code_lang='', $value_lang )
{
    global $db;

    $s = 'UPDATE lumonata_menu_category SET lcmenu_name = %s WHERE lcmenu_id = %d';
    $q = $db->prepare_query( $s, $cat_name, $cat_id );
    
    if( $db->do_query( $q ) )
    {
        if(!empty($code_lang))
        {
            $code_lang = explode(",",$code_lang);
            for($i=0;$i<count($code_lang);$i++)
            {
                $cat_lang = (empty($value_lang[$code_lang[$i]]) ? $cat_name : $value_lang[$code_lang[$i]]);

                // DELETE DATA
                $delete_cat = delete_additional_menu_fields($cat_id, "lcmenu_name_".$code_lang[$i], 'menu_category');

                // INSERT DATA
                $insert_cat = insert_additional_menu_fields($cat_id, "lcmenu_name_".$code_lang[$i], $cat_lang, 'menu_category');
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_category_menu( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_menu_category SET lorder = %d, lcmenu_parent = %d WHERE lcmenu_id = %d';
            $q = $db->prepare_query( $s, $i, 0, $obj['id'] );
            $r = $db->do_query( $q );

            if( isset( $obj['children'] ) )
            {
                foreach( $obj['children'] as $idx => $child )
                {
                    $s = 'UPDATE lumonata_menu_category SET lorder = %d, lcmenu_parent = %d WHERE lcmenu_id = %d';
                    $q = $db->prepare_query( $s, $idx, $obj['id'], $child['id'] );
                    $r = $db->do_query( $q );
                }
            }
        }

        return true;
    }
}

function delete_category_menu( $cat_id )
{
    global $db;

    $s = 'SELECT * FROM lumonata_menu_category WHERE lcmenu_id = %d OR lcmenu_parent = %d';
    $q = $db->prepare_query( $s, $cat_id, $cat_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $id = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $id[] = $d['lcmenu_id'];
        }

        $q2 = 'SELECT * FROM lumonata_menu WHERE lcmenu_id IN( ' . implode( ',', $id ) . ' )';
        $r2 = $db->do_query( $q2 );
        $n2 = $db->num_rows( $r2 );

        if( $n2 > 0 )
        {
            return false;
        }
        else
        {
            $s3 = 'DELETE FROM lumonata_menu_category WHERE lcmenu_id = %d';
            $q3 = $db->prepare_query( $s3, $cat_id );
            
            if( $db->do_query( $q3 ) )
            {
                // UNLINK PDF FILE Default
                $pdf_cat = get_additional_menu_field( $cat_id, 'category_menu_pdf', 'menu_category' );
                if( file_exists( ROOT_PATH . $pdf_cat ) )
                {
                    unlink( ROOT_PATH . $pdf_cat );
                }

                // UNLINK PDF FILE JP
                $pdf_cat_jp = get_additional_menu_field( $cat_id, 'category_menu_pdf_jp', 'menu_category' );
                if(!empty($pdf_cat_jp))
                {
                    if( file_exists( ROOT_PATH . $pdf_cat_jp ) )
                    {
                        unlink( ROOT_PATH . $pdf_cat_jp );
                    }
                }

                // UNLINK PDF FILE CN
                $pdf_cat_cn = get_additional_menu_field( $cat_id, 'category_menu_pdf_cn', 'menu_category' );
                if(!empty($pdf_cat_cn))
                {
                    if( file_exists( ROOT_PATH . $pdf_cat_cn ) )
                    {
                        unlink( ROOT_PATH . $pdf_cat_cn );
                    }
                }

                // UNLINK IMAGE FILE
                $image_cat = get_additional_menu_field( $cat_id, 'category_menu_image', 'menu_category' );
                if(!empty($image_cat))
                {
                    $img = json_decode( $image_cat, true );

                    if( !empty( $img ) )
                    {
                        if( file_exists( ROOT_PATH . $img['original'] ) )
                        {
                            unlink( ROOT_PATH . $img['original'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['large'] ) )
                        {
                            unlink( ROOT_PATH . $img['large'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['medium'] ) )
                        {
                            unlink( ROOT_PATH . $img['medium'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['small'] ) )
                        {
                            unlink( ROOT_PATH . $img['small'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['thumb'] ) )
                        {
                            unlink( ROOT_PATH . $img['thumb'] );
                        }
                    }
                }

                $delete_cat = delete_additional_menu_fields($cat_id, '', 'menu_category');

                return true;
            }
            else
            {
                return false;
            }
        }
    }
    else
    {
        $s2 = 'DELETE FROM lumonata_menu_category WHERE lcmenu_id = %d';
        $q2 = $db->prepare_query( $s2, $cat_id );
        
        if( $db->do_query( $q2 ) )
        {
            // UNLINK PDF FILE
            $pdf_cat = get_additional_menu_field( $cat_id, 'category_menu_pdf', 'menu_category' );
            if( file_exists( ROOT_PATH . $pdf_cat ) )
            {
                unlink( ROOT_PATH . $pdf_cat );
            }

            // UNLINK PDF FILE JP
            $pdf_cat_jp = get_additional_menu_field( $cat_id, 'category_menu_pdf_jp', 'menu_category' );
            if(!empty($pdf_cat_jp))
            {
                if( file_exists( ROOT_PATH . $pdf_cat_jp ) )
                {
                    unlink( ROOT_PATH . $pdf_cat_jp );
                }
            }

            // UNLINK PDF FILE CN
            $pdf_cat_cn = get_additional_menu_field( $cat_id, 'category_menu_pdf_cn', 'menu_category' );
            if(!empty($pdf_cat_cn))
            {
                if( file_exists( ROOT_PATH . $pdf_cat_cn ) )
                {
                    unlink( ROOT_PATH . $pdf_cat_cn );
                }
            }


            // UNLINK IMAGE FILE
            $image_cat = get_additional_menu_field( $cat_id, 'category_menu_image', 'menu_category' );
            if(!empty($image_cat))
            {
                $img = json_decode( $image_cat, true );

                if( !empty( $img ) )
                {
                    if( file_exists( ROOT_PATH . $img['original'] ) )
                    {
                        unlink( ROOT_PATH . $img['original'] );
                    }
                    
                    if( file_exists( ROOT_PATH . $img['large'] ) )
                    {
                        unlink( ROOT_PATH . $img['large'] );
                    }
                    
                    if( file_exists( ROOT_PATH . $img['medium'] ) )
                    {
                        unlink( ROOT_PATH . $img['medium'] );
                    }
                    
                    if( file_exists( ROOT_PATH . $img['small'] ) )
                    {
                        unlink( ROOT_PATH . $img['small'] );
                    }
                    
                    if( file_exists( ROOT_PATH . $img['thumb'] ) )
                    {
                        unlink( ROOT_PATH . $img['thumb'] );
                    }
                }
            }
            $delete_cat = delete_additional_menu_fields($cat_id, '', 'menu_category');

            return true;
        }
        else
        {
            return false;
        } 
    }
}

function get_main_menu( $post_id, $lang_code )
{
    global $db;

    $s = 'SELECT *, ( 
                SELECT lcmenu_name 
                FROM lumonata_menu_category AS a2 
                WHERE a2.lcmenu_id = b.lcmenu_parent 
            ) AS lcmenu_parent_name
          FROM lumonata_menu AS a
          LEFT JOIN lumonata_menu_category AS b ON a.lcmenu_id = b.lcmenu_id
          WHERE b.larticle_id = %d ORDER BY a.lorder';
    $q = $db->prepare_query( $s, $post_id );
    $r = $db->do_query( $q );

    if( $db->num_rows( $r ) > 0 )
    {
        $data = array();

        while( $d = $db->fetch_array( $r ) )
        {
            $data_name = "";
            $data_desc = "";
            if(!empty($lang_code))
            {
                $code_lang = explode(",", $lang_code);
                $data_name[] = "";
                $data_desc[] = "";
                for($i=0;$i<count($code_lang);$i++)
                {
                    $cl = $code_lang[$i];
                    $lkey_name = "lmenu_name_".$cl;
                    $lkey_desc = "lmenu_desc_".$cl;

                    $data_name["menu_name_".$cl] = array(
                        'value' => get_additional_menu_field($d['lmenu_id'], $lkey_name, 'menu_list')
                    );

                    $data_desc["menu_desc_".$cl] = array(
                        'value' => get_additional_menu_field($d['lmenu_id'], $lkey_desc, 'menu_list')
                    );
                }
            }

            $data[] = array( 
                'lmenu_id'    => $d['lmenu_id'], 
                'lcmenu_id'   => $d['lcmenu_id'],
                'lmenu_name'  => $d['lmenu_name'],
                'lmenu_desc'  => $d['lmenu_desc'],
                'lmenu_price' => $d['lmenu_price'],
                'lmenu_type'  => empty( $d['lmenu_type'] ) ? '-' : json_decode( $d['lmenu_type'] ),
                'lcmenu_name' => ( $d['lcmenu_parent_name'] != '' ? $d['lcmenu_parent_name'] . ' | ' . $d['lcmenu_name'] : $d['lcmenu_name'] ),
                'lang_name'   => $data_name,
                'lang_desc'   => $data_desc
            );
        }

        return $data;
    }
}

function save_main_menu( $category, $name, $description = '', $type = array(), $price = 0, $code_lang='', $menu_name_lang, $menu_des_lang )
{
    global $db;

    if( empty( $type ) )
    {
        $s = 'INSERT INTO lumonata_menu( lcmenu_id, lmenu_name, lmenu_desc, lmenu_price ) VALUES( %d, %s, %s, %s )';
        $q = $db->prepare_query( $s, $category, $name, $description, $price );
    }
    else
    {
        $s = 'INSERT INTO lumonata_menu( lcmenu_id, lmenu_name, lmenu_desc, lmenu_type, lmenu_price ) VALUES( %d, %s, %s, %s, %s )';
        $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price );
    }
    
    if( reset_order_id( 'lumonata_menu' ) )
    {
        if( $db->do_query( $q ) )
        {
            $mysql_insert_id = mysql_insert_id();

            // add to additional data for menu language
            if(!empty($code_lang))
            {
                $code_lang = explode(",",$code_lang);
                for($i=0;$i<count($code_lang);$i++)
                {
                    $name_lang = (empty($menu_name_lang[$code_lang[$i]]) ? $name : $menu_name_lang[$code_lang[$i]]);

                    $des_lang = (empty($menu_des_lang[$code_lang[$i]]) ? $description : $menu_des_lang[$code_lang[$i]]);

                    $insert_name = insert_additional_menu_fields($mysql_insert_id, "lmenu_name_".$code_lang[$i], $name_lang, "menu_list");

                    $insert_desc = insert_additional_menu_fields($mysql_insert_id, "lmenu_desc_".$code_lang[$i], $des_lang, "menu_list");
                }
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        return false;
    }
}

function update_main_menu( $menu_id, $category, $name, $description = '', $type = array(), $price = 0, $code_lang='', $menu_name_lang, $menu_des_lang )
{
    global $db;

    if( empty( $type ) )
    {
        $s = 'UPDATE lumonata_menu SET 
                lcmenu_id = %s, 
                lmenu_name = %s,
                lmenu_desc = %s,
                lmenu_price = %s
              WHERE lmenu_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, $price, $menu_id );
    }
    else
    {
        $s = 'UPDATE lumonata_menu SET 
                lcmenu_id = %s, 
                lmenu_name = %s,
                lmenu_desc = %s,
                lmenu_type = %s,
                lmenu_price = %s
              WHERE lmenu_id = %d';
        $q = $db->prepare_query( $s, $category, $name, $description, json_encode( $type ), $price, $menu_id );
    }
    
    if( $db->do_query( $q ) )
    {
        if(!empty($code_lang))
        {
            $code_lang = explode(",",$code_lang);
            for($i=0;$i<count($code_lang);$i++)
            {
                $name_lang = (empty($menu_name_lang[$code_lang[$i]]) ? $name : $menu_name_lang[$code_lang[$i]]);

                $des_lang = (empty($menu_des_lang[$code_lang[$i]]) ? $description : $menu_des_lang[$code_lang[$i]]);

                // DELETE DATA
                $delete_name = delete_additional_menu_fields($menu_id, "lmenu_name_".$code_lang[$i], "menu_list");
                $delete_desc = delete_additional_menu_fields($menu_id, "lmenu_desc_".$code_lang[$i], "menu_list");

                // INSERT DATA
                $insert_name = insert_additional_menu_fields($menu_id, "lmenu_name_".$code_lang[$i], $name_lang, "menu_list");
                $insert_desc = insert_additional_menu_fields($menu_id, "lmenu_desc_".$code_lang[$i], $des_lang, "menu_list");
            }
        }
        return true;
    }
    else
    {
        return false;
    }
}

function reorder_main_menu( $list_arr = array() )
{
    global $db;

    if( empty( $list_arr ) )
    {
        return false;
    }
    else
    {
        foreach( $list_arr as $i => $obj )
        {
            $s = 'UPDATE lumonata_menu SET lorder = %d WHERE lmenu_id = %d';
            $q = $db->prepare_query( $s, $i, $obj['id'] );
            $r = $db->do_query( $q );
        }

        return true;
    }
}

function delete_main_menu( $menu_id )
{
    global $db;

    $s = 'DELETE FROM lumonata_menu WHERE lmenu_id = %d';
    $q = $db->prepare_query( $s, $menu_id );
    
    if( $db->do_query( $q ) )
    {
        $delete_cat = delete_additional_menu_fields($menu_id, '', 'menu_list');
        return true;
    }
    else
    {
        return false;
    }
}


function execute_menus_ajax_request()
{    
    if( is_ajax_request() )
    {
        if( $_POST[ 'ajax_key' ] == 'get-category-menu' )
        {
            $data = get_category_menu( $_POST['post_id'], $_POST['lang_code'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-category-menu' )
        {
            if( delete_category_menu( $_POST['cat_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-category-menu' )
        {
            if( save_category_menu( $_POST['post_id'], $_POST['cat_name'], $_POST['lang_code'], $_POST['category_lang'], $_POST['category_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-category-menu' )
        {
            if( update_category_menu( $_POST['cat_id'], $_POST['cat_name'], $_POST['lang_code'], $_POST['category_lang'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-category-menu' )
        {
            if( reorder_category_menu( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'get-category-menu-option' )
        {
            $options = get_menu_category_option( $_POST['post_id'] );

            if( empty( $options ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $options ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'get-main-menu' )
        {
            $data = get_main_menu( $_POST['post_id'], $_POST['lang_code'] );

            if( empty( $data ) )
            {
                echo json_encode( array( 'result' => 'failed' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'success', 'data' => $data ) );
            }
        }

        if( $_POST[ 'ajax_key' ] == 'save-main-menu' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( save_main_menu( $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['type'], $_POST['price'], $_POST['lang_code'], $_POST['menu_name_lang'], $_POST['menu_des_lang'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'update-main-menu' )
        {
            $_POST['type'] = isset( $_POST['type'] ) ? $_POST['type'] : array();

            if( update_main_menu( $_POST['menu_id'], $_POST['category'], $_POST['name'], $_POST['desc'], $_POST['type'], $_POST['price'], $_POST['lang_code'], $_POST['menu_name_lang'], $_POST['menu_des_lang'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'reorder-main-menu' )
        {
            if( reorder_main_menu( $_POST['list_arr'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-main-menu' )
        {
            if( delete_main_menu( $_POST['menu_id'] ) )
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-pdf-menu-file' )
        {
            if( delete_pdf_menu_file( $_POST['post_id'], 'dining' ))
            {
                echo json_encode( array( 'result' => 'success' ) );
            }
            else
            {
                echo json_encode( array( 'result' => 'failed' ) ); 
            }
        }

        if( $_POST[ 'ajax_key' ] == 'upload-category-menu-images' )
        {
            if( isset( $_FILES['category_menu_image'] ) && $_FILES['category_menu_image']['error']==0 )
            {
                $post_id     = $_POST['post_id'];
                $file_name   = $_FILES['category_menu_image']['name'];
                $file_size   = $_FILES['category_menu_image']['size'];
                $file_type   = $_FILES['category_menu_image']['type'];
                $file_source = $_FILES['category_menu_image']['tmp_name'];

                if( is_allow_file_size( $file_size ) )
                {
                    if( is_allow_file_type( $file_type, 'image' ) )
                    {
                        $file_ext  = file_name_filter( $file_name, true );
                        $file_name = file_name_filter( $file_name );
                        
                        $file_name_t = $file_name . '-' . time() . '-thumbnail' . $file_ext;
                        $file_name_s = $file_name . '-' . time() . '-small' . $file_ext;
                        $file_name_m = $file_name . '-' . time() . '-medium' . $file_ext;
                        $file_name_l = $file_name . '-' . time() . '-large' . $file_ext;
                        $file_name   = $file_name . '-' . time() . $file_ext;
                        
                        if( !file_exists( PLUGINS_PATH . '/menus/category_menu_image/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/menus/category_menu_image/' );
                        }
                        
                        $destination_t = PLUGINS_PATH . '/menus/category_menu_image/' . $file_name_t;
                        $destination_s = PLUGINS_PATH . '/menus/category_menu_image/' . $file_name_s;
                        $destination_m = PLUGINS_PATH . '/menus/category_menu_image/' . $file_name_m;
                        $destination_l = PLUGINS_PATH . '/menus/category_menu_image/' . $file_name_l;
                        $destination   = PLUGINS_PATH . '/menus/category_menu_image/' . $file_name;
                        
                        $destination_save_t = '/lumonata-plugins/menus/category_menu_image/' . $file_name_t;
                        $destination_save_s = '/lumonata-plugins/menus/category_menu_image/' . $file_name_s;
                        $destination_save_m = '/lumonata-plugins/menus/category_menu_image/' . $file_name_m;
                        $destination_save_l = '/lumonata-plugins/menus/category_menu_image/' . $file_name_l;
                        $destination_save   = '/lumonata-plugins/menus/category_menu_image/' . $file_name;
                        
                        upload_crop( $file_source, $destination_t, $file_type, thumbnail_image_width(), thumbnail_image_height(), true );
                        upload_resize( $file_source, $destination_s, $file_type, thumbnail_image_width(), thumbnail_image_height() );
                        upload_resize( $file_source, $destination_m, $file_type, medium_image_width(), medium_image_height() );
                        upload_resize( $file_source, $destination_l, $file_type, large_image_width(), large_image_height() );
                        upload( $file_source, $destination );

                        $cat = get_additional_menu_field( $post_id, 'category_menu_image', 'menu_category' );
                        $arr = array(
                            'thumb' => $destination_save_t,
                            'small' => $destination_save_s,
                            'medium' => $destination_save_m,
                            'large' => $destination_save_l,
                            'original' => $destination_save 
                        );

                        if( empty( $cat ) )
                        {
                            if( insert_additional_menu_fields( $post_id, 'category_menu_image', json_encode( $arr ), 'menu_category' ) )
                            {
                                echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name_m . '"}';
                            }
                        }
                        else
                        {
                            $img = json_decode( $cat, true );
                        
                            if( !empty( $img ) )
                            {
                                if( file_exists( ROOT_PATH . $img['original'] ) )
                                {
                                    unlink( ROOT_PATH . $img['original'] );
                                }
                                
                                if( file_exists( ROOT_PATH . $img['large'] ) )
                                {
                                    unlink( ROOT_PATH . $img['large'] );
                                }
                                
                                if( file_exists( ROOT_PATH . $img['medium'] ) )
                                {
                                    unlink( ROOT_PATH . $img['medium'] );
                                }
                                
                                if( file_exists( ROOT_PATH . $img['small'] ) )
                                {
                                    unlink( ROOT_PATH . $img['small'] );
                                }
                                
                                if( file_exists( ROOT_PATH . $img['thumb'] ) )
                                {
                                    unlink( ROOT_PATH . $img['thumb'] );
                                }
                            }

                            // DELETE DATA
                            $delete_cat = delete_additional_menu_fields($post_id, 'category_menu_image', 'menu_category');
                            
                            if( insert_additional_menu_fields( $post_id, 'category_menu_image', json_encode( $arr ), 'menu_category' ) )
                            {
                                echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name_m . '"}';
                            }
                        }
                    }
                    else
                    {
                        echo '{"result":"failed","error":"File is not an image"}';
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"Maximum file size is 2MB"}';
                }
            }
        }


        if( $_POST[ 'ajax_key' ] == 'upload-category-menu-pdf' )
        {
            if( isset( $_FILES['pdf_file_treatment'] ) && $_FILES['pdf_file_treatment']['error']==0 )
            {
                $post_id     = $_POST['post_id'];
                $file_name   = $_FILES['pdf_file_treatment']['name'];
                $file_size   = $_FILES['pdf_file_treatment']['size'];
                $file_type   = $_FILES['pdf_file_treatment']['type'];
                $file_source = $_FILES['pdf_file_treatment']['tmp_name'];

                if( is_allow_file_size( $file_size ) )
                {
                    if( is_allow_file_type( $file_type, 'pdf' ) )
                    {
                        $file_ext  = file_name_filter( $file_name, true );
                        $file_name = file_name_filter( $file_name );
                        
                        $file_name   = $file_name . '-' . time() . $file_ext;
                        
                        if( !file_exists( PLUGINS_PATH . '/menus/category_menu_pdf/' ) )
                        {
                            mkdir( PLUGINS_PATH . '/menus/category_menu_pdf/' );
                        }

                        $code_lang = $_POST['code_lang'];
                        if(!empty($code_lang))
                        {
                            if( !file_exists( PLUGINS_PATH . '/menus/category_menu_pdf/'.$code_lang.'/' ) )
                            {
                                mkdir( PLUGINS_PATH . '/menus/category_menu_pdf/'.$code_lang.'/' );
                            }
                        }

                        $destination   = PLUGINS_PATH . '/menus/category_menu_pdf/' . $file_name;
                        $destination_save   = '/lumonata-plugins/menus/category_menu_pdf/' . $file_name;
                        $key_add = "category_menu_pdf";

                        if(!empty($code_lang))
                        {
                            $destination   = PLUGINS_PATH . '/menus/category_menu_pdf/'.$code_lang.'/'.$file_name;
                            $destination_save   = '/lumonata-plugins/menus/category_menu_pdf/'.$code_lang.'/'.$file_name;
                            $key_add = "category_menu_pdf_".$code_lang;
                        }

                        
                        
                        upload( $file_source, $destination );

                        $cat = get_additional_menu_field( $post_id, $key_add, 'menu_category' );

                        // if( empty( $cat ) && !file_exists( ROOT_PATH . $cat ) )
                        // {
                        //     if( insert_additional_menu_fields( $post_id, 'category_menu_pdf', $destination_save, 'menu_category' ) )
                        //     {
                        //         echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name . '"}';
                        //     }
                        // }
                        // else
                        // {

                            if( file_exists( ROOT_PATH . $cat ) )
                            {
                                unlink( ROOT_PATH . $cat );
                            }

                            // DELETE DATA
                            $delete_cat = delete_additional_menu_fields($post_id, $key_add, 'menu_category');
                            
                            if( insert_additional_menu_fields( $post_id, $key_add, $destination_save, 'menu_category' ) )
                            {
                                echo '{"result":"success","post_id":"' . $post_id . '","filename":"' . $file_name . '"}';
                            }
                        // }
                    }
                    else
                    {
                        echo '{"result":"failed","error":"File is not an PDF"}';
                    }
                }
                else
                {
                    echo '{"result":"failed","error":"Maximum file size is 2MB"}';
                }
            }
        }


        if( $_POST[ 'ajax_key' ] == 'delete-category-menu-images' )
        {
            if( empty( $_POST['post_id'] ) )
            {
                echo '{"result":"failed"}';
            }
            else
            {
                $featured = get_additional_menu_field( $_POST['post_id'], 'category_menu_image', 'menu_category');

                if( !empty( $featured ) )
                {
                    $img = json_decode( $featured, true );
                
                    if( !empty( $img ) )
                    {
                        if( file_exists( ROOT_PATH . $img['original'] ) )
                        {
                            unlink( ROOT_PATH . $img['original'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['large'] ) )
                        {
                            unlink( ROOT_PATH . $img['large'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['medium'] ) )
                        {
                            unlink( ROOT_PATH . $img['medium'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['small'] ) )
                        {
                            unlink( ROOT_PATH . $img['small'] );
                        }
                        
                        if( file_exists( ROOT_PATH . $img['thumb'] ) )
                        {
                            unlink( ROOT_PATH . $img['thumb'] );
                        }
                        
                        if( delete_additional_menu_fields($_POST['post_id'], 'category_menu_image', 'menu_category') )
                        {
                            echo '{"result":"success"}';
                        }
                        else
                        {
                            echo '{"result":"failed"}';
                        }
                    }
                    else
                    {
                        echo '{"result":"failed"}';
                    }
                }
                else
                {
                    echo '{"result":"failed"}';
            }
            }
        }

        if( $_POST[ 'ajax_key' ] == 'delete-category-menu-pdf' )
        {
            if( empty( $_POST['post_id'] ) )
            {
                echo '{"result":"failed"}';
            }
            else
            {
                $lang = $_POST['lang'];
                $pdf_file = "";

                if($lang=="en")
                {
                    $pdf_file = get_additional_menu_field( $_POST['post_id'], 'category_menu_pdf', 'menu_category');
                    $key = 'category_menu_pdf';
                }
                elseif($lang=="jp")
                {
                    $pdf_file = get_additional_menu_field( $_POST['post_id'], 'category_menu_pdf_jp', 'menu_category');
                    $key = 'category_menu_pdf_jp';
                }
                elseif($lang=="cn")
                {
                    $pdf_file = get_additional_menu_field( $_POST['post_id'], 'category_menu_pdf_cn', 'menu_category');
                    $key = 'category_menu_pdf_cn';
                }

                if( !empty( $pdf_file ) )
                {
                    if( file_exists( ROOT_PATH . $pdf_file ) )
                    {
                        unlink( ROOT_PATH . $pdf_file );
                    }
                    
                    if( delete_additional_menu_fields($_POST['post_id'], $key, 'menu_category') )
                    {
                        echo '{"result":"success"}';
                    }
                    else
                    {
                        echo '{"result":"failed t"}';
                    }
                }
            }
        }
    }
    
    exit;
}

function insert_additional_menu_fields($app_id, $key, $value, $app_name)
{
    global $db;

    $q = $db->prepare_query("INSERT INTO lumonata_additional_menu_fields VALUES(%d, %s, %s, %s)", $app_id, $key, $value, $app_name);

    return $db->do_query($q);
}

function delete_additional_menu_fields($app_id, $key='', $app_name)
{
    global $db;

    if(!empty($key))
    {
        $q = $db->prepare_query("DELETE FROM lumonata_additional_menu_fields WHERE lapp_id=%d AND lkey=%s AND lapp_name=%s", $app_id, $key, $app_name);
    }
    else
    {
        $q = $db->prepare_query("DELETE FROM lumonata_additional_menu_fields WHERE lapp_id=%d AND lapp_name=%s", $app_id, $app_name);
    }

    return $db->do_query($q);
}


/*
| -------------------------------------------------------------------------------------
| Syncronize Additional Data Menu
| -------------------------------------------------------------------------------------
*/
function additional_field_menu_sync( $old_id, $new_id )
{
    global $db;

    $s = 'UPDATE lumonata_additional_menu_fields SET lapp_id = %d WHERE lapp_id = %d';
    $q = $db->prepare_query( $s, $new_id, $old_id );

    return $db->do_query( $q );
}

?>