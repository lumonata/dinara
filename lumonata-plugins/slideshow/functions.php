<?php

function set_admin_slideshow()
{
	add_actions( 'header_elements', 'get_custom_css', HTTP . site_url() . '/lumonata-plugins/slideshow/css/style.css' );
	
	if( is_add_new() )
	{
		return add_slideshow() ;
	}
	elseif( is_ajax() )
	{
		return get_slideshow_ajax() ;
	}
	elseif( is_edit() )
	{ 
		return edit_slideshow();
	}
	elseif( is_edit_all() && isset($_POST['select']) )
	{
		return edit_page();
	}
	elseif( is_delete_all() )
	{
		return batch_delete_slideshow();
	}
	elseif( is_confirm_delete() )
	{
		foreach( $_POST['id'] as $key=>$val )
		{
			delete_slideshow($val);
		}
	}
	
	if( is_num_slideshow() > 0 )
	{
		return get_slideshow_list();
	}
	else
	{
		header( 'location:'.get_state_url('slideshow&prc=add_new') );
	}
}

function is_num_slideshow()
{
	return is_num_articles('type=slideshow');
}

function get_slideshow_list()
{
	add_actions( 'header_elements', 'get_javascript', 'jquery_ui' );
    add_actions( 'header_elements', 'get_javascript', 'articles_list' );                    
    return get_article_list( 'slideshow', 'Slideshow', '' );
}

function add_slideshow()
{
	$button	= '
	<li>'.button( 'button=save_changes&label=Save' ).'</li>
	<li>'.button( 'button=cancel', get_state_url('slideshow') ).'</li>
	<li>'.button( 'button=add_new', get_state_url('slideshow&prc=add_new') ).'</li>';

	save_slideshow();
	
	set_template( PLUGINS_PATH.'/slideshow/template/form.html', 'slideshow_form_template' );

	add_block( 'form_block', 'fblock', 'slideshow_form_template' );

	add_variable( 'post_on_slide', set_slideshow_post_list_option(null, false) );
	add_variable( 'post_list', set_slideshow_post_list_option() );
	add_variable( 'post_id', time() );
	add_variable( 'post_title', '' );
	
	add_variable( 'ajax_url', get_state_url('slideshow&prc=ajax') );
	add_variable( 'title_form', 'Add New Slideshow' );
	add_variable( 'site_url', HTTP.SITE_URL );
	add_variable( 'button', $button );
	
	add_actions( 'section_title', 'Slideshow - Add New' );
	add_actions( 'header_elements', 'get_custom_css', HTTP.SITE_URL.'/lumonata-plugins/slideshow/css/dropzone.min.css' );
	add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/jquery-ui-1.8.2.custom.min.js' );
	add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/jquery.mjs.nestedSortable.js') ;
    add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/dropzone.min.js' );
    add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/scripts.js' );
	
	parse_template( 'form_block', 'fblock', false );
	return return_template( 'slideshow_form_template' );
}

function edit_slideshow()
{
	$button	= '
	<li>'.button( 'button=save_changes&label=Save' ).'</li>
	<li>'.button( 'button=cancel',get_state_url('slideshow') ).'</li>
	<li>'.button( 'button=add_new',get_state_url('slideshow&prc=add_new') ).'</li>';

	update_slideshow();

	$article = fetch_artciles('id='.$_GET['id'].'&type=slideshow');
	
	set_template( PLUGINS_PATH.'/slideshow/template/form.html', 'slideshow_form_template' );

	add_block( 'form_block', 'fblock', 'slideshow_form_template' );
	
	add_variable( 'post_on_slide', set_slideshow_post_list_option($article['larticle_id'], false) );
	add_variable( 'post_list', set_slideshow_post_list_option($article['larticle_id']) );
	add_variable( 'post_title', $article['larticle_title'] );
	add_variable( 'post_id', $article['larticle_id'] );
	
	add_variable( 'ajax_url', get_state_url('slideshow&prc=ajax') );
	add_variable( 'title_form', 'Edit Slideshow' );
	add_variable( 'site_url', HTTP.SITE_URL );
	add_variable( 'button', $button );

	add_actions( 'section_title','Slideshow - Edit' );
	add_actions( 'header_elements', 'get_custom_css', HTTP.SITE_URL.'/lumonata-plugins/slideshow/css/dropzone.min.css' );
	add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/jquery-ui-1.8.2.custom.min.js' );
	add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/jquery.mjs.nestedSortable.js') ;
    add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/dropzone.min.js' );
    add_actions( 'footer_elements', 'get_custom_javascript', HTTP.SITE_URL.'/lumonata-plugins/slideshow/js/scripts.js' );
	
	parse_template('form_block','fblock',false);
	return return_template('slideshow_form_template');
}

function batch_delete_slideshow()
{	
    set_template( PLUGINS_PATH . '/slideshow/template/batch-delete.html', 'slideshow_batch_delete_template' );
    add_block( 'loop-block', 'lclblock', 'slideshow_batch_delete_template' );
    add_block( 'delete-block', 'lcblock', 'slideshow_batch_delete_template' );

    foreach( $_POST['select'] as $key=>$val )
    {
        $d = fetch_artciles('id='.$val.'&type=slideshow');

        add_variable('slide_name',  $d['larticle_title'] );
        add_variable('slide_id', $d['larticle_id'] );

        parse_template('loop-block', 'lclblock', true);
    }

    add_variable('message', 'Are you sure want to delete ' . ( count( $_POST['select'] ) == 1 ? 'this' : 'these' ) . ' slideshow? :' );
    add_variable('action', get_state_url('slideshow'));

    add_actions( 'section_title', 'Delete Slideshow' );

    parse_template( 'delete-block', 'lcblock', false );

    return return_template( 'slideshow_batch_delete_template' );
}

function delete_slideshow( $id )
{
    if( delete_article( $id , 'slideshow' ) )
    {
		 return true;  	
    }

	return false;
}

function delete_slideshow_img( $attach_id )
{
	if( delete_attachment( $attach_id ) )
	{
		if( delete_additional_field( $attach_id, 'attachment' ) )
		{
			return true;
		}
	}

	return false;
}

function save_slideshow()
{
	if( isset($_POST['save_changes']) )
	{
		$title = ( empty($_POST['post_title']) ? 'Untitled' : $_POST['post_title'] );

		if( save_article( $title, '', 'publish' , 'slideshow', date( 'Y-m-d H:i:s' ), 'not-allowed' ) )
		{
			$article_id = mysql_insert_id();

			attachment_sync( $_POST['post_id'], $article_id );

            foreach( $_POST['additional_fields'] as $key=>$val )
            {
                edit_additional_field($article_id, $key, $val, 'slideshow');
            }

			$message = '<div class="alert_green">Add Slideshow has save succesfully.</div>';
		}
		else
		{
			$message = '<div class="alert_red">Something wrong, please try again.</div>';
		}
		
		add_variable('message', $message);
	}
}

function update_slideshow()
{
	if( isset($_POST['save_changes']) and isset($_GET['prc']) and $_GET['prc']=='edit' )
	{
		if( update_article( $_GET['id'], $_POST['post_title'], '', 'publish', 'slideshow', date( 'Y-m-d H:i:s' ), 'not-allowed' ) )
		{
            foreach( $_POST['additional_fields'] as $key=>$val )
            {
                edit_additional_field($_GET['id'], $key, $val, 'slideshow');
            }

			$message = '<div class="alert_green">Edit header has succesfully.</div>';
		}
		else
		{
			$message = '<div class="alert_red">Something wrong, please try againt.</div>';
		}
		
		add_variable('message', $message);
	}
}

function get_slideshow( $id )
{
	global $db;

	$s = 'SELECT * FROM lumonata_attachment WHERE larticle_id = %d AND lattach_app_name = %s ORDER BY lorder';
	$q = $db->prepare_query($s, $id, 'slideshow');
	$r = $db->do_query($q);
	$n = $db->num_rows($r);
	if($n > 0)
	{
		$data = array();

		while( $d=$db->fetch_array($r) )
		{
			$title        = get_additional_field( $d['lattach_id'], 'slideshow-title', 'attachment' );
			$subtitle     = get_additional_field( $d['lattach_id'], 'slideshow-subtitle', 'attachment' );
			$url          = get_additional_field( $d['lattach_id'], 'slideshow-url', 'attachment' );
			$description  = get_additional_field( $d['lattach_id'], 'slideshow-description', 'attachment' );
			$use_link     = get_additional_field( $d['lattach_id'], 'slideshow-use-link', 'attachment' );
			$post_link_id = get_additional_field( $d['lattach_id'], 'slideshow-post-link-id', 'attachment' );
			$button_text  = get_additional_field( $d['lattach_id'], 'slideshow-button-text', 'attachment' );

			$filename = explode('/', $d['lattach_loc_thumb']);
			$filename = array_reverse($filename);

			$data[] = array(
				'title'=>$title,
				'subtitle'=>$subtitle,
				'description'=>$description,
				'use_link'=>$use_link,
				'post_link_id'=>$post_link_id,
				'button_text'=>$button_text,
				'url'=>$url,
				'img_id'=>$d['lattach_id'],
				'img_title'=>$filename[0],
				'img_thumb'=>HTTP.SITE_URL.$d['lattach_loc_thumb'],
				'img_medium'=>HTTP.SITE_URL.$d['lattach_loc_medium'],
				'img_large'=>HTTP.SITE_URL.$d['lattach_loc_large'],
				'img_original'=>HTTP.SITE_URL.$d['lattach_loc']
			);
		}

		return $data;
	}
}

function set_slideshow_post_list_option( $id='', $show_on=true )
{
	global $db;

	$post_id = $show_on ? get_additional_field( $id, 'show_on', 'slideshow' ) : '';

	$s = 'SELECT DISTINCT larticle_type FROM lumonata_articles WHERE larticle_type NOT IN ("slideshow","banner")';
	$q = $db->prepare_query($s);
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

	$option = '
	<option value="00" '.($post_id=='00' ? 'selected' : '').' data-link="'.HTTP.SITE_URL.'">Homepage</option>
	<option value="01" '.($post_id=='01' ? 'selected' : '').' data-link="'.HTTP.SITE_URL.'/blogs">Blog Archive</option>
	<option value="02" '.($post_id=='02' ? 'selected' : '').' data-link="'.HTTP.SITE_URL.'/diving">Diving Archive</option>
	<option value="03" '.($post_id=='03' ? 'selected' : '').' data-link="'.HTTP.SITE_URL.'/diving">Diving Trip Archive</option>
	<option value="04" '.($post_id=='04' ? 'selected' : '').' data-link="'.HTTP.SITE_URL.'/accommodation">Accommodation Archive</option>
	<option value="05" '.($post_id=='05' ? 'selected' : '').' data-link="'.HTTP.SITE_URL.'/news">News Archive</option>';

	if($n > 0)
	{
		$s2 = 'SELECT lvalue FROM lumonata_additional_fields WHERE lkey=%s AND lapp_name=%s';
		$q2 = $db->prepare_query($s2, 'show_on', 'slideshow');
		$r2 = $db->do_query($q2);

		$sel_id = array();
		while( $d2=$db->fetch_array($r2) )
		{
			$sel_id[] = $d2['lvalue'];
		}

		while( $d=$db->fetch_array($r) )
		{
			$s3 = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder';
			$q3 = $db->prepare_query($s3, $d['larticle_type']);
			$r3 = $db->do_query($q3);

			$option .= '<optgroup label="'.ucfirst($d['larticle_type']).'">';

			while( $d3=$db->fetch_array($r3) )
			{
				if( $d3['larticle_id']!=$post_id && !empty($sel_id) && in_array($d3['larticle_id'], $sel_id) )
				{
					continue;
				}
				else
				{
					$permalink = $d3['larticle_type']=='products' ? product_permalink($d3['larticle_id']) : permalink($d3['larticle_id']);

					if( $d3['larticle_id']==$post_id )
					{
						$option .= '<option selected value="'.$d3['larticle_id'].'" data-link="'.$permalink.'">'.$d3['larticle_title'].'</option>';
					}
					else
					{
						$option .= '<option value="'.$d3['larticle_id'].'" data-link="'.$permalink.'">'.$d3['larticle_title'].'</option>';
					}
				}
			}

			$option .= '</optgroup>';
		}
	}

	return $option;
}

function upload_slideshow()
{
	if( isset($_FILES['file']['name']) && !empty($_FILES['file']['name']) )
	{
		$file_name   = $_FILES['file']['name'];
		$file_size   = $_FILES['file']['size'];
		$file_type   = $_FILES['file']['type'];
		$file_source = $_FILES['file']['tmp_name'];
		
		if( !empty($file_name) )
		{
			if( is_allow_file_size($file_size) )
			{
				if( is_allow_file_type($file_type,'image') )
				{
					$ext  = file_name_filter($file_name, true);
					$name = file_name_filter($file_name, false).'-'.time().$ext;
					
					//-- CHECK FOLDER EXIST
					if( !file_exists(PLUGINS_PATH.'/slideshow/files/large/') ) 
					{
						mkdir( PLUGINS_PATH.'/slideshow/files/large/' );
					}

					if( !file_exists(PLUGINS_PATH.'/slideshow/files/medium/') ) 
					{
						mkdir( PLUGINS_PATH.'/slideshow/files/medium/' );
					}

					if( !file_exists(PLUGINS_PATH.'/slideshow/files/thumb/') ) 
					{
						mkdir (PLUGINS_PATH.'/slideshow/files/thumb/' );
					}
						
					$ldestination = PLUGINS_PATH.'/slideshow/files/large/'.$name;
					$mdestination = PLUGINS_PATH.'/slideshow/files/medium/'.$name;
					$tdestination = PLUGINS_PATH.'/slideshow/files/thumb/'.$name;
					$fdestination = PLUGINS_PATH.'/slideshow/files/'.$name;

					if( upload_resize($file_source, $ldestination, $file_type, 1920, 1920) )
					{
						if( upload_resize($file_source, $mdestination, $file_type, 1024, 1024) )
						{
							if( upload_crop($file_source, $tdestination, $file_type, 550, 550) )
							{
								if( upload($file_source, $fdestination) )
								{
									$loc        = '/lumonata-plugins/slideshow/files/'.$name;
									$loc_thumb  = '/lumonata-plugins/slideshow/files/thumb/'.$name;
									$loc_medium = '/lumonata-plugins/slideshow/files/medium/'.$name;
									$loc_large  = '/lumonata-plugins/slideshow/files/large/'.$name;

									if( insert_attachment( $_POST['post_id'], $name, $file_type, $loc, $loc_thumb, $loc_medium, $loc_large, null, null, 'slideshow' ) )
									{
										$attach_id = mysql_insert_id();

										add_additional_field( $attach_id, 'slideshow-title', '' , 'attachment');
										add_additional_field( $attach_id, 'slideshow-subtitle', '' , 'attachment');
										add_additional_field( $attach_id, 'slideshow-url', '' , 'attachment');
										add_additional_field( $attach_id, 'slideshow-description', '' , 'attachment');
										add_additional_field( $attach_id, 'slideshow-use-link', '' , 'attachment');
										add_additional_field( $attach_id, 'slideshow-post-link-id', '' , 'attachment');
										add_additional_field( $attach_id, 'slideshow-button-text', '' , 'attachment');										

										return $attach_id;
									}
								}
								else
								{
									unlink($tdestination);
								}
							}
							else
							{
								unlink($mdestination);
							}
						}
						else
						{
							unlink($ldestination);
						}
					}
				}
			}
		}
	}
}

function edit_slideshow_info()
{
	edit_additional_field( $_POST['attach_id'], 'slideshow-title', $_POST['title'] , 'attachment');
	edit_additional_field( $_POST['attach_id'], 'slideshow-subtitle', $_POST['subtitle'] , 'attachment');
	edit_additional_field( $_POST['attach_id'], 'slideshow-url', $_POST['url'] , 'attachment');
	edit_additional_field( $_POST['attach_id'], 'slideshow-description', $_POST['description'] , 'attachment');
	edit_additional_field( $_POST['attach_id'], 'slideshow-use-link', $_POST['use_link'] , 'attachment');
	edit_additional_field( $_POST['attach_id'], 'slideshow-post-link-id', $_POST['post_link_id'] , 'attachment');
	edit_additional_field( $_POST['attach_id'], 'slideshow-button-text', $_POST['button_text'] , 'attachment');

	return true;
}

function reorder_slideshow()
{
	global $db;

	if( isset($_POST['value']) && !empty($_POST['value']) )
	{
		$value = json_decode($_POST['value'], true);

		foreach( $value as $i=>$val )
		{
			$s = 'UPDATE lumonata_attachment SET lorder=%d WHERE lattach_id=%d AND larticle_id=%d';
			$q = $db->prepare_query($s, $i, $val, $_POST['id']);
			$r = $db->do_query($q);
		}

		return true;
	}
	else
	{
		return false;
	}
}

function get_slideshow_ajax()
{
	global $db;

	if( isset($_POST['ajax_key']) && $_POST['ajax_key'] == 'post_combo_article' )
	{
		echo option_article('',$_POST['app_type']);
	}
	
	if( isset($_POST['ajax_key']) && $_POST['ajax_key'] == 'upload_slideshow' )
	{
		$attach_id = upload_slideshow();

		if( empty($attach_id) )
		{
			echo '{"result":"failed"}';
		}
		else
		{
			echo '{"result":"success","attach_id":"'.$attach_id.'"}';
		}
	}

	if( isset($_POST['ajax_key']) && $_POST['ajax_key'] =='delete_slideshow_img' )
	{
		if( delete_slideshow_img( $_POST['attach_id'] ) )
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if( isset($_POST['ajax_key']) && $_POST['ajax_key'] =='edit_slideshow_info' )
	{
		if( edit_slideshow_info() )
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if( isset($_POST['ajax_key']) && $_POST['ajax_key'] =='get_slideshow' )
	{
		$images = get_slideshow( $_POST['post_id'] );

		if( empty($images) )
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$prm['result'] = 'success';
			$prm['data']   = $images;

			echo json_encode($prm);
		}
	}

	if( isset($_POST['ajax_key']) && $_POST['ajax_key'] =='reorder_slideshow' )
	{
		if( reorder_slideshow() )
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	die();
}

?>