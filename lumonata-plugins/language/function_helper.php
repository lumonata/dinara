<?php
//-- Add Actions
add_actions( 'page_language_data', 'language_data_page_func' );
add_actions( 'post_language_data', 'language_data_post_func' );
add_actions( 'taxonomy_language_data', 'language_data_taxonomy_func' );
add_actions( 'page_post_language_button', 'language_button_func', 'default' );
add_actions( 'menus_language_button', 'language_button_func', 'menu' );
add_actions( 'post_type_setting_language_data', 'language_data_post_type_setting_func' );
add_actions( 'post_type_setting_location_language_data', 'language_data_post_type_setting_location_func' );
add_actions( 'menu_list_language_data', 'language_data_menu_list_func' );
add_actions( 'menu_category_language_data', 'language_data_menu_category_func' );
add_actions( 'treatment_category_language_data', 'language_data_treatment_category_func' );
add_actions( 'treatment_list_language_data', 'language_data_treatment_list_func' );
add_actions( 'destination_language_data', 'language_data_destination_func' );



/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada page
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_page_func()
{
	global $db;

	set_template(PLUGINS_PATH.'/language/template_form/page_form.html','language_page_form_template');
	add_block('language_page_form_loop','lang_page_form_loop','language_page_form_template');

	$q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

    if($n > 0)
    {
		$i_lang = 0;
        while($d = $db->fetch_array($r))
        {
			$id              = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id, 'status_default', 'language' );
			$class_active    = (($status_default == 1) ? ' class="active"' : '');

            if($status_default != 1)
            {
				add_variable('title_lang', ucwords($title));
				add_variable('code_lang', $code);
				add_variable('code_lang_lower', $code_lang_lower);

                if(is_edit())
                {
					$id = $_GET['id'];
					$article_title_lang = get_additional_field($id,'title_'.$code_lang_lower,'pages');
					$article_subtitle_lang = get_additional_field($id,'subtitle_'.$code_lang_lower,'pages');
					$article_content_lang = get_additional_field($id,'content_'.$code_lang_lower,'pages');
					$brief_lang = get_additional_field($id,'brief_'.$code_lang_lower,'pages');

					// META DATA
					$meta_title_lang = get_additional_field($id,'meta_title_'.$code_lang_lower,'pages');
					$meta_keywords_lang = get_additional_field($id,'meta_keywords_'.$code_lang_lower,'pages');
					$meta_description_lang = get_additional_field($id,'meta_description_'.$code_lang_lower,'pages');

					add_variable('article_title_lang', $article_title_lang);
					add_variable('article_subtitle_lang', $article_subtitle_lang);
					add_variable('article_content_lang', $article_content_lang);
					add_variable('brief_lang', $brief_lang);

					add_variable('meta_title_lang', $meta_title_lang);
					add_variable('meta_keywords_lang', $meta_keywords_lang);
					add_variable('meta_description_lang', $meta_description_lang);
					add_variable('i_lang', $i_lang);
					$i_lang++;
				}

				parse_template('language_page_form_loop','lang_page_form_loop',true);
			}
		}
	}

	return return_template('language_page_form_template');
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada taxonomy
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_taxonomy_func()
{
	global $db;

	set_template(PLUGINS_PATH.'/language/template_form/taxonomy_form.html','language_taxonomy_form_template');
	add_block('language_taxonomy_form_loop','lang_taxonomy_form_loop','language_taxonomy_form_template');

	$q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

    if($n > 0)
    {
		$i_lang = 0;
        while($d = $db->fetch_array($r))
        {
			$id              = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id, 'status_default', 'language' );
			$class_active    = (($status_default == 1) ? ' class="active"' : '');

            if($status_default != 1)
            {
				add_variable('title_lang', ucwords($title));
				add_variable('code_lang', $code);
				add_variable('code_lang_lower', $code_lang_lower);

                if(is_edit())
                {
					add_variable('title_lang', ucwords($title));
					$id = $_GET['id'];
					$app_name = $_GET['state'];

					$name_lang = get_rule_additional_field($id,'name_'.$code_lang_lower,$app_name);
					$description_lang = get_rule_additional_field($id,'description_'.$code_lang_lower,$app_name);

					add_variable('name_lang', $name_lang);
					add_variable('description_lang', $description_lang);
					add_variable('i_lang', $i_lang);
					$i_lang++;

                }
                else
                {
					add_variable('i_lang', 0);
				}

				parse_template('language_taxonomy_form_loop','lang_taxonomy_form_loop',true);
			}
		}
	}

	// parse_template('language_taxonomy_form','lang_taxonomy_form',false);
	return return_template('language_taxonomy_form_template');
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan button language sesuai dengan database
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_button_func($type='default')
{
	global $db;

	$q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

    if($n > 0)
    {
        $class = "button-language";
        if($type!="default")
        {
            $class = "button-language-".$type;
        }
		$html ='
		<div class="'.$class.'">
			<ul>';
            while($d = $db->fetch_array($r))
            {
				$id = $d['larticle_id'];
				$title = $d['larticle_title'];
				$code = strtoupper(get_additional_field( $id, 'language_code', 'language' ));
				$status_default = get_additional_field( $id, 'status_default', 'language' );
				$class_active = (($status_default == 1) ? ' class="active"' : '');
				$icon = get_additional_field($id,'icon_language','language');

				if(file_exists(PLUGINS_PATH.'/language/background/'.$icon))
				{
					$style_bg = 'style="background-image: url('.URL_PLUGINS.'/language/background/'.$icon.')"';
				}

				$html .= '<li'.$class_active.'><a href="#" data-filter="'.$code.'" class="btn" '.$style_bg.'>'.$title.'</a></li>';
			}
		$html .= '
				<div class="clear"></div>
			</ul>
		</div>
		';

		return $html;
	}

	return '';
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada post type setting
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_post_type_setting_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/post_type_setting_form.html','language_post_type_setting_form_template');
    add_block('language_post_type_setting_form_loop','lang_post_type_settingform_loop','language_post_type_setting_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        $i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');
            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang', ucwords($title));
				add_variable('code_lang', $code);
                add_variable('code_lang_lower', $code_lang_lower);

                if(check_active_plugin_destination())
                {
                    add_variable('destination_lang_post_setting_template', destination_lang_post_setting_template( $id_lang, $code_lang_lower, $state ));
                }

                parse_template('language_post_type_setting_form_loop','lang_post_type_settingform_loop',true);
            }

        }
    }

    return return_template('language_post_type_setting_form_template');
}


function destination_lang_post_setting_template( $id, $code_lang, $app_name="blog" )
{
    global $db;

    $qdes = $db->prepare_query("SELECT larticle_id, larticle_title, lsef FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title ASC", "destinations", "publish");
    $rdes = $db->do_query($qdes);
    $ndes = $db->num_rows($rdes);


    if($ndes > 0){

    // echo
        set_template(PLUGINS_PATH.'/language/template_form/list_destination_post_type_lang.html','destination_template_'.$id);
        add_block('loop_destination_language','loop_destination_lang_'.$id,'destination_template_'.$id);

        while($ddes = $db->fetch_array($rdes))
        {
            $dest_title = ucwords($ddes['larticle_title']);
            $dest_id    = $ddes['larticle_id'];
            $array_name = "[$dest_id]";

            add_variable( 'dest_title', $dest_title );
            add_variable( 'dest_id', $dest_id );
            add_variable( 'array_name', $array_name );
            add_variable( 'code_lang_des', $code_lang );

            $d = get_costom_post_setting_lang( $app_name, '', $dest_id, $code_lang );

            if($app_name == "spa")
            {
                $text_additional_book = "";
                if(isset($d['text_additional_book_'.$code_lang]))
                {
                    $text_additional_book = $d['text_additional_book_'.$code_lang];
                }

                $pdf_file_spa_menu = "";
                if(isset($d['pdf_file_spa_menu_'.$code_lang]))
                {
                    $pdf_file_spa_menu = $d['pdf_file_spa_menu_'.$code_lang];
                }

                $content_add = '
                    <fieldset>
                        <p>Text Additional Booking Popup</p>
                        <textarea class="textarea tinymce" name="text_additional_book_'.$code_lang.$array_name.'" autocomplete="off">'.$text_additional_book.'</textarea>
                    </fieldset>
                ';

                if(!empty($pdf_file_spa_menu) && file_exists(PLUGINS_PATH . '/custom-post/pdf/'.$code_lang.'/'.$pdf_file_spa_menu))
                {
                    $content_add .= '
                    <fieldset>
                        <p style="margin-bottom: 0px;">Current PDF File Spa Menu</p>
                        <a href="'.URL_PLUGINS . 'custom-post/pdf/'.$code_lang.'/'.$pdf_file_spa_menu.'" target="_blank">'.$pdf_file_spa_menu.'</a>
                    </fieldset>
                    ';
                }

                $content_add .= '
                    <fieldset>
                        <p>PDF File Spa Menu</p>
                        <input type="file" class="pdf-upload-'.$code_lang.'" name="pdf_file_spa_menu_'.$code_lang.$array_name.'" autocomplete="off" />
                    </fieldset>
                ';

                add_variable( 'content_add', $content_add );
            }

            // echo $d['bg_image_small'];
            add_variable( 'title_des_lang', $d['title_'.$code_lang] );
            add_variable( 'subtitle_des_lang', $d['subtitle_'.$code_lang] );
            add_variable( 'description_lang', $d['description_'.$code_lang] );
            add_variable( 'brief_lang', $d['brief_'.$code_lang] );
            add_variable( 'meta_title_lang', $d['meta_title_'.$code_lang] );
            add_variable( 'meta_keywords_lang', $d['meta_keywords_'.$code_lang] );
            add_variable( 'meta_description_lang', $d['meta_description_'.$code_lang] );

            parse_template( 'loop_destination_language', 'loop_destination_lang_'.$id, true );
        }

        return return_template('destination_template_'.$id);
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada post setting location
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_post_type_setting_location_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/post_type_setting_location_form.html','language_post_type_setting_form_template');
    add_block('language_post_type_setting_form_loop','lang_post_type_settingform_loop','language_post_type_setting_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        $i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');
            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang', ucwords($title));
				add_variable('code_lang', $code);
                add_variable('code_lang_lower', $code_lang_lower);

                if(check_active_plugin_destination())
                {
                    add_variable('destination_lang_post_setting_template', destination_lang_post_setting_location_template( $id_lang, $code_lang_lower, $state ));
                }

                parse_template('language_post_type_setting_form_loop','lang_post_type_settingform_loop',true);
            }

        }
    }

    return return_template('language_post_type_setting_form_template');
}

function destination_lang_post_setting_location_template( $id, $code_lang, $app_name="blog" )
{
    global $db;

    $qdes = $db->prepare_query("SELECT larticle_id, larticle_title, lsef FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY larticle_title ASC", "destinations", "publish");
    $rdes = $db->do_query($qdes);
    $ndes = $db->num_rows($rdes);


    if($ndes > 0){

    // echo
        set_template(PLUGINS_PATH.'/language/template_form/list_destination_post_type_location_lang.html','destination_template_'.$id);
        add_block('loop_destination_language','loop_destination_lang_'.$id,'destination_template_'.$id);

        while($ddes = $db->fetch_array($rdes))
        {
            $dest_title = ucwords($ddes['larticle_title']);
            $dest_id    = $ddes['larticle_id'];
            $array_name = "[$dest_id]";

            add_variable( 'dest_title', $dest_title );
            add_variable( 'dest_id', $dest_id );
            add_variable( 'array_name', $array_name );
            add_variable( 'code_lang_des', $code_lang );

            $d = get_costom_post_setting_lang( $app_name, '', $dest_id, $code_lang );

            // echo $d['bg_image_small'];

            add_variable('title_des_lang', get_additional_field($dest_id, 'location_title_'.$code_lang, 'destinations'));
            add_variable('description_lang', get_additional_field($dest_id, 'location_text_'.$code_lang, 'destinations'));
            add_variable('meta_title_lang', get_additional_field($dest_id, 'location_meta_title_'.$code_lang, 'destinations'));
            add_variable('meta_description_lang', get_additional_field($dest_id, 'location_meta_description_'.$code_lang, 'destinations'));
            add_variable('meta_keywords_lang', get_additional_field($dest_id, 'location_meta_keywords_'.$code_lang, 'destinations'));

            parse_template( 'loop_destination_language', 'loop_destination_lang_'.$id, true );
        }

        return return_template('destination_template_'.$id);
    }
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada post
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_post_func()
{
	global $db;

	set_template(PLUGINS_PATH.'/language/template_form/post_form.html','language_post_form_template');
	add_block('language_post_form_loop','lang_post_form_loop','language_post_form_template');

	$q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

	$type = $_GET['state'];

    if($n > 0)
    {
		$i_lang = 0;
        while($d = $db->fetch_array($r))
        {
			$id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
			$class_active    = (($status_default == 1) ? ' class="active"' : '');

            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang', ucwords($title));
				add_variable('code_lang', $code);
				add_variable('code_lang_lower', $code_lang_lower);

                $custom_field_lang  = get_custom_field_content( $i_lang, $id, $state, true, $code_lang_lower );
                $custom_field_lang_html = "";

                if(!empty($custom_field_lang))
                {
                    $custom_field_lang_html  = '<fieldset class="custom_field_lang">';
                    $custom_field_lang_html .= $custom_field_lang;
                    $custom_field_lang_html .= '</fieldset>';
                }

                if($state == "experience")
                {

                    $link_experience_lang  = get_additional_field( $id, 'link_experience_'.$code_lang_lower, $state );

                    $custom_field_lang_html .= '
                    <fieldset>
                        <div class="additional_data">
                            <h2>Link ('.$title.')</h2>
                            <div class="additional_content additional-news">
                                <fieldset>
                                    <p>URL</p>
                                    <input type="text" class="textbox" name="additional_fields[link_experience_'.$code_lang_lower.'][0]" placeholder="Link Experience" autocomplete="off" value="' . $link_experience_lang . '" />
                                </fieldset>
                            </div>
                        </div>
                    </fieldset>
                    ';
                }

                add_variable( 'custom_field_lang', $custom_field_lang_html);

                if(is_edit())
                {
                    $article_title_lang = get_additional_field($id,'title_'.$code_lang_lower,$state);
					$article_subtitle_lang = get_additional_field($id,'subtitle_'.$code_lang_lower,$state);
					$article_content_lang = get_additional_field($id,'content_'.$code_lang_lower,$state);
					$brief_lang = get_additional_field($id,'brief_'.$code_lang_lower,$state);

					// META DATA
					$meta_title_lang = get_additional_field($id,'meta_title_'.$code_lang_lower,$state);
					$meta_keywords_lang = get_additional_field($id,'meta_keywords_'.$code_lang_lower,$state);
					$meta_description_lang = get_additional_field($id,'meta_description_'.$code_lang_lower,$state);

					add_variable('article_title_lang', $article_title_lang);
					add_variable('article_subtitle_lang', $article_subtitle_lang);
					add_variable('article_content_lang', $article_content_lang);
					add_variable('brief_lang', $brief_lang);

					add_variable('meta_title_lang', $meta_title_lang);
					add_variable('meta_keywords_lang', $meta_keywords_lang);
					add_variable('meta_description_lang', $meta_description_lang);
					add_variable('i_lang', $i_lang);
					$i_lang++;
                }
                else
                {
					// GET CATEGORIES
                    add_variable( 'all_categories_lang', all_categories_lang( 0, 'categories', $type, '', '', $code_lang_lower ) );
                    add_variable( 'most_used_categories_lang', get_most_used_categories_lang( $type, '', array(), $code_lang_lower ) );

                    // GET TAGS
                    add_variable( 'all_tags_lang', get_post_tags_lang( 0, 0, $type, $code_lang_lower ) );
                    add_variable( 'most_used_tags_lang', get_most_used_tags_lang( $type,'', $code_lang_lower ) );

					add_variable('i_lang', 0);
				}

				parse_template('language_post_form_loop','lang_post_form_loop',true);
			}
		}
	}

	return return_template('language_post_form_template');
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan taxonomy language
| -------------------------------------------------------------------------------------------------------------------------
*/
function all_categories_lang( $index, $rule_name, $group, $rule_id = array(), $lang="", $code_lang )
{
    $xcode  = json_encode( $rule_id );
    $return = recursive_taxonomy_lang( $index, $rule_name, $group, 'checkbox', $rule_id, '', '', '', '', $code_lang );
    $return .= "<span id=\"selected_category_".$code_lang."_" . $index . "\">$xcode</span>";
    return $return;
}


function recursive_taxonomy_lang( $index, $rule_name, $group, $type = 'select', $rule_id = array(), $order = 'ASC', $parent = 0, $level = 0, $related_to_article = false, $code_lang )
{
    global $db;
    if( !$related_to_article )
    {
        $s = 'SELECT * FROM lumonata_rules
                  WHERE lrule=%s AND (lgroup=%s OR lgroup=%s) AND lparent=%d ORDER BY lorder ' . $order;
    }
    else
    {
        $s = 'SELECT a.* FROM lumonata_rules a, lumonata_rule_relationship b, lumonata_articles c
                  WHERE a.lrule=%s
                  AND (a.lgroup=%s OR a.lgroup=%s)
                  AND a.lparent=%d
                  AND a.lrule_id=b.lrule_id
                  AND b.lapp_id=c.larticle_id
                  AND c.lshare_to=0
                  GROUP BY a.lrule_id
                  ORDER BY a.lorder ' . $order;
    }
    $q        = $db->prepare_query( $s, $rule_name, $group, 'default', $parent );
    $r        = $db->do_query( $q );
    $n        = $db->num_rows( $r );
    $items    = '';
    $end_item = '';
    $sts      = '';
    if( $type == 'li' && $n > 0 )
    {
        $items    = '<ul>';
        $end_item = '</ul>';
    }
    elseif( $type == 'checkbox' )
    {
        $items    = '<ul class="' . ( $level == 0 ? 'the_categories' : '' ) . '">';
        $end_item = '</ul>';
    }
    if( $n > 0 )
    {
        $level += 1;
    }
    else
    {
        $level = $level;
    }
    $dashed = '';
    for( $i = 0; $i < $level; $i++ )
    {
        $x = $level - 1;
        if( $x == $i )
        {
            $dashed .= '';
        }
        else
        {
            $dashed .= '&nbsp;&nbsp;&nbsp;';
        }
    }
    while( $d = $db->fetch_array( $r ) )
    {
		$next_level = recursive_taxonomy_lang( $index, $rule_name, $group, $type, $rule_id, $order, $d['lrule_id'], $level, $related_to_article, $code_lang );
		// $lname      = $d['lname'];

		$lname = get_rule_additional_field($d['lrule_id'],'name_'.$code_lang, $group);
        if( $type == 'select' )
        {
            $sts = in_array( $d['lrule_id'], $rule_id ) ? 'selected="selected"' : '';
            $items .= '<option data-level="' . $level . '" value="' . $d['lrule_id'] . '" ' . $sts . '>' . $dashed . $lname . '</option>';
            $items .= $next_level;
        }
        elseif( $type == 'checkbox' )
        {
            if( is_array( $rule_id ) )
            {
                $sts = in_array( $d['lrule_id'], $rule_id ) ? 'checked="checked"' : '';
            }
            $items .= '
                <li>
                    <input type="checkbox" name="category[' . $index . '][]" value="' . $d['lrule_id'] . '" id="the_category_' . $index . '_' . $d['lrule_id'] . '" ' . $sts . ' autocomplete="off"/>
                    <label for=the_category_' . $index . '_' . $d['lrule_id'] . '" class="forclone" rel="checkbox-rule" data-rule="' . $rule_name . '">' . $lname . '</label>
                    <script type="text/javascript">
                        $("#the_category_' . $index . '_' . $d['lrule_id'] . '").click(function(){
                            var selected_val=$(this).val();
                            var checked_status = this.checked;

                            $("#the_most_category_' . $index . '_' . $d['lrule_id'] . '").each(function(){
                                if( $(this).val()==selected_val )
                                {
                                    this.checked = checked_status;
                                }
                            });
                        });
                    </script>
                </li>';
            $items .= $next_level;
        }
        elseif( $type == 'li' )
        {
            if( $rule_name == 'categories' )
            {
                if( is_permalink() )
                {
                    $the_link =  HTTP . site_url() . "/" . $group . "/" . $d['lsef'] . "/";
                }
                else
                {
                    $the_link =  HTTP . site_url() . "/?app_name=" . $group . "&cat_id=" . $d['lrule_id'];
                }
            }
            elseif( $rule_name == 'tags' )
            {
                if( is_permalink() )
                {
                    $the_link =  HTTP . site_url() . "/tag/" . $d['lsef'] . "/";
                }
                else
                {
                    $the_link =  HTTP . site_url() . "/?tag=" . $d['lsef'];
                }
            }
            $items .= "<li><a href=\"" . $the_link . "\">" . $lname . "</a>" . $next_level . "</li>";
        }
        else
        {
            if( is_array( $rule_id ) )
            {
                if( in_array( $d['lrule_id'], $rule_id ) )
                {
                    $sign = ", ";
                    if( $rule_name == 'categories' )
                    {
                        if( is_permalink() )
                        {
                            $the_category_link =  HTTP . site_url() . "/" . $group . "/" . $d['lsef'] . "/";
                        }
                        else
                        {
                            $the_category_link =  HTTP . site_url() . "/?app_name=" . $group . "&cat_id=" . $d['lrule_id'];
                        }
                        $items .= "<a href=\"" . $the_category_link . "\">" . $lname . "</a>" . $sign;
                    }
                    elseif( $rule_name == 'tags' )
                    {
                        if( is_permalink() )
                        {
                            $the_tag_link =  HTTP . site_url() . "/tag/" . $d['lsef'] . "/";
                        }
                        else
                        {
                            $the_tag_link =  HTTP . site_url() . "/?tag=" . $d['lsef'];
                        }
                        $items .= "<a href=\"" . $the_tag_link . "\">" . $lname . "</a>" . $sign;
                    }
                }
            }
            $items .= $next_level;
        }
        $items .= '';
    }
    $items .= $end_item;
    return $items;
}


function get_most_used_categories_lang( $group, $index = 0, $rule_id = array(), $code_lang )
{
    global $db;
    $s = 'SELECT a.lname, a.lrule_id FROM lumonata_rules a
              WHERE a.lrule=%s AND (a.lgroup=%s OR a.lgroup=%s)
              ORDER BY a.lcount DESC LIMIT 15';
    $q = $db->prepare_query( $s, 'categories', $group, 'default' );
    $r = $db->do_query( $q );
    if( $db->num_rows( $r ) == 0 )
    {
        $notag = '<li>No category found, please add new category.</li>';
    }
    else
    {
        $notag = '';
    }
    $return = '
        <ul class="the_categories">';
    $return .= $notag;
    while( $d = $db->fetch_array( $r ) )
    {
        $lname = get_rule_additional_field($d['lrule_id'],'name_'.$code_lang, $group);
        $sts = in_array( $d['lrule_id'], $rule_id ) ? 'checked="checked"' : '';
        $return .= '
                <li>
                    <input type="checkbox" name="most_used_category[' . $index . '][]" value="' . $d['lrule_id'] . '" id="the_most_category_' . $index . '_' . $d['lrule_id'] . '" ' . $sts . ' />
                    <label class="forclone" rel="checkbox-rule" data-rule="categories">' . $lname . '</label>
                    <script type="text/javascript">
                        $("#the_most_category_' . $index . '_' . $d['lrule_id'] . '").click(function(){
                            var selected_val   = $(this).val();
                            var checked_status = this.checked;
                            $("#the_category_' . $index . '_' . $d['lrule_id'] . '").each(function(){
                                if( $(this).val()==selected_val )
                                {
                                    this.checked = checked_status;
                                }
                            });
                        });
                    </script>
                </li>';
    }
    $return .= '
        </ul>';
    return $return;
}

function get_post_tags_lang( $post_id, $index, $group, $code_lang )
{
    global $db;
    $sql       = $db->prepare_query( "SELECT a.lname, a.lrule_id

                            FROM lumonata_rules a, lumonata_rule_relationship b

                            WHERE a.lrule_id=b.lrule_id AND a.lrule=%s AND a.lgroup=%s AND b.lapp_id=%d", 'tags', $group, $post_id );
    $r         = $db->do_query( $sql );
    $count_row = $db->num_rows( $r );
    if( $count_row == 0 )
        $notag = "No tag found, please add new tag or choose from most used tags.";
    else
        $notag = "";
    $return = "<div class=\"the_categories\" id=\"tag_list_" . $index . "\">";
    $return .= $notag;
    $i = $count_row - 1;
    while( $d = $db->fetch_array( $r ) )
    {
        $lname = get_rule_additional_field($d['lrule_id'],'name_'.$code_lang, $group);
        $return .= "<div class=\"tag_list tag_index_" . $index . " clearfix\" id=\"the_tag_list_" . $index . "_" . $i . "\">

                            <div class=\"tag_name\">" . $lname . "</div>

                            <div class=\"tag_action\"><a href=\"javascript:;\" onclick=\"$('#the_tag_list_" . $index . "_" . $i . "').remove();\">X</a></div>

                            <input type=\"hidden\" name=\"tags[$index][]\" value=\"" . $lname . "\" />

                     </div>";
        $i--;
    }
    $return .= "</div>";
    return $return;
}

function get_most_used_tags_lang( $group, $index = 0, $code_lang)
{
    global $db;
    $s      = 'SELECT a.lname, a.lrule_id
              FROM lumonata_rules a
              WHERE a.lrule=%s
              AND a.lgroup NOT IN ("global_settings", "profile")
              ORDER BY a.lcount DESC
              LIMIT 15';
    $q      = $db->prepare_query( $s, 'tags' );
    $r      = $db->do_query( $q );
    $n      = $db->num_rows( $r );
    $notag  = $n == 0 ? 'No tag found, please add new tag.' : '';
    $return = '
        <div class="the_categories">';
    $return .= $notag;
    $i = $n - 1;
    while( $d = $db->fetch_array( $r ) )
    {
        $lname = get_rule_additional_field($d['lrule_id'],'name_'.$code_lang, $group);

        $return .= "
                <div class=\"most_tag_list most_tag_" . $index . "_" . $i . " clearfix\">
                    <div class=\"tag_name\" data-id=\"" . $d['lrule_id'] . "\">" . $lname . "</div>
                    <div class=\"tag_action\"><a href=\"javascript:;\" id=\"append_tags_" . $index . "_" . $i . "\">+</a></div>
                </div>

                <script type=\"text/javascript\">
                    $(\"#append_tags_" . $index . "_" . $i . "\").click(function(){
                        var taglabel    = '" . $lname . "';
                        var count_child = $('.tag_index_" . $index . "').size();

                        thetag  = '<div class=\"tag_list tag_index_" . $index . " clearfix\" id=\"the_tag_list_" . $index . "_'+count_child+'\" >';
                        thetag += '     <div class=\"tag_name\">" . $lname . "</div>';
                        thetag += '     <div class=\"tag_action\">';
                        thetag += '         <a href=\"javascript:;\" id=\"remove_tag_'+count_child+'\" onclick=\"$(\'#the_tag_list_" . $index . "_'+count_child+'\').remove(); $(\'.most_tag_" . $index . "_" . $i . "\').animate({\'backgroundColor\':\'#FFFFFF\' },500);\">X</a>';
                        thetag += '     </div>';
                        thetag += '     <input type=\"hidden\" name=\"tags[" . $index . "][]\" value=\"" . $lname . "\" />';
                        thetag += '</div>';

                        if(count_child==0)
                        {
                            $(\"#tag_list_" . $index . "\").html('');
                            $(\"#tag_list_" . $index . "\").append(thetag);
                        }
                        else
                        {
                            $(\".tag_index_" . $index . ":first\").before(thetag);
                        }

                        $('.most_tag_" . $index . "_" . $i . "').animate({'backgroundColor':'#FF6666' },500);
                        $('.most_tag_" . $index . "_" . $i . "').animate({'backgroundColor':'#cccccc' },500);
                    });
                </script>";
        $i++;
    }
    $return .= '
        </div>';
    return $return;
}


function get_costom_post_setting_lang($type = 'blogs', $field = '', $app_id=0, $code_lang)
{
    $meta = get_meta_data( 'post_type_setting', $type, $app_id );
    $meta = json_decode( $meta, true );
    $data = array();

    if( !empty( $meta ) && json_last_error() === JSON_ERROR_NONE )
    {
        $title   = isset( $meta['title_'.$code_lang] ) && !empty( $meta['title_'.$code_lang] ) ? $meta['title_'.$code_lang] : '';
        $subtitle   = isset( $meta['subtitle_'.$code_lang] ) && !empty( $meta['subtitle_'.$code_lang] ) ? $meta['subtitle_'.$code_lang] : '';
        $desc   = isset( $meta['description_'.$code_lang] ) && !empty( $meta['description_'.$code_lang] ) ? $meta['description_'.$code_lang] : '';
        $brief   = isset( $meta['brief_'.$code_lang] ) && !empty( $meta['brief_'.$code_lang] ) ? $meta['brief_'.$code_lang] : '';

        $text_additional_book = "";
        $pdf_file_spa_menu = "";
        if($type == "spa"){
            $text_additional_book   = isset( $meta['text_additional_book_'.$code_lang] ) && !empty( $meta['text_additional_book_'.$code_lang] ) ? $meta['text_additional_book_'.$code_lang] : '';
            $pdf_file_spa_menu   = isset( $meta['pdf_file_spa_menu_'.$code_lang] ) && !empty( $meta['pdf_file_spa_menu_'.$code_lang] ) ? $meta['pdf_file_spa_menu_'.$code_lang] : '';
        }

        $m_title = isset( $meta['meta_title_'.$code_lang] ) && !empty( $meta['meta_title_'.$code_lang] ) ? $meta['meta_title_'.$code_lang] : '';
        $m_kwrds = isset( $meta['meta_keywords_'.$code_lang] ) && !empty( $meta['meta_keywords_'.$code_lang] ) ? $meta['meta_keywords_'.$code_lang] : '';
        $m_desc  = isset( $meta['meta_description_'.$code_lang] ) && !empty( $meta['meta_description_'.$code_lang] ) ? $meta['meta_description_'.$code_lang] : '';

        $data = array(
            'title_'.$code_lang                => $title,
            'subtitle_'.$code_lang             => $subtitle,
            'description_'.$code_lang          => $desc,
            'brief_'.$code_lang                => $brief,
            'meta_title_'.$code_lang           => $m_title,
            'meta_keywords_'.$code_lang        => $m_kwrds,
            'meta_description_'.$code_lang     => $m_desc,
            'text_additional_book_'.$code_lang => $text_additional_book,
            'pdf_file_spa_menu_'.$code_lang    => $pdf_file_spa_menu
        );
    }
    else
    {
        $data = array(
            'title_'.$code_lang                => '',
            'subtitle_'.$code_lang             => '',
            'description_'.$code_lang          => '',
            'brief_'.$code_lang                => '',
            'meta_title_'.$code_lang           => '',
            'meta_keywords_'.$code_lang        => '',
            'meta_description_'.$code_lang     => '',
            'text_additional_book_'.$code_lang => '',
            'pdf_file_spa_menu_'.$code_lang    => ''
        );
    }

    if( empty( $field ) )
    {
        return $data;
    }
    else
    {
        return $data[$field];
    }
}


function get_language_data()
{
    global $db;

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
	$n = $db->num_rows($r);

    $data = array();
    if($n > 0)
    {
        while($d = $db->fetch_array($r))
        {
            $id             = $d['larticle_id'];
            $title          = ucwords($d['larticle_title']);
            $code           = strtolower(get_additional_field( $id, 'language_code', 'language' ));
            $status_default = get_additional_field( $id, 'status_default', 'language' );
            if($status_default != 1)
            {
                $data['code_lang'][]  = $code;
                $data['title_lang'][] = $title;
            }
        }
    }

    return $data;
}


function label_menu_form_lang_func($id, $data_lang='', $items_val='')
{
    $html = "
        <a href=\"\" class=\"config_menu\" data-filter=\"form_label_lang_$id\">Configuration Label Multi Language</a>
        <div class=\"clear-element\"></div>
        <div class=\"form_lang_label form_label_lang_$id\">
    ";
    if(is_array($data_lang))
    {
        $code_lang  = $data_lang['code_lang'];
        $title_lang = $data_lang['title_lang'];
        $i = 0;
        foreach($code_lang as $d)
        {
            $value = "";
            if(!empty($items_val))
            {
                $value = (isset($items_val[0]['label_'.$d]) ? $items_val[0]['label_'.$d] : '' );
            }
            $html .= '
                <div class="input_lang">
                    <label>Label '.$title_lang[$i].': </label>
                    <input type="text" value="'.$value.'" id="label_'.$d.'_'.$id.'" name="title_'.$d.'['.$id.']" class="medium_textbox" />
                </div>
            ';
            $i++;
        }
    }

    $html .= "
        </div>
    ";

    return $html;
}


function script_js_label_menu_lang_func()
{
    $html = "
    <script type=\"text/javascript\">
        jQuery(document).ready(function(){
            $('.config_menu').each(function(){
                $(this).click(function(){
                    var filter = $(this).attr(\"data-filter\");
                    console.log(filter);
                    if($('.'+filter).is(':visible')){
                        $('.'+filter).slideUp(300);
                    }else{
                        $('.'+filter).slideDown(300);
                    }
                    return false;
                });
            })
        });
    </script>
    ";
    return $html;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
|	Fungsi ini digunakan untuk menampilkan form multilanguage pada menu list restaurant
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_menu_list_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/menu_list_form.html','language_menu_form_template');
    add_block('language_menu_list_form_loop','lang_menu_list_form_loop','language_menu_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        $i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');
            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang_menu', ucwords($title));
				add_variable('code_lang_menu', $code);
                add_variable('code_lang_lower_menu', $code_lang_lower);

                parse_template('language_menu_list_form_loop','lang_menu_list_form_loop',true);
            }
        }
    }

    return return_template('language_menu_form_template');
}


/*
| -------------------------------------------------------------------------------------------------------------------------
|	Fungsi ini digunakan untuk menampilkan form multilanguage pada menu category restaurant
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_menu_category_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/menu_category_form.html','language_menu_category_form_template');
    add_block('language_menu_category_form_loop','lang_menu_category_form_loop','language_menu_category_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        $i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');
            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang_menu', ucwords($title));
				add_variable('code_lang_menu', $code);
                add_variable('code_lang_lower_menu', $code_lang_lower);

                parse_template('language_menu_category_form_loop','lang_menu_category_form_loop',true);
            }
        }
    }

    return return_template('language_menu_category_form_template');
}


/*
| -------------------------------------------------------------------------------------------------------------------------
|	Fungsi ini digunakan untuk menampilkan form multilanguage pada treatment list di spa
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_treatment_list_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/treatment_list_form.html','language_treatment_form_template');
    add_block('language_treatment_list_form_loop','lang_treatment_list_form_loop','language_treatment_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        $i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');
            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang_treatment', ucwords($title));
				add_variable('code_lang_treatment', $code);
                add_variable('code_lang_lower_treatment', $code_lang_lower);

                parse_template('language_treatment_list_form_loop','lang_treatment_list_form_loop',true);
            }
        }
    }

    return return_template('language_treatment_form_template');
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada treatment category di spa
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_treatment_category_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/treatment_category_form.html','language_treatment_category_form_template');
    add_block('language_treatment_category_form_loop','lang_treatment_category_form_loop','language_treatment_category_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);
    if($n > 0)
    {
        $i_lang = 0;
        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');
            if($status_default != 1)
            {
                $id = ((isset($_GET['id'])) ? $_GET['id'] : '' );
                $state = $_GET['state'];
				add_variable('title_lang_treatment', ucwords($title));
				add_variable('code_lang_treatment', $code);
                add_variable('code_lang_lower_treatment', $code_lang_lower);

                parse_template('language_treatment_category_form_loop','lang_treatment_category_form_loop',true);
            }
        }
    }

    return return_template('language_treatment_category_form_template');
}



/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan form multilanguage pada post type setting
| -------------------------------------------------------------------------------------------------------------------------
*/
function language_data_destination_func()
{
    global $db;

    set_template(PLUGINS_PATH.'/language/template_form/destination_form.html','language_destination_form_template');
    add_block('language_destination_form_loop','lang_destination_form_loop','language_destination_form_template');

    $q = $db->prepare_query("SELECT larticle_id, larticle_title FROM lumonata_articles WHERE larticle_type=%s AND larticle_status=%s ORDER BY lorder ASC", "language", "publish");
	$r = $db->do_query($q);
    $n = $db->num_rows($r);

    if($n > 0)
    {
        $i_lang = 0;

        $id    = ((isset($_GET['id'])) ? $_GET['id']: '' );
        $state = $_GET['state'];

        while($d = $db->fetch_array($r))
        {
            $id_lang         = $d['larticle_id'];
			$title           = $d['larticle_title'];
			$code            = strtoupper(get_additional_field( $id_lang, 'language_code', 'language' ));
			$code_lang_lower = strtolower($code);
			$status_default  = get_additional_field( $id_lang, 'status_default', 'language' );
            $class_active    = (($status_default == 1) ? ' class="active"' : '');

            if($status_default != 1)
            {
                $title_destination            = get_additional_field( $id, 'title_'.$code_lang_lower, $state );
                $title_home_page              = get_additional_field( $id, 'title_home_page_'.$code_lang_lower, $state );
                $subtitle_home_page           = get_additional_field( $id, 'subtitle_home_page_'.$code_lang_lower, $state );

								$title_landing_page              = get_additional_field( $id, 'title_landing_page_'.$code_lang_lower, $state );
                $subtitle_landing_page           = get_additional_field( $id, 'subtitle_landing_page_'.$code_lang_lower, $state );

                $content_lang                 = get_additional_field( $id, 'content_'.$code_lang_lower, $state );
                $brief_landing_page_lang      = get_additional_field( $id, 'brief_'.$code_lang_lower, $state );
                $title_reception_venue_1_lang = get_additional_field( $id, 'title_reception_venue_1_'.$code_lang_lower, $state );
                $title_reception_venue_2_lang = get_additional_field( $id, 'title_reception_venue_2_'.$code_lang_lower, $state );
                $meta_title_lang              = get_additional_field( $id, 'meta_title_'.$code_lang_lower, $state );
                $meta_keywords_lang           = get_additional_field( $id, 'meta_keywords_'.$code_lang_lower, $state );
                $meta_description_lang        = get_additional_field( $id, 'meta_description_'.$code_lang_lower, $state );
                $url_popup_newsletter_lang    = get_additional_field( $id, 'url_popup_newsletter_'.$code_lang_lower, $state );
                $title_policy_lang            = get_additional_field( $id, 'title_policy_'.$code_lang_lower, $state );
                $sub_title_policy_lang        = get_additional_field( $id, 'sub_title_policy_'.$code_lang_lower, $state );
                $description_policy_lang      = get_additional_field( $id, 'description_policy_'.$code_lang_lower, $state );
                $meta_title_policy_lang       = get_additional_field( $id, 'meta_title_policy_'.$code_lang_lower, $state );
                $meta_keywords_policy_lang    = get_additional_field( $id, 'meta_keywords_policy_'.$code_lang_lower, $state );
                $meta_description_policy_lang = get_additional_field( $id, 'meta_description_policy_'.$code_lang_lower, $state );

				add_variable('title_lang', ucwords($title));
				add_variable('code_lang', $code);
                add_variable('code_lang_lower', $code_lang_lower);
                add_variable('title_destination', $title_destination);
                add_variable('title_home_page_lang', $title_home_page);
                add_variable('subtitle_home_page_lang', $subtitle_home_page);

								add_variable('title_landing_page_lang', $title_landing_page);
                add_variable('subtitle_landing_page_lang', $subtitle_landing_page);

                add_variable('title_reception_venue_1_lang', $title_reception_venue_1_lang);
                add_variable('title_reception_venue_2_lang', $title_reception_venue_2_lang);
                add_variable('meta_title_lang', $meta_title_lang);
                add_variable('meta_keywords_lang', $meta_keywords_lang);
                add_variable('meta_description_lang', $meta_description_lang);
                add_variable('url_popup_newsletter_lang', $url_popup_newsletter_lang);
                add_variable('title_policy_lang', $title_policy_lang);
                add_variable('sub_title_policy_lang', $sub_title_policy_lang);
                add_variable('description_policy_lang', $description_policy_lang);
                add_variable('meta_title_policy_lang', $meta_title_policy_lang);
                add_variable('meta_keywords_policy_lang', $meta_keywords_policy_lang);
                add_variable('meta_description_policy_lang', $meta_description_policy_lang);
                add_variable('post_content_lang',new_textarea('post_content_lang[content_'.$code_lang_lower.']',0,$content_lang,0,true));
                add_variable('brief_lang',new_textarea('brief_lang[brief_'.$code_lang_lower.']',0,$brief_landing_page_lang,0,true));

                parse_template('language_destination_form_loop','lang_destination_form_loop',true);
            }
        }
    }

    return return_template('language_destination_form_template');
}
?>
