jQuery(document).ready(function(){
    var state_url  = jQuery('[name=state_url]').val();
    var plugin_url = jQuery('[name=plugin_url]').val();

     //-- Coorporate Image Function
     jQuery('[name=icon_language]').change(function(el){
        var data = new FormData();
            data.append('ajax_key', 'add-icon-image');
            data.append('app_name', 'language');
            data.append('post_id', jQuery('[name=post_id]').val());
            data.append('icon_language', el.currentTarget.files[0]);

        var xhrObject = new XMLHttpRequest();
        xhrObject.open('POST', state_url);
        xhrObject.onreadystatechange = function(){
            if(xhrObject.readyState==4 && xhrObject.status==200){
                var e = JSON.parse(xhrObject.responseText);

                if(e.result=='success'){
                     var images =
                    '<div class="box">'+
                        '<img src="http://'+plugin_url+'/background/'+e.filename+'" />'+
                        '<div class="overlay" style="display: none;">'+
                            '<a data-post-id="'+e.post_id+'" class="cp-delete-header">'+
                                '<img src="http://'+plugin_url+'/images/delete.png">'+
                            '</a>'+
                        '</div>'+
                    '</div>';

                    jQuery('.icon-wrapp').html(images);
                }else{
                    alert('Failed to add icon images')
                }
            }
        }

        xhrObject.send(data);
    });

    jQuery('.cp-delete-header').live('click',function(){
        var selector = jQuery(this).parent().parent();
        var url = state_url;
        var prm = new Object;
            prm.ajax_key = 'delete-icon-image';
            prm.post_id  = jQuery(this).attr('data-post-id');

        jQuery.post(url, prm, function(e){
            if(e.result=='success'){
                selector.fadeOut(200,function(){ jQuery(this).remove(); });
            }
        },'json');
    });

    jQuery('.list-box, .background-wrapp .box, .icon-wrapp .box').live('mouseover',function(){
        jQuery(this).find('.overlay').show();
    });

    jQuery('.list-box, .background-wrapp .box, .icon-wrapp .box').live('mouseout',function(){
        jQuery(this).find('.overlay').hide();
    });

    
});
