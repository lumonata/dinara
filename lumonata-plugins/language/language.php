<?php
/*
| -------------------------------------------------------------------------------------------------------------------------
    Plugin Name: Lumonata Language Application
    Plugin URL: http://lumonata.com/
    Description: This plugin is use for adding multi language
    Author: Tantri Mindrawan
    Author URL: http://lumonata.com/
    Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

require_once "function_helper.php";
require_once "function_string_translation.php";

add_privileges('administrator', 'language', 'insert');
add_privileges('administrator', 'language', 'update');
add_privileges('administrator', 'language', 'delete');



//-- Add custom icon menu
add_actions('header_elements','language_header_element');
function language_header_element()
{
    return '<link href="'.HTTP.SITE_URL.'/lumonata-plugins/language/style.css" rel="stylesheet">';
}

//-- Add sub menu under applications menu
add_main_menu(array('language'=>'Language'));
add_actions('language','set_language_data');

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini otomatis akan dijalankan untuk melakukan filter action
	yang akan dilakukan -- (get, add, edit, delete, delete all)
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_language_data()
{
	if(is_add_new())
	{
		return add_language();
	}
	elseif(is_edit())
	{
		return edit_language();
	}
	elseif(is_delete_all())
	{
		return delete_all_language();
	}
	elseif(is_confirm_delete())
	{
		foreach($_POST['id'] as $key=>$val)
		{
			delete_language($val);
		}
	}
	elseif(is_ajax_request())
	{
		return execute_ajax_request_language();
	}

	if(is_num_language() > 0)
	{
		return get_language_list();
	}
	else
	{
		header('location:'.get_state_url('language').'&prc=add_new');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Fungsi ini digunakan untuk mendapatkan jumlah data language dari database
| -------------------------------------------------------------------------------------------------------------------------
*/
function is_num_language()
{
	global $db;

	$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
	$q = $db->prepare_query($s,'language');
	$r = $db->do_query($q);

	return $db->num_rows($r);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
| Fungsi ini digunakan untuk menampilkan data language dari database
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_language_list()
{
    global $db;

    $list   = '';
	$search = (isset($_POST['s']) ? $_POST['s'] : '');
	$page   = (isset($_GET['page']) ? $_GET['page'] : 1);
	$url    = get_state_url('language')."&page=";
	$viewed = list_viewed();
	$limit  = ($page-1) * $viewed;
    $start  = ($page - 1) * $viewed + 1;

    if(is_search())
	{
		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s ORDER BY lorder';
		$q = $db->prepare_query($s,'language',"%".$_POST['s']."%");
		$r = $db->do_query($q);
		$n = $db->num_rows($r);

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s ORDER BY lorder LIMIT %d, %d ';
		$q = $db->prepare_query($s,'language','%'.$_POST['s'].'%',$limit,$viewed);
	}
	else
	{
		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder';
		$q = $db->prepare_query($s,'language');
		$r = $db->do_query($q);
		$n = $db->num_rows($r);

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder LIMIT %d, %d';
		$q = $db->prepare_query($s,'language',$limit,$viewed);
	}


  // NOTE: BUTTON LIST LANGUAGE
	// $button = '<li>'.button('button=add_new',get_state_url('language').'&prc=add_new').'</li><li>'.button('button=delete&type=submit&enable=false').'</li>';
	$button = "<li><input type=\"button\" class=\"btn-default-button\" name=\"language_string\" value=\"String Translation Language\" onclick=\"location='".get_state_url('language-string')."';\"></li>";
	// $button = '';

    set_template(PLUGINS_PATH."/language/list.html",'language_list_template');

	$r = $db->do_query($q);
	if($db->num_rows($r) > 0)
	{
		add_block('language_list','lang_list','language_list_template');

		while($d=$db->fetch_array($r))
		{
			$status = $d['larticle_status'];
            $status_default = get_additional_field($d['larticle_id'],'status_default','language');
            $status_default_text = (($status_default == 1) ? 'Yes' : '');

            $button_make_default = (($status_default != 1) ? '|
            <a href="javascript:;" class="delete_link" id="delete_'.$d['larticle_id'].'" rel="'.$d['larticle_id'].'" title="'.$d['larticle_title'].'">Make Default</a> ' : '');

            // $button_delete = (($status_default != 1) ? '| <a href="javascript:;" class="delete_link" id="delete_'.$d['larticle_id'].'" rel="'.$d['larticle_id'].'" title="'.$d['larticle_title'].'">Delete</a>' : '');
			$button_delete = '';

			$button_status = "";
			if($status_default==0){
				$button_status = (($status == "publish") ? '|
				<a href="javascript:;" class="change_status" id="change_status_'.$d['larticle_id'].'" rel="'.$d['larticle_id'].'" title="'.$d['larticle_title'].'" data-status="unpublish">Unpublish</a> ' : '| <a href="javascript:;" class="change_status" id="change_status_'.$d['larticle_id'].'" rel="'.$d['larticle_id'].'" title="'.$d['larticle_title'].'" data-status="publish">Publish</a>');
			}

			add_variable('id',$d['larticle_id']);
			add_variable('title',$d['larticle_title']);
			add_variable('description',limit_words(strip_tags($d['larticle_content']), 50));
			add_variable('status_default',$status_default_text);
			add_variable('button_make_default',$button_make_default);
			add_variable('button_delete',$button_delete);
			add_variable('button_status',$button_status);
			add_variable('status',ucwords($status));
			add_variable('status_class',$status);

			parse_template('language_list','lang_list',true);
		}
    }

    add_block('language','dest','language_list_template');
	add_actions('section_title','Language - List');

	add_variable('button',$button);
	add_variable('search_val',$search);
	add_variable('start_order',$start);
	add_variable('state','language');
	add_variable('state_url',get_state_url('language'));
	add_variable('template_url',TEMPLATE_URL);
	add_variable('http',HTTP);

	parse_template('language','dest',false);
	return return_template('language_list_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk meyimpan data language ke database
| -------------------------------------------------------------------------------------------------------------------------
*/
function add_language()
{
    $message  = '';
	$mess_arr = array();

	$post_id       = (isset($_POST['post_id']) ? $_POST['post_id'] : time());
	$post_title    = (isset($_POST['post_title']) ? $_POST['post_title'] : '');
    $post_content  = (isset($_POST['post_content']) ? rem_slashes($_POST['post_content']) : '');
    $language_code = (isset($_POST['language_code']) ? $_POST['language_code'] : '');
    $language_code_symbol = (isset($_POST['language_code_symbol']) ? $_POST['language_code_symbol'] : '');
    $url_direct    = (isset($_POST['url_direct']) ? $_POST['url_direct'] : '');

    if(is_save_changes())
	{
		if(empty($post_title))
		{
			$mess_arr[] = '- Please input your language title';

		}

		if(empty($post_content))
		{
			$mess_arr[] = '- Please input your language description';
        }

        if(empty($language_code))
		{
			$mess_arr[] = '- Please input your language code';
		}

		if(empty($mess_arr))
		{
			$is_save_success = save_article($post_title, '', '',  $post_content, 'publish', 'language','', '', '', '', '', '', '', '', 'not-allowed');

			if($is_save_success)
			{
                $post_id = mysql_insert_id();

				set_additional_language($post_id);
				sync_attachment_images_language($_POST['post_id'], $post_id);

				$post_id = time();
				$post_title = '';
				$post_content = '';
				$language_code = '';
				$language_code_symbol = '';
				$url_direct = '';

				$message = '<div class="message success">New language has save succesfully.</div>';
			}
			else
			{
				$message = '<div class="message error">Something wrong, please try again.</div>';
			}
		}
		else
		{
			$message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
		}
	}

  // NOTE: BUTTON DIDALAM SETELAH ADD NEW

  // $button	= '
	// <li>'.button('button=save_changes&label=Save').'</li>
	// <li>'.button('button=add_new',get_state_url('language').'&prc=add_new').'</li>
	// <li>'.button('button=cancel',get_state_url('language')).'</li>';

	// $button ='
	// <li>'.button('button=save_changes&label=Save').'</li>
	// <li>'.button('button=cancel',get_state_url('language')).'</li>
	// ';

	$button ='
	<li>'.button('button=cancel',get_state_url('language')).'</li>
	';

    $option_status_default = '
        <option value="1">Yes</option>
        <option value="0">No</option>
    ';

    set_template(PLUGINS_PATH.'/language/form.html','language_form_template');
    add_block('language_form','lang_form','language_form_template');
    add_actions('section_title','Add New Language');

    add_variable('form_title','Add New Language');
	add_variable('post_title',$post_title);
	add_variable('post_content',new_textarea('post_content',0,$post_content,0,true));

    add_variable('site_url',SITE_URL);
	add_variable('plugin_url',SITE_URL.'/lumonata-plugins/language');
	add_variable('state_url',get_state_url('language'));
	add_variable('post_id',$post_id);
	add_variable('message',$message);
	add_variable('button',$button);
	add_variable('option_status_default',$option_status_default);

    parse_template('language_form','lang_form',false);
	return return_template('language_form_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengubah data language di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function edit_language()
{
    global $db;

	$message  = '';
	$mess_arr = array();
	$whatsapp_content = '';

	$post_title   = (isset($_POST['post_title']) ? $_POST['post_title'] : '');
	$post_content = (isset($_POST['post_content']) ? rem_slashes($_POST['post_content']) : '');
	$language_code = (isset($_POST['language_code']) ? $_POST['language_code'] : '');
	$language_code_symbol = (isset($_POST['language_code_symbol']) ? $_POST['language_code_symbol'] : '');
	$url_direct = (isset($_POST['url_direct']) ? $_POST['url_direct'] : '');

    if(is_save_changes())
	{
		if(empty($post_title))
		{
			$mess_arr[] = '- Please input your language title';
		}

		if(empty($post_content))
		{
			$mess_arr[] = '- Please input your language description';
        }

        if(empty($language_code))
		{
			$mess_arr[] = '- Please input your language code';
		}

		if(empty($mess_arr))
		{
			$is_edit_success = update_article($_GET['id'], $post_title, '', '', $post_content, 'publish', 'language','', '', '', '', '', '', '', '', '', 'not-allowed', '');

			if($is_edit_success)
			{
				set_additional_language($_GET['id']);

				$message = '<div class="message success">Language data has been successfully edited.</div>';
			}
			else
			{
				$message = '<div class="message error">Something wrong, please try again.</div>';
			}
		}
		else
		{
			$message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
		}
    }

    // $button	= '
  	// <li>'.button('button=save_changes&label=Save').'</li>
  	// <li>'.button('button=add_new',get_state_url('language').'&prc=add_new').'</li>
  	// <li>'.button('button=cancel',get_state_url('language')).'</li>';

  	$button ='
  	<li>'.button('button=save_changes&label=Save').'</li>
  	<li>'.button('button=cancel',get_state_url('language')).'</li>
  	';

	set_template(PLUGINS_PATH.'/language/form.html','language_form_template');
	add_block('language_form','lang_form','language_form_template');
    add_actions('section_title','Edit Language');

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
	$q = $db->prepare_query($s,'language',$_GET['id']);
	$r = $db->do_query($q);
	if($db->num_rows($r) > 0)
	{
        $d = $db->fetch_array($r);

        add_variable('form_title','Edit Language');
		add_variable('post_title',$d['larticle_title']);
        add_variable('post_content',new_textarea('post_content',0,rem_slashes($d['larticle_content']),$_GET['id'],true));
        add_variable('language_code',get_additional_field($_GET['id'],'language_code','language'));
        add_variable('language_code_symbol',get_additional_field($_GET['id'],'language_code_symbol','language'));
        add_variable('url_direct',get_additional_field($_GET['id'],'url_direct','language'));
        add_variable('icon_language',set_icon($_GET['id']));

        $status_default = get_additional_field($_GET['id'],'status_default','language');
        if($status_default == 1){
            $option_status_default = '
                <option value="1" selected>Yes</option>
                <option value="0">No</option>
            ';
        }else{
            $option_status_default = '
                <option value="1">Yes</option>
                <option value="0" selected>No</option>
            ';
        }

        add_variable('site_url',SITE_URL);
		add_variable('plugin_url',SITE_URL.'/lumonata-plugins/language');
		add_variable('state_url',get_state_url('language'));
		add_variable('post_id',$_GET['id']);
		add_variable('message',$message);
		add_variable('button',$button);
		add_variable('option_status_default',$option_status_default);
		add_variable('readonly', 'readonly');

		parse_template('language_form','lang_form',false);
		return return_template('language_form_template');
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan icon language pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_icon($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'icon_language','language');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/language/background/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="cp-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/language/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data language information di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_additional_language($post_id)
{
	// Language Code
	if(!is_edit()){
		$language_code = (isset($_POST['language_code']) ? trim($_POST['language_code']) : '');
		edit_additional_field($post_id, 'language_code', $language_code, 'language');
	}

	// Status Default
	// if(isset($_POST['status_default'])==1){
	// 	update_default_to_no();
	// }
    $status_default = (isset($_POST['status_default']) ? $_POST['status_default'] : '0');
	edit_additional_field($post_id, 'status_default', $status_default, 'language');

	$url_direct = (isset($_POST['url_direct']) ? $_POST['url_direct'] : '');
	edit_additional_field($post_id, 'url_direct', $url_direct, 'language');

	$language_code_symbol = (isset($_POST['language_code_symbol']) ? $_POST['language_code_symbol'] : '');
    edit_additional_field($post_id, 'language_code_symbol', $language_code_symbol, 'language');
}



/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk update status default all language menjadi 0
| -------------------------------------------------------------------------------------------------------------------------
*/
function update_default_to_no()
{
    global $db;

    $q = $db->prepare_query("UPDATE lumonata_additional_fields SET lvalue=%d WHERE lkey=%s AND lapp_name=%s", 0, 'status_default', 'language');
    $r = $db->do_query($q);

    return $r;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus multiple data language di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_all_language()
{
    global $db;
    add_actions('section_title','Delete Languae');

    $warning = '
	<h1 class="form-title">Delete Language</h1>
	<div class="tab_container">
		<div class="single_content language-wrapp">';
			$warning .= '
			<form action="" method="post">';
				$warning .= '
				<div class="alert_red_form boxs">
					<strong>
						Are you sure want to delete this
						language'.(count($_POST['select']) > 1 ? 's' : '').' :
					</strong>
					<ol>';
						foreach($_POST['select'] as $key=>$val)
						{
							$s = 'SELECT larticle_title FROM lumonata_articles WHERE larticle_id=%d';
							$q = $db->prepare_query($s,$val);
							$r = $db->do_query($q);
							$d = $db->fetch_array($r);

							$warning .= '
							<li>
								'.$d['larticle_title'].'
								<input type="hidden" name="id[]" value="'.$val.'" />
							</li>';
						}
						$warning .= '
					</ol>
				</div>
				<div class="alert-button-box">
					<input type="submit" name="confirm_delete" value="Yes" class="button" />
					<input type="button" name="confirm_delete" value="No" class="button" onclick="location=\''.get_state_url('language').'\'" />
				</div>
			</form>
		</div>
	</div>';

	return $warning;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus data language di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_language($id='')
{
	global $db;

	$s = 'DELETE FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
	$q = $db->prepare_query($s,'language',$id);

	if($db->do_query($q))
	{
		delete_additional_field($id, 'language');

		return true;
	}
	else
	{
		return false;
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menjalankan ajax request
| -------------------------------------------------------------------------------------------------------------------------
*/
function execute_ajax_request_language()
{
	global $db;



	if($_POST['ajax_key'] =='search-language')
	{
	    $result = '';

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
	    $q = $db->prepare_query($s,'language',"%".$_POST['search_val']."%");
	   	$r = $db->do_query($q);
		if($db->num_rows($r) > 0)
	    {
			while($d=$db->fetch_array($r))
			{
				$result .= '
                <div class="list_item boxs clearfix" id="theitem_'.$d['larticle_id'].'">
                    <div class="boxs title push-left">
                        <input type="checkbox" name="select[]" class="title_checkbox select" value="'.$d['larticle_id'].'" />
                        <label>'.$d['larticle_title'].'</label>
                    </div>
                    <div class="boxs description push-left"><p>'.limit_words(strip_tags($d['larticle_content']), 50).'</p></div>
                    <div class="the_navigation_list">
                        <div class="list_navigation" style="display:none;" id="the_navigation_'.$d['larticle_id'].'">
                            <a href="'.get_state_url('language').'&prc=edit&id='.$d['larticle_id'].'">Edit</a> |
                            <a href="javascript:;" class="delete_link" id="'.$d['larticle_id'].'" rel="delete_'.$d['larticle_id'].'">Delete</a>
                        </div>
                    </div>
                </div>';
			}
        }
		else
		{
			$result = '
			<div class="alert_yellow_form">
				No result found for <em>"'.$_POST['search_val'].'"</em>.
				Check your spellling or try another terms
			</div>';
        }

		echo $result;
	}

	if($_POST['ajax_key'] =='delete-language')
	{
		if(delete_language($_POST['dest_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

  if($_POST['ajax_key'] =='reorder-lang')
	{
		update_articles_order($_POST['theitem'],$_POST['start'],'language');
		echo '{"result":"success"}';

	}

	if($_POST['ajax_key'] =='add-icon-image')
	{
		$data = set_icon_language();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-icon-image')
	{
		if(delete_icon_language($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='change-status')
	{
		if(change_status_lang($_POST['lang_id'], $_POST['status']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	exit;
}


// function reorder_language($item,$start)
// {
//   global $db;
//   foreach( $order as $key => $val )
//   {
//       $sql = $db->prepare_query( "UPDATE lumonata_articles
//                                    SET lorder=%d,
//                                    lupdated_by=%s,
//                                    ldlu=%s
//                                    WHERE larticle_id=%d AND larticle_type=%s", $key + $start, $_COOKIE['user_id'], date( "Y-m-d H:i:s" ), $val, $app_name );
//       $db->do_query( $sql );
//
//
//   }
// }

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data icon language di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_icon_language()
{
	$fix_file_name = '';

	if(isset($_FILES['icon_language']))
	{
		$file_name   = $_FILES['icon_language']['name'];
		$file_size   = $_FILES['icon_language']['size'];
		$file_type   = $_FILES['icon_language']['type'];
		$file_source = $_FILES['icon_language']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/language/background/'))
				{
					mkdir(PLUGINS_PATH.'/language/background/');
				}

				$destination = PLUGINS_PATH.'/language/background/'.$fix_file_name;

				if(upload($file_source, $destination))
				{
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'icon_language', 'language');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/language/background/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/language/background/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'icon_language', $fix_file_name, 'language');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus icon language di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_icon_language($post_id='')
{
	if(empty($post_id)) return true;

	$cp_image = get_additional_field($post_id,'icon_language','language');

	if(empty($cp_image)) return true;

	if(file_exists(PLUGINS_PATH.'/language/background/'.$cp_image))
	{
		unlink(PLUGINS_PATH.'/language/background/'.$cp_image);
	}

	if(edit_additional_field($post_id, 'icon_language', '', 'language'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengupdate data temporary images
| -------------------------------------------------------------------------------------------------------------------------
*/
function sync_attachment_images_language($old_id, $new_id)
{
	global $db;

	$s = 'UPDATE lumonata_additional_fields SET lapp_id=%d WHERE lapp_id=%d';
	$q = $db->prepare_query($s, $new_id, $old_id);

	return $db->do_query($q);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengupdate status language di database -------------------------------------------------------------------------------------------------------------------------
*/
function change_status_lang($id, $status)
{
	global $db;

	$q = $db->prepare_query("UPDATE lumonata_articles SET larticle_status=%s WHERE larticle_id=%d", $status, $id);
	if($db->do_query($q))
	{
		return true;
	}
	else
	{
		return false;
	}
}

?>
