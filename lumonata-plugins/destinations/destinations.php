<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
    Plugin Name: Lumonata Destination Application
    Plugin URL: http://lumonata.com/
    Description: This plugin is use for adding destination post
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

add_privileges('administrator', 'destinations', 'insert');
add_privileges('administrator', 'destinations', 'update');
add_privileges('administrator', 'destinations', 'delete');

//-- Add custom icon menu
add_actions('header_elements','destinations_header_element');
function destinations_header_element()
{
    return '<link href="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/style.css?v=1.0.1" rel="stylesheet">';
}

//-- Add sub menu under applications menu
add_main_menu(array('destinations'=>'Destinations'));
add_actions('destinations','set_destinations_data');


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini otomatis akan dijalankan untuk melakukan filter action
	yang akan dilakukan -- (get, add, edit, delete, delete all)
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_destinations_data()
{
	if(is_add_new())
	{
		return add_destinations();
	}
	elseif(is_edit())
	{
		return edit_destinations();
	}
	elseif(is_delete_all())
	{
		return delete_all_destinations();
	}
	elseif(is_confirm_delete())
	{
		foreach($_POST['id'] as $key=>$val)
		{
			delete_destinations($val);
		}
	}
	elseif(is_ajax_request())
	{
		return execute_ajax_request();
	}

	if(is_num_destinations() > 0)
	{
		return get_destinations_list();
	}
	else
	{
		header('location:'.get_state_url('destinations').'&prc=add_new');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengambil jumlah data destinaton
| -------------------------------------------------------------------------------------------------------------------------
*/
function is_num_destinations()
{
	global $db;

	$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
	$q = $db->prepare_query($s,'destinations');
	$r = $db->do_query($q);

	return $db->num_rows($r);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengambil data destinaton dari database
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_destinations_list()
{
	global $db;

	$list   = '';
	$search = (isset($_POST['s']) ? $_POST['s'] : '');
	$page   = (isset($_GET['page']) ? $_GET['page'] : 1);
	$url    = get_state_url('destinations')."&page=";
	$viewed = list_viewed();
	$limit  = ($page-1) * $viewed;
	$start  = ($page - 1) * $viewed + 1;

	if(is_search())
	{
		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
		$q = $db->prepare_query($s,'destinations',"%".$_POST['s']."%");
		$r = $db->do_query($q);
		$n = $db->num_rows($r);

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s LIMIT %d, %d';
		$q = $db->prepare_query($s,'destinations','%'.$_POST['s'].'%',$limit,$viewed);
	}
	else
	{
		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
		$q = $db->prepare_query($s,'destinations');
		$r = $db->do_query($q);
		$n = $db->num_rows($r);

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s LIMIT %d, %d';
		$q = $db->prepare_query($s,'destinations',$limit,$viewed);
	}

	$button = '
	<li>'.button('button=add_new',get_state_url('destinations').'&prc=add_new').'</li>
	<li>'.button('button=delete&type=submit&enable=false').'</li>';

	$button = '';

	set_template(PLUGINS_PATH."/destinations/list.html",'destinations_list_template');

	$r = $db->do_query($q);
	if($db->num_rows($r) > 0)
	{
		add_block('destinations_list','dest_list','destinations_list_template');

		while($d=$db->fetch_array($r))
		{
			add_variable('id',$d['larticle_id']);
			add_variable('title',$d['larticle_title']);
			add_variable('description',limit_words(strip_tags($d['larticle_content']), 50));

			parse_template('destinations_list','dest_list',true);
		}
	}

	add_block('destinations','dest','destinations_list_template');
	add_actions('section_title','Destinations - List');

	add_variable('button',$button);
	add_variable('search_val',$search);
	add_variable('start_order',$start);
	add_variable('state','destinations');
	add_variable('state_url',get_state_url('destinations'));
	add_variable( 'HTTP', HTTP );
	add_variable('template_url',TEMPLATE_URL);

	parse_template('destinations','dest',false);
	return return_template('destinations_list_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk meyimpan data destinaton ke database
| -------------------------------------------------------------------------------------------------------------------------
*/
function add_destinations()
{
	$message  = '';
	$mess_arr = array();

	$post_id            = (isset($_POST['post_id']) ? $_POST['post_id']: time());
	$post_title         = (isset($_POST['post_title']) ? $_POST['post_title']: '');
	$post_content       = (isset($_POST['post_content']) ? rem_slashes($_POST['post_content']): '');
	$brief_landing_page = (isset($_POST['brief_landing_page']) ? rem_slashes($_POST['brief_landing_page']) : '');


	$title_home_page         	= (isset($_POST['title_home_page']) ? $_POST['title_home_page']: '');
	$subtitle_home_page         = (isset($_POST['subtitle_home_page']) ? $_POST['subtitle_home_page']: '');

  $title_landing_page         	= (isset($_POST['title_landing_page']) ? $_POST['title_landing_page']: '');
  $subtitle_landing_page         = (isset($_POST['subtitle_landing_page']) ? $_POST['subtitle_landing_page']: '');

	$title_reception_venue_1         = (isset($_POST['title_reception_venue_1']) ? $_POST['title_reception_venue_1']: '');
	$title_reception_venue_2         = (isset($_POST['title_reception_venue_2']) ? $_POST['title_reception_venue_2']: '');

	$phone_number               = (isset($_POST['phone_number']) ? $_POST['phone_number'] : '');
	$fax_number                 = (isset($_POST['fax_number']) ? $_POST['fax_number'] : '');
	$reservation_phone_number   = (isset($_POST['reservation_phone_number']) ? $_POST['reservation_phone_number'] : '');
	$reservation_fax_number     = (isset($_POST['reservation_fax_number']) ? $_POST['reservation_fax_number'] : '');
	$fb_link                    = (isset($_POST['fb_link']) ? $_POST['fb_link'] : '');
	$youtube_link               = (isset($_POST['youtube_link']) ? $_POST['youtube_link'] : '');
	$instagram_link             = (isset($_POST['instagram_link']) ? $_POST['instagram_link'] : '');
	$twitter_link           	= (isset($_POST['twitter_link']) ? $_POST['twitter_link'] : '');
	$email_address              = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
	$whatsapp_number            = (isset($_POST['whatsapp_number']) ? $_POST['whatsapp_number'] : '');
	$whatsapp_status            = (isset($_POST['whatsapp_status']) ? $_POST['whatsapp_status'] : '');
	$reservation_email_address  = (isset($_POST['reservation_email_address']) ? $_POST['reservation_email_address'] : '');

	$spa_reservation_email_address  = (isset($_POST['spa_reservation_email_address']) ? $_POST['spa_reservation_email_address'] : '');
	$tax_default_spa  				= (isset($_POST['tax_default_spa']) ? $_POST['tax_default_spa'] : '');

	$events_inquiry_email_address     = (isset($_POST['events_inquiry_email_address']) ? $_POST['events_inquiry_email_address']: '');
	$dining_reservation_email_address = (isset($_POST['dining_reservation_email_address']) ? $_POST['dining_reservation_email_address'] : '');

	$instagram_user             = (isset($_POST['instagram_user']) ? $_POST['instagram_user'] : '');
	$coordinate_location        = (isset($_POST['coordinate_location']) ? $_POST['coordinate_location'] : '');
	$link_direction        		= (isset($_POST['link_direction']) ? $_POST['link_direction'] : '');
	$address                    = (isset($_POST['address']) ? $_POST['address'] : '');

	$widget_traveller_choice			= (isset($_POST['widget_traveller_choice']) ? $_POST['widget_traveller_choice'] : '');
	$widget_certificate_excellenge		= (isset($_POST['widget_certificate_excellenge']) ? $_POST['widget_certificate_excellenge'] : '');

	$meta_title          		= (isset($_POST['meta_title']) ? $_POST['meta_title'] : '');
	$meta_keywords          	= (isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '');
	$meta_description          	= (isset($_POST['meta_description']) ? $_POST['meta_description'] : '');

	$mailchimp_api_key          = (isset($_POST['mailchimp_api_key']) ? $_POST['mailchimp_api_key'] : '');
	$mailchimp_list_id     		= (isset($_POST['mailchimp_list_id']) ? $_POST['mailchimp_list_id'] : '');
	$fb_properties_name     	= (isset($_POST['fb_properties_name']) ? $_POST['fb_properties_name'] : '');
	$fb_hotels_name     		= (isset($_POST['fb_hotels_name']) ? $_POST['fb_hotels_name'] : '');

	$status_popup_newsletter 	= (isset($_POST['status_popup_newsletter']) ? $_POST['status_popup_newsletter'] : '');
	$url_popup_newsletter 		= (isset($_POST['url_popup_newsletter']) ? $_POST['url_popup_newsletter'] : '');

	$title_policy     			= (isset($_POST['title_policy']) ? $_POST['title_policy'] : '');
	$sub_title_policy     		= (isset($_POST['sub_title_policy']) ? $_POST['sub_title_policy'] : '');
	$description_policy     	= (isset($_POST['description_policy']) ? $_POST['description_policy'] : '');
	$meta_title_policy     		= (isset($_POST['meta_title_policy']) ? $_POST['meta_title_policy'] : '');
	$meta_keywords_policy     	= (isset($_POST['meta_keywords_policy']) ? $_POST['meta_keywords_policy'] : '');
	$meta_description_policy    = (isset($_POST['meta_description_policy']) ? $_POST['meta_description_policy'] : '');

	$ig_access_token     		= (isset($_POST['ig_access_token']) ? $_POST['ig_access_token'] : '');
	$fb_access_token     		= (isset($_POST['fb_access_token']) ? $_POST['fb_access_token'] : '');
	$fb_page_id     			= (isset($_POST['fb_page_id']) ? $_POST['fb_page_id'] : '');
	$whatsapp_content 			= '';

	$whatsapp_text 				= array( 'Off', 'On' );
	for ( $i = 0; $i < 2 ; $i++ ) {
		if ( $i == $whatsapp_status ) {
			$whatsapp_content	.= '<option value="'.$i.'" selected>'.$whatsapp_text[$i].'</option>';
		}
		else {
			$whatsapp_content	.= '<option value="'.$i.'">'.$whatsapp_text[$i].'</option>';
		}
	}

	if(is_save_changes())
	{
		if(empty($post_title))
		{
			$mess_arr[] = '- Please input your destination title';

		}

		if(empty($post_content))
		{
			$mess_arr[] = '- Please input your destination description';
		}

		if(empty($mess_arr))
		{
			$is_save_success = save_article($post_title, '', '',  $post_content, 'publish', 'destinations','', '', '', '', '', '', '', '', 'not-allowed', '', '', $brief_landing_page);

			if($is_save_success)
			{
                $post_id = mysql_insert_id();

				set_informations($post_id);
				sync_attachment_images($_POST['post_id'], $post_id);

				$post_id                          = time();
				$post_title                       = '';
				$post_content                     = '';
				$brief_landing_page               = '';
				$title_reception_venue_1          = '';
				$title_reception_venue_2          = '';
				$phone_number                     = '';
				$fax_number                       = '';
				$reservation_phone_number         = '';
				$reservation_fax_number           = '';
				$fb_link                          = '';
				$youtube_link                     = '';
				$instagram_link                   = '';
				$twitter_link          	          = '';
				$email_address                    = '';
				$whatsapp_number                  = '';
				$whatsapp_status                  = 0;
				$reservation_email_address        = '';
				$spa_reservation_email_address    = '';
				$events_inquiry_email_address     = '';
				$dining_reservation_email_address = '';
				$tax_default_spa                  = '';
				$instagram_user                   = '';
				$coordinate_location              = '';
				$link_direction                   = '';
				$widget_traveller_choice          = '';
				$widget_certificate_excellenge    = '';
				$meta_title                       = '';
				$meta_keywords                    = '';
				$meta_description                 = '';
				$mailchimp_api_key                = '';
				$mailchimp_list_id                = '';
				$fb_properties_name               = '';
				$fb_hotels_name                   = '';
				$status_popup_newsletter          = '';
				$url_popup_newsletter             = '';
				$title_policy                     = '';
				$sub_title_policy                 = '';
				$description_policy               = '';
				$meta_title_policy                = '';
				$meta_keywords_policy             = '';
				$meta_description_policy          = '';
				$ig_access_token                  = '';
				$fb_access_token                  = '';
				$fb_page_id                       = '';
				$address                          = '';

				$message = '<div class="message success">New destination has save succesfully.</div>';
			}
			else
			{
				$message = '<div class="message error">Something wrong, please try again.</div>';
			}
		}
		else
		{
			$message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
		}
	}

	$button	= '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=add_new',get_state_url('destinations').'&prc=add_new').'</li>
	<li>'.button('button=cancel',get_state_url('destinations')).'</li>';

	$button	= '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=cancel',get_state_url('destinations')).'</li>';

	set_template(PLUGINS_PATH.'/destinations/form.html','destinations_form_template');
	add_block('destinations_form','dest_form','destinations_form_template');
	add_actions('section_title','Add New Destination');

	add_variable( 'HTTP', HTTP );
	add_variable('form_title','Add New Destination');
	add_variable('post_title',$post_title);
	add_variable('post_content',new_textarea('post_content',0,$post_content,0,true));
	add_variable('brief_landing_page',new_textarea('brief_landing_page',0,$brief_landing_page,0,true));
	add_variable('background_image',set_background($post_id));
	add_variable('background_image_homepage',set_background_homepage($post_id));
	add_variable('bg_events',set_background_events($post_id));
	add_variable('corporate_image',set_corporate($post_id));
	add_variable('footer_image',set_footer($post_id));
	add_variable('bg_image_policy',set_bg_image_policy_image($post_id));
	add_variable('popup_image_newsletter',set_popup_image_newsletter_image($post_id));
	add_variable('dinner_reception_venue_1',set_dinner_reception_venue_1_image($post_id));
	add_variable('dinner_reception_venue_2',set_dinner_reception_venue_2_image($post_id));

	add_variable('title_home_page',$title_home_page);
	add_variable('subtitle_home_page',$subtitle_home_page);

  add_variable('title_landing_page',$title_landing_page);
	add_variable('subtitle_landing_page',$subtitle_landing_page);

	add_variable('title_reception_venue_1',$title_reception_venue_1);
	add_variable('title_reception_venue_2',$title_reception_venue_2);
	add_variable('phone_number',$phone_number);
	add_variable('fax_number',$fax_number);
	add_variable('reservation_phone_number',$reservation_phone_number);
	add_variable('reservation_fax_number',$reservation_fax_number);
	add_variable('fb_link',$fb_link);
	add_variable('youtube_link',$youtube_link);
	add_variable('instagram_link',$instagram_link);
	add_variable('twitter_link',$twitter_link);
	add_variable('email_address',$email_address);
	add_variable('whatsapp_number',$whatsapp_number);
	add_variable('whatsapp_status',$whatsapp_status);
	add_variable('whatsapp_content',$whatsapp_content);
	add_variable('reservation_email_address',$reservation_email_address);
	add_variable('spa_reservation_email_address',$spa_reservation_email_address);
	add_variable('tax_default_spa',$tax_default_spa);
	add_variable('events_inquiry_email_address',$events_inquiry_email_address);
	add_variable('dining_reservation_email_address',$dining_reservation_email_address);
	add_variable('instagram_user',$instagram_user);
	add_variable('coordinate_location',$coordinate_location);
	add_variable('link_direction',$link_direction);
	add_variable('widget_traveller_choice',$widget_traveller_choice);
	add_variable('widget_certificate_excellenge',$widget_certificate_excellenge);
	add_variable('mailchimp_list_id',$mailchimp_list_id);
	add_variable('mailchimp_apimeta_title_key',$meta_title);
	add_variable('meta_keywords',$meta_keywords);
	add_variable('meta_description',$meta_description);
	add_variable('mailchimp_api_key',$mailchimp_api_key);
	add_variable('fb_properties_name',$fb_properties_name);
	add_variable('fb_hotels_name',$fb_hotels_name);

	if($status_popup_newsletter == 1)
	{
		$status_popup_newsletter_html = '
			<input type="radio" class="radio" name="status_popup_newsletter" checked value="1" />On
			<input type="radio" class="radio" name="status_popup_newsletter" value="0" />Off
		';
	}
	else
	{
		$status_popup_newsletter_html = '
			<input type="radio" class="radio" name="status_popup_newsletter" value="1" />On
			<input type="radio" class="radio" name="status_popup_newsletter" value="0" checked />Off
		';
	}

	add_variable('status_popup_newsletter_html',$status_popup_newsletter_html);
	add_variable('url_popup_newsletter',$url_popup_newsletter);

	add_variable('title_policy',$title_policy);
	add_variable('sub_title_policy',$sub_title_policy);
	add_variable('description_policy',$description_policy);
	add_variable('meta_title_policy',$meta_title_policy);
	add_variable('meta_keywords_policy',$meta_keywords_policy);
	add_variable('meta_description_policy',$meta_description_policy);
	add_variable('ig_access_token',$ig_access_token);
	add_variable('fb_access_token',$fb_access_token);
	add_variable('fb_page_id',$fb_page_id);
	add_variable('address',$address);

	add_variable('site_url',SITE_URL);
	add_variable('plugin_url',SITE_URL.'/lumonata-plugins/destinations');
	add_variable('state_url',get_state_url('destinations'));
	add_variable('post_id',$post_id);
	add_variable('message',$message);
	add_variable('button',$button);

	add_variable( 'language_button', attemp_actions( 'page_post_language_button' ) );

	parse_template('destinations_form','dest_form',false);
	return return_template('destinations_form_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengubah data destinaton di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function edit_destinations()
{
	global $db;

	$message  = '';
	$mess_arr = array();
	$whatsapp_content = '';

	$post_title   = (isset($_POST['post_title']) ? $_POST['post_title'] : '');
	$post_content = (isset($_POST['post_content']) ? rem_slashes($_POST['post_content']) : '');
	$brief_landing_page = (isset($_POST['brief_landing_page']) ? rem_slashes($_POST['brief_landing_page']) : '');

	if(is_save_changes())
	{
		if(empty($post_title))
		{
			$mess_arr[] = '- Please input your destination title';
		}

		if(empty($post_content))
		{
			$mess_arr[] = '- Please input your destination description';
		}

		if(empty($mess_arr))
		{
			$is_edit_success = update_article($_GET['id'], $post_title, '', '', $post_content, 'publish', 'destinations','', '', '', '', '', '', '', '', 'not-allowed', 0, $brief_landing_page);

			if($is_edit_success)
			{
				set_informations($_GET['id']);

				$message = '<div class="message success">Destination data has been successfully edited.</div>';
			}
			else
			{
				$message = '<div class="message error">Something wrong, please try again.</div>';
			}
		}
		else
		{
			$message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
		}
	}

	$button	= '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=add_new',get_state_url('destinations').'&prc=add_new').'</li>
	<li>'.button('button=cancel',get_state_url('destinations')).'</li>';

	$button	= '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=cancel',get_state_url('destinations')).'</li>';

	set_template(PLUGINS_PATH.'/destinations/form.html','destinations_form_template');
	add_block('destinations_form','dest_form','destinations_form_template');
	add_actions('section_title','Edit Destination');

	$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
	$q = $db->prepare_query($s,'destinations',$_GET['id']);
	$r = $db->do_query($q);
	if($db->num_rows($r) > 0)
	{
		$d = $db->fetch_array($r);

		add_variable('form_title','Edit Destination');
		add_variable('post_title',$d['larticle_title']);
		add_variable('post_content',new_textarea('post_content',0,rem_slashes($d['larticle_content']),$_GET['id'],true));
		add_variable('brief_landing_page',new_textarea('brief_landing_page',0,rem_slashes($d['larticle_brief']),$_GET['id'],true));
		add_variable('background_image',set_background($_GET['id']));
		add_variable('background_image_homepage',set_background_homepage($_GET['id']));
		add_variable('bg_events',set_background_events($_GET['id']));
		add_variable('corporate_image',set_corporate($_GET['id']));
		add_variable('footer_image',set_footer($_GET['id']));
		add_variable('dinner_reception_venue_1',set_dinner_reception_venue_1_image($_GET['id']));
		add_variable('dinner_reception_venue_2',set_dinner_reception_venue_2_image($_GET['id']));
		add_variable('bg_image_policy',set_bg_image_policy_image($_GET['id']));
		add_variable('popup_image_newsletter',set_popup_image_newsletter_image($_GET['id']));


		add_variable('title_home_page',get_additional_field($_GET['id'],'title_home_page','destinations'));
		add_variable('subtitle_home_page',get_additional_field($_GET['id'],'subtitle_home_page','destinations'));

    add_variable('title_landing_page',get_additional_field($_GET['id'],'title_landing_page','destinations'));
		add_variable('subtitle_landing_page',get_additional_field($_GET['id'],'subtitle_landing_page','destinations'));

		add_variable('title_reception_venue_1',get_additional_field($_GET['id'],'title_reception_venue_1','destinations'));
		add_variable('title_reception_venue_2',get_additional_field($_GET['id'],'title_reception_venue_2','destinations'));
		add_variable('phone_number',get_additional_field($_GET['id'],'phone_number','destinations'));
		add_variable('fax_number',get_additional_field($_GET['id'],'fax_number','destinations'));
		add_variable('reservation_phone_number',get_additional_field($_GET['id'],'reservation_phone_number','destinations'));
		add_variable('reservation_fax_number',get_additional_field($_GET['id'],'reservation_fax_number','destinations'));
		add_variable('fb_link',get_additional_field($_GET['id'],'fb_link','destinations'));
		add_variable('youtube_link',get_additional_field($_GET['id'],'youtube_link','destinations'));
		add_variable('instagram_link',get_additional_field($_GET['id'],'instagram_link','destinations'));
		add_variable('twitter_link',get_additional_field($_GET['id'],'twitter_link','destinations'));
		add_variable('email_address',get_additional_field($_GET['id'],'email_address','destinations'));
		add_variable('whatsapp_number',get_additional_field($_GET['id'],'whatsapp_number','destinations'));
		add_variable('whatsapp_status',get_additional_field($_GET['id'],'whatsapp_status','destinations'));
		add_variable('reservation_email_address',get_additional_field($_GET['id'],'reservation_email_address','destinations'));
		add_variable('spa_reservation_email_address',get_additional_field($_GET['id'],'spa_reservation_email_address','destinations'));
		add_variable('tax_default_spa',get_additional_field($_GET['id'],'tax_default_spa','destinations'));
		add_variable('events_inquiry_email_address',get_additional_field($_GET['id'],'events_inquiry_email_address','destinations'));
		add_variable('dining_reservation_email_address',get_additional_field($_GET['id'],'dining_reservation_email_address','destinations'));
		add_variable('instagram_user',get_additional_field($_GET['id'],'instagram_user','destinations'));
		add_variable('coordinate_location',get_additional_field($_GET['id'],'coordinate_location','destinations'));
		add_variable('link_direction',get_additional_field($_GET['id'],'link_direction','destinations'));
		add_variable('widget_traveller_choice',get_additional_field($_GET['id'],'widget_traveller_choice','destinations'));
		add_variable('widget_certificate_excellenge',get_additional_field($_GET['id'],'widget_certificate_excellenge','destinations'));
		add_variable('meta_title',get_additional_field($_GET['id'],'meta_title','destinations'));
		add_variable('meta_keywords',get_additional_field($_GET['id'],'meta_keywords','destinations'));
		add_variable('meta_description',get_additional_field($_GET['id'],'meta_description','destinations'));
		add_variable('mailchimp_api_key',get_additional_field($_GET['id'],'mailchimp_api_key','destinations'));
		add_variable('mailchimp_list_id',get_additional_field($_GET['id'],'mailchimp_list_id','destinations'));
		add_variable('fb_properties_name',get_additional_field($_GET['id'],'fb_properties_name','destinations'));
		add_variable('fb_hotels_name',get_additional_field($_GET['id'],'fb_hotels_name','destinations'));

		$status_popup_newsletter = get_additional_field($_GET['id'],'status_popup_newsletter','destinations');
		if($status_popup_newsletter == 1)
		{
			$status_popup_newsletter_html = '
				<input type="radio" class="radio" name="status_popup_newsletter" checked value="1" />On
				<input type="radio" class="radio" name="status_popup_newsletter" value="0" />Off
			';
		}
		else
		{
			$status_popup_newsletter_html = '
				<input type="radio" class="radio" name="status_popup_newsletter" value="1" />On
				<input type="radio" class="radio" name="status_popup_newsletter" value="0" checked />Off
			';
		}

		add_variable('status_popup_newsletter_html', $status_popup_newsletter_html);
		add_variable('url_popup_newsletter',get_additional_field($_GET['id'],'url_popup_newsletter','destinations'));
		add_variable('title_policy',get_additional_field($_GET['id'],'title_policy','destinations'));
		add_variable('sub_title_policy',get_additional_field($_GET['id'],'sub_title_policy','destinations'));
		add_variable('description_policy',get_additional_field($_GET['id'],'description_policy','destinations'));
		add_variable('meta_title_policy',get_additional_field($_GET['id'],'meta_title_policy','destinations'));
		add_variable('meta_keywords_policy',get_additional_field($_GET['id'],'meta_keywords_policy','destinations'));
		add_variable('meta_description_policy',get_additional_field($_GET['id'],'meta_description_policy','destinations'));
		add_variable('ig_access_token',get_additional_field($_GET['id'],'ig_access_token','destinations'));
		add_variable('fb_access_token',get_additional_field($_GET['id'],'fb_access_token','destinations'));
		add_variable('fb_page_id',get_additional_field($_GET['id'],'fb_page_id','destinations'));
		add_variable('address',get_additional_field($_GET['id'],'address','destinations'));

		$whatsapp_status 			= get_additional_field($_GET['id'],'whatsapp_status','destinations');
		$whatsapp_text 				= array( 'Off', 'On' );
		for ( $i = 0; $i < 2 ; $i++ ) {
			if ( $i == $whatsapp_status ) {
				$whatsapp_content	.= '<option value="'.$i.'" selected>'.$whatsapp_text[$i].'</option>';
			}
			else {
				$whatsapp_content	.= '<option value="'.$i.'">'.$whatsapp_text[$i].'</option>';
			}
		}
		add_variable('whatsapp_content',$whatsapp_content);

		add_variable('HTTP',HTTP);
		add_variable('site_url',SITE_URL);
		add_variable('plugin_url',SITE_URL.'/lumonata-plugins/destinations');
		add_variable('state_url',get_state_url('destinations'));
		add_variable('post_id',$_GET['id']);
		add_variable('message',$message);
		add_variable('button',$button);

		add_variable( 'language_button', attemp_actions( 'page_post_language_button' ) );
		add_variable( 'language_data', attemp_actions( 'destination_language_data' ) );

		parse_template('destinations_form','dest_form',false);
		return return_template('destinations_form_template');
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus data destinaton di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_destinations($id='')
{
	global $db;

	$s = 'DELETE FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
	$q = $db->prepare_query($s,'destinations',$id);

	if($db->do_query($q))
	{
		delete_background_image($id);
		delete_corporate_image($id);
		delete_additional_field($id, 'destinations');

		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus multiple data destinaton di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_all_destinations()
{
	global $db;

	add_actions('section_title','Delete Destination');

	$warning = '
	<h1 class="form-title">Delete Destination</h1>
	<div class="tab_container">
		<div class="single_content destination-wrapp">';
			$warning .= '
			<form action="" method="post">';
				$warning .= '
				<div class="alert_red_form boxs">
					<strong>
						Are you sure want to delete this
						destination'.(count($_POST['select']) > 1 ? 's' : '').' :
					</strong>
					<ol>';
						foreach($_POST['select'] as $key=>$val)
						{
							$s = 'SELECT larticle_title FROM lumonata_articles WHERE larticle_id=%d';
							$q = $db->prepare_query($s,$val);
							$r = $db->do_query($q);
							$d = $db->fetch_array($r);

							$warning .= '
							<li>
								'.$d['larticle_title'].'
								<input type="hidden" name="id[]" value="'.$val.'" />
							</li>';
						}
						$warning .= '
					</ol>
				</div>
				<div class="alert-button-box">
					<input type="submit" name="confirm_delete" value="Yes" class="button" />
					<input type="button" name="confirm_delete" value="No" class="button" onclick="location=\''.get_state_url('destinations').'\'" />
				</div>
			</form>
		</div>
	</div>';

	return $warning;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk mengupdate data temporary images
| -------------------------------------------------------------------------------------------------------------------------
*/
function sync_attachment_images($old_id, $new_id)
{
	global $db;

	$s = 'UPDATE lumonata_additional_fields SET lapp_id=%d WHERE lapp_id=%d';
	$q = $db->prepare_query($s, $new_id, $old_id);

	return $db->do_query($q);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton background image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background_images()
{
	$fix_file_name = '';

	if(isset($_FILES['bg_image']))
	{
		$file_name   = $_FILES['bg_image']['name'];
		$file_size   = $_FILES['bg_image']['size'];
		$file_type   = $_FILES['bg_image']['type'];
		$file_source = $_FILES['bg_image']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/background/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/background/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/background/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/background/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/background/large/' . $fix_file_name;
                $destination_medium = PLUGINS_PATH . '/destinations/background/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'background_image', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/small/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'background_image', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton background image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background_images_homepage()
{
	$fix_file_name = '';

	if(isset($_FILES['background_image_homepage']))
	{
		$file_name   = $_FILES['background_image_homepage']['name'];
		$file_size   = $_FILES['background_image_homepage']['size'];
		$file_type   = $_FILES['background_image_homepage']['type'];
		$file_source = $_FILES['background_image_homepage']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/background/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/background/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/background/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/background/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/background/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/background/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'background_image_homepage', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/small/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'background_image_homepage', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton background image events
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background_images_events()
{
	$fix_file_name = '';

	if(isset($_FILES['bg_events']))
	{
		$file_name   = $_FILES['bg_events']['name'];
		$file_size   = $_FILES['bg_events']['size'];
		$file_type   = $_FILES['bg_events']['type'];
		$file_source = $_FILES['bg_events']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/background/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/background/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/background/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/background/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/background/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/background/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'bg_image_events', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/small/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'bg_image_events', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton footer image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_footer_images()
{
	$fix_file_name = '';

	if(isset($_FILES['footer_image']))
	{
		$file_name   = $_FILES['footer_image']['name'];
		$file_size   = $_FILES['footer_image']['size'];
		$file_type   = $_FILES['footer_image']['type'];
		$file_source = $_FILES['footer_image']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/footer/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/footer/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/footer/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/footer/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/footer/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/footer/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/footer/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/footer/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/footer/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/footer/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/footer/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/footer/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'footer_image', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/footer/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/footer/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/footer/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/footer/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/footer/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/footer/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/footer/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/footer/small/'.$old_bg);
					}
					edit_additional_field($_POST['post_id'], 'footer_image', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data background image policy
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_popup_image_newsletter()
{
	$fix_file_name = '';

	if(isset($_FILES['popup_image_newsletter']))
	{
		$file_name   = $_FILES['popup_image_newsletter']['name'];
		$file_size   = $_FILES['popup_image_newsletter']['size'];
		$file_type   = $_FILES['popup_image_newsletter']['type'];
		$file_source = $_FILES['popup_image_newsletter']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/popup_image_newsletter/');
				}

				// if( !file_exists( PLUGINS_PATH . '/destinations/popup_image_newsletter/large/' ) )
				// {
				// 	mkdir( PLUGINS_PATH . '/destinations/popup_image_newsletter/large/' );
				// }

				// if( !file_exists( PLUGINS_PATH . '/destinations/popup_image_newsletter/medium/' ) )
				// {
				// 	mkdir( PLUGINS_PATH . '/destinations/popup_image_newsletter/medium/' );
				// }

				// if( !file_exists( PLUGINS_PATH . '/destinations/popup_image_newsletter/small/' ) )
				// {
				// 	mkdir( PLUGINS_PATH . '/destinations/popup_image_newsletter/small/' );
				// }

				$destination = PLUGINS_PATH.'/destinations/popup_image_newsletter/'.$fix_file_name;
				// $destination_small = PLUGINS_PATH . '/destinations/popup_image_newsletter/small/' . $fix_file_name;
                // $destination_large = PLUGINS_PATH . '/destinations/popup_image_newsletter/large/' . $fix_file_name;
				// $destination_medium = PLUGINS_PATH . '/destinations/popup_image_newsletter/medium/' . $fix_file_name;

				// $is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                // $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                // $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				// if( $is_upload && $is_resize )
				if( $is_upload )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'popup_image_newsletter', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/'.$old_bg);
					}

					// if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/large/'.$old_bg))
					// {
					// 	unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/large/'.$old_bg);
					// }

					// if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/medium/'.$old_bg))
					// {
					// 	unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/medium/'.$old_bg);
					// }

					// if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/small/'.$old_bg))
					// {
					// 	unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/small/'.$old_bg);
					// }

					edit_additional_field($_POST['post_id'], 'popup_image_newsletter', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data background image policy
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_bg_image_policy()
{
	$fix_file_name = '';

	if(isset($_FILES['bg_image_policy']))
	{
		$file_name   = $_FILES['bg_image_policy']['name'];
		$file_size   = $_FILES['bg_image_policy']['size'];
		$file_type   = $_FILES['bg_image_policy']['type'];
		$file_source = $_FILES['bg_image_policy']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/bg_image_policy/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/bg_image_policy/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/bg_image_policy/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/bg_image_policy/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/bg_image_policy/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/bg_image_policy/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/bg_image_policy/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/bg_image_policy/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/bg_image_policy/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/bg_image_policy/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/bg_image_policy/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'bg_image_policy', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/bg_image_policy/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/bg_image_policy/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/bg_image_policy/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/bg_image_policy/small/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'bg_image_policy', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton reception venue 1
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_dinner_reception_venue_1()
{
	$fix_file_name = '';

	if(isset($_FILES['dinner_reception_venue_1']))
	{
		$file_name   = $_FILES['dinner_reception_venue_1']['name'];
		$file_size   = $_FILES['dinner_reception_venue_1']['size'];
		$file_type   = $_FILES['dinner_reception_venue_1']['type'];
		$file_source = $_FILES['dinner_reception_venue_1']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/reception-venue/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/reception-venue/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/reception-venue/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/reception-venue/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/reception-venue/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/reception-venue/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/reception-venue/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/reception-venue/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/reception-venue/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/reception-venue/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/reception-venue/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/reception-venue/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'dinner_reception_venue_1', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/small/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'dinner_reception_venue_1', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton reception venue 2
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_dinner_reception_venue_2()
{
	$fix_file_name = '';

	if(isset($_FILES['dinner_reception_venue_2']))
	{
		$file_name   = $_FILES['dinner_reception_venue_2']['name'];
		$file_size   = $_FILES['dinner_reception_venue_2']['size'];
		$file_type   = $_FILES['dinner_reception_venue_2']['type'];
		$file_source = $_FILES['dinner_reception_venue_2']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/reception-venue/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/reception-venue/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/reception-venue/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/reception-venue/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/reception-venue/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/reception-venue/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/reception-venue/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/reception-venue/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/reception-venue/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/reception-venue/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/reception-venue/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/reception-venue/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'dinner_reception_venue_2', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/reception-venue/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/reception-venue/small/'.$old_bg);
					}


					edit_additional_field($_POST['post_id'], 'dinner_reception_venue_2', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}



/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton corporate image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_corporate_images()
{
	$fix_file_name = '';

	if(isset($_FILES['cp_image']))
	{
		$file_name   = $_FILES['cp_image']['name'];
		$file_size   = $_FILES['cp_image']['size'];
		$file_type   = $_FILES['cp_image']['type'];
		$file_source = $_FILES['cp_image']['tmp_name'];

		if(is_allow_file_size($file_size))
		{
			if(is_allow_file_type($file_type,'image'))
			{
				$fix_file_name = file_name_filter(character_filter($file_name)).'-'.time();
				$file_ext      = file_name_filter($file_name,true);
				$fix_file_name = $fix_file_name.$file_ext;

				//-- CHECK FOLDER EXIST
				if(!file_exists(PLUGINS_PATH.'/destinations/background/'))
				{
					mkdir(PLUGINS_PATH.'/destinations/background/');
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/large/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/large/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/medium/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/medium/' );
				}

				if( !file_exists( PLUGINS_PATH . '/destinations/background/small/' ) )
				{
					mkdir( PLUGINS_PATH . '/destinations/background/small/' );
				}

				$destination = PLUGINS_PATH.'/destinations/background/'.$fix_file_name;
				$destination_small = PLUGINS_PATH . '/destinations/background/small/' . $fix_file_name;
                $destination_large = PLUGINS_PATH . '/destinations/background/large/' . $fix_file_name;
				$destination_medium = PLUGINS_PATH . '/destinations/background/medium/' . $fix_file_name;

				$is_resize = upload_resize( $file_source, $destination_large, $file_type, 1920, 1920 );
                $is_resize = upload_resize( $file_source, $destination_medium, $file_type, 640, 640 );
                $is_resize = upload_resize( $file_source, $destination_small, $file_type, 450, 450 );
				$is_upload = upload( $file_source, $destination );

				if( $is_upload && $is_resize )
                {
					//-- Delete old image
					$old_bg = get_additional_field($_POST['post_id'], 'corporate_image', 'destinations');

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/large/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/large/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/medium/'.$old_bg);
					}

					if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/destinations/background/small/'.$old_bg))
					{
						unlink(PLUGINS_PATH.'/destinations/background/small/'.$old_bg);
					}

					edit_additional_field($_POST['post_id'], 'corporate_image', $fix_file_name, 'destinations');
				}
			}
		}
	}

	return array('post_id'=>$_POST['post_id'],'filename'=>$fix_file_name);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton information di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_informations($post_id)
{
	//-- Title Home Page
	$title_home_page = (isset($_POST['title_home_page']) ? $_POST['title_home_page'] : '');
	edit_additional_field($post_id, 'title_home_page', $title_home_page, 'destinations');

	// Sub Title Home Page
	$subtitle_home_page = (isset($_POST['subtitle_home_page']) ? $_POST['subtitle_home_page'] : '');
	edit_additional_field($post_id, 'subtitle_home_page', $subtitle_home_page, 'destinations');

  // NOTE: new
  //-- Title Home Page
  $title_landing_page = (isset($_POST['title_landing_page']) ? $_POST['title_landing_page'] : '');
  edit_additional_field($post_id, 'title_landing_page', $title_landing_page, 'destinations');

  // Sub Title Home Page
  $subtitle_landing_page = (isset($_POST['subtitle_landing_page']) ? $_POST['subtitle_landing_page'] : '');
  edit_additional_field($post_id, 'subtitle_landing_page', $subtitle_landing_page, 'destinations');
  // end new

	$title_reception_venue_1 = (isset($_POST['title_reception_venue_1']) ? $_POST['title_reception_venue_1'] : '');
	edit_additional_field($post_id, 'title_reception_venue_1', $title_reception_venue_1, 'destinations');

	$title_reception_venue_2 = (isset($_POST['title_reception_venue_2']) ? $_POST['title_reception_venue_2'] : '');
	edit_additional_field($post_id, 'title_reception_venue_2', $title_reception_venue_2, 'destinations');

	//-- Phone Number
	$phone_number = (isset($_POST['phone_number']) ? $_POST['phone_number'] : '');
	edit_additional_field($post_id, 'phone_number', $phone_number, 'destinations');

	//-- Fax Number
	$fax_number = (isset($_POST['fax_number']) ? $_POST['fax_number'] : '');
	edit_additional_field($post_id, 'fax_number', $fax_number, 'destinations');

	//-- Reservation Phone Number
	$reservation_phone_number = (isset($_POST['reservation_phone_number']) ? $_POST['reservation_phone_number'] : '');
	edit_additional_field($post_id, 'reservation_phone_number', $reservation_phone_number, 'destinations');

	//-- Reservation Fax Number
	$reservation_fax_number = (isset($_POST['reservation_fax_number']) ? $_POST['reservation_fax_number'] : '');
	edit_additional_field($post_id, 'reservation_fax_number', $reservation_fax_number, 'destinations');

	//-- Facebook Link
	$fb_link = (isset($_POST['fb_link']) ? $_POST['fb_link'] : '');
	edit_additional_field($post_id, 'fb_link', $fb_link, 'destinations');

	//-- Youtube Link
	$youtube_link = (isset($_POST['youtube_link']) ? $_POST['youtube_link'] : '');
	edit_additional_field($post_id, 'youtube_link', $youtube_link, 'destinations');

	//-- Instagram Link
	$instagram_link = (isset($_POST['instagram_link']) ? $_POST['instagram_link'] : '');
	edit_additional_field($post_id, 'instagram_link', $instagram_link, 'destinations');

	//-- Twitter Link
	$twitter_link = (isset($_POST['twitter_link']) ? $_POST['twitter_link'] : '');
	edit_additional_field($post_id, 'twitter_link', $twitter_link, 'destinations');

	//-- Email Address
	$email_address = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
	edit_additional_field($post_id, 'email_address', $email_address, 'destinations');

	//-- Whatsapp Number
	$whatsapp_number = (isset($_POST['whatsapp_number']) ? $_POST['whatsapp_number'] : '');
	edit_additional_field($post_id, 'whatsapp_number', $whatsapp_number, 'destinations');

	//-- Whatsapp Status
	$whatsapp_status = (isset($_POST['whatsapp_status']) ? $_POST['whatsapp_status'] : '');
	edit_additional_field($post_id, 'whatsapp_status', $whatsapp_status, 'destinations');

	//-- Reservation Email Address
	$reservation_email_address = (isset($_POST['reservation_email_address']) ? $_POST['reservation_email_address'] : '');
	edit_additional_field($post_id, 'reservation_email_address', $reservation_email_address, 'destinations');

	$spa_reservation_email_address = (isset($_POST['spa_reservation_email_address']) ? $_POST['spa_reservation_email_address'] : '');
	edit_additional_field($post_id, 'spa_reservation_email_address', $spa_reservation_email_address, 'destinations');

	$tax_default_spa = (isset($_POST['tax_default_spa']) ? $_POST['tax_default_spa'] : '');
	edit_additional_field($post_id, 'tax_default_spa', $tax_default_spa, 'destinations');

	$events_inquiry_email_address = (isset($_POST['events_inquiry_email_address']) ? $_POST['events_inquiry_email_address'] : '');
	edit_additional_field($post_id, 'events_inquiry_email_address', $events_inquiry_email_address, 'destinations');

	$dining_reservation_email_address = (isset($_POST['dining_reservation_email_address']) ? $_POST['dining_reservation_email_address'] : '');
	edit_additional_field($post_id, 'dining_reservation_email_address', $dining_reservation_email_address, 'destinations');

	//-- Instagram Username
	$instagram_user = (isset($_POST['instagram_user']) ? $_POST['instagram_user'] : '');
	edit_additional_field($post_id, 'instagram_user', $instagram_user, 'destinations');

	//-- Position On Map
	$coordinate_location = (isset($_POST['coordinate_location']) ? $_POST['coordinate_location'] : '');
	edit_additional_field($post_id, 'coordinate_location', $coordinate_location, 'destinations');

	//-- LINK DIRECTION
	$link_direction = (isset($_POST['link_direction']) ? $_POST['link_direction'] : '');
	edit_additional_field($post_id, 'link_direction', $link_direction, 'destinations');

	$widget_traveller_choice = (isset($_POST['widget_traveller_choice']) ? $_POST['widget_traveller_choice'] : '');
	edit_additional_field($post_id, 'widget_traveller_choice', $widget_traveller_choice, 'destinations');

	$widget_certificate_excellenge = (isset($_POST['widget_certificate_excellenge']) ? $_POST['widget_certificate_excellenge'] : '');
	edit_additional_field($post_id, 'widget_certificate_excellenge', $widget_certificate_excellenge, 'destinations');

	$meta_title = (isset($_POST['meta_title']) ? $_POST['meta_title'] : '');
	edit_additional_field($post_id, 'meta_title', $meta_title, 'destinations');

	$meta_keywords = (isset($_POST['meta_keywords']) ? $_POST['meta_keywords'] : '');
	edit_additional_field($post_id, 'meta_keywords', $meta_keywords, 'destinations');

	$meta_description = (isset($_POST['meta_description']) ? $_POST['meta_description'] : '');
	edit_additional_field($post_id, 'meta_description', $meta_description, 'destinations');

	//-- MAILCHIMP API KEY
	$mailchimp_api_key = (isset($_POST['mailchimp_api_key']) ? $_POST['mailchimp_api_key'] : '');
	edit_additional_field($post_id, 'mailchimp_api_key', $mailchimp_api_key, 'destinations');

	//-- MAILCHIMP LIST ID
	$mailchimp_list_id = (isset($_POST['mailchimp_list_id']) ? $_POST['mailchimp_list_id'] : '');
	edit_additional_field($post_id, 'mailchimp_list_id', $mailchimp_list_id, 'destinations');

	$fb_properties_name = (isset($_POST['fb_properties_name']) ? $_POST['fb_properties_name'] : '');
	edit_additional_field($post_id, 'fb_properties_name', $fb_properties_name, 'destinations');

	$fb_hotels_name = (isset($_POST['fb_hotels_name']) ? $_POST['fb_hotels_name'] : '');
	edit_additional_field($post_id, 'fb_hotels_name', $fb_hotels_name, 'destinations');

	$status_popup_newsletter = (isset($_POST['status_popup_newsletter']) ? $_POST['status_popup_newsletter'] : '');
	edit_additional_field($post_id, 'status_popup_newsletter', $status_popup_newsletter, 'destinations');

	$url_popup_newsletter = (isset($_POST['url_popup_newsletter']) ? $_POST['url_popup_newsletter'] : '');
	edit_additional_field($post_id, 'url_popup_newsletter', $url_popup_newsletter, 'destinations');

	$title_policy = (isset($_POST['title_policy']) ? $_POST['title_policy'] : '');
	edit_additional_field($post_id, 'title_policy', $title_policy, 'destinations');

	$sub_title_policy = (isset($_POST['sub_title_policy']) ? $_POST['sub_title_policy'] : '');
	edit_additional_field($post_id, 'sub_title_policy', $sub_title_policy, 'destinations');

	$description_policy = (isset($_POST['description_policy']) ? $_POST['description_policy'] : '');
	edit_additional_field($post_id, 'description_policy', $description_policy, 'destinations');

	$meta_title_policy = (isset($_POST['meta_title_policy']) ? $_POST['meta_title_policy'] : '');
	edit_additional_field($post_id, 'meta_title_policy', $meta_title_policy, 'destinations');

	$meta_keywords_policy = (isset($_POST['meta_keywords_policy']) ? $_POST['meta_keywords_policy'] : '');
	edit_additional_field($post_id, 'meta_keywords_policy', $meta_keywords_policy, 'destinations');

	$meta_description_policy = (isset($_POST['meta_description_policy']) ? $_POST['meta_description_policy'] : '');
	edit_additional_field($post_id, 'meta_description_policy', $meta_description_policy, 'destinations');

	$ig_access_token = (isset($_POST['ig_access_token']) ? $_POST['ig_access_token'] : '');
	edit_additional_field($post_id, 'ig_access_token', $ig_access_token, 'destinations');

	$fb_access_token = (isset($_POST['fb_access_token']) ? $_POST['fb_access_token'] : '');
	edit_additional_field($post_id, 'fb_access_token', $fb_access_token, 'destinations');

	$fb_page_id = (isset($_POST['fb_page_id']) ? $_POST['fb_page_id'] : '');
	edit_additional_field($post_id, 'fb_page_id', $fb_page_id, 'destinations');

	//-- Address
	$address = (isset($_POST['address']) ? $_POST['address'] : '');
	edit_additional_field($post_id, 'address', $address, 'destinations');


	// LANGUAGE DATA
	if(check_active_plugin_language())
	{
		$title_lang 					= (isset($_POST['title_lang']) ? $_POST['title_lang'] : '');
		$title_home_page_lang 			= (isset($_POST['title_home_page_lang']) ? $_POST['title_home_page_lang'] : '');
		$subtitle_home_page_lang 		= (isset($_POST['subtitle_home_page_lang']) ? $_POST['subtitle_home_page_lang'] : '');

    $title_landing_page_lang 			= (isset($_POST['title_landing_page_lang']) ? $_POST['title_landing_page_lang'] : '');
		$subtitle_landing_page_lang 		= (isset($_POST['subtitle_landing_page_lang']) ? $_POST['subtitle_landing_page_lang'] : '');

		$post_content_lang 				= (isset($_POST['post_content_lang']) ? $_POST['post_content_lang'] : '');
		$brief_lang 					= (isset($_POST['brief_lang']) ? $_POST['brief_lang'] : '');
		$title_reception_venue_1_lang	= (isset($_POST['title_reception_venue_1_lang']) ? $_POST['title_reception_venue_1_lang'] : '');
		$title_reception_venue_2_lang	= (isset($_POST['title_reception_venue_2_lang']) ? $_POST['title_reception_venue_2_lang'] : '');
		$meta_title_lang				= (isset($_POST['meta_title_lang']) ? $_POST['meta_title_lang'] : '');
		$meta_keywords_lang				= (isset($_POST['meta_keywords_lang']) ? $_POST['meta_keywords_lang'] : '');
		$meta_description_lang			= (isset($_POST['meta_description_lang']) ? $_POST['meta_description_lang'] : '');
		$url_popup_newsletter_lang		= (isset($_POST['url_popup_newsletter_lang']) ? $_POST['url_popup_newsletter_lang'] : '');
		$title_policy_lang				= (isset($_POST['title_policy_lang']) ? $_POST['title_policy_lang'] : '');
		$sub_title_policy_lang			= (isset($_POST['sub_title_policy_lang']) ? $_POST['sub_title_policy_lang'] : '');
		$description_policy_lang		= (isset($_POST['description_policy_lang']) ? $_POST['description_policy_lang'] : '');
		$meta_title_policy_lang			= (isset($_POST['meta_title_policy_lang']) ? $_POST['meta_title_policy_lang'] : '');
		$meta_keywords_policy_lang		= (isset($_POST['meta_keywords_policy_lang']) ? $_POST['meta_keywords_policy_lang'] : '');
		$meta_description_policy_lang	= (isset($_POST['meta_description_policy_lang']) ? $_POST['meta_description_policy_lang'] : '');

		foreach($title_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($title_home_page_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($subtitle_home_page_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

    foreach($title_landing_page_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($subtitle_landing_page_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}
    
		foreach($title_reception_venue_1_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($title_reception_venue_2_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($meta_title_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($meta_keywords_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($meta_description_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($url_popup_newsletter_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($title_policy_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($sub_title_policy_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($description_policy_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($meta_title_policy_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($meta_keywords_policy_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($meta_description_policy_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($post_content_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}

		foreach($brief_lang as $key => $d)
		{
			edit_additional_field($post_id, $key, $d, 'destinations');
		}
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus background image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_background_image($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'background_image','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/background/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/small/'.$bg_image);
	}

	if(edit_additional_field($post_id, 'background_image', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus background image homepage di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_background_image_homepage($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'background_image_homepage','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/background/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/small/'.$bg_image);
	}

	if(edit_additional_field($post_id, 'background_image_homepage', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus background image events di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_background_image_events($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'bg_image_events','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/background/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/small/'.$bg_image);
	}

	if(edit_additional_field($post_id, 'bg_image_events', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus footer image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_footer_image($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'footer_image','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/footer/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/footer/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/footer/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/footer/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/footer/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/footer/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/footer/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/footer/small/'.$bg_image);
	}

	if(edit_additional_field($post_id, 'footer_image', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus popup image newsletter di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_popup_image_newsletter($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'popup_image_newsletter','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/'.$bg_image);
	}

	// if(file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/large/'.$bg_image))
	// {
	// 	unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/large/'.$bg_image);
	// }

	// if(file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/medium/'.$bg_image))
	// {
	// 	unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/medium/'.$bg_image);
	// }

	// if(file_exists(PLUGINS_PATH.'/destinations/popup_image_newsletter/small/'.$bg_image))
	// {
	// 	unlink(PLUGINS_PATH.'/destinations/popup_image_newsletter/small/'.$bg_image);
	// }

	if(edit_additional_field($post_id, 'popup_image_newsletter', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus background image policy di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_bg_image_policy($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'bg_image_policy','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/bg_image_policy/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/bg_image_policy/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/bg_image_policy/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/bg_image_policy/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/bg_image_policy/small/'.$bg_image);
	}

	if(edit_additional_field($post_id, 'bg_image_policy', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus reception venue 1 image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_reception_venue_1_image($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'dinner_reception_venue_1','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/small/'.$bg_image);
	}


	if(edit_additional_field($post_id, 'dinner_reception_venue_1', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus reception venue 2 image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_reception_venue_2_image($post_id='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'dinner_reception_venue_2','destinations');

	if(empty($bg_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/large/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/large/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/medium/'.$bg_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/reception-venue/small/'.$bg_image))
	{
		unlink(PLUGINS_PATH.'/destinations/reception-venue/small/'.$bg_image);
	}


	if(edit_additional_field($post_id, 'dinner_reception_venue_2', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus corporate image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_corporate_image($post_id='')
{
	if(empty($post_id)) return true;

	$cp_image = get_additional_field($post_id,'corporate_image','destinations');

	if(empty($cp_image)) return true;

	if(file_exists(PLUGINS_PATH.'/destinations/background/'.$cp_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/'.$cp_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/large/'.$cp_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/large/'.$cp_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$cp_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/medium/'.$cp_image);
	}

	if(file_exists(PLUGINS_PATH.'/destinations/background/small/'.$cp_image))
	{
		unlink(PLUGINS_PATH.'/destinations/background/small/'.$cp_image);
	}

	if(edit_additional_field($post_id, 'corporate_image', '', 'destinations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan background image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background($post_id='')
{
	$result   = '';
	$bg_image = get_additional_field($post_id,'background_image','destinations');

	if(!empty($bg_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="bg-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan background image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background_homepage($post_id='')
{
	$result   = '';
	$bg_image = get_additional_field($post_id,'background_image_homepage','destinations');

	if(!empty($bg_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="bg-homepage-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan background image events pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_background_events($post_id='')
{
	$result   = '';
	$bg_image = get_additional_field($post_id,'bg_image_events','destinations');

	if(!empty($bg_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$bg_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="bg-delete-events">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan corporate image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_corporate($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'corporate_image','destinations');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="cp-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan corporate image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_footer($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'footer_image','destinations');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/footer/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="cp-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan reception image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_dinner_reception_venue_1_image($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'dinner_reception_venue_1','destinations');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/reception-venue/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="reception-1-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan reception image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_dinner_reception_venue_2_image($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'dinner_reception_venue_2','destinations');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/reception-venue/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="reception-2-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan popup image newsletter
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_popup_image_newsletter_image($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'popup_image_newsletter','destinations');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/popup_image_newsletter/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="popup-image-newsletter-delete">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menampilkan reception image pada admin page
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_bg_image_policy_image($post_id='')
{
	$result   = '';
	$cp_image = get_additional_field($post_id,'bg_image_policy','destinations');

	if(!empty($cp_image))
	{
		$result .= '
        <div class="box">
            <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/bg_image_policy/'.$cp_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" class="bg-policy-delete-header">
                    <img src="'.HTTP.SITE_URL.'/lumonata-plugins/destinations/images/delete.png">
                </a>
            </div>
        </div>';
	}

	return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi-fungsi lainnya
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_background_image($post_id, $type='original')
{
	$result   = '';
	$filename = get_additional_field($post_id,'background_image','destinations');

	if($type=="original")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
		}
	}
	elseif($type=="large")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/large/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/large/'.$filename;
		}
	}
	elseif($type=="medium")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/medium/'.$filename;
		}
	}
	elseif($type=="small")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/small/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/small/'.$filename;
		}
	}

	if(empty($result))
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
		}
	}

	return $result;
}

function get_background_image_homepage($post_id, $type='original')
{
	$result   = '';
	$filename = get_additional_field($post_id,'background_image_homepage','destinations');

	if($type=="original")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
		}
	}
	elseif($type=="large")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/large/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/large/'.$filename;
		}
	}
	elseif($type=="medium")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/medium/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/medium/'.$filename;
		}
	}
	elseif($type=="small")
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/small/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/small/'.$filename;
		}
	}

	if(empty($result))
	{
		if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
		{
			$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
		}
	}

	return $result;
}


function get_corporate_image($post_id)
{
	$result   = '';
	$filename = get_additional_field($post_id,'corporate_image','destinations');

	if(!empty($filename) && file_exists(PLUGINS_PATH.'/destinations/background/'.$filename))
	{
		$result = HTTP.SITE_URL.'/lumonata-plugins/destinations/background/'.$filename;
	}

	return $result;
}

function get_position_on_map($post_id)
{
	$coordinate = get_additional_field($post_id,'coordinate_location','destinations');
	return $coordinate;
}

function get_link_direction($post_id)
{
	$direction = get_additional_field($post_id,'link_direction','destinations');
	return $direction;
}

function get_meta_title_dest($post_id)
{
	$meta_title = get_additional_field($post_id,'meta_title','destinations');
	return $meta_title;
}

function get_meta_keywords_dest($post_id)
{
	$meta_keywords = get_additional_field($post_id,'meta_keywords','destinations');
	return $meta_keywords;
}

function get_meta_description_dest($post_id)
{
	$meta_description = get_additional_field($post_id,'meta_description','destinations');
	return $meta_description;
}

function get_mailchimp_api_key($post_id)
{
	$mailchimp_api_key = get_additional_field($post_id,'mailchimp_api_key','destinations');
	return $mailchimp_api_key;
}

function get_mailchimp_list_id($post_id)
{
	$mailchimp_list_id = get_additional_field($post_id,'mailchimp_list_id','destinations');
	return $mailchimp_list_id;
}

function get_fb_properties_name($post_id)
{
	$fb_properties_name = get_additional_field($post_id,'fb_properties_name','destinations');
	return $fb_properties_name;
}

function get_fb_hotels_name($post_id)
{
	$fb_hotels_name = get_additional_field($post_id,'fb_hotels_name','destinations');
	return $fb_hotels_name;
}

function get_ig_access_token($post_id)
{
	$ig_access_token = get_additional_field($post_id,'ig_access_token','destinations');
	return $ig_access_token;
}

function get_fb_access_token($post_id)
{
	$fb_access_token = get_additional_field($post_id,'fb_access_token','destinations');
	return $fb_access_token;
}

function get_fb_page_id($post_id)
{
	$fb_page_id = get_additional_field($post_id,'fb_page_id','destinations');
	return $fb_page_id;
}

function get_address($post_id)
{
	$address = get_additional_field($post_id,'address','destinations');
	return nl2br($address);
}

function get_phone_number($post_id)
{
	$phone_number = get_additional_field($post_id,'phone_number','destinations');
	return $phone_number;
}

function get_fax_number($post_id)
{
	$fax_number = get_additional_field($post_id,'fax_number','destinations');
	return $fax_number;
}

function get_reservation_phone_number($post_id)
{
	$reservation_phone_number = get_additional_field($post_id,'reservation_phone_number','destinations');
	return $reservation_phone_number;
}

function get_reservation_fax_number($post_id)
{
	$reservation_fax_number = get_additional_field($post_id,'reservation_fax_number','destinations');
	return $reservation_fax_number;
}

function get_email_address($post_id)
{
	$email_address = get_additional_field($post_id,'email_address','destinations');
	return $email_address;
}

function get_whatsapp_number($post_id)
{
	$whatsapp_number = get_additional_field($post_id,'whatsapp_number','destinations');
	return $whatsapp_number;
}

function get_whatsapp_status($post_id)
{
	$whatsapp_status = get_additional_field($post_id,'whatsapp_status','destinations');
	return $whatsapp_status;
}

function get_reservation_email_address($post_id)
{
	$reservation_email_address = get_additional_field($post_id,'reservation_email_address','destinations');
	return $reservation_email_address;
}

function get_spa_reservation_email_address($post_id)
{
	$spa_reservation_email_address = get_additional_field($post_id,'spa_reservation_email_address','destinations');
	return $spa_reservation_email_address;
}

function get_events_inqury_email_address($post_id)
{
	$events_inquiry_email_address = get_additional_field($post_id,'events_inquiry_email_address','destinations');
	return $events_inquiry_email_address;
}

function get_tax_default_spa($post_id)
{
	$tax_default_spa = get_additional_field($post_id,'tax_default_spa','destinations');
	return $tax_default_spa;
}

function get_fb_link($post_id)
{
	$fb_link = get_additional_field($post_id,'fb_link','destinations');
	return $fb_link;
}

function get_instagram_link($post_id)
{
	$instagram_link = get_additional_field($post_id,'instagram_link','destinations');
	return $instagram_link;
}

function get_youtube_link($post_id)
{
	$youtube_link = get_additional_field($post_id,'youtube_link','destinations');
	return $youtube_link;
}

function get_twitter_link($post_id)
{
	$twitter_link = get_additional_field($post_id,'twitter_link','destinations');
	return $twitter_link;
}

function get_instagram_photo_test($post_id)
{
    $username  = get_additional_field($post_id,'instagram_user','destinations');
    $client_id = '69d2e031ff9b4195a8d378075238dfe9';

    $u = 'https://api.instagram.com/v1/users/search?q='.$username.'&client_id='.$client_id;
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$u);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,false);

    $r = curl_exec($ch);
         curl_close($ch);
    $d = json_decode($r,true);

    $list = '';
    if(isset($d['data'][0]['id']))
    {
        $uu = 'https://api.instagram.com/v1/users/'.$d['data'][0]['id'].'/media/recent/?client_id='.$client_id.'&count=12';
        $ss = curl_init();
        curl_setopt($ss,CURLOPT_URL,$uu);
        curl_setopt($ss,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ss,CURLOPT_SSL_VERIFYPEER,false);

        $rr = curl_exec($ss);
              curl_close($ss);
        $result = json_decode($rr,true);

        if(isset($result['data']) && !empty($result['data']))
        {
            foreach($result['data'] as $i=>$obj)
            {
                $list .= '
                <div class="box">
                    <a href="'.$obj['link'].'" target="_blank" rel="instagram">
                        <img class="b-lazy" src="'.$obj['images']['standard_resolution']['url'].'" data-src="'.$obj['images']['standard_resolution']['url'].'"/>
                    </a>
                </div>';

                if($i==7) break;
            }
        }
    }

    return $list;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menjalankan ajax request
| -------------------------------------------------------------------------------------------------------------------------
*/
function execute_ajax_request()
{
	global $db;

	if($_POST['ajax_key'] =='search-destination')
	{
	    $result = '';

		$s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
	    $q = $db->prepare_query($s,'destinations',"%".$_POST['search_val']."%");
	   	$r = $db->do_query($q);
		if($db->num_rows($r) > 0)
	    {
			while($d=$db->fetch_array($r))
			{
				$result .= '
                <div class="list_item boxs clearfix" id="theitem_'.$d['larticle_id'].'">
                    <div class="boxs title push-left">
                        <input type="checkbox" name="select[]" class="title_checkbox select" value="'.$d['larticle_id'].'" />
                        <label>'.$d['larticle_title'].'</label>
                    </div>
                    <div class="boxs description push-left"><p>'.limit_words(strip_tags($d['larticle_content']), 50).'</p></div>
                    <div class="the_navigation_list">
                        <div class="list_navigation" style="display:none;" id="the_navigation_'.$d['larticle_id'].'">
                            <a href="'.get_state_url('destinations').'&prc=edit&id='.$d['larticle_id'].'">Edit</a> |
                            <a href="javascript:;" class="delete_link" id="'.$d['larticle_id'].'" rel="delete_'.$d['larticle_id'].'">Delete</a>
                        </div>
                    </div>
                </div>';
			}
        }
		else
		{
			$result = '
			<div class="alert_yellow_form">
				No result found for <em>"'.$_POST['search_val'].'"</em>.
				Check your spellling or try another terms
			</div>';
        }

		echo $result;
	}

	if($_POST['ajax_key'] =='delete-destination')
	{
		if(delete_destinations($_POST['dest_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-background-image')
	{
		$data = set_background_images();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-background-image')
	{
		if(delete_background_image($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-background-image-homepage')
	{
		$data = set_background_images_homepage();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-background-image-homepage')
	{
		if(delete_background_image_homepage($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-bg-image-events')
	{
		$data = set_background_images_events();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-bg-image-events')
	{
		if(delete_background_image_events($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-footer-image')
	{
		$data = set_footer_images();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-footer-image')
	{
		if(delete_footer_image($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-popup-image-newsletter')
	{
		$data = set_popup_image_newsletter();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-popup-image-newsletter')
	{
		if(delete_popup_image_newsletter($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-bg-image-policy')
	{
		$data = set_bg_image_policy();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-bg-image-policy')
	{
		if(delete_bg_image_policy($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-dinner-reception-venue-1')
	{
		$data = set_dinner_reception_venue_1();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-reception-venue-1-image')
	{
		if(delete_reception_venue_1_image($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-dinner-reception-venue-2')
	{
		$data = set_dinner_reception_venue_2();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-reception-venue-2-image')
	{
		if(delete_reception_venue_2_image($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	if($_POST['ajax_key'] =='add-corporate-image')
	{
		$data = set_corporate_images();

		if(empty($data['filename']))
		{
			echo '{"result":"failed"}';
		}
		else
		{
			$res['result']   = 'success';
			$res['post_id']  = $data['post_id'];
			$res['filename'] = $data['filename'];

			echo json_encode($res);
		}
	}

	if($_POST['ajax_key'] =='delete-corporate-image')
	{
		if(delete_corporate_image($_POST['post_id']))
		{
			echo '{"result":"success"}';
		}
		else
		{
			echo '{"result":"failed"}';
		}
	}

	exit;
}
?>
