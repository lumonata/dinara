<?php

/*
| -------------------------------------------------------------------------------------------------------------------------
    Plugin Name: Lumonata Locations Application
    Plugin URL: http://lumonata.com/
    Description: This plugin is use for adding locations on google map
    Author: Ngurah Rai
    Author URL: http://lumonata.com/
    Version: 1.0.0
| -------------------------------------------------------------------------------------------------------------------------
*/

add_privileges('administrator', 'locations', 'insert');
add_privileges('administrator', 'locations', 'update');
add_privileges('administrator', 'locations', 'delete');

//-- Add custom icon menu
add_actions('header_elements', 'locations_header_element');
function locations_header_element()
{
    return '<link href="'.HTTP.SITE_URL.'/lumonata-plugins/locations/style.css" rel="stylesheet">';
}

//-- Add sub menu under applications menu
add_main_menu(array('locations'=>'Locations'));
add_actions('locations', 'set_locations_data');


/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini otomatis akan dijalankan untuk melakukan filter action
    yang akan dilakukan -- (get, add, edit, delete, delete all)
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_locations_data()
{
    if (is_add_new()) {
        return add_locations();
    } elseif (is_edit()) {
        return edit_locations();
    } elseif (is_delete_all()) {
        return delete_all_locations();
    } elseif (is_confirm_delete()) {
        foreach ($_POST['id'] as $key=>$val) {
            delete_locations($val);
            delete_locations_image($val);
        }
    } elseif (is_ajax_request()) {
        return execute_loc_ajax_request();
    }

    if (is_num_locations() > 0) {
        if (isset($_GET['tab']) && $_GET['tab']=='setting') {
            return set_location_setting();
        } else {
            return get_locations_list();
        }
    } else {
        header('location:'.get_state_url('locations').'&prc=add_new');
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk mengambil jumlah data location
| -------------------------------------------------------------------------------------------------------------------------
*/
function is_num_locations()
{
    global $db;

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
    $q = $db->prepare_query($s, 'locations');
    $r = $db->do_query($q);

    return $db->num_rows($r);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk mengambil data locations dari database
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_locations_list()
{
    global $db;

    $list   = '';
    $search = (isset($_POST['s']) ? $_POST['s'] : '');
    $page   = (isset($_GET['page']) ? $_GET['page'] : 1);
    $url    = get_state_url('locations')."&page=";
    $viewed = list_viewed();
    $limit  = ($page-1) * $viewed;
    $start  = ($page - 1) * $viewed + 1;

    if (is_search()) {
        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
        $q = $db->prepare_query($s, 'locations', "%".$_POST['s']."%");
        $r = $db->do_query($q);
        $n = $db->num_rows($r);

        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s ORDER BY lorder LIMIT %d, %d';
        $q = $db->prepare_query($s, 'locations', '%'.$_POST['s'].'%', $limit, $viewed);
    } else {
        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
        $q = $db->prepare_query($s, 'locations');
        $r = $db->do_query($q);
        $n = $db->num_rows($r);

        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder LIMIT %d, %d';
        $q = $db->prepare_query($s, 'locations', $limit, $viewed);
    }

    $button = '
	<li>'.button('button=add_new', get_state_url('locations').'&prc=add_new').'</li>
	<li>'.button('button=delete&type=submit&enable=false').'</li>';

    set_template(PLUGINS_PATH."/locations/list.html", 'locations_list_template');

    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        add_block('locations_list', 'loc_list', 'locations_list_template');

        while ($d=$db->fetch_array($r)) {
            // $corporate = get_additional_field($d['larticle_id'], 'loc_type', 'locations');
            $loctype   = get_additional_field($d['larticle_id'], 'loc_type', 'locations');
            $destination   = get_additional_field($d['larticle_id'], 'destination', 'locations');

            add_variable('id', $d['larticle_id']);
            add_variable('title', $d['larticle_title']);
            add_variable('destination', $destination);
            // add_variable('destination', ucfirst($corporate));
            add_variable('type', $loctype);
            add_variable('type_img', 'http://'.SITE_URL.'/lumonata-plugins/locations/images/ico-'.strtolower($loctype).'.png');

            parse_template('locations_list', 'loc_list', true);
        }
    }

    add_block('locations', 'loc', 'locations_list_template');
    add_actions('section_title', 'Locations - List');

    add_variable('app_title', 'Locations List');
    add_variable('button', $button);
    add_variable('search_val', $search);
    add_variable('start_order', $start);
    add_variable('state', 'locations');
    add_variable('state_url', get_state_url('locations'));
	add_variable( 'HTTP', HTTP );
    add_variable('template_url', TEMPLATE_URL);
    add_variable('tab', set_tabs(array('blogs'=>'Locations','setting'=>'Setting'), 'blogs'));
    add_variable('view_more', ($n > $viewed ? get_load_more_link($page) : ''));

    parse_template('locations', 'loc', false);
    return return_template('locations_list_template');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk mengambil data locations via ajax request
| -------------------------------------------------------------------------------------------------------------------------
*/
function get_location_list_ajax()
{
    global $db;

    $list   = '';
    $search = (isset($_POST['s']) ? $_POST['s'] : '');
    $page   = (isset($_POST['page']) ? $_POST['page']+1 : 1);
    $url    = get_state_url('locations').'&page=';
    $viewed = list_viewed();
    $limit  = ($page-1) * $viewed;
    $start  = ($page - 1) * $viewed + 1;

    if (is_search()) {
        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s';
        $q = $db->prepare_query($s, 'locations', "%".$search."%");
        $r = $db->do_query($q);
        $n = $db->num_rows($r);

        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_title LIKE %s LIMIT %d, %d';
        $q = $db->prepare_query($s, 'locations', '%'.$search.'%', $limit, $viewed);
    } else {
        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
        $q = $db->prepare_query($s, 'locations');
        $r = $db->do_query($q);
        $n = $db->num_rows($r);

        $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s LIMIT %d, %d';
        $q = $db->prepare_query($s, 'locations', $limit, $viewed);
    }

    $r = $db->do_query($q);

    if ($db->num_rows($r) > 0) {
        while ($d=$db->fetch_array($r)) {
            $corporate = get_additional_field($d['larticle_id'], 'loc_corporate', 'locations');
            $loctype   = get_additional_field($d['larticle_id'], 'loc_type', 'locations');

            $list .= '
            <div class="list_item boxs clearfixed" id="theitem_'.$d['larticle_id'].'">
                <div class="boxs title push-left">
                    <input type="checkbox" name="select[]" class="title_checkbox select" value="'.$d['larticle_id'].'" />
                    <label>'.$d['larticle_title'].'</label>
                </div>
                <div class="boxs destination push-left"><p>'.ucfirst($corporate).'</p></div>
                <div class="boxs type push-left">
                	<p>
                		<img src="http://'.SITE_URL.'/lumonata-plugins/locations/images/ico-'.strtolower($loctype).'.png" />
                		'.$loctype.'
                	</p>
                </div>
                <div class="the_navigation_list">
                    <div class="list_navigation" id="the_navigation_'.$d['larticle_id'].'">
                        <a href="'.get_state_url('locations').'&prc=edit&id='.$d['larticle_id'].'">Edit</a> |
                        <a href="javascript:;" class="delete_link" id="delete_'.$d['larticle_id'].'" rel="'.$d['larticle_id'].'" title="'.$d['larticle_title'].'">Delete</a>
                    </div>
                </div>
            </div>';
        }

        $result['list'] = $list;
        $result['more'] = ($n > $viewed ? get_load_more_link($page) : '');
    } else {
        if (is_search()) {
            $list .= '
			<div class="alert_yellow_form">
				No result found for <em>"'.$search.'"</em>.
				Check your spellling or try another terms
			</div>';
        } else {
            $list .= '
			<div class="alert_yellow_form">
				No data was found
			</div>';
        }

        $result['list'] = $list;
        $result['more'] = '';
    }

    return $result;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk meyimpan data locations ke database
| -------------------------------------------------------------------------------------------------------------------------
*/
function add_locations()
{
    $message  = '';
    $mess_arr = array();

    $loc_name      = (isset($_POST['loc_name']) ? $_POST['loc_name'] : '');
    $loc_type      = (isset($_POST['loc_type']) ? $_POST['loc_type'] : '');
    // $loc_corporate = (isset($_POST['corporate']) ? $_POST['corporate'] : '');
    $get_off       = (isset($_POST['get_off']) ? $_POST['get_off'] : '');
    $longitude     = (isset($_POST['longitude']) ? $_POST['longitude'] : '');
    $latitude      = (isset($_POST['latitude']) ? $_POST['latitude'] : '');
    $loc_content   = (isset($_POST['loc_content']) ? rem_slashes($_POST['loc_content']) : '');

    $def_location  = array(-8.670409, 115.212585);
    if (!empty($latitude)) {
        $def_location = explode(',', trim($latitude));
    }

    if (is_save_changes()) {
        if (empty($loc_name)) {
            $mess_arr[] = '- Please input your location name';
        }

        if (empty($loc_type)) {
            $mess_arr[] = '- Please select your location type';
        }

        if (empty($latitude)) {
            $mess_arr[] = '- Please input your location coordinate on map';
        }

        // if (empty($loc_corporate)) {
        //     $mess_arr[] = '- Please input the corporate of your location';
        // }

        if (empty($mess_arr)) {
            $is_save_success = save_article($loc_name, '', '', $loc_content, 'publish', 'locations');

            if ($is_save_success) {
                $post_id = mysql_insert_id();
                set_additional_data($post_id);
                set_locations_images($post_id);

                $loc_name      = '';
                $loc_type      = '';
                // $loc_corporate = '';
                $get_off    = '';
                $longitude    = '';
                $latitude = '';
                $loc_content   = '';

                $message = '<div class="message success">New locations has save succesfully.</div>';
            } else {
                $message = '<div class="message error">Something wrong, please try again.</div>';
            }
        } else {
            $message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
        }
    }

    $default_pointer  = 'http://'.SITE_URL.'/lumonata-plugins/locations/images/ico-pointer.png';

    $button    = '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=add_new', get_state_url('locations').'&prc=add_new').'</li>
	<li>'.button('button=cancel', get_state_url('locations')).'</li>';

    set_template(PLUGINS_PATH.'/locations/form.html', 'locations_form_template');
    add_block('locations_form', 'loc_form', 'locations_form_template');
    add_actions('section_title', 'Add New Location');

	add_variable( 'HTTP', HTTP );
    add_variable('form_title', 'Add New Location');
    add_variable('loc_id', '');
    add_variable('loc_name', $loc_name);
    add_variable('loc_content', textarea('loc_content', 0, $loc_content, 0, true));
    add_variable('loc_type', get_loc_type_list($loc_type));
    add_variable('get_off', $get_off);
    add_variable('longitude', $longitude);
    add_variable('latitude', $latitude);
    // add_variable('locations_image',get_location_image_top(0, $_GET['id'], 'locations'));
    add_variable('destination_form_choose', destination_form_choose_location('', ''));


    // add_variable('corporate_list', get_corporate_option_list($loc_corporate));
    add_variable('default_pointer', $default_pointer);
    add_variable('state_url', get_state_url('locations'));
    add_variable('message', $message);

    add_variable('button', $button);
    add_variable('site_url', SITE_URL);
    add_variable('plugin_url', SITE_URL.'/lumonata-plugins/locations');

    parse_template('locations_form', 'loc_form', false);
    return return_template('locations_form_template');
}


function destination_form_choose_location($post_id, $apps)
{
    $destination_opt = get_destination_option($post_id, $apps);
    
    $html = '
    <div class="additional_data">
        <h2>Destination</h2>
        <div class="additional_content">
            <div class="boxs-wrapp">
                <div class="boxs">
                    <label>Select your destination page for this post : </label>
                    <select name="destination" class="dest-opt" />
                        '.$destination_opt.'
                    </select>
                </div>
            </div>
        </div>
    </div>
    ';

    return $html;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk mengubah data locations di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function edit_locations()
{
    global $db;

    $message  = '';
    $mess_arr = array();

    $loc_name      = (isset($_POST['loc_name']) ? $_POST['loc_name'] : '');
    $loc_type      = (isset($_POST['loc_type']) ? $_POST['loc_type'] : '');
    // $loc_corporate = (isset($_POST['corporate']) ? $_POST['corporate'] : '');
    $get_off       = (isset($_POST['get_off']) ? $_POST['get_off'] : '');
    $longitude     = (isset($_POST['get_off']) ? $_POST['get_off'] : '');
    $latitude      = (isset($_POST['latitude']) ? $_POST['latitude'] : '');
    $loc_content   = (isset($_POST['loc_content']) ? rem_slashes($_POST['loc_content']) : '');

    if (is_save_changes()) {
        if (empty($loc_name)) {
            $mess_arr[] = '- Please input your location name';
        }

        if (empty($loc_type)) {
            $mess_arr[] = '- Please select your location type';
        }

        if (empty($latitude)) {
            $mess_arr[] = '- Please input your location coordinate on map';
        }
        //
        // if (empty($loc_corporate)) {
        //     $mess_arr[] = '- Please input the corporate of your location';
        // }

        if (empty($mess_arr)) {
            $is_save_success = update_article($_GET['id'], $loc_name, '', '', $loc_content, 'publish', 'locations');

            if ($is_save_success) {
                set_additional_data($_GET['id']);
                set_locations_images($_GET['id']);
                $message = '<div class="message success">New locations has save succesfully.</div>';
            } else {
                $message = '<div class="message error">Something wrong, please try again.</div>';
            }
        } else {
            $message = '<div class="message error"><p>'.implode('<br />', $mess_arr).'</p></div>';
        }
    }

    $default_pointer  = 'http://'.SITE_URL.'/lumonata-plugins/locations/images/ico-pointer.png';

    $button    = '
	<li>'.button('button=save_changes&label=Save').'</li>
	<li>'.button('button=add_new', get_state_url('locations').'&prc=add_new').'</li>
	<li>'.button('button=cancel', get_state_url('locations')).'</li>';

    set_template(PLUGINS_PATH.'/locations/form.html', 'locations_form_template');
    add_block('locations_form', 'loc_form', 'locations_form_template');
    add_actions('section_title', 'Edit Locations');

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
    $q = $db->prepare_query($s, 'locations', $_GET['id']);
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {
        $d = $db->fetch_array($r);

        $loc_type = get_additional_field($_GET['id'], 'loc_type', 'locations');
        // $loc_corporate = get_additional_field($_GET['id'], 'loc_corporate', 'locations');

        add_variable('site_url', SITE_URL);
        add_variable('message', $message);
        add_variable('button', $button);
        add_variable('post_id', '');

        add_variable('form_title', 'Edit Location');
        add_variable('loc_name', $d['larticle_title']);
        add_variable('loc_content', textarea('loc_content', 0, rem_slashes($d['larticle_content']), $_GET['id'], true));
        add_variable('loc_type', get_loc_type_list($loc_type));
        add_variable('get_off', get_additional_field($_GET['id'], 'get_off', 'locations'));
        add_variable('longitude', get_additional_field($_GET['id'], 'longitude', 'locations'));
        add_variable('latitude', get_additional_field($_GET['id'], 'latitude', 'locations'));

        add_variable('locations_image',get_location_image_top(0, $_GET['id'], 'locations'));

        
        add_variable('destination_form_choose', destination_form_choose_location($_GET['id'], 'locations'));


        // add_variable('corporate_list', get_corporate_option_list($loc_corporate));
        add_variable('default_pointer', $default_pointer);
        add_variable('state_url', get_state_url('locations'));
        add_variable('message', $message);

        add_variable('button', $button);
        add_variable('site_url', SITE_URL);
        add_variable('plugin_url', SITE_URL.'/lumonata-plugins/locations');

        parse_template('locations_form', 'loc_form', false);
        return return_template('locations_form_template');
    }
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk menghapus data locations di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_locations($id='')
{
    global $db;

    $s = 'DELETE FROM lumonata_articles WHERE larticle_type=%s AND larticle_id=%d';
    $q = $db->prepare_query($s, 'locations', $id);
    if ($db->do_query($q)) {
        $s2 = 'DELETE FROM lumonata_additional_fields WHERE lapp_id=%d AND lapp_name=%s';
        $q2 = $db->prepare_query($s2, $id, 'locations');
        $r2 = $db->do_query($q2);

        return true;
    } else {
        return false;
    }
}


function get_location_image_top($index, $post_id, $apps)
{
    $content = '';
    $main_image = get_additional_field($post_id, 'locations_image', $apps);
    if (!empty($main_image)) {
        $content .= '';
        $content .= '
        <div class="box">
            <img src="http://'.SITE_URL.'/lumonata-plugins/locations/background/'.$main_image.'" />
            <input type="hidden" name="additional_fields[locations_image]['.$index.']" value="'.$main_image.'" />
            <div class="overlay" style="display: none;">
                <a data-post-id="'.$post_id.'" data-bg-name="'.$main_image.'" class="locations-image-delete-header">
                    <img src="http://'.SITE_URL.'/lumonata-plugins/additional/images/delete.png">
                </a>
            </div>
        </div>';
    }

    return $content;
}


/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk menghapus multiple data locations di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_all_locations()
{
    global $db;

    add_actions('section_title', 'Delete Locations');

    $warning = '
	<h1 class="form-title">Delete Locations</h1>
	<div class="tab_container">
		<div class="single_content locations-wrapp">';
    $warning .= '
			<form action="" method="post">';
    $warning .= '
				<div class="alert_red_form boxs">
					<strong>
						Are you sure want to delete this
						location'.(count($_POST['select']) > 1 ? 's' : '').' :
					</strong>
					<ol>';
    foreach ($_POST['select'] as $key=>$val) {
        $s = 'SELECT larticle_title FROM lumonata_articles WHERE larticle_id=%d';
        $q = $db->prepare_query($s, $val);
        $r = $db->do_query($q);
        $d = $db->fetch_array($r);


        $warning .= '
							<li>
								'.$d['larticle_title'].'
								<input type="hidden" name="id[]" value="'.$val.'" />
							</li>';
    }
    $warning .= '
					</ol>
				</div>
				<div class="alert-button-box">
					<input type="submit" name="confirm_delete" value="Yes" class="button" />
					<input type="button" name="confirm_delete" value="No" class="button" onclick="location=\''.get_state_url('Locations').'\'" />
				</div>
			</form>
		</div>
	</div>';

    return $warning;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk menyimpan data additional di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_additional_data($post_id)
{
    //-- Address Line 1
    $get_off = (isset($_POST['get_off']) ? $_POST['get_off'] : '');
    edit_additional_field($post_id, 'get_off', $get_off, 'locations');

    //-- Address Line 2
    $longitude = (isset($_POST['longitude']) ? $_POST['longitude'] : '');
    edit_additional_field($post_id, 'longitude', $longitude, 'locations');

    //-- Coordinate on Map
    $latitude = (isset($_POST['latitude']) ? $_POST['latitude'] : '');
    edit_additional_field($post_id, 'latitude', $latitude, 'locations');

    //-- Location type
    $loc_type = (isset($_POST['loc_type']) ? $_POST['loc_type'] : '');
    edit_additional_field($post_id, 'loc_type', $loc_type, 'locations');

    //-- Destination
    $destination = (isset($_POST['destination']) ? $_POST['destination'] : '');
    edit_additional_field($post_id, 'destination', $destination, 'locations');

    //-- Corporate
    // $loc_corporate = (isset($_POST['corporate']) ? $_POST['corporate'] : '');
    // edit_additional_field($post_id, 'loc_corporate', $loc_corporate, 'locations');
}

/*
| -------------------------------------------------------------------------------------------------------------------------
    Fungsi ini digunakan untuk menyimpan data setting locations
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_location_setting()
{
    global $db;

    //-- Run Ajax Request
    if (is_ajax_request()) {
        return execute_loc_ajax_request();
    }

    //-- Save Setting Data
    $message = '';

    if (is_save_changes()) {
        foreach ($_POST['description'] as $idx=>$post) 
        {
            $is_edit_success = edit_additional_field($idx, 'location_text', $post, 'destinations');
        }

        foreach ($_POST['title'] as $idx=>$post) 
        {
            $is_edit_success = edit_additional_field($idx, 'location_title', $post, 'destinations');
        }

        foreach ($_POST['meta_title'] as $idx=>$post) 
        {
            $is_edit_success = edit_additional_field($idx, 'location_meta_title', $post, 'destinations');
        }

        foreach ($_POST['meta_description'] as $idx=>$post) 
        {
            $is_edit_success = edit_additional_field($idx, 'location_meta_description', $post, 'destinations');
        }

        foreach ($_POST['meta_keywords'] as $idx=>$post) 
        {
            $is_edit_success = edit_additional_field($idx, 'location_meta_keywords', $post, 'destinations');
        }

        foreach($_POST['title_lang'] as $idx=>$post)
        {
            foreach($post as $index=>$value)
            {
                $is_edit_success = edit_additional_field($index, 'location_title_'.$idx, $value, 'destinations');
            }    
        }

        foreach($_POST['description_lang'] as $idx=>$post)
        {
            foreach($post as $index=>$value)
            {
                $is_edit_success = edit_additional_field($index, 'location_text_'.$idx, $value, 'destinations');
            }    
        }

        foreach($_POST['meta_title_lang'] as $idx=>$post)
        {
            foreach($post as $index=>$value)
            {
                $is_edit_success = edit_additional_field($index, 'location_meta_title_'.$idx, $value, 'destinations');
            }    
        }

        foreach($_POST['meta_description_lang'] as $idx=>$post)
        {
            foreach($post as $index=>$value)
            {
                $is_edit_success = edit_additional_field($index, 'location_meta_description_'.$idx, $value, 'destinations');
            }    
        }

        foreach($_POST['meta_keywords_lang'] as $idx=>$post)
        {
            foreach($post as $index=>$value)
            {
                $is_edit_success = edit_additional_field($index, 'location_meta_keywords_'.$idx, $value, 'destinations');
            }    
        }
        // if ($is_edit_success) {
        //     $message = '<div class="message success">Location setting data has been successfully edited.</div>';
        // } else {
        //     $message = '<div class="message error">Something wrong, please try again.</div>';
        // }
    }

    //-- Get Setting Data
    $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s ORDER BY lorder ASC';
    $q = $db->prepare_query($s, 'destinations');
    $r = $db->do_query($q);
    if ($db->num_rows($r) > 0) {

        set_template(PLUGINS_PATH.'/locations/setting.html', 'setting_form_template');
        add_block('setting_form_list', 'set_form_list', 'setting_form_template');

        while ($d = $db->fetch_array($r)) {
            add_variable('dest_id', $d['larticle_id']);
            add_variable('dest_name', $d['larticle_title']);
            add_variable('title', get_additional_field($d['larticle_id'], 'location_title', 'destinations'));
            add_variable('description', get_additional_field($d['larticle_id'], 'location_text', 'destinations'));
            add_variable('meta_title', get_additional_field($d['larticle_id'], 'location_meta_title', 'destinations'));
            add_variable('meta_description', get_additional_field($d['larticle_id'], 'location_meta_description', 'destinations'));
            add_variable('meta_keywords', get_additional_field($d['larticle_id'], 'location_meta_keywords', 'destinations'));

            parse_template('setting_form_list', 'set_form_list', true);
        }

        $button    = '
		<li>'.button('button=save_changes&label=Save').'</li>
		<li>'.button('button=cancel', get_state_url($_GET['state']).'&tab=blogs').'</li>';

        add_block('setting_form', 'set_form', 'setting_form_template');
        add_actions('section_title', 'Locations - Setting');

        add_variable( 'language_button', attemp_actions( 'page_post_language_button' ) );
        add_variable('app_title', 'Locations Setting');
        add_variable('button', $button);
        add_variable('message', $message);
        add_variable( 'HTTP', HTTP );
        add_variable('site_url', SITE_URL);
        add_variable('state_url', get_state_url($_GET['state']).'&tab=setting');
        add_variable('tab', set_tabs(array('blogs'=>'Locations','setting'=>'Setting'), 'setting'));

        add_variable( 'language_data', attemp_actions( 'post_type_setting_location_language_data' ) );

        parse_template('setting_form', 'set_form', false);
        return return_template('setting_form_template');
    }
}

// function get_corporate_option_list($sef='')
// {
//     global $db;
//
//
//     $s = 'SELECT * FROM lumonata_articles WHERE larticle_type=%s';
//     $q = $db->prepare_query($s, 'destinations');
//     $r = $db->do_query($q);
//
//     $options = '<option value="" data-pointer="">-- Select corporate destination --</option>';
//     while ($d=$db->fetch_array($r)) {
//         $coordinate = get_additional_field($d['larticle_id'], 'coordinate_location', 'destinations');
//         $options .= '<option data-pointer="'.$coordinate.'" value="'.$d['lsef'].'" '.($sef==$d['lsef'] ? 'selected="selected"' : '').'>'.$d['larticle_title'].'</option>';
//     }
//
//     return $options;
// }

function get_loc_type_list($type='')
{
    global $db;

    $result = '';
    $data = array(
        array('name'=>'Shops','icon'=>'icon_shop_small.svg'),
        array('name'=>'Restaurants','icon'=>'icon_restaurant_small.svg'),
        array('name'=>'Attractions','icon'=>'icon_attractions_small.svg'),
        array('name'=>'Misc','icon'=>'icon_misc_small.svg')
    );

    foreach ($data as $i=>$obj) {
        $checked = ($obj['name']==$type ? 'checked' : '');
        $result .= '
		<li>
			<div class="lbox clearfixed">
				<input id="opt-'.$i.'" type="radio" name="loc_type" value="'.$obj['name'].'" autocomplete="off" '.$checked.' />
				<label for="opt-'.$i.'">'.$obj['name'].'</label>
				<img src="http://'.SITE_URL.'/lumonata-plugins/locations/images/'.$obj['icon'].'" />
			</div>
		</li>';
    }

    return $result;
}

function get_destination_location_text($post_id)
{
    $location_text = get_additional_field($post_id, 'location_text', 'destinations');
    return nl2br($location_text);
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menyimpan data destinaton corporate image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function set_locations_images($post_id)
{
	if(isset($_POST['locations_image']))
	{
		//-- CHECK FOLDER EXIST
		if(!file_exists(PLUGINS_PATH.'/locations/background/'))
			mkdir(PLUGINS_PATH.'/locations/background/');

		preg_match('#^data:image/\w+;base64,#i', $_POST['locations_image'], $matches);
		if(!empty($matches))
		{
			while(true){
				$name = uniqid(rand(), true).'.jpg';
				if(!file_exists(PLUGINS_PATH.'/locations/background/'.$name))
				break;
			}

			$background = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $_POST['locations_image']));
			file_put_contents(PLUGINS_PATH.'/locations/background/'.$name, $background);

			if(file_exists(PLUGINS_PATH.'/locations/background/'.$name))
			{
				$filename = $name;
			}

			//-- Delete old image
			$old_bg = get_additional_field($post_id, 'locations_image', 'locations');
			if(!empty($old_bg) && file_exists(PLUGINS_PATH.'/locations/background/'.$old_bg))
				unlink(PLUGINS_PATH.'/locations/background/'.$old_bg);
		}
		else
		{
			$filename = $_POST['locations_image'];
		}

	    edit_additional_field($post_id, 'locations_image', $filename, 'locations');
	}
}

function execute_loc_ajax_request()
{
    global $db;

    if ($_POST['ajax_key'] =='delete-locations') {
        if (delete_locations($_POST['loc_id'])) {
            echo '{"result":"success"}';
        } else {
            echo '{"result":"failed"}';
        }
    }

    if($_POST['ajax_key'] =='delete-background-image')
    {
      if(delete_locations_image($_POST['post_id'], $_POST['bg_name']))
      {
        echo '{"result":"success"}';
        exit;
      }
      else
      {
        echo '{"result":"failed"}';
        exit;
      }
    }

    if ($_POST['ajax_key'] =='view-list-locations') {
        $data = get_location_list_ajax();

        if (empty($data)) {
            echo '{"result":"failed"}';
            exit;
        } else {
            $prm['result'] = 'success';
            $prm['list'] = $data['list'];
            $prm['more'] = $data['more'];

            echo json_encode($prm);
        }
    }

    if ($_POST['ajax_key'] =='reorder-list') {
        update_articles_order($_POST['theitem'], $_POST['start'], $_POST['state']);
    }


    exit;
}

/*
| -------------------------------------------------------------------------------------------------------------------------
	Fungsi ini digunakan untuk menghapus background image di database
| -------------------------------------------------------------------------------------------------------------------------
*/
function delete_locations_image($post_id='',$bg_name='')
{
	if(empty($post_id)) return true;

	$bg_image = get_additional_field($post_id,'locations_image','locations');
	if($bg_image==$bg_name)
	{
		if(file_exists(PLUGINS_PATH.'/locations/background/'.$bg_image)){
      unlink(PLUGINS_PATH.'/locations/background/'.$bg_image);
    }
	}

	if(edit_additional_field($post_id, 'locations_image', '', 'locations'))
	{
		return true;
	}
	else
	{
		return false;
	}
}
