<?php

/*
	Plugin Name: Lumonata Static Setting
	Plugin URL: http://lumonata.com/
	Description: -
	Author: Ngurah Rai
	Author URL: http://lumonata.com/
	Version: 1.0.1
*/

add_apps_privileges( 'static', 'Static' );

add_actions( 'tab_admin_aditional', 'view_static_setting', array( 'static' => 'Static' ) );

function view_static_setting()
{
    $alert = '';

    if( isset( $_POST['save_changes'] ) )
    {
        $update = set_static_setting();

        if( $update )
        {
            $alert = '<div class="alert_green_form">Your setting has been updated.</div>';
        }
        else
        {
            $alert = 'div class="alert_green_form">Your setting failed to update.</div>';
        }
    }

    $page_list          = get_all_page_list();

    // $amnaya_best        = get_meta_data( 'amnaya_best', 'static_setting' );

    $company            = get_meta_data( 'company', 'static_setting' );
    $address            = get_meta_data( 'address', 'static_setting' );
    $phone_number       = get_meta_data( 'phone_number', 'static_setting' );
    $fax_number         = get_meta_data( 'fax_number', 'static_setting' );
    $email_address      = get_meta_data( 'email_address', 'static_setting' );

    $pl_viasa           = get_meta_data( 'pl_viasa', 'static_setting' );
    $pl_dinara          = get_meta_data( 'pl_dinara', 'static_setting' );

    $smtp_server        = get_meta_data( 'smtp', 'static_setting' );
    $smtp_email_port    = get_meta_data( 'smtp_email_port', 'static_setting' );
    $smtp_email_pwd     = get_meta_data( 'smtp_email_pwd', 'static_setting' );
    $smtp_email_address = get_meta_data( 'smtp_email_address', 'static_setting' );

    $r_public_key       = get_meta_data( 'r_public_key', 'static_setting' );
    $r_secret_key       = get_meta_data( 'r_secret_key', 'static_setting' );
    // $snipset            = get_meta_data( 'snipset', 'static_setting' );

    $map_api_key        = get_meta_data( 'map_api_key', 'static_setting' );
    $longlat            = get_meta_data( 'longlat', 'static_setting' );
    $short_link         = get_meta_data( 'short_link', 'static_setting' );

    $mc_api_key         = get_meta_data( 'mail_chimp_api_key', 'static_setting' );
    $mc_list_id         = get_meta_data( 'mail_chimp_list_id', 'static_setting' );

    // $fb_link            = get_meta_data( 'fb_link', 'static_setting' );
    // $tweet_link         = get_meta_data( 'tweet_link', 'static_setting' );
    // $google_plus_link   = get_meta_data( 'google_plus_link', 'static_setting' );
    // $linkedin_link      = get_meta_data( 'linkedin_link', 'static_setting' );
    // $youtube_link       = get_meta_data( 'youtube_link', 'static_setting' );
    // $vimeo_link         = get_meta_data( 'vimeo_link', 'static_setting' );
    // $flickr_link        = get_meta_data( 'flickr_link', 'static_setting' );
    // $pinterest_link     = get_meta_data( 'pinterest_link', 'static_setting' );
    // $tumblr_link        = get_meta_data( 'tumblr_link', 'static_setting' );
    // $tripadvisor_link   = get_meta_data( 'tripadvisor_link', 'static_setting' );
    // $instagram_link     = get_meta_data( 'instagram_link', 'static_setting' );

    // $fb_text            = get_meta_data( 'fb_text', 'static_setting' );
    // $tweet_text         = get_meta_data( 'tweet_text', 'static_setting' );
    // $google_plus_text   = get_meta_data( 'google_plus_text', 'static_setting' );
    // $linkedin_text      = get_meta_data( 'linkedin_text', 'static_setting' );
    // $youtube_text       = get_meta_data( 'youtube_text', 'static_setting' );
    // $vimeo_text         = get_meta_data( 'vimeo_text', 'static_setting' );
    // $flickr_text        = get_meta_data( 'flickr_text', 'static_setting' );
    // $pinterest_text     = get_meta_data( 'pinterest_text', 'static_setting' );
    // $tumblr_text        = get_meta_data( 'tumblr_text', 'static_setting' );
    // $tripadvisor_text   = get_meta_data( 'tripadvisor_text', 'static_setting' );
    // $instagram_text     = get_meta_data( 'instagram_text', 'static_setting' );
    // $video_youtube_link = get_meta_data( 'video_youtube_link', 'static_setting' );

    $ig_client_id       = get_meta_data( 'ig_client_id', 'static_setting' );
    $ig_client_secret   = get_meta_data( 'ig_client_secret', 'static_setting' );
    $ig_token           = get_meta_data( 'ig_token', 'static_setting' );

    // $google_analytics            = get_meta_data( 'google_analytics', 'static_setting' );
    $google_tag_manager          = get_meta_data( 'google_tag_manager', 'static_setting' );
    $google_tag_manager_noscript = get_meta_data( 'google_tag_manager_noscript', 'static_setting' );
    // $google_adwords              = get_meta_data( 'google_adwords', 'static_setting' );
    $facebook_pixel              = get_meta_data( 'facebook_pixel', 'static_setting' );
    // $hotjar_tracking             = get_meta_data( 'hotjar_tracking', 'static_setting' );

    $disqus_script     = get_meta_data( 'disqus_script', 'static_setting' );
    $disqus_count_code = get_meta_data( 'disqus_count_code', 'static_setting' );



    // $description_404    = get_meta_data( 'description_404', 'static_setting' );

    set_template( PLUGINS_PATH . '/static/static-form.html', 'add_settings_template' );

    add_block( 'settings_form', 'stf', 'add_settings_template' );

    // add_variable( 'amnaya_best', $amnaya_best );
    add_variable( 'alert', $alert );
    add_variable( 'company', $company );
    add_variable( 'address', $address );
    add_variable( 'phone_number', $phone_number );
    add_variable( 'fax_number', $fax_number );
    add_variable( 'email_address', $email_address );

    add_variable( 'pl_viasa', $pl_viasa );
    add_variable( 'pl_dinara', $pl_dinara );

    add_variable( 'smtp_server', $smtp_server );
    add_variable( 'smtp_email_port', $smtp_email_port );
    add_variable( 'smtp_email_address', $smtp_email_address );
    add_variable( 'smtp_email_pwd', $smtp_email_pwd );

    add_variable( 'r_public_key', $r_public_key );
    add_variable( 'r_secret_key', $r_secret_key );
    // add_variable( 'snipset', $snipset );

    add_variable( 'map_api_key', $map_api_key );
    add_variable( 'longlat', $longlat );
    add_variable( 'short_link', $short_link );

    add_variable( 'mail_chimp_api_key', $mc_api_key );
    add_variable( 'mail_chimp_list_id', $mc_list_id );
    add_variable( 'page_list', $page_list );

    add_variable( 'ig_client_id', $ig_client_id );
    add_variable( 'ig_client_secret', $ig_client_secret );
    add_variable( 'ig_token', $ig_token );

    $head_html = htmlspecialchars("(<head></head>)");
    $body_html = htmlspecialchars("(<body></body>)");
    add_variable( 'head_html', $head_html );
    add_variable( 'body_html', $body_html );

    // add_variable( 'fb_link', $fb_link );
    // add_variable( 'tweet_link', $tweet_link );
    // add_variable( 'google_plus_link', $google_plus_link );
    // add_variable( 'linkedin_link', $linkedin_link );
    // add_variable( 'youtube_link', $youtube_link );
    // add_variable( 'vimeo_link', $vimeo_link );
    // add_variable( 'flickr_link', $flickr_link );
    // add_variable( 'pinterest_link', $pinterest_link );
    // add_variable( 'tumblr_link', $tumblr_link );
    // add_variable( 'tripadvisor_link', $tripadvisor_link );
    // add_variable( 'instagram_link', $instagram_link );

    // add_variable( 'fb_text', $fb_text );
    // add_variable( 'tweet_text', $tweet_text );
    // add_variable( 'google_plus_text', $google_plus_text );
    // add_variable( 'linkedin_text', $linkedin_text );
    // add_variable( 'youtube_text', $youtube_text );
    // add_variable( 'vimeo_text', $vimeo_text );
    // add_variable( 'flickr_text', $flickr_text );
    // add_variable( 'pinterest_text', $pinterest_text );
    // add_variable( 'tumblr_text', $tumblr_text );
    // add_variable( 'tripadvisor_text', $tripadvisor_text );
    // add_variable( 'instagram_text', $instagram_text );
    // add_variable( 'video_youtube_link', $video_youtube_link );

    add_variable( 'google_tag_manager', $google_tag_manager );
    add_variable( 'google_tag_manager_noscript', $google_tag_manager_noscript );
    // add_variable( 'google_analytics', $google_analytics );
    // add_variable( 'google_adwords', $google_adwords );
    add_variable( 'facebook_pixel', $facebook_pixel );
    // add_variable( 'hotjar_tracking', $hotjar_tracking );
    // add_variable( 'description_404', $description_404 );

    add_variable( 'disqus_script', $disqus_script );
    add_variable( 'disqus_count_code', $disqus_count_code );

    add_variable( 'admin_tab', setting_tabs() );
    add_variable( 'save_changes_botton', save_changes_botton() );
    add_variable( 'plugin_url', SITE_URL . '/lumonata-plugins/static' );


    add_variable( 'HTTP', HTTP );
    add_variable( 'version', "?v=".time() );

    parse_template( 'settings_form', 'stf', false );

    return return_template( 'add_settings_template' );
}

function is_num_static_setting( $key )
{
    global $db;

    $s = 'SELECT * FROM lumonata_meta_data WHERE lmeta_name=%s AND lapp_name=%s';
    $q = $db->prepare_query( $s, $key, 'static_setting' );
    $n = count_rows( $q );

    if( $n > 0 )
    {
        return true;
    }

    return false;
}

function set_static_setting()
{
    global $db;

    upload_static_file();

    foreach( $_POST as $key => $val )
    {
        if( $key != 'save_changes' )
        {
            if( is_num_static_setting( $key ) )
            {
                $s = 'UPDATE lumonata_meta_data SET lmeta_value=%s
					  WHERE lmeta_name=%s AND lapp_name=%s';
                $q = $db->prepare_query( $s, $val, $key, 'static_setting' );
                $r = $db->do_query( $q );
            }
            else
            {
                $s = 'INSERT INTO lumonata_meta_data(
							lmeta_name,
							lmeta_value,
							lapp_name)
					  VALUES(%s,%s,%s)';
                $q = $db->prepare_query( $s, $key, $val, 'static_setting' );
                $r = $db->do_query( $q );
            }
        }
    }

    return true;
}

function upload_static_file()
{
    global $db;

    if( !isset( $_FILES['menu']['name'] ) && empty( $_FILES['menu']['name'] ) )
    {
        return;
    }

    $file_name   = $_FILES['menu']['name'];
    $file_size   = $_FILES['menu']['size'];
    $file_type   = $_FILES['menu']['type'];
    $file_source = $_FILES['menu']['tmp_name'];

    if( is_allow_file_size( $file_size ) )
    {
        if( is_allow_file_type( $file_type, 'pdf' ) )
        {
            $destination = PLUGINS_PATH . '/static/files/' . $file_name;
            if( upload( $file_source, $destination ) )
            {
                $old_file = get_meta_data( 'menu', 'static_setting' );

                if( file_exists( PLUGINS_PATH . '/static/files/' . $old_file ) )
                {
                    unlink( PLUGINS_PATH . '/static/files/' . $old_file );
                }

                if( is_num_static_setting( 'menu' ) )
                {
                    $s = 'UPDATE lumonata_meta_data SET lmeta_value=%s
						  WHERE lmeta_name=%s AND lapp_name=%s';
                    $q = $db->prepare_query( $s, $file_name, 'menu', 'static_setting' );
                    $r = $db->do_query( $q );
                }
                else
                {
                    $s = 'INSERT INTO lumonata_meta_data(
								lmeta_name,
								lmeta_value,
								lapp_name)
						  VALUES(%s,%s,%s)';
                    $q = $db->prepare_query( $s, 'menu', $file_name, 'static_setting' );
                    $r = $db->do_query( $q );
                }
            }
        }
    }
}

function update_static_setting()
{
    global $db;

    foreach( $_POST as $key => $val )
    {
        if( $key != 'save_changes' )
        {
            $s = 'UPDATE lumonata_meta_data SET lmeta_value=%s
				  WHERE lmeta_name=%s AND lapp_name=%s';
            $q = $db->prepare_query( $s, $val, $key, 'static_setting' );
            $r = $db->do_query( $q );
        }
    }
}

function setting_tabs()
{
    global $actions;

    $tabs = run_actions( 'tab_admin' );
    $tabs = $actions->action['tab_admin']['args'][0][0];

    if( is_contributor() || is_author() )
    {
        $tabs = $tabs[0];
    }
    else
    {
        $tabs = $tabs;
    }

    $tabb     = '';
    $tab_keys = array_keys( $tabs );

    if( empty( $_GET['tab'] ) )
    {
        $the_tab = $tab_keys[0];
    }
    else
    {
        $the_tab = $_GET['tab'];
    }

    $tabs = set_tabs( $tabs, $the_tab );

    return $tabs;
}

function get_all_page_list()
{
    global $db;

    $list = '';

    $s = 'SELECT * FROM lumonata_articles WHERE larticle_status=%s AND larticle_type=%s';
    $q = $db->prepare_query( $s, 'publish', 'pages' );
    $n = count_rows( $q );

    if( $n == 0 )
    {
        return $list;
    }

    $id = get_meta_data( 'homepage_content', 'static_setting' );

    $r = $db->do_query( $q );

    while( $d = $db->fetch_array( $r ) )
    {
        $list .= '
        <option value="' . $d['larticle_id'] . '" ' . ( $id == $d['larticle_id'] ? 'selected="selected"' : '' ) . '>
        	' . $d['larticle_title'] . '
        </option>';
    }

    return $list;
}

?>
